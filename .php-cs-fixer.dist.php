<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__)
    ->exclude('vendor')
    ->exclude('var')
    ->exclude('node_modules')
    ->exclude('src/Service/Factory') // Because CS Fixer is doing weird things with `match`
;

$config = new PhpCsFixer\Config();
return $config->setRules([
    '@Symfony' => true,
    'full_opening_tag' => false,
    'yoda_style' => false,
    /**
     * Workaround, avoid union types to have whitespaces
     * @see https://github.com/FriendsOfPHP/PHP-CS-Fixer/issues/5495
     *
     */
    'binary_operator_spaces' => ['operators' => ['|' => null]],
    'phpdoc_to_comment' => false, // Disable conversion of phpdoc to simple comments
])
    ->setFinder($finder);