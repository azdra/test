Feature: Clients data feature

  Scenario: I search a client by name
    Given I am authenticated as admin
    When I globally search "Live" in clients
    Then I should receive "1" result in clients
    And I should receive a successful response

  Scenario: I globally search with POST request
    Given I am authenticated as admin
    When I send a "POST" request to "/client/global-search"
    Then I should receive a response with status code "405"