@telephonyprofile
Feature: Telephony profile

  #Scenario: As an admin I should be able to modify the language of telephony profile
  #  Given I am authenticated as admin
  #  Given the language of telephony profile 'Telephony profile AC #1' is 'fr'
  #  When I update the language of a telephony profile 'Telephony profile AC #1' to 'en'
  #  Then I should receive a successful response
  #  Then the language of telephony profile 'Telephony profile AC #1' is 'en'

  #Scenario: As a manager I should not be able to modify the language of telephony profile
  #  Given I am authenticated as manager
  #  Given the language of telephony profile 'Telephony profile AC #1' is 'en'
  #  When I update the language of a telephony profile 'Telephony profile AC #1' to 'fr'
  #  Then I should receive a forbidden response
  #  Then the language of telephony profile 'Telephony profile AC #1' is 'en'

  #Scenario: As an admin I should be able to modify the phone number of telephony profile
  #  Given I am authenticated as admin
  #  Given the number of telephony profile 'Telephony profile AC #1' is '0808080808'
  #  When I update the phone number of a telephony profile 'Telephony profile AC #1' to '0606060606'
  #  Then I should receive a successful response
  #  Then the number of telephony profile 'Telephony profile AC #1' is '0606060606'

  #Scenario: As a manager I should not be able to modify the phone number of telephony profile
  #  Given I am authenticated as manager
  #  Given the number of telephony profile 'Telephony profile AC #1' is '0606060606'
  #  When I update the phone number of a telephony profile 'Telephony profile AC #1' to '0808080808'
  #  Then I should receive a forbidden response
  #  Then the number of telephony profile 'Telephony profile AC #1' is '0606060606'