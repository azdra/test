Feature: Convocation feature

  #Scenario: I can see convocation models list on my client account as admin
  #  Given I am authenticated as admin
  #  When I send a "GET" request to "/client/edit/1?tab-name=clientEmailsForm"
  #  Then I should receive a response with status code "200"

  Scenario: I can add a new convocation model as admin only
    Given I am authenticated as admin
    When I send a "GET" request to "/convocation/create/1"
    Then I should receive a response with status code "200"
    Given I am authenticated as manager
    When I send a "GET" request to "/convocation/create/1"
    Then I should receive an access denied exception

  Scenario: I can edit an existing convocation model as admin only
    Given I am authenticated as admin
    When I send a "POST" request to "/convocation/update/1"
    Then I should receive a response with status code "200"
    Given I am authenticated as manager
    When I send a "POST" request to "/convocation/update/1"
    Then I should receive an access denied exception

  Scenario: I can desactivate an existing convocation model as admin only
    Given I am authenticated as admin
    When I send a "GET" request to "/convocation/disable/1"
    Then I should receive a response with status code "302"
    And I should be redirected to "/client/edit/2?tab-name=clientEmailsForm"
    Given I am authenticated as manager
    When I send a "GET" request to "/convocation/disable/1"
    Then I should receive an access denied exception