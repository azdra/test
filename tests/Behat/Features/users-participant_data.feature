Feature: Users participant data feature

  Scenario: I search a user participant by firstname
    Given I am authenticated as admin
    When I globally search "Gaspard" in users participants
    Then I should receive "2" result in users participants
    And I should receive a successful response

  Scenario: I search a user participant by lastname
    Given I am authenticated as admin
    When I globally search "Alizan" in users participants
    Then I should receive "2" result in users participants
    And I should receive a successful response

  Scenario: I globally search with POST request
    Given I am authenticated as admin
    When I send a "POST" request to "/user/global-search?context=participant"
    Then I should receive a response with status code "405"