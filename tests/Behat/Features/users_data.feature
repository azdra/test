Feature: Users data feature

  Scenario: I search a user by firstname
    Given I am authenticated as admin
    When I globally search "Jean" in users
    Then I should receive "1" result in users
    And I should receive a successful response

  Scenario: I search a user by lastname
    Given I am authenticated as admin
    When I globally search "ValJean" in users
    Then I should receive "1" result in users
    And I should receive a successful response

  Scenario: I globally search with POST request
    Given I am authenticated as admin
    When I send a "POST" request to "/user/global-search?context=manager"
    Then I should receive a response with status code "405"