@downloadarchive
Feature: download file
  Scenario: A manager should be able to download generated archive
    Given I am authenticated as manager
    And a file is stored with token "tokengenerated"
    When I send a "GET" request to "/file/download-archive/tokengenerated"
    Then I should receive a successful response