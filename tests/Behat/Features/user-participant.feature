Feature: User participant feature

  Scenario: Accessing participant list without being logged in
    When I send a "GET" request to "/participant/list"
    Then I should receive a response with status code "302"
    And I should be redirected to "/login"

  Scenario: Admin retrieve complete list of participants
    Given I am authenticated as admin
    When I send a "GET" request to "/participant/list"
    Then I should receive a response with status code "200"

  Scenario: Manager retrieve complete list of participants
    When I am authenticated as manager
    And I send a "GET" request to "/participant/list"
    Then I should receive a response with status code "200"

  Scenario: I can edit a participant as Admin
    Given I am authenticated as admin
    When I send a "GET" request to "/user/edit/participant/2"
    Then I should receive a response with status code "200"

  Scenario: I can edit a participant as Manager
    Given I am authenticated as manager
    When I send a "GET" request to "/user/edit/participant/2"
    Then I should receive a response with status code "200"

  Scenario: I can't edit a participant as simple user
    Given I am logged in as user "laurent.gina@live-session.fr" with password "test"
    And I send a "POST" request to "/user/edit/participant/2"
    Then I should receive an access denied exception

  Scenario: I can delete a participant as Admin
    Given I am authenticated as admin
    When I send a "POST" request to "/user/delete/6"
    And I should receive a response with status code "302"

  Scenario: I can delete a participant as Manager
    Given I am authenticated as manager
    When I send a "POST" request to "/user/delete/7"
    And I should receive a response with status code "302"

  Scenario: I can not delete a participant of another platform as Manager
    Given I am authenticated as manager
    When I send a "POST" request to "/user/delete/11"
    Then I should receive an access denied exception

  Scenario: I can't delete a participant as simple user
    Given I am logged in as user "laurent.gina@live-session.fr" with password "test"
    When I send a "POST" request to "/user/delete/9"
    Then I should receive an access denied exception

  Scenario: I can create a participant as Admin
    Given I am authenticated as admin
    When I send a "POST" request to "/user/create/participant"
    Then I should receive a response with status code "200"

  Scenario: I can create a participant as Manager
    Given I am authenticated as manager
    And I send a "POST" request to "/user/create/participant"
    Then I should receive a response with status code "200"

  Scenario: I can create a participant as simple user
    Given I am logged in as user "laurent.gina@live-session.fr" with password "test"
    And I send a "POST" request to "/user/create/participant"
    Then I should receive an access denied exception