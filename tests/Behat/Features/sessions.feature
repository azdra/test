Feature: Sessions feature

  Scenario: I create a participant with an invalid provider
    Given I am authenticated as admin
    When I send a "GET" request to "/session/create/0"
    Then I should receive a response with status code "404"

  Scenario: I retrieve an invalid session identifier
    Given I am authenticated as admin
    When I send a "GET" request to "/session/get/123456789"
    Then I should receive a response with status code "404"

  Scenario: I update an invalid session identifier as an admin
    Given I am authenticated as admin
    When I send a "POST" request to "/session/get/123456789"
    Then I should receive a response with status code "404"

  Scenario: I update an invalid session identifier as an manager
    Given I am authenticated as manager
    When I send a "POST" request to "/session/get/123456789"
    Then I should receive a response with status code "404"

  Scenario: I update a session as an admin
    Given I am authenticated as admin
    When I send a "GET" request to "/session/1"
    Then I should receive a successful response

  #Scenario: I update a session as an manager
  #  Given I am authenticated as manager
  #  When I send a "GET" request to "/session/1"
  #  Then I should receive a successful response

  Scenario: I can not update a session of another client as an manager
    Given I am authenticated as manager
    When I send a "GET" request to "/session/2"
    Then I should receive an access denied exception

  Scenario: I delete an invalid session identifier
    Given I am authenticated as admin
    When I send a "GET" request to "/session/delete/123456789"
    Then I should receive a response with status code "404"

  Scenario: I retrieve complete list of sessions
    Given I am authenticated as admin
    When I send a "GET" request to "/session/"
    Then I should receive a successful response

  Scenario: I can access to the redirect endpoint of session as anonymous
    When I send a "GET" request to "/session/access/myToken"
    Then I should receive a successful response

  #Scenario: I send a valid file import of sessions
  #  Given I am authenticated as admin
  #  When I send a "POST" request to "/session/import-file-sessions" with files:
  #"""
  #        {
  #            "files" : [{"/data/imports_sessions/MLSM3_Import_Sessions_AC.xlsx"}],
  #        }
  #"""
  #  Then I should receive a response with status code "200"