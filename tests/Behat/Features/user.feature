Feature: User feature

  Scenario: I am logged in with valid credentials
    Given I am logged in as user "contact@live-session.fr" with password "admin"
    Then I should receive a response with status code "200"

  Scenario: I am logged in with invalid credentials
    Given I am logged in as user "contact@live-session.fr" with password "plop"
    Then I should receive an error because of bad credentials

  Scenario: I am logged in with empty username
    Given I am logged in as user "" with password "admin"
    Then I should receive an error because of bad credentials

  Scenario: I am logged in with empty password
    Given I am logged in as user "contact@live-session.fr" with password ""
    Then I should receive an error because of bad credentials

  Scenario: I retrieve complete list of users as Admin
    Given I am authenticated as admin
    When I send a "GET" request to "/user/list"
    Then I should receive a response with status code "200"

  Scenario: I retrieve complete list of users as Manager
    Given I am authenticated as manager
    When I send a "GET" request to "/user/list"
    Then I should receive a response with status code "403"

  Scenario: I can't retrieve complete list of users as simple user
    Given I am logged in as user "laurent.gina@live-session.fr" with password "test"
    And I send a "GET" request to "/user/list"
    Then I should receive an access denied exception

  Scenario: I retrieve complete list of participants as Admin
    Given I am authenticated as admin
    When I send a "GET" request to "/participant/list"
    Then I should receive a response with status code "200"

  Scenario: I retrieve complete list of participants as Manager
    Given I am authenticated as manager
    When I send a "GET" request to "/participant/list"
    Then I should receive a response with status code "200"

  Scenario: I can't retrieve complete list of participants as simple user
    Given I am logged in as user "laurent.gina@live-session.fr" with password "test"
    And I send a "GET" request to "/participant/list"
    Then I should receive an access denied exception

  Scenario: I want to update my password without being logged in
    When I send a "GET" request to "/user/password/update"
    Then I should receive a response with status code "302"
    And I should be redirected to "/login"

  Scenario: I want to update my password while being logged in
    Given I am authenticated as admin
    When I send a "GET" request to "/user/password/update"
    Then I should receive a successful response

  Scenario: I access password reset request
    When I send a "GET" request to "/password/reset-request"
    Then I should receive a successful response

  Scenario: I access password reset page with an invalid token
    When I send a "GET" request to "/password/reset/my_invalid"
    Then I should receive a response with status code "302"
    And I should be redirected to "/login"

  Scenario: I access password reset page with an valid token
    When I send a "GET" request to "/password/reset/my_token"
    Then I should receive a successful response

  Scenario: I access profile update without being logged in
    When I send a "GET" request to "/user/profile"
    Then I should receive a response with status code "302"
    And I should be redirected to "/login"

  Scenario: I access profile update as user
    Given I am authenticated as admin
    When I send a "GET" request to "/user/profile"
    Then I should receive a successful response

  Scenario: I want to logout
    Given I am authenticated as admin
    When I send a "GET" request to "/logout"
    Then I should receive a response with status code "302"
    And I should be redirected to "http://localhost/login"

