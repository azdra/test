Feature: Participants feature
  Scenario: I retrieve complete list of participants
    Given I am authenticated as admin
    When I send a "GET" request to "/participant/list"
    Then I should receive a successful response