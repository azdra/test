@animator
Feature: Animator user feature

  Scenario: A user can not access a session without the right token
    Given I am an anonymous user
    When I send a "GET" request to "/session/4?token=fake-token"
    Then I should be redirected to "/login"

  #Scenario: A user can access a session with the right token
  #  Given I am an anonymous user
  #  When I try to connect to session 'Session for animator features' with the animator token
  #  Then I should be redirected to "/session/7"

  #Scenario: An animator can access the session he animates
  #  Given I am connected as animator to session 'Session for animator features'
  #  When I send a "GET" request to "/session/7"
  #  Then I should receive a successful response

  Scenario: An animator can not update the session he animates
    Given I am connected as animator to session 'Session for animator features'
    When I send a "GET" request to "/session/save"
    Then I should receive a forbidden response

  Scenario: An animator can not delete the session he animates
    Given I am connected as animator to session 'Session for animator features'
    When I send a "GET" request to "/session/delete/7"
    Then I should receive a forbidden response

  Scenario: An animator can not cancel the session he animates
    Given I am connected as animator to session 'Session for animator features'
    When I send a "GET" request to "/session/cancel/7"
    Then I should receive a forbidden response

  Scenario: An animator can not uncancel the session he animates
    Given I am connected as animator to session 'Session for animator features'
    When I send a "GET" request to "/session/uncancel/7"
    Then I should receive a forbidden response

  Scenario: An animator can not access the virtual room he animates
    Given I am connected as animator to session 'Session for animator features'
    When I send a "GET" request to "/session/7/redirect-connector"
    Then I should receive a forbidden response

  Scenario: An animator can not update the participant of the session he animate
    Given I am connected as animator to session 'Session for animator features'
    When I send a "GET" request to "/session/7/update-participants"
    Then I should receive a not allowed response

  #Scenario: An animator can send attendance report of the session he animates
  #  Given I am connected as animator to session 'Session for animator features'
  #  When I send a "POST" request to "/session/send-attendance-report/7"
  #  Then I should be redirected to "/session/7"

  #Scenario: An animator can not send attendance report of the another session
  #  Given I am connected as animator to session 'Session for animator features'
  #  When I send a "POST" request to "/session/send-attendance-report/6"
  #  Then I should receive a forbidden response

  #Scenario: An animator can send training certificate of the session he animates
  #  Given I am connected as animator to session 'Session for animator features'
  #  When I send a "POST" request to "/session/send-training-certificate/7"
  #  Then I should be redirected to "/session/7"

  #Scenario: An animator can not send training certificate of another session
  #  Given I am connected as animator to session 'Session for animator features'
  #  When I send a "POST" request to "/session/send-training-certificate/6"
  #  Then I should receive a forbidden response

  #Scenario: An animator can send the evaluation link of the session he animates
  #  Given I am connected as animator to session 'Session for animator features'
  #  When I send a "POST" request to "/evaluation/send-email-evaluation/7?participant=3"
  #  Then I should receive a successful response

  #Scenario: An animator can not send the evaluation link of another session
  #  Given I am connected as animator to session 'Session for animator features'
  #  When I send a "POST" request to "/evaluation/send-email-evaluation/6"
  #  Then I should receive a forbidden response

  #Scenario: An animator can send the signature link of the session he animates
  #  Given I am connected as animator to session 'Session for animator features'
  #  When I send a "POST" request to "/attendance/send-email-signature/7?participant=3"
  #  Then I should receive a successful response

  #Scenario: An animator can not send the signature link of another session
  #  Given I am connected as animator to session 'Session for animator features'
  #  When I send a "POST" request to "/attendance/send-email-signature/6"
  #  Then I should receive a forbidden response

  #Scenario: An animator can download export of a session
  #  Given I am connected as animator to session 'Session for animator features'
  #  When I send a "POST" request to "/session/download-export/7"
  #  Then I should be redirected near "/file/download-archive"

  #Scenario: An animator can not download export of another session
  #  Given I am connected as animator to session 'Session for animator features'
  #  When I send a "POST" request to "/session/download-export/6"
  #  Then I should receive a forbidden response

  Scenario: An animator can not remove student of the session
    Given I am connected as animator to session 'Session for animator features'
    When I send a "GET" request to "/participant-session-role/4/remove"
    Then I should receive a forbidden response

  #Scenario: An animator can change the status of a participant of the session
  #  Given I am connected as animator to session 'Session for animator features'
  #  When I send a "GET" request to "/participant-session-role/update-manual-presence-status/4/1"
  #  Then I should be redirected to "/session/7?tabName="

  Scenario: An animator can not change the status of a participant of another session
    Given I am connected as animator to session 'Session for animator features'
    When I send a "GET" request to "/participant-session-role/update-manual-presence-status/2/1"
    Then I should receive a forbidden response

  #Scenario: An animator can send the the convocation to a participant of the session
  #  Given I am connected as animator to session 'Session for animator features'
  #  When I send a "GET" request to "/participant-session-role/convocation/send/4"
  #  Then I should be redirected to "/session/7"

  Scenario: An animator can not send the the convocation to a participant of another session
    Given I am connected as animator to session 'Session for animator features'
    When I send a "GET" request to "/participant-session-role/convocation/send/2"
    Then I should receive a forbidden response
