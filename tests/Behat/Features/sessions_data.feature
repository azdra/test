Feature: Sessions data feature

  Scenario: I retrieve animators from an unknown session
    Given I am authenticated as admin
    When I fetch animators from an unknown session
    Then I should receive a response with status code "404"

  Scenario: I retrieve trainees from an unknown session
    Given I am authenticated as admin
    When I fetch animators from an unknown session
    Then I should receive a response with status code "404"

  Scenario: I retrieve animators from a session with animators
    Given I am authenticated as admin
    When I fetch animators from session with reference "Session with only animators"
    Then I should receive "1" animators
    And I should receive a successful response

  Scenario: I retrieve trainees from a session with trainees
    Given I am authenticated as admin
    When I fetch trainees from session with reference "Session with only trainees"
    Then I should receive "1" trainees
    And I should receive a successful response

  Scenario: I retrieve empty animators from a session without animators
    Given I am authenticated as admin
    When I fetch animators from session with reference "Session with only trainees"
    Then I should receive no animators
    And I should receive a successful response

  Scenario: I retrieve empty trainees from a session without trainees
    Given I am authenticated as admin
    When I fetch trainees from session with reference "Session with only animators"
    Then I should receive no trainees
    And I should receive a successful response

  Scenario: I search an outdated session
    Given I am authenticated as admin
    When I globally search "FR - ACN"
    Then I should receive "0" result
    And I should receive a successful response

  Scenario: I search a session by name or reference
    Given I am authenticated as admin
    # I search by name
    When I globally search "Name search"
    Then I should receive "2" result
    And I should receive a successful response
    # I search by reference
    When I globally search "ref-search"
    Then I should receive "2" result
    And I should receive a successful response
    # I search an outdated session
    When I globally search "ref-search" in all sessions
    Then I should receive "3" result
    And I should receive a successful response

  Scenario: I globally search without POST request
    Given I am authenticated as admin
    When I send a "POST" request to "/session/global-search"
    Then I should receive a response with status code "405"