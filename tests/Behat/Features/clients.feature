Feature: Client feature

  Scenario: I retrieve complete list of clients
    Given I am authenticated as admin
    When I send a "GET" request to "/client/"
    Then I should receive a response with status code "200"

  Scenario: I can create an account as an admin
    Given I am authenticated as admin
    When I send a "GET" request to "/client/create"
    Then I should receive a successful response

  Scenario: I can see the lis of accounts as an admin
    Given I am authenticated as admin
    When I send a "GET" request to "/client/"
    Then I should receive a successful response

  Scenario: I can't create an account as a manager
    Given I am authenticated as manager
    When I send a "GET" request to "/client/create"
    Then I should receive a response with status code "403"

  Scenario: I can't see the lis of accounts as an admin
    Given I am authenticated as manager
    When I send a "GET" request to "/client/"
    Then I should receive a response with status code "403"

  #Scenario: I should access successfully to an existing client tab form
  #  Given I am authenticated as admin
  #  When I send a "GET" request to "/client/edit/1?tab-name=clientOrganizationForm"
  #  Then I should receive a successful response

  Scenario: I should not access to an NON existing client tab form
    Given I am authenticated as admin
    When I send a "GET" request to "/client/edit/1?tab-name=clientNonExistedForm"
    Then I should receive a logic exception

  #Scenario: Admin can retrieve list of user account manager
  #  Given I am authenticated as admin
  #  When I send a "GET" request to "/client/edit/1?tab-name=clientAccountManagerListTab"
  #  Then I should receive a response with status code "200"

  #Scenario: No error when accessing client edition with no service associated
  #  Given I am authenticated as admin
  #  When I send a "GET" request to "/client/edit/1"
  #  Then I should receive a response with status code "200"

  #Scenario: Manager can acces to the user account manager list (view mode)
  #  Given I am authenticated as manager
  #  When I send a "GET" request to "/client/edit/1?tab-name=clientAccountManagerListTab"
  #  Then I should receive a response with status code "200"

  Scenario: Simple User can't retrieve list of user account manager
    Given I am logged in as user "laurent.gina@live-session.fr" with password "test"
    And I send a "POST" request to "/client/edit/1?tab-name=clientAccountManagerListTab"
    Then I should receive an access denied exception

  Scenario: Admin can create a new User account manager
    Given I am authenticated as admin
    When I send a "POST" request to "/client/edit/1/create-account-manager?tab-name=clientAccountManagerList"
    Then I should receive a response with status code "200"

  Scenario: Manager can't create a new User account manager
    Given I am authenticated as manager
    When I send a "POST" request to "/client/edit/1/create-account-manager?tab-name=clientAccountManagerList"
    Then I should receive an access denied exception

  Scenario: Manager can't create a new User account manager
    Given I am logged in as user "laurent.gina@live-session.fr" with password "test"
    And I send a "POST" request to "/client/edit/1/create-account-manager?tab-name=clientAccountManagerList"
    Then I should receive an access denied exception

  #Scenario: I should NOT have access to the configuration holder edition as a Manager
  #  Given I am authenticated as manager
  #  When I send a "POST" request to "/client/1/update-client-provider-configuration-holders"
  #  Then I should receive a response with status code "403"

  Scenario: I should NOT have access to the configuration holder edition with a GET method
    Given I am authenticated as manager
    When I send a "GET" request to "/client/1/update-client-provider-configuration-holders"
    Then I should receive a response with status code "405"