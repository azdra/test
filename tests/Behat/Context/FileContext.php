<?php

namespace App\Tests\Behat\Context;

use App\Service\Exporter\ExporterService;

class FileContext extends AbstractContext
{
    public function __construct(private \Redis $redisClient, private string $archivesDirectory)
    {
        parent::__construct();
    }

    /**
     * @Given a file is stored with token :token
     */
    public function aFileIsStored(string $token): void
    {
        $this->redisClient->set(ExporterService::PREFIX_REDIS.$token, "1|{$this->archivesDirectory}/certificat_footer_vide_info.png");
    }
}
