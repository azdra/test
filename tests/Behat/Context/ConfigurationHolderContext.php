<?php

namespace App\Tests\Behat\Context;

use App\Entity\Adobe\AdobeConnectTelephonyProfile;
use App\Repository\AdobeConnectTelephonyProfileRepository;
use Doctrine\ORM\EntityManagerInterface;

class ConfigurationHolderContext extends AbstractContext
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private AdobeConnectTelephonyProfileRepository $telephonyProfileRepository
    ) {
        parent::__construct();
    }

    /**
     * @When I update the language of a telephony profile :telephonyProfileName to :lang
     */
    public function IUpdateLanguageTelephonyProfile(string $telephonyProfileName, string $lang)
    {
        /** @var AdobeConnectTelephonyProfile $telephonyProfile */
        $telephonyProfile = $this->telephonyProfileRepository->findOneBy(['name' => $telephonyProfileName]);
        $id = $telephonyProfile->getId();
        self::$client->request(
            'POST',
            "/configuration-holder/adobe-connect/update/language/audios/$id?language=$lang"
        );
        static::$response = self::$client->getResponse();
    }

    /**
     * @When I update the phone number of a telephony profile :telephonyProfileName to :number
     */
    public function IUpdatePhoneNumberTelephonyProfile(string $telephonyProfileName, string $number)
    {
        /** @var AdobeConnectTelephonyProfile $telephonyProfile */
        $telephonyProfile = $this->telephonyProfileRepository->findOneBy(['name' => $telephonyProfileName]);
        $id = $telephonyProfile->getId();
        self::$client->request(
            'POST',
            "/configuration-holder/adobe-connect/update/number/audios/$id?number=$number"
        );
        static::$response = self::$client->getResponse();
    }

    /**
     * @Given the language of telephony profile :telephonyProfileName is :expectedLang
     */
    public function assertTelephonyProfileLang(string $telephonyProfileName, string $expectedLang)
    {
        $this->entityManager->clear();
        /** @var AdobeConnectTelephonyProfile $telephonyProfile */
        $telephonyProfile = $this->telephonyProfileRepository->findOneBy(['name' => $telephonyProfileName]);
        $this->assertEquals($expectedLang, $telephonyProfile->getPhoneLang());
    }

    /**
     * @Given the number of telephony profile :telephonyProfileName is :expectedNumber
     */
    public function assertTelephonyProfileNumber(string $telephonyProfileName, string $expectedNumber)
    {
        $this->entityManager->clear();
        /** @var AdobeConnectTelephonyProfile $telephonyProfile */
        $telephonyProfile = $this->telephonyProfileRepository->findOneBy(['name' => $telephonyProfileName]);
        $this->assertEquals($expectedNumber, $telephonyProfile->getPhoneNumber());
    }
}
