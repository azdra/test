<?php

namespace App\Tests\Behat\Context;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class BaseContext extends AbstractContext
{
    protected EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    /**
     * @When /^I send a "([^"]*)" request to "([^"]*)"$/
     */
    public function iSendARequestTo($method, $url): void
    {
        self::$client->request($method, $url);
        static::$response = self::$client->getResponse();
    }

    /**
     * @Then /^I should receive a response with status code "([^"]*)"$/
     */
    public function iShouldReceiveAResponseWithStatusCode($code): void
    {
        self::assertResponseStatusCodeSame($code);
    }

    /**
     * @Then I should receive a forbidden response
     */
    public function iShouldReceiveAForbiddenResponse(): void
    {
        $this->iShouldReceiveAResponseWithStatusCode(403);
    }

    /**
     * @Then I should receive a not allowed response
     */
    public function iShouldReceiveANotAllowedResponse(): void
    {
        $this->iShouldReceiveAResponseWithStatusCode(405);
    }

    /**
     * @Then I should be redirected near :route
     */
    public function iShouldBeRedirectedToRouteStartingWith(string $route): void
    {
        $this->assertResponseRedirects();
        $this->assertTrue(self::$client->getResponse()->headers->has('Location'));
        $this->assertStringStartsWith($route, self::$client->getResponse()->headers->get('Location')); //        $constraint = new ResponseConstraint\ResponseIsRedirected();
    }

    /**
     * @Then /^I should receive a successful response$/
     */
    public function iShouldReceiveSuccessfulResponse(): void
    {
        self::assertResponseIsSuccessful();
    }

    /**
     * @Given /^I am authenticated as admin$/
     */
    public function iAmAuthenticatedAsAdmin()
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => 'admin@live-session.fr']);
        self::$client->loginUser($user);
    }

    /**
     * @Given /^I should be redirected to "([^"]*)"$/
     */
    public function iShouldBeRedirectTo($arg1)
    {
        self::assertResponseRedirects($arg1, 302);
    }

    /**
     * @Given /^I am authenticated as manager$/
     */
    public function iAmAuthenticatedAsManager()
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => 'gestionnaire1ls@live-session.fr']);
        self::$client->loginUser($user);
    }

    /**
     * @Then /^I should receive a logic exception$/
     */
    public function iShouldReceiveLogicExceptionResponse()
    {
        $this->expectException(\LogicException::class);
    }

    /**
     * @Then /^I should receive an access denied exception$/
     */
    public function iShouldReceiveAcccessDeniedExceptionResponse()
    {
        $this->expectException(AccessDeniedException::class);
    }
}
