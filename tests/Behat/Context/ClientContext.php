<?php

namespace App\Tests\Behat\Context;

class ClientContext extends AbstractContext
{
    /**
     * @When /^I globally search "([^"]*)" in clients$/
     */
    public function iGloballySearchClients($arg1)
    {
        self::$client->request('GET', '/client/global-search?q='.$arg1);
        static::$response = self::$client->getResponse();
    }

    /**
     * @Then /^I should receive "([^"]*)" result in clients$/
     */
    public function iShouldReceiveResultClients($arg1)
    {
        $content = \json_decode(static::$response->getContent(), true);
        $this->assertCount($arg1, $content);
    }

    /**
     * @When /^I globally search in clients without query$/
     */
    public function iGloballySearchClientsWithoutQuery()
    {
        self::$client->request('GET', '/client/global-search');
        static::$response = self::$client->getResponse();
    }
}
