<?php

namespace App\Tests\Behat\Context;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use Doctrine\ORM\EntityManagerInterface;

class SessionContext extends AbstractContext
{
    protected EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    /**
     * @When /^I fetch animators from session with reference "([^"]*)"$/
     */
    public function iFetchAnimatorsFromSessionWithReference($arg1)
    {
        $session = $this->entityManager->getRepository(ProviderSession::class)->findOneBy(['ref' => $arg1]);

        self::$client->request('GET', '/session/list-animators/'.$session->getId());
        static::$response = self::$client->getResponse();
    }

    /**
     * @When /^I fetch trainees from session with reference "([^"]*)"$/
     */
    public function iFetchTraineesFromSessionWithReference($arg1)
    {
        $session = $this->entityManager->getRepository(ProviderSession::class)->findOneBy(['ref' => $arg1]);

        self::$client->request('GET', '/session/list-trainees/'.$session->getId());
        static::$response = self::$client->getResponse();
    }

    /**
     * @Then /^I should receive "([^"]*)" animators$/
     */
    public function iShouldReceiveAnimators($arg1)
    {
        $content = \json_decode(static::$response->getContent(), true);
        $this->assertCount($arg1, $content);

        foreach ($content as $item) {
            $this->assertSame(ProviderParticipantSessionRole::ROLE_ANIMATOR, $item['role']);
        }
    }

    /**
     * @Then /^I should receive "([^"]*)" trainees$/
     */
    public function iShouldReceiveTrainees($arg1)
    {
        $content = \json_decode(static::$response->getContent(), true);
        $this->assertCount($arg1, $content);

        foreach ($content as $item) {
            $this->assertSame(ProviderParticipantSessionRole::ROLE_TRAINEE, $item['role']);
        }
    }

    /**
     * @Then /^I should receive no animators$/
     */
    public function iShouldReceiveNoAnimators()
    {
        $content = \json_decode(static::$response->getContent(), true);
        $this->assertEmpty($content);
    }

    /**
     * @Then /^I should receive no trainees$/
     */
    public function iShouldReceiveNoTrainees()
    {
        $content = \json_decode(static::$response->getContent(), true);
        $this->assertEmpty($content);
    }

    /**
     * @When /^I globally search "([^"]*)"$/
     */
    public function iGloballySearch($arg1)
    {
        self::$client->request('GET', '/session/global-search?q='.$arg1.'&filters-search');
        static::$response = self::$client->getResponse();
    }

    /**
     * @Then /^I should receive "([^"]*)" result$/
     */
    public function iShouldReceiveResult($arg1)
    {
        $content = \json_decode(static::$response->getContent(), true);
        $this->assertCount($arg1, $content['sessions']);
    }

    /**
     * @When /^I globally search "([^"]*)" in all sessions$/
     */
    public function iGloballySearchInAllSessions($arg1)
    {
        self::$client->request('GET', '/session/global-search?q='.$arg1.'&allow-outdated=true&filters-search=[]');
        static::$response = self::$client->getResponse();
    }

    /**
     * @When /^I fetch animators from an unknown session$/
     */
    public function iFetchAnimatorsFromAnUnknownSession()
    {
        self::$client->request('GET', '/session/list-animators/'.uniqid());
        static::$response = self::$client->getResponse();
    }

    /**
     * @When /^I globally search without query$/
     */
    public function iGloballySearchWithoutQuery()
    {
        self::$client->request('GET', '/session/global-search');
        static::$response = self::$client->getResponse();
    }
}
