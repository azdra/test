<?php

namespace App\Tests\Behat\Context;

use App\Entity\ProviderParticipantSessionRole;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class AnimatorContext extends AbstractContext
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private Security $security
    ) {
        parent::__construct();
    }

    /**
     * @When I try to connect to session :ref with the animator token
     */
    public function accessSessionWithAnimatorToken(string $ref): void
    {
        /** @var ProviderParticipantSessionRole $role */
        $role = $this->entityManager->getRepository(ProviderParticipantSessionRole::class)->createQueryBuilder('role')
            ->join('role.session', 'session')
            ->where('session.ref = :ref')
            ->andWhere("role.role = 'animator'")
            ->setParameter('ref', $ref)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        $this->assertNotNull($role);

        $route = '/session/'.$role->getSession()->getId().'?token='.$role->getProviderSessionAccessToken()->getToken();

        self::$client->request('GET', $route);
    }

    /**
     * @Given I am connected as animator to session :ref
     */
    public function authenticatAnimatorOfSession($ref): void
    {
        /** @var ProviderParticipantSessionRole $role */
        $role = $this->entityManager->getRepository(ProviderParticipantSessionRole::class)->createQueryBuilder('role')
            ->join('role.session', 'session')
            ->where('session.ref = :ref')
            ->andWhere("role.role = 'animator'")
            ->setParameter('ref', $ref)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        self::$client->loginUser($role->getParticipant()->getUser());
    }
}
