<?php

namespace App\Tests\Behat\Context;

class UserParticipantContext extends AbstractContext
{
    /**
     * @When /^I globally search "([^"]*)" in users participants$/
     */
    public function iGloballySearchInUsersParticipants($arg1)
    {
        self::$client->request('GET', '/user/global-search?context=participant&q='.$arg1);
        static::$response = self::$client->getResponse();
    }

    /**
     * @Then /^I should receive "([^"]*)" result in users participants$/
     */
    public function iShouldReceiveResultInUsersParticipants($arg1)
    {
        $content = \json_decode(static::$response->getContent(), true);
        $this->assertCount($arg1, $content);
    }

    /**
     * @When /^I globally search in users participants without query$/
     */
    public function iGloballySearchUsersParticipantsWithoutQuery()
    {
        self::$client->request('GET', '/user/global-search?context=participant');
        static::$response = self::$client->getResponse();
    }
}
