<?php

namespace App\Tests\Behat\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractContext extends WebTestCase implements Context
{
    protected static ?Response $response = null;

    protected static ?KernelBrowser $client = null;

    /**
     * @BeforeScenario
     */
    public function bootKernelForScenario(BeforeScenarioScope $scope): void
    {
        self::$booted = false;
        self::$client = static::createClient();
    }

    /**
     * @AfterScenario
     */
    public function cleanUpKernel(AfterScenarioScope $scope): void
    {
        self::ensureKernelShutdown();
        self::$kernel = null;
        self::$booted = false;
    }
}
