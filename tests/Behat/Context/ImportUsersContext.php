<?php

namespace App\Tests\Behat\Context;

class ImportUsersContext extends AbstractContext
{
    /**
     * @When I post an import file users
     */
    public function iPostAnImportFileUsers()
    {
        throw new PendingException();
    }

    /*
     * @Then /^I should receive the message result "([^"]*)"$/
     */
    public function iShouldReceiveResultMessage($arg1)
    {
        $content = \json_decode(static::$response->getContent(), true);
        $this->assertCount($arg1, $content);
    }
}
