<?php

namespace App\Tests\Behat\Context;

use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Security;

class UserContext extends AbstractContext
{
    public function __construct(private Security $security)
    {
        parent::__construct();
    }

    /**
     * @Given /^I am an anonymous user$/
     */
    public function IAmAnAnonymousUser(): void
    {
        $this->assertEmpty($this->security->getUser());
    }

    /**
     * @Given /^I am logged in as user "([^"]*)" with password "([^"]*)"$/
     */
    public function iAmLoggedInAsUserWithPassword($arg1, $arg2)
    {
        $content = [
            '_password' => $arg2,
            '_username' => $arg1,
        ];

        self::$client->request('GET', '/login', content: http_build_query($content));
        static::$response = self::$client->getResponse();
    }

    /**
     * @Then /^I should receive an error because of bad credentials$/
     */
    public function iShouldReceiveAException()
    {
        $this->expectException(BadCredentialsException::class);
    }

    /**
     * @When /^I globally search "([^"]*)" in users$/
     */
    public function iGloballySearchUsers($arg1)
    {
        self::$client->request('GET', '/user/global-search?context=participant&q='.$arg1);
        static::$response = self::$client->getResponse();
    }

    /**
     * @Then /^I should receive "([^"]*)" result in users$/
     */
    public function iShouldReceiveResultUsers($arg1)
    {
        $content = \json_decode(static::$response->getContent(), true);
        $this->assertCount($arg1, $content);
    }

    /**
     * @When /^I globally search in users without query$/
     */
    public function iGloballySearchUsersWithoutQuery()
    {
        self::$client->request('GET', '/user/global-search');
        static::$response = self::$client->getResponse();
    }
}
