<?php

namespace App\Tests\PathFunctional;

use App\Entity\ActivityLog;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ProviderSession;
use App\Repository\ActivityLogRepository;
use App\Repository\ClientRepository;
use App\Service\ActivityLogger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BillingByCreditTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;
    private ClientRepository $clientRepository;
    private ActivityLogRepository $activityLogRepository;
    private ActivityLogger $activityLogger;

    public function setUp(): void
    {
        self::bootKernel();
        $this->entityManager = $this->getContainer()->get(EntityManagerInterface::class);
        $this->clientRepository = $this->getContainer()->get(ClientRepository::class);
        $this->activityLogRepository = $this->getContainer()->get(ActivityLogRepository::class);
        $this->activityLogger = $this->getContainer()->get(ActivityLogger::class);
    }

    public function createSession(AbstractProviderConfigurationHolder $configurationHolder, bool $asTest): ProviderSession
    {
        $session = (new WebexRestMeeting($configurationHolder))
            ->setMeetingRestId(123)
            ->setMeetingRestNumber('123456')
            ->setLicence('test@test.test')
            ->setRef('test'.random_int(0, 100000, ))
            ->setDateStart(new \DateTime())
            ->setDuration(120)
            ->setStatus(ProviderSession::STATUS_SCHEDULED)
            ->setSessionTest($asTest);
        $this->entityManager->persist($session);
        $this->entityManager->flush();
        $this->activityLogger->initActivityLogContext($configurationHolder->getClient(), null, ActivityLog::ORIGIN_USER_INTERFACE);
        $this->activityLogger->flushActivityLogs();

        return $session;
    }

    public function testCreateSessionConsumeACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->setBillingMode(Client::BILLING_MODE_CREDIT)->setAllowedCredit(10);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getCurrentSessionCount();

        $this->createSession($configurationHolder, false);

        $expectedCreditUsed = $beforeCreditUsed + 1;
        $this->assertEquals($expectedCreditUsed, $client->getCurrentSessionCount());
    }

    public function testCreateTestSessionDoNotConsumeACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getCurrentSessionCount();

        $this->createSession($configurationHolder, true);

        $this->assertEquals($beforeCreditUsed, $client->getCurrentSessionCount());
    }

    public function testUpdateSessionToTestGiveBackACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->setBillingMode(Client::BILLING_MODE_CREDIT)->setAllowedCredit(10);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $session = $this->createSession($configurationHolder, false);
        $beforeCreditUsed = $client->getCurrentSessionCount();

        $session = $this->entityManager->getRepository(ProviderSession::class)->find($session->getId());
        $session->setSessionTest(true);
        $this->entityManager->flush();

        $expectedCreditUsed = $beforeCreditUsed - 1;
        $this->assertEquals($expectedCreditUsed, $client->getCurrentSessionCount());
    }

    public function testUpdateSessionToNotTestConsumeACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->setBillingMode(Client::BILLING_MODE_CREDIT)->setAllowedCredit(10);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $session = $this->createSession($configurationHolder, true);
        $beforeCreditUsed = $client->getCurrentSessionCount();

        $session = $this->entityManager->getRepository(ProviderSession::class)->find($session->getId());
        $session->setSessionTest(false);
        $this->entityManager->flush();

        $expectedCreditUsed = $beforeCreditUsed + 1;
        $this->assertEquals($expectedCreditUsed, $client->getCurrentSessionCount());
    }

    public function testDeleteSessionGiveBackACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->setBillingMode(Client::BILLING_MODE_CREDIT)->setAllowedCredit(10);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $session = $this->createSession($configurationHolder, false);
        $beforeCreditUsed = $client->getCurrentSessionCount();

        $this->entityManager->remove($session);
        $this->entityManager->flush();

        $expectedCreditUsed = $beforeCreditUsed - 1;
        $this->assertEquals($expectedCreditUsed, $client->getCurrentSessionCount());
    }

    public function testDeleteTestSessionDoNotGiveBackACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $session = $this->createSession($configurationHolder, true);
        $beforeCreditUsed = $client->getCurrentSessionCount();

        $this->entityManager->remove($session);
        $this->entityManager->flush();

        $this->assertEquals($beforeCreditUsed, $client->getCurrentSessionCount());
    }

    public function testCreateSessionShouldBePossibleIfCreditAvailable(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getCurrentSessionCount();

        $this->createSession($configurationHolder, false);

        $expectedCreditUsed = $beforeCreditUsed;
        $this->assertEquals($expectedCreditUsed, $client->getCurrentSessionCount());
    }

    public function testCreateSessionShouldNotBePossibleIfNoMoreCredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $configurationHolder = $client->getConfigurationHolders()->first();

        $this->assertEquals(0, $client->getAllowedCredit());
    }

    public function testCreateSessionNoLogAddedWhenEnoughCredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->setAllowedCredit(15);
        $configurationHolder = $client->getConfigurationHolders()->first();

        $this->createSession($configurationHolder, false);

        /** @var ActivityLog $lastLogs */
        $lastLogs = $this->activityLogRepository->findOneBy([], ['id' => 'desc']);
        $this->assertNotEquals('Your account only has %remaining_credit%. You can contact your Live Session advisor  to add more.', $lastLogs->getInfos()['message']['key']);
    }

    public function testCreateSessionAddLogForLowRemainingCredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->setAllowedCredit(5);
        $preferences = $client->getPreferences();
        $preferences[Client::NOTIFICATION_NO_MORE_CREDIT_OPTION_LIMIT] = 4;
        $client->setPreferences($preferences);
        $configurationHolder = $client->getConfigurationHolders()->first();

        $this->createSession($configurationHolder, false);

        /** @var ActivityLog $lastLogs */
        $lastLogs = $this->activityLogRepository->findOneBy([], ['id' => 'desc']);
        $this->assertEquals('Creating the  session  <a href="%route%">%sessionName%</a> (ref: %ref%)', $lastLogs->getInfos()['message']['key']);
    }

    public function testCreateSessionAddLogForNoMoreCredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->setAllowedCredit(1);
        $configurationHolder = $client->getConfigurationHolders()->first();

        $this->createSession($configurationHolder, false);

        /** @var ActivityLog $lastLogs */
        $lastLogs = $this->activityLogRepository->findOneBy([], ['id' => 'desc']);

        $this->assertEquals('Creating the  session  <a href="%route%">%sessionName%</a> (ref: %ref%)', $lastLogs->getInfos()['message']['key']);
    }

    public function testConsumeCreditOnUpdateShouldNotBePossibleIfNoMoreCredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $configurationHolder = $client->getConfigurationHolders()->first();

        $this->assertEquals(0, $client->getAllowedCredit());
    }

    public function testConsumeCreditOnUpdateNoLogAddedWhenEnoughCredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $client->setAllowedCredit(15);

        $this->createSession($configurationHolder, false);

        /** @var ActivityLog $lastLogs */
        $lastLogs = $this->activityLogRepository->findOneBy([], ['id' => 'desc']);
        $this->assertNotEquals('Your account only has %remaining_credit%. You can contact your Live Session advisor  to add more.', $lastLogs->getInfos()['message']['key']);
        $this->assertNotEquals('Your account has no credit left. Contact your Live Session advisor to add credit.', $lastLogs->getInfos()['message']['key']);
    }

    public function testConsumeCreditOnUpdateAddLogForNoMoreCredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $client->setAllowedCredit(5);
        $preferences = $client->getPreferences();
        $preferences[Client::NOTIFICATION_NO_MORE_CREDIT_OPTION_LIMIT] = 4;
        $client->setPreferences($preferences);

        $this->createSession($configurationHolder, false);

        /** @var ActivityLog $lastLogs */
        $lastLogs = $this->activityLogRepository->findOneBy([], ['id' => 'desc']);
        $this->assertEquals('Creating the  session  <a href="%route%">%sessionName%</a> (ref: %ref%)', $lastLogs->getInfos()['message']['key']);
    }

    public function testConsumeCreditOnUpdateAddLogForLowRemainingCredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $client->setAllowedCredit(1);

        $this->createSession($configurationHolder, false);

        /** @var ActivityLog $lastLogs */
        $lastLogs = $this->activityLogRepository->findOneBy([], ['id' => 'desc']);
        $this->assertEquals('Creating the  session  <a href="%route%">%sessionName%</a> (ref: %ref%)', $lastLogs->getInfos()['message']['key']);
    }
}
