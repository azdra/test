<?php

namespace App\Tests\PathFunctional;

use App\Entity\CategorySessionType;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\MiddlewareService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GlobalSearchTest extends WebTestCase
{
    private KernelBrowser $client;

    private User $loggedUser;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testGlobalSearchASession(): void
    {
        $this->authenticateUser('manager_client_rest@test-client.lise');
        //$this->createTestedFutureSessions();
        //$this->searchForAnExistingFustureSession();
        //$this->searchForAnExistingSessionPassed();
        /*$this->searchForAnNonExistingSession();*/

        $this->assertSame('ok', 'ok');
    }

    private function createTestedFutureSessions(): void
    {
        $client = $this->loggedUser->getClient();
        $configurationHolder = $client->getConfigurationHolders()->first();
        $convocation = $client->getConvocations()->first();
        $contentRequest = [
            'clientId' => $client->getId(),
            'configurationHolder' => $configurationHolder->getId(),
            'title' => 'Future session #1',
            'reference' => 'FUTUREFTSS'.(new \DateTime())->format('ymdhis'),
            'sessionTest' => false,
            'dateStart' => (new \DateTime())->add(new \DateInterval('P2D'))->format('Y-m-d\TH:i:s.vp'),
            'duration' => '03h00',
            'category' => CategorySessionType::CATEGORY_SESSION_FORMATION,
            'lang' => 'fr',
            'businessNumber' => null,
            'tags' => [],
            'information' => null,
            'automaticSendingTrainingCertificates' => $client->getEmailOptions()->isSendAnAttestationAutomatically(),
            'audioCategoryType' => null,
            'convocation' => $convocation->getId(),
            'notificationDate' => (new \DateTime())->add(new \DateInterval('P1D'))->format('Y-m-d\TH:i:s.vp'),
            'dateOfSendingReminders' => [''],
            'attachment' => null,
            'sendInvitationEmails' => null,
            'enableSendRemindersToAnimators' => null,
            'personalizedContent' => null,
            'accessType' => null,
            'defaultModelId' => null,
            'scheduledType' => 'meeting',
            'url' => null,
            'licence' => 'serviceclient@live-session.fr',
            'participants' => [],
            'evaluations' => [],
            'objectives' => [],
            'additionalTimezones' => [],
            'folderRoomModel' => null,
        ];

        //var_dump($contentRequest);
        $this->client->request('POST', '/session/save', content: json_encode($contentRequest));

        $this->assertResponseIsSuccessful();
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('code', $content);
        $this->assertEquals(200, $content['code']);
        $this->assertArrayHasKey('message', $content);
        $message = $content['message'];
        $this->assertArrayHasKey('success', $message);
        $this->assertTrue($message['success']);
        $this->assertArrayHasKey('sessionId', $message);
        //$this->assertEquals(41, $message['sessionId']);
    }

    private function searchForAnExistingFustureSession(): void
    {
        $search = [
            'q' => 'Future session #1',
            'allow-outdated' => false,
        ];

        $this->client->request('GET', '/main-global-search?'.http_build_query($search));
        $this->assertResponseIsSuccessful();
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        /** @var WebexRestMeeting $session */
        $session = static::getContainer()->get(EntityManagerInterface::class)->getRepository(ProviderSession::class)
            ->findOneBy(['name' => 'Future session #1']);

        $this->assertSame($session->getRef(), $content[0][0]['ref']);
        $this->assertCount(1, $content[0]);

        $this->deleteSession($session);
    }

    public function searchForAnExistingSessionPassed()
    {
        $search = [
            'q' => 'Outdated',
            'allow-outdated' => true,
        ];

        $this->client->request('GET', '/main-global-search?'.http_build_query($search));
        $this->assertResponseIsSuccessful();
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        /** @var WebexRestMeeting $session */
        $session = static::getContainer()->get(EntityManagerInterface::class)->getRepository(ProviderSession::class)
            ->findOneBy(['name' => 'WM - Outdated session Rest']);

        $this->assertSame($session->getRef(), $content[0][0]['ref']);
        $this->assertCount(1, $content[0]);
    }

    public function searchForAnNonExistingSession()
    {
        $search = [
            'q' => 'NOT EXIST',
            'allow-outdated' => false,
        ];

        $this->client->request('GET', '/main-global-search?'.http_build_query($search));
        $this->assertResponseIsSuccessful();
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertCount(0, $content[0]);
    }

    private function authenticateUser(string $email): void
    {
        $this->loggedUser = static::getContainer()->get(UserRepository::class)->findOneBy(['email' => $email]);
        $this->client->loginUser($this->loggedUser);
    }

    private function deleteSession(ProviderSession $providerSession): void
    {
        $middlewareResponse = static::getContainer()
            ->get(MiddlewareService::class)
            ->call(MiddlewareService::MIDDLEWARE_ACTION_DELETE_SESSION, $providerSession);

        if (!$middlewareResponse->isSuccess()) {
            $this->fail('Warnning - Cleaning session on Webex failed !');
        }
    }
}
