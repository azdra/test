<?php

namespace App\Tests\PathFunctional\Utils;

use App\Entity\User;
use App\Repository\UserRepository;
use Twig\Environment;
use Twig\Extension\CoreExtension;

trait AuthenticationTrait
{
    private User $loggedUser;

    private function authenticateUser(string $email): void
    {
        $this->loggedUser = static::getContainer()->get(UserRepository::class)->findOneBy(['email' => $email]);
        static::getContainer()->get(Environment::class)->getExtension(CoreExtension::class)->setTimezone('Europe/Paris');
        $this->client->loginUser($this->loggedUser);
    }
}
