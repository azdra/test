<?php

namespace App\Tests\PathFunctional\Utils;

use App\Entity\ActivityLog;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Repository\UserRepository;

trait SessionTrait
{
    public function createMeeting(AbstractProviderConfigurationHolder $configurationHolder, bool $asTest = false): ProviderSession
    {
        $random = random_int(0, 100000);
        $convocation = $configurationHolder->getClient()->getConvocations()->first();
        $webexLicence = static::getContainer()->getParameter('CISCO_WEBEX_CONNECTOR_HOST_WEBEX_ID');

        $session = match (get_class($configurationHolder)) {
            AdobeConnectConfigurationHolder::class => new AdobeConnectSCO($configurationHolder),
            WebexConfigurationHolder::class => (new WebexMeeting($configurationHolder))
                ->setLicence($webexLicence),
            default => throw new \Exception('Unknown configuration holder')
        };

        $session->setName("test$random")
                ->setRef("test$random")
                ->setConvocation($convocation)
                ->setDateStart(new \DateTime())
                ->setDuration(120)
                ->setConvocation($convocation)
                ->setSessionTest($asTest);

        $this->providerSessionService->createSession($session);

        $this->entityManager->persist($session);
        $this->entityManager->flush();

        $this->activityLogger->initActivityLogContext($configurationHolder->getClient(), null, ActivityLog::ORIGIN_USER_INTERFACE);
        $this->activityLogger->flushActivityLogs();

        return $session;
    }

    public function subscribeUserToSession(ProviderSession $session, $email): ProviderParticipantSessionRole
    {
        $user = static::getContainer()->get(UserRepository::class)->findOneBy(['email' => $email]);
        $userRole = $this->subscriberService->subscribe($session, $user, ProviderParticipantSessionRole::ROLE_TRAINEE);
        $this->entityManager->flush();

        return $userRole;
    }

    public function deleteSession(ProviderSession $session): void
    {
        /** @var ProviderSession $session */
        $session = $this->entityManager->merge($session);
        $this->providerSessionService->deleteSession($session);
        $this->entityManager->flush();
    }
}
