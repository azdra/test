<?php

namespace App\Tests\PathFunctional;

use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Repository\ClientRepository;
use App\Service\ConfigurationHolderFormHandler\AdobeConnectWebinarConfigurationJsonHandler;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdobeConnectWebinarConfigurationHolderTest extends WebTestCase
{
    public function testLoginWithSameIdentifierForDifferentClientIsSuccessWebinar(): void
    {
        $clientRepository = static::getContainer()->get(ClientRepository::class);
        $adobeConnectWebinarConfigurationHolderJsonHandler = static::getContainer()->get(AdobeConnectWebinarConfigurationJsonHandler::class);

        /** @var AdobeConnectWebinarConfigurationHolder $livesessionWebinarConfigurationHolder */
        $livesession2WebinarClient = $clientRepository->findOneBy(['name' => 'Live Session 2']);
        $livesession2WebinarConfigurationHolder = (new AdobeConnectWebinarConfigurationHolder());
        $livesession2WebinarConfigurationHolder->setClient($livesession2WebinarClient);

        $livesession2WebinarConfigurationHolder->setSubdomain('livesession2');
        $livesession2WebinarConfigurationHolder->setUsername('mlsm@live-session.fr');
        $livesession2WebinarConfigurationHolder->setPassword('Ls2015Enigm!');

        $adobeConnectWebinarConfigurationHolderJsonHandler->tryToLogin($livesession2WebinarConfigurationHolder);

        $this->assertSame('1066068249', $livesession2WebinarConfigurationHolder->getAdminGroupScoIds()[0]);
        $this->assertSame('1066068239', $livesession2WebinarConfigurationHolder->getAdminGroupScoIds()[1]);
        $this->assertSame('1066068248', $livesession2WebinarConfigurationHolder->getAdminGroupScoIds()[2]);
    }
}
