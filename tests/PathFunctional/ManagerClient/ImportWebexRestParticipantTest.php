<?php

namespace App\Tests\PathFunctional\ManagerClient;

use App\Entity\AsyncTask;
use App\Entity\CategorySessionType;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Repository\AsyncTaskRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\AsyncWorker\TaskInterface;
use App\Service\AsyncWorker\Tasks\ActionsFromImportUserTask;
use App\Service\MiddlewareService;
use App\Service\WebexRestMeetingService;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImportWebexRestParticipantTest extends WebTestCase
{
    public const WEBEX_SHEET = 0;
    public const SESSION_1_TITLE = 'Webex Rest Session TF import Participant';
    public const SESSION_1_REF = 'UNITTESTEN00000001REST';

    public const USER_1_FIRSTNAME = 'Bernadette';
    public const USER_1_LASTNAME = 'Courtemanche';
    public const USER_1_EMAIL = 'ba.courtemanche@testfunctionnal.fr';

    public const USER_2_FIRSTNAME = 'Aubrey';
    public const USER_2_LASTNAME = 'Jobin';
    public const USER_2_EMAIL = 'aubrey.jobin@testfunctionnal.fr';

    public const USER_3_FIRSTNAME = 'Pénélope';
    public const USER_3_LASTNAME = 'Croteau';
    public const USER_3_EMAIL = 'penelope.croteau@testfunctionnal.fr';

    public const USER_4_FIRSTNAME = 'Toussaint';
    public const USER_4_LASTNAME = 'Perrault';
    public const USER_4_EMAIL = 't.perrault@testfunctionnal.fr';

    public const USER_5_FIRSTNAME = 'Marc';
    public const USER_5_LASTNAME = 'Peanuts';
    public const USER_5_EMAIL = 'marc.peanuts@testfunctionnal.fr';

    public const IMPORT_FILE_NAME = 'path_functional_webex_rest_participants_import.xlsx';

    private string $importParticipantModelFile = 'public/archives/Model_Import_Participants_Standard.xlsx';

    private KernelBrowser $client;

    private WebexRestMeetingService $webexRestMeetingService;

    private User $loggedUser;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->webexRestMeetingService = static::getContainer()->get(webexRestMeetingService::class);
    }

    public function testCreateIncomingSession(): void
    {
        /*$this->authenticateUser('super_admin_rest@test-client.lise');
        $providerSession = $this->createIncommingTestSession();
        $this->createImportWebexRestParticipantsFileWithAvailableModele($providerSession);
        $this->importParticipantsFile();
        $this->assertAsyncTaskSuccessfullyCreated();
        $this->launchAsyncTaskWorker();
        $this->assertCreatedParticipantsInDatabase();
        $this->cleanUp();*/
        $this->assertSame('ok', 'ok');
    }

    private function createIncommingTestSession(): ProviderSession
    {
        $client = $this->loggedUser->getClient();
        $configurationHolder = $client->getConfigurationHolders()->first();
        $this->webexRestMeetingService->updateAccessToken($configurationHolder);
        $convocation = $client->getConvocations()->first();

        $contentRequest = [
            'clientId' => $client->getId(),
            'configurationHolder' => $configurationHolder->getId(),
            'title' => self::SESSION_1_TITLE,
            'reference' => self::SESSION_1_REF,
            'sessionTest' => false,
            'dateStart' => (new \DateTime())->add(new \DateInterval('P1D'))->format('Y-m-d\TH:i:s.vp'),
            'duration' => '03h00',
            'category' => CategorySessionType::CATEGORY_SESSION_FORMATION,
            'lang' => 'fr',
            'businessNumber' => null,
            'tags' => [],
            'information' => null,
            'emailReferrer' => 'emai.referrer@gmail.com',
            'automaticSendingTrainingCertificates' => $client->getEmailOptions()->isSendAnAttestationAutomatically(),
            'audioCategoryType' => null,
            'convocation' => $convocation->getId(),
            'notificationDate' => (new \DateTime())->sub(new \DateInterval('P1D'))->format('Y-m-d\TH:i:s.vp'),
            'dateOfSendingReminders' => [''],
            'attachment' => null,
            'sendInvitationEmails' => null,
            'enableSendRemindersToAnimators' => null,
            'personalizedContent' => null,
            'accessType' => null,
            'defaultModelId' => null,
            'url' => null,
            'licence' => 'serviceclient@live-session.fr',
            'participants' => [],
            'evaluations' => [],
            'objectives' => [],
        ];
        $this->client->request('POST', '/session/save', content: json_encode($contentRequest));

        $providerSession = static::getContainer()->get(ProviderSessionRepository::class)->findOneBy(['ref' => $contentRequest['reference']]);
        $this->assertNotNull($providerSession);

        return $providerSession;
    }

    private function createImportWebexRestParticipantsFileWithAvailableModele(ProviderSession $providerSession): void
    {
        $reference = $providerSession->getRef();

        $row = 4;
        $spreadsheet = IOFactory::load($this->importParticipantModelFile);

        $worksheet = $spreadsheet->getSheet(self::WEBEX_SHEET);

        // TEST #1 - First Line - Creation - Required Fields + no associated Session
        $worksheet
            ->setCellValue('A'.$row, self::USER_1_FIRSTNAME)
            ->setCellValue('B'.$row, self::USER_1_LASTNAME)
            ->setCellValue('C'.$row, self::USER_1_EMAIL)
            ->setCellValue('E'.$row, ActionsFromImportUserTask::ACTION_USER_ADD_OR_UPDATE);

        ++$row;

        // TEST #2 - Second Line - Creation - Required Fields + associated Session
        $worksheet
            ->setCellValue('A'.$row, self::USER_2_FIRSTNAME)
            ->setCellValue('B'.$row, self::USER_2_LASTNAME)
            ->setCellValue('C'.$row, self::USER_2_EMAIL)
            ->setCellValue('E'.$row, ActionsFromImportUserTask::ACTION_USER_ADD_OR_UPDATE)
            ->setCellValue('J'.$row, $reference)
            ->setCellValue('K'.$row, ActionsFromImportUserTask::ROLE_PARTICIPANT_TRAINEE);

        ++$row;

        // TEST #3 - Thrid Line - Create participant Trainee for modification + associated Session
        $worksheet
            ->setCellValue('A'.$row, self::USER_3_FIRSTNAME)
            ->setCellValue('B'.$row, self::USER_3_LASTNAME)
            ->setCellValue('C'.$row, self::USER_3_EMAIL)
            ->setCellValue('E'.$row, ActionsFromImportUserTask::ACTION_USER_ADD_OR_UPDATE)
            ->setCellValue('J'.$row, $reference)
            ->setCellValue('K'.$row, ActionsFromImportUserTask::ROLE_PARTICIPANT_TRAINEE);

        ++$row;

        // TEST #3 - Fourth Line - Modifiying a participant, Trainee To Animator
        $worksheet
            ->setCellValue('A'.$row, self::USER_3_FIRSTNAME)
            ->setCellValue('B'.$row, self::USER_3_LASTNAME)
            ->setCellValue('C'.$row, self::USER_3_EMAIL)
            ->setCellValue('E'.$row, ActionsFromImportUserTask::ACTION_USER_ADD_OR_UPDATE)
            ->setCellValue('J'.$row, $reference)
            ->setCellValue('K'.$row, ActionsFromImportUserTask::ROLE_PARTICIPANT_ANIMATOR);

        ++$row;

        // TEST #4 - Fifth Line - Create participant for remove from session
        $worksheet
            ->setCellValue('A'.$row, self::USER_4_FIRSTNAME)
            ->setCellValue('B'.$row, self::USER_4_LASTNAME)
            ->setCellValue('C'.$row, self::USER_4_EMAIL)
            ->setCellValue('E'.$row, ActionsFromImportUserTask::ACTION_USER_ADD_OR_UPDATE)
            ->setCellValue('J'.$row, $reference)
            ->setCellValue('K'.$row, ActionsFromImportUserTask::ROLE_PARTICIPANT_TRAINEE);

        ++$row;

        // TEST #4 - Sixth Line - Remove a participant from session
        $worksheet
            ->setCellValue('A'.$row, self::USER_4_FIRSTNAME)
            ->setCellValue('B'.$row, self::USER_4_LASTNAME)
            ->setCellValue('C'.$row, self::USER_4_EMAIL)
            ->setCellValue('E'.$row, ActionsFromImportUserTask::ACTION_USER_ADD_OR_UPDATE)
            ->setCellValue('J'.$row, $reference)
            ->setCellValue('K'.$row, ActionsFromImportUserTask::ROLE_PARTICIPANT_REMOVE);

        ++$row;

        // TEST #5 - Fifth Line - Create participant for delete - so remove all session associated
        $worksheet
            ->setCellValue('A'.$row, self::USER_5_FIRSTNAME)
            ->setCellValue('B'.$row, self::USER_5_LASTNAME)
            ->setCellValue('C'.$row, self::USER_5_EMAIL)
            ->setCellValue('E'.$row, ActionsFromImportUserTask::ACTION_USER_ADD_OR_UPDATE)
            ->setCellValue('J'.$row, $reference)
            ->setCellValue('K'.$row, ActionsFromImportUserTask::ROLE_PARTICIPANT_TRAINEE);

        ++$row;

        // TEST #5 - Sixth Line - Remove a participant from session
        $worksheet
            ->setCellValue('A'.$row, self::USER_5_FIRSTNAME)
            ->setCellValue('B'.$row, self::USER_5_LASTNAME)
            ->setCellValue('C'.$row, self::USER_5_EMAIL)
            ->setCellValue('E'.$row, ActionsFromImportUserTask::ACTION_USER_REMOVE);

        (new Xlsx($spreadsheet))->save('data/'.self::IMPORT_FILE_NAME);

        $this->assertTrue(file_exists(__DIR__.'/../../../data/'.self::IMPORT_FILE_NAME));
    }

    private function importParticipantsFile(): void
    {
        $client = $this->loggedUser->getClient();

        $uploadedFile = new UploadedFile(
            __DIR__.'/../../../data/'.self::IMPORT_FILE_NAME,
            self::IMPORT_FILE_NAME
        );

        $importRequestParameters = [
            'client' => $client->getId(),
        ];

        $this->client->request('POST', '/user/import-file-users', $importRequestParameters, [
            'file' => $uploadedFile,
        ]);

        $this->assertResponseIsSuccessful();
    }

    private function assertAsyncTaskSuccessfullyCreated(): void
    {
        /** @var AsyncTask $asyncTaskCreated */
        $asyncTaskCreated = static::getContainer()
            ->get(AsyncTaskRepository::class)
            ->findOneBy([], ['id' => 'desc']);

        $this->assertNotNull($asyncTaskCreated);

        $this->assertSame(1, $asyncTaskCreated->getClientOrigin()?->getId());
        $this->assertSame(2, $asyncTaskCreated->getUserOrigin()?->getId());
        $this->assertSame(ActionsFromImportUserTask::class, $asyncTaskCreated->getClassName());
        $this->assertSame(TaskInterface::STATUS_READY, $asyncTaskCreated->getStatus());
        $this->assertSame(['numberRows' => 8, 'configurationHolder' => 6], $asyncTaskCreated->getInfos());
        $this->assertNotEmpty($asyncTaskCreated->getData());
        $this->assertNull($asyncTaskCreated->getProcessingStartTime());
        $this->assertNull($asyncTaskCreated->getProcessingEndTime());
    }

    private function launchAsyncTaskWorker(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $command = $application->find('async:worker:watch');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
        ]);
        $commandTester->assertCommandIsSuccessful();
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('finished successfully.', $output);
    }

    private function assertCreatedParticipantsInDatabase(): void
    {
        static::getContainer()->get(EntityManagerInterface::class)->clear();

        /** @var WebexRestMeeting $session */
        $session = static::getContainer()->get(EntityManagerInterface::class)->getRepository(ProviderSession::class)
            ->findOneBy(['name' => self::SESSION_1_TITLE]);

        $this->assertNotNull($session);

        /** @var User $user_1 */
        $user_1 = static::getContainer()
            ->get(UserRepository::class)
            ->findOneBy(['email' => self::USER_1_EMAIL]);

        $this->assertNotNull($user_1);
        $this->assertCount(0, $user_1->getParticipants());

        /** @var User $user_2 */
        $user_2 = static::getContainer()
            ->get(UserRepository::class)
            ->findOneBy(['email' => self::USER_2_EMAIL]);

        $this->assertNotNull($user_2);
        $this->assertCount(1, $user_2->getParticipants());
        /** @var ProviderParticipant $participant_user_2 */
        $participant_user_2 = $user_2->getParticipants()->first();
        $this->assertSame($session, $participant_user_2->getSessionRoles()->first()->getSession());

        /** @var User $user_3 */
        $user_3 = static::getContainer()
            ->get(UserRepository::class)
            ->findOneBy(['email' => self::USER_3_EMAIL]);

        $this->assertNotNull($user_3);
        $this->assertCount(1, $user_3->getParticipants());
        /** @var ProviderParticipant $participant_user_3 */
        $participant_user_3 = $user_3->getParticipants()->first();
        $this->assertSame($session, $participant_user_3->getSessionRoles()->first()->getSession());
        $this->assertSame(ProviderParticipantSessionRole::ROLE_ANIMATOR, $participant_user_3->getSessionRoles()->first()->getRole());

        /** @var User $user_4 */
        $user_4 = static::getContainer()
            ->get(UserRepository::class)
            ->findOneBy(['email' => self::USER_4_EMAIL]);

        $this->assertNotNull($user_4);
        $this->assertEmpty($user_4->getParticipants()->first()->getSessionRoles());

        /** @var User $user_5 */
        $user_5 = static::getContainer()
            ->get(UserRepository::class)
            ->findOneBy(['email' => self::USER_5_EMAIL]);

        $this->assertNull($user_5);

        $this->deleteSession($session);
    }

    private function authenticateUser(string $email): void
    {
        $this->loggedUser = static::getContainer()->get(UserRepository::class)->findOneBy(['email' => $email]);
        $this->client->loginUser($this->loggedUser);
    }

    private function cleanUp(): void
    {
        unlink('data/'.self::IMPORT_FILE_NAME);
        $this->assertFalse(file_exists(__DIR__.'/../../../data/'.self::IMPORT_FILE_NAME));
    }

    private function deleteSession(ProviderSession $providerSession): void
    {
        $middlewareResponse = static::getContainer()
            ->get(MiddlewareService::class)
            ->call(MiddlewareService::MIDDLEWARE_ACTION_DELETE_SESSION, $providerSession);

        if (!$middlewareResponse->isSuccess()) {
            $this->fail('Warnning - Cleaning session on Webex Rest failed !');
        }
    }
}
