<?php

namespace App\Tests\PathFunctional\ManagerClient;

use App\Entity\AsyncTask;
use App\Entity\ProviderSession;
use App\Repository\AsyncTaskRepository;
use App\Repository\ProviderSessionRepository;
use App\Service\AsyncWorker\TaskInterface;
use App\Service\AsyncWorker\Tasks\ActionsFromImportSessionTask;
use App\Service\MiddlewareService;
use App\Service\ProviderSessionService;
use App\Tests\PathFunctional\Utils\AuthenticationTrait;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImportWebexSessionTest extends WebTestCase
{
    use AuthenticationTrait;

    public const WEBEX_SHEET = 1;
    public const SESSION_1_TITLE = 'My perfect Session #1';
    public const SESSION_2_TITLE = 'My Session #2';
    public const SESSION_2_REF = 'UNITTESTEN00000001';
    public const SESSION_3_TITLE = 'TO DELETE Session #3';
    public const SESSION_3_REF = 'UNITTESTEN00000222';
    public const SESSION_4_TITLE = 'TO CANCEL Session #4';
    public const SESSION_4_REF = 'UNITTESTEN000005444';

    public const IMPORT_FILE_NAME = 'path_functional_webex_session_import.xlsx';

    private string $importSessionModelFile = 'public/archives/Model_Import_Sessions_Standard.xlsx';

    private KernelBrowser $client;

    private ProviderSessionService $providerSessionService;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->providerSessionService = static::getContainer()->get(ProviderSessionService::class);
    }

    public function testCreateIncomingSession(): void
    {
        /*$this->authenticateUser('manager_client@test-client.lise');
        $this->createImportWebexSessionFileWithAvailableModele();
        $this->importSessionFile();
        $this->assertAsyncTaskSuccessfullyCreated();
        $this->launchAsyncTaskWorker();
        $this->assertCreatedSessionInDatabase();
        $this->cleanUp();*/

        $this->assertSame('ok', 'ok');
    }

    private function createImportWebexSessionFileWithAvailableModele(): void
    {
        $row = 5;
        $spreadsheet = IOFactory::load($this->importSessionModelFile);

        // First Line - Creation - Only required Fields
        $worksheet = $spreadsheet->getSheet(self::WEBEX_SHEET);

        $worksheet
            ->setCellValue('A'.$row, self::SESSION_1_TITLE)
            ->setCellValue('B'.$row, (new \DateTime())->modify('+1 day')->format('d/m/Y H:i'))
            ->setCellValue('C'.$row, '02:30')
            ->setCellValue('D'.$row, 'Convocation functional test')
            ->setCellValue('E'.$row, 'tests@live-session.fr');

        ++$row;

        // Third Line - Create the Session to update
        $worksheet
            ->setCellValue('A'.$row, self::SESSION_2_TITLE)
            ->setCellValue('B'.$row, (new \DateTime())->modify('+2 day')->format('d/m/Y H:i'))
            ->setCellValue('C'.$row, '01:00') // Change duration 60 to 120 min
            ->setCellValue('D'.$row, 'Convocation functional test')
            ->setCellValue('E'.$row, 'tests@live-session.fr')
            ->setCellValue('H'.$row, self::SESSION_2_REF)
            ->setCellValue('I'.$row, ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE);

        ++$row;

        // Second Line - UPDATE Session
        $worksheet
            ->setCellValue('A'.$row, self::SESSION_2_TITLE)
            ->setCellValue('B'.$row, (new \DateTime())->modify('+3 day')->format('d/m/Y H:i'))
            ->setCellValue('C'.$row, '02:00') // Change duration 60 to 120 min
            ->setCellValue('D'.$row, 'Convocation functional test')
            ->setCellValue('E'.$row, 'tests@live-session.fr')
            ->setCellValue('G'.$row, 'EN') // Change Language FR to EN
            ->setCellValue('H'.$row, self::SESSION_2_REF);

        ++$row;

        // Third Line - Create the Session to delete
        $worksheet
            ->setCellValue('A'.$row, self::SESSION_3_TITLE)
            ->setCellValue('B'.$row, (new \DateTime())->modify('+4 day')->format('d/m/Y H:i'))
            ->setCellValue('C'.$row, '01:00') // Change duration 60 to 120 min
            ->setCellValue('D'.$row, 'Convocation functional test')
            ->setCellValue('E'.$row, 'tests@live-session.fr')
            ->setCellValue('H'.$row, self::SESSION_3_REF)
            ->setCellValue('I'.$row, ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE);

        ++$row;

        // Fourth Line - Create the Session to delete
        $worksheet
            ->setCellValue('A'.$row, self::SESSION_3_TITLE)
            ->setCellValue('B'.$row, (new \DateTime())->modify('+4 day')->format('d/m/Y H:i'))
            ->setCellValue('C'.$row, '01:00') // Change duration 60 to 120 min
            ->setCellValue('D'.$row, 'Convocation functional test')
            ->setCellValue('E'.$row, 'tests@live-session.fr')
            ->setCellValue('H'.$row, self::SESSION_3_REF)
            ->setCellValue('I'.$row, ActionsFromImportSessionTask::ACTION_SESSION_DELETE);

        ++$row;

        // Fifth Line - Create the session to cancel
        $worksheet
            ->setCellValue('A'.$row, self::SESSION_4_TITLE)
            ->setCellValue('B'.$row, (new \DateTime())->modify('+5 day')->format('d/m/Y H:i'))
            ->setCellValue('C'.$row, '01:00') // Change duration 60 to 120 min
            ->setCellValue('D'.$row, 'Convocation functional test')
            ->setCellValue('E'.$row, 'tests@live-session.fr')
            ->setCellValue('H'.$row, self::SESSION_4_REF)
            ->setCellValue('I'.$row, ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE);

        ++$row;

        // Sixth Line - Cancel an existing session
        $worksheet
            ->setCellValue('A'.$row, self::SESSION_4_TITLE)
            ->setCellValue('B'.$row, (new \DateTime())->modify('+5 day')->format('d/m/Y H:i'))
            ->setCellValue('C'.$row, '01:00') // Change duration 60 to 120 min
            ->setCellValue('D'.$row, 'Convocation functional test')
            ->setCellValue('E'.$row, 'tests@live-session.fr')
            ->setCellValue('H'.$row, self::SESSION_4_REF)
            ->setCellValue('I'.$row, ActionsFromImportSessionTask::ACTION_SESSION_CANCEL);

        (new Xlsx($spreadsheet))->save('data/'.self::IMPORT_FILE_NAME);

        $this->assertTrue(file_exists(__DIR__.'/../../../data/'.self::IMPORT_FILE_NAME));
    }

    private function importSessionFile(): void
    {
        $client = $this->loggedUser->getClient();

        $uploadedFile = new UploadedFile(
            __DIR__.'/../../../data/'.self::IMPORT_FILE_NAME,
            self::IMPORT_FILE_NAME
        );

        $importRequestParameters = [
            'client' => $client->getId(),
            'configurationHolder' => $client->getConfigurationHolders()->first()->getId(),
        ];

        $this->client->request('POST', '/session/import-file-sessions', $importRequestParameters, [
            'file' => $uploadedFile,
        ]);

        $this->assertResponseIsSuccessful();
    }

    private function assertAsyncTaskSuccessfullyCreated(): void
    {
        /** @var AsyncTask $asyncTaskCreated */
        $asyncTaskCreated = static::getContainer()
            ->get(AsyncTaskRepository::class)
            ->findOneBy([], ['id' => 'desc']);

        $this->assertNotNull($asyncTaskCreated);
        $this->assertSame(2, $asyncTaskCreated->getClientOrigin()?->getId());
        $this->assertSame(3, $asyncTaskCreated->getUserOrigin()?->getId());
        $this->assertSame(ActionsFromImportSessionTask::class, $asyncTaskCreated->getClassName());
        $this->assertSame(TaskInterface::STATUS_READY, $asyncTaskCreated->getStatus());
        $this->assertSame(['numberRows' => 7, 'configurationHolder' => 1], $asyncTaskCreated->getInfos());
        $this->assertNotEmpty($asyncTaskCreated->getData());
        $this->assertNull($asyncTaskCreated->getProcessingStartTime());
        $this->assertNull($asyncTaskCreated->getProcessingEndTime());
    }

    private function launchAsyncTaskWorker(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $command = $application->find('async:worker:watch');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
        ]);

        $commandTester->assertCommandIsSuccessful();
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('finished successfully.', $output);
    }

    private function assertCreatedSessionInDatabase(): void
    {
        $sessionOne = static::getContainer()
            ->get(ProviderSessionRepository::class)
            ->findOneBy(['name' => self::SESSION_1_TITLE]);

        $this->assertNotNull($sessionOne);
        $this->deleteSession($sessionOne);

        /** @var ProviderSession $sessionUpdated */
        $sessionUpdated = static::getContainer()
            ->get(ProviderSessionRepository::class)
            ->findOneBy(['ref' => self::SESSION_2_REF]);

        $this->assertNotNull($sessionUpdated);
        $this->assertSame(120, $sessionUpdated->getDuration());
        $this->assertSame('en', $sessionUpdated->getLanguage());
        $this->deleteSession($sessionUpdated);

        /** @var ProviderSession $sessionDeleted */
        $sessionDeleted = static::getContainer()
            ->get(ProviderSessionRepository::class)
            ->findOneBy(['ref' => self::SESSION_3_REF]);

        $this->assertNull($sessionDeleted);

        /** @var ProviderSession $sessionCancel */
        $sessionCancel = static::getContainer()
            ->get(ProviderSessionRepository::class)
            ->findOneBy(['ref' => self::SESSION_4_REF]);

        $this->assertNotNull($sessionCancel);
        $this->assertSame(ProviderSession::STATUS_CANCELED, $sessionCancel->getStatus());
        $this->deleteSession($sessionCancel);
    }

    private function cleanUp(): void
    {
        unlink('data/'.self::IMPORT_FILE_NAME);
        $this->assertFalse(file_exists(__DIR__.'/../../../data/'.self::IMPORT_FILE_NAME));
    }

    private function deleteSession(ProviderSession $providerSession): void
    {
        $middlewareResponse = static::getContainer()
            ->get(MiddlewareService::class)
            ->call(MiddlewareService::MIDDLEWARE_ACTION_DELETE_SESSION, $providerSession);

        if (!$middlewareResponse->isSuccess()) {
            $this->fail('Warnning - Cleaning session on Webex failed !');
        }
    }
}
