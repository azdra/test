<?php

namespace App\Tests\PathFunctional\ManagerClient;

use App\Entity\CategorySessionType;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Repository\ProviderParticipantRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Tests\PathFunctional\Utils\AuthenticationTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ManageSessionAndParticipantTest extends WebTestCase
{
    use AuthenticationTrait;

    private KernelBrowser $client;

    private User $loggedUser;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testCreateIncomingSession(): void
    {
        /*$this->authenticateUser('manager_client@test-client.lise');
        $providerSession = $this->createSession();
        $user = $this->createUserParticipant();
        $this->accessSession($providerSession);
        $participant = $this->addParticipantToSession($providerSession, $user);
        $this->setParticipantWasHere($providerSession, $participant);
        $this->setParticipantWasNotHere($providerSession, $participant);
        $this->deleteSession($providerSession);*/
        $this->assertEquals(200, 200);
    }

    private function createSession(): ProviderSession
    {
        $client = $this->loggedUser->getClient();
        $configurationHolder = $client->getConfigurationHolders()->first();
        $convocation = $client->getConvocations()->first();

        $reference = 'FTSS'.(new \DateTime())->format('ymdhis');

        $contentRequest = [
            'clientId' => $client->getId(),
            'configurationHolder' => $configurationHolder->getId(),
            'title' => 'Functional test suite session',
            'reference' => $reference,
            'sessionTest' => false,
            'dateStart' => (new \DateTime())->add(new \DateInterval('P1D'))->format('Y-m-d\TH:i:s.vp'),
            'duration' => '03h00',
            'category' => CategorySessionType::CATEGORY_SESSION_FORMATION,
            'lang' => 'fr',
            'businessNumber' => null,
            'tags' => [],
            'information' => null,
            'automaticSendingTrainingCertificates' => $client->getEmailOptions()->isSendAnAttestationAutomatically(),
            'audioCategoryType' => null,
            'convocation' => $convocation->getId(),
            'notificationDate' => (new \DateTime())->sub(new \DateInterval('P1D'))->format('Y-m-d\TH:i:s.vp'),
            'dateOfSendingReminders' => [''],
            'attachment' => null,
            'sendInvitationEmails' => null,
            'enableSendRemindersToAnimators' => null,
            'personalizedContent' => null,
            'accessType' => null,
            'defaultModelId' => null,
            'url' => null,
            'licence' => self::getContainer()->getParameter('CISCO_WEBEX_CONNECTOR_HOST_WEBEX_ID'),
            'participants' => [],
            'evaluations' => [],
            'objectives' => [],
            'additionalTimezones' => [],
        ];
        $this->client->request('POST', '/session/save', content: json_encode($contentRequest));

        $this->assertResponseIsSuccessful();
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('code', $content);
        $this->assertEquals(200, $content['code']);
        $this->assertArrayHasKey('message', $content);
        $message = $content['message'];
        $this->assertArrayHasKey('success', $message);
        $this->assertTrue($message['success']);
        $this->assertArrayHasKey('sessionId', $message);

        $providerSession = static::getContainer()->get(ProviderSessionRepository::class)->findOneBy(['ref' => $contentRequest['reference']]);
        $this->assertNotNull($providerSession);

        return $providerSession;
    }

    private function createUserParticipant(): User
    {
        $contentRequest = [
            'user_participant_form' => [
                'email' => 'functional@testsuite.user',
                'firstName' => 'Functional',
                'lastName' => 'participant',
                'company' => '',
                'phone' => '',
                'preferredLang' => 'fr',
                'information' => '',
            ],
        ];
        $this->client->request('POST', '/user/create/participant', $contentRequest, server: ['CONTENT_TYPE' => 'application/x-www-form-urlencoded'], );

        $this->assertResponseRedirects();
        $this->client->followRedirect();
        $this->assertSelectorTextContains('.alert-success', "L'utilisateur a été ajouté avec succès");

        $userParticipant = static::getContainer()
            ->get(UserRepository::class)
            ->findOneBy([
                'email' => 'functional@testsuite.user',
            ]);

        $this->assertNotNull($userParticipant);
        $this->assertEquals($contentRequest['user_participant_form']['email'], $userParticipant->getEmail());

        return $userParticipant;
    }

    private function accessSession(ProviderSession $providerSession): void
    {
        $this->client->request('GET', '/session/'.$providerSession->getId());

        $this->assertResponseIsSuccessful();
    }

    private function addParticipantToSession(ProviderSession $providerSession, User $user): ProviderParticipantSessionRole
    {
        $contentRequest = [
            'clientId' => 1,
            'participants' => [
                [
                    'firstName' => 'Functional',
                    'lastName' => 'participant',
                    'email' => 'functional@testsuite.participant',
                    'searchableField' => 'participant Functional functional@testsuite.participant',
                    'user_id' => $user->getId(),
                    'role' => 'trainee',
                    'situationMixte' => '1',
                ],
            ],
        ];
        $this->client->request('POST', '/session/'.$providerSession->getId().'/update-participants', content: json_encode($contentRequest));

        $this->assertResponseIsSuccessful();
        $response = $this->client->getResponse();
        $content = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('code', $content);
        $this->assertEquals(200, $content['code']);
        $this->assertArrayHasKey('message', $content);
        $message = $content['message'];
        $this->assertArrayHasKey('success', $message);

        $client = $providerSession->getClient();
        $configurationHolder = $client->getConfigurationHolders()->first();

        $participant = static::getContainer()->get(ProviderParticipantRepository::class)->findOneBy([
            'user' => $user,
            'configurationHolder' => $configurationHolder,
        ]);

        $this->assertNotNull($participant);

        $sessionRole = static::getContainer()->get(ProviderParticipantSessionRoleRepository::class)->findOneBy([
            'participant' => $participant,
            'session' => $providerSession,
        ]);

        $this->assertNotNull($sessionRole);

        return $sessionRole;
    }

    private function setParticipantWasHere(ProviderSession $providerSession, ProviderParticipantSessionRole $participant): void
    {
        $this->client->request('GET', '/participant-session-role/update-manual-presence-status/'.$participant->getId().'/1');

        $this->assertResponseRedirects('/session/'.$providerSession->getId().'?tabName=');
        /** @var ProviderParticipantSessionRole $sessionRole */
        $sessionRole = static::getContainer()->get(ProviderParticipantSessionRoleRepository::class)->findOneBy([
            'participant' => $participant->getParticipant(),
            'session' => $providerSession,
        ]);

        $this->assertTrue($sessionRole->isManualPresenceStatus());
    }

    private function setParticipantWasNotHere(ProviderSession $providerSession, ProviderParticipantSessionRole $participant): void
    {
        $this->client->request('GET', '/participant-session-role/update-manual-presence-status/'.$participant->getId().'/0');

        $this->assertResponseRedirects('/session/'.$providerSession->getId().'?tabName=');
        /** @var ProviderParticipantSessionRole $sessionRole */
        $sessionRole = static::getContainer()->get(ProviderParticipantSessionRoleRepository::class)->findOneBy([
            'participant' => $participant->getParticipant(),
            'session' => $providerSession,
        ]);

        $this->assertFalse($sessionRole->isManualPresenceStatus());
    }

    private function deleteSession(ProviderSession $providerSession): void
    {
        $this->client->request('GET', '/session/delete/'.$providerSession->getId());

        $this->assertResponseRedirects('/session/');
        $this->assertNull(static::getContainer()->get(ProviderSessionRepository::class)->find($providerSession->getId()));
    }
}
