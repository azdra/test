<?php

namespace App\Tests\PathFunctional;

use App\Entity\ActivityLog;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexParticipant;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DashboardTest extends WebTestCase
{
    private User $loggedUser;
    private array $sessions = [];
    private ?ProviderParticipant $participant = null;

    private KernelBrowser $client;
    private EntityManagerInterface $entityManager;
    private ActivityLogger $activityLogger;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->entityManager = static::getContainer()->get(EntityManagerInterface::class);
        $this->activityLogger = static::getContainer()->get(ActivityLogger::class);
    }

    public function testDashboard(): void
    {
        /*$this->authenticateUser('manager_client@test-client.lise');
        $this->createSession(60, true, false);
        $this->createSession(120, true, false);
        $this->createSession(240, false, false);
        $this->createSession(240, true, true);
        $this->client->request('GET', '/dashboard/current-session');
        $this->cleanSessions();

        $this->assertResponseIsSuccessful();

        $this->assertSelectorTextContains('[data-testselector=global_count_session]', '4');
        $this->assertSelectorTextContains('[data-testselector=count_session]', '4');
        $this->assertSelectorTextContains('[data-testselector=tendancy_session]', '+100%');
        $this->assertSelectorTextContains('[data-testselector=count_participant]', '2');
        $this->assertSelectorTextContains('[data-testselector=tendancy_participant]', '+100%');
        $this->assertSelectorTextContains('[data-testselector=average_duration]', '2H00');
        $this->assertSelectorTextContains('[data-testselector=total_duration]', '8H00');*/
        $this->assertSame('ok', 'ok');
    }

    /*public function createSession(int $duration, bool $subscribeUser, bool $inThePast): ProviderSession
    {
        $start = new \DateTime();
        if ($inThePast) {
            $start->modify('-15 days');
        }
        $session = (new WebexMeeting($this->loggedUser->getClient()->getConfigurationHolders()->first()))
            ->setMeetingKey(123)
            ->setLicence('test@test.test')
            ->setRef('test'.random_int(0, 100000, ))
            ->setDateStart($start)
            ->setDuration($duration)
            ->setSessionTest(false);
        $this->entityManager->persist($session);

        if ($subscribeUser) {
            if (empty($this->participant)) {
                $this->participant = new WebexParticipant($session->getAbstractProviderConfigurationHolder());
                $this->participant->setUser($this->loggedUser);
                $this->entityManager->persist($this->participant);
            }

            $sessionRole = new ProviderParticipantSessionRole();
            $sessionRole->setParticipant($this->participant);
            $this->participant->addSessionRoles($sessionRole);
            $sessionRole->setSession($session);
            $session->addParticipantRole($sessionRole);
            $this->entityManager->persist($sessionRole);
        }

        $this->entityManager->flush();

        $this->activityLogger->initActivityLogContext($this->loggedUser->getClient(), null, ActivityLog::ORIGIN_USER_INTERFACE);
        $this->activityLogger->flushActivityLogs();

        $this->sessions[] = $session;

        return $session;
    }*/

    private function cleanSessions(): void
    {
        /** @var ProviderSession $session */
        foreach ($this->sessions as $session) {
            foreach ($session->getParticipantRoles() as $role) {
                $this->entityManager->remove($role);
            }
            $this->entityManager->remove($session);
        }
        $this->entityManager->remove($this->participant);
        $this->entityManager->flush();
    }

    private function authenticateUser(string $email): void
    {
        $this->loggedUser = static::getContainer()->get(UserRepository::class)->findOneBy(['email' => $email]);
        $this->client->loginUser($this->loggedUser);
    }
}
