<?php

namespace App\Tests\PathFunctional;

use App\Entity\ActivityLog;
use App\Entity\CategorySessionType;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Cisco\WebexRestParticipant;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Repository\ActivityLogRepository;
use App\Repository\ClientRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BillingByCreditParticipantsTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;
    private ClientRepository $clientRepository;
    private ActivityLogger $activityLogger;

    public function setUp(): void
    {
        self::bootKernel();
        $this->entityManager = $this->getContainer()->get(EntityManagerInterface::class);
        $this->clientRepository = $this->getContainer()->get(ClientRepository::class);
        $this->activityLogRepository = $this->getContainer()->get(ActivityLogRepository::class);
        $this->activityLogger = $this->getContainer()->get(ActivityLogger::class);
    }

    public function createSession(AbstractProviderConfigurationHolder $configurationHolder, bool $asTest): ProviderSession
    {
        $session = (new WebexRestMeeting($configurationHolder))
            ->setMeetingRestId(123)
            ->setMeetingRestNumber('123456')
            ->setLicence('test@test.test')
            ->setRef('test'.random_int(0, 100000, ))
            ->setDateStart(new \DateTime())
            ->setDuration(120)
            ->setCategory(CategorySessionType::CATEGORY_SESSION_FORMATION)
            ->setStatus(ProviderSession::STATUS_SCHEDULED)
            ->setSessionTest($asTest);
        $this->entityManager->persist($session);
        $this->entityManager->flush();
        $this->activityLogger->initActivityLogContext($configurationHolder->getClient(), null, ActivityLog::ORIGIN_USER_INTERFACE);
        $this->activityLogger->flushActivityLogs();

        return $session;
    }

    public function subscribeUserToSession(ProviderSession $session, $email, int $situationMixte, bool $isConvocationSend): ProviderParticipantSessionRole
    {
        $user = static::getContainer()->get(UserRepository::class)->findOneBy(['email' => $email]);

        $participant = new WebexRestParticipant($session->getAbstractProviderConfigurationHolder());
        $participant->setUser($user);

        $userRole = new ProviderParticipantSessionRole();
        $userRole->setSession($session)
                 ->setParticipant($participant)
                 ->setRole(ProviderParticipantSessionRole::ROLE_TRAINEE)
                 ->setSituationMixte($situationMixte);
        $session->getParticipantRoles()->add($userRole);
        if ($isConvocationSend) {
            $userRole->setLastConvocationSent(new \DateTimeImmutable());
        }

        $this->entityManager->persist($userRole);
        $this->entityManager->flush();

        return $userRole;
    }

    public function testCreateSessionNoConsumeACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getCurrentParticipantsCount();

        $this->createSession($configurationHolder, false);

        $expectedCreditUsed = $beforeCreditUsed;
        $this->assertEquals($expectedCreditUsed, $client->getCurrentParticipantsCount());
    }

    public function testCreateTestSessionDoNotConsumeACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getCurrentParticipantsCount();

        $session = $this->createSession($configurationHolder, true);
        $userRole = $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);

        $this->assertEquals($beforeCreditUsed, $client->getCurrentParticipantsCount());
    }

    public function testCreateTestSessionConsumeACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getCurrentParticipantsCount();

        $session = $this->createSession($configurationHolder, false);
        $userRole = $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);
        $expectedCreditUsed = $beforeCreditUsed + 1;

        $this->assertEquals($expectedCreditUsed, $client->getCurrentParticipantsCount());
    }

    public function testCreateTestSessionConsumeTwoCredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getCurrentParticipantsCount();

        $session = $this->createSession($configurationHolder, false);
        $userRole1 = $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);
        $userRole2 = $this->subscribeUserToSession($session, 'simple.participant-rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);
        $expectedCreditUsed = $beforeCreditUsed + 2;

        $this->assertEquals($expectedCreditUsed, $client->getCurrentParticipantsCount());
    }

    public function testUpdateSessionToTestDontGiveBackACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $session = $this->createSession($configurationHolder, false);
        $beforeCreditUsed = $client->getCurrentParticipantsCount();

        $session = $this->entityManager->getRepository(ProviderSession::class)->find($session->getId());
        $session->setSessionTest(true);
        $this->entityManager->flush();

        $expectedCreditUsed = $beforeCreditUsed;
        $this->assertEquals($expectedCreditUsed, $client->getCurrentParticipantsCount());
    }

    public function testUpdateSessionToTestGiveBackACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->addAllowedCreditParticipants(1)
            ->setBillingMode(Client::BILLING_MODE_PARTICIPANT);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getRemainingCreditParticipants();

        $session = $this->createSession($configurationHolder, false);
        $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);
        $afterCreditUsed = $client->getRemainingCreditParticipants();

        $session->setSessionTest(true);
        $this->entityManager->flush();

        $this->assertEquals(1, $beforeCreditUsed);
        $this->assertEquals(0, $afterCreditUsed);
        $this->assertEquals($beforeCreditUsed, $client->getRemainingCreditParticipants());
    }

    public function testUpdateSessionToTestGiveBackTwoCredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->addAllowedCreditParticipants(2)
            ->setBillingMode(Client::BILLING_MODE_PARTICIPANT);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getRemainingCreditParticipants();

        $session = $this->createSession($configurationHolder, false);
        $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);
        $this->subscribeUserToSession($session, 'simple.participant-rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);

        $afterCreditUsed = $client->getRemainingCreditParticipants();

        $session->setSessionTest(true);
        $this->entityManager->flush();

        $this->assertEquals(2, $beforeCreditUsed);
        $this->assertEquals(0, $afterCreditUsed);
        $this->assertEquals($beforeCreditUsed, $client->getRemainingCreditParticipants());
    }

    public function testCancelSessionGiveBackACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->addAllowedCreditParticipants(2)
            ->setBillingMode(Client::BILLING_MODE_PARTICIPANT);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getRemainingCreditParticipants();

        $session = $this->createSession($configurationHolder, false);
        $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);
        $this->subscribeUserToSession($session, 'simple.participant-rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);

        $afterCreditUsed = $client->getRemainingCreditParticipants();

        $session->setStatus(ProviderSession::STATUS_CANCELED);
        $this->entityManager->flush();

        $this->assertEquals(2, $beforeCreditUsed);
        $this->assertEquals(0, $afterCreditUsed);
        $this->assertEquals($beforeCreditUsed, $client->getRemainingCreditParticipants());
    }

    public function testCancelSessionTestNotGiveBackACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->addAllowedCreditParticipants(2)
            ->setBillingMode(Client::BILLING_MODE_PARTICIPANT);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getRemainingCreditParticipants();

        $session = $this->createSession($configurationHolder, true);
        $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);
        $this->subscribeUserToSession($session, 'simple.participant-rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);

        $afterCreditUsed = $client->getRemainingCreditParticipants();

        $session->setStatus(ProviderSession::STATUS_CANCELED);
        $this->entityManager->flush();

        $this->assertEquals(2, $beforeCreditUsed);
        $this->assertEquals($beforeCreditUsed, $afterCreditUsed);
        $this->assertEquals($beforeCreditUsed, $client->getRemainingCreditParticipants());
    }

    public function testUnCancelSessionTestNotGiveBackACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->addAllowedCreditParticipants(2)
            ->setBillingMode(Client::BILLING_MODE_PARTICIPANT);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getRemainingCreditParticipants();

        $session = $this->createSession($configurationHolder, true)
            ->setStatus(ProviderSession::STATUS_CANCELED);
        $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);
        $this->subscribeUserToSession($session, 'simple.participant-rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);

        $afterCreditUsed = $client->getRemainingCreditParticipants();

        $session->setStatus(ProviderSession::STATUS_SCHEDULED);
        $this->entityManager->flush();

        $this->assertEquals(2, $beforeCreditUsed);
        $this->assertEquals($beforeCreditUsed, $afterCreditUsed);
        $this->assertEquals($beforeCreditUsed, $client->getRemainingCreditParticipants());
    }

    public function testUnCancelSessionTestGiveBackACredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->addAllowedCreditParticipants(2)
            ->setBillingMode(Client::BILLING_MODE_PARTICIPANT);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getRemainingCreditParticipants();

        $session = ($this->createSession($configurationHolder, false))
            ->setStatus(ProviderSession::STATUS_CANCELED);

        $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);
        $this->subscribeUserToSession($session, 'simple.participant-rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);

        $afterCreditUsed = $client->getRemainingCreditParticipants();

        $session->setStatus(ProviderSession::STATUS_SCHEDULED);
        $this->entityManager->flush();

        $this->assertEquals(2, $beforeCreditUsed);
        $this->assertEquals($beforeCreditUsed, $afterCreditUsed);
        $this->assertEquals(0, $client->getRemainingCreditParticipants());
    }

    public function testUpdateSessionToReunionGiveBackTwoCredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->addAllowedCreditParticipants(2)
            ->setBillingMode(Client::BILLING_MODE_PARTICIPANT);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getRemainingCreditParticipants();

        $session = $this->createSession($configurationHolder, false);
        $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);
        $this->subscribeUserToSession($session, 'simple.participant-rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);

        $afterCreditUsed = $client->getRemainingCreditParticipants();

        $session->setCategory(CategorySessionType::CATEGORY_SESSION_REUNION);
        $this->entityManager->flush();

        $this->assertEquals(2, $beforeCreditUsed);
        $this->assertEquals(0, $afterCreditUsed);
        $this->assertEquals($beforeCreditUsed, $client->getRemainingCreditParticipants());
    }

    public function testDontCountSessionReunionTwoCredit(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $client->addAllowedCreditParticipants(2)
            ->setBillingMode(Client::BILLING_MODE_PARTICIPANT);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $beforeCreditUsed = $client->getRemainingCreditParticipants();

        $session = ($this->createSession($configurationHolder, false))
            ->setCategory(CategorySessionType::CATEGORY_SESSION_REUNION);
        $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);
        $this->subscribeUserToSession($session, 'simple.participant-rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);
        $afterCreditUsed = $client->getRemainingCreditParticipants();
        $this->entityManager->flush();

        $this->assertEquals(2, $beforeCreditUsed);
        $this->assertEquals($beforeCreditUsed, $afterCreditUsed);
        $this->assertEquals($beforeCreditUsed, $client->getRemainingCreditParticipants());
    }
}
