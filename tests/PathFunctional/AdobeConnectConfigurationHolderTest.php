<?php

namespace App\Tests\PathFunctional;

use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Repository\AdobeConnectConfigurationHolderRepository;
use App\Repository\ClientRepository;
use App\Service\ConfigurationHolderFormHandler\AdobeConnectConfigurationJsonHandler;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdobeConnectConfigurationHolderTest extends WebTestCase
{
    public function testLoginWithSameIdentifierForDifferentClientIsSuccess(): void
    {
        $clientRepository = static::getContainer()->get(ClientRepository::class);
        $adobeConnectConfigurationHolderRepository = static::getContainer()->get(AdobeConnectConfigurationHolderRepository::class);
        $adobeConnectConfigurationHolderJsonHandler = static::getContainer()->get(AdobeConnectConfigurationJsonHandler::class);

        /** @var AdobeConnectConfigurationHolder $livesessionConfigurationHolder */
        $livesessionConfigurationHolder = $adobeConnectConfigurationHolderRepository->findOneBy(['subdomain' => 'livesession']);
        $livesessionConfigurationHolder->setUsername('mlsm@live-session.fr');

        $livesession2Client = $clientRepository->findOneBy(['name' => 'Live Session 2']);
        $livesession2ConfigurationHolder = (new AdobeConnectConfigurationHolder())
            ->setClient($livesession2Client)
            ->setSubdomain('livesession2')
            ->setUsername($livesessionConfigurationHolder->getUsername())
            ->setPassword($livesessionConfigurationHolder->getPassword());

        $adobeConnectConfigurationHolderJsonHandler->tryToLogin($livesessionConfigurationHolder);
        $adobeConnectConfigurationHolderJsonHandler->tryToLogin($livesession2ConfigurationHolder);

        $this->assertSame(1066068249, $livesession2ConfigurationHolder->getHostGroupScoId());
    }
}
