<?php

namespace App\Tests\PathFunctional\Participant;

use App\Entity\CategorySessionType;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Cisco\WebexRestParticipant;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Repository\ClientRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SendConvocationTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;
    private ClientRepository $clientRepository;

    public function setUp(): void
    {
        $this->entityManager = $this->getContainer()->get(EntityManagerInterface::class);
        $this->activityLogger = $this->getContainer()->get(ActivityLogger::class);
        $this->clientRepository = $this->getContainer()->get(ClientRepository::class);
    }

    public function createSession(AbstractProviderConfigurationHolder $configurationHolder, CategorySessionType $category): ProviderSession
    {
        $session = (new WebexRestMeeting($configurationHolder))
            ->setMeetingRestId(123)
            ->setMeetingRestNumber('123456')
            ->setLicence('test@test.test')
            ->setRef('test'.random_int(0, 100000, ))
            ->setDateStart(new \DateTime())
            ->setDuration(120)
            ->setCategory($category)
            ->setConvocation($configurationHolder->getClient()->getConvocations()->first());

        $this->entityManager->persist($session);
        $this->entityManager->flush();

        return $session;
    }

    public function subscribeUserToSession(ProviderSession $session, $email, int $situationMixte, bool $isConvocationSend): ProviderParticipantSessionRole
    {
        $user = static::getContainer()->get(UserRepository::class)->findOneBy(['email' => $email]);

        $participant = new WebexRestParticipant($session->getAbstractProviderConfigurationHolder());
        $participant->setUser($user);

        $userRole = new ProviderParticipantSessionRole();
        $userRole->setSession($session)
            ->setParticipant($participant)
            ->setRole(ProviderParticipantSessionRole::ROLE_TRAINEE)
            ->setSituationMixte($situationMixte);

        if ($isConvocationSend) {
            $userRole->setLastConvocationSent(new \DateTimeImmutable());
        }

        $this->entityManager->persist($userRole);
        $this->entityManager->flush();

        return $userRole;
    }

    public function testConvocationMustBeSendIfSituationChangedAndConvocationAlreadySent(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $session = $this->createSession($client->getConfigurationHolders()->first(), CategorySessionType::CATEGORY_SESSION_MIXTE);
        $userRole = $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);

        $userRole->setSituationMixte(ProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL);
        $this->entityManager->flush();

        $this->assertEmailCount(1);
    }

    public function testConvocationMustNotBeSendIfConvocationNotSent(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $session = $this->createSession($client->getConfigurationHolders()->first(), CategorySessionType::CATEGORY_SESSION_MIXTE);
        $userRole = $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, false);

        $userRole->setSituationMixte(ProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL);
        $this->entityManager->flush();

        $this->assertEmailCount(0);
    }

    public function testConvocationMustNotBeSendIfSituationNotChanged(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $session = $this->createSession($client->getConfigurationHolders()->first(), CategorySessionType::CATEGORY_SESSION_MIXTE);
        $userRole = $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);

        $userRole->setLastReminderSent(new \DateTimeImmutable());
        $this->entityManager->flush();

        $this->assertEmailCount(0);
    }

    public function testConvocationMustNotBeSentForNotAMixedSession(): void
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Test client Rest']);
        $session = $this->createSession($client->getConfigurationHolders()->first(), CategorySessionType::CATEGORY_SESSION_FORMATION);
        $userRole = $this->subscribeUserToSession($session, 'manager_client_rest@test-client.lise', ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, true);

        $userRole->setSituationMixte(ProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL);
        $this->entityManager->flush();

        $this->assertEmailCount(0);
    }
}
