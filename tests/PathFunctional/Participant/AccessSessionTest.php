<?php

namespace App\Tests\PathFunctional\Participant;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Client\Client;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Repository\ClientRepository;
use App\Service\ActivityLogger;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\ProviderSessionService;
use App\Tests\PathFunctional\Utils\AuthenticationTrait;
use App\Tests\PathFunctional\Utils\SessionTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AccessSessionTest extends WebTestCase
{
    use AuthenticationTrait;
    use SessionTrait;

    private EntityManagerInterface $entityManager;
    private ClientRepository $clientRepository;
    private ActivityLogger $activityLogger;
    private ParticipantSessionSubscriber $subscriberService;
    private ProviderSessionService $providerSessionService;
    private ProviderSession $sessionTested;
    private ProviderParticipantSessionRole $userRoleTested;
    private AdobeConnectConnector $adobeConnectConnector;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->entityManager = $this->getContainer()->get(EntityManagerInterface::class);
        $this->clientRepository = $this->getContainer()->get(ClientRepository::class);
        $this->activityLogger = $this->getContainer()->get(ActivityLogger::class);
        $this->subscriberService = $this->getContainer()->get(ParticipantSessionSubscriber::class);
        $this->providerSessionService = $this->getContainer()->get(ProviderSessionService::class);
        $this->adobeConnectConnector = $this->getContainer()->get(AdobeConnectConnector::class);
    }

    /*public function testAccessWebexSession(): void
    {
        $this->createSessionAndParticipant('manager_client_rest@test-client.lise', 'Test client Rest', 'simple.participant-rest@test-client.lise');
        $this->studentClickOnConvocationLink();
        $this->cleanUp();
    }*/

    public function testAccessAdobeConnectSessionWithWrongPassword(): void
    {
        $this->createSessionAndParticipant('manager_ac@test-client.lise', 'Live Session 2', 'simple.participant-ac@test-client.lise');

        // Change stored password on database
        /** @var AdobeConnectPrincipal $participant */
        $participant = $this->userRoleTested->getParticipant();
        $originalParticipantPassword = $participant->getPassword();
        $this->changeAdobePrincipalPassword($participant, $originalParticipantPassword);
        $participant->setPassword(rand(100000000, 999999999));

        // Try to connect to session
        $this->client->request('GET', '/session/access/'.$this->userRoleTested->getProviderSessionAccessToken()->getToken());
        //$this->assertNull($this->client->getResponse()->headers->get('Location'));
        $this->assertResponseRedirects();

        // Set original the password and try to connect again
        $this->changeAdobePrincipalPassword($participant, $originalParticipantPassword);
        $this->client->request('GET', '/session/access/'.$this->userRoleTested->getProviderSessionAccessToken()->getToken());
        $this->assertResponseRedirects();
        $this->assertStringContainsString($this->sessionTested->getAbstractProviderConfigurationHolder()->getDomain(), $this->client->getResponse()->headers->get('Location'));

        $this->cleanUp();
    }

    private function createSessionAndParticipant(string $emailUserToAuthenticate, string $clientName, string $participantEmail)
    {
        $this->authenticateUser($emailUserToAuthenticate);
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => $clientName]);
        $configurationHolder = $client->getConfigurationHolders()->first();
        $configurationHolder->setClient($client);
        $this->sessionTested = $this->createMeeting($configurationHolder);
        $this->userRoleTested = $this->subscribeUserToSession($this->sessionTested, $participantEmail);
    }

    private function studentClickOnConvocationLink()
    {
        $this->client->request('GET', '/session/access/'.$this->userRoleTested->getProviderSessionAccessToken()->getToken());

        $this->assertResponseRedirects();
        $this->assertStringContainsString('livesession38.webex.com', $this->client->getResponse()->headers->get('Location'));
    }

    private function changeAdobePrincipalPassword(AdobeConnectPrincipal $adobeConnectPrincipal, string $password)
    {
        $adobeConnectPrincipal->setPassword($password);
        $response = $this->adobeConnectConnector->call($adobeConnectPrincipal->getConfigurationHolder(), AdobeConnectConnector::ACTION_UPDATE_PRINCIPAL_PASSWORD, $adobeConnectPrincipal);

        if (!$response->isSuccess()) {
            throw $response->getThrown();
        }
    }

    private function cleanUp()
    {
        $this->deleteSession($this->sessionTested);
        $this->entityManager->flush();
    }
}
