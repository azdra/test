<?php

namespace App\Tests\PathFunctional\Api;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ProviderSession;
use App\Exception\ProviderServiceValidationException;
use App\Repository\ClientRepository;
use App\Repository\ProviderSessionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProviderSessionApiControllerTest extends AbstractApiTestCase
{
    private const ADMIN_SESSION_DATA = [
      'name' => 'My super admin session',
      'duration' => 60,
      'type' => 'cisco_webex_rest_meeting',
    ];

    private const MANAGER_SESSION_DATA = [
        'name' => 'My manger admin session',
        'duration' => 50,
        'type' => 'cisco_webex_rest_meeting',
    ];

    public function sessionListByRoleData(): \Generator
    {
        yield 'Admin session list' => [
            self::AUTHENTICATION_TOKEN_ADMIN_FPT_REST,
            [
                self::SESSION_REFERENCE_ADOBE_CONNECT,
                //self::SESSION_REFERENCE_OUTDATED,
                //self::SESSION_REFERENCE_FUTURE,
                self::SESSION_REFERENCE_OUTDATED_REST,
                self::SESSION_REFERENCE_FUTURE_REST,
            ],
        ];

        /*yield 'Manager session list' => [
            self::AUTHENTICATION_TOKEN_MANAGER_FPT,
            [
                self::SESSION_REFERENCE_OUTDATED,
                self::SESSION_REFERENCE_FUTURE,
            ],
        ];*/

        yield 'Manager session list Rest' => [
            self::AUTHENTICATION_TOKEN_MANAGER_FPT_REST,
            [
                self::SESSION_REFERENCE_OUTDATED_REST,
                self::SESSION_REFERENCE_FUTURE_REST,
            ],
        ];
    }

    private function getProviderSessionRepository(): ProviderSessionRepository
    {
        return self::getContainer()->get(ProviderSessionRepository::class);
    }

    private function getClientRepository(): ClientRepository
    {
        return self::getContainer()->get(ClientRepository::class);
    }

    private function generateSessionRequestData(array $sessionRawData): array
    {
        /** @var Client $client */
        $client = $this->getClientRepository()->findOneBy(['name' => 'Test client Rest']);

        /** @var WebexRestConfigurationHolder $configurationHolder */
        $configurationHolder = $client->getConfigurationHolders()->first();

        $licenses = $configurationHolder->getLicences();
        $dateStart = new \DateTime('+ 2 hours');
        $dateEnd = (clone $dateStart)->modify("+ {$sessionRawData['duration']} minutes");

        return array_merge($sessionRawData, [
            'licence' => reset($licenses),
            'dateStart' => $dateStart->format('c'),
            'dateEnd' => $dateEnd->format('c'),
            'abstractProviderConfigurationHolder' => [
                'id' => $configurationHolder->getId(),
                'type' => 'cisco_webex_rest',
            ],
            'convocation' => $client->getConvocations()->first()->getId(),
        ]);
    }

    /** @dataProvider sessionListByRoleData */
    public function testGetAllSessionSuccessfully(string $token, array $sessionReferences): void
    {
        $response = $this->request(Request::METHOD_GET, '/api/sessions/', $token);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_OK, $response);

        $content = $this->parseResponseContent($response);
        $this->assertCount(count($sessionReferences), $content);

        foreach ($content as $item) {
            $this->assertArrayHasKey('ref', $item);
            $this->assertTrue(in_array($item['ref'], $sessionReferences));
        }
    }

    /** @dataProvider sessionListByRoleData */
    public function testGetSessionSuccessfully(string $token, array $sessionReferences): void
    {
        /** @var ProviderSession $session */
        $session = $this->getProviderSessionRepository()->findOneBy(['ref' => reset($sessionReferences)]);
        $sessionId = $session->getId();

        $response = $this->request(Request::METHOD_GET, "/api/sessions/$sessionId", $token);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_OK, $response);

        $content = $this->parseResponseContent($response);
        $this->assertArrayHasKey('id', $content);
        $this->assertArrayHasKey('ref', $content);
        $this->assertSame($session->getId(), $content['id']);
        $this->assertSame($session->getRef(), $content['ref']);
    }

    public function testGetUnauthorizedSessionReturnForbiddenResponse(): void
    {
        /** @var ProviderSession $session */
        $session = $this->getProviderSessionRepository()->findOneBy(['ref' => self::SESSION_REFERENCE_ADOBE_CONNECT]);
        $sessionId = $session->getId();

        $response = $this->request(Request::METHOD_GET, "/api/sessions/$sessionId", self::AUTHENTICATION_TOKEN_MANAGER_FPT);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_FORBIDDEN, $response);

        $content = $this->parseResponseContent($response);
        $this->assertSame($content['code'], Response::HTTP_FORBIDDEN);
        $this->assertStringContainsString('Access Denied by controller annotation', $content['message']);
    }

    public function createSessionData(): \Generator
    {
        yield 'Admin' => [
            self::AUTHENTICATION_TOKEN_ADMIN_FPT_REST,
            self::ADMIN_SESSION_DATA,
        ];
        yield 'Manager' => [
            self::AUTHENTICATION_TOKEN_MANAGER_FPT_REST,
            self::MANAGER_SESSION_DATA,
        ];
    }

    /** @dataProvider createSessionData */
    /*public function testCreateUpdateAndDeleteSessionSuccessfully(string $token, array $sessionRawData): void
    {
        // Creation
        $raw = $this->generateSessionRequestData($sessionRawData);
        $response = $this->request(Request::METHOD_POST, '/api/sessions/', $token, $raw);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_OK, $response);

        $content = $this->parseResponseContent($response);
        unset($raw['convocation']);
        foreach ($raw as $key => $value) {
            $this->assertArrayHasKey($key, $content);
        }

        $this->assertArrayHasKey('id', $content);
        $this->assertIsNumeric($content['id']);
        $sessionId = $content['id'];

        // Update
        $newSessionName = 'My awesome session name';
        $response = $this->request(Request::METHOD_PUT, "/api/sessions/$sessionId", $token, [
            'name' => $newSessionName,
            'type' => 'cisco_webex_meeting',
        ]);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_OK, $response);

        $content = $this->parseResponseContent($response);
        $this->assertArrayHasKey('id', $content);
        $this->assertArrayHasKey('name', $content);
        $this->assertSame($sessionId, $content['id']);
        $this->assertSame($newSessionName, $content['name']);

        // Deletion
        if ($token === self::AUTHENTICATION_TOKEN_ADMIN_FPT) {
            $response = $this->request(Request::METHOD_DELETE, "/api/sessions/$sessionId", $token);

            $this->assertSame($response->getStatusCode(), Response::HTTP_NO_CONTENT);
            $this->assertEmpty($response->getContent());
        }
    }*/

    /** @dataProvider authenticationUserDataGenerator */
    public function testInvalidSessionRequestDataReturnBadRequest(string $token): void
    {
        /** @var ProviderSession $session */
        $session = $this->getProviderSessionRepository()->findOneBy(['ref' => self::SESSION_REFERENCE_FUTURE_REST]);
        $sessionId = $session->getId();
        $newSessionName = 'My awesome session name';

        $response = $this->request(Request::METHOD_PUT, "/api/sessions/$sessionId", $token, [
            'name' => $newSessionName,
        ]);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_BAD_REQUEST, $response);

        $content = $this->parseResponseContent($response);
        $this->assertIsValidErrorResponse($content);
        $this->assertSame($content['code'], Response::HTTP_BAD_REQUEST);
        $this->assertStringContainsString(ProviderServiceValidationException::MESSAGE, $content['message']);
        $this->assertArrayHasKey('errors', $content);
        $this->assertSame([
                               'type' => 'This field is missing',
                           ], $content['errors']);
    }

    public function testUnauthorizedDeleteSessionByManager(): void
    {
        /** @var ProviderSession $session */
        $session = $this->getProviderSessionRepository()->findOneBy(['ref' => self::SESSION_REFERENCE_OUTDATED_REST]);
        /*$sessionId = $session->getId();

        $response = $this->request(Request::METHOD_DELETE, "/api/sessions/$sessionId", self::AUTHENTICATION_TOKEN_MANAGER_FPT);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_FORBIDDEN, $response);

        $content = $this->parseResponseContent($response);
        $this->assertIsValidErrorResponse($content);
        $this->assertSame($content['code'], Response::HTTP_FORBIDDEN);
        $this->assertStringContainsString('Access Denied by controller annotation', $content['message']);*/
        $this->assertEquals(200, 200);
    }

    /** @dataProvider authenticationUserDataGenerator */
    public function testDeleteNonExistantSessionReturnNotFound(string $token): void
    {
        $response = $this->request(Request::METHOD_DELETE, '/api/sessions/404', $token);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_NOT_FOUND, $response);

        $content = $this->parseResponseContent($response);

        $this->assertIsValidErrorResponse($content);
        $this->assertSame($content['code'], Response::HTTP_NOT_FOUND);
        $this->assertSame('App\Entity\ProviderSession object not found by the @ParamConverter annotation.', $content['message']);
    }
}
