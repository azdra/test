<?php

namespace App\Tests\PathFunctional\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultApiControllerTest extends AbstractApiTestCase
{
    public function testUnloggedUserCantMakeCalls(): void
    {
        $response = $this->request(Request::METHOD_GET, '/api/ping');
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_UNAUTHORIZED, $response);

        $content = $this->parseResponseContent($response);

        $this->assertIsValidErrorResponse($content);
        $this->assertSame('Full authentication is required to access this resource.', $content['message']);
    }

    public function testUserUnauthorizedToUseApi(): void
    {
        $response = $this->request(Request::METHOD_GET, '/api/ping', self::AUTHENTICATION_TOKEN_UNAUTHORISED);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_FORBIDDEN, $response);

        $content = $this->parseResponseContent($response);

        $this->assertIsValidErrorResponse($content);
        $this->assertSame('Access Denied.', $content['message']);
    }

    /** @dataProvider authenticationUserDataGenerator */
    public function testCallPingReturnPongSuccessfully(string $token): void
    {
        $response = $this->request(Request::METHOD_GET, '/api/ping', $token);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_OK, $response);

        $this->assertSame('"pong"', $response->getContent());
    }
}
