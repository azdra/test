<?php

namespace App\Tests\PathFunctional\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserApiControllerTest extends AbstractApiTestCase
{
    public const USER_RAW_DATA = [
        'firstName' => 'Xavier',
        'lastName' => 'baud',
        'email' => 'xbaud.apicreatrion@tsnstest.test',
        'reference' => 'XB0007',
        'company' => 'company',
        'phone' => '0650176341',
        'information' => 'information',
    ];

    public const ADMIN_USER_DATA = [
        'firstName' => 'Super',
        'lastName' => 'ADMIN',
        'email' => 'super_admin_rest@test-client.lise',
        'reference' => 'fptsa2',
        'company' => null,
        'phone' => null,
        'information' => null,
        'sessions' => [],
    ];

    private const EMAIL_PARTICIPANT_ADOBE_CONNECT = 'simple.participant-ac@test-client.lise';
    private const EMAIL_PARTICIPANT_WEBEX = 'simple.participant@test-client.lise';
    private const EMAIL_PARTICIPANT_WEBEX_REST = 'simple.participant-rest@test-client.lise';

    private function getUserRepository(): UserRepository
    {
        return self::getContainer()->get(UserRepository::class);
    }

    public function userListByRoleData(): \Generator
    {
        yield 'Admin user list' => [
            self::AUTHENTICATION_TOKEN_ADMIN_FPT_REST,
            [
                'tests@live-session.fr',
                'simple.participant@test-client.lise',
                'simple.participant-rest@test-client.lise',
                //'simple.participant-ac@test-client.lise',
                'serviceclient@live-session.fr',
            ],
        ];

        yield 'Manager user list' => [
            self::AUTHENTICATION_TOKEN_MANAGER_FPT_REST,
            [
                'tests@live-session.fr',
                //'simple.participant@test-client.lise',
                'simple.participant-rest@test-client.lise',
                'serviceclient@live-session.fr',
            ],
        ];
    }

    /** @dataProvider userListByRoleData */
    public function testGetAllUserSuccessfully(string $token, array $userEmails): void
    {
        $response = $this->request(Request::METHOD_GET, '/api/users/', $token);
        //var_dump($response);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_OK, $response);

        $content = $this->parseResponseContent($response);
        //var_dump($content);
        //var_dump($userEmails);
        //$this->assertCount(count($userEmails), $content);

        foreach ($content as $item) {
            $this->assertArrayHasKey('email', $item);
            //var_dump($item['email']);
            //var_dump($userEmails);
            //$this->assertTrue(in_array($item['email'], $userEmails));
        }
    }

    /** @dataProvider authenticationUserDataGenerator */
    public function testGetHimselfSuccessfully(string $token, string $email): void
    {
        $response = $this->request(Request::METHOD_GET, '/api/users/me', $token);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_OK, $response);

        $content = $this->parseResponseContent($response);
        $this->assertArrayHasKey('email', $content);
        $this->assertSame($email, $content['email']);
    }

    /** @dataProvider authenticationUserDataGenerator */
    public function testGetUserSuccessfully(string $token, string $email): void
    {
        /** @var User $user */
        $user = $this->getUserRepository()->findOneBy(['email' => $email]);
        $userId = $user->getId();

        $response = $this->request(Request::METHOD_GET, "/api/users/$userId", $token);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_OK, $response);

        $content = $this->parseResponseContent($response);
        $this->assertArrayHasKey('email', $content);
        $this->assertArrayHasKey('id', $content);
        $this->assertSame($userId, $content['id']);
        $this->assertSame($email, $content['email']);
    }

    public function testGetUnauthorizedUserReturnForbiddenResponse(): void
    {
        /** @var User $user */
        $user = $this->getUserRepository()->findOneBy(['email' => self::EMAIL_PARTICIPANT_ADOBE_CONNECT]);
        $userId = $user->getId();

        $response = $this->request(Request::METHOD_GET, "/api/users/$userId", self::AUTHENTICATION_TOKEN_MANAGER_FPT);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_FORBIDDEN, $response);

        $content = $this->parseResponseContent($response);
        $this->assertIsValidErrorResponse($content);
        $this->assertSame($content['code'], Response::HTTP_FORBIDDEN);
        $this->assertStringContainsString('Access Denied by controller annotation', $content['message']);
    }

    /** @dataProvider authenticationUserDataGenerator */
    public function testCreateUserSuccessfully(string $token): void
    {
        $response = $this->request(Request::METHOD_POST, '/api/users/', $token, self::USER_RAW_DATA);

        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_OK, $response);

        $content = $this->parseResponseContent($response);

        foreach (self::USER_RAW_DATA as $key => $value) {
            $this->assertArrayHasKey($key, $content);
            $this->assertSame($value, $content[$key]);
        }

        $this->assertArrayHasKey('id', $content);
        $this->assertIsNumeric($content['id']);
    }

    /** @dataProvider authenticationUserDataGenerator */
    public function testUnableToCreateAnExistingUser(string $token): void
    {
        $response = $this->request(Request::METHOD_POST, '/api/users/', $token, self::ADMIN_USER_DATA);

        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_BAD_REQUEST, $response);

        $content = $this->parseResponseContent($response);
        $this->assertIsValidErrorResponse($content);
        $this->assertSame($content['code'], Response::HTTP_BAD_REQUEST);
        $this->assertArrayHasKey('errors', $content);
        $this->assertSame('This user is already exist', $content['message']);
        $this->assertNotEmpty($content['errors']);
    }

    /** @dataProvider authenticationUserDataGenerator */
    public function testBadRequestIsReturnedWithInvalidUserData(string $token): void
    {
        $rawUserData = self::USER_RAW_DATA;
        unset($rawUserData['email']);

        $response = $this->request(Request::METHOD_POST, '/api/users/', $token, $rawUserData);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_BAD_REQUEST, $response);

        $content = $this->parseResponseContent($response);
        $this->assertIsValidErrorResponse($content);
        $this->assertSame($content['code'], Response::HTTP_BAD_REQUEST);
        $this->assertArrayHasKey('errors', $content);
        $this->assertSame('One or more field are invalid on this user', $content['message']);
        $this->assertNotEmpty($content['errors']);
        $this->assertArrayHasKey('email', $content['errors']);
    }

    /** @dataProvider authenticationUserDataGenerator */
    public function testUpdateUserSuccessfully(string $token, string $email): void
    {
        $userRawData = self::USER_RAW_DATA;
        $userRawData['phone'] = '09123654789';

        /** @var User $user */
        $user = $this->getUserRepository()->findOneBy(['email' => $email]);
        $userId = $user->getId();

        $response = $this->request(Request::METHOD_PUT, "/api/users/$userId", $token, $userRawData);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_OK, $response);

        $content = $this->parseResponseContent($response);
        $this->assertArrayHasKey('id', $content);
        $this->assertSame($userId, $content['id']);
        $this->assertArrayHasKey('phone', $content);
        $this->assertSame($userRawData['phone'], $content['phone']);
    }

    /** @dataProvider authenticationUserDataGenerator */
    public function testReturnNotFoundWhenUpdatingANonExistingUser(string $token): void
    {
        $response = $this->request(Request::METHOD_PUT, '/api/users/404', $token, self::USER_RAW_DATA);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_NOT_FOUND, $response);

        $content = $this->parseResponseContent($response);
        $this->assertIsValidErrorResponse($content);
        $this->assertSame($content['code'], Response::HTTP_NOT_FOUND);
        $this->assertSame('App\Entity\User object not found by the @ParamConverter annotation.', $content['message']);
    }

    /** @dataProvider authenticationUserDataGenerator */
    public function testDeleteUserSuccessfully(string $token): void
    {
        /** @var User $user */
        $user = $this->getUserRepository()->findOneBy(['email' => self::EMAIL_PARTICIPANT_WEBEX_REST]);
        $userId = $user->getId();

        $response = $this->request(Request::METHOD_DELETE, "/api/users/$userId", $token);
        $this->assertSame($response->getStatusCode(), Response::HTTP_NO_CONTENT);
        $this->assertEmpty($response->getContent());
    }

    /** @dataProvider authenticationUserDataGenerator */
    public function testDeleteNonExistantUserReturnNotFound(string $token): void
    {
        $response = $this->request(Request::METHOD_DELETE, '/api/users/404', $token);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_NOT_FOUND, $response);

        $content = $this->parseResponseContent($response);
        $this->assertIsValidErrorResponse($content);
        $this->assertSame($content['code'], Response::HTTP_NOT_FOUND);
        $this->assertSame('App\Entity\User object not found by the @ParamConverter annotation.', $content['message']);
    }
}
