<?php

namespace App\Tests\PathFunctional\Api;

use App\Tests\PathFunctional\Utils\AuthenticationTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractApiTestCase extends WebTestCase
{
    use AuthenticationTrait;

    protected const AUTHENTICATION_TOKEN_MANAGER_FPT = 'fpt-mctclise';
    protected const AUTHENTICATION_TOKEN_MANAGER_FPT_REST = 'fpt-mctclise-rest';
    protected const AUTHENTICATION_TOKEN_ADMIN_FPT = 'fpt-fptsa1';
    protected const AUTHENTICATION_TOKEN_ADMIN_FPT_REST = 'fpt-fptsa2';
    protected const AUTHENTICATION_TOKEN_UNAUTHORISED = 'ls2-maclise';
    protected const AUTHENTICATION_USERS_DATA = [
        'Admin' => [
            'token' => self::AUTHENTICATION_TOKEN_ADMIN_FPT,
            'email' => 'super_admin@test-client.lise',
        ],
        'AdminRest' => [
            'token' => self::AUTHENTICATION_TOKEN_ADMIN_FPT_REST,
            'email' => 'super_admin_rest@test-client.lise',
        ],

        'ManagerRest' => [
            'token' => self::AUTHENTICATION_TOKEN_MANAGER_FPT_REST,
            'email' => 'manager_client_rest@test-client.lise',
        ],
    ];
    protected const SESSION_REFERENCE_ADOBE_CONNECT = 'complete_sco_ref';
    //protected const SESSION_REFERENCE_OUTDATED = 'Outdated session';
    //protected const SESSION_REFERENCE_FUTURE = 'Future session';
    protected const SESSION_REFERENCE_OUTDATED_REST = 'Outdated session Rest';
    protected const SESSION_REFERENCE_FUTURE_REST = 'Future session Rest';

    protected KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    protected function request(string $method, string $uri, ?string $authenticationToken = null, array $content = []): Response
    {
        $serverParameters = [
            'HTTP_ACCEPT' => 'application/json',
        ];

        if (!is_null($authenticationToken)) {
            $serverParameters['HTTP_x-auth-token'] = $authenticationToken;
        }

        if (!empty($content)) {
            $serverParameters['CONTENT_TYPE'] = 'application/json';
        }

        $this->client->request($method, $uri, server: $serverParameters, content: json_encode($content));

        return $this->client->getResponse();
    }

    protected function parseResponseContent(Response $response): array
    {
        return json_decode($response->getContent(), true);
    }

    protected function assertIsValidAndEqualToStatutCode(int $expectedStatutCode, Response $response): void
    {
        $this->assertSame($expectedStatutCode, $response->getStatusCode());
        $this->assertJson($response->getContent());
    }

    protected function assertIsValidErrorResponse(array $content): void
    {
        $this->assertArrayHasKey('code', $content);
        $this->assertArrayHasKey('message', $content);
    }

    protected function authenticationUserDataGenerator(): \Generator
    {
        yield 'AdminRest' => self::AUTHENTICATION_USERS_DATA['AdminRest'];
        yield 'ManagerRest' => self::AUTHENTICATION_USERS_DATA['ManagerRest'];
    }
}
