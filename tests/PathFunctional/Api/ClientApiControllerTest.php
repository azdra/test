<?php

namespace App\Tests\PathFunctional\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ClientApiControllerTest extends AbstractApiTestCase
{
    public function clientListData(): \Generator
    {
        yield 'Client list for super admin' => [
            self::AUTHENTICATION_TOKEN_ADMIN_FPT_REST,
            [
                //'Test client',
                'Test client Rest',
                'Live Session',
                'Live Session 2',
                'client_limit_credit',
                //'client_rest_with_limit_credit',
                'CNAS',
            ],
        ];
        /*yield 'Client list for manager' => [
            self::AUTHENTICATION_TOKEN_MANAGER_FPT,
            [
                'Test client',
            ],
        ];*/
        yield 'Client list for manager Rest' => [
            self::AUTHENTICATION_TOKEN_MANAGER_FPT_REST,
            [
                'Test client Rest',
            ],
        ];
    }

    /** @dataProvider clientListData */
    public function testReturnClientListForAdminSuccessfully(string $token, array $clientNames): void
    {
        $response = $this->request(Request::METHOD_GET, '/api/clients/', $token);
        $this->assertIsValidAndEqualToStatutCode(Response::HTTP_OK, $response);

        $content = $this->parseResponseContent($response);
        $this->assertCount(count($clientNames), $content);

        foreach ($content as $item) {
            $this->assertArrayHasKey('name', $item);
            $this->assertTrue(in_array($item['name'], $clientNames));
        }
    }
}
