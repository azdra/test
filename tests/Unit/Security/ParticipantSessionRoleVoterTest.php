<?php

namespace App\Tests\Unit\Security;

use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexParticipant;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Security\ParticipantSessionRoleVoter;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Security;

class ParticipantSessionRoleVoterTest extends TestCase
{
    private Security|MockObject $security;
    private ProviderParticipantSessionRoleRepository|MockObject $sessionRoleRepository;

    public function setUp(): void
    {
        $this->security = $this->createMock(Security::class);
        $this->sessionRoleRepository = $this->createMock(ProviderParticipantSessionRoleRepository::class);
    }

    private function getVoter(): ParticipantSessionRoleVoter
    {
        return new ParticipantSessionRoleVoter($this->security, $this->sessionRoleRepository);
    }

    private function createUserWithClient(int $idClient = 12): User
    {
        $client = $this->createPartialMock(Client::class, ['getId']);
        $client->expects($this->any())->method('getId')->willReturn($idClient);

        $user = new User();
        $user->setClient($client);

        return $user;
    }

    private function createSubject(int $idClient = 12): ProviderParticipantSessionRole
    {
        $participant = new WebexParticipant((new WebexConfigurationHolder())->setClient(new Client()));
        $participant->setUser($this->createUserWithClient($idClient));

        $participantSession = new ProviderParticipantSessionRole();
        $participantSession->setParticipant($participant);

        $session = new WebexMeeting($participant->getAbstractProviderConfigurationHolder());
        $participantSession->setSession($session);

        return $participantSession;
    }

    public function createToken(int $idClient = 12): TokenInterface
    {
        $token = $this->createMock(TokenInterface::class);

        if ($idClient !== null) {
            $token->expects($this->any())->method('getUser')->willReturn($this->createUserWithClient($idClient));
        } else {
            $token->expects($this->any())->method('getUser')->willReturn((new User())->setEmail('my-emayl@gg.com'));
        }

        return $token;
    }

    public function abstainTestCase(): \Generator
    {
        yield 'Does not support wrong attributes' => ['bad_attributes', new ProviderParticipantSessionRole()];
        yield 'Does not support wrong subject' => [ParticipantSessionRoleVoter::EDIT, new User()];
    }

    /**
     * @dataProvider abstainTestCase
     */
    public function testVoteAbstain(string $attribute, object $subject): void
    {
        $token = $this->createToken();

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);
        $this->security->expects($this->never())->method('isGranted');

        $this->assertEquals(VoterInterface::ACCESS_ABSTAIN, $actual);
    }

    public function deniedTestCase(): \Generator
    {
        yield 'User does not have the rol BO' => [false, true];
        yield 'Logged user and participant user does not belongs to the same Client' => [true, false];
    }

    /**
     * @dataProvider deniedTestCase
     */
    public function testVoteDenied(?bool $isBo, bool $isSameClient): void
    {
        $token = $this->createToken();
        $subject = $this->createSubject($isSameClient ? 12 : 42);
        $attribute = ParticipantSessionRoleVoter::EDIT;
        if (null === $isBo) {
            $this->security->expects($this->never())->method('isGranted');
        } else {
            $this->security->expects($this->exactly(2))->method('isGranted')
            ->withConsecutive(['ROLE_HOTLINE_CUSTOMER_SERVICE'], ['ROLE_MANAGER'])->willReturnOnConsecutiveCalls(false, $isBo);
        }

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_DENIED, $actual);
    }

    public function testVoteIsGrantedSameClient(): void
    {
        $token = $this->createToken();
        $subject = $this->createSubject();
        $attribute = ParticipantSessionRoleVoter::EDIT;

        $this->security->expects($this->exactly(2))->method('isGranted')
            ->withConsecutive(['ROLE_HOTLINE_CUSTOMER_SERVICE'], ['ROLE_MANAGER'])->willReturnOnConsecutiveCalls(false, true);
        $this->sessionRoleRepository->expects($this->never())->method('isAnimator');

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_GRANTED, $actual);
    }

    public function testVoteIsGrantedNotSameClientButAdmin(): void
    {
        $token = $this->createToken();
        $subject = $this->createSubject(idClient: 42);
        $attribute = ParticipantSessionRoleVoter::EDIT;

        $this->security->expects($this->once())->method('isGranted')->with('ROLE_HOTLINE_CUSTOMER_SERVICE')->willReturn(true);
        $this->sessionRoleRepository->expects($this->never())->method('isAnimator');

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_GRANTED, $actual);
    }

    public function testAccessGrantedForAnimator(): void
    {
        $token = $this->createToken();
        $subject = $this->createSubject(idClient: 42);
        $attribute = ParticipantSessionRoleVoter::ACCESS;

        $this->security->expects($this->exactly(2))->method('isGranted')
            ->withConsecutive(['ROLE_HOTLINE_CUSTOMER_SERVICE'], ['ROLE_MANAGER'])->willReturn(false);
        $this->sessionRoleRepository->expects($this->once())->method('isAnimator')->willReturn(true);

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_GRANTED, $actual);
    }

    public function testAccessDeniedForNotAdminNorManagerNorAnimator(): void
    {
        $token = $this->createToken();
        $subject = $this->createSubject(idClient: 42);
        $attribute = ParticipantSessionRoleVoter::ACCESS;

        $this->security->expects($this->exactly(2))->method('isGranted')
            ->withConsecutive(['ROLE_HOTLINE_CUSTOMER_SERVICE'], ['ROLE_MANAGER'])->willReturn(false);
        $this->sessionRoleRepository->expects($this->once())->method('isAnimator')->willReturn(false);

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_DENIED, $actual);
    }
}
