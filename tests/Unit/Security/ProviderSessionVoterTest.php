<?php

namespace App\Tests\Unit\Security;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Security\ProviderSessionVoter;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Security;

class ProviderSessionVoterTest extends TestCase
{
    private Security|MockObject $security;
    private ProviderParticipantSessionRoleRepository|MockObject $sessionRoleRepository;

    public function setUp(): void
    {
        $this->security = $this->createMock(Security::class);
        $this->sessionRoleRepository = $this->createMock(ProviderParticipantSessionRoleRepository::class);
    }

    public function getVoter(): ProviderSessionVoter
    {
        return new ProviderSessionVoter($this->security, $this->sessionRoleRepository);
    }

    public function createClient(int $idClient): Client
    {
        $client = $this->createPartialMock(Client::class, ['getId']);
        $client->expects($this->any())->method('getId')->willReturn($idClient);

        return $client;
    }

    private function createUserWithClient(int $idClient): User
    {
        $user = new User();
        $user->setClient($this->createClient($idClient));

        return $user;
    }

    private function createTokenAndExpectationForUserType(string $userType): TokenInterface
    {
        $this->security
            ->expects($this->atLeast(1))
            ->method('isGranted')
            ->willReturnMap([
                [User::ROLE_ADMIN, null, $userType === 'admin'],
                [User::ROLE_HOTLINE_CUSTOMER_SERVICE, null, $userType === 'hotline'],
                [User::ROLE_MANAGER, null, $userType === 'manager'],
            ]);

        $this->sessionRoleRepository
            ->method('isAnimator')
            ->willReturn($userType === 'animator');

        return $this->createValidToken(12);
    }

    public function createValidToken(int $idClient): TokenInterface
    {
        $token = $this->createMock(TokenInterface::class);
        $token->expects($this->any())->method('getUser')->willReturn($this->createUserWithClient($idClient));

        return $token;
    }

    public function createInvalidToken(): TokenInterface
    {
        $token = $this->createMock(TokenInterface::class);
        $token->expects($this->any())->method('getUser')->willReturn(null);

        return $token;
    }

    private function createSessionForType(string $sessionType, int $idClient = 12): ProviderSession
    {
        $configurationHolder = (new AdobeConnectConfigurationHolder())
            ->setClient($this->createClient($idClient));

        return match ($sessionType) {
            'finished' => $this->createFinishedSession($configurationHolder),
            'running' => $this->createRunningSession($configurationHolder),
            default => $this->createIncomingSession($configurationHolder)
        };
    }

    public function createFinishedSession(AbstractProviderConfigurationHolder $configurationHolder): ProviderSession
    {
        return (new AdobeConnectSCO($configurationHolder))
            ->setStatus(ProviderSession::STATUS_SCHEDULED)
            ->setDateStart((new \DateTime())->modify('- 2 day'))
            ->setDuration(60);
    }

    public function createRunningSession(AbstractProviderConfigurationHolder $configurationHolder): ProviderSession
    {
        return (new AdobeConnectSCO($configurationHolder))
            ->setStatus(ProviderSession::STATUS_SCHEDULED)
            ->setDateStart((new \DateTime())->modify('- 30 minute'))
            ->setDuration(60);
    }

    public function createIncomingSession(AbstractProviderConfigurationHolder $configurationHolder): ProviderSession
    {
        return (new AdobeConnectSCO($configurationHolder))
            ->setStatus(ProviderSession::STATUS_SCHEDULED)
            ->setDateStart((new \DateTime())->modify('+ 1 day'))
            ->setDuration(60);
    }

    public function createSubject(int $idClient, bool $isTerminated = false): ProviderSession
    {
        return $this->createSessionForType($isTerminated ? 'finished' : 'incoming', $idClient);
    }

    public function testAbstainIfAttributeIsNotSupported(): void
    {
        $token = $this->createValidToken(12);
        $subject = $this->createSubject(12);
        $attribute = 'invalid';
        $this->security->expects($this->never())->method('isGranted');
        $this->sessionRoleRepository->expects($this->never())->method('isAnimator');

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_ABSTAIN, $actual);
    }

    public function testAbstainIfSubjectIsNotSupported(): void
    {
        $token = $this->createValidToken(12);
        $subject = new AdobeConnectConfigurationHolder();
        $attribute = ProviderSessionVoter::EDIT;
        $this->security->expects($this->never())->method('isGranted');
        $this->sessionRoleRepository->expects($this->never())->method('isAnimator');

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_ABSTAIN, $actual);
    }

    /**
     * @dataProvider allVerb
     */
    public function testAttributeGrantedForAdmin($attribute): void
    {
        $token = $this->createValidToken(12);
        $subject = $this->createSubject(21, true);

        $this->security->expects($this->once())->method('isGranted')
            ->with(User::ROLE_ADMIN)->willReturn(true);
        $this->sessionRoleRepository->expects($this->never())->method('isAnimator');

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_GRANTED, $actual);
    }

    public function testCancelDeniedIsSessionIsADraft(): void
    {
        $token = $this->createValidToken(12);
        $subject = $this->createSubject(21, true);
        $subject->setStatus(ProviderSession::STATUS_DRAFT);
        $attribute = ProviderSessionVoter::CANCEL;

        $this->security->expects($this->never())->method('isGranted');
        $this->sessionRoleRepository->expects($this->never())->method('isAnimator');

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_DENIED, $actual);
    }

    /**
     * @dataProvider allVerb
     */
    public function testAttributeGrantedForManagerOnTheSameClient(string $attribute): void
    {
        $token = $this->createValidToken(12);
        $subject = $this->createSubject(12);

        $this->security->expects($this->exactly(2))->method('isGranted')
            ->withConsecutive([User::ROLE_ADMIN], [User::ROLE_MANAGER])->willReturnOnConsecutiveCalls(false, true);

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_GRANTED, $actual);
    }

    /**
     * @dataProvider verbDeniedIfSessionIsFinished
     */
    public function testAttributeDeniedAndManagerChecksNeverCalledOnSessionFinished(string $attribute): void
    {
        $token = $this->createValidToken(12);
        $subject = $this->createSubject(12, true);

        $this->security->expects($this->exactly(1))->method('isGranted')
            ->with(User::ROLE_ADMIN)->willReturn(false);

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_DENIED, $actual);
    }

    public function testAccessGrantedForAnimator(): void
    {
        $token = $this->createValidToken(12);
        $subject = $this->createSubject(12);
        $attribute = ProviderSessionVoter::ACCESS;
        $this->security->expects($this->exactly(2))->method('isGranted')
            ->withConsecutive([User::ROLE_ADMIN], [User::ROLE_MANAGER])->willReturn(false);
        $this->sessionRoleRepository->expects($this->once())->method('isAnimator')->willReturn(true);

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_GRANTED, $actual);
    }

    public function testAccessDeniedForClassicUserNotAnimator(): void
    {
        $token = $this->createValidToken(12);
        $subject = $this->createSubject(12);
        $attribute = ProviderSessionVoter::ACCESS;
        $this->security->expects($this->exactly(3))->method('isGranted')
            ->withConsecutive([User::ROLE_ADMIN], [User::ROLE_MANAGER])->willReturn(false);
        $this->sessionRoleRepository->expects($this->once())->method('isAnimator')->willReturn(false);

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_DENIED, $actual);
    }

    /**
     * @dataProvider verbAlwaysDeniedForClassicUser
     */
    public function testAttributeDeniedAndAnimatorCheckNeverCalled(string $attribute): void
    {
        $token = $this->createValidToken(12);
        $subject = $this->createSubject(12);

        $this->security->expects($this->exactly(2))->method('isGranted')
            ->withConsecutive([User::ROLE_ADMIN], [User::ROLE_MANAGER])->willReturn(false);
        $this->sessionRoleRepository->expects($this->never())->method('isAnimator');

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_DENIED, $actual);
    }

    /**
     * @dataProvider adminUseCase
     * @dataProvider clientManagerUseCase
     * @dataProvider animatorUseCase
     * @dataProvider simpleUserUseCase
     */
    public function testDetailedUseCase(string $userType, string $sessionType, string $verb, int $expected): void
    {
        $token = $this->createTokenAndExpectationForUserType($userType);
        $subject = $this->createSessionForType($sessionType);

        $actual = $this->getVoter()->vote($token, $subject, [$verb]);

        $this->assertEquals($expected, $actual);
    }

    private function adminUseCase(): \Generator
    {
        yield 'Admin should be allowed to update participant of incoming session' => ['admin', 'incoming', ProviderSessionVoter::UPDATE_PARTICIPANT, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should be allowed to send convocation of incoming session' => ['admin', 'incoming', ProviderSessionVoter::SEND_CONVOCATION, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should be allowed to access an incoming session' => ['admin', 'incoming', ProviderSessionVoter::ACCESS, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should be allowed to edit an incoming session' => ['admin', 'incoming', ProviderSessionVoter::EDIT, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should be allowed to cancel an incoming session' => ['admin', 'incoming', ProviderSessionVoter::DELETE, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should be allowed to delete an incoming session' => ['admin', 'incoming', ProviderSessionVoter::CANCEL, VoterInterface::ACCESS_GRANTED];

        yield 'Admin should be allowed to update participant of running session' => ['admin', 'running', ProviderSessionVoter::UPDATE_PARTICIPANT, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should be allowed to send convocation of running session' => ['admin', 'running', ProviderSessionVoter::SEND_CONVOCATION, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should be allowed to access a running session' => ['admin', 'running', ProviderSessionVoter::ACCESS, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should NOT be allowed to edit a running session' => ['admin', 'running', ProviderSessionVoter::EDIT, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should NOT be allowed to cancel a running session' => ['admin', 'running', ProviderSessionVoter::DELETE, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should NOT be allowed to delete a running session' => ['admin', 'running', ProviderSessionVoter::CANCEL, VoterInterface::ACCESS_GRANTED];

        yield 'Admin should NOT be allowed to update participant of finished session' => ['admin', 'finished', ProviderSessionVoter::UPDATE_PARTICIPANT, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should NOT be allowed to send convocation of finished session' => ['admin', 'finished', ProviderSessionVoter::SEND_CONVOCATION, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should be allowed to access a finished session' => ['admin', 'finished', ProviderSessionVoter::ACCESS, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should NOT be allowed to edit a finished session' => ['admin', 'finished', ProviderSessionVoter::EDIT, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should NOT be allowed to cancel a finished session' => ['admin', 'finished', ProviderSessionVoter::DELETE, VoterInterface::ACCESS_GRANTED];
        yield 'Admin should NOT be allowed to delete a finished session' => ['admin', 'finished', ProviderSessionVoter::CANCEL, VoterInterface::ACCESS_GRANTED];
    }

    public function clientManagerUseCase(): \Generator
    {
        yield 'Client manager should be allowed to update participant of incoming session' => ['manager', 'incoming', ProviderSessionVoter::UPDATE_PARTICIPANT, VoterInterface::ACCESS_GRANTED];
        yield 'Client manager should be allowed to send convocation of incoming session' => ['manager', 'incoming', ProviderSessionVoter::SEND_CONVOCATION, VoterInterface::ACCESS_GRANTED];
        yield 'Client manager should be allowed to access an incoming session' => ['manager', 'incoming', ProviderSessionVoter::ACCESS, VoterInterface::ACCESS_GRANTED];
        yield 'Client manager should be allowed to edit an incoming session' => ['manager', 'incoming', ProviderSessionVoter::EDIT, VoterInterface::ACCESS_GRANTED];
        yield 'Client manager should be allowed to cancel an incoming session' => ['manager', 'incoming', ProviderSessionVoter::DELETE, VoterInterface::ACCESS_GRANTED];
        yield 'Client manager should be allowed to delete an incoming session' => ['manager', 'incoming', ProviderSessionVoter::CANCEL, VoterInterface::ACCESS_GRANTED];

        yield 'Client manager should be allowed to update participant of running session' => ['manager', 'running', ProviderSessionVoter::UPDATE_PARTICIPANT, VoterInterface::ACCESS_GRANTED];
        yield 'Client manager should be allowed to send convocation of running session' => ['manager', 'running', ProviderSessionVoter::SEND_CONVOCATION, VoterInterface::ACCESS_GRANTED];
        yield 'Client manager should be allowed to access a running session' => ['manager', 'running', ProviderSessionVoter::ACCESS, VoterInterface::ACCESS_GRANTED];
        yield 'Client manager should NOT be allowed to edit a running session' => ['manager', 'running', ProviderSessionVoter::EDIT, VoterInterface::ACCESS_DENIED];
        yield 'Client manager should NOT be allowed to cancel a running session' => ['manager', 'running', ProviderSessionVoter::DELETE, VoterInterface::ACCESS_DENIED];
        yield 'Client manager should NOT be allowed to delete a running session' => ['manager', 'running', ProviderSessionVoter::CANCEL, VoterInterface::ACCESS_DENIED];

        yield 'Client manager should NOT be allowed to update participant of finished session' => ['manager', 'finished', ProviderSessionVoter::UPDATE_PARTICIPANT, VoterInterface::ACCESS_DENIED];
        yield 'Client manager should NOT be allowed to send convocation of finished session' => ['manager', 'finished', ProviderSessionVoter::SEND_CONVOCATION, VoterInterface::ACCESS_DENIED];
        yield 'Client manager should be allowed to access a finished session' => ['manager', 'finished', ProviderSessionVoter::ACCESS, VoterInterface::ACCESS_GRANTED];
        yield 'Client manager should NOT be allowed to edit a finished session' => ['manager', 'finished', ProviderSessionVoter::EDIT, VoterInterface::ACCESS_DENIED];
        yield 'Client manager should NOT be allowed to cancel a finished session' => ['manager', 'finished', ProviderSessionVoter::DELETE, VoterInterface::ACCESS_DENIED];
        yield 'Client manager should NOT be allowed to delete a finished session' => ['manager', 'finished', ProviderSessionVoter::CANCEL, VoterInterface::ACCESS_DENIED];
    }

    public function animatorUseCase(): \Generator
    {
        yield 'Animator should NOT be allowed to update participant of incoming session' => ['animator', 'incoming', ProviderSessionVoter::UPDATE_PARTICIPANT, VoterInterface::ACCESS_DENIED];
        yield 'Animator should NOT be allowed to send convocation of incoming session' => ['animator', 'incoming', ProviderSessionVoter::SEND_CONVOCATION, VoterInterface::ACCESS_DENIED];
        yield 'Animator should NOT be allowed to access an incoming session' => ['animator', 'incoming', ProviderSessionVoter::ACCESS, VoterInterface::ACCESS_GRANTED];
        yield 'Animator should NOT be allowed to edit an incoming session' => ['animator', 'incoming', ProviderSessionVoter::EDIT, VoterInterface::ACCESS_DENIED];
        yield 'Animator should NOT be allowed to cancel an incoming session' => ['animator', 'incoming', ProviderSessionVoter::DELETE, VoterInterface::ACCESS_DENIED];
        yield 'Animator should NOT be allowed to delete an incoming session' => ['animator', 'incoming', ProviderSessionVoter::CANCEL, VoterInterface::ACCESS_DENIED];

        yield 'Animator should NOT be allowed to update participant of running session' => ['animator', 'running', ProviderSessionVoter::UPDATE_PARTICIPANT, VoterInterface::ACCESS_DENIED];
        yield 'Animator should NOT be allowed to send convocation of running session' => ['animator', 'running', ProviderSessionVoter::SEND_CONVOCATION, VoterInterface::ACCESS_DENIED];
        yield 'Animator should NOT be allowed to access a running session' => ['animator', 'running', ProviderSessionVoter::ACCESS, VoterInterface::ACCESS_GRANTED];
        yield 'Animator should NOT be allowed to edit a running session' => ['animator', 'running', ProviderSessionVoter::EDIT, VoterInterface::ACCESS_DENIED];
        yield 'Animator should NOT be allowed to cancel a running session' => ['animator', 'running', ProviderSessionVoter::DELETE, VoterInterface::ACCESS_DENIED];
        yield 'Animator should NOT be allowed to delete a running session' => ['animator', 'running', ProviderSessionVoter::CANCEL, VoterInterface::ACCESS_DENIED];

        yield 'Animator should NOT be allowed to update participant of finished session' => ['animator', 'finished', ProviderSessionVoter::UPDATE_PARTICIPANT, VoterInterface::ACCESS_DENIED];
        yield 'Animator should NOT be allowed to send convocation of finished session' => ['animator', 'finished', ProviderSessionVoter::SEND_CONVOCATION, VoterInterface::ACCESS_DENIED];
        yield 'Animator should NOT be allowed to access a finished session' => ['animator', 'finished', ProviderSessionVoter::ACCESS, VoterInterface::ACCESS_GRANTED];
        yield 'Animator should NOT be allowed to edit a finished session' => ['animator', 'finished', ProviderSessionVoter::EDIT, VoterInterface::ACCESS_DENIED];
        yield 'Animator should NOT be allowed to cancel a finished session' => ['animator', 'finished', ProviderSessionVoter::DELETE, VoterInterface::ACCESS_DENIED];
        yield 'Animator should NOT be allowed to delete a finished session' => ['animator', 'finished', ProviderSessionVoter::CANCEL, VoterInterface::ACCESS_DENIED];
    }

    public function simpleUserUseCase(): \Generator
    {
        yield 'Simple user should NOT be allowed to update participant of incoming session' => ['user', 'incoming', ProviderSessionVoter::UPDATE_PARTICIPANT, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to send convocation of incoming session' => ['user', 'incoming', ProviderSessionVoter::SEND_CONVOCATION, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to access an incoming session' => ['user', 'incoming', ProviderSessionVoter::ACCESS, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to edit an incoming session' => ['user', 'incoming', ProviderSessionVoter::EDIT, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to cancel an incoming session' => ['user', 'incoming', ProviderSessionVoter::DELETE, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to delete an incoming session' => ['user', 'incoming', ProviderSessionVoter::CANCEL, VoterInterface::ACCESS_DENIED];

        yield 'Simple user should NOT be allowed to update participant of running session' => ['user', 'running', ProviderSessionVoter::UPDATE_PARTICIPANT, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to send convocation of running session' => ['user', 'running', ProviderSessionVoter::SEND_CONVOCATION, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to access a running session' => ['user', 'running', ProviderSessionVoter::ACCESS, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to edit a running session' => ['user', 'running', ProviderSessionVoter::EDIT, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to cancel a running session' => ['user', 'running', ProviderSessionVoter::DELETE, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to delete a running session' => ['user', 'running', ProviderSessionVoter::CANCEL, VoterInterface::ACCESS_DENIED];

        yield 'Simple user should NOT be allowed to update participant of finished session' => ['user', 'finished', ProviderSessionVoter::UPDATE_PARTICIPANT, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to send convocation of finished session' => ['user', 'finished', ProviderSessionVoter::SEND_CONVOCATION, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to access a finished session' => ['user', 'finished', ProviderSessionVoter::ACCESS, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to edit a finished session' => ['user', 'finished', ProviderSessionVoter::EDIT, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to cancel a finished session' => ['user', 'finished', ProviderSessionVoter::DELETE, VoterInterface::ACCESS_DENIED];
        yield 'Simple user should NOT be allowed to delete a finished session' => ['user', 'finished', ProviderSessionVoter::CANCEL, VoterInterface::ACCESS_DENIED];
    }

    public function allVerb(): \Generator
    {
        yield 'Edit attribute' => [ProviderSessionVoter::EDIT];
        yield 'Delete attribute' => [ProviderSessionVoter::DELETE];
        yield 'Cancel attribute' => [ProviderSessionVoter::CANCEL];
        yield 'Access attribute' => [ProviderSessionVoter::ACCESS];
    }

    public function verbDeniedIfSessionIsFinished(): \Generator
    {
        yield 'Delete attribute' => [ProviderSessionVoter::DELETE];
        yield 'Cancel attribute' => [ProviderSessionVoter::CANCEL];
        yield 'Edit attribute' => [ProviderSessionVoter::EDIT];
        yield 'Update participant attribute' => [ProviderSessionVoter::UPDATE_PARTICIPANT];
        yield 'Send convocation attribute' => [ProviderSessionVoter::SEND_CONVOCATION];
    }

    public function verbAlwaysDeniedForClassicUser(): \Generator
    {
        yield 'Edit attribute' => [ProviderSessionVoter::EDIT];
        yield 'Delete attribute' => [ProviderSessionVoter::DELETE];
        yield 'Cancel attribute' => [ProviderSessionVoter::CANCEL];
    }
}
