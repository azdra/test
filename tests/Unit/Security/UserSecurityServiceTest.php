<?php

namespace App\Tests\Unit\Security;

use App\Entity\Client\Client;
use App\Entity\User;
use App\Event\UserPasswordEvent;
use App\Repository\UserRepository;
use App\Security\UserSecurityService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserSecurityServiceTest extends TestCase
{
    protected function createUserSecurityService(?EventDispatcherInterface $dispatcher = null, ?UserRepository $userRepository = null, ?EntityManagerInterface $entityManager = null): UserSecurityService
    {
        $dispatcher ??= $this->createMock(EventDispatcherInterface::class);
        $userRepository ??= $this->createMock(UserRepository::class);
        $entityManager ??= $this->createMock(EntityManagerInterface::class);

        return new UserSecurityService($dispatcher, $userRepository, $entityManager);
    }

    public function testResetPasswordLessThan10MinutesSinceLastResetFails(): void
    {
        $user = (new User())
            ->setLastPasswordReset(new \DateTimeImmutable('now'));

        $dispatcher = $this->createMock(EventDispatcherInterface::class);
        $dispatcher->expects($this->never())
            ->method('dispatch');

        $service = $this->createUserSecurityService($dispatcher);
        $service->requestPasswordReset($user);
    }

    public function testResetPasswordGenerateTokenAndSetLastResetDate(): void
    {
        $user = new User();

        $dispatcher = $this->createMock(EventDispatcherInterface::class);
        $dispatcher->expects($this->once())
            ->method('dispatch')
            ->with($this->anything(), UserPasswordEvent::RESET);

        $service = $this->createUserSecurityService($dispatcher);
        $service->requestPasswordReset($user);
        $this->assertNotNull($user->getPasswordResetToken());
        $this->assertNotNull($user->getLastPasswordReset());
    }

    public function testSetPasswordToken(): void
    {
        $user = new User();

        $service = $this->createUserSecurityService();

        $service->setPasswordToken($user);

        $this->assertNotNull($user->getPasswordResetToken());
        $this->assertNotNull($user->getLastPasswordReset());
    }

    public function testUpdatePasswordHashPasswordAndSetResetTokenToNull(): void
    {
        $user = (new User())
            ->setEmail('test@test.unit')
            ->setClient(new Client())
            ->setStatus(User::STATUS_ACTIVE);

        $userBis = (new User())
            ->setEmail('test@test.unit')
            ->setClient(new Client())
            ->setStatus(User::STATUS_INACTIVE);

        $userTer = (new User())
            ->setEmail('test@test.unit')
            ->setClient(new Client())
            ->setStatus(User::STATUS_INACTIVE);

        $userRepository = $this->createMock(UserRepository::class);
        $userRepository->method('findBy')
            ->willReturn([$user, $userBis, $userTer]);

        $service = $this->createUserSecurityService(userRepository: $userRepository);
        $service->updatePassword($user, 'plain');

        $this->assertNull($user->getPasswordResetToken());
        $this->assertSame('plain', $user->getClearPassword());
    }

    public function testRandomPassword(): void
    {
        $user = new User();

        $service = $this->createUserSecurityService();
        $password = $service->randomPassword();

        $user->setPassword($password);

        $this->assertSame($password, $user->getPassword());
    }

    public function testCreatePassword(): void
    {
        $user = (new User())
            ->setEmail('test@test.unit')
            ->setClient(new Client())
            ->setStatus(User::STATUS_ACTIVE);

        $userRepository = $this->createMock(UserRepository::class);
        $userRepository->method('findBy')
            ->willReturn([]);

        $service = $this->createUserSecurityService(userRepository: $userRepository);

        $service->createOrClonePassword($user);

        $this->assertNotNull($user->getPasswordResetToken());
        $this->assertNotNull($user->getLastPasswordReset());
    }

    public function testClonePassword(): void
    {
        $user = (new User())
            ->setEmail('test@test.unit')
            ->setClient(new Client())
            ->setStatus(User::STATUS_ACTIVE);

        $userBis = (new User())
            ->setEmail('test@test.unit')
            ->setClient(new Client())
            ->setStatus(User::STATUS_INACTIVE)
            ->setPassword('$2y$13$UL.k.bizDYnRvIaroBE/J.LUlA16IY6hZyK84XqxMpm6ry9VGOM52');

        $userTer = (new User())
            ->setEmail('test@test.unit')
            ->setClient(new Client())
            ->setStatus(User::STATUS_INACTIVE)
            ->setPassword('$2y$13$4gpY4HlFNuR.uIK4xscynOf.PN.v/FO7Y5mPHW4KBryt3yaWM29IW');

        $userRepository = $this->createMock(UserRepository::class);
        $userRepository->method('findBy')
            ->willReturn([$userBis, $userTer]);

        $service = $this->createUserSecurityService(userRepository: $userRepository);

        $service->createOrClonePassword($user);

        $this->assertNull($user->getPasswordResetToken());
        $this->assertNotNull($user->getPassword());
    }
}
