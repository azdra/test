<?php

namespace App\Tests\Unit\Security;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Entity\User;
use App\Security\ProviderParticipantVoter;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Security;

class ProviderParticipantVoterTest extends TestCase
{
    private Security|MockObject $security;

    public function setUp(): void
    {
        $this->security = $this->createMock(Security::class);
    }

    public function getVoter(): ProviderParticipantVoter
    {
        return new ProviderParticipantVoter($this->security);
    }

    public function createClient(int $idClient): Client
    {
        $client = $this->createPartialMock(Client::class, ['getId']);
        $client->expects($this->any())->method('getId')->willReturn($idClient);

        return $client;
    }

    private function createUserWithClient(int $idClient): User
    {
        $user = new User();
        $user->setClient($this->createClient($idClient));

        return $user;
    }

    public function createValidToken(int $idClient): TokenInterface
    {
        $token = $this->createMock(TokenInterface::class);
        $token->expects($this->any())->method('getUser')->willReturn($this->createUserWithClient($idClient));

        return $token;
    }

    public function createInvalidToken(): TokenInterface
    {
        $token = $this->createMock(TokenInterface::class);
        $token->expects($this->any())->method('getUser')->willReturn(null);

        return $token;
    }

    public function createSubject(int $idClient): ProviderParticipant
    {
        $user = $this->createUserWithClient($idClient);
        $configurationHolder = new AdobeConnectConfigurationHolder();
        $configurationHolder->setClient($user->getClient());
        $subject = new AdobeConnectPrincipal($configurationHolder);
        $subject->setUser($user);

        return $subject;
    }

    public function testAbstainIfAttributeIsNotSupported(): void
    {
        $token = $this->createValidToken(12);
        $subject = $this->createSubject(12);
        $attribute = 'invalid';
        $this->security->expects($this->never())->method('isGranted');

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_ABSTAIN, $actual);
    }

    public function testAbstainIfSubjectIsNotSupported(): void
    {
        $token = $this->createValidToken(12);
        $subject = new AdobeConnectConfigurationHolder();
        $attribute = ProviderParticipantVoter::EDIT;
        $this->security->expects($this->never())->method('isGranted');

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_ABSTAIN, $actual);
    }

    public function testGrantedForAdmin(): void
    {
        $token = $this->createValidToken(12);
        $subject = $this->createSubject(21);
        $attribute = ProviderParticipantVoter::EDIT;
        $this->security->expects($this->once())->method('isGranted')
            ->with(User::ROLE_ADMIN)->willReturn(true);

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_GRANTED, $actual);
    }

    public function testDeniedForUserNotAdminNorManager(): void
    {
        $token = $this->createValidToken(12);
        $subject = $this->createSubject(12);
        $attribute = ProviderParticipantVoter::EDIT;
        $this->security->expects($this->exactly(2))->method('isGranted')
            ->withConsecutive([User::ROLE_ADMIN], [User::ROLE_MANAGER])->willReturn(false);

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_DENIED, $actual);
    }

    public function testGrantedForManagerOnTheSameClient(): void
    {
        $token = $this->createValidToken(12);
        $subject = $this->createSubject(12);
        $attribute = ProviderParticipantVoter::EDIT;
        $this->security->expects($this->exactly(2))->method('isGranted')
            ->withConsecutive([User::ROLE_ADMIN], [User::ROLE_MANAGER])->willReturnOnConsecutiveCalls(false, true);

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_GRANTED, $actual);
    }

    public function testDeniedForManagerNotOnTheSameClient(): void
    {
        $token = $this->createValidToken(12);
        $subject = $this->createSubject(21);
        $attribute = ProviderParticipantVoter::EDIT;
        $this->security->expects($this->exactly(2))->method('isGranted')
            ->withConsecutive([User::ROLE_ADMIN], [User::ROLE_MANAGER])->willReturnOnConsecutiveCalls(false, true);

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_DENIED, $actual);
    }
}
