<?php

namespace App\Tests\Unit\Security;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\SwitchUserVoter;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class SwitchUserVoterTest extends TestCase
{
    private Security|MockObject $security;
    private UserRepository|MockObject $userRepository;

    protected function setUp(): void
    {
        $this->security = $this->createMock(Security::class);
        $this->userRepository = $this->createMock(UserRepository::class);
    }

    private function getVoter(): SwitchUserVoter
    {
        return new SwitchUserVoter(
            $this->security,
            $this->userRepository
        );
    }

    private function createUser(): UserInterface
    {
        $user = (new User())->setEmail('test@user.fr');

        return $user;
    }

    public function createTokenWithNullUser(): TokenInterface
    {
        $token = $this->createMock(TokenInterface::class);

        $token->expects($this->any())
            ->method('getUser')
            ->willReturn(
                null
            );

        return $token;
    }

    public function createToken(): TokenInterface
    {
        $token = $this->createMock(TokenInterface::class);

        $token->expects($this->any())
            ->method('getUser')
            ->willReturn(
                $this->createUser()
            );

        return $token;
    }

    public function abstainTestCase(): \Generator
    {
        yield 'Does not support wrong attributes' => ['ROLE_USER', new User()];
        yield 'Does not support wrong subject' => ['CAN_SWITCH_USER', new ProviderParticipantSessionRole()];
    }

    /**
     * @dataProvider abstainTestCase
     */
    public function testVoteAbstain(string $attribute, object $subject): void
    {
        $token = $this->createToken();

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);
        $this->security->expects($this->never())->method('isGranted');

        $this->assertEquals(VoterInterface::ACCESS_ABSTAIN, $actual);
    }

    public function deniedTestCase(): \Generator
    {
        yield 'User return by Token is null' => [$this->createTokenWithNullUser(), true, false];
        yield 'User is not Admin a Manager' => [$this->createToken(), false, false];
        yield 'User is not is available users retrieve' => [$this->createToken(), true, false];
    }

    /**
     * @dataProvider deniedTestCase
     */
    public function testVoteDenied(TokenInterface $token, bool $isManager, bool $isAvailableUser): void
    {
        $subject = $this->createUser();
        $attribute = 'CAN_SWITCH_USER';

        if (!empty($token->getUser())) {
            $this->security
                ->expects($this->exactly(1))
                ->method('isGranted')
                ->with('ROLE_MANAGER')
                ->willReturn($isManager);
        }

        if (!$isAvailableUser) {
            $this->userRepository
                ->expects($this->atMost(1))
                ->method('findBy')
                ->willReturn([new User(), new User(), new User()]);
        } else {
            $this->userRepository
                ->expects($this->atMost(1))
                ->method('findBy')
                ->willReturn([new User(), new User(), $subject]);
        }

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_DENIED, $actual);
    }

    public function testVoteIsGrantedManagerAndAvailableUserForSwitching(): void
    {
        $token = $this->createToken();
        $subject = $this->createUser();
        $attribute = SwitchUserVoter::CAN_SWITCH_USER;

        $this->security
            ->expects($this->exactly(1))
            ->method('isGranted')
            ->with('ROLE_MANAGER')
            ->willReturn(true);

        $this->userRepository
            ->expects($this->exactly(1))
            ->method('findBy')
            ->willReturn([new User(), new User(), $subject]);

        $actual = $this->getVoter()->vote($token, $subject, [$attribute]);

        $this->assertEquals(VoterInterface::ACCESS_GRANTED, $actual);
    }
}
