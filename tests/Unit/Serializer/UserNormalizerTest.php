<?php

namespace App\Tests\Unit\Serializer;

use App\Entity\User;
use App\Serializer\UserNormalizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserNormalizerTest extends TestCase
{
    protected function createConcreteNormalizer(?ObjectNormalizer $normalizer = null, TranslatorInterface $translator): UserNormalizer
    {
        return new UserNormalizer(
            $normalizer ?? $this->createMock(ObjectNormalizer::class),
            $translator
        );
    }

    public function getValidUserData(): \Generator
    {
        yield [new User()];
    }

    public function getInvalidUserData(): \Generator
    {
        yield [123];
        yield ['User'];
        yield [true];
        yield [null];
        yield [new \stdClass()];
    }

    /**
     * @dataProvider getValidUserData
     */
    public function testNormalizerSupportsUser($user): void
    {
        $this->assertTrue($this->createConcreteNormalizer(null, $this->createMock(TranslatorInterface::class))->supportsNormalization($user));
    }

    /**
     * @dataProvider getInvalidUserData
     */
    public function testNormalizerDoesntSupportInvalidUser($data): void
    {
        $this->assertFalse($this->createConcreteNormalizer(null, $this->createMock(TranslatorInterface::class))->supportsNormalization($data));
    }

    public function testNormalizerFillsRoles()
    {
        $objectNormalizer = $this->createMock(ObjectNormalizer::class, TranslatorInterface::class);
        $objectNormalizer->expects($this->once())
            ->method('normalize')
            ->willReturn([]);
        $translator = $this->createMock(TranslatorInterface::class);

        $normalized = $this->createConcreteNormalizer($objectNormalizer, $translator)
            ->normalize(
                (new User())
                    ->setId(0)
                    ->setRoles(User::ROLES),
                context: ['groups' => 'read_user']
            );

        $this->assertArrayHasKey('transform', $normalized);
        $this->assertArrayHasKey('Participant', $normalized['transform']['roles_readable']);
        $this->assertArrayHasKey('Super Admin', $normalized['transform']['roles_readable']);
        $this->assertArrayHasKey('Account manager', $normalized['transform']['roles_readable']);
    }
}
