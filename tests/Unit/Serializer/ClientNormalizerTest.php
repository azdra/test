<?php

namespace App\Tests\Unit\Serializer;

use App\Entity\Client\Client;
use App\Serializer\ClientNormalizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ClientNormalizerTest extends TestCase
{
    protected function createConcreteNormalizer(?ObjectNormalizer $normalizer = null): ClientNormalizer
    {
        return new ClientNormalizer(
            $normalizer ?? $this->createMock(ObjectNormalizer::class)
        );
    }

    public function getValidClientData(): \Generator
    {
        yield [new Client()];
    }

    public function getInvalidClientData(): \Generator
    {
        yield [123];
        yield ['Client'];
        yield [true];
        yield [null];
        yield [new \stdClass()];
    }

    /**
     * @dataProvider getValidClientData
     */
    public function testNormalizerSupportsClient($client): void
    {
        $this->assertTrue($this->createConcreteNormalizer()->supportsNormalization($client));
    }

    /**
     * @dataProvider getInvalidClientData
     */
    public function testNormalizerDoesntSupportInvalidClient($data): void
    {
        $this->assertFalse($this->createConcreteNormalizer()->supportsNormalization($data));
    }
}
