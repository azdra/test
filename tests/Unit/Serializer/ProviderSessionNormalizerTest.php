<?php

namespace App\Tests\Unit\Serializer;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Serializer\ProviderSessionNormalizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ProviderSessionNormalizerTest extends TestCase
{
    protected function createConcreteNormalizer(?ObjectNormalizer $normalizer = null, string $registrationPageTemplateLogoUri = './'): ProviderSessionNormalizer
    {
        return new ProviderSessionNormalizer(
            $normalizer ?? $this->createMock(ObjectNormalizer::class), $registrationPageTemplateLogoUri
        );
    }

    public function getValidProviderSessionData(): \Generator
    {
        yield [new WebexMeeting((new WebexConfigurationHolder())->setClient(new Client()))];
        yield [new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client()))];
    }

    public function getInvalidProviderSessionData(): \Generator
    {
        yield [123];
        yield ['Session'];
        yield [true];
        yield [null];
        yield [new \stdClass()];
    }

    /**
     * @dataProvider getValidProviderSessionData
     */
    public function testNormalizerSupportsProviderSession($session): void
    {
        $this->assertTrue($this->createConcreteNormalizer()->supportsNormalization($session));
    }

    /**
     * @dataProvider getInvalidProviderSessionData
     */
    public function testNormalizerDoesntSupportInvalidSession($data): void
    {
        $this->assertFalse($this->createConcreteNormalizer()->supportsNormalization($data));
    }

    public function testNormalizerFillsInAnimatorsAndTraineesCount(): void
    {
        $objectNormalizer = $this->createMock(ObjectNormalizer::class);
        $objectNormalizer->expects($this->once())
            ->method('normalize')
            ->willReturn([]);

        $session = (new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client())))
            ->addParticipantRole((new ProviderParticipantSessionRole())->setRole(ProviderParticipantSessionRole::ROLE_ANIMATOR))
            ->addParticipantRole((new ProviderParticipantSessionRole())->setRole(ProviderParticipantSessionRole::ROLE_ANIMATOR))
            ->addParticipantRole((new ProviderParticipantSessionRole())->setRole(ProviderParticipantSessionRole::ROLE_TRAINEE))
            ->addParticipantRole((new ProviderParticipantSessionRole())->setRole(ProviderParticipantSessionRole::ROLE_TRAINEE))
            ->addParticipantRole((new ProviderParticipantSessionRole())->setRole(ProviderParticipantSessionRole::ROLE_TRAINEE))
            ->addParticipantRole((new ProviderParticipantSessionRole())->setRole(ProviderParticipantSessionRole::ROLE_TRAINEE));

        $normalized = $this->createConcreteNormalizer($objectNormalizer)->normalize($session->setId(0));
    }
}
