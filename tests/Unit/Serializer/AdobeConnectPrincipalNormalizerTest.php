<?php

namespace App\Tests\Unit\Serializer;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Serializer\AdobeConnectPrincipalNormalizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class AdobeConnectPrincipalNormalizerTest extends TestCase
{
    protected function createConcreteNormalizer(): AdobeConnectPrincipalNormalizer
    {
        return new AdobeConnectPrincipalNormalizer($this->createMock(ObjectNormalizer::class));
    }

    public function getValidAdobeConnectPrincipalData(): \Generator
    {
        yield [new AdobeConnectPrincipal(new AdobeConnectConfigurationHolder())];
    }

    public function getInvalidAdobeConnectPrincipalData(): \Generator
    {
        yield [123];
        yield ['Client'];
        yield [true];
        yield [null];
        yield [new \stdClass()];
        yield [new Client()];
    }

    /**
     * @dataProvider getValidAdobeConnectPrincipalData
     */
    public function testNormalizerSupportsClient($adobeConnectPrincipal): void
    {
        $this->assertTrue($this->createConcreteNormalizer()->supportsNormalization($adobeConnectPrincipal));
    }

    /**
     * @dataProvider getInvalidAdobeConnectPrincipalData
     */
    public function testNormalizerDoesntSupportInvalidClient($data): void
    {
        $this->assertFalse($this->createConcreteNormalizer()->supportsNormalization($data));
    }
}
