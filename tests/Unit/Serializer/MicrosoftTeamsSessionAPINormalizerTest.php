<?php

namespace App\Tests\Unit\Serializer;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsParticipant;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Serializer\MicrosoftTeamsSessionAPINormalizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class MicrosoftTeamsSessionAPINormalizerTest extends TestCase
{
    protected function createConcreteNormalizer(?ObjectNormalizer $normalizer = null): MicrosoftTeamsSessionAPINormalizer
    {
        return new MicrosoftTeamsSessionAPINormalizer(
            $normalizer ?? $this->createMock(ObjectNormalizer::class)
        );
    }

    public function getValidProviderSessionData(): \Generator
    {
        yield [new MicrosoftTeamsSession((new MicrosoftTeamsConfigurationHolder())->setClient(new Client()))];
    }

    public function getInvalidProviderSessionData(): \Generator
    {
        yield [123];
        yield ['Session'];
        yield [true];
        yield [null];
        yield [new \stdClass()];
        yield [new WebexMeeting((new WebexConfigurationHolder())->setClient(new Client()))];
        yield [new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client()))];
    }

    /**
     * @dataProvider getValidProviderSessionData
     */
    public function testNormalizerSupportsProviderSession($session): void
    {
        $this->assertTrue($this->createConcreteNormalizer()->supportsNormalization($session));
    }

    /**
     * @dataProvider getInvalidProviderSessionData
     */
    public function testNormalizerDoesntSupportInvalidSession($data): void
    {
        $this->assertFalse($this->createConcreteNormalizer()->supportsNormalization($data));
    }

    public function testNormalizeSessionWithoutParticipants(): void
    {
        $session = $this->createPartialMock(MicrosoftTeamsSession::class, ['getParticipantRoles']);
        $session->expects($this->never())
            ->method('getParticipantRoles');

        $session->setName('[Test] Alexandre Daubois')
            ->setRef('My ref')
            ->setDateStart(new \DateTime('tomorrow 2:00am'))
            ->setDateEnd(new \DateTime('tomorrow 3:00am'));
        $session->setMeetingIdentifier('my_identifier');

        $data = $this->createConcreteNormalizer()->normalize($session, context: ['groups' => 'microsoft_teams_session', 'include_participants' => false]);
        $this->assertSame('[Test] Alexandre Daubois', $data['subject']);
        $this->assertSame((new \DateTime('tomorrow 2:00am'))->format('c'), $data['startDateTime']);
        $this->assertSame((new \DateTime('tomorrow 3:00am'))->format('c'), $data['endDateTime']);
    }

    public function testNormalizeSessionWithParticipants(): void
    {
        $participant = (new MicrosoftTeamsParticipant((new MicrosoftTeamsConfigurationHolder())->setClient(new Client())))
            ->setUser(
                (new User())
                    ->setFirstName('Alexandre')
                    ->setLastName('Daubois')
                    ->setEmail('adaubois@norsys.fr')
            );

        $session = new MicrosoftTeamsSession((new MicrosoftTeamsConfigurationHolder())->setClient(new Client()));
        $session->setName('[Test] Alexandre Daubois')
            ->setRef('My ref')
            ->setDateStart(new \DateTime('tomorrow 2:00am'))
            ->setDateEnd(new \DateTime('tomorrow 3:00am'))
            ->setLicence('licence@gmail.com');
        $session->setMeetingIdentifier('my_identifier');

        $role = new ProviderParticipantSessionRole();
        $role->setSession($session)
            ->setParticipant($participant);

        $session->addParticipantRole($role);

        $data = $this->createConcreteNormalizer()->normalize($session, context: ['groups' => 'microsoft_teams_session', 'include_participants' => true]);

        $this->assertSame('[Test] Alexandre Daubois', $data['subject']);
        $this->assertSame((new \DateTime('tomorrow 2:00am'))->format('c'), $data['startDateTime']);
        $this->assertSame((new \DateTime('tomorrow 3:00am'))->format('c'), $data['endDateTime']);
        $this->assertArrayHasKey('allowedPresenters', $data);
        $this->assertSame('roleIsPresenter', $data['allowedPresenters']);

        $this->assertArrayHasKey('participants', $data);
        $this->assertArrayHasKey('attendees', $data['participants']);
        $this->assertCount(1, $data['participants']['attendees']);
        $this->assertSame($participant->getEmail(), current($data['participants']['attendees'])['upn']);
        $this->assertSame(MicrosoftTeamsParticipant::ROLE_ATTENDEE, current($data['participants']['attendees'])['role']);

        $this->assertArrayHasKey('identity', current($data['participants']['attendees']));
        $this->assertArrayHasKey('user', current($data['participants']['attendees'])['identity']);
        $this->assertArrayHasKey('displayName', current($data['participants']['attendees'])['identity']['user']);
        $this->assertArrayHasKey('id', current($data['participants']['attendees'])['identity']['user']);
        $this->assertSame($participant->getFirstName().' '.$participant->getLastName(), current($data['participants']['attendees'])['identity']['user']['displayName']);
        $this->assertEmpty(current($data['participants']['attendees'])['identity']['user']['id']);
    }
}
