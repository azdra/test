<?php

namespace App\Tests\Unit\Service;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectTelephonyProfile;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Exception\InvalidArgumentException;
use App\Exception\ProviderServiceValidationException;
use App\Model\ProviderResponse;
use App\Repository\AdobeConnectPrincipalRepository;
use App\Repository\AdobeConnectSCORepository;
use App\Repository\AdobeConnectTelephonyProfileRepository;
use App\Repository\UserRepository;
use App\Service\AdobeConnectService;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Generator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class AdobeConnectServiceTest extends TestCase
{
    private MockObject|EntityManagerInterface                 $entityManager;
    private MockObject|ValidatorInterface                     $validator;
    private MockObject|AdobeConnectSCORepository              $adobeConnectSCORepository;
    private MockObject|AdobeConnectConnector $connector;
    private MockObject|AdobeConnectTelephonyProfileRepository $adobeConnectTelephonyProfileRepository;
    private MockObject|\Redis                                  $redisClient;
    private MockObject|SerializerInterface                    $serializer;
    private MockObject|LoggerInterface $logger;
    private MockObject|AdobeConnectPrincipalRepository $adobeConnectPrincipalRepository;
    private MockObject|UserRepository $userRepository;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->adobeConnectSCORepository = $this->createMock(AdobeConnectSCORepository::class);
        $this->connector = $this->createMock(AdobeConnectConnector::class);
        $this->adobeConnectTelephonyProfileRepository = $this->createMock(AdobeConnectTelephonyProfileRepository::class);
        $this->redisClient = $this->createMock(\Redis::class);
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->adobeConnectPrincipalRepository = $this->createMock(AdobeConnectPrincipalRepository::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->entityManager
            ->expects($this->any())
            ->method('getRepository')
            ->willReturnMap(
                [
                    [AdobeConnectSCO::class, $this->adobeConnectSCORepository],
                    [AdobeConnectPrincipal::class, $this->adobeConnectPrincipalRepository],
                ]
            );
    }

    public function getService(EntityManager $entityManager = null, array $methodsToMock = []): AdobeConnectService|MockObject
    {
        $service = $this->getMockBuilder(AdobeConnectService::class)
            ->onlyMethods($methodsToMock)
            ->setConstructorArgs([
                !empty($entityManager) ? $entityManager : $this->entityManager,
                $this->validator,
                $this->adobeConnectSCORepository,
                $this->connector,
                $this->adobeConnectTelephonyProfileRepository,
                $this->redisClient,
                $this->serializer,
                'test',
                $this->logger,
                $this->userRepository,
            ])
            ->getMock();

        return $service;
    }

    protected function createAdobeConnectConfigurationHolder(): AdobeConnectConfigurationHolder|MockObject
    {
        $configurationHolder = $this->getMockBuilder(AdobeConnectConfigurationHolder::class)
            ->onlyMethods(['getId'])
            ->getMock();

        $configurationHolder->expects($this->any())
            ->method('getId')
            ->willReturn(418);

        $configurationHolder
            ->setSubdomain('subdomain')
            ->setUsername('username')
            ->setPassword('password')
            ->setHostGroupScoId(123456789)
            ->setSavingMeetingFolderScoId(987654321)
            ->setClient(new Client());

        return $configurationHolder;
    }

    protected function getValidSCOForCreation(): AdobeConnectSCO
    {
        return (new AdobeConnectSCO($this->createAdobeConnectConfigurationHolder()))
            ->setName('My test')
            ->setDateStart(new DateTime('yesterday'))
            ->setDateEnd(new DateTime('tomorrow'))
            ->setRef('My reference');
    }

    protected function getValidPrincipal(): AdobeConnectPrincipal
    {
        return (new AdobeConnectPrincipal($this->createAdobeConnectConfigurationHolder()))
            ->setUser(
                (new User())
                    ->setFirstName('My test')
                    ->setLastName('Lastname')
                    ->setEmail('test@test.fr')
                    ->setClient(new Client())
                    ->setStatus(User::STATUS_ACTIVE)
            )->setConfigurationHolder($this->createAdobeConnectConfigurationHolder());
    }

    public function testFailSync()
    {
        $this->adobeConnectSCORepository->method('findOneBy')->willReturn(null);

        $this->entityManager->expects($this->never())->method('persist');

        $this->connector->expects($this->never())->method('call');

        $sco = $this->getValidSCOForCreation();

        $list = new ConstraintViolationList();
        $list->add($this->createMock(ConstraintViolation::class));

        $this->validator->expects($this->atLeastOnce())
            ->method('validate')
            ->with($sco, null, 'create')
            ->willReturn($list);

        $service = $this->getService();

        $this->expectException(ProviderServiceValidationException::class);
        $service->syncSession($sco);
    }

    public function testSyncToCreatePersistNewEntity(): void
    {
        $this->adobeConnectSCORepository->method('findOneBy')
            ->willReturn(null);

        $this->entityManager->expects($this->once())
            ->method('persist');

        $this->connector->expects($this->atMost(5))
            ->method('call')
            ->willReturn(new ProviderResponse());

        $sco = $this->getValidSCOForCreation();

        $this->validator->expects($this->atLeastOnce())
            ->method('validate')
            ->with($sco, null, 'create')
            ->willReturn(new ConstraintViolationList());

        $service = $this->getService(methodsToMock: ['getOrCreateScoFolder']);

        $service->expects($this->once())
                ->method('getOrCreateScoFolder')
                ->willReturn(123654789);

        $service->syncSession($sco);
    }

    public function testSyncToUpdateDoesntPersistNewEntity(): void
    {
        $this->adobeConnectSCORepository->method('findOneBy')
            ->willReturn(new AdobeConnectSCO($this->createAdobeConnectConfigurationHolder()));

        $this->entityManager->expects($this->never())
            ->method('persist');

        $this->connector->expects($this->atMost(3))
            ->method('call')
            ->willReturn((new ProviderResponse())->setSuccess(false));

        $sco = $this->getValidSCOForCreation()
            ->setUniqueScoIdentifier(123)
            ->setUrlPath('/test');

        $this->validator->expects($this->atLeastOnce())
            ->method('validate')
            ->with($sco, null, 'update')
            ->willReturn(new ConstraintViolationList());

        $service = $this->getService();
        $service->syncSession($sco);
    }

    public function testSyncCallsConnector(): void
    {
        $this->adobeConnectSCORepository->method('findOneBy')
            ->willReturn(new AdobeConnectSCO($this->createAdobeConnectConfigurationHolder()));

        $this->connector->expects($this->atLeastOnce())
            ->method('call');

        $this->validator->expects($this->atLeastOnce())
            ->method('validate')
            ->willReturn(new ConstraintViolationList());

        $service = $this->getService();
        $service->syncSession($this->getValidSCOForCreation());
    }

    public function testSuccessfulDeleteCallsEntityManager(): void
    {
        $response = (new ProviderResponse())
            ->setSuccess(true);

        $this->connector->expects($this->atLeastOnce())
            ->method('call')
            ->willReturn($response);

        $service = $this->getService();
        $service->deleteSession($this->getValidSCOForCreation());
    }

    public function testFailedDeleteNeverCallsEntityManager(): void
    {
        $response = (new ProviderResponse())
            ->setSuccess(false);

        $this->entityManager->expects($this->never())
            ->method('remove');

        $this->connector->expects($this->atLeastOnce())
            ->method('call')
            ->willReturn($response);

        $service = $this->getService();
        $service->deleteSession($this->getValidSCOForCreation());
    }

    public function testSuccessfulDeletePrincipalCallsEntityManager(): void
    {
        $response = (new ProviderResponse())
            ->setSuccess(true);

        $this->entityManager->expects($this->once())
            ->method('remove');

        $this->connector->expects($this->atLeastOnce())
            ->method('call')
            ->willReturn($response);

        $service = $this->getService();
        $service->deleteParticipant($this->getValidPrincipal());
    }

    public function testFailedDeletePrincipalNeverCallsEntityManager(): void
    {
        $response = (new ProviderResponse())
            ->setSuccess(false);

        $this->entityManager->expects($this->never())
            ->method('remove');

        $this->connector->expects($this->atLeastOnce())
            ->method('call')
            ->willReturn($response);

        $service = $this->getService();
        $service->deleteParticipant($this->getValidPrincipal());
    }

    public function testPermissionsUpdateCallsConnector(): void
    {
        $this->connector->expects($this->once())
            ->method('call');

        $sco = $this->getValidSCOForCreation()
            ->setUniqueScoIdentifier(123)
            ->setUrlPath('/test');

        $principal = $this->getValidPrincipal()
            ->setPrincipalIdentifier(123);

        $ppsr = (new ProviderParticipantSessionRole())
            ->setSession($sco)
            ->setParticipant($principal)
            ->setRole(ProviderParticipantSessionRole::ROLE_TRAINEE);

        $this->validator->expects($this->atLeastOnce())
            ->method('validate')
            ->with([$sco, $principal], [new Valid()], 'update')
            ->willReturn(new ConstraintViolationList());

        $service = $this->getService();
        $service->updateParticipantSessionRole($ppsr);
    }

    public function testPrincipalUpdatePersistNewlyCreatedEntity(): void
    {
        $this->connector->expects($this->once())
            ->method('call')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $this->adobeConnectPrincipalRepository->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $this->entityManager->expects($this->once())
            ->method('persist');

        $principal = $this->getValidPrincipal()
            ->setPrincipalIdentifier(123);

        $this->validator->expects($this->atLeastOnce())
            ->method('validate')
            ->with($principal, null, ['create'])
            ->willReturn(new ConstraintViolationList());

        $service = $this->getService();
        $service->syncPrincipal($principal, 'create');
    }

    public function testPrincipalUpdateDoesntPersistExistingEntity(): void
    {
        $this->connector->expects($this->once())
            ->method('call')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $this->adobeConnectPrincipalRepository->expects($this->once())
            ->method('findOneBy')
            ->willReturn(new AdobeConnectPrincipal($this->createAdobeConnectConfigurationHolder()));

        $this->entityManager->expects($this->never())
            ->method('persist');

        $principal = $this->getValidPrincipal()
            ->setPrincipalIdentifier(123);

        $this->validator->expects($this->atLeastOnce())
            ->method('validate')
            ->with($principal, null, ['update'])
            ->willReturn(new ConstraintViolationList());

        $service = $this->getService();
        $service->syncPrincipal($principal, 'update');
    }

    public function testPrincipalUpdateDoesntPersistOnFailedCall(): void
    {
        $this->connector->expects($this->once())
            ->method('call')
            ->willReturn((new ProviderResponse())->setSuccess(false));

        $this->entityManager->expects($this->never())
            ->method('persist');

        $principal = $this->getValidPrincipal()
            ->setPrincipalIdentifier(123);

        $this->validator->expects($this->atLeastOnce())
            ->method('validate')
            ->with($principal, null, ['create'])
            ->willReturn(new ConstraintViolationList());

        $service = $this->getService();
        $service->syncPrincipal($principal, 'create');
    }

    private function getDataReportMeetingAttendance(): array
    {
        $response = [
            0 => [
                'principal-id' => 6174962120,
                'sco-id' => 5349612830,
                'asset-id' => 6227025621,
                'login' => 'amelie.billay@grdf.fr',
                'session-name' => 'Amelie BILLAY',
                'sco-name' => 'Votre classe virtuelle',
                'date-created' => DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 11:44:03'),
                'date-end' => DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 12:01:25'),
                'participant-name' => 'Amelie BILLAY',
            ],
            1 => [
                'principal-id' => 5904294442,
                'sco-id' => 5349612830,
                'asset-id' => 6227025621,
                'login' => 'christine.milletdusastre@gilead.com',
                'session-name' => 'Christine MILLET-DUSASTRE',
                'sco-name' => 'Votre classe virtuelle',
                'date-created' => DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 10:30:51'),
                'date-end' => DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 12:01:22'),
                'participant-name' => 'Christine MILLET-DUSASTRE',
            ],
            2 => [
                'principal-id' => 6174914242,
                'sco-id' => 5349612830,
                'asset-id' => 6227025621,
                'login' => 'monique.brassie@assurance-maladie.fr',
                'session-name' => 'Monique BRASSIE',
                'sco-name' => 'Votre classe virtuelle',
                'date-created' => DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 10:32:51'),
                'date-end' => DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 10:33:54'),
                'participant-name' => 'Monique BRASSIE',
            ],
            3 => [
                'principal-id' => 6174914242,
                'sco-id' => 5349612830,
                'asset-id' => 6227025621,
                'login' => 'monique.brassie@assurance-maladie.fr',
                'session-name' => 'Monique BRASSIE',
                'sco-name' => 'Votre classe virtuelle',
                'date-created' => DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 10:30:53'),
                'date-end' => DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 10:30:54'),
                'participant-name' => 'Monique BRASSIE',
            ],
            4 => [
                'principal-id' => 6174914242,
                'sco-id' => 5349612830,
                'asset-id' => 6227025621,
                'login' => 'monique.brassie@assurance-maladie.fr',
                'session-name' => 'Monique BRASSIE',
                'sco-name' => 'Votre classe virtuelle',
                'date-created' => DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 10:31:30'),
                'date-end' => DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 12:01:36'),
                'participant-name' => 'Monique BRASSIE',
            ],
            5 => [
                'principal-id' => 6174962120,
                'sco-id' => 5349612830,
                'asset-id' => 6227025621,
                'login' => 'amelie.billay@grdf.fr',
                'session-name' => 'Amelie BILLAY',
                'sco-name' => 'Votre classe virtuelle',
                'date-created' => DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 10:29:58'),
                'date-end' => DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 11:23:31'),
                'participant-name' => 'Amelie BILLAY',
            ],
        ];

        return $response;
    }

    public function testReportMeetingAttendanceWithGetDataNotEmpty(): void
    {
        $providerResponse = new ProviderResponse();
        $providerResponse
            ->setData($this->getDataReportMeetingAttendance())
            ->setSuccess(true);

        $this->connector->expects($this->once())
            ->method('call')
            ->willReturn($providerResponse);

        $adobeConnectSCO = new AdobeConnectSCO($this->createAdobeConnectConfigurationHolder());
        $adobeConnectSCO
            ->setDateStart(DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 10:30:00'))
            ->setDateEnd(DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 12:30:00'));

        $service = $this->getService();

        $response = $service->reportMeetingAttendance($adobeConnectSCO);

        $this->assertIsArray($response->getData());
        $this->assertEquals(6, count($response->getData()));

        foreach ($response as $key => $value) {
            $this->assertNotEmpty($value['principal-id']);
            $this->assertNotEmpty($value['date-from']);
            $this->assertNotEmpty($value['date-to']);
        }
    }

    public function testReportMeetingAttendanceWithGetDataEmpty(): void
    {
        $providerResponse = new ProviderResponse();
        $providerResponse
            ->setData([])
            ->setSuccess(true);

        $this->connector->expects($this->once())
            ->method('call')
            ->willReturn($providerResponse);

        $adobeConnectSCO = new AdobeConnectSCO($this->createAdobeConnectConfigurationHolder());
        $adobeConnectSCO
            ->setDateStart(DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 10:30:00'))
            ->setDateEnd(DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-04 12:30:00'));

        $service = $this->getService();

        $response = $service->reportMeetingAttendance($adobeConnectSCO);

        $this->assertIsArray($response->getData());
        $this->assertEquals(0, count($response->getData()));
    }

    public function testReportActiveMeetingsCallsRepository(): void
    {
        $sco = new AdobeConnectSCO($this->createAdobeConnectConfigurationHolder());

        $this->adobeConnectSCORepository->expects($this->once())
            ->method('getActiveMeetings')
            ->willReturn([$sco]);

        $service = $this->getService();
        $this->assertSame([$sco], $service->reportActiveMeetings());
    }

    public function provideMethodsForValidationErrors(): Generator
    {
        yield ['syncSession', [new AdobeConnectSCO($this->createAdobeConnectConfigurationHolder())]];
        yield ['updateParticipantSessionRole', [
            (new ProviderParticipantSessionRole())
                ->setSession(new AdobeConnectSCO($this->createAdobeConnectConfigurationHolder()))
                ->setParticipant(new AdobeConnectPrincipal($this->createAdobeConnectConfigurationHolder()))
                ->setRole('permission'),
        ]];
        yield ['updateParticipant', [new AdobeConnectPrincipal($this->createAdobeConnectConfigurationHolder()), 'create']];
        yield ['updateParticipant', [new AdobeConnectPrincipal($this->createAdobeConnectConfigurationHolder()), 'update']];
    }

    /**
     * @dataProvider provideMethodsForValidationErrors
     */
    public function testValidatorExceptionNeverCallsConnector($method, array $parameters = []): void
    {
        $list = new ConstraintViolationList();
        $list->add($this->createMock(ConstraintViolation::class));

        $this->validator->expects($this->atLeastOnce())
            ->method('validate')
            ->willReturn($list);

        $this->expectException(ProviderServiceValidationException::class);

        $this->connector->expects($this->never())
            ->method('call');

        $service = $this->getService();
        $service->$method(...$parameters);
    }

    public function testGetPrincipalInHostGroupFailWithInvalidHostGroupId(): void
    {
        $configurationHolder = $this->createAdobeConnectConfigurationHolder();
        $configurationHolder->setHostGroupScoId(null);
        $adobeConnectService = $this->getService();

        $this->expectException(InvalidArgumentException::class);

        $adobeConnectService->getPrincipalInHostGroup($configurationHolder);
    }

    public function testGetPrincipalInHostGroupSuccessfully(): void
    {
        $configurationHolder = $this->createAdobeConnectConfigurationHolder();

        $this->connector->expects($this->once())
                  ->method('call')
                  ->willReturn(
                      (new ProviderResponse())
                          ->setSuccess(true)
                          ->setData([])
                  );

        $service = $this->getService();
        $response = $service->getPrincipalInHostGroup($configurationHolder);

        $this->assertTrue($response->isSuccess());
    }

    public function testGetSharedMeetingTemplatesSuccessfully(): void
    {
        $configurationHolder = $this->createAdobeConnectConfigurationHolder();

        $this->connector->expects($this->any())
                  ->method('call')
                  ->willReturn(new ProviderResponse(data: []));

        $service = $this->getService();
        $response = $service->getSharedMeetingsTemplate($configurationHolder);

        $this->assertTrue($response->isSuccess());
    }

    private function getValidAdobeConnectProfileTelephony(): array
    {
        $arrayAudio = [];
        $arrayAudio[] = (new AdobeConnectTelephonyProfile())
            ->setProfileIdentifier(3330016363)
            ->setConnectionType('InterCall')
            ->setName('FR_LIVESESSION_AUDIO_02_FVN')
            ->setPhoneLang('fr');

        $arrayAudio[] = (new AdobeConnectTelephonyProfile())
            ->setProfileIdentifier(1878965615)
            ->setConnectionType('InterCall')
            ->setName('FR_LIVESESSION_AUDIO_INT_01_LJA')
            ->setPhoneLang('fr');

        return $arrayAudio;
    }

    public function getDataSessions(): Generator
    {
        yield [[(new AdobeConnectSCO($this->createAdobeConnectConfigurationHolder()))
            ->setName('My test')
            ->setDateStart(new DateTime('2021-07-26 12:00:00'))
            ->setDateEnd(new DateTime('2021-07-26 14:00:00'))
            ->setRef('My reference')
            ->setProviderAudio($this->getValidAdobeConnectProfileTelephony()[0]), ]];

        yield [[(new AdobeConnectSCO($this->createAdobeConnectConfigurationHolder()))
            ->setName('My test')
            ->setDateStart(new DateTime('2021-07-27 09:00:00'))
            ->setDateEnd(new DateTime('2021-07-27 12:00:00'))
            ->setRef('My reference')
            ->setProviderAudio($this->getValidAdobeConnectProfileTelephony()[0]), ]];

        yield [[]];
    }

    /**
     * @dataProvider getDataSessions
     */
    public function testSuccessReturnArrayAudiosFree($sessions): void
    {
        $audio = $this->getValidAdobeConnectProfileTelephony();
        $configurationHolder = $this->createAdobeConnectConfigurationHolder();

        $dateBegin = DateTime::createFromFormat('Y-m-d H:i:s', '2021-07-27 12:00:00');
        $dateEnd = DateTime::createFromFormat('Y-m-d H:i:s', '2021-07-27 14:00:00');

        $this->adobeConnectSCORepository->expects($this->once())
            ->method('getSCOBetweenDateForAudioFromAdobeConnect')
            ->willReturn($sessions);

        $this->adobeConnectTelephonyProfileRepository->expects($this->once())
            ->method('findBy')
            ->with([
                'phoneLang' => 'fr',
                'connectionType' => AbstractProviderAudio::AUDIO_INTERCALL,
                'configurationHolder' => $configurationHolder,
            ])
            ->willReturn($audio);

        $service = $this->getService();

        $response = $service->getAvailableAudios($dateBegin, $dateEnd, AbstractProviderAudio::AUDIO_INTERCALL, 'fr', $configurationHolder);

        $this->assertTrue(is_array($sessions));
        if (empty($sessions)) {
            $this->assertEquals(2, count($response));
        } else {
            $this->assertEquals(1, count($response));
        }
    }

    private function generateFolderData(): array
    {
        $folder = [
            'uniq_identifier' => 987654321,
            'name' => 'MyFolderName',
        ];

        return [
            'target' => $folder,
            'parentFolderId' => 134679825,
        ];
    }

    public function testGetOrCreateAFolderOnAdobeConnectSuccessfullyWithAnExistingFolder(): void
    {
        $configurationHolder = $this->createAdobeConnectConfigurationHolder();
        $folderData = $this->generateFolderData();

        $folder = $folderData['target'];
        $parentFolderId = $folderData['parentFolderId'];

        $this->connector->expects($this->once())
                        ->method('call')
                        ->willReturn((new ProviderResponse(data: $folder['uniq_identifier'])));

        $service = $this->getService();
        $result = $service->getOrCreateScoFolder($configurationHolder, $parentFolderId, $folder['name']);

        $this->assertEquals($result, $folder['uniq_identifier']);
    }

    public function testGetOrCreateAFolderOnAdobeConnectSuccessfullyWithoutExistingFolder(): void
    {
        $configurationHolder = $this->createAdobeConnectConfigurationHolder();
        $folderData = $this->generateFolderData();

        $folder = $folderData['target'];
        $parentFolderId = $folderData['parentFolderId'];

        $this->connector->expects($this->exactly(2))
                        ->method('call')
                        ->willReturnOnConsecutiveCalls(
                            new ProviderResponse(data: null),
                            new ProviderResponse(data: $folder['uniq_identifier'])
                        );

        $service = $this->getService();
        $result = $service->getOrCreateScoFolder($configurationHolder, $parentFolderId, $folder['name']);

        $this->assertEquals($result, $folder['uniq_identifier']);
    }

    private function generateSavingScoIdList(): array
    {
        $folders = [];

        foreach (AdobeConnectConnector::ROOT_PATH_SAVE_MEETING as $folderName) {
            $folders[$folderName] = rand(100000000, 999999999);
        }

        $folders['_environment'] = rand(100000000, 999999999);

        return $folders;
    }

    public function testGetSavingFolderScoIdReturnTheLastScoId(): void
    {
        $configurationHolder = $this->createAdobeConnectConfigurationHolder();
        $foldersResponse = $this->generateSavingScoIdList();
        $service = $this->getService(methodsToMock: ['getOrCreateScoFolder']);

        $this->connector->expects($this->once())
                        ->method('call')
            ->willReturn(new ProviderResponse(data: 123456789));

        $service->expects($this->exactly(count($foldersResponse)))
            ->method('getOrCreateScoFolder')
            ->willReturnOnConsecutiveCalls(...$foldersResponse);

        $response = $service->getSavingFolderScoId($configurationHolder);

        $this->assertTrue($response->isSuccess());
        $this->assertEquals(end($foldersResponse), $response->getData());
    }
}
