<?php

namespace App\Tests\Unit\Service;

    // Import necessary classes
    use App\Entity\Client\Client;
    use App\Entity\GroupParticipant;
    use App\Repository\ClientRepository;
    use App\Repository\GroupParticipantRepository;
    use App\Repository\UserRepository;
    use App\Service\GroupParticipantService;
    use ErrorException;
    use PHPUnit\Framework\MockObject\MockObject;
    use PHPUnit\Framework\TestCase;
    use Psr\Log\LoggerInterface;

    class GroupParticipantServiceTest extends TestCase
    {
        private GroupParticipantService $groupParticipantService;

        // Declare mock objects for dependencies
        protected MockObject|GroupParticipant $groupParticipant;
        protected MockObject|ClientRepository $clientRepository;
        protected MockObject|GroupParticipantRepository $groupParticipantRepository;
        protected MockObject|UserRepository $userRepository;
        protected MockObject|LoggerInterface $logger;

        // Set up the test environment before each test
        public function setUp(): void
        {
            parent::setUp();
            // Initialize mock objects
            $this->clientRepository = $this->createMock(ClientRepository::class);
            $this->groupParticipantRepository = $this->createMock(GroupParticipantRepository::class);
            $this->userRepository = $this->createMock(UserRepository::class);
            $this->logger = $this->createMock(LoggerInterface::class);
            $this->groupParticipant = $this->createMock(GroupParticipant::class);
            // Initialize the service to be tested with the mock dependencies
            $this->groupParticipantService = new GroupParticipantService(
                $this->clientRepository,
                $this->groupParticipantRepository,
                $this->userRepository,
                $this->logger
            );
        }

        // Test the updateGroupParticipant method
        /**
         * @dataProvider dataProviderUpdateGroupParticipant
         */
        public function testUpdateGroupParticipant($data, $expectedResult): void
        {
            // Create a new instance of GroupParticipant with predefined properties
            $groupParticipant = (new GroupParticipant())
                ->setId(1)
                ->setName('Test Group')
                ->setClient((new Client())->setId(99)->setName('Initial client'))
                ->setParent(null)
                ->setActive(false);

            try {
                // Update the instance with the test data
                $groupParticipantResult = $this->groupParticipantService->updateGroupParticipant($groupParticipant, $data);

                // Verify that the instance properties have been updated as expected
                $this->assertSame($expectedResult['newName'], $groupParticipantResult->getName());
                $this->assertSame($expectedResult['newActive'], $groupParticipantResult->isActive());
                $this->assertSame($expectedResult['newParent'], $groupParticipantResult->getParent());
                $this->assertSame($expectedResult['newClient'], $groupParticipantResult->getClient());
            } catch (\Exception $errorException) {
                // Assert that an ErrorException is thrown in case of invalid data
                $this->assertInstanceOf(ErrorException::class, $errorException);
            }
        }

        public function dataProviderUpdateGroupParticipant(): array
        {
            // Create instances of Client for the test cases
            $firstClient = (new Client())->setId(1)->setName('First client');
            $secondClient = (new Client())->setId(2)->setName('Second client');
            $invalidClient = 'invalid client'; // Ce n'est pas une instance de Client

            return [
                // Test case for Group A with valid data
                'Group A' => [
                    'data' => [
                        'name' => 'Groupe A',
                        'client' => $firstClient,
                        'active' => true,
                    ],
                    'expectedResult' => [
                        'newName' => 'Groupe A',
                        'newActive' => true,
                        'newParent' => null,
                        'newClient' => $firstClient,
                    ],
                ],
                // Test case for Group B with valid data
                'Group B' => [
                    'data' => [
                        'name' => 'Groupe B',
                        'client' => $secondClient,
                        'parent' => null, // Remplacer par un mock approprié si nécessaire
                        'active' => true,
                    ],
                    'expectedResult' => [
                        'newName' => 'Groupe B',
                        'newActive' => true,
                        'newParent' => null,
                        'newClient' => $secondClient,
                    ],
                ],
                // Test case for invalid client data
                'Invalid Client' => [
                    'data' => [
                        'name' => 'Invalid Client Group',
                        'client' => $invalidClient,
                        'active' => true,
                    ],
                    'expectedResult' => [
                        'newName' => 'Test Group', // Nom initial, car la mise à jour doit échouer
                        'newActive' => false, // Valeur initiale
                        'newParent' => null, // Valeur initiale
                        'newClient' => $invalidClient, // Cela devrait rester inchangé ou lever une exception
                    ],
                ],
                // Test case for null client
                'Client Null' => [
                    'data' => [
                        'name' => 'Client Null',
                        'client' => null,
                        'active' => true,
                    ],
                    'expectedResult' => [
                        'newName' => 'Test Group',
                        'newActive' => false,
                        'newParent' => null,
                        'newClient' => $invalidClient,
                    ],
                ],
                // Test case for non-null and non-GroupParticipant parent
                'Parent not null & not GroupParticipant' => [
                    'data' => [
                        'name' => 'Client Null',
                        'client' => null,
                        'active' => true,
                        'parent' => 15,
                    ],
                    'expectedResult' => [
                        'newName' => 'Test Group',
                        'newActive' => false,
                        'newParent' => null,
                        'newClient' => $invalidClient,
                    ],
                ],
            ];
        }

        // Test the handleDataTransformation method
        public function testHandleDataTransformation(): void
        {
            // Create mock objects for client and group participant
            $clientMock = $this->createMock(Client::class);
            $groupParticipantMock = $this->createMock(GroupParticipant::class);
            // Data to be transformed
            $data = ['client' => 1, 'parent' => 2];

            // Expect the clientRepository to be called once with specific parameters
            $this->clientRepository->expects($this->once())
                                   ->method('findOneBy')
                                   ->with(['id' => 1])
                                   ->willReturn($clientMock);

            // Expect the groupParticipantRepository to be called once with specific parameters
            $this->groupParticipantRepository->expects($this->once())
                                             ->method('findOneBy')
                                             ->with(['id' => 2])
                                             ->willReturn($groupParticipantMock);

            // Test handleDataTransformation
            $this->groupParticipantService->handleDataTransformation($data, $groupParticipantMock);

            // Assert that the transformation is as expected
            $this->assertSame($clientMock, $data['client']);
            $this->assertSame($groupParticipantMock, $data['parent']);
        }

        // Test updateGroupParticipant with invalid data
        public function testUpdateGroupParticipantWithInvalidData(): void
        {
            // Expect an ErrorException to be thrown
            $this->expectException(ErrorException::class);

            // Invalid data for testing
            $invalidData = ['name' => 'Test Group', 'client' => 'invalid client'];

            // Test updateGroupParticipant with invalid data
            $this->groupParticipantService->updateGroupParticipant($this->groupParticipant, $invalidData);
        }

        // Test the isDataValid method
        public function testIsDataValid(): void
        {
            // Test case where 'client' is not an instance of Client
            $dataInvalidClient = ['client' => 'not a client instance'];
            $this->assertFalse($this->groupParticipantService->isDataValid($dataInvalidClient));

            // Test case where 'parent' is not null and not an instance of GroupParticipant
            $dataInvalidParent = ['client' => $this->createMock(Client::class), 'parent' => 'not a group participant instance'];
            $this->assertFalse($this->groupParticipantService->isDataValid($dataInvalidParent));
        }
    }
