<?php

namespace App\Tests\Unit\Service;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Exception\ProviderSessionAccessToken\InvalidProviderSessionAccessTokenException;
use App\Exception\ProviderSessionAccessToken\UnauthorizedProviderAccessTokenException;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Service\ProviderParticipantService;
use App\Service\ProviderParticipantSessionRoleService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ProviderParticipantSessionRoleServiceTest extends TestCase
{
    public const SECRET_KEY = 'mySecret';

    protected ProviderParticipantService|MockObject $providerParticipantSession;
    protected ProviderParticipantSessionRoleRepository|MockObject $providerParticipantSessionRoleRepository;

    public function setUp(): void
    {
        $this->providerParticipantSessionRoleRepository = $this->createMock(ProviderParticipantSessionRoleRepository::class);
        $this->providerParticipantSession = $this->createMock(ProviderParticipantService::class);
    }

    private function createProviderParticipantSessionRoleService(?array $decrytedData = null): ProviderParticipantSessionRoleService
    {
        $this->providerParticipantSession->expects($this->any())
            ->method('decryptSessionAccessToken')
            ->willReturn($decrytedData ?? [
                    'plop@gg.com',
                    'AwesomeReference418',
                    ProviderParticipantSessionRole::ROLE_ANIMATOR,
                ]);

        return new ProviderParticipantSessionRoleService(
            $this->providerParticipantSession,
            self::SECRET_KEY,
            $this->providerParticipantSessionRoleRepository
        );
    }

    private function createProviderSessionRole(
        ?string $providerParticipantEmail = null,
        ?string $providerSessionReference = null,
        ?string $role = null
    ): ProviderParticipantSessionRole {
        $providerSession = (new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client())))->setRef($providerSessionReference ?? 'AwesomeReference418');
        $providerParticipant = (new AdobeConnectPrincipal((new AdobeConnectConfigurationHolder())->setClient(new Client())))->setUser((new User())->setEmail($providerParticipantEmail ?? 'plop@gg.com')->setClient(new Client()));

        return (new ProviderParticipantSessionRole())
            ->setSession($providerSession)
            ->setParticipant($providerParticipant)
            ->setRole($role ?? ProviderParticipantSessionRole::ROLE_ANIMATOR);
    }

    public function testSessionAccessTokenIsValid(): void
    {
        $providerParticipantSessionRole = $this->createProviderSessionRole();

        $providerParticipantSessionRoleService = $this->createProviderParticipantSessionRoleService();

        $this->assertTrue($providerParticipantSessionRoleService->hasValidSessionAccessToken($providerParticipantSessionRole));
    }

    public function getInvalidProviderSessionRole(): \Generator
    {
        yield [$this->createProviderSessionRole('my.emayl@lol.com')];
        yield [$this->createProviderSessionRole('my.emayl@lol.com', 'theRefPowerful')];
        yield [$this->createProviderSessionRole('my.emayl@lol.com', role: ProviderParticipantSessionRole::ROLE_TRAINEE)];
        yield [$this->createProviderSessionRole('my.emayl@lol.com', role: ProviderParticipantSessionRole::ROLE_TRAINEE)];
        yield [$this->createProviderSessionRole('my.emayl@lol.com', 'theRefPowerful', ProviderParticipantSessionRole::ROLE_TRAINEE)];

        yield [$this->createProviderSessionRole(providerSessionReference: 'theRefPowerful')];
        yield [$this->createProviderSessionRole(providerSessionReference: 'theRefPowerful', role: ProviderParticipantSessionRole::ROLE_TRAINEE)];
        yield [$this->createProviderSessionRole(role: ProviderParticipantSessionRole::ROLE_TRAINEE)];
    }

    /** @dataProvider  getInvalidProviderSessionRole */
    public function testSessionAccessTokenIsInvalid(ProviderParticipantSessionRole $providerParticipantSessionRole): void
    {
        $providerParticipantSessionRoleService = $this->createProviderParticipantSessionRoleService();

        $this->expectException(InvalidProviderSessionAccessTokenException::class);

        $providerParticipantSessionRoleService->hasValidSessionAccessToken($providerParticipantSessionRole);
    }

    public function getValidProviderParticipantSessionRole(): \Generator
    {
        yield [
            $this->createProviderSessionRole(role: ProviderParticipantSessionRole::ROLE_ANIMATOR),
            new \DateTime(),
        ];
        yield [
            $this->createProviderSessionRole(role: ProviderParticipantSessionRole::ROLE_TRAINEE),
            (new \DateTime())->modify('- 10min'),
        ];
        yield [
            $this->createProviderSessionRole(role: ProviderParticipantSessionRole::ROLE_TRAINEE),
            (new \DateTime())->modify('+ 10min'),
        ];
        yield [
            $this->createProviderSessionRole(role: ProviderParticipantSessionRole::ROLE_TRAINEE),
            (new \DateTime())->modify('+ 30min'),
        ];
    }

    /** @dataProvider  getValidProviderParticipantSessionRole */
    public function testSuccessOfAuthorizedToAccessToTheSession(ProviderParticipantSessionRole $providerParticipantSessionRole, \DateTime $date): void
    {
        $providerParticipantSessionRoleService = $this->createProviderParticipantSessionRoleService([
            $providerParticipantSessionRole->getParticipant()->getEmail(),
            $providerParticipantSessionRole->getSession()->getRef(),
            $providerParticipantSessionRole->getRole(),
        ]);

        if ($providerParticipantSessionRole->isATrainee()) {
            $providerParticipantSessionRole->getSession()->setDateStart($date);
        }

        $this->assertTrue($providerParticipantSessionRoleService->isAuthorizedToAccessToTheSession($providerParticipantSessionRole));
    }

    public function testFailureOfAuthorizedToAccessToTheSession(): void
    {
        $providerParticipantSessionRole = $this->createProviderSessionRole(role: ProviderParticipantSessionRole::ROLE_TRAINEE);
        $providerParticipantSessionRole->getSession()->setDateStart((new \DateTime())->modify('+50min'));

        $providerParticipantSessionRoleService = $this->createProviderParticipantSessionRoleService([
            $providerParticipantSessionRole->getParticipant()->getEmail(),
            $providerParticipantSessionRole->getSession()->getRef(),
            $providerParticipantSessionRole->getRole(),
        ]);

        $this->expectException(UnauthorizedProviderAccessTokenException::class);

        $providerParticipantSessionRoleService->isAuthorizedToAccessToTheSession($providerParticipantSessionRole);
    }
}
