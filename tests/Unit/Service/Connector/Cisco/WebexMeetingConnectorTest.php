<?php

namespace App\Tests\Unit\Service\Connector\Cisco;

use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexParticipant;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Exception\ProviderGenericException;
use App\Repository\WebexParticipantRepository;
use App\Service\Connector\Cisco\WebexMeetingConnector;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twig\Environment;

final class WebexMeetingConnectorTest extends TestCase
{
    private MockObject|HttpClientInterface $httpClient;
    private MockObject|WebexParticipantRepository $webexParticipantRepository;
    private MockObject|LoggerInterface $logger;
    private MockObject|LoggerInterface $providerLogger;

    public function setUp(): void
    {
        $this->httpClient = $this->createMock(HttpClientInterface::class);
        $this->webexParticipantRepository = $this->createMock(WebexParticipantRepository::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->providerLogger = $this->createMock(LoggerInterface::class);
    }

    private function getResponseContent(string $fileName): string
    {
        return file_get_contents(__DIR__."/../../../Fixtures/xml/$fileName");
    }

    public function getWebexConfigurationHolder(): WebexConfigurationHolder
    {
        return (new WebexConfigurationHolder())
            ->setUsername('site_name')
            ->setPassword('password')
            ->setSiteName('site_name')
            ->setClient(new Client());
    }

    public function generateUser(): User
    {
        return (new User())
            ->setFirstName('John')
            ->setLastName('DOE')
            ->setEmail('john.doe@gg.com');
    }

    /**
     * Returns a concrete connector, or a partially mocked connector if something is given in $onlyMockedMethods.
     */
    protected function createWebexMeetingConnector(array $onlyMockedMethods = []): WebexMeetingConnector|MockObject
    {
        if (empty($onlyMockedMethods)) {
            return new WebexMeetingConnector(
                $this->httpClient,
                $this->createMock(Environment::class),
                $this->webexParticipantRepository,
                $this->logger,
                $this->providerLogger
            )
            ;
        }

        return $this->getMockBuilder(WebexMeetingConnector::class)
            ->setConstructorArgs([
                $this->httpClient,
                $this->createMock(Environment::class),
                $this->webexParticipantRepository,
            ])
            ->onlyMethods($onlyMockedMethods)
            ->getMock();
    }

    protected function createMockResponse(int $statusCode, array $headers = [], ?string $content = null): MockObject|MockResponse
    {
        $response = $this->createMock(MockResponse::class);
        $response->method('getStatusCode')
            ->willReturn($statusCode);

        if ($content) {
            $response->method('getContent')
                ->willReturn($content);
        }

        if (!empty($headers)) {
            $response->method('getHeaders')
                ->willReturn($headers);
        }

        return $response;
    }

    private function getErrorXML(): string
    {
        return $this->getResponseContent('response.webex_meeting_error.xml');
    }

    private function createWebexMeeting(array $data = []): WebexMeeting
    {
        $webexMeeting = ( new WebexMeeting($data['configurationHolder'] ?? (new WebexConfigurationHolder())->setClient(new Client())) )
            ->setName('My session')
            ->setMeetingKey(rand(1000, 9999));

        if (!empty($data['licence'])) {
            $webexMeeting->setLicence($data['licence']);
        }

        $webexParticipant = ( new WebexParticipant($this->getWebexConfigurationHolder()) )
            ->setUser(( new User() )->setEmail($data['participant_email'] ?? 'email@gg.com'));

        $webexMeeting->addParticipantRole(
            (new ProviderParticipantSessionRole())
                ->setSession($webexMeeting)
                ->setParticipant($webexParticipant)
        );

        return $webexMeeting;
    }

    public function testSuccessfulCreateMeeting()
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: $this->getResponseContent('response.webex_meeting_create_meeting.xml'));

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        $connector = $this->createWebexMeetingConnector();
        $webexMeeting = $this->createWebexMeeting(['licence' => 'email@gg.com']);
        $response = $connector->call($this->getWebexConfigurationHolder(), WebexMeetingConnector::ACTION_CREATE_MEETING, $webexMeeting);

        $this->assertTrue($response->isSuccess());
        $this->assertSame(25583100664, $response->getData()->getMeetingKey());
        $this->assertSame('https://livesession38.webex.com/livesession38/j.php?MTID=m56e5e8c086a6683aec70530b10f34c85', $response->getData()->getHostICalendarUrl());
        $this->assertSame('https://livesession38.webex.com/livesession38/j.php?MTID=ma1f36c6a2a8022f60028a2396b73ff91', $response->getData()->getAttendeeICalendarUrl());
    }

    public function testSuccessfulSetMeeting()
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: $this->getResponseContent('response.webex_meeting_set_meeting.xml'));

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        $connector = $this->createWebexMeetingConnector();
        $webexMeeting = $this->createWebexMeeting(['licence' => 'email@gg.com']);
        $response = $connector->call($this->getWebexConfigurationHolder(), WebexMeetingConnector::ACTION_SET_MEETING, $webexMeeting);

        $this->assertTrue($response->isSuccess());
        $this->assertSame('https://livesession38.webex.com/livesession38/j.php?MTID=m56e5e8c086a6683aec70530b10f34c85', $response->getData()->getHostICalendarUrl());
        $this->assertSame('https://livesession38.webex.com/livesession38/j.php?MTID=ma1f36c6a2a8022f60028a2396b73ff91', $response->getData()->getAttendeeICalendarUrl());
    }

    public function getActionsForFailCreateOrUpdateData(): \Generator
    {
        yield [WebexMeetingConnector::ACTION_CREATE_MEETING];
        yield [WebexMeetingConnector::ACTION_SET_MEETING];
    }

    /** @dataProvider getActionsForFailCreateOrUpdateData */
    public function testFailureCreateWebexMeetingWithoutValidLicence(string $action): void
    {
        $response = $this->createMockResponse(
            Response::HTTP_OK,
            content: $this->getResponseContent('response.webex_meeting_error.xml')
        );

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        $connector = $this->createWebexMeetingConnector();
        $webexMeeting = $this->createWebexMeeting(['licence' => 'email@gg.com']);

        $response = $connector->call($this->getWebexConfigurationHolder(), $action, $webexMeeting);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testSuccessfulGetHostUrlMeeting()
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: $this->getResponseContent('response.webex_meeting_get_host_url_meeting.xml'));

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        $connector = $this->createWebexMeetingConnector();
        $response = $connector->call($this->getWebexConfigurationHolder(), WebexMeetingConnector::ACTION_GET_HOST_URL_MEETING, new WebexMeeting($this->getWebexConfigurationHolder()));

        $this->assertTrue($response->isSuccess());
        $this->assertSame('https://my-incredible-webex.cisco.com', $response->getData());
    }

    public function testSuccessfulGetJoinUrlMeeting()
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: $this->getResponseContent('response.webex_meeting_get_join_url_meeting.xml'));

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        $connector = $this->createWebexMeetingConnector();

        $participantRole = new ProviderParticipantSessionRole();
        $participantRole->setSession(new WebexMeeting($this->getWebexConfigurationHolder()))
            ->setParticipant((new WebexParticipant($this->getWebexConfigurationHolder()))->setUser($this->generateUser()));

        $response = $connector->call($this->getWebexConfigurationHolder(), WebexMeetingConnector::ACTION_GET_JOIN_URL_MEETING, $participantRole);

        $this->assertTrue($response->isSuccess());
        $this->assertSame('https://my-incredible-webex.cisco.com?AN=My Participant', $response->getData());
    }

    public function testSuccessfulDeleteMeeting()
    {
        $meeting = (new WebexMeeting($this->getWebexConfigurationHolder()))
            ->setMeetingKey(123456);

        $response = $this->createMockResponse(Response::HTTP_OK, content: $this->getResponseContent('response.webex_meeting_delete_meeting.xml'));

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        $connector = $this->createWebexMeetingConnector();
        $response = $connector->call($this->getWebexConfigurationHolder(), WebexMeetingConnector::ACTION_DELETE_MEETING, $meeting);

        $this->assertTrue($response->isSuccess());
        $this->assertInstanceOf(Crawler::class, $response->getData());
    }

    public function testListMeetingAttendeeReturnsNewAttendee()
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: $this->getResponseContent('response.webex_meeting_list_meeting_attendee.xml'));

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        $meeting = new WebexMeeting($this->getWebexConfigurationHolder());

        $connector = $this->createWebexMeetingConnector();
        $response = $connector->call($this->getWebexConfigurationHolder(), WebexMeetingConnector::ACTION_LIST_MEETING_ATTENDEE, $meeting);

        $this->assertTrue($response->isSuccess());
        $this->assertIsArray($response->getData());
    }

    public function getActionsDataForFailures(): \Generator
    {
        yield [WebexMeetingConnector::ACTION_CREATE_MEETING, $this->createWebexMeeting(['licence' => 'email@gg.com'])];
        yield [WebexMeetingConnector::ACTION_SET_MEETING, $this->createWebexMeeting(['licence' => 'email@gg.com'])];
        yield [WebexMeetingConnector::ACTION_DELETE_MEETING, $this->createWebexMeeting()];
        yield [WebexMeetingConnector::ACTION_GET_HOST_URL_MEETING, $this->createWebexMeeting()];
        yield [WebexMeetingConnector::ACTION_GET_JOIN_URL_MEETING, $this->getWebexParticipantroel()];
        yield [WebexMeetingConnector::ACTION_GET_CONNECTOR_JOIN_URL_MEETING, $this->createWebexMeeting()->getMeetingKey()];
        yield [WebexMeetingConnector::ACTION_LIST_MEETING_ATTENDEE, $this->createWebexMeeting()];
    }

    private function getWebexParticipantroel()
    {
        $participantRole = new ProviderParticipantSessionRole();
        $participantRole->setSession(new WebexMeeting($this->getWebexConfigurationHolder()))
            ->setParticipant((new WebexParticipant($this->getWebexConfigurationHolder()))->setUser($this->generateUser()));

        return $participantRole;
    }

    /**
     * @dataProvider getActionsDataForFailures
     */
    public function testActionDoesntReturn200(string $action, mixed $connectorData = null): void
    {
        $response = $this->createMockResponse(Response::HTTP_FORBIDDEN, content: $this->getErrorXML());

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        $connector = $this->createWebexMeetingConnector();

        $response = $connector->call($this->getWebexConfigurationHolder(), $action, $connectorData);

        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    /**
     * @dataProvider getActionsDataForFailures
     */
    public function testActionFails(string $action, mixed $connectorData = null): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: $this->getErrorXML());

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        $connector = $this->createWebexMeetingConnector();

        $response = $connector->call($this->getWebexConfigurationHolder(), $action, $connectorData);

        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
        $this->assertStringContainsString('There is a fatal error!', $response->getThrown()->getMessage());
    }

    public function testSuccessfulGetUser()
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: $this->getResponseContent('response.webex_meeting_get_user.xml'));

        $this->httpClient->expects($this->once())
                         ->method('request')
                         ->willReturn($response);

        $connector = $this->createWebexMeetingConnector();

        $participant = new WebexParticipant($this->getWebexConfigurationHolder());
        $participant->setUser(
            (new User())
                ->setEmail('john.doe@livesession.fr')
                ->setPassword('password')
        );

        $response = $connector->call($this->getWebexConfigurationHolder(), WebexMeetingConnector::ACTION_GET_USER, $participant);

        $this->assertTrue($response->isSuccess());
        /** @var WebexParticipant $participant */
        $participant = $response->getData();

        $this->assertInstanceOf(WebexParticipant::class, $participant);
    }
}
