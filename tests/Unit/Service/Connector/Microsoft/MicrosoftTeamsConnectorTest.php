<?php

namespace App\Tests\Unit\Service\Connector\Microsoft;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Exception\ProviderGenericException;
use App\Model\ProviderResponse;
use App\Serializer\MicrosoftTeamsSessionAPINormalizer;
use App\Service\Connector\Microsoft\MicrosoftTeamsConnector;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class MicrosoftTeamsConnectorTest extends TestCase
{
    private MockObject|HttpClientInterface $httpClient;
    private MockObject|\Redis $redis;
    private MockObject|MicrosoftTeamsSessionAPINormalizer $apiNormalizer;
    private MockObject|LoggerInterface $logger;
    private MockObject|LoggerInterface $providerLogger;

    public function setUp(): void
    {
        $this->httpClient = $this->createMock(HttpClientInterface::class);
        $this->redis = $this->createMock(\Redis::class);
        $this->apiNormalizer = $this->createMock(MicrosoftTeamsSessionAPINormalizer::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->providerLogger = $this->createMock(LoggerInterface::class);
    }

    public function getMicrosoftTeamsConfigurationHolder(): MicrosoftTeamsConfigurationHolder
    {
        return (new MicrosoftTeamsConfigurationHolder())
            ->setTokenEndpoint('')
            ->setTenantId('')
            ->setScope('')
            ->setGrantType('')
            ->setOrganizerEmail('')
            ->setOrganizerUniqueIdentifier('')
            ->setClientId('')
            ->setClientSecret('')
            ->setAutomaticLicensing(true)
            ->setLicences([])
            ->setClient(new Client())
        ;
    }

    /**
     * Returns a concrete connector, or a partially mocked connector if something is given in $onlyMockedMethods.
     */
    protected function createMicrosoftTeamsConnector(array $onlyMockedMethods = []): MicrosoftTeamsConnector|MockObject
    {
        if (empty($onlyMockedMethods)) {
            return new MicrosoftTeamsConnector(
                $this->httpClient,
                $this->redis,
                $this->apiNormalizer,
                $this->logger,
                $this->providerLogger
            );
        }

        return $this->getMockBuilder(MicrosoftTeamsConnector::class)
            ->setConstructorArgs([
                $this->httpClient,
                $this->redis,
                $this->apiNormalizer,
            ])
            ->onlyMethods($onlyMockedMethods)
            ->getMock();
    }

    protected function createMockResponse(int $statusCode, array $headers = [], ?string $content = null): MockObject|MockResponse
    {
        $response = $this->createMock(MockResponse::class);
        $response->method('getStatusCode')
            ->willReturn($statusCode);

        if ($content) {
            $response->method('getContent')
                ->willReturn($content);
        }

        if (!empty($headers)) {
            $response->method('getHeaders')
                ->willReturn($headers);
        }

        return $response;
    }

    protected function getValidSessionForCreation(): MicrosoftTeamsSession
    {
        return (new MicrosoftTeamsSession($this->getMicrosoftTeamsConfigurationHolder()))
            ->setName('My test')
            ->setRef('My ref')
            ->setDateStart(new \DateTime())
            ->setDateEnd(new \DateTime())
            ->setLicence('licence@gmail.com');
    }

    protected function getValidSessionForUpdate(): MicrosoftTeamsSession
    {
        return $this->getValidSessionForCreation()
            ->setMeetingIdentifier('my_identifier')
            ->setLicence('licence@gmail.com');
    }

    /*public function testLoginWithSessionInCache()
    {
        $this->redis->expects($this->once())
            ->method('exists')
            ->willReturn(true);

        $this->redis->expects($this->once())
            ->method('get')
            ->willReturn('my_token');

        $connector = $this->createMicrosoftTeamsConnector();

        $connector->call($this->getMicrosoftTeamsConfigurationHolder(), MicrosoftTeamsConnector::ACTION_GET_TOKEN);
    }*/

    public function testLoginWithValidCredentials()
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: file_get_contents(__DIR__.'/../../../Fixtures/json/microsoft_teams_token.json'));

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        /*$this->redis->expects($this->once())
            ->method('exists')
            ->willReturn(false);*/

        $this->redis->expects($this->atLeastOnce())
            ->method('set');

        $connector = $this->createMicrosoftTeamsConnector();

        $response = $connector->call($this->getMicrosoftTeamsConfigurationHolder(), MicrosoftTeamsConnector::ACTION_GET_TOKEN);
        $this->assertInstanceOf(ProviderResponse::class, $response);
        $this->assertTrue($response->isSuccess());
    }

    public function testLoginWithInvalidCredentials()
    {
        $response = $this->createMockResponse(Response::HTTP_BAD_REQUEST);

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        /*$this->redis->expects($this->once())
            ->method('exists')
            ->willReturn(false);*/

        $this->redis->expects($this->never())
            ->method('set');

        $connector = $this->createMicrosoftTeamsConnector();

        $response = $connector->call($this->getMicrosoftTeamsConfigurationHolder(), MicrosoftTeamsConnector::ACTION_GET_TOKEN);
        $this->assertInstanceOf(ProviderResponse::class, $response);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testSuccessfulCreateSession(): void
    {
        $loginResponse = $this->createMockResponse(Response::HTTP_OK, content: file_get_contents(__DIR__.'/../../../Fixtures/json/microsoft_teams_token.json'));

        $response = $this->createMockResponse(Response::HTTP_CREATED, content: file_get_contents(__DIR__.'/../../../Fixtures/json/microsoft_teams_create_session.json'));

        $userResponse = $this->createMockResponse(Response::HTTP_OK, content: json_encode(['id' => 14]));

        $this->httpClient->expects($this->exactly(4))
            ->method('request')
            ->willReturnOnConsecutiveCalls($loginResponse, $userResponse, $loginResponse, $response);

        $connector = $this->createMicrosoftTeamsConnector();

        /*$this->redis->expects($this->never())->method('exists')->willReturn(false);
        $this->redis->expects($this->once())->method('get')->willReturn('session');*/

        $session = $this->getValidSessionForCreation();
        $response = $connector->call($this->getMicrosoftTeamsConfigurationHolder(), MicrosoftTeamsConnector::ACTION_SESSION_CREATE, $session);

        $this->assertTrue($response->isSuccess());
        $this->assertSame('my_microsoft_meeting_id', $response->getData()->getMeetingIdentifier());
        $this->assertSame('my_microsoft_meeting_join_url', $response->getData()->getJoinWebUrl());
        $this->assertSame($session, $response->getData());
    }

    public function testSuccessfulUpdateSession(): void
    {
        $loginResponse = $this->createMockResponse(Response::HTTP_OK, content: file_get_contents(__DIR__.'/../../../Fixtures/json/microsoft_teams_token.json'));

        $response = $this->createMockResponse(Response::HTTP_OK, content: file_get_contents(__DIR__.'/../../../Fixtures/json/microsoft_teams_create_session.json'));

        $userResponse = $this->createMockResponse(Response::HTTP_OK, content: json_encode(['id' => 14]));

        $this->httpClient->expects($this->exactly(4))
            ->method('request')
            ->willReturnOnConsecutiveCalls($loginResponse, $userResponse, $loginResponse, $response);

        $connector = $this->createMicrosoftTeamsConnector();

        /*$this->redis->expects($this->never())->method('exists')->willReturn(false);
        $this->redis->expects($this->once())->method('get')->willReturn('session');*/

        $session = $this->getValidSessionForUpdate();
        $response = $connector->call($this->getMicrosoftTeamsConfigurationHolder(), MicrosoftTeamsConnector::ACTION_SESSION_UPDATE, $session);

        $this->assertTrue($response->isSuccess());
        $this->assertSame($session, $response->getData());
    }

    public function testSuccessfulDeleteSession(): void
    {
        $loginResponse = $this->createMockResponse(Response::HTTP_OK, content: file_get_contents(__DIR__.'/../../../Fixtures/json/microsoft_teams_token.json'));

        $response = $this->createMockResponse(Response::HTTP_NO_CONTENT);

        $userResponse = $this->createMockResponse(Response::HTTP_OK, content: json_encode(['id' => 14]));

        $this->httpClient->expects($this->exactly(4))
            ->method('request')
            ->willReturn($loginResponse, $userResponse, $loginResponse, $response);

        $connector = $this->createMicrosoftTeamsConnector();

        /*$this->redis->expects($this->never())->method('exists')->willReturn(false);
        $this->redis->expects($this->once())->method('get')->willReturn('session');*/

        $session = $this->getValidSessionForUpdate();
        $response = $connector->call($this->getMicrosoftTeamsConfigurationHolder(), MicrosoftTeamsConnector::ACTION_SESSION_DELETE, $session);

        $this->assertTrue($response->isSuccess());
        $this->assertSame($session, $response->getData());
    }

    public function getActionsDataForFailures(): \Generator
    {
        yield [MicrosoftTeamsConnector::ACTION_SESSION_CREATE, $this->getValidSessionForCreation()];
        yield [MicrosoftTeamsConnector::ACTION_SESSION_UPDATE, $this->getValidSessionForUpdate()];
        yield [MicrosoftTeamsConnector::ACTION_SESSION_DELETE, $this->getValidSessionForUpdate()];
    }

    /**
     * @dataProvider getActionsDataForFailures
     */
    public function testActionDoesntReturn200(string $action, mixed $connectorData = null): void
    {
        $loginResponse = $this->createMockResponse(Response::HTTP_OK, content: file_get_contents(__DIR__.'/../../../Fixtures/json/microsoft_teams_token.json'));

        $response = $this->createMockResponse(Response::HTTP_FORBIDDEN);

        $userResponse = $this->createMockResponse(Response::HTTP_OK, content: json_encode(['id' => 14]));

        $this->httpClient->expects($this->exactly(4))
            ->method('request')
            ->willReturn($loginResponse, $userResponse, $loginResponse, $response);

        $connector = $this->createMicrosoftTeamsConnector();

        $response = $connector->call($this->getMicrosoftTeamsConfigurationHolder(), $action, $connectorData);

        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }
}
