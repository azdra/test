<?php

namespace App\Tests\Unit\Service\Connector\Adobe;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectTelephonyProfile;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\User;
use App\Exception\AbstractProviderException;
use App\Exception\InvalidArgumentException;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\ProviderGenericException;
use App\Exception\UnexpectedProviderActionErrorException;
use App\Model\ProviderResponse;
use App\Repository\AdobeConnectMeetingAttendeeRepository;
use App\Repository\AdobeConnectPrincipalRepository;
use App\Repository\AdobeConnectSCORepository;
use App\Repository\AdobeConnectTelephonyProfileRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use App\Service\Factory\Adobe\AdobeConnectPrincipalFactory;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class AdobeConnectConnectorTest extends TestCase
{
    private MockObject|HttpClientInterface $httpClient;
    private MockObject|AdobeConnectSCORepository $adobeConnectScoRepository;
    private MockObject|AdobeConnectPrincipalRepository $adobeConnectPrincipalRepository;
    private MockObject|AdobeConnectMeetingAttendeeRepository $adobeConnectMeetingAttendeeRepository;
    private MockObject|AdobeConnectTelephonyProfileRepository $adobeConnectTelephonyProfileRepository;
    private MockObject|ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository;
    private MockObject|EntityManagerInterface $entityManager;
    private MockObject|AdobeConnectConnector $adobeConnectConnector;
    private MockObject|ValidatorInterface $validator;
    private MockObject|LoggerInterface $logger;
    private MockObject|LoggerInterface $providerLogger;
    private MockObject|\Redis $redis;

    public function setUp(): void
    {
        $this->httpClient = $this->createMock(HttpClientInterface::class);
        $this->adobeConnectScoRepository = $this->createMock(AdobeConnectSCORepository::class);
        $this->adobeConnectPrincipalRepository = $this->createMock(AdobeConnectPrincipalRepository::class);
        $this->adobeConnectMeetingAttendeeRepository = $this->createMock(AdobeConnectMeetingAttendeeRepository::class);
        $this->adobeConnectTelephonyProfileRepository = $this->createMock(AdobeConnectTelephonyProfileRepository::class);
        $this->providerParticipantSessionRoleRepository = $this->createMock(ProviderParticipantSessionRoleRepository::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->adobeConnectConnector = $this->createMock(AdobeConnectConnector::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->providerLogger = $this->createMock(LoggerInterface::class);
        $this->redis = $this->createMock(\Redis::class);
    }

    public function getAdobeConnectConfigurationHolder(): AdobeConnectConfigurationHolder
    {
        return (new AdobeConnectConfigurationHolder())
            ->setSubdomain('subdomain')
            ->setUsername('username')
            ->setPassword('password')
            ->setClient(new Client());
    }

    /**
     * Returns a concrete connector, or a partially mocked connector if something is given in $onlyMockedMethods.
     */
    protected function createAdobeConnectConnector(): AdobeConnectConnector|MockObject
    {
        return new AdobeConnectConnector(
            $this->httpClient,
            $this->redis,
            $this->adobeConnectScoRepository,
            new AdobeConnectPrincipalFactory($this->adobeConnectPrincipalRepository),
            $this->adobeConnectPrincipalRepository,
            $this->adobeConnectTelephonyProfileRepository,
            $this->adobeConnectMeetingAttendeeRepository,
            $this->providerParticipantSessionRoleRepository,
            $this->logger, $this->providerLogger);
    }

    protected function createMockResponse(int $statusCode, array $headers = [], ?string $content = null): MockObject|MockResponse
    {
        $response = $this->createMock(MockResponse::class);
        $response->method('getStatusCode')
            ->willReturn($statusCode);

        if ($content) {
            $response->method('getContent')
                ->willReturn($content);
        }

        if (!empty($headers)) {
            $response->method('getHeaders')
                ->willReturn($headers);
        }

        return $response;
    }

    public function testWithValidConfigurationHolder(): void
    {
        $connector = $this->createAdobeConnectConnector();

        $this->assertInstanceOf(ProviderResponse::class, $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_LOGIN));
    }

    public function testWithNotAdobeConnectConfigurationHolder(): void
    {
        $connector = $this->createAdobeConnectConnector();

        $adobeConfigHolder = $this->createMock(AdobeConnectConfigurationHolder::class);
        $adobeConfigHolder->expects($this->never())->method('isValid');

        $response = $connector->call(new WebexConfigurationHolder(), AdobeConnectConnector::ACTION_LOGIN);

        $this->assertInstanceOf(InvalidProviderConfigurationHandler::class, $response->getThrown());
    }

    public function testWithInvalidConfigurationHolder(): void
    {
        $connector = $this->createAdobeConnectConnector();

        $adobeConfigHolder = $this->createMock(AdobeConnectConfigurationHolder::class);
        $adobeConfigHolder->expects($this->exactly(1))->method('isValid')->willReturn(false);

        $response = $connector->call($adobeConfigHolder, AdobeConnectConnector::ACTION_LOGIN);

        $this->assertInstanceOf(InvalidProviderConfigurationHandler::class, $response->getThrown());
    }

    public function testLoginWithSessionInCache(): void
    {
        $this->redis->expects($this->once())
            ->method('exists')
            ->willReturn(true);

        $this->redis->expects($this->once())
            ->method('get')
            ->willReturn('my_breeze_session');

        $connector = $this->createAdobeConnectConnector();

        $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_LOGIN);
    }

    public function testLoginWithValidCredentials(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, ['set-cookie' => ['BREEZESESSION=My Breeze Session; Path=/']], <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <OWASP_CSRFTOKEN>
        <token>my_token</token>
    </OWASP_CSRFTOKEN>
</results>
XML);

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        $this->redis->expects($this->once())
            ->method('exists')
            ->willReturn(false);

        $this->redis->expects($this->atLeastOnce())
            ->method('set');

        $connector = $this->createAdobeConnectConnector();

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_LOGIN);
        $this->assertInstanceOf(ProviderResponse::class, $response);
        $this->assertTrue($response->isSuccess());
    }

    public function testLoginWithInvalidCredentials(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="no-data" subcode="user-suspended">
        <invalid field="action"/>
    </status>
</results>
XML);

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        $this->redis->expects($this->once())
            ->method('exists')
            ->willReturn(false);

        $connector = $this->createAdobeConnectConnector();

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_LOGIN);
        $this->assertInstanceOf(ProviderResponse::class, $response);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testLoginReturnsUnexpectedCode(): void
    {
        $response = $this->createMockResponse(Response::HTTP_FORBIDDEN);

        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn($response);

        $this->redis->expects($this->once())
            ->method('exists')
            ->willReturn(false);

        $connector = $this->createAdobeConnectConnector();

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_LOGIN);
        $this->assertInstanceOf(ProviderResponse::class, $response);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(UnexpectedProviderActionErrorException::class, $response->getThrown());
    }

    public function getValidRootFolderType(): \Generator
    {
        yield ['my-events'];
        yield ['shared-meeting-templates'];
    }

    public function getInvalidRootFolderType(): \Generator
    {
        yield ['my-meeting-templates'];
        yield [''];
        yield ['toto'];
        yield ['{}'];
    }

    /**
     * @dataProvider getValidRootFolderType
     */
    public function testGetRootFolderWithValidFolderType($folderType): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <shortcuts>
        <sco tree-id="720068036" sco-id="2803769833" type="my-events">
            <domain-name>http://livesession.adobeconnect.com</domain-name>
        </sco>
        <sco tree-id="789789789" sco-id="123456789" type="shared-meeting-templates">
            <domain-name>http://livesession.adobeconnect.com</domain-name>
        </sco>
        <sco tree-id="456456456" sco-id="987654321" type="shared-meeting-templates">
            <domain-name>http://livesession.adobeconnect.com</domain-name>
        </sco>
    </shortcuts>
</results>
XML);
        $this->httpClient->method('request')
               ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_SCO_ROOT_FOLDER, $folderType);

        $scoId = [
            'my-events' => 2803769833,
            'shared-meeting-templates' => 987654321,
        ];

        $this->assertTrue($response->isSuccess());
        $this->assertSame($scoId[$folderType], $response->getData());
    }

    /**
     * @dataProvider getInvalidRootFolderType
     */
    public function testGetRootFolderWithInvalidFolderType($data): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <shortcuts>
        <sco tree-id="720068036" sco-id="2803769833" type="my-events">
            <domain-name>http://livesession.adobeconnect.com</domain-name>
        </sco>
        <sco tree-id="789789789" sco-id="123456789" type="shared-meeting-templates">
            <domain-name>http://livesession.adobeconnect.com</domain-name>
        </sco>
        <sco tree-id="456456456" sco-id="987654321" type="shared-meeting-templates">
            <domain-name>http://livesession.adobeconnect.com</domain-name>
        </sco>
    </shortcuts>
</results>
XML);
        $this->httpClient->method('request')
               ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_SCO_ROOT_FOLDER, $data);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(AbstractProviderException::class, $response->getThrown());
    }

    public function testScoNameListFromValidParentSco(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <scos>
        <sco sco-id="4195307006" source-sco-id="4195306837" folder-id="2803769833" type="meeting" icon="event" display-seq="0" duration="3600" is-folder="0">
            <name>webinar test flora 2</name>
            <url-path>/test_flora/</url-path>
            <date-begin>2020-02-19T11:00:00.000+01:00</date-begin>
            <date-end>2020-02-19T12:00:00.000+01:00</date-end>
            <date-created>2020-02-18T09:20:19.793+01:00</date-created>
            <date-modified>2020-02-18T09:21:35.130+01:00</date-modified>
            <is-seminar>false</is-seminar>
        </sco>
    </scos>
</results>
XML);
        $this->httpClient->method('request')
               ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_SCO_NAME_LIST_FROM_PARENT_SCO, ['rootFolder' => 123456789, 'filter' => 'all']);
        $data = $response->getData();

        $this->assertTrue($response->isSuccess());
        $this->assertCount(1, $data);

        $first = reset($data);
        $this->assertArrayHasKey('uniq_identifier', $first);
        $this->assertArrayHasKey('name', $first);
        $this->assertSame(4195307006, $first['uniq_identifier']);
        $this->assertSame('webinar test flora 2', $first['name']);
    }

    public function testGetScoContentsWithNullIdentifier(): void
    {
        $connector = $this->createAdobeConnectConnector();

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_SCO_CONTENTS, new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()));
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(InvalidArgumentException::class, $response->getThrown());
    }

    public function testGetScoContentsWithInvalidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="na-access" subcode="user-suspended">
        <invalid field="action"/>
    </status>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_SCO_CONTENTS, new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()));
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(InvalidArgumentException::class, $response->getThrown());
    }

    public function testScoContentsReturnsMeetingsWithDates(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <scos>
        <sco sco-id="123" source-sco-id="456" folder-id="789" type="meeting" icon="meeting" display-seq="0" duration="3600" is-folder="0">
            <name>My super meeting</name>
            <url-path>/my-super-meeting/</url-path>
            <date-begin>2019-11-07T17:00:00.000+01:00</date-begin>
            <date-end>2019-11-07T18:00:00.000+01:00</date-end>
            <date-created>2019-11-07T16:00:00.000+01:00</date-created>
            <date-modified>2019-11-07T16:30:00.000+01:00</date-modified>
            <is-seminar>true</is-seminar>
        </sco>
    </scos>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $this->adobeConnectScoRepository->method('findOneBy')
            ->willReturn(null);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_SCO_CONTENTS, (new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()))->setUniqueScoIdentifier(123456));
        $this->assertTrue($response->isSuccess());

        $fetchedSco = $response->getData();
        $this->assertCount(1, $fetchedSco);

        /** @var AdobeConnectSCO $meeting */
        $meeting = current($fetchedSco);
        $this->assertInstanceOf(AdobeConnectSCO::class, $meeting);
        $this->assertSame(AdobeConnectSCO::TYPE_MEETING, $meeting->getType());
        $this->assertSame('My super meeting', $meeting->getName());
        $this->assertSame('/my-super-meeting/', $meeting->getUrlPath());
        $this->assertEquals(new \DateTime('2019-11-07T17:00:00.000+01:00'), $meeting->getDateStart());
        $this->assertEquals(new \DateTime('2019-11-07T18:00:00.000+01:00'), $meeting->getDateEnd());
        $this->assertEquals(new \DateTime('2019-11-07T16:00:00.000+01:00'), $meeting->getCreatedAt());
        $this->assertEquals(new \DateTime('2019-11-07T16:30:00.000+01:00'), $meeting->getUpdatedAt());
        $this->assertTrue($meeting->isSeminar());
    }

    public function testScoContentsReturnsMeetingsWithoutDates(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <scos>
        <sco sco-id="123" source-sco-id="456" folder-id="789" type="meeting" icon="meeting" display-seq="0" duration="3600" is-folder="0">
            <name>My super meeting</name>
            <url-path>/my-super-meeting/</url-path>
            <date-created>2019-11-07T16:00:00.000+01:00</date-created>
            <date-modified>2019-11-07T16:30:00.000+01:00</date-modified>
            <is-seminar>true</is-seminar>
        </sco>
    </scos>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $this->adobeConnectScoRepository->method('findOneBy')
            ->willReturn(null);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_SCO_CONTENTS, (new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()))->setUniqueScoIdentifier(123456));
        $this->assertTrue($response->isSuccess());

        $fetchedSco = $response->getData();
        $this->assertCount(1, $fetchedSco);

        $meeting = current($fetchedSco);
        $this->assertInstanceOf(AdobeConnectSCO::class, $meeting);
        $this->assertSame(AdobeConnectSCO::TYPE_MEETING, $meeting->getType());
        $this->assertNull($meeting->getDateStart());
        $this->assertNull($meeting->getDateEnd());
    }

    public function testUpdateScoWithInvalidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="invalid" subcode="user-suspended">
        <invalid field="action"/>
    </status>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $parent = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());
        $parent->setUniqueScoIdentifier(456789)
            ->setType(AdobeConnectSCO::TYPE_FOLDER);

        $sco = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());
        $sco->setUniqueScoIdentifier(-1)
            ->setName('My super SCO')
            ->setDateStart(\DateTime::createFromFormat('Y-m-d H:i:s', '2021-11-13 10:30:00'))
            ->setDateEnd(\DateTime::createFromFormat('Y-m-d H:i:s', '2021-11-13 12:30:00'));

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_SCO_UPDATE, $sco);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testUpdateScoWithValidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <sco sco-id="123456">
        <url-path>My URL</url-path>
        <date-created>2019-11-07T16:00:00.000+01:00</date-created>
        <date-modified>2019-11-07T16:30:00.000+01:00</date-modified>
    </sco>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $parent = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());
        $parent->setUniqueScoIdentifier(456789)
            ->setType(AdobeConnectSCO::TYPE_FOLDER);

        $sco = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());
        $sco
            ->setName('My super SCO')
            ->setDateStart(\DateTime::createFromFormat('Y-m-d H:i:s', '2021-11-13 10:30:00'))
            ->setDateEnd(\DateTime::createFromFormat('Y-m-d H:i:s', '2021-11-13 12:30:00'));

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_SCO_UPDATE, $sco);
        $this->assertTrue($response->isSuccess());

        /** @var AdobeConnectSCO $updatedSco */
        $updatedSco = $response->getData();
        $this->assertSame('My URL', $updatedSco->getUrlPath());
        $this->assertEquals(new \DateTime('2019-11-07T16:00:00.000+01:00'), $updatedSco->getCreatedAt());
        $this->assertEquals(new \DateTime('2019-11-07T16:30:00.000+01:00'), $updatedSco->getUpdatedAt());
    }

    public function testDeleteScoWithInvalidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="invalid" subcode="user-suspended">
        <invalid field="action"/>
    </status>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client()));
        $sco->setName('My super SCO');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_SCO_DELETE, $sco);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testDeleteScoWithValidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client()));
        $sco->setName('My super SCO');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_SCO_DELETE, $sco);
        $this->assertTrue($response->isSuccess());
    }

    public function testUpdateUrlPathWithInvalidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="invalid" subcode="format">
        <invalid field="action"/>
    </status>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());
        $sco->setUrlPath('/$*#@é');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_SCO_DELETE, $sco);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testUpdateUrlPathWithValidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());
        $sco->setUrlPath('/my-test-url/');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_SCO_DELETE, $sco);
        $this->assertTrue($response->isSuccess());
        $this->assertSame('/my-test-url/', $response->getData()->getUrlPath());
    }

    public function testGetPermissionsInfoWithNullScoIdentifier(): void
    {
        $connector = $this->createAdobeConnectConnector();

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PERMISSIONS_INFO, new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()));
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(InvalidArgumentException::class, $response->getThrown());
    }

    public function testGetPermissionsInfoWithInvalidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="invalid" subcode="user-suspended">
        <invalid field="action"/>
    </status>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());
        $sco->setUniqueScoIdentifier(-1)
            ->setName('My super SCO');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PERMISSIONS_INFO, $sco);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testGetPermissionsInfoForUserWithValidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <permissions>
        <principal principal-id="123456" is-primary="false" type="learners" has-children="false" permission-id="" training-group-id="">
            <name>My principal</name>
            <login>my-principal</login>
        </principal>
    </permissions>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());
        $sco->setUniqueScoIdentifier(132)
            ->setName('My super SCO');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PERMISSIONS_INFO, $sco);
        $this->assertTrue($response->isSuccess());

        /** @var AdobeConnectPrincipal $principal */
        $principal = current($response->getData());
        $this->assertInstanceOf(AdobeConnectPrincipal::class, $principal);
        $this->assertSame(123456, $principal->getPrincipalIdentifier());
        $this->assertSame('my-principal', $principal->getEmail());
        $this->assertSame('My principal', $principal->getName());
    }

    public function testPrincipalListSuccessfulCall(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <principal-list>
        <principal principal-id="123456" is-primary="false" type="learners" has-children="false" permission-id="" training-group-id="">
            <name>My principal</name>
            <login>my-principal</login>
        </principal>
    </principal-list>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());
        $sco->setUniqueScoIdentifier(132)
            ->setName('My super SCO');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PRINCIPAL_LIST, []);
        $this->assertTrue($response->isSuccess());

        /** @var AdobeConnectPrincipal $principal */
        $principal = current($response->getData());
        $this->assertInstanceOf(AdobeConnectPrincipal::class, $principal);
        $this->assertSame(123456, $principal->getPrincipalIdentifier());
        $this->assertSame('my-principal', $principal->getEmail());
        $this->assertSame('My principal', $principal->getName());
    }

    public function testPrincipalListFilterByLoginNoResult(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <principal-list/>
    <system-groups-count>
        <system-count>10</system-count>
    </system-groups-count>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());
        $sco->setUniqueScoIdentifier(132)
            ->setName('My super SCO');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PRINCIPAL_LIST,
            [
                false,
                ['filter-login' => 'test@noexist.fr'],
            ]
        );
        $this->assertTrue($response->isSuccess());

        /* @var AdobeConnectPrincipal $principal */
        $this->assertSame([], $response->getData());
    }

    public function testPrincipalListSuccessfulCallAsGroup(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <principal-list>
         <principal do-not-display="0" principal-id="720460002" account-id="719989999" type="live-admins" has-children="true" is-primary="true" is-hidden="false" training-group-id="">
            <name>Hôtes de réunions</name>
            <login>Hôtes de réunions</login>
            <display-uid>Hôtes de réunions</display-uid>
            <description>Groupe d'hôtes de réunions</description>
        </principal>
    </principal-list>
</results>
XML);
        $this->httpClient->method('request')
               ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PRINCIPAL_LIST, [true]);
        $this->assertTrue($response->isSuccess());

        /** @var AdobeConnectPrincipal $principal */
        $principal = current($response->getData());
        $this->assertInstanceOf(AdobeConnectPrincipal::class, $principal);
        $this->assertSame(720460002, $principal->getPrincipalIdentifier());
        $this->assertSame('Hôtes de réunions', $principal->getName());
    }

    public function testGetPermissionsInfoForGroupDoesntCreateIt(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <permissions>
        <principal principal-id="123456" is-primary="false" type="event-group" has-children="true" permission-id="" training-group-id="">
        </principal>
    </permissions>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());
        $sco->setUniqueScoIdentifier(132)
            ->setName('My super SCO');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PERMISSIONS_INFO, $sco);
        $this->assertTrue($response->isSuccess());
        $this->assertEmpty($response->getData());
    }

    public function testGetPermissionsInfoWithStringPrincipalIdentifierIsSkipped(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <permissions>
        <principal principal-id="global-principal" is-primary="false" type="event-group" has-children="true" permission-id="" training-group-id="">
        </principal>
    </permissions>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());
        $sco->setUniqueScoIdentifier(132)
            ->setName('My super SCO');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PERMISSIONS_INFO, $sco);
        $this->assertTrue($response->isSuccess());
        $this->assertEmpty($response->getData());
    }

    public function testPermissionsUpdateWithInvalidPermissionVerb(): void
    {
        $connector = $this->createAdobeConnectConnector();
        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PERMISSIONS_UPDATE, [new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()), new AdobeConnectPrincipal($this->getAdobeConnectConfigurationHolder()), 'invalid-verb']);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(InvalidArgumentException::class, $response->getThrown());
        $this->assertStringContainsString('Permission verb is not valid', $response->getThrown()->getMessage());
    }

    public function testPermissionsUpdateWithValidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = (new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()))->setUniqueScoIdentifier(132456);
        $principal = new AdobeConnectPrincipal($this->getAdobeConnectConfigurationHolder());

        $sco->addPrincipal($principal);

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PERMISSIONS_UPDATE, [$sco, $principal, AdobeConnectConnector::PERMISSION_VERB_VIEW]);
        $this->assertTrue($response->isSuccess());
        $this->assertSame($principal, $response->getData());
    }

    public function testPermissionsUpdateWithInvalidParametersFromAdobeConnect(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="invalid" subcode="user-suspended">
        <invalid field="action"/>
    </status>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = (new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()))->setUniqueScoIdentifier(-1);
        $principal = new AdobeConnectPrincipal($this->getAdobeConnectConfigurationHolder());

        $sco->addPrincipal($principal);

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PERMISSIONS_UPDATE, [$sco, $principal, AdobeConnectConnector::PERMISSION_VERB_VIEW]);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testPermissionsResetWithInvalidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="invalid" subcode="user-suspended">
        <invalid field="action"/>
    </status>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = (new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()))->setUniqueScoIdentifier(-1);

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PERMISSIONS_RESET, $sco);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testPermissionsResetWithNullScoIdentifier(): void
    {
        $connector = $this->createAdobeConnectConnector();

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PERMISSIONS_RESET, new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()));
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(InvalidArgumentException::class, $response->getThrown());
        $this->assertStringContainsString('SCO identifier cannot be null', $response->getThrown()->getMessage());
    }

    public function testSuccessfulPermissionsReset(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = (new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()))->setUniqueScoIdentifier(132456);

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PERMISSIONS_RESET, $sco);
        $this->assertTrue($response->isSuccess());
    }

    public function testPrincipalUpdateWithInvalidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="invalid" subcode="user-suspended">
        <invalid field="action"/>
    </status>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $principal = (new AdobeConnectPrincipal($this->getAdobeConnectConfigurationHolder()))
            ->setPrincipalIdentifier(-1)
            ->setUser(
                (new User())
                    ->setFirstName('firstname')
                    ->setLastName('lastname')
                    ->setEmail('failure')
            );

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PRINCIPAL_UPDATE, $principal);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testPrincipalUpdateWithValidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <principal principal-id="123456" account-id="456789" type="user" has-children="0">
        <login>john.doe@live-session.fr</login>  
        <name>John Doe</name>  
    </principal>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $principal = new AdobeConnectPrincipal($this->getAdobeConnectConfigurationHolder());
        $principal->setUser(
            (new User())
                ->setFirstName('John')
                ->setLastName('Doe')
                ->setEmail('john.doe@live-session.fr')
        );

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PRINCIPAL_UPDATE, $principal);

        $this->assertTrue($response->isSuccess());
        $this->assertInstanceOf(AdobeConnectPrincipal::class, $response->getData());
        $this->assertSame(123456, $principal->getPrincipalIdentifier());
        $this->assertSame('John Doe', $principal->getName());
    }

    public function testPrincipalDelete(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $principal = new AdobeConnectPrincipal($this->getAdobeConnectConfigurationHolder());
        $principal
            ->setName('John Doe')
            ->setUser(
                (new User())
                    ->setFirstName('John')
                    ->setLastName('Doe')
                    ->setEmail('john.doe@live-session.fr')
            );

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_PRINCIPAL_DELETE, $principal);

        $this->assertTrue($response->isSuccess());
        $this->assertInstanceOf(AdobeConnectPrincipal::class, $response->getData());
        $this->assertSame('John Doe', $principal->getName());
    }

    public function testTelephonyProfileListWithInvalidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="invalid" subcode="user-suspended">
        <invalid field="action"/>
    </status>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $adobeConnectPrincipal = (new AdobeConnectPrincipal($this->getAdobeConnectConfigurationHolder()))
            ->setPrincipalIdentifier(-1);

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_TELEPHONY_PROFILE_LIST, $adobeConnectPrincipal);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testTelephonyProfileListWithValidParametersReturnsNewlyCreated(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <telephony-profiles>
        <profile profile-id="123456" provider-id="456789" profile-status="enabled">
            <adaptor-id>intercall-adaptor</adaptor-id>
            <name>InterCall</name>
            <profile-name>en_test_audio001</profile-name>
        </profile>
    </telephony-profiles>
</results>
XML);
        $response2 = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <telephony-profile profile-id="2502337416" provider-id="866232700" profile-status="enabled" hide-toll-free="false" provider-type="integrated">
        <adaptor-id>intercall-adaptor</adaptor-id>
        <provider-name>InterCall</provider-name>
        <profile-name>en_test_cegos_audio001</profile-name>
    </telephony-profile>
    <telephony-profile-fields disabled="" hide-toll-free="false" principal-id="1874022171" profile-id="2502337416" profile-status="enabled" provider-id="866232700">
        <x-tel-intercall-participant-code>8843044563</x-tel-intercall-participant-code>
        <x-tel-intercall-leader-pin>3416</x-tel-intercall-leader-pin>
        <x-tel-intercall-uv-conference-number>08082380274</x-tel-intercall-uv-conference-number>
    </telephony-profile-fields>
</results>
XML);

        $this->httpClient->method('request')
            ->willReturnOnConsecutiveCalls($response, $response2);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->exactly(2))->method('exists')->willReturn(true);
        $this->redis->expects($this->exactly(2))->method('get')->willReturn('session');

        $adobeConnectPrincipal = (new AdobeConnectPrincipal($this->getAdobeConnectConfigurationHolder()))
            ->setPrincipalIdentifier(456789);

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_TELEPHONY_PROFILE_LIST, $adobeConnectPrincipal);
        $this->assertTrue($response->isSuccess());
        $this->assertCount(1, $response->getData());

        /** @var AdobeConnectTelephonyProfile $profile */
        $profile = current($response->getData());
        $this->assertInstanceOf(AdobeConnectTelephonyProfile::class, $profile);
        $this->assertSame(123456, $profile->getProfileIdentifier());
        $this->assertSame('InterCall', $profile->getConnectionType());
        $this->assertSame('en_test_audio001', $profile->getName());

        $this->assertSame($profile, $adobeConnectPrincipal->getTelephonyProfiles()->first());
    }

    public function testTelephonyProfileListWithValidParametersDoesntReturnAlreadyExistingEntity(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <telephony-profiles>
        <profile profile-id="2502337416" provider-id="866232700" profile-status="enabled">
            <adaptor-id>intercall-adaptor</adaptor-id>
            <name>InterCall</name>
            <profile-name>en_test_cegos_audio001</profile-name>
        </profile>
    </telephony-profiles>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $this->adobeConnectTelephonyProfileRepository->expects($this->once())
            ->method('findOneBy')
            ->willReturn(new AdobeConnectTelephonyProfile());

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $adobeConnectPrincipal = (new AdobeConnectPrincipal($this->getAdobeConnectConfigurationHolder()))
            ->setPrincipalIdentifier(456789);

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_TELEPHONY_PROFILE_LIST, $adobeConnectPrincipal);
        $this->assertTrue($response->isSuccess());
        $this->assertEmpty($response->getData());
        $this->assertCount(1, $adobeConnectPrincipal->getTelephonyProfiles());
    }

    public function testPassingNullAdobeConnectPrincipalReturnsNewlyCreatedEntity(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <telephony-profiles>
        <profile profile-id="2502337416" provider-id="866232700" profile-status="enabled">
            <adaptor-id>intercall-adaptor</adaptor-id>
            <name>InterCall</name>
            <profile-name>en_test_cegos_audio001</profile-name>
        </profile>
    </telephony-profiles>
</results>
XML);
        $response2 = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <telephony-profile profile-id="2502337416" provider-id="866232700" profile-status="enabled" hide-toll-free="false" provider-type="integrated">
        <adaptor-id>intercall-adaptor</adaptor-id>
        <provider-name>InterCall</provider-name>
        <profile-name>en_test_cegos_audio001</profile-name>
    </telephony-profile>
    <telephony-profile-fields disabled="" hide-toll-free="false" principal-id="1874022171" profile-id="2502337416" profile-status="enabled" provider-id="866232700">
        <x-tel-intercall-participant-code>8843044563</x-tel-intercall-participant-code>
        <x-tel-intercall-leader-pin>3416</x-tel-intercall-leader-pin>
        <x-tel-intercall-uv-conference-number>08082380274</x-tel-intercall-uv-conference-number>
    </telephony-profile-fields>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturnOnConsecutiveCalls($response, $response2);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->exactly(2))->method('exists')->willReturn(true);
        $this->redis->expects($this->exactly(2))->method('get')->willReturn('session');

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_TELEPHONY_PROFILE_LIST);
        $this->assertTrue($response->isSuccess());
        $this->assertCount(1, $response->getData());
    }

    public function testReportMeetingAttendanceWithNullScoIdentifier(): void
    {
        $connector = $this->createAdobeConnectConnector();
        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_REPORT_MEETING_ATTENDANCE, new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()));

        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(InvalidArgumentException::class, $response->getThrown());
    }

    public function testReportMeetingAttendanceWithInvalidParameters(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="invalid" subcode="user-suspended">
        <invalid field="action"/>
    </status>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $sco = (new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()))
            ->setUniqueScoIdentifier(-1);

        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_REPORT_MEETING_ATTENDANCE, $sco);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testFindAudioWithValidParametersReturnIt(): void
    {
        $response = $this->createMockResponse(Response::HTTP_OK, content: <<<XML
<?xml version="1.0" encoding="utf-8"?>
<results>
    <status code="ok"/>
    <telephony-profiles>
        <profile profile-id="3330016363" provider-id="866232700" profile-status="enabled">
            <adaptor-id>intercall-adaptor</adaptor-id>
            <name>InterCall</name>
            <profile-name>FR_LIVESESSION_AUDIO_02_FVN</profile-name>
        </profile>
    </telephony-profiles>
</results>
XML);
        $this->httpClient->method('request')
            ->willReturn($response);

        $adobeConnectTelephonyProfile = (new AdobeConnectTelephonyProfile())
            ->setProfileIdentifier(3330016363);

        $this->adobeConnectTelephonyProfileRepository->expects($this->once())
            ->method('findOneBy')
            ->willReturn($adobeConnectTelephonyProfile);

        $connector = $this->createAdobeConnectConnector();

        $this->redis->expects($this->once())->method('exists')->willReturn(true);
        $this->redis->expects($this->once())->method('get')->willReturn('session');

        $adobeConnectPrincipal = (new AdobeConnectPrincipal($this->getAdobeConnectConfigurationHolder()))
            ->setPrincipalIdentifier(1874022171);
        $response = $connector->call($this->getAdobeConnectConfigurationHolder(), AdobeConnectConnector::ACTION_TELEPHONY_PROFILE_LIST, $adobeConnectPrincipal);

        $adobeConnectSCOWithAudio = (new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client())))
            ->setProviderAudio($adobeConnectTelephonyProfile);

        $adobeConnectSCOWithoutAudio = (new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client())))
            ->setProviderAudio(null);

        $this->assertTrue($response->isSuccess());
        $this->assertCount(1, $adobeConnectPrincipal->getTelephonyProfiles());
        $this->assertEquals($adobeConnectTelephonyProfile, $adobeConnectSCOWithAudio->getProviderAudio());
        $this->assertEmpty($adobeConnectSCOWithoutAudio->getProviderAudio());
    }
}
