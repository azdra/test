<?php

namespace App\Tests\Unit\Service\ConfigurationHolderFormHandler;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Exception\ConfigurationHolderViolationsException;
use App\Exception\ProviderGenericException;
use App\Model\Provider\AdobeConnect\AdobeConnectCommonInfo;
use App\Model\ProviderResponse;
use App\Service\AdobeConnectService;
use App\Service\ConfigurationHolderFormHandler\AdobeConnectConfigurationJsonHandler;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdobeConnectConfigurationJsonHandlerTest extends TestCase
{
    private MockObject|AdobeConnectConnector $adobeConnectConnector;
    private MockObject|AdobeConnectService $adobeConnectService;
    private MockObject|TranslatorInterface $translator;
    private MockObject|LoggerInterface $logger;

    public function setUp(): void
    {
        $this->adobeConnectConnector = $this->createMock(AdobeConnectConnector::class);
        $this->adobeConnectService = $this->createMock(AdobeConnectService::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
    }

    private function getHandler()
    {
        return new AdobeConnectConfigurationJsonHandler(
            $this->adobeConnectConnector,
            $this->adobeConnectService,
            $this->translator,
            $this->logger
        );
    }

    public function getCheckViolationsFailedCase(): \Generator
    {
        yield 'id not set' => ['id', 'unset', null];
        yield 'provider not set' => ['provider', 'unset', null];
        yield 'provider empty' => ['provider', 'empty', null];
        yield 'username not set' => ['username', 'unset', 'Connector ID'];
        yield 'username empty' => ['username', 'empty', 'Connector ID'];
        yield 'password not set' => ['password', 'unset', 'Password'];
        yield 'password empty' => ['password', 'empty', 'Password'];
        yield 'subdomain not set' => ['subdomain', 'unset', 'Subdomain'];
        yield 'subdomain empty' => ['subdomain', 'empty', 'Subdomain'];
        yield 'defaultConnexionType not set' => ['defaultConnexionType', 'unset', 'Default connection type'];
        yield 'defaultConnexionType empty' => ['defaultConnexionType', 'empty', 'Default connection type'];
        yield 'licenceType not set' => ['licenceType', 'unset', 'License type'];
        yield 'licenceType empty' => ['licenceType', 'empty', 'License type'];
    }

    /**
     * @dataProvider getCheckViolationsFailedCase
     */
    public function testCheckViolationsFailure(string $field, string $errorType, ?string $expectedTrans)
    {
        $configuration = $this->getConfiguration($field, $errorType);
        $handler = $this->getHandler();

        if (null === $expectedTrans) {
            $this->translator->expects($this->once())
                ->method('trans')
                ->with("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators')
                ->willReturn('Error');
        } else {
            $this->translator->expects($this->exactly(2))
                ->method('trans')
                ->withConsecutive([$expectedTrans], ["Field \"%field%\" canno't be empty", ['%field%' => 'field'], 'validators'])
                ->willReturnOnConsecutiveCalls('field', 'Error');
        }
        $violations = $handler->checkViolations($configuration);

        $this->assertEquals([$field => 'Error'], $violations);
    }

    public function testCheckViolationsSuccessfull()
    {
        $configuration = $this->getConfiguration();
        $handler = $this->getHandler();

        $this->translator->expects($this->never())
            ->method('trans');
        $violations = $handler->checkViolations($configuration);

        $this->assertEquals([], $violations);
    }

    public function testCreateConfigurationHolder()
    {
        $configuration = $this->getConfiguration();
        $handler = $this->getHandler();

        $configurationHolder = $handler->createConfigurationHolder($configuration);

        $this->assertInstanceOf(AdobeConnectConfigurationHolder::class, $configurationHolder);
    }

    public function testUpdateConfigurationHolder()
    {
        $configuration = $this->getConfiguration();
        $handler = $this->getHandler();
        $configurationHolder = $this->createPartialMock(AdobeConnectConfigurationHolder::class, ['getId']);
        $configurationHolder->method('getId')->willReturn(2);

        $handler->updateConfigurationHolder($configurationHolder, $configuration);

        $this->assertEquals('username', $configurationHolder->getUsername());
        $this->assertEquals('password', $configurationHolder->getPassword());
        $this->assertEquals('name', $configurationHolder->getName());
        $this->assertEquals('subdomain', $configurationHolder->getSubdomain());
        $this->assertEquals('defaultConnexionType', $configurationHolder->getDefaultConnexionType());
        $this->assertEquals('123', $configurationHolder->getLicenceType());
        $this->assertEquals('456', $configurationHolder->getDefaultRoom());
        $this->assertEquals('01/12/2021 16:30:28', $configurationHolder->getBirthday()->format('d/m/Y H:i:s'));
        $this->assertEquals(true, $configurationHolder->isStandardViewOnOpeningRoom());
        $licences = $configurationHolder->getLicences();
        $this->assertCount(1, $licences);
        $licence = $licences[0];
        $this->assertEquals(false, $licence['shared']);
        $this->assertEquals('pass', $licence['password']);
        $this->assertEquals(8, $licence['user_id']);
        $this->assertEquals('licence', $licence['name']);
        $this->assertArrayHasKey('tag', $licence);
        $this->assertArrayHasKey('iv', $licence);
    }

    public function testTestConnexionAndGetDefaultDataLoginFailed()
    {
        $configurationHolder = new AdobeConnectConfigurationHolder();
        $configurationHolder->setPassword('password');
        $loginResponse = new ProviderResponse();
        $loginResponse->setSuccess(false);
        $handler = $this->getHandler();

        $this->adobeConnectConnector->expects($this->once())
            ->method('call')
            ->with($configurationHolder, AdobeConnectConnector::ACTION_LOGIN)
            ->willReturn($loginResponse);

        $this->adobeConnectService->expects($this->never())
            ->method('getHostGroupPrincipal');

        $this->translator->expects($this->once())
            ->method('trans')
            ->with('Unable to connect to this provider')
            ->willReturn('Error');

        try {
            $handler->testConnexionAndGetDefaultData($configurationHolder);
        } catch (ConfigurationHolderViolationsException $exception) {
            $this->assertEquals(['global' => 'Error'], $exception->violations);

            return;
        }
        $this->fail('Failed asserting that exception of type "ConfigurationHolderViolationsException" is thrown.');
    }

    public function testTestConnexionAndGetDefaultDataGetHostGroupFailed()
    {
        $configurationHolder = new AdobeConnectConfigurationHolder();
        $configurationHolder->setPassword('password');
        $handler = $this->getHandler();

        $this->adobeConnectConnector->expects($this->once())
            ->method('call')
            ->with($configurationHolder, AdobeConnectConnector::ACTION_LOGIN)
            ->willReturn(new ProviderResponse());

        $this->adobeConnectService->expects($this->once())
            ->method('getHostGroupPrincipal')
            ->with($configurationHolder)
            ->willReturn(new ProviderResponse(false));

        $this->translator->expects($this->once())
            ->method('trans')
            ->with('Unable to find the host group')
            ->willReturn('Error');

        try {
            $handler->testConnexionAndGetDefaultData($configurationHolder);
        } catch (ConfigurationHolderViolationsException $exception) {
            $this->assertEquals(['global' => 'Error'], $exception->violations);

            return;
        }
        $this->fail('Failed asserting that exception of type "ConfigurationHolderViolationsException" is thrown.');
    }

    public function testTestConnexionAndGetDefaultDataGetSavingFolderScoIdFailed()
    {
        $configurationHolder = new AdobeConnectConfigurationHolder();
        $configurationHolder->setPassword('password');
        $hostGroupResponse = new ProviderResponse(data: [
            (new AdobeConnectPrincipal($configurationHolder))->setPrincipalIdentifier(123456),
        ]);
        $adminGroupResponse = new ProviderResponse(data: [
            (new AdobeConnectPrincipal($configurationHolder))->setPrincipalIdentifier(123456),
        ]);

        $handler = $this->getHandler();

        $this->adobeConnectConnector->expects($this->once())
                                    ->method('call')
                                    ->with($configurationHolder, AdobeConnectConnector::ACTION_LOGIN)
                                    ->willReturn(new ProviderResponse());

        $this->adobeConnectService->expects($this->once())
                                  ->method('getHostGroupPrincipal')
                                  ->with($configurationHolder)
                                  ->willReturn($hostGroupResponse);

        $this->adobeConnectService->expects($this->once())
                                  ->method('getAdminsGroupPrincipal')
                                  ->with($configurationHolder)
                                  ->willReturn($adminGroupResponse);

        $this->adobeConnectService->expects($this->once())
                                  ->method('getSavingFolderScoId')
                                  ->with($configurationHolder)
                                  ->willReturn(new ProviderResponse(false));

        $this->adobeConnectService->expects($this->never())
                                  ->method('getConnectorInformation');

        $this->translator->expects($this->once())
                         ->method('trans')
                         ->with('Unable to find the place to store sessions')
                         ->willReturn('Error');

        try {
            $handler->testConnexionAndGetDefaultData($configurationHolder);
        } catch (ConfigurationHolderViolationsException $exception) {
            $this->assertEquals(['global' => 'Error'], $exception->violations);

            return;
        }
        $this->fail('Failed asserting that exception of type "ConfigurationHolderViolationsException" is thrown.');
    }

    public function testTestConnexionAndGetDefaultDataOnProviderGenericException()
    {
        $configurationHolder = new AdobeConnectConfigurationHolder();
        $configurationHolder->setPassword('password');
        $handler = $this->getHandler();

        $this->adobeConnectConnector->expects($this->once())
            ->method('call')
            ->with($configurationHolder, AdobeConnectConnector::ACTION_LOGIN)
            ->willThrowException(new ProviderGenericException('class', 'error'));

        $this->adobeConnectService->expects($this->never())
            ->method('getHostGroupPrincipal');

        $this->translator->expects($this->once())
            ->method('trans')
            ->with('Unable to connect to this provider')
            ->willReturn('Error');

        try {
            $handler->testConnexionAndGetDefaultData($configurationHolder);
        } catch (ConfigurationHolderViolationsException $exception) {
            $this->assertEquals(['global' => 'Error'], $exception->violations);

            return;
        }
        $this->fail('Failed asserting that exception of type "ConfigurationHolderViolationsException" is thrown.');
    }

    public function testTestConnexionAndGetDefaultDataSuccess()
    {
        $configurationHolder = (new AdobeConnectConfigurationHolder())->setPassword('password');
        $hostGroupId = 12345;
        $adminGroupId = 12345;
        $hostGroupResponse = new ProviderResponse(data: [
            (new AdobeConnectPrincipal($configurationHolder))->setPrincipalIdentifier($hostGroupId),
        ]);
        $adminGroupResponse = new ProviderResponse(data: [
            (new AdobeConnectPrincipal($configurationHolder))->setPrincipalIdentifier($adminGroupId),
        ]);

        $savingFolderScoId = 123654789;
        $adobeConnectCommonInfo = new AdobeConnectCommonInfo(987654320, 'login@gg.com');
        $adobeConnectCommonInfoResponse = ( new ProviderResponse() )
            ->setSuccess(true)
            ->setData($adobeConnectCommonInfo);
        $handler = $this->getHandler();

        $this->adobeConnectConnector->expects($this->once())
            ->method('call')
            ->with($configurationHolder, AdobeConnectConnector::ACTION_LOGIN)
            ->willReturn(new ProviderResponse());

        $this->adobeConnectService->expects($this->once())
            ->method('getHostGroupPrincipal')
            ->with($configurationHolder)
            ->willReturn($hostGroupResponse);

        $this->adobeConnectService->expects($this->once())
                                  ->method('getAdminsGroupPrincipal')
                                  ->with($configurationHolder)
                                  ->willReturn($adminGroupResponse);

        $this->adobeConnectService->expects($this->once())
                                  ->method('getSavingFolderScoId')
                                  ->with($configurationHolder)
                                  ->willReturn(new ProviderResponse(data: $savingFolderScoId));

        $this->adobeConnectService->expects($this->once())
                                  ->method('getConnectorInformation')
                                  ->with($configurationHolder)
                                  ->willReturn($adobeConnectCommonInfoResponse);

        $this->translator->expects($this->never())
            ->method('trans');

        $handler->testConnexionAndGetDefaultData($configurationHolder);
        $this->assertEquals($hostGroupId, $configurationHolder->getHostGroupScoId());
        $this->assertEquals($adobeConnectCommonInfo->getUserId(), $configurationHolder->getUsernameScoId());
        $this->assertEquals($savingFolderScoId, $configurationHolder->getSavingMeetingFolderScoId());
    }

    private function getConfiguration(?string $field = null, ?string $method = null): array
    {
        $default = [
            'id' => 42,
            'provider' => 'adobe',
            'username' => 'username',
            'password' => 'password',
            'name' => 'name',
            'subdomain' => 'subdomain',
            'defaultConnexionType' => 'defaultConnexionType',
            'licenceType' => '123',
            'defaultRoom' => '456',
            'birthday' => '12/01/2021 16:30:28',
            'standardViewOnOpeningRoom' => true,
            'licences' => [['shared' => false, 'password' => 'pass', 'user_id' => 8, 'name' => 'licence']],
            'automaticLicensing' => false,
        ];

        if ($field === null) {
            return $default;
        }

        $configuration = [];
        foreach ($default as $key => $value) {
            if ($key === $field) {
                if ($method === 'empty') {
                    $configuration[$key] = '';
                } elseif ($method !== 'unset') {
                    $configuration[$key] = $value;
                }
            } else {
                $configuration[$key] = $value;
            }
        }

        return $configuration;
    }
}
