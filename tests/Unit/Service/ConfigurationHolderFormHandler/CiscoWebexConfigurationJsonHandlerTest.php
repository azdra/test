<?php

namespace App\Tests\Unit\Service\ConfigurationHolderFormHandler;

use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Exception\ConfigurationHolderViolationsException;
use App\Exception\ProviderGenericException;
use App\Model\ProviderResponse;
use App\Service\ConfigurationHolderFormHandler\CiscoWebexConfigurationJsonHandler;
use App\Service\WebexMeetingService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CiscoWebexConfigurationJsonHandlerTest extends TestCase
{
    private MockObject|WebexMeetingService $webexMeetingService;
    private MockObject|TranslatorInterface $translator;
    private MockObject|LoggerInterface $logger;

    public function setUp(): void
    {
        $this->webexMeetingService = $this->createMock(WebexMeetingService::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
    }

    public function getHandler(): CiscoWebexConfigurationJsonHandler
    {
        return new CiscoWebexConfigurationJsonHandler(
            $this->webexMeetingService,
            $this->translator,
            $this->logger
        );
    }

    public function getCheckViolationsFailedCase(): \Generator
    {
        yield 'id not set' => ['id', 'unset', null];
        yield 'provider not set' => ['provider', 'unset', null];
        yield 'provider empty' => ['provider', 'empty', null];
        yield 'username not set' => ['username', 'unset', 'Connector ID'];
        yield 'username empty' => ['username', 'empty', 'Connector ID'];
        yield 'password not set' => ['password', 'unset', 'Password'];
        yield 'password empty' => ['password', 'empty', 'Password'];
        yield 'siteName not set' => ['siteName', 'unset', 'Subdomain'];
        yield 'siteName empty' => ['siteName', 'empty', 'Subdomain'];
    }

    /**
     * @dataProvider getCheckViolationsFailedCase
     */
    public function testCheckViolationsFailure(string $field, string $errorType, ?string $expectedTrans)
    {
        $configuration = $this->getConfiguration($field, $errorType);
        $handler = $this->getHandler();

        if (null === $expectedTrans) {
            $this->translator->expects($this->once())
                ->method('trans')
                ->with("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators')
                ->willReturn('Error');
        } else {
            $this->translator->expects($this->exactly(2))
                ->method('trans')
                ->withConsecutive([$expectedTrans], ["Field \"%field%\" canno't be empty", ['%field%' => 'field'], 'validators'])
                ->willReturnOnConsecutiveCalls('field', 'Error');
        }
        $violations = $handler->checkViolations($configuration);

        $this->assertEquals([$field => 'Error'], $violations);
    }

    public function testCheckViolationsSuccessfull()
    {
        $configuration = $this->getConfiguration();
        $handler = $this->getHandler();

        $this->translator->expects($this->never())
            ->method('trans');
        $violations = $handler->checkViolations($configuration);

        $this->assertEquals([], $violations);
    }

    public function testCreateConfigurationHolder()
    {
        $configuration = $this->getConfiguration();
        $handler = $this->getHandler();

        $configurationHolder = $handler->createConfigurationHolder($configuration);

        $this->assertInstanceOf(WebexConfigurationHolder::class, $configurationHolder);
    }

    public function testUpdateConfigurationHolder()
    {
        $configuration = $this->getConfiguration();
        $handler = $this->getHandler();
        $configurationHolder = new WebexConfigurationHolder();

        $handler->updateConfigurationHolder($configurationHolder, $configuration);

        $this->assertEquals('username', $configurationHolder->getUsername());
        $this->assertEquals('password', $configurationHolder->getPassword());
        $this->assertEquals('name', $configurationHolder->getName());
        $this->assertEquals('siteName', $configurationHolder->getSiteName());
        $this->assertEquals(['optionJoinTeleconfBeforeHost' => 'true', 'optionOpenTime' => 123], $configurationHolder->getDefaultOptions());
    }

    public function testTestConnexionAndGetDefaultDataLoginFailed()
    {
        $configurationHolder = new WebexConfigurationHolder();
        $configurationHolder->setPassword('password');
        $loginResponse = new ProviderResponse();
        $loginResponse->setSuccess(false);
        $handler = $this->getHandler();

        $this->webexMeetingService->expects($this->once())
            ->method('loginUser')
            ->with($configurationHolder)
            ->willReturn($loginResponse);

        $this->translator->expects($this->once())
            ->method('trans')
            ->with('Unable to connect to this provider')
            ->willReturn('Error');

        try {
            $handler->testConnexionAndGetDefaultData($configurationHolder);
        } catch (ConfigurationHolderViolationsException $exception) {
            $this->assertEquals(['global' => 'Error'], $exception->violations);

            return;
        }
        $this->fail('Failed asserting that exception of type "ConfigurationHolderViolationsException" is thrown.');
    }

    public function testTestConnexionAndGetDefaultDataLoginThrow()
    {
        $configurationHolder = new WebexConfigurationHolder();
        $configurationHolder->setPassword('password');
        $handler = $this->getHandler();

        $this->webexMeetingService->expects($this->once())
            ->method('loginUser')
            ->with($configurationHolder)
            ->willThrowException(new ProviderGenericException('class', 'error'));

        $this->translator->expects($this->once())
            ->method('trans')
            ->with('Unable to connect to this provider')
            ->willReturn('Error');

        try {
            $handler->testConnexionAndGetDefaultData($configurationHolder);
        } catch (ConfigurationHolderViolationsException $exception) {
            $this->assertEquals(['global' => 'Error'], $exception->violations);

            return;
        }
        $this->fail('Failed asserting that exception of type "ConfigurationHolderViolationsException" is thrown.');
    }

    public function testTestConnexionAndGetDefaultDataSuccessfull()
    {
        $configurationHolder = new WebexConfigurationHolder();
        $configurationHolder->setPassword('password');
        $loginResponse = new ProviderResponse();
        $loginResponse->setSuccess(true);
        $handler = $this->getHandler();

        $this->webexMeetingService->expects($this->once())
            ->method('loginUser')
            ->with($configurationHolder)
            ->willReturn($loginResponse);

        $this->translator->expects($this->never())
            ->method('trans');

        $handler->testConnexionAndGetDefaultData($configurationHolder);
    }

    private function getConfiguration(?string $field = null, ?string $method = null): array
    {
        $default = [
            'id' => 42,
            'provider' => 'adobe',
            'name' => 'name',
            'username' => 'username',
            'password' => 'password',
            'siteName' => 'siteName',
            'optionJoinTeleconfBeforeHost' => 'true',
            'optionOpenTime' => '123',
            'automaticLicensing' => false,
            'safetyTime' => 0,
        ];

        if ($field === null) {
            return $default;
        }

        $configuration = [];
        foreach ($default as $key => $value) {
            if ($key === $field) {
                if ($method === 'empty') {
                    $configuration[$key] = '';
                } elseif ($method !== 'unset') {
                    $configuration[$key] = $value;
                }
            } else {
                $configuration[$key] = $value;
            }
        }

        return $configuration;
    }
}
