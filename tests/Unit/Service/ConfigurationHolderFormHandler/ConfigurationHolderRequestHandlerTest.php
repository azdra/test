<?php

namespace App\Tests\Unit\Service\ConfigurationHolderFormHandler;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Exception\ConfigurationHolderViolationsException;
use App\Exception\UnknownProviderConfigurationHolderException;
use App\Repository\ProviderConfigurationHolderRepository;
use App\Service\ConfigurationHolderFormHandler\AdobeConnectConfigurationJsonHandler;
use App\Service\ConfigurationHolderFormHandler\AdobeConnectWebinarConfigurationJsonHandler;
use App\Service\ConfigurationHolderFormHandler\CiscoWebexConfigurationJsonHandler;
use App\Service\ConfigurationHolderFormHandler\CiscoWebexRestConfigurationJsonHandler;
use App\Service\ConfigurationHolderFormHandler\ConfigurationHolderRequestHandler;
use App\Service\ConfigurationHolderFormHandler\MicrosoftTeamsConfigurationJsonHandler;
use App\Service\ConfigurationHolderFormHandler\MicrosoftTeamsEventConfigurationJsonHandler;
use App\Service\ConfigurationHolderFormHandler\WhiteConfigurationJsonHandler;
use App\Service\MiddlewareService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConfigurationHolderRequestHandlerTest extends TestCase
{
    private MockObject|AdobeConnectConfigurationJsonHandler $adobeConnectConfigurationJsonHandler;
    private MockObject|AdobeConnectWebinarConfigurationJsonHandler $adobeConnectWebinarConfigurationJsonHandler;
    private MockObject|CiscoWebexConfigurationJsonHandler $ciscoWebexConfigurationJsonHandler;
    private MockObject|CiscoWebexRestConfigurationJsonHandler $ciscoWebexRestConfigurationJsonHandler;
    private MockObject|MicrosoftTeamsConfigurationJsonHandler $microsoftTeamsConfigurationJsonHandler;
    private MockObject|MicrosoftTeamsEventConfigurationJsonHandler $microsoftTeamsEventConfigurationJsonHandler;
    private MockObject|WhiteConfigurationJsonHandler $whiteConfigurationJsonHandler;
    private MockObject|ProviderConfigurationHolderRepository $providerConfigurationHolderRepository;
    private MockObject|EntityManagerInterface $entityManager;
    private MockObject|TranslatorInterface $translator;
    private LoggerInterface $logger;

    public function setUp(): void
    {
        $this->adobeConnectConfigurationJsonHandler = $this->createMock(AdobeConnectConfigurationJsonHandler::class);
        $this->adobeConnectWebinarConfigurationJsonHandler = $this->createMock(AdobeConnectWebinarConfigurationJsonHandler::class);
        $this->ciscoWebexConfigurationJsonHandler = $this->createMock(CiscoWebexConfigurationJsonHandler::class);
        $this->ciscoWebexRestConfigurationJsonHandler = $this->createMock(CiscoWebexRestConfigurationJsonHandler::class);
        $this->microsoftTeamsConfigurationJsonHandler = $this->createMock(MicrosoftTeamsConfigurationJsonHandler::class);
        $this->microsoftTeamsEventConfigurationJsonHandler = $this->createMock(MicrosoftTeamsEventConfigurationJsonHandler::class);
        $this->whiteConfigurationJsonHandler = $this->createMock(WhiteConfigurationJsonHandler::class);
        $this->providerConfigurationHolderRepository = $this->createMock(ProviderConfigurationHolderRepository::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->translator->method('trans')->willReturn('error');
    }

    public function getHandler(): ConfigurationHolderRequestHandler
    {
        return new ConfigurationHolderRequestHandler(
            $this->adobeConnectConfigurationJsonHandler,
            $this->adobeConnectWebinarConfigurationJsonHandler,
            $this->ciscoWebexConfigurationJsonHandler,
            $this->ciscoWebexRestConfigurationJsonHandler,
            $this->microsoftTeamsConfigurationJsonHandler,
            $this->microsoftTeamsEventConfigurationJsonHandler,
            $this->whiteConfigurationJsonHandler,
            $this->providerConfigurationHolderRepository,
            $this->entityManager,
            $this->translator,
            $this->logger
        );
    }

    public function providerFactorySuccess(): \Generator
    {
        yield 'AdobeConnected' => [MiddlewareService::SERVICE_TYPE_ADOBE_CONNECT, AdobeConnectConfigurationJsonHandler::class];
        yield 'WebexMeeting' => [MiddlewareService::SERVICE_TYPE_CISCO_WEBEX, CiscoWebexConfigurationJsonHandler::class];
    }

    /**
     * @dataProvider providerFactorySuccess
     */
    public function testProviderConfiguratonJsonHandlerFactoryReturnGoodHandler(string $providerAsked, string $expectedHandler)
    {
        $handler = $this->getHandler();

        $actualHandler = $handler->providerConfigurationJsonHandlerFactory($providerAsked);

        $this->assertInstanceOf($expectedHandler, $actualHandler);
    }

    public function testProviderConfiguratonJsonHandlerFactoryThrowIfNotFound()
    {
        $handler = $this->getHandler();

        $this->expectException(UnknownProviderConfigurationHolderException::class);

        $handler->providerConfigurationJsonHandlerFactory('unknown_provider');
    }

    public function testHandleRequestNoDataThrow()
    {
        $request = $this->mockRequest();
        $request->method('getContent')->willReturn(null);
        $client = $this->getClient();
        $handler = $this->getHandler();

        $this->expectException(BadRequestException::class);

        $handler->handleRequest($client, $request);
    }

    public function testHandleRequestEmptyDataThrow()
    {
        $request = $this->mockRequest([]);
        $client = $this->getClient();
        $handler = $this->getHandler();

        $this->expectException(BadRequestException::class);

        $handler->handleRequest($client, $request);
    }

    public function testHandleRequestSingleConfigurationWithInvalidProvider()
    {
        $request = $this->mockRequest([['identifier' => 123, 'provider' => 'unknown_provider', 'archived' => false]]);
        $client = $this->getClient();
        $handler = $this->getHandler();

        try {
            $handler->handleRequest($client, $request);
        } catch (ConfigurationHolderViolationsException $exception) {
            $this->assertEquals([123 => ['global' => 'error']], $exception->violations);

            return;
        }
        $this->fail('Failed asserting that exception of type "ConfigurationHolderViolationsException" is thrown.');
    }

    public function testHandleRequestSingleConfigurationHasViolations()
    {
        $singleConfiguration = ['identifier' => 123, 'provider' => MiddlewareService::SERVICE_TYPE_CISCO_WEBEX, 'archived' => false];
        $request = $this->mockRequest([$singleConfiguration]);
        $client = $this->getClient();
        $handler = $this->getHandler();

        $this->expectsViolation($singleConfiguration);

        try {
            $handler->handleRequest($client, $request);
        } catch (ConfigurationHolderViolationsException $exception) {
            $this->assertEquals([123 => ['field_1' => 'error', 'field_2' => 'error']], $exception->violations);

            return;
        }
        $this->fail('Failed asserting that exception of type "ConfigurationHolderViolationsException" is thrown.');
    }

    public function testHandleRequestSingleConfigurationCreateIfNotExist()
    {
        $singleConfiguration = ['id' => 42, 'identifier' => 123, 'provider' => MiddlewareService::SERVICE_TYPE_CISCO_WEBEX, 'archived' => false];
        $request = $this->mockRequest([$singleConfiguration]);
        $client = $this->getClient();
        $handler = $this->getHandler();

        $this->expectsNoVioldations($singleConfiguration);
        $this->expectConfigurationHolderDoesNotExist($singleConfiguration);

        $handler->handleRequest($client, $request);
    }

    public function testHandleRequestSingleConfigurationUpdateIfExist()
    {
        $singleConfiguration = ['id' => 42, 'identifier' => 123, 'provider' => MiddlewareService::SERVICE_TYPE_CISCO_WEBEX, 'archived' => false];
        $request = $this->mockRequest([$singleConfiguration]);
        $client = $this->getClient();
        $handler = $this->getHandler();

        $this->expectsNoVioldations($singleConfiguration);
        $this->expectsConfigurationHolderExist($singleConfiguration);

        $handler->handleRequest($client, $request);
    }

    public function testHandleRequestSingleConfigurationTestConnexionFailed()
    {
        $singleConfiguration = ['id' => 42, 'identifier' => 123, 'provider' => MiddlewareService::SERVICE_TYPE_CISCO_WEBEX, 'archived' => false];
        $request = $this->mockRequest([$singleConfiguration]);
        $client = $this->getClient();
        $handler = $this->getHandler();

        $this->expectsNoVioldations($singleConfiguration);
        $configurationHolder = $this->expectsConfigurationHolderExist($singleConfiguration);
        $this->expectsTestConnectionFailed($configurationHolder);

        try {
            $handler->handleRequest($client, $request);
        } catch (ConfigurationHolderViolationsException $exception) {
            $this->assertEquals([123 => ['global' => 'error']], $exception->violations);

            return;
        }
        $this->fail('Failed asserting that exception of type "ConfigurationHolderViolationsException" is thrown.');
    }

    public function testHandleRequestDoNotRemoveConfigurationOfClient()
    {
        $singleConfiguration = ['id' => 42, 'identifier' => 123, 'provider' => MiddlewareService::SERVICE_TYPE_CISCO_WEBEX, 'archived' => false];
        $request = $this->mockRequest([$singleConfiguration]);
        $client = $this->getClient();
        $clientConfigurationHolder = $this->createMock(AbstractProviderConfigurationHolder::class);
        $clientConfigurationHolder->method('getId')->willReturn(42);
        $client->addConfigurationHolder($clientConfigurationHolder);
        $handler = $this->getHandler();

        $this->expectsNoVioldations($singleConfiguration);
        $configurationHolder = $this->expectsConfigurationHolderExist($singleConfiguration);
        $this->expectsTestConnectionSuccessfull($configurationHolder);

        $handler->handleRequest($client, $request);

        $this->assertNotEmpty($client->getConfigurationHolders()->toArray());
    }

    public function testHandleRequestRemoveConfigurationOfClient()
    {
        $singleConfiguration = ['id' => 42, 'identifier' => 123, 'provider' => MiddlewareService::SERVICE_TYPE_CISCO_WEBEX, 'archived' => false];
        $request = $this->mockRequest([$singleConfiguration]);
        $client = $this->getClient();
        $clientConfigurationHolder = $this->createMock(AbstractProviderConfigurationHolder::class);
        $clientConfigurationHolder->method('getId')->willReturn(69);
        $client->addConfigurationHolder($clientConfigurationHolder);
        $handler = $this->getHandler();

        $this->expectsNoVioldations($singleConfiguration);
        $configurationHolder = $this->expectsConfigurationHolderExist($singleConfiguration);
        $this->expectsTestConnectionSuccessfull($configurationHolder);

        $handler->handleRequest($client, $request);

        $this->assertEmpty($client->getConfigurationHolders()->toArray());
    }

    private function mockRequest(array $content = []): MockObject|Request
    {
        $request = $this->createMock(Request::class);
        $request->method('getContent')->willReturn(json_encode($content));

        return $request;
    }

    private function getClient(): Client
    {
        return new Client();
    }

    private function expectsViolation(array $configuration): array
    {
        $violations = ['field_1' => 'error', 'field_2' => 'error'];
        $this->ciscoWebexConfigurationJsonHandler->expects($this->once())
            ->method('checkViolations')
            ->with($configuration)
            ->willReturn($violations);

        return $violations;
    }

    private function expectsNoVioldations(array $configuration): array
    {
        $this->ciscoWebexConfigurationJsonHandler->expects($this->once())
        ->method('checkViolations')
        ->with($configuration)
        ->willReturn([]);

        return [];
    }

    private function expectConfigurationHolderDoesNotExist(array $configuration): AbstractProviderConfigurationHolder
    {
        $configurationHolder = new WebexConfigurationHolder();

        $this->providerConfigurationHolderRepository->expects($this->once())
            ->method('find')
            ->with($configuration['id'])
            ->willReturn(null);

        $this->ciscoWebexConfigurationJsonHandler->expects($this->once())
            ->method('createConfigurationHolder')
            ->with($configuration)
            ->willReturn($configurationHolder);

        $this->ciscoWebexConfigurationJsonHandler->expects($this->once())
            ->method('updateConfigurationHolder')
            ->with($configurationHolder, $configuration);

        return $configurationHolder;
    }

    private function expectsConfigurationHolderExist(array $configuration): AbstractProviderConfigurationHolder
    {
        $configurationHolder = new WebexConfigurationHolder();

        $this->providerConfigurationHolderRepository->expects($this->once())
            ->method('find')
            ->with($configuration['id'])
            ->willReturn($configurationHolder);

        $this->ciscoWebexConfigurationJsonHandler->expects($this->never())
            ->method('createConfigurationHolder');

        $this->ciscoWebexConfigurationJsonHandler->expects($this->once())
            ->method('updateConfigurationHolder')
            ->with($configurationHolder, $configuration);

        return $configurationHolder;
    }

    private function expectsTestConnectionFailed(AbstractProviderConfigurationHolder $configurationHolder): void
    {
        $this->ciscoWebexConfigurationJsonHandler->expects($this->once())
            ->method('testConnexionAndGetDefaultData')
            ->willThrowException(new ConfigurationHolderViolationsException(['global' => 'error']));
    }

    private function expectsTestConnectionSuccessfull(AbstractProviderConfigurationHolder $configurationHolder): void
    {
        $this->ciscoWebexConfigurationJsonHandler->expects($this->once())
            ->method('testConnexionAndGetDefaultData');
    }
}
