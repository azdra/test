<?php

namespace App\Tests\Unit\Service;

use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexMeetingTelephonyProfile;
use App\Entity\Cisco\WebexParticipant;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Exception\ProviderGenericException;
use App\Exception\ProviderServiceValidationException;
use App\Model\ProviderResponse;
use App\Service\Connector\Cisco\WebexMeetingConnector;
use App\Service\Provider\Webex\WebexLicensingService;
use App\Service\WebexMeetingService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class WebexMeetingServiceTest extends TestCase
{
    private MockObject|WebexMeetingConnector $webexMeetingConnector;
    private MockObject|EntityManagerInterface $entityManager;
    private MockObject|ValidatorInterface $validator;
    private WebexLicensingService $webexLicensingService;
    private LoggerInterface $logger;

    public function setUp(): void
    {
        $this->webexMeetingConnector = $this->createMock(WebexMeetingConnector::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->webexLicensingService = $this->createMock(WebexLicensingService::class);
        $this->logger = $this->createMock(LoggerInterface::class);
    }

    protected function createWebexSessionService(): WebexMeetingService
    {
        return new WebexMeetingService($this->entityManager, $this->validator, $this->webexLicensingService, $this->webexMeetingConnector, $this->logger);
    }

    protected function getValidParticipant(): WebexParticipant
    {
        $user = (new User())
            ->setFirstName('My test')
            ->setLastName('Lastname')
            ->setEmail('test@test.fr')
            ->setClient(new Client())
            ->setStatus(User::STATUS_ACTIVE);

        return (new WebexParticipant($this->createWebexConfigurationHolder()))
            ->setUser($user);
    }

    protected function createWebexConfigurationHolder(): WebexConfigurationHolder|MockObject
    {
        return (new WebexConfigurationHolder())
            ->setUsername('site_name')
            ->setPassword('password')
            ->setSiteName('site_name')
            ->setClient(new Client());
    }

    protected function getValidMeetingForCreation(): WebexMeeting
    {
        $session = new WebexMeeting($this->createWebexConfigurationHolder());
        $session->setName('Meeting')
            ->setId(rand(0000, 9999))
            ->setRef('For testing')
            ->setDateStart(new \DateTime('yesterday'))
            ->setDateEnd(new \DateTime('now'));

        return $session;
    }

    protected function getValidMeetingForUpdate(): WebexMeeting
    {
        return $this->getValidMeetingForCreation()
            ->setMeetingKey(123456);
    }

    public function testCreateWebexSession()
    {
        $session = $this->getValidMeetingForCreation();

        $this->validator->expects($this->once())->method('validate')
            ->willReturn(new ConstraintViolationList());

        $this->webexMeetingConnector->expects($this->exactly(3))
            ->method('call')
            ->withConsecutive([
                $session->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_CREATE_MEETING, $session,
            ],
            [
                $session->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_GET_MEETING_LINK, $session,
            ],
            [
                $session->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_TELEPHONY_PROFILE_INFO, $session,
            ])
            ->will($this->onConsecutiveCalls(
                    ( new ProviderResponse() )->setSuccess(true)->setData($session),
                    ( new ProviderResponse() )->setSuccess(true)->setData($session),
                    ( new ProviderResponse() )->setSuccess(true)->setData(new WebexMeetingTelephonyProfile())
                )
            );

        $this->entityManager->expects($this->once())
            ->method('persist');

        $service = $this->createWebexSessionService();

        $response = $service->createSession($session);
        $this->assertTrue($response->isSuccess());
        $this->assertSame($session, $response->getData());
    }

    public function testFailedCreateMeetingNeverPersist()
    {
        $session = $this->getValidMeetingForCreation();

        $this->validator->expects($this->once())->method('validate')
            ->willReturn(new ConstraintViolationList());

        $this->webexMeetingConnector->expects($this->once())
            ->method('call')
            ->with($session->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_CREATE_MEETING, $session)
            ->willReturn((new ProviderResponse())->setSuccess(false));

        $this->entityManager->expects($this->never())
            ->method('persist');

        $service = $this->createWebexSessionService();

        $response = $service->createSession($session);
        $this->assertFalse($response->isSuccess());
    }

    public function testSetWebexSession()
    {
        $session = $this->getValidMeetingForUpdate();

        $this->validator->expects($this->once())->method('validate')
            ->willReturn(new ConstraintViolationList());

        $this->webexMeetingConnector->expects($this->once())
            ->method('call')
            ->with($session->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_SET_MEETING, $session)
            ->willReturn(
                (new ProviderResponse())->setSuccess(true)
                    ->setData($session)
            );

        $service = $this->createWebexSessionService();

        $response = $service->updateSession($session);
        $this->assertTrue($response->isSuccess());
        $this->assertSame($session, $response->getData());
    }

    public function testSuccessfulDeleteWebexSessionRemoves()
    {
        $meeting = $this->getValidMeetingForUpdate();

        $this->validator->expects($this->once())->method('validate')
            ->willReturn(new ConstraintViolationList());

        $this->webexMeetingConnector->expects($this->once())
            ->method('call')
            ->with($meeting->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_DELETE_MEETING, $meeting)
            ->willReturn(
                (new ProviderResponse())->setSuccess(true)
                    ->setData($meeting)
            );

        $service = $this->createWebexSessionService();

        $response = $service->deleteSession($meeting);
        $this->assertTrue($response->isSuccess());
        $this->assertSame($meeting, $response->getData());
    }

    public function testFailedDeleteWebexSessionNeverRemoves()
    {
        $meeting = $this->getValidMeetingForUpdate();

        $this->validator->expects($this->once())->method('validate')
            ->willReturn(new ConstraintViolationList());

        $this->webexMeetingConnector->expects($this->once())
            ->method('call')
            ->with($meeting->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_DELETE_MEETING, $meeting)
            ->willReturn(
                (new ProviderResponse())->setSuccess(false)
            );

        $this->entityManager->expects($this->never())
            ->method('remove');

        $service = $this->createWebexSessionService();

        $response = $service->deleteSession($meeting);
        $this->assertFalse($response->isSuccess());
    }

    public function testFetchHostJoinUrlCallsConnector()
    {
        $meeting = $this->getValidMeetingForUpdate();

        $this->validator->expects($this->once())->method('validate')
            ->willReturn(new ConstraintViolationList());

        $this->webexMeetingConnector->expects($this->once())
            ->method('call')
            ->with($meeting->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_GET_HOST_URL_MEETING, $meeting);

        $service = $this->createWebexSessionService();

        $service->fetchHostJoinUrl($meeting);
    }

    public function testFetchGuestJoinUrlCallsConnector()
    {
        $meeting = $this->getValidMeetingForUpdate();
        $participant = $this->getValidParticipant();

        $participantSessionRole = new ProviderParticipantSessionRole();
        $participantSessionRole->setParticipant($participant)
            ->setSession($meeting);

        $this->validator->expects($this->once())->method('validate')
            ->willReturn(new ConstraintViolationList());

        $this->webexMeetingConnector->expects($this->once())
            ->method('call')
            ->with($participantSessionRole->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_GET_JOIN_URL_MEETING, $participantSessionRole);

        $service = $this->createWebexSessionService();

        $service->fetchGuestJoinUrl($participantSessionRole);
    }

    public function testListAttendeePersistsNewlyCreatedAttendees()
    {
        $session = $this->getValidMeetingForUpdate();

        $this->validator->expects($this->once())->method('validate')
            ->willReturn(new ConstraintViolationList());

        $this->webexMeetingConnector->expects($this->once())
            ->method('call')
            ->with($session->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_LIST_MEETING_ATTENDEE, $session)
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createWebexSessionService();

        $response = $service->listAttendee($session);

        $this->assertTrue($response->isSuccess());
    }

    public function testFailedGetUser()
    {
        $participant = $this->getValidParticipant();

        $providerResponse = new ProviderResponse();

        $this->validator->expects($this->once())->method('validate')->willReturn(new ConstraintViolationList());

        $this->webexMeetingConnector->expects($this->once())
            ->method('call')
            ->with($participant->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_GET_USER, $participant)
            ->willReturn($providerResponse);

        $providerResponse->setSuccess(false)
            ->setThrown(new ProviderGenericException($this->webexMeetingConnector::class, ''));

        $service = $this->createWebexSessionService();

        $response = $service->getParticipant($participant);
        $this->assertSame($providerResponse, $response);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testSuccessfulGetUser()
    {
        $participant = $this->getValidParticipant();

        $providerResponse = new ProviderResponse();

        $this->validator->expects($this->once())->method('validate')->willReturn(new ConstraintViolationList());

        $this->webexMeetingConnector->expects($this->once())
            ->method('call')
            ->with($participant->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_GET_USER, $participant)
            ->willReturn($providerResponse);

        $providerResponse->setSuccess(true)
            ->setData($participant);

        $service = $this->createWebexSessionService();

        $response = $service->getParticipant($participant);
        $this->assertSame($providerResponse, $response);
        $this->assertTrue($response->isSuccess());
        $this->assertSame($participant, $response->getData());
    }

    public function provideMethodsForValidationErrors(): \Generator
    {
        yield 'createSession' => ['createSession', [$this->createWebexMeeting()]];
        yield 'updateSession' => ['updateSession', [$this->createWebexMeeting(), '']];
        yield 'deleteSession' => ['deleteSession', [$this->createWebexMeeting()]];
        yield 'fetchHostJoinUrl' => ['fetchHostJoinUrl', [$this->createWebexMeeting()]];
        yield 'fetchGuestJoinUrl' => ['fetchGuestJoinUrl', [$this->createWebexParticipantRoel()]];
        yield 'getConnectorJoinUrl' => ['getConnectorJoinUrl', [$this->createWebexMeeting()]];
        yield 'listAttendee' => ['listAttendee', [$this->createWebexMeeting()]];
        yield 'getParticipant' => ['getParticipant', [new WebexParticipant($this->createWebexConfigurationHolder())]];
    }

    private function createWebexMeeting(): WebexMeeting
    {
        return ( new WebexMeeting($this->createWebexConfigurationHolder()) )
            ->setId(rand(0000, 9999));
    }

    private function createWebexParticipantRoel(): ProviderParticipantSessionRole
    {
        return (new ProviderParticipantSessionRole())
            ->setSession(new WebexMeeting($this->createWebexConfigurationHolder()))
            ->setParticipant(new WebexParticipant($this->createWebexConfigurationHolder()));
    }

    /**
     * @dataProvider provideMethodsForValidationErrors
     */
    public function testValidatorExceptionNeverCallsConnector($method, array $parameters = [])
    {
        $list = new ConstraintViolationList();
        $list->add($this->createMock(ConstraintViolation::class));

        $this->validator->expects($this->atLeastOnce())
            ->method('validate')
            ->willReturn($list);

        $this->expectException(ProviderServiceValidationException::class);

        $this->webexMeetingConnector->expects($this->never())
            ->method('call');

        $service = $this->createWebexSessionService();
        $service->$method(...$parameters);
    }

    private function getDataReportMeetingAttendance(): array
    {
        $presences = [
            'aghnassia@icloud.com' => [
                'email' => 'aghnassia@icloud.com',
                'presenceDateStart' => \DateTimeImmutable::createFromFormat('m/d/Y H:i:s', '11/02/2021 07:40:54'),
                'presenceDateEnd' => \DateTimeImmutable::createFromFormat('m/d/Y H:i:s', '11/02/2021 15:58:31'),
                'duration' => (int) '499',
            ],
            'delphine.weiss@ca-des-savoie.fr' => [
                'email' => 'delphine.weiss@ca-des-savoie.fr',
                'presenceDateStart' => \DateTimeImmutable::createFromFormat('m/d/Y H:i:s', '11/02/2021 07:50:27'),
                'presenceDateEnd' => \DateTimeImmutable::createFromFormat('m/d/Y H:i:s', '11/02/2021 15:58:45'),
                'duration' => (int) '489',
            ],
            'odile.bacquet@ca-paris.fr' => [
                'email' => 'odile.bacquet@ca-paris.fr',
                'presenceDateStart' => \DateTimeImmutable::createFromFormat('m/d/Y H:i:s', '11/02/2021 07:50:47'),
                'presenceDateEnd' => \DateTimeImmutable::createFromFormat('m/d/Y H:i:s', '11/02/2021 12:43:44'),
                'duration' => (int) '293',
            ],
            'audrey.poinsignon@ca-lorraine.fr' => [
                'email' => 'audrey.poinsignon@ca-lorraine.fr',
                'presenceDateStart' => \DateTimeImmutable::createFromFormat('m/d/Y H:i:s', '11/02/2021 07:54:44'),
                'presenceDateEnd' => \DateTimeImmutable::createFromFormat('m/d/Y H:i:s', '11/02/2021 15:58:27'),
                'duration' => (int) '484',
            ],
        ];

        return $presences;
    }

    public function testReportWebexMeetingAttendanceWithDataEmpty(): void
    {
        $webexMeeting = new WebexMeeting($this->createWebexConfigurationHolder());
        $webexMeeting->setName('My Meeting Test')
            ->setRef('01020306054')
            ->setDateStart(\DateTime::createFromFormat('m/d/Y H:i:s', '11/02/2021 07:30:00'), )
            ->setDateEnd(\DateTime::createFromFormat('m/d/Y H:i:s', '11/02/2021 16:00:00'));

        $providerResponse = new ProviderResponse();
        $providerResponse
            ->setData([])
            ->setSuccess(true);

        $this->webexMeetingConnector->expects($this->once())
            ->method('call')
            ->with($webexMeeting->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_LIST_MEETING_ATTENDEE_HISTORY, $webexMeeting)
            ->willReturn($providerResponse);

        $service = $this->createWebexSessionService();

        $response = $service->reportMeetingAttendance($webexMeeting);
        $this->assertTrue($response->isSuccess());
        $this->assertIsArray($response->getData());
    }

    public function testReportMeetingAttendanceWithGetDataNotEmpty(): void
    {
        $webexMeeting = new WebexMeeting($this->createWebexConfigurationHolder());
        $webexMeeting->setName('My Meeting Test')
            ->setRef('01020306054')
            ->setDateStart(\DateTime::createFromFormat('m/d/Y H:i:s', '11/02/2021 07:30:00'), )
            ->setDateEnd(\DateTime::createFromFormat('m/d/Y H:i:s', '11/02/2021 16:00:00'));

        $providerResponse = new ProviderResponse();
        $providerResponse
            ->setData($this->getDataReportMeetingAttendance())
            ->setSuccess(true);

        $this->webexMeetingConnector->expects($this->once())
            ->method('call')
            ->with($webexMeeting->getAbstractProviderConfigurationHolder(), WebexMeetingConnector::ACTION_LIST_MEETING_ATTENDEE_HISTORY, $webexMeeting)
            ->willReturn($providerResponse);

        $service = $this->createWebexSessionService();

        $response = $service->reportMeetingAttendance($webexMeeting);
        $this->assertTrue($response->isSuccess());
        $this->assertEquals(4, count($response->getData()));
    }
}
