<?php

namespace App\Tests\Unit\Service;

use App\Entity\Client\Client;
use App\Service\SupportService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SupportServiceTest extends TestCase
{
    private MockObject|TranslatorInterface $translator;
    private MockObject|LoggerInterface $logger;

    public function setUp(): void
    {
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
    }

    public function getService(): SupportService
    {
        return new SupportService(
            $this->translator,
            $this->logger,
        );
    }

    public function testOverlapInMinutes(): void
    {
        $startSession = new \DateTime('2022/03/01 09:00');
        $endSession = new \DateTime('2022/03/01 12:00');
        $startPeriod = new \DateTime('2022/03/01 10:00');
        $endPeriod = new \DateTime('2022/03/01 11:00');
        $resultInMin = $this->getService()->overlapInMinutes($startSession, $endSession, $startPeriod, $endPeriod);

        $this->assertEquals(60, $resultInMin);
    }

    public function clientServices()
    {
        $client = new Client();
        $client->getServices()
            ->setSupportType(2)
            ->setAssistanceType(2)
            ->getStandardHours()
                ->setActive(true)
                ->setStartTimeAm(new \DateTime('today 09:00:00'))
                ->setEndTimeAm(new \DateTime('today 12:30:59'))
                ->setStartTimeLh(new \DateTime('today 12:31:00'))
                ->setEndTimeLh(new \DateTime('today 13:59:59'))
                ->setStartTimePm(new \DateTime('today 14:00:00'))
                ->setEndTimePm(new \DateTime('today 17:30:00'));
        $client->getServices()
            ->getOutsideStandardHours()
                ->setActive(true)
                ->setStartTimeAm(new \DateTime('today 07:00:00'))
                ->setEndTimeAm(new \DateTime('today 08:59:59'))
                ->setStartTimeLh(new \DateTime('today 12:31:00'))
                ->setEndTimeLh(new \DateTime('today 13:59:59'))
                ->setStartTimePm(new \DateTime('today 17:31:00'))
                ->setEndTimePm(new \DateTime('today 19:00:00'))
                ->setActive(true)
            ;

        return $client;
    }

    public function sessionsDataCaseProvider(): \Generator
    {
        yield 'Session HS only' => [$this->clientServices(), new \DateTime('today 09:30:00'), new \DateTime('today 11:30:00'),
            ['beforeHHS' => 0, 'inHHSAMStartAndHSAMStart' => 0, 'inHSAMStartAndHHSLHStart' => 120, 'inHHSLHStartHAndHSPMStart' => 0, 'inHSPMStartAndHHSPMStart' => 0, 'inHHSPMStartHandHHSPMEnd' => 0, 'afterHHS' => 0], ];

        yield 'Session HHS and HS only' => [$this->clientServices(), new \DateTime('today 07:30:00'), new \DateTime('today 11:30:00'),
            ['beforeHHS' => 0, 'inHHSAMStartAndHSAMStart' => 89, 'inHSAMStartAndHHSLHStart' => 150, 'inHHSLHStartHAndHSPMStart' => 0, 'inHSPMStartAndHHSPMStart' => 0, 'inHHSPMStartHandHHSPMEnd' => 0, 'afterHHS' => 0], ];

        yield 'Session HHS and HS and Out' => [$this->clientServices(), new \DateTime('today 04:30:00'), new \DateTime('today 11:30:00'),
            ['beforeHHS' => 150, 'inHHSAMStartAndHSAMStart' => 119, 'inHSAMStartAndHHSLHStart' => 150, 'inHHSLHStartHAndHSPMStart' => 0, 'inHSPMStartAndHHSPMStart' => 0, 'inHHSPMStartHandHHSPMEnd' => 0, 'afterHHS' => 0], ];

        yield 'Session HHS and Out only' => [$this->clientServices(), new \DateTime('today 04:30:00'), new \DateTime('today 07:30:00'),
            ['beforeHHS' => 150, 'inHHSAMStartAndHSAMStart' => 30, 'inHSAMStartAndHHSLHStart' => 0, 'inHHSLHStartHAndHSPMStart' => 0, 'inHSPMStartAndHHSPMStart' => 0, 'inHHSPMStartHandHHSPMEnd' => 0, 'afterHHS' => 0], ];

        yield 'Session Out only' => [$this->clientServices(), new \DateTime('today 04:30:00'), new \DateTime('today 05:30:00'),
            ['beforeHHS' => 60, 'inHHSAMStartAndHSAMStart' => 0, 'inHSAMStartAndHHSLHStart' => 0, 'inHHSLHStartHAndHSPMStart' => 0, 'inHSPMStartAndHHSPMStart' => 0, 'inHHSPMStartHandHHSPMEnd' => 0, 'afterHHS' => 0], ];
    }

    /**
     * @dataProvider sessionsDataCaseProvider
     */
    public function testOverlapsMultiCase(Client $client, \DateTime $sessionDateStart, \DateTime $sessionDateEnd, array $slotsResults): void
    {
        $resultInMin = $this->getService()->getOverlapped($client, $sessionDateStart, $sessionDateEnd);
        foreach ($resultInMin['slotsResults'] as $k => $v) {
            $this->assertEquals($slotsResults[$k], $resultInMin['slotsResults'][$k]);
        }
    }
}
