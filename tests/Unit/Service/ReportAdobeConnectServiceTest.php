<?php

namespace App\Tests\Unit\Service;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Model\ProviderResponse;
use App\Service\AdobeConnectService;
use App\Service\Provider\Adobe\ReportAdobeConnectService;
use App\Service\ProviderSessionService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ReportAdobeConnectServiceTest extends TestCase
{
    private MockObject|EntityManagerInterface $entityManager;

    private MockObject|AdobeConnectService $abobeConnectService;
    private MockObject|ProviderSessionService $providerSessionService;

    private MockObject|ReportAdobeConnectService $reportAdobeConnectService;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->abobeConnectService = $this->createMock(AdobeConnectService::class);
        $this->providerSessionService = $this->createMock(ProviderSessionService::class);
    }

    public function getService(): ReportAdobeConnectService
    {
        return new ReportAdobeConnectService(
            $this->entityManager,
            $this->abobeConnectService,
            $this->providerSessionService
        );
    }

    private function getDataReportAttendees(): array
    {
        $response = [
            'yoann.duquesnes@ca-cotesdarmor.fr' => [
                'principal-id' => 5561739189,
                'date-from' => new \DateTimeImmutable('2021-10-12 09:07:07'),
                'date-to' => new \DateTimeImmutable('2021-10-12 12:26:10'),
                'login' => 'yoann.duquesnes@ca-cotesdarmor.fr',
            ],
            'jeremie.heuze@ca-cotesdarmor.fr' => [
                'principal-id' => 5561871030,
                'date-from' => new \DateTimeImmutable('2021-10-12 09:20:34'),
                'date-to' => new \DateTimeImmutable('2021-10-12 12:22:59'),
                'login' => 'jeremie.heuze@ca-cotesdarmor.fr',
            ],
        ];

        return $response;
    }

    public function testGetReportAdobeConnectWithoutParticipant(): void
    {
        $adobeConnectSCO = new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client()));
        $adobeConnectSCO
            ->setDateStart(\DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-18 00:00:00'))
            ->setDateEnd(\DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-18 23:59:00'));

        $response = $this->getService()->getReportAdobeConnect($adobeConnectSCO);

        $this->assertIsArray($response);
        $this->assertEquals(0, count($response));
    }

    public function testGetReportAdobeConnectWithParticipantFullPersist(): void
    {
        $adobeConnectSCO = new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client()));
        $adobeConnectSCO
            ->setId(224488)
            ->setDateStart(new \DateTime('2021-10-12 09:00:00'))
            ->setDateEnd(new \DateTime('2021-10-12 12:30:00'));

        $providerResponse = new ProviderResponse();
        $providerResponse
            ->setData($this->getDataReportAttendees())
            ->setSuccess(true);

        $this->abobeConnectService->expects($this->once())
            ->method('reportMeetingAttendance')
            ->willReturn($providerResponse);

        $response = $this->getService()->getReportAdobeConnect($adobeConnectSCO);

        $this->assertIsArray($response);
        $this->assertEquals(0, count($response));
    }

    public function testPrepareAttendees(): void
    {
        $rawAttendees = [
            0 => [
                'principal-id' => 666,
                'sco-id' => 999,
                'asset-id' => 6242265169,
                'login' => 'multiple.slot@live-session.test',
                'session-name' => 'MULTIPLE SLOT',
                'sco-name' => 'Test for arrange attendees',
                'date-created' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                'date-end' => new \DateTimeImmutable('2021-10-12 13:00:00'),
                'participant-name' => 'MULTIPLE SLOT',
            ],
            1 => [
                'principal-id' => 777,
                'sco-id' => 999,
                'asset-id' => 6242265169,
                'login' => 'single.slot@live-session.test',
                'session-name' => 'SINGLE SLOT',
                'sco-name' => 'Test for arrange attendees',
                'date-created' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                'date-end' => new \DateTimeImmutable('2021-10-12 09:30:00'),
                'participant-name' => 'SINGLE SLOT',
            ],
            2 => [
                'principal-id' => 666,
                'sco-id' => 999,
                'asset-id' => 6242265169,
                'login' => 'multiple.slot@live-session.test',
                'session-name' => 'MULTIPLE SLOT',
                'sco-name' => 'Test for arrange attendees',
                'date-created' => new \DateTimeImmutable('2021-10-12 08:00:00'),
                'date-end' => new \DateTimeImmutable('2021-10-12 10:00:00'),
                'participant-name' => 'MULTIPLE SLOT',
            ],
            3 => [
                'principal-id' => 42,
                'sco-id' => 999,
                'asset-id' => 6242265169,
                'login' => 'access.date.outside.sco.date@live-session.test',
                'session-name' => 'ACCESS DATE OUTSIDE SCO DATE SLOT',
                'sco-name' => 'Test for arrange attendees',
                'date-created' => new \DateTimeImmutable('2021-10-12 07:00:00'),
                'date-end' => new \DateTimeImmutable('2021-10-12 18:00:00'),
                'participant-name' => 'ACCESS DATE OUTSIDE SCO DATE SLOT',
            ],
        ];
        $adobeConnectSCO = new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client()));
        $adobeConnectSCO
            ->setDateStart(\DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-12 09:00:00'))
            ->setDateEnd(\DateTime::createFromFormat('Y-m-d H:i:s', '2021-10-12 12:30:00'));

        $actual = $this->getService()->prepareAttendees($rawAttendees, $adobeConnectSCO);

        $expected = [
            'multiple.slot@live-session.test' => [
                'principal-id' => 666,
                'date-from' => new \DateTimeImmutable('2021-10-12 08:00:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 13:00:00'),
                'final-duration' => 300,
                'slots-duration' => [
                    [
                        'date-from' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                        'date-to' => new \DateTimeImmutable('2021-10-12 13:00:00'),
                    ],
                    [
                        'date-from' => new \DateTimeImmutable('2021-10-12 08:00:00'),
                        'date-to' => new \DateTimeImmutable('2021-10-12 10:00:00'),
                    ],
                ],
            ],
            'single.slot@live-session.test' => [
                'principal-id' => 777,
                'date-from' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 09:30:00'),
                'final-duration' => 30,
                'slots-duration' => [
                    [
                        'date-from' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                        'date-to' => new \DateTimeImmutable('2021-10-12 09:30:00'),
                    ],
                ],
            ],
            'access.date.outside.sco.date@live-session.test' => [
                'principal-id' => 42,
                'date-from' => new \DateTimeImmutable('2021-10-12 08:00:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 13:30:00'),
                'final-duration' => 330,
                'slots-duration' => [
                    [
                        'date-from' => new \DateTimeImmutable('2021-10-12 08:00:00'),
                        'date-to' => new \DateTimeImmutable('2021-10-12 13:30:00'),
                    ],
                ],
            ],
        ];

        $this->assertEquals($expected, $actual);
    }

    public function testArrangeAttendees(): void
    {
        $rawAttendees = [
            0 => [
                'principal-id' => 666,
                'sco-id' => 999,
                'asset-id' => 6242265169,
                'login' => 'multiple.slot@live-session.test',
                'session-name' => 'MULTIPLE SLOT',
                'sco-name' => 'Test for arrange attendees',
                'date-created' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                'date-end' => new \DateTimeImmutable('2021-10-12 13:00:00'),
                'participant-name' => 'MULTIPLE SLOT',
            ],
            1 => [
                'principal-id' => 777,
                'sco-id' => 999,
                'asset-id' => 6242265169,
                'login' => 'single.slot@live-session.test',
                'session-name' => 'SINGLE SLOT',
                'sco-name' => 'Test for arrange attendees',
                'date-created' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                'date-end' => new \DateTimeImmutable('2021-10-12 09:30:00'),
                'participant-name' => 'SINGLE SLOT',
            ],
            2 => [
                'principal-id' => 666,
                'sco-id' => 999,
                'asset-id' => 6242265169,
                'login' => 'multiple.slot@live-session.test',
                'session-name' => 'MULTIPLE SLOT',
                'sco-name' => 'Test for arrange attendees',
                'date-created' => new \DateTimeImmutable('2021-10-12 08:00:00'),
                'date-end' => new \DateTimeImmutable('2021-10-12 10:00:00'),
                'participant-name' => 'MULTIPLE SLOT',
            ],
            3 => [
                'principal-id' => 42,
                'sco-id' => 999,
                'asset-id' => 6242265169,
                'login' => 'access.date.outside.sco.date@live-session.test',
                'session-name' => 'ACCESS DATE OUTSIDE SCO DATE SLOT',
                'sco-name' => 'Test for arrange attendees',
                'date-created' => new \DateTimeImmutable('2021-10-12 07:00:00'),
                'date-end' => new \DateTimeImmutable('2021-10-12 18:00:00'),
                'participant-name' => 'ACCESS DATE OUTSIDE SCO DATE SLOT',
            ],
        ];
        $scoDateStart = new \DateTimeImmutable('2021-10-12 08:00:00');
        $scoDateEnd = new \DateTimeImmutable('2021-10-12 13:30:00');

        $actual = $this->getService()->arrangeAttendees($rawAttendees, $scoDateStart, $scoDateEnd);

        $expected = [
            'multiple.slot@live-session.test' => [
                'principal-id' => 666,
                'slots-duration' => [
                    [
                        'date-from' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                        'date-to' => new \DateTimeImmutable('2021-10-12 13:00:00'),
                    ],
                    [
                        'date-from' => new \DateTimeImmutable('2021-10-12 08:00:00'),
                        'date-to' => new \DateTimeImmutable('2021-10-12 10:00:00'),
                    ],
                ],
            ],
            'single.slot@live-session.test' => [
                'principal-id' => 777,
                'slots-duration' => [
                    [
                        'date-from' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                        'date-to' => new \DateTimeImmutable('2021-10-12 09:30:00'),
                    ],
                ],
            ],
            'access.date.outside.sco.date@live-session.test' => [
                'principal-id' => 42,
                'slots-duration' => [
                    [
                        'date-from' => new \DateTimeImmutable('2021-10-12 08:00:00'),
                        'date-to' => new \DateTimeImmutable('2021-10-12 13:30:00'),
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $actual, );
    }

    public function testEvaluateDateAndDurationFromSlotsSingleSlot(): void
    {
        $slots = [
            [
                'date-from' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 09:30:00'),
            ],
        ];

        $actual = $this->getService()->evaluateDateAndDurationFromSlots($slots);

        $expected = [30, new \DateTimeImmutable('2021-10-12 09:00:00'), new \DateTimeImmutable('2021-10-12 09:30:00')];
        $this->assertEquals($expected, $actual);
    }

    public function testEvaluateDateAndDurationFromSlotsMultipleSlotSecondInside(): void
    {
        // CASE TESTED
        //$dFromSlot|$dToSlot                               |-------------------|
        //$rawSlot['date-from']|$rawSlot['date-to']               |----------|
        //$final-duration                                   |-------------------|
        $slots = [
            [
                'date-from' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 10:00:00'),
            ],
            [
                'date-from' => new \DateTimeImmutable('2021-10-12 09:15:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 09:30:00'),
            ],
        ];

        $actual = $this->getService()->evaluateDateAndDurationFromSlots($slots);

        $expected = [60, new \DateTimeImmutable('2021-10-12 09:00:00'), new \DateTimeImmutable('2021-10-12 10:00:00')];
        $this->assertEquals($expected, $actual);
    }

    public function testCalculatedurationFromTimeSlotsMultipleSlotSecondStartAfter(): void
    {
        // CASE TESTED
        //$dFromSlot|$dToSlot                               |----------------|
        //$rawSlot['date-from']|$rawSlot['date-to']                       |----------|
        //$final-duration                                   |----------------------|
        $slots = [
            [
                'date-from' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 09:30:00'),
            ],
            [
                'date-from' => new \DateTimeImmutable('2021-10-12 09:15:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 09:45:00'),
            ],
        ];

        $actual = $this->getService()->evaluateDateAndDurationFromSlots($slots);

        $expected = [45, new \DateTimeImmutable('2021-10-12 09:00:00'), new \DateTimeImmutable('2021-10-12 09:45:00')];
        $this->assertEquals($expected, $actual);
    }

    public function testCalculatedurationFromTimeSlotsMultipleSlotNoOverlap(): void
    {
        // CASE TESTED
        //$dFromSlot|$dToSlot                               |----------------|
        //$rawSlot['date-from']|$rawSlot['date-to']                               |----------|
        //$final-duration                                   |----------------|  |----------|                               |----------------------|
        $slots = [
            [
                'date-from' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 09:30:00'),
            ],
            [
                'date-from' => new \DateTimeImmutable('2021-10-12 09:45:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 10:00:00'),
            ],
        ];

        $actual = $this->getService()->evaluateDateAndDurationFromSlots($slots);

        $expected = [45, new \DateTimeImmutable('2021-10-12 09:00:00'), new \DateTimeImmutable('2021-10-12 10:00:00')];
        $this->assertEquals($expected, $actual);
    }

    public function testCalculatedurationFromTimeSlotsMultipleSlotShuffledAllCases(): void
    {
        $slots = [
            [
                'date-from' => new \DateTimeImmutable('2021-10-12 09:45:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 10:00:00'),
            ],
            [
                'date-from' => new \DateTimeImmutable('2021-10-12 09:00:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 09:30:00'),
            ],
            [
                'date-from' => new \DateTimeImmutable('2021-10-12 10:15:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 10:30:00'),
            ],
            [
                'date-from' => new \DateTimeImmutable('2021-10-12 09:55:00'),
                'date-to' => new \DateTimeImmutable('2021-10-12 10:50:00'),
            ],
        ];

        $actual = $this->getService()->evaluateDateAndDurationFromSlots($slots);

        $expected = [95, new \DateTimeImmutable('2021-10-12 09:00:00'), new \DateTimeImmutable('2021-10-12 10:50:00')];
        $this->assertEquals($expected, $actual);
    }
}
