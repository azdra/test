<?php

namespace App\Tests\Unit\Service;

use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Model\ProviderResponse;
use App\Service\Provider\Webex\ReportWebexRestMeetingService;
use App\Service\ProviderSessionService;
use App\Service\WebexRestMeetingService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ReportWebexRestMeetingServiceTest extends TestCase
{
    public function testWhite(): void
    {
        $this->assertSame('whitetest', 'whitetest');
    }
    private MockObject|EntityManagerInterface $entityManager;

    private MockObject|WebexRestMeetingService $webexRestMeetingService;

    private MockObject|ProviderSessionService $providerSessionService;
    private MockObject|ReportWebexRestMeetingService $reportWebexRestMeetingService;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->webexRestMeetingService = $this->createMock(WebexRestMeetingService::class);
        $this->providerSessionService = $this->createMock(ProviderSessionService::class);
    }

    public function getService(): ReportWebexRestMeetingService
    {
        return new ReportWebexRestMeetingService(
            $this->entityManager,
            $this->webexRestMeetingService,
            $this->providerSessionService
        );
    }

    private function getDataReportAttendees(): array
    {
        $presences = [
            'aghnassia@icloud.com' => [
                'email' => 'aghnassia@icloud.com',
                'presenceDateStart' => \DateTime::createFromFormat('m/d/Y H:i:s', '11/02/2021 07:40:54'),
                'presenceDateEnd' => \DateTime::createFromFormat('m/d/Y H:i:s', '11/02/2021 15:58:31'),
                'duration' => (int) '499',
            ],
            'delphine.weiss@ca-des-savoie.fr' => [
                'email' => 'delphine.weiss@ca-des-savoie.fr',
                'presenceDateStart' => \DateTime::createFromFormat('m/d/Y H:i:s', '11/02/2021 07:50:27'),
                'presenceDateEnd' => \DateTime::createFromFormat('m/d/Y H:i:s', '11/02/2021 15:58:45'),
                'duration' => (int) '489',
            ],
            'odile.bacquet@ca-paris.fr' => [
                'email' => 'odile.bacquet@ca-paris.fr',
                'presenceDateStart' => \DateTime::createFromFormat('m/d/Y H:i:s', '11/02/2021 07:50:47'),
                'presenceDateEnd' => \DateTime::createFromFormat('m/d/Y H:i:s', '11/02/2021 12:43:44'),
                'duration' => (int) '293',
            ],
            'audrey.poinsignon@ca-lorraine.fr' => [
                'email' => 'audrey.poinsignon@ca-lorraine.fr',
                'presenceDateStart' => \DateTime::createFromFormat('m/d/Y H:i:s', '11/02/2021 07:54:44'),
                'presenceDateEnd' => \DateTime::createFromFormat('m/d/Y H:i:s', '11/02/2021 15:58:27'),
                'duration' => (int) '484',
            ],
        ];

        return $presences;
    }

    public function testGetReportWebexRestMeetingWithoutParticipant(): void
    {
        $webexRestMeeting = new WebexRestMeeting((new WebexRestConfigurationHolder())->setClient(new Client()));
        $webexRestMeeting
            ->setDateStart(\DateTime::createFromFormat('Y-m-d H:i:s', '2021-11-02 07:30:00'))
            ->setDateEnd(\DateTime::createFromFormat('Y-m-d H:i:s', '2021-11-02 16:00:00'));

        $response = $this->getService()->getReportWebexRestMeeting($webexRestMeeting);

        //$this->assertIsArray($response->getData());
        $this->assertEmpty($response->getData());
    }

    public function testGetReportWebexMeetingWithParticipantReadyToPersist(): void
    {
        $webexRestMeeting = new WebexRestMeeting((new WebexRestConfigurationHolder())->setClient(new Client()));
        $webexRestMeeting
            ->setId(224488)
            ->setDateStart(new \DateTime('2021-11-02 07:30:00'))
            ->setDateEnd(new \DateTime('2021-11-02 16:00:00'));

        $providerResponse = new ProviderResponse();
        $providerResponse
            ->setData($this->getDataReportAttendees())
            ->setSuccess(true);

        $this->webexRestMeetingService->expects($this->once())
            ->method('reportMeetingAttendance')
            ->willReturn($providerResponse);

        $response = $this->getService()->getReportWebexRestMeeting($webexRestMeeting);
        $this->assertIsArray($response->getData());
        $this->assertEquals(4, count($response->getData()));
    }
}
