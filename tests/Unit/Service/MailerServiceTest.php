<?php

namespace App\Tests\Unit\Service;

use App\Entity\Client\Client;
use App\Entity\User;
use App\Exception\InvalidArgumentException;
use App\Service\MailerService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class MailerServiceTest extends TestCase
{
    /**
     * @dataProvider sendTemplatedMailDatas
     */
    public function testSendTemplatedMail(Address|string $address, string $subject, string $template, array $context = [], ?string $pathToAttach = null, array $metadatas = null): void
    {
        $mailer = $this->createMock(MailerInterface::class);
        $mailer->expects(self::exactly(1))
            ->method('send');

        $mailerService = new MailerService(
            $mailer,
            $this->createMock(LoggerInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(Environment::class),
            $this->createMock(HttpClientInterface::class),
            'testmail@test.com',
            'noreply@test.com',
            'Test Sender Name',
            '9a8d7a8faddb26cb31d877828e207f1a6d3e8ea5810b7594167dc8110725496c',
            'https://preprod.idfuse.fr/',
            '123654789',
            $this->createMock(KernelInterface::class),
            'test@live-session.fr'
        );

        $mailerService->sendTemplatedMail(
            $address,
            $subject,
            $template,
            $context,
            $pathToAttach,
            $metadatas
        );
    }

    public function sendTemplatedMailDatas(): \Generator
    {
        // #0 - Simple call with email string / subject and template
        yield ['test@test.fr', 'test subject', 'my\twig\template.html.twig'];
        // #1 - Simple call with all param
        yield ['test@test.fr', 'test subject', 'my\twig\template.html.twig', ['user' => (new User())->setClient(new Client())], '/path/to/my/attached/file.pdf', ['user' => '1', 'session' => '2']];
        // #2 - Call with an address instance and full param
        $address = $address = new Address('test@test.fr', 'Jean Michel test');
        yield [$address, 'test subject', 'my\twig\template.html.twig', ['user' => (new User())->setClient(new Client())], '/path/to/my/attached/file.pdf', ['user' => '1', 'session' => '2']];
    }

    /**
     * @dataProvider FailedSendTemplatedMailDatas
     */
    public function testFailedSendTemplatedMail(Address|string $address, string $subject, string $template, $exceptionMessage): void
    {
        $mailer = $this->createMock(MailerInterface::class);
        $mailer->expects(self::never())
            ->method('send');

        $mailerService = new MailerService(
            $mailer,
            $this->createMock(LoggerInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(Environment::class),
            $this->createMock(HttpClientInterface::class),
            'testmail@test.com',
            'noreply@test.com',
            'Test Sender Name',
            '9a8d7a8faddb26cb31d877828e207f1a6d3e8ea5810b7594167dc8110725496c',
            'https://preprod.idfuse.fr/',
            '123654789',
            $this->createMock(KernelInterface::class),
            'test@live-session.fr'
        );

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($exceptionMessage);

        $mailerService->sendTemplatedMail(
            $address,
            $subject,
            $template
        );
    }

    public function FailedSendTemplatedMailDatas(): \Generator
    {
        // #0 Empty email address
        yield ['', 'This is my subject', 'path/to/template.html.twig', 'The email address is invalid'];
        // #1 Empty template
        yield ['test@test.fr', 'This is my subject', '', 'The email template is not valid'];
    }
}
