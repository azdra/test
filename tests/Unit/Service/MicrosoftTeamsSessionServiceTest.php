<?php

namespace App\Tests\Unit\Service;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\Microsoft\MicrosoftTeamsTelephonyProfile;
use App\Exception\ProviderGenericException;
use App\Exception\ProviderServiceValidationException;
use App\Model\ProviderResponse;
use App\Service\AsyncWorker\AsyncTaskService;
use App\Service\Connector\Microsoft\MicrosoftTeamsConnector;
use App\Service\MicrosoftTeamsService;
use App\Service\Provider\Microsoft\MicrosoftTeamsLicensingService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class MicrosoftTeamsSessionServiceTest extends TestCase
{
    protected MockObject|EntityManagerInterface $entityManager;
    protected MockObject|ValidatorInterface $validator;
    protected MockObject|MicrosoftTeamsLicensingService $microsoftTeamsLicensingService;
    protected MockObject|MicrosoftTeamsConnector $microsoftTeamsConnector;
    protected AsyncTaskService $asyncTaskService;
    private LoggerInterface $logger;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->microsoftTeamsLicensingService = $this->createMock(MicrosoftTeamsLicensingService::class);
        $this->microsoftTeamsConnector = $this->createMock(MicrosoftTeamsConnector::class);
        $this->asyncTaskService = new AsyncTaskService($this->entityManager);
        $this->logger = $this->createMock(LoggerInterface::class);
    }

    protected function createMicrosoftTeamsSessionService(): MicrosoftTeamsService
    {
        return new MicrosoftTeamsService(
            $this->entityManager,
            $this->validator,
            $this->microsoftTeamsLicensingService,
            $this->microsoftTeamsConnector,
            $this->asyncTaskService,
            $this->logger
        );
    }

    protected function getValidSession(): MicrosoftTeamsSession
    {
        return (new MicrosoftTeamsSession($this->createMicrosoftTeamsConfigurationHolder()))
            ->setName('My test')
            ->setRef('My ref')
            ->setDateStart(new \DateTime())
            ->setDateEnd(new \DateTime())
            ->setMeetingIdentifier('my_identifier');
    }

    protected function createMicrosoftTeamsConfigurationHolder(): MicrosoftTeamsConfigurationHolder|MockObject
    {
        return (new MicrosoftTeamsConfigurationHolder())
            ->setTokenEndpoint('')
            ->setTenantId('')
            ->setScope('')
            ->setGrantType('')
            ->setOrganizerEmail('')
            ->setOrganizerUniqueIdentifier('')
            ->setClientId('')
            ->setClientSecret('')
            ->setClient(new Client());
    }

    public function testSuccessfulCreatePersistsEntity()
    {
        $session = $this->getValidSession();

        $providerResponse = new ProviderResponse();
        $providerResponse->setSuccess(true)
            ->setData($session);

        $this->validator->expects($this->once())
            ->method('validate')
            ->willReturn(new ConstraintViolationList());

        $providerAudio = new MicrosoftTeamsTelephonyProfile();
        $providerResponseAudio = new ProviderResponse();
        $providerResponseAudio->setSuccess(true)
            ->setData($providerAudio);

        $this->microsoftTeamsConnector->expects($this->exactly(2))
            ->method('call')
            ->withConsecutive([$session->getAbstractProviderConfigurationHolder(), MicrosoftTeamsConnector::ACTION_SESSION_CREATE, $session], [$session->getAbstractProviderConfigurationHolder(), MicrosoftTeamsConnector::ACTION_TELEPHONY_PROFILE_INFO, $session])
            ->willReturnOnConsecutiveCalls($providerResponse, $providerResponseAudio);

        $this->entityManager->expects($this->once())
            ->method('persist')
            ->with($session);

        $service = $this->createMicrosoftTeamsSessionService();

        $response = $service->createSession($session);
        $this->assertSame($providerResponse, $response);
        $this->assertTrue($response->isSuccess());
        $this->assertSame($session, $response->getData());
    }

    public function testFailedCreateNeverPersists()
    {
        $session = $this->getValidSession();

        $providerResponse = new ProviderResponse();

        $this->validator->expects($this->once())
            ->method('validate')
            ->willReturn(new ConstraintViolationList());

        $this->microsoftTeamsConnector->expects($this->exactly(2))
            ->method('call')
            ->withConsecutive([$session->getAbstractProviderConfigurationHolder(), MicrosoftTeamsConnector::ACTION_SESSION_CREATE, $session], [$session->getAbstractProviderConfigurationHolder(), MicrosoftTeamsConnector::ACTION_TELEPHONY_PROFILE_INFO, $session])
            ->willReturn($providerResponse);

        $providerResponse->setSuccess(false)
            ->setThrown(new ProviderGenericException($this->microsoftTeamsConnector::class, ''));

        $this->entityManager->expects($this->never())
            ->method('persist')
            ->with($session);

        $service = $this->createMicrosoftTeamsSessionService();

        $response = $service->createSession($session);
        $this->assertSame($providerResponse, $response);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testFailedUpdate()
    {
        $session = $this->getValidSession();

        $providerResponse = new ProviderResponse();

        $this->validator->expects($this->once())
            ->method('validate')
            ->willReturn(new ConstraintViolationList());

        $this->microsoftTeamsConnector->expects($this->once())
            ->method('call')
            ->with($session->getAbstractProviderConfigurationHolder(), MicrosoftTeamsConnector::ACTION_SESSION_UPDATE, $session)
            ->willReturn($providerResponse);

        $providerResponse->setSuccess(false)
            ->setThrown(new ProviderGenericException($this->microsoftTeamsConnector::class, ''));

        $service = $this->createMicrosoftTeamsSessionService();

        $response = $service->updateSession($session);
        $this->assertSame($providerResponse, $response);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function testSuccessfulUpdate()
    {
        $session = $this->getValidSession();

        $providerResponse = new ProviderResponse();

        $this->validator->expects($this->once())
            ->method('validate')
            ->willReturn(new ConstraintViolationList());

        $this->microsoftTeamsConnector->expects($this->once())
            ->method('call')
            ->with($session->getAbstractProviderConfigurationHolder(), MicrosoftTeamsConnector::ACTION_SESSION_UPDATE, $session)
            ->willReturn($providerResponse);

        $providerResponse->setSuccess(true)
            ->setData($session);

        $service = $this->createMicrosoftTeamsSessionService();

        $response = $service->updateSession($session);
        $this->assertSame($providerResponse, $response);
        $this->assertTrue($response->isSuccess());
        $this->assertSame($session, $response->getData());
    }

    public function testSuccessfulDeleteRemovesEntity()
    {
        $session = $this->getValidSession();

        $providerResponse = new ProviderResponse();
        $providerResponse->setSuccess(true)
            ->setData($session);

        $this->validator->expects($this->once())
            ->method('validate')
            ->willReturn(new ConstraintViolationList());

        $this->microsoftTeamsConnector->expects($this->once())
            ->method('call')
            ->with($session->getAbstractProviderConfigurationHolder(), MicrosoftTeamsConnector::ACTION_SESSION_DELETE, $session)
            ->willReturn($providerResponse);

        $service = $this->createMicrosoftTeamsSessionService();

        $response = $service->deleteSession($session);
        $this->assertSame($providerResponse, $response);
        $this->assertTrue($response->isSuccess());
        $this->assertSame($session, $response->getData());
    }

    public function testFailedDeleteNeverPersists()
    {
        $session = $this->getValidSession();

        $providerResponse = new ProviderResponse();

        $this->validator->expects($this->once())
            ->method('validate')
            ->willReturn(new ConstraintViolationList());

        $this->microsoftTeamsConnector->expects($this->once())
            ->method('call')
            ->with($session->getAbstractProviderConfigurationHolder(), MicrosoftTeamsConnector::ACTION_SESSION_DELETE, $session)
            ->willReturn($providerResponse);

        $providerResponse->setSuccess(false)
            ->setThrown(new ProviderGenericException($this->microsoftTeamsConnector::class, ''));

        $this->entityManager->expects($this->never())
            ->method('remove')
            ->with($session);

        $service = $this->createMicrosoftTeamsSessionService();

        $response = $service->deleteSession($session);
        $this->assertSame($providerResponse, $response);
        $this->assertFalse($response->isSuccess());
        $this->assertInstanceOf(ProviderGenericException::class, $response->getThrown());
    }

    public function provideMethodsForValidationErrors(): \Generator
    {
        yield ['createSession', [new MicrosoftTeamsSession($this->createMicrosoftTeamsConfigurationHolder())]];
        yield ['updateSession', [new MicrosoftTeamsSession($this->createMicrosoftTeamsConfigurationHolder())]];
        yield ['deleteSession', [new MicrosoftTeamsSession($this->createMicrosoftTeamsConfigurationHolder())]];
    }

    /**
     * @dataProvider provideMethodsForValidationErrors
     */
    public function testValidatorExceptionNeverCallsConnector($method, array $parameters = [])
    {
        $list = new ConstraintViolationList();
        $list->add($this->createMock(ConstraintViolation::class));

        $this->validator->expects($this->once())
            ->method('validate')
            ->willReturn($list);

        $this->expectException(ProviderServiceValidationException::class);

        $this->microsoftTeamsConnector->expects($this->never())
            ->method('call');

        $service = $this->createMicrosoftTeamsSessionService();
        $service->$method(...$parameters);
    }
}
