<?php

namespace App\Tests\Unit\Service;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Cisco\WebexRestParticipant;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\MiddlewareManagedEntityInterface;
use App\Exception\ProviderServiceValidationException;
use App\Exception\UnknownMethodException;
use App\Exception\UnknownProviderConfigurationHolderException;
use App\Model\ProviderResponse;
use App\Repository\ProviderSessionRepository;
use App\Service\AdobeConnectService;
use App\Service\MicrosoftTeamsService;
use App\Service\MiddlewareService;
use App\Service\WebexRestMeetingService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class MiddlewareServiceTest extends TestCase
{
    private MockObject|AdobeConnectService     $adobeConnectService;
    private MockObject|WebexRestMeetingService     $webexRestSessionService;
    private MockObject|MicrosoftTeamsService $microsoftTeamsService;
    private array $providerServices;
    private MockObject|LoggerInterface $logger;
    private MockObject|ProviderSessionRepository $providerSessionRepository;
    private MockObject|EventDispatcherInterface $eventDispatcher;

    public function setUp(): void
    {
        $mockedMethods = [
            'createSession', 'updateSession', 'deleteSession',
            'createParticipant', 'updateParticipant', 'deleteParticipant',
            'updateParticipantSessionRole', 'redirectParticipantSessionRole',
        ];
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->adobeConnectService = $this->createPartialMock(AdobeConnectService::class, $mockedMethods);
        $this->webexRestSessionService = $this->createPartialMock(WebexRestMeetingService::class, $mockedMethods);
        $this->microsoftTeamsService = $this->createPartialMock(MicrosoftTeamsService::class, $mockedMethods);

        $this->providerServices = [
            $this->adobeConnectService,
            $this->webexRestSessionService,
            $this->microsoftTeamsService,
        ];

        $this->providerSessionRepository = $this->createMock(ProviderSessionRepository::class);
        $this->providerSessionRepository->expects($this->any())->method('find')->willReturn(new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()));

        $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
    }

    public function getAdobeConnectConfigurationHolder(): AdobeConnectConfigurationHolder
    {
        return (new AdobeConnectConfigurationHolder())
            ->setSubdomain('subdomain')
            ->setUsername('username')
            ->setPassword('password')
            ->setClient(new Client());
    }

    public function getWebexRestConfigurationHolder(): WebexRestConfigurationHolder
    {
        return (new WebexRestConfigurationHolder())
            ->setUsername('site_name')
            ->setPassword('password')
            ->setSiteName('site_name')
            ->setClient(new Client());
    }

    public function getMicrosoftTeamsConfigurationHolder(): MicrosoftTeamsConfigurationHolder
    {
        return (new MicrosoftTeamsConfigurationHolder())
            ->setTokenEndpoint('')
            ->setTenantId('')
            ->setScope('')->setGrantType('')
            ->setOrganizerEmail('')
            ->setOrganizerUniqueIdentifier('')
            ->setClientId('')
            ->setClientSecret('')
            ->setClient(new Client());
    }

    protected function createMiddlewareService(): MiddlewareService
    {
        return new MiddlewareService($this->providerServices, $this->logger, $this->eventDispatcher);
    }

    public function testCreateWithAdobeConnectSessionModelCallsService()
    {
        $this->adobeConnectService->expects($this->once())
            ->method('createSession')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_CREATE_SESSION, $model);

        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testUpdateWithAdobeConnectSessionModelCallsService()
    {
        $this->adobeConnectService->expects($this->once())
            ->method('updateSession')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_UPDATE_SESSION, $model);

        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testCreateWithAdobeConnectParticipantModelCallsService()
    {
        $this->adobeConnectService->expects($this->once())
            ->method('createParticipant')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new AdobeConnectPrincipal($this->getAdobeConnectConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_CREATE_PARTICIPANT, $model);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testUpdateWithAdobeConnectParticipantModelCallsService()
    {
        $this->adobeConnectService->expects($this->once())
            ->method('updateParticipant')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new AdobeConnectPrincipal($this->getAdobeConnectConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_UPDATE_PARTICIPANT, $model);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testCreateWebexRestMeetingCallsService()
    {
        $this->webexRestSessionService->expects($this->once())
            ->method('createSession')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new WebexRestMeeting($this->getWebexRestConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_CREATE_SESSION, $model);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testCreateWebexRestParticipantCallsService()
    {
        $this->webexRestSessionService->expects($this->once())
            ->method('createParticipant')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new WebexRestParticipant($this->getWebexRestConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_CREATE_PARTICIPANT, $model);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testUpdateWebexRestMeeting()
    {
        $this->webexRestSessionService->expects($this->once())
            ->method('updateSession')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new WebexRestMeeting($this->getWebexRestConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_UPDATE_SESSION, $model);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testUpdateWebexRestParticipant()
    {
        $this->webexRestSessionService->expects($this->once())
            ->method('updateParticipant')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new WebexRestParticipant($this->getWebexRestConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_UPDATE_PARTICIPANT, $model);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testDeleteAdobeConnectPrincipal()
    {
        $this->adobeConnectService->expects($this->once())
            ->method('deleteParticipant')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new AdobeConnectPrincipal($this->getAdobeConnectConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_DELETE_PARTICIPANT, $model);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testDeleteWebexRestMeeting()
    {
        $this->webexRestSessionService->expects($this->once())
            ->method('deleteSession')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new WebexRestMeeting($this->getWebexRestConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_DELETE_SESSION, $model);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testDeleteWebexRestParticipant()
    {
        $this->webexRestSessionService->expects($this->once())
            ->method('deleteParticipant')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new WebexRestParticipant($this->getWebexRestConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_DELETE_PARTICIPANT, $model);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testCreateMicrosoftTeamsSessionCallsService()
    {
        $this->microsoftTeamsService->expects($this->once())
            ->method('createSession')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new MicrosoftTeamsSession($this->getMicrosoftTeamsConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_CREATE_SESSION, $model);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testUpdateMicrosoftTeamsSessionCallsService()
    {
        $this->microsoftTeamsService->expects($this->once())
            ->method('updateSession')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new MicrosoftTeamsSession($this->getMicrosoftTeamsConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_UPDATE_SESSION, $model);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testDeleteMicrosoftTeamsSessionCallsService()
    {
        $this->microsoftTeamsService->expects($this->once())
            ->method('deleteSession')
            ->willReturn((new ProviderResponse())->setSuccess(true));

        $service = $this->createMiddlewareService();

        $model = new MicrosoftTeamsSession($this->getMicrosoftTeamsConfigurationHolder());

        $response = $service->call(MiddlewareService::MIDDLEWARE_ACTION_DELETE_SESSION, $model);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
    }

    public function testUnsupportedFunction()
    {
        $service = $this->createMiddlewareService();

        $response = $service->call('unknownMethod', new AdobeConnectSCO($this->getAdobeConnectConfigurationHolder()));

        $this->assertFalse($response->isSuccess());
        $this->assertFalse($response->isFromProvider());
        $this->assertInstanceOf(UnknownMethodException::class, $response->getData());
    }

    public function createFakeMiddlewareManagedEntity(): MiddlewareManagedEntityInterface
    {
        return new class() implements MiddlewareManagedEntityInterface {
            public function getAbstractProviderConfigurationHolder(): AbstractProviderConfigurationHolder
            {
                return new class() extends AbstractProviderConfigurationHolder {
                    public function isValid(): bool
                    {
                        return true;
                    }
                };
            }
        };
    }

    public function testUnsupportedMiddlewareManagedEntity()
    {
        $service = $this->createMiddlewareService();
        $response = $service->call(
            MiddlewareService::MIDDLEWARE_ACTION_CREATE_SESSION,
            $this->createFakeMiddlewareManagedEntity()
        );

        $this->assertFalse($response->isSuccess());
        $this->assertFalse($response->isFromProvider());
        $this->assertInstanceOf(UnknownProviderConfigurationHolderException::class, $response->getData());
    }

    public function testThrowValidationException()
    {
        $list = new ConstraintViolationList();
        $list->add($this->createMock(ConstraintViolation::class));

        $exception = new ProviderServiceValidationException($list);

        $this->webexRestSessionService->expects($this->once())
            ->method($this->anything())
            ->willThrowException($exception);

        $service = $this->createMiddlewareService();
        $response = $service->call(
            MiddlewareService::MIDDLEWARE_ACTION_CREATE_PARTICIPANT,
            new WebexRestParticipant($this->getWebexRestConfigurationHolder())
        );

        $this->assertFalse($response->isSuccess());
        $this->assertSame($list, $response->getData());
        $this->assertFalse($response->isFromProvider());
    }
}
