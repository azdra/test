<?php

namespace App\Tests\Unit\Service;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Bounce;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\BounceService;
use App\Service\MailerService;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Contracts\Translation\TranslatorInterface;

class BounceServiceTest extends TestCase
{
    private LoggerInterface|MockObject $logger;
    private ProviderSessionRepository $sessionRepository;
    private UserRepository|MockObject $userRepository;
    private MailerService|MockObject $mailerService;
    private TranslatorInterface|MockObject $translator;
    private MailerInterface|MockObject $mailer;
    protected string $idfuseApiToken = '123654789';
    protected string $idfuseEndPointEnv = 'https://app.idfuse.fr/';
    protected string $brevoMailerApiKey = '9874563210';

    protected function setUp(): void
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->sessionRepository = $this->createMock(ProviderSessionRepository::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->mailerService = $this->createMock(MailerService::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->mailer = $this->createMock(MailerInterface::class);
    }

    /**
     * @dataProvider createBounceData
     */
    public function testCreateBounce(string $name, array $messageDatas, $reason): void
    {
        $this->userRepository->expects($this->exactly(1))
            ->method('find')
            ->willReturn((new User())->setClient(new Client()));

        $this->sessionRepository->expects($this->atMost(1))
            ->method('find')
            ->willReturn(new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient((new Client())->setName('testClient'))));

        $this->logger->expects(self::once())
            ->method('info');
        $bounceService = $this->createBounce(['sendEmailForInvalidMail']);
        $bounceService->expects(self::once())
            ->method('sendEmailForInvalidMail');
        $bounce = $bounceService->createBounce($name, $messageDatas);

        $this->assertInstanceOf(Bounce::class, $bounce);
        $this->assertSame($messageDatas['metadata']['origin'], $bounce->getMailOrigin());
        $this->assertSame($reason, $bounce->getReason());
    }

    protected function createBounce(array $onlyMockedMethods = []): BounceService|MockObject
    {
        return $this->getMockBuilder(BounceService::class)
            ->setConstructorArgs([
                $this->userRepository,
                $this->sessionRepository,
                $this->mailerService,
                $this->translator,
                $this->logger,
                $this->idfuseApiToken,
                $this->idfuseEndPointEnv,
                $this->brevoMailerApiKey,
            ])
            ->onlyMethods($onlyMockedMethods)
            ->getMock();
    }

    public function createBounceData(): \Generator
    {
        $message = [
            'ts' => 1365109999,
            'subject' => 'This an example webhook message',
            'email' => 'example.webhook@mandrillapp.com',
            'sender' => 'example.sender@mandrillapp.com',
            'tags' => [
                'webhook-example',
            ],
            'opens' => [],
            'clicks' => [],
            'state' => 'deferred',
            'metadata' => [
                'origin' => 'Rénitialisation de mot de passe',
                'user' => '3',
                'session' => 12,
            ],
            '_id' => 'exampleaaaaaaaaaaaaaaaaaaaaaaaaa',
            '_version' => 'exampleaaaaaaaaaaaaaaa',
            'smtp_events' => [
                [
                    'destination_ip' => '127.0.0.1',
                    'diag' => '451 4.3.5 Temporarily unavailable, try again later.',
                    'source_ip' => '127.0.0.1',
                    'ts' => 1365111111,
                    'type' => 'deferred',
                    'size' => 0,
                ],
            ],
        ];

        yield ['mandrill.message.deferral', $message, 'Deferred'];

        $message = [
              'ts' => 1634135838,
              '_id' => '1a2f7e0edeaa4aa8bfb06a0fbbb318fc',
              'state' => 'soft-bounced',
              'subject' => 'Réinitialisation de votre mot de passe',
              'email' => 'soft_bounce@test.mandrillapp.com',
              'tags' => [],
              'smtp_events' => [],
              'resends' => [],
              '_version' => 'Quw7fxgrVP04WVoPX1cgZA',
              'diag' => 'smtp;550 Test soft bounce generated by Mandrill',
              'bgtools_code' => 20,
              'metadata' => [
                'origin' => 'Rénitialisation de mot de passe',
                'user' => '3',
                'session' => 12,
              ],
              'sender' => 'tests@live-session.fr',
              'template' => null,
              'bounce_description' => 'general',
        ];

        yield ['mandrill.message.soft_bounce', $message, 'Temporary error'];

        $message = [
            'ts' => 1634136270,
            '_id' => 'ba211ae22fba469aad0992ab54965f6a',
            'state' => 'bounced',
            'subject' => 'Réinitialisation de votre mot de passe',
            'email' => 'hard_bounce@test.mandrillapp.com',
            'tags' => [],
            'smtp_events' => [],
            'resends' => [],
            '_version' => 'A2oR-gxxpBA6mjXUXeUlcA',
            'diag' => 'smtp;550 Test hard bounce generated by Mandrill',
            'bgtools_code' => 10,
            'metadata' => [
                'origin' => 'Rénitialisation de mot de passe',
                'user' => '25',
                'session' => '4',
            ],
            'sender' => 'tests@live-session.fr',
            'template' => null,
            'bounce_description' => 'bad_mailbox',
        ];

        yield ['mandrill.message.hard_bounce', $message, 'Email error'];
    }

    /**
     * @dataProvider failedCreateBounceData
     */
    public function testFailedBounceData(string $name, array $messageDatas): void
    {
        $this->sessionRepository->expects(self::never())
            ->method('find');

        $this->logger->expects(self::never())
            ->method('info');

        $this->userRepository->expects(self::atMost(1))
            ->method('find');

        $bounceService = new BounceService(
            $this->userRepository,
            $this->sessionRepository,
            $this->mailerService,
            $this->translator,
            $this->logger,
            $this->idfuseApiToken,
            $this->idfuseEndPointEnv,
            $this->brevoMailerApiKey,
        );

        $this->expectException(UserNotFoundException::class);
        $this->expectExceptionMessage('User return by Mandrill Webhook was not found. No bounce created.');

        $bounceService->createBounce($name, $messageDatas);
    }

    public function failedCreateBounceData(): \Generator
    {
        $message = [
            'ts' => 1365109999,
            'subject' => 'This an example webhook message',
            'email' => 'example.webhook@mandrillapp.com',
            'sender' => 'example.sender@mandrillapp.com',
            'tags' => [
                'webhook-example',
            ],
            'opens' => [],
            'clicks' => [],
            'state' => 'deferred',
            '_id' => 'exampleaaaaaaaaaaaaaaaaaaaaaaaaa',
            '_version' => 'exampleaaaaaaaaaaaaaaa',
            'smtp_events' => [
                [
                    'destination_ip' => '127.0.0.1',
                    'diag' => '451 4.3.5 Temporarily unavailable, try again later.',
                    'source_ip' => '127.0.0.1',
                    'ts' => 1365111111,
                    'type' => 'deferred',
                    'size' => 0,
                ],
            ],
        ];

        yield '#0 No metadata return by mandrill => cannot found associated user' => ['mandrill.message.deferral', $message];

        $message['metadata'] = [];
        yield '#1 Metadatas is set but empty' => ['mandrill.message.deferral', $message];

        $message['metadata'] = [
            'origin' => 'sendResetPasswordRequest',
            'user' => '3',
            'session' => '1',
        ];

        yield '#2 Metadatas passend but no user associated found' => ['mandrill.message.deferral', $message];
    }

    /**
     * @dataProvider sendEmailForInvalidMaileData
     */
    public function testSendEmailForInvalidMail(ProviderSession $session, string $subject, string $emailInvalid, bool $sendEmail, User $user): void
    {
        ($sendEmail) ?
            ($this->mailerService->expects(self::once())->method('sendTemplatedMail')) :
            ($this->mailerService->expects(self::never())->method('sendTemplatedMail'));

        $bounceService = $this->createBounce();
        $bounceService->sendEmailForInvalidMail($session, $subject, $emailInvalid, $user);
    }

    public function sendEmailForInvalidMaileData(): \Generator
    {
        $preferencesEnable['EmailInvalidNotification'] = true;
        $preferencesDisable['EmailInvalidNotification'] = false;

        $user = $this->createUser();

        $session = $this->createConfiguredSession(role: '', preferences: [], mailUser: '', withManagers: false);
        yield '#0 client empty manager list' => [$session, 'subject mail invalid', 'empty.manager.invalid@invalid.com', false, $user];

        $session1 = $this->createConfiguredSession(role: 'ROLE_MANAGER', preferences: $preferencesEnable, mailUser: 'mailManagerTest@test.com', withManagers: true);
        yield '#1 client with manager enable to notification email invalid' => [$session1, 'subject mail invalid', 'manager-valid@invalid.com', true, $user];

        $session2 = $this->createConfiguredSession(role: 'ROLE_MANAGER', preferences: $preferencesDisable, mailUser: 'mailManagerTest@test.com', withManagers: true);
        yield '#2 client without manager enable to notification email invalid' => [$session2, 'subject mail invalid', 'manager-non-valid@invalid.com', false, $user];

        $session3 = $this->createConfiguredSession(role: 'ROLE_TRAINEE', preferences: $preferencesEnable, mailUser: 'mailManagerTest@test.com', withManagers: true);
        yield '#3 client with trainee enable to notification email invalid' => [$session3, 'subject mail invalid', 'trainee-valid@invalid.com', false, $user];

        $session4 = $this->createConfiguredSession(role: 'ROLE_MANAGER', preferences: $preferencesEnable, mailUser: 'manager-valid-same-mail@invalid.com', withManagers: true);
        yield '#4 manager have the same email with the invalid email bounce' => [$session4, 'subject mail invalid', 'manager-valid-same-mail@invalid.com', false, $user];
    }

    public function createConfiguredSession(string $role, array $preferences, string $mailUser, bool $withManagers): ProviderSession
    {
        $client = (new Client())->setName('TEST Client');
        if ($withManagers) {
            $manager = (new User())->setClient($client)->setRoles([$role])->setId(1)->setPreferences($preferences)->setEmail($mailUser);
            $listeManager = (new ArrayCollection());
            $listeManager->add($manager);
            $client->setUsers($listeManager);
        }
        $providerSession = new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient($client));
        $providerSession->getAbstractProviderConfigurationHolder()->setClient($client);

        return $providerSession->setId(1);
    }

    public function createUser(): User
    {
        return (new User())->setFirstName('Jean')->setLastName('Pierre');
    }
}
