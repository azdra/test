<?php

namespace App\Tests\Unit\Service\Provider\MicrosoftTeams;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Repository\MicrosoftTeamsRepository;
use App\Repository\UserRepository;
use App\Service\Provider\Microsoft\MicrosoftTeamsLicensingService;
use App\Service\Provider\Microsoft\MicrosoftTeamsParticipantCreator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MicrosoftTeamsLicensingServiceTest extends TestCase
{
    private MicrosoftTeamsRepository $microsoftTeamsRepository;
    private UserRepository $userRepository;
    private MicrosoftTeamsParticipantCreator|MockObject $microsoftTeamsParticipantCreator;

    public function setUp(): void
    {
        $this->microsoftTeamsRepository = $this->createMock(MicrosoftTeamsRepository::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->microsoftTeamsParticipantCreator = $this->createMock(MicrosoftTeamsParticipantCreator::class);
    }

    // <editor-folder> Global generators
    private function createMicrosoftTeamsLicensingService(array $onlyMockedMethods = []): MicrosoftTeamsLicensingService|MockObject
    {
        return $this->getMockBuilder(MicrosoftTeamsLicensingService::class)
            ->setConstructorArgs([
                $this->microsoftTeamsRepository,
                $this->userRepository,
                $this->microsoftTeamsParticipantCreator,
            ])
            ->onlyMethods($onlyMockedMethods)
            ->getMock();
    }

    private function generateConfigurationHolder(array $data = []): MicrosoftTeamsConfigurationHolder
    {
        return (new MicrosoftTeamsConfigurationHolder())
            ->setTenantId('my_tenant')
            ->setLicences($data['licences'] ?? [])
            ->setAutomaticLicensing($data['automaticLicensing'] ?? true)
            ->setClient((new Client())->setName('My client'));
    }

    private function generateSession(array $data = []): MicrosoftTeamsSession
    {
        $configurationHolder = $this->generateConfigurationHolder($data);

        return (new MicrosoftTeamsSession($configurationHolder))
            ->setDateStart(new \DateTime($data['sessionDateStart'] ?? 'today 09:00:00'))
            ->setDateEnd(new \DateTime($data['sessionDateEnd'] ?? 'today 10:00:00'))
            ->setLicence($data['licence'] ?? 'otherLicence@gg.com')
            ->setName('My name')
            ->setCandidates([]);
    }
    // </editor-folder> Global generators

    // <editor-folder> licensingASession
    public function testLicensingASessionWithALicence(): void
    {
        $session = $this->generateSession();
        $license = 'otherLicence@gg.com';

        $microsoftTeamsLicensingService = $this->createMicrosoftTeamsLicensingService(['getNextAvailableLicence', 'addParticipantRole']);

        $this->expectException(ProviderSessionRequestHandlerException::class);
        $this->expectExceptionMessage('You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');

        $microsoftTeamsLicensingService->licensingASession($session);
    }

    public function testLicensingASessionWithoutLicenceThrowAnError(): void
    {
        $session = $this->generateSession();
        $session->setLicence(null);
        $initialLicence = $session->getLicence();

        $microsoftTeamsLicensingService = $this->createMicrosoftTeamsLicensingService(['getNextAvailableLicence']);

        $this->expectException(ProviderSessionRequestHandlerException::class);
        $this->expectExceptionMessage('You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');

        $microsoftTeamsLicensingService->licensingASession($session);
        $this->assertEquals($initialLicence, $session->getLicence());
    }
    // </editor-folder> licensingASession

    // <editor-folder> getNextAvailableLicence
    public function availableLicencesGenerator(): \Generator
    {
        yield ['licenceTarget', [
            'licenceTarget' => [
                'iv' => '/FzneE0kePpC7+sL',
                'tag' => 'Ngc+3j3aHFL3hyJCuGw0kw==',
                'shared' => true,
                'private' => false,
                'password' => 'Z1V5Y2I5aU5uY3FZT2xLQ01uNVU=',
            ],
        ]];
        yield ['licenceTarget', [
            'licenceTarget' => [
                'iv' => '/FzneE0kePpC7+sL',
                'tag' => 'Ngc+3j3aHFL3hyJCuGw0kw==',
                'shared' => true,
                'private' => false,
                'password' => 'Z1V5Y2I5aU5uY3FZT2xLQ01uNVU=',
            ],
            'otherLicence' => [
                'iv' => '/FzneE0kePpC7+sL',
                'tag' => 'Ngc+3j3aHFL3hyJCuGw0kw==',
                'shared' => true,
                'private' => false,
                'password' => 'Z1V5Y2I5aU5uY3FZT2xLQ01uNVU=',
            ],
        ]];
    }

    /**
     * @dataProvider availableLicencesGenerator
     */
    public function testNextLicenceAlwaysReturnSomething(string $expectedLicence, array $availableLicences): void
    {
        $microsoftTeamsLicensingService = $this->createMicrosoftTeamsLicensingService(['getAvailableLicences']);
        $microsoftTeamsLicensingService->expects($this->once())
                                     ->method('getAvailableLicences')
                                     ->willReturn($availableLicences);

        $configurationHolder = $this->generateConfigurationHolder();

        $this->assertEquals($expectedLicence, $microsoftTeamsLicensingService->getNextAvailableLicence($configurationHolder, new \DateTime(), new \DateTime(), 0));
    }

    public function testNextLicenceRerunNullWhenNoMoreLicenceAvailable(): void
    {
        $microsoftTeamsLicensingService = $this->createMicrosoftTeamsLicensingService(['getAvailableLicences']);
        $microsoftTeamsLicensingService->expects($this->once())
                                     ->method('getAvailableLicences')
                                     ->willReturn([]);

        $configurationHolder = $this->generateConfigurationHolder();

        $this->assertNull($microsoftTeamsLicensingService->getNextAvailableLicence($configurationHolder, new \DateTime(), new \DateTime(), 0));
    }
    // </editor-folder> getNextAvailableLicence

    // <editor-folder> getAvailableLicences

    public function generateUsedLicences(): \Generator
    {
        yield [[
            'licence1@gg.com' => [
                'iv' => '/FzneE0kePpC7+sL',
                'tag' => 'Ngc+3j3aHFL3hyJCuGw0kw==',
                'shared' => 'true',
                'private' => 'false',
                'password' => 'Z1V5Y2I5aU5uY3FZT2xLQ01uNVU=',
            ],
            'licence2@gg.com' => [
                'iv' => '/FzneE0kePpC7+sL',
                'tag' => 'Ngc+3j3aHFL3hyJCuGw0kw==',
                'shared' => 'true',
                'private' => 'false',
                'password' => 'Z1V5Y2I5aU5uY3FZT2xLQ01uNVU=',
            ],
            'licence3@gg.com' => [
                'iv' => '/FzneE0kePpC7+sL',
                'tag' => 'Ngc+3j3aHFL3hyJCuGw0kw==',
                'shared' => 'true',
                'private' => 'false',
                'password' => 'Z1V5Y2I5aU5uY3FZT2xLQ01uNVU=',
            ],
        ], [], true];
        yield [[
                'licence1@gg.com' => [
                    'iv' => '/FzneE0kePpC7+sL',
                    'tag' => 'Ngc+3j3aHFL3hyJCuGw0kw==',
                    'shared' => 'true',
                    'private' => 'false',
                    'password' => 'Z1V5Y2I5aU5uY3FZT2xLQ01uNVU=',
                ],
                'licence2@gg.com' => [
                    'iv' => '/FzneE0kePpC7+sL',
                    'tag' => 'Ngc+3j3aHFL3hyJCuGw0kw==',
                    'shared' => 'true',
                    'private' => 'false',
                    'password' => 'Z1V5Y2I5aU5uY3FZT2xLQ01uNVU=',
                ],
            ], [
            ['id' => 1, 'licence' => 'licence3@gg.com'],
        ], true];
        yield [[], [
            ['id' => 1, 'licence' => 'licence1@gg.com'],
            ['id' => 2, 'licence' => 'licence2@gg.com'],
            ['id' => 3, 'licence' => 'licence3@gg.com'],
        ], true];
    }

    /**
     * @dataProvider generateUsedLicences
     */
    public function testAvailableLicenceDoNotIncludesUsedLicences(array $expectedLicences, $usedLicences, $isFound): void
    {
        $this->microsoftTeamsRepository->expects($this->once())
            ->method('getUsedLicencesOnSlot')
            ->willReturn($usedLicences);

        $configurationHolder = $this->generateConfigurationHolder(['licences' => [
            'licence1@gg.com' => [
                'iv' => '/FzneE0kePpC7+sL',
                'tag' => 'Ngc+3j3aHFL3hyJCuGw0kw==',
                'shared' => 'true',
                'private' => 'false',
                'password' => 'Z1V5Y2I5aU5uY3FZT2xLQ01uNVU=',
            ],
            'licence2@gg.com' => [
                'iv' => '/FzneE0kePpC7+sL',
                'tag' => 'Ngc+3j3aHFL3hyJCuGw0kw==',
                'shared' => 'true',
                'private' => 'false',
                'password' => 'Z1V5Y2I5aU5uY3FZT2xLQ01uNVU=',
            ],
            'licence3@gg.com' => [
                'iv' => '/FzneE0kePpC7+sL',
                'tag' => 'Ngc+3j3aHFL3hyJCuGw0kw==',
                'shared' => 'true',
                'private' => 'false',
                'password' => 'Z1V5Y2I5aU5uY3FZT2xLQ01uNVU=',
            ],
        ]]);

        $microsoftTeamsLicensingService = $this->createMicrosoftTeamsLicensingService();
        $availableLicences = $microsoftTeamsLicensingService->getAvailableLicences($configurationHolder, new \DateTime(), new \DateTime(), 0);

        foreach ($expectedLicences as $licence => $params) {
            $result = array_key_exists($licence, $availableLicences);
            $this->assertSame($isFound, $result);
        }
    }
    // </editor-folder> getAvailableLicences
}
