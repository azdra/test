<?php

namespace App\Tests\Unit\Service\Provider\Webex;

use App\Entity\Cisco\WebexMeeting;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\UserRepository;
use App\Repository\WebexMeetingRepository;
use App\Service\Provider\Webex\WebexLicensingService;
use App\Service\Provider\Webex\WebexParticipantCreator;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class WebexLicensingServiceTest extends TestCase
{
    private WebexMeetingRepository $webexMeetingRepository;
    private UserRepository $userRepository;
    private WebexParticipantCreator|MockObject $webexParticipantCreator;
    private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository;
    private EntityManagerInterface $entityManager;

    public function setUp(): void
    {
        $this->webexMeetingRepository = $this->createMock(WebexMeetingRepository::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->webexParticipantCreator = $this->createMock(WebexParticipantCreator::class);
        $this->providerParticipantSessionRoleRepository = $this->createMock(ProviderParticipantSessionRoleRepository::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
    }

    // <editor-folder> Global generators
    private function createWebexMeetingLicensingService(array $onlyMockedMethods = []): WebexLicensingService|MockObject
    {
        return $this->getMockBuilder(WebexLicensingService::class)
            ->setConstructorArgs([
                $this->webexMeetingRepository,
                $this->userRepository,
                $this->webexParticipantCreator,
                $this->providerParticipantSessionRoleRepository,
                $this->entityManager,
            ])
            ->onlyMethods($onlyMockedMethods)
            ->getMock();
    }

    private function generateConfigurationHolder(array $data = []): WebexConfigurationHolder
    {
        return (new WebexConfigurationHolder())
            ->setLicences($data['licences'] ?? [])
            ->setAutomaticLicensing($data['automaticLicensing'] ?? true)
            ->setClient((new Client())->setName('My client'));
    }

    private function generateSession(array $data = []): WebexMeeting
    {
        $configurationHolder = $this->generateConfigurationHolder($data);

        return (new WebexMeeting($configurationHolder))
            ->setDateStart(new \DateTime($data['sessionDateStart'] ?? 'today 09:00:00'))
            ->setDateEnd(new \DateTime($data['sessionDateEnd'] ?? 'today 10:00:00'))
            ->setLicence($data['licence'] ?? 'otherLicence@gg.com')
            ->setName('My name');
    }
    // </editor-folder> Global generators

    // <editor-folder> licensingASession
    public function testLicensingASessionWithALicence(): void
    {
        $session = $this->generateSession();
        $license = 'myLicence@gg.com';

        $webexMeetingLicensingService = $this->createWebexMeetingLicensingService(['getNextAvailableLicence', 'addParticipantRole']);
        $webexMeetingLicensingService->expects($this->once())
            ->method('getNextAvailableLicence')
            ->willReturn($license);

        $returnedLicence = $webexMeetingLicensingService->licensingASession($session);

        $this->assertEquals($license, $session->getLicence());
        $this->assertEquals($license, $returnedLicence);
    }

    public function testLicensingASessionWithoutLicenceThrowAnError(): void
    {
        $session = $this->generateSession();
        $initialLicence = $session->getLicence();

        $webexMeetingLicensingService = $this->createWebexMeetingLicensingService(['getNextAvailableLicence']);
        $webexMeetingLicensingService->expects($this->once())
                                     ->method('getNextAvailableLicence')
                                     ->willReturn(null);

        $this->expectException(ProviderSessionRequestHandlerException::class);
        $this->expectExceptionMessage('You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');

        $webexMeetingLicensingService->licensingASession($session);

        $this->assertEquals($initialLicence, $session->getLicence());
    }
    // </editor-folder> licensingASession

    // <editor-folder> getNextAvailableLicence
    public function availableLicencesGenerator(): \Generator
    {
        yield ['licenceTarget', ['licenceTarget']];
        yield ['licenceTarget', ['licenceTarget', 'otherLicence']];
    }

    /**
     * @dataProvider availableLicencesGenerator
     */
    public function testNextLicenceAlwaysReturnSomething(string $expectedLicence, array $availableLicences): void
    {
        $webexMeetingLicensingService = $this->createWebexMeetingLicensingService(['getAvailableLicences']);
        $webexMeetingLicensingService->expects($this->once())
                                     ->method('getAvailableLicences')
                                     ->willReturn($availableLicences);

        $configurationHolder = $this->generateConfigurationHolder();

        $this->assertEquals($expectedLicence, $webexMeetingLicensingService->getNextAvailableLicence($configurationHolder, new \DateTime(), new \DateTime(), 0));
    }

    public function testNextLicenceRerunNullWhenNoMoreLicenceAvailable(): void
    {
        $webexMeetingLicensingService = $this->createWebexMeetingLicensingService(['getAvailableLicences']);
        $webexMeetingLicensingService->expects($this->once())
                                     ->method('getAvailableLicences')
                                     ->willReturn([]);

        $configurationHolder = $this->generateConfigurationHolder();

        $this->assertNull($webexMeetingLicensingService->getNextAvailableLicence($configurationHolder, new \DateTime(), new \DateTime(), 0));
    }
    // </editor-folder> getNextAvailableLicence

    // <editor-folder> getAvailableLicences

    public function generateUsedLicences(): \Generator
    {
        yield [['licence1@gg.com', 'licence2@gg.com', 'licence3@gg.com'], []];
        yield [['licence1@gg.com', 'licence2@gg.com'], [
            ['id' => 1, 'licence' => 'licence3@gg.com'],
        ]];
        yield [[], [
            ['id' => 1, 'licence' => 'licence1@gg.com'],
            ['id' => 2, 'licence' => 'licence2@gg.com'],
            ['id' => 3, 'licence' => 'licence3@gg.com'],
        ]];
    }

    /**
     * @dataProvider generateUsedLicences
     */
    public function testAvailableLicenceDoNotIncludesUsedLicences(array $expectedLicences, $usedLicences): void
    {
        $this->webexMeetingRepository->expects($this->once())
            ->method('getUsedLicencesOnSlot')
            ->willReturn($usedLicences);

        $configurationHolder = $this->generateConfigurationHolder(['licences' => [
            'licence1@gg.com',
            'licence2@gg.com',
            'licence3@gg.com',
        ]]);

        $webexMeetingLicensingService = $this->createWebexMeetingLicensingService();

        $this->assertEquals($expectedLicences, $webexMeetingLicensingService->getAvailableLicences($configurationHolder, new \DateTime(), new \DateTime(), 0));
    }
    // </editor-folder> getAvailableLicences
}
