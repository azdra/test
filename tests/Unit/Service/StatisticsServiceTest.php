<?php

namespace App\Tests\Unit\Service;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\CategorySessionType;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Repository\ProviderSessionRepository;
use App\Service\StatisticsService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class StatisticsServiceTest extends TestCase
{
    private ProviderSessionRepository|MockObject $sessionRepository;
    private EntityManagerInterface|MockObject $entityManager;

    public function setUp(): void
    {
        $this->sessionRepository = $this->createMock(ProviderSessionRepository::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
    }

    public function getService(): StatisticsService
    {
        return new StatisticsService($this->entityManager);
    }

    public function statsSessionForPeriodData()
    {
        yield 'Status test : count scheduled session' => [[$this->createScheduledSession()], ['byState.Scheduled' => 1, 'total.count' => 1, 'total.duration' => 72, 'byCategory.Formation' => 1]];

        yield 'Status test : count sent session' => [[$this->createSentSession()], ['byState.Sent' => 1, 'total.count' => 1, 'total.duration' => 72, 'byCategory.Formation' => 1]];

        yield 'Status test : count running session' => [[$this->createRunningSession()], ['byState.Running' => 1, 'total.count' => 1, 'total.duration' => 240, 'byCategory.Formation' => 1]];

        yield 'Status test : count finished session' => [[$this->createFinishedSession()], ['byState.Finished' => 1, 'total.count' => 1, 'total.duration' => 72, 'byCategory.Formation' => 1]];

        yield 'Status test : count cancelled session' => [[$this->createCancelledSession()], ['byState.Cancelled' => 1, 'total.count' => 1, 'byCategory.Formation' => 1]];

        yield 'Status test : count draft session' => [[$this->createDraftSession()], ['byState.Draft' => 1, 'total.count' => 1, 'byCategory.Formation' => 1]];

        yield 'Category test : count formation session' => [[$this->createFormationSession()], ['byCategory.Formation' => 1, 'total.count' => 1, 'byState.Draft' => 1]];

        yield 'Category test : count meeting session' => [[$this->createMeetingSession()], ['byCategory.Meeting' => 1, 'total.count' => 1, 'byState.Draft' => 1]];

        yield 'Category test : count test session' => [[$this->createTestSession()], ['byCategory.Test' => 1, 'total.count' => 1, 'byState.Draft' => 1]];

        yield 'Category test : count face-to-face session' => [[$this->createFaceToFaceSession()], ['byCategory.Face-to-face' => 1, 'total.count' => 1, 'byState.Draft' => 1]];

        yield 'Multiple session : total count and duration ok' => $this->createMultipleSessionData();
    }

    public function testGetStatisticsForPeriodWhenNoSessionFound()
    {
        $start = (new \DateTime())->modify('-10 days')->setTime(0, 0, 1);
        $end = (new \DateTime())->modify('+10 days')->setTime(23, 59, 59);
        $this->sessionRepository->expects($this->never())->method('findSessionForPeriod')
            ->with($start, $end)
            ->willReturn([]);

        $expectedSessionStatistics = $this->buildExpectedSessionStatistics([]);
        $expectedParticipantStatistics = ['total' => ['count' => 0, 'test' => 0, 'attendees' => 0, 'absences' => 0, 'max' => 0], 'byCategory' => []];

        $actualStatistics = $this->getService()->getStatisticsSessionsForPeriod($start, $end, []);

        $this->assertEquals($expectedSessionStatistics, $actualStatistics['sessions']);

        $this->assertEquals($expectedParticipantStatistics, $actualStatistics['participants']);

        $this->assertArrayHasKey('sessions', $actualStatistics['evolution']);
        $this->assertCount(21, $actualStatistics['evolution']['sessions']);

        $this->assertArrayHasKey('participants', $actualStatistics['evolution']);
        $this->assertCount(21, $actualStatistics['evolution']['participants']);
    }

    /** @dataProvider statsSessionForPeriodData */
    public function testGetStatisticsSessionsForPeriod(array $sessionFound, array $expectedStatisticsChange)
    {
        $start = (new \DateTime())->modify('-10 days');
        $end = (new \DateTime())->modify('+10 days');
        $this->sessionRepository->expects($this->never())->method('findSessionForPeriod')
            ->with($start, $end)
            ->willReturn($sessionFound);
        $expectedStatistics = $this->buildExpectedSessionStatistics($expectedStatisticsChange);

        $actualStatistics = $this->getService()->getStatisticsSessionsForPeriod($start, $end, $sessionFound)['sessions'];

        $this->assertEquals($expectedStatistics, $actualStatistics);
    }

    public function testGetStatisticsParticipantsForPeriodCountForFinishedSession()
    {
        $start = (new \DateTime())->modify('-10 days');
        $end = (new \DateTime())->modify('+10 days');
        $sessions = [];
        // addition properly finished session
        $sessions[] = $this->createFinishedSessionWithParticipantAndAttendees(10, 4);
        $sessions[] = $this->createFinishedSessionWithParticipantAndAttendees(15, 5);
        $sessions[] = $this->createFinishedSessionWithParticipantAndAttendees(10, 4, CategorySessionType::CATEGORY_SESSION_REUNION);
        // do not count unfinished session
        $sessions[] = $this->createRunningSessionWithParticipantAndAttendees(10, 4);
        $this->sessionRepository->expects($this->never())->method('findSessionForPeriod')
            ->with($start, $end)
            ->willReturn($sessions);

        $expectedStatistics = [
            'total' => ['count' => 35, 'test' => 0, 'attendees' => 22, 'absences' => 13, 'max' => 15],
            'byCategory' => [
                'Formation' => ['count' => 25, 'test' => 0, 'attendees' => 16, 'absences' => 9, 'max' => 15, 'finishedSession' => 2],
                'Meeting' => ['count' => 10, 'test' => 0, 'attendees' => 6, 'absences' => 4, 'max' => 10, 'finishedSession' => 1],
            ],
        ];

        $actualStatistics = $this->getService()->getStatisticsSessionsForPeriod($start, $end, $sessions)['participants'];

        $this->assertEquals($expectedStatistics, $actualStatistics);
    }

    public function testGetStatisticsEvolutionByDay()
    {
        $start = (new \DateTime())->modify('-10 days')->setTime(0, 0, 1);
        $end = (new \DateTime())->modify('+10 days')->setTime(23, 59, 59);
        $sessions = [];

        $firstStartDate = (new \DateTime())->modify('+4 days');
        $sessions[] = $this->createSessionWithDatesAndParticipant($firstStartDate, 15);
        $sessions[] = $this->createSessionWithDatesAndParticipant($firstStartDate, 10);

        $secondStartDate = (new \DateTime())->modify('-2 days');
        $sessions[] = $this->createSessionWithDatesAndParticipant($secondStartDate, 10);

        $this->sessionRepository->expects($this->never())->method('findSessionForPeriod')
            ->with($start, $end)
            ->willReturn($sessions);

        $actualStatistics = $this->getService()->getStatisticsSessionsForPeriod($start, $end, $sessions)['evolution'];

        $this->assertArrayHasKey('sessions', $actualStatistics);
        $this->assertCount(21, $actualStatistics['sessions']);
        $this->assertEquals(2, $actualStatistics['sessions'][$firstStartDate->format('d/m')]);
        $this->assertEquals(1, $actualStatistics['sessions'][$secondStartDate->format('d/m')]);

        $this->assertArrayHasKey('participants', $actualStatistics);
        $this->assertCount(21, $actualStatistics['participants']);
        $this->assertEquals(25, $actualStatistics['participants'][$firstStartDate->format('d/m')]);
        $this->assertEquals(10, $actualStatistics['participants'][$secondStartDate->format('d/m')]);
    }

    public function testGetStatisticsEvolutionByMont()
    {
        $strDate = new \DateTime();
        $start = (clone $strDate)->modify('-2 months')->modify('first day of this month');
        $end = (clone $strDate)->modify('+2 months')->modify('first day of this month');
        $sessions = [];

        $firstStartDate = (clone $strDate)->modify('-1 month');
        $sessions[] = $this->createSessionWithDatesAndParticipant($firstStartDate, 15);
        $sessions[] = $this->createSessionWithDatesAndParticipant($firstStartDate, 10);

        $secondStartDate = (clone $strDate)->modify('1 month');
        $sessions[] = $this->createSessionWithDatesAndParticipant($secondStartDate, 10);

        $this->sessionRepository->expects($this->never())->method('findSessionForPeriod')
            ->with($start, $end)
            ->willReturn($sessions);

        $actualStatistics = $this->getService()->getStatisticsSessionsForPeriod($start, $end, $sessions)['evolution'];

        $this->assertArrayHasKey('sessions', $actualStatistics);
        $this->assertCount(4, $actualStatistics['sessions']);
        $this->assertEquals(2, $actualStatistics['sessions'][$firstStartDate->format('m/y')]);
        $this->assertEquals(1, $actualStatistics['sessions'][$secondStartDate->format('m/y')]);

        $this->assertArrayHasKey('participants', $actualStatistics);
        $this->assertCount(4, $actualStatistics['participants']);
        $this->assertEquals(25, $actualStatistics['participants'][$firstStartDate->format('m/y')]);
        $this->assertEquals(10, $actualStatistics['participants'][$secondStartDate->format('m/y')]);
    }

    private function createSessionWithDatesAndParticipant(\DateTime $startDate, int $expectedParticipantCount)
    {
        $session = $this->createSession()
            ->setDateStart($startDate);
        $this->subscribeParticipant($session, $expectedParticipantCount, 0);

        return $session;
    }

    private function createFinishedSessionWithParticipantAndAttendees(int $expectedParticipantCount, int $expectedAttendeesCount, CategorySessionType $category = CategorySessionType::CATEGORY_SESSION_FORMATION): ProviderSession
    {
        $session = $this->createFinishedSession();
        $session->setCategory($category);
        $this->subscribeParticipant($session, $expectedParticipantCount, $expectedAttendeesCount);

        return $session;
    }

    private function createRunningSessionWithParticipantAndAttendees(int $expectedParticipantCount, int $expectedAttendeesCount)
    {
        $session = $this->createRunningSession();
        $session->setCategory(CategorySessionType::CATEGORY_SESSION_FORMATION);
        $this->subscribeParticipant($session, $expectedParticipantCount, $expectedAttendeesCount);

        return $session;
    }

    private function subscribeParticipant(ProviderSession $session, int $expectedParticipantCount, int $expectedAttendeesCount): void
    {
        if ($expectedAttendeesCount > $expectedParticipantCount) {
            $expectedAttendeesCount = $expectedParticipantCount;
        }

        for ($i = 0; $i < $expectedParticipantCount; ++$i) {
            $participant = new ProviderParticipantSessionRole();
            $participant->setPresenceStatus($i >= $expectedAttendeesCount);
            $session->addParticipantRole($participant);
        }
    }

    private function buildExpectedSessionStatistics(array $expectedStatisticsChange): array
    {
        $expectedStatistics = [
            'total' => [
                'duration' => 0,
                'count' => 0,
            ],
            'byState' => [
                ProviderSession::FINAL_STATUS_SCHEDULED => 0,
                ProviderSession::FINAL_STATUS_SENT => 0,
                ProviderSession::FINAL_STATUS_RUNNING => 0,
                ProviderSession::FINAL_STATUS_FINISHED => 0,
                ProviderSession::FINAL_STATUS_DRAFT => 0,
                ProviderSession::FINAL_STATUS_CANCELED => 0,
            ],
            'byCategory' => [
                ProviderSession::FINAL_CATEGORY_FORMATION => 0,
                ProviderSession::FINAL_CATEGORY_MEETING => 0,
                ProviderSession::FINAL_CATEGORY_FACE_TO_FACE => 0,
                ProviderSession::FINAL_CATEGORY_MIXTE => 0,
                ProviderSession::FINAL_CATEGORY_WEBINAR => 0,
                ProviderSession::FINAL_CATEGORY_TEST => 0,
                ProviderSession::FINAL_CATEGORY_UNKNOWN => 0,
            ],
        ];

        foreach ($expectedStatisticsChange as $keys => $value) {
            list($firstIndex, $subIndex) = explode('.', $keys);
            $expectedStatistics[$firstIndex][$subIndex] = $value;
        }

        return $expectedStatistics;
    }

    private function createSession(): ProviderSession
    {
        return (new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client())))
            ->setDateStart(new \DateTime())
            ->setDateEnd(new \DateTime());
    }

    private function createScheduledSession(): ProviderSession
    {
        return $this->createSession()->setStatus(ProviderSession::STATUS_SCHEDULED)
            ->setDateStart((new \DateTime())->modify('+5 days'))
            ->setDateEnd((new \DateTime())->modify('+8 days'));
    }

    private function createSentSession(): ProviderSession
    {
        return $this->createScheduledSession()
            ->setNotificationDate(new \DateTimeImmutable())
            ->setLastConvocationSent(new \DateTimeImmutable());
    }

    private function createRunningSession(): ProviderSession
    {
        return $this->createSession()->setStatus(ProviderSession::STATUS_SCHEDULED)
            ->setDateStart((new \DateTime())->modify('-5 days'))
            ->setDateEnd((new \DateTime())->modify('+5 days'));
    }

    private function createFinishedSession(): ProviderSession
    {
        return $this->createSession()->setStatus(ProviderSession::STATUS_SCHEDULED)
            ->setDateStart((new \DateTime())->modify('-8 days'))
            ->setDateEnd((new \DateTime())->modify('-5 days'));
    }

    private function createCancelledSession(): ProviderSession
    {
        return $this->createSession()->setStatus(ProviderSession::STATUS_CANCELED);
    }

    private function createDraftSession(): ProviderSession
    {
        return $this->createSession()->setStatus(ProviderSession::STATUS_DRAFT);
    }

    private function createFormationSession(): ProviderSession
    {
        return $this->createSession()->setCategory(CategorySessionType::CATEGORY_SESSION_FORMATION);
    }

    private function createMeetingSession(): ProviderSession
    {
        return $this->createSession()->setCategory(CategorySessionType::CATEGORY_SESSION_REUNION);
    }

    private function createTestSession(): ProviderSession
    {
        return $this->createSession()->setCategory(CategorySessionType::CATEGORY_SESSION_TEST);
    }

    private function createFaceToFaceSession(): ProviderSession
    {
        return $this->createSession()->setCategory(CategorySessionType::CATEGORY_SESSION_PRESENTIAL);
    }

    private function createMultipleSessionData(): array
    {
        $sessions = [];
        $sessions[] = $this->createSession()
            ->setDateStart((new \DateTime())->modify('+2 hours'))
            ->setDateEnd((new \DateTime())->modify('+3 hours')); // 1 hour
        $sessions[] = $this->createSession()
            ->setDateStart((new \DateTime())->modify('+2 hours'))
            ->setDateEnd((new \DateTime())->modify('+5 hours')); // 3 hour

        $expectedChange = [
            'total.count' => 2,
            'total.duration' => 240, // (1 + 3) * 60
            'byState.Draft' => 2,
            'byCategory.Formation' => 2,
        ];

        return [$sessions, $expectedChange];
    }
}
