<?php

namespace App\Tests\Unit\Service;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\CategorySessionType;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Service\WebCalService;
use Eluceo\iCal\Presentation\Component;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class WebCalServiceTest extends TestCase
{
    public function testGenerateWebCalFile(): void
    {
        $configHolder = (new AdobeConnectConfigurationHolder())
            ->setClient(new Client());

        $session = (new AdobeConnectSCO($configHolder))
            ->setId(12)
            ->setRef('REFLS123965')
            ->setCategory(CategorySessionType::CATEGORY_SESSION_FORMATION)
            ->setDateStart(new \DateTime())
            ->setDateEnd(new \DateTime());

        $router = $this->createMock(RouterInterface::class);
        $router->expects($this->any())->method('generate')->withAnyParameters()->willReturn('http://url-to-session/id/89');
        $translator = $this->createMock(TranslatorInterface::class);
        $translator->expects($this->any())->method('trans')->withAnyParameters()->willReturn('Lorem ipsum');

        $sut = new WebCalService(
            $router,
            $translator
        );

        $this->assertInstanceOf(
            Component::class,
            $sut->generateWebCalFile([$session, $session, $session])
        );
    }
}
