<?php

namespace App\Tests\Unit\Service\AsyncWorker\Tasks;

use App\Entity\AsyncTask;
use App\Exception\AsyncWorker\AsyncTaskException;
use App\Repository\ClientRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\ActivityLogService;
use App\Service\AsyncWorker\Tasks\ActionsFromImportUserTask;
use App\Service\MiddlewareService;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ActionsFromImportUserTaskTest extends TestCase
{
    private MockObject|EntityManagerInterface $entityManager;
    private MockObject|UserPasswordHasherInterface $passwordHasher;
    private MockObject|UserService $userService;
    private MockObject|UserRepository $userRepository;
    private MockObject|ClientRepository $clientRepository;
    private MockObject|ProviderSessionRepository $providerSessionRepository;
    private MockObject|TranslatorInterface $translator;
    private MockObject|ActivityLogService $activityLogService;
    private MockObject|MiddlewareService $middlewareService;
    private MockObject|ValidatorInterface $validator;
    private MockObject|participantSessionSubscriber $participantSessionSubscriber;
    private MockObject|ActivityLogger $activityLogger;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->passwordHasher = $this->createMock(UserPasswordHasherInterface::class);
        $this->userService = $this->createMock(UserService::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->clientRepository = $this->createMock(ClientRepository::class);
        $this->providerSessionRepository = $this->createMock(ProviderSessionRepository::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->activityLogService = $this->createMock(ActivityLogService::class);
        $this->middlewareService = $this->createMock(MiddlewareService::class);
        $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->participantSessionSubscriber = $this->createMock(ParticipantSessionSubscriber::class);
        $this->activityLogger = $this->createMock(ActivityLogger::class);
    }

    public function getAsyncTasks(): ActionsFromImportUserTask
    {
        return new ActionsFromImportUserTask(
            $this->entityManager,
            $this->activityLogService,
            $this->passwordHasher,
            $this->userService,
            $this->userRepository,
            $this->clientRepository,
            $this->translator,
            $this->providerSessionRepository,
            $this->middlewareService,
            $this->validator,
            $this->participantSessionSubscriber,
            $this->activityLogger
        );
    }

    public function testMissingRowsThrowException(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager->expects($this->never())
            ->method('flush');

        $this->translator->expects($this->once())
            ->method('trans')
            ->willReturn('The data does not contain the "row" key');

        $task = $this->getAsyncTasks();

        $asyncTask = new AsyncTask();
        $this->expectException(AsyncTaskException::class);
        $this->expectExceptionMessage('The data does not contain the "row" key');
        $task->execute($asyncTask);
    }
}
