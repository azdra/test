<?php

namespace App\Tests\Unit\Service\AsyncWorker\Tasks;

use App\Entity\AsyncTask;
use App\Entity\Client\Client;
use App\Entity\Client\ClientEmailScheduling;
use App\Entity\User;
use App\Repository\AsyncTaskRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\ActivityLogService;
use App\Service\AsyncWorker\Tasks\ExportUserRGPDDataTask;
use App\Service\Exporter\ExporterService;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExportUserRGPDDataTaskTest extends TestCase
{
    private MockObject|EntityManagerInterface $entityManager;
    private MockObject|ActivityLogService $activityLogService;
    private MockObject|ActivityLogger $activityLogger;
    private MockObject|ExporterService $exporterService;
    private MockObject|UserRepository $userRepository;
    private MockObject|LoggerInterface $logger;
    private MockObject|MailerService $mailerService;
    private MockObject|TranslatorInterface $translator;
    protected string $exportsSavefilesDirector = 'data/exports_savefiles';

    public function setUp(): void
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->entityManager->expects($this->any())->method('getRepository')
            ->willReturnMap([
                [AsyncTask::class, $this->createMock(AsyncTaskRepository::class)],
            ]);

        $this->activityLogService = $this->createMock(ActivityLogService::class);
        $this->activityLogger = $this->createMock(ActivityLogger::class);
        $this->exporterService = $this->createMock(ExporterService::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->userRepository->expects($this->atMost(2))
            ->method('find')
            ->willReturnOnConsecutiveCalls(
                (new User())->setId(42)->setFirstName('Test')->setLastName('Paul')->setEmail('test@paul.fr'),
                (new User())->setId(1)->setFirstName('Test')->setLastName('Marc')->setEmail('test@marc.fr')
            );

        $this->mailerService = $this->createMock(MailerService::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
    }

    public function getService(): ExportUserRGPDDataTask
    {
        return new ExportUserRGPDDataTask(
            $this->entityManager,
            $this->activityLogService,
            $this->activityLogger,
            $this->logger,
            $this->exporterService,
            $this->userRepository,
            $this->mailerService,
            $this->translator,
            $this->exportsSavefilesDirector
        );
    }

    private function createAsyncTask(array $datas = null): AsyncTask
    {
        $client = (new Client())
            ->setName('Fake client')
            ->setEmailScheduling(new ClientEmailScheduling());

        if ($datas === null) {
            $datas = [
                'user' => '42',
                'userOrigin' => '1',
            ];
        }

        return (new AsyncTask())
            ->setClassName(ExportUserRGPDDataTask::class)
            ->setData($datas)
            ->setStatus('ready')
            ->setUserOrigin((new User())->setId(1)->setEmail('test@userorigin.fr'))
            ->setClientOrigin($client)
            ->setProcessingStartTime(new \DateTime());
    }

    public function testExecute(): void
    {
        $exportUserRGPDDataTask = $this->getService();
        $asyncTask = $this->createAsyncTask();

        $this->exporterService->expects($this->exactly(1))
            ->method('generateArchive')
            ->willReturn(
                (new Response())
                    ->setStatusCode(Response::HTTP_OK)
                    ->setContent(json_encode(['message' => 'GS1Lo1-GUfGu-j5uKB-dbug7b']))
            );

        $this->entityManager->expects($this->once())
            ->method('flush');

        $exportUserRGPDDataTask->execute($asyncTask);

        $asyncResult = $asyncTask->getResult();

        $this->assertTrue($asyncResult['success']);
    }

    public function cannotExecuteDataProvider(): \Generator
    {
        yield 'No User on data' => [['userOrigin' => 42], 'Empty value for user or user origin, export cannot be done'];

        yield 'No User Origin on data' => [['user' => 1], 'Empty value for user or user origin, export cannot be done'];

        yield 'Empty data for task' => [[], 'Empty value for user or user origin, export cannot be done'];
    }

    /**
     * @dataProvider cannotExecuteDataProvider
     */
    public function testCannotExecute(array $datas, string $exceptionMessage): void
    {
        $exportUserRGPDDataTask = $this->getService();
        $asyncTask = $this->createAsyncTask($datas);

        $this->entityManager->expects($this->never())
            ->method('flush');

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage($exceptionMessage);

        $exportUserRGPDDataTask->execute($asyncTask);
    }

    public function testFailedExecute()
    {
        $exportUserRGPDDataTask = $this->getService();
        $asyncTask = $this->createAsyncTask(
            [
                'user' => 1,
                'userOrigin' => 42,
            ]
        );
        $asyncTask->setId(25);
        $this->exporterService->expects($this->exactly(1))
            ->method('generateArchive')
            ->willReturn((new Response())->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR));

        $exportUserRGPDDataTask->execute($asyncTask);

        $asyncResult = $asyncTask->getResult();

        $this->assertFalse($asyncResult['success']);
    }

    public function testGenerateExtractThrowExceptionWhileTaskExecute(): void
    {
        $exportUserRGPDDataTask = $this->getService();
        $asyncTask = $this->createAsyncTask(
            [
                'user' => 1,
                'userOrigin' => 42,
            ]
        );
        $asyncTask->setId(25);
        $this->exporterService->expects($this->exactly(1))
            ->method('generateArchive')
            ->willThrowException(new \Exception('An error occured'));

        $this->translator->expects($this->atMost(2))
            ->method('trans')
            ->willReturn('An error occured');

        $exportUserRGPDDataTask->execute($asyncTask);

        $asyncResult = $asyncTask->getResult();

        $this->assertFalse($asyncResult['success']);

        $this->assertSame('An error occured', $asyncResult['message']);
    }

    public function testGetOriginTask(): void
    {
        $exportUserRGPDDataTask = $this->getService();

        $this->assertSame('ExportUserRGPDDataTask', $exportUserRGPDDataTask->getOriginTask());
    }
}
