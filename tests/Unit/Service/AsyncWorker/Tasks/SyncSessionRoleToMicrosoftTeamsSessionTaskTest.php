<?php

namespace App\Tests\Unit\Service\AsyncWorker\Tasks;

use App\Entity\AsyncTask;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Exception\AsyncWorker\AsyncTaskException;
use App\Exception\ProviderGenericException;
use App\Model\ProviderResponse;
use App\Service\AsyncWorker\Tasks\SyncSessionRoleToMicrosoftTeamsSessionTask;
use App\Service\Connector\Microsoft\MicrosoftTeamsConnector;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class SyncSessionRoleToMicrosoftTeamsSessionTaskTest extends TestCase
{
    private MockObject|EntityManagerInterface $entityManager;
    private MockObject|EntityRepository $repository;
    private MockObject|MicrosoftTeamsConnector $microsoftTeamsConnector;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->repository = $this->createMock(EntityRepository::class);
        $this->entityManager->expects($this->any())->method('getRepository')->willReturn($this->repository);
        $this->microsoftTeamsConnector = $this->createMock(MicrosoftTeamsConnector::class);
    }

    public function getAsyncTask(): SyncSessionRoleToMicrosoftTeamsSessionTask
    {
        return new SyncSessionRoleToMicrosoftTeamsSessionTask(
            $this->entityManager,
            $this->microsoftTeamsConnector
        );
    }

    public function testMissingSessionIdentifierThrowException(): void
    {
        $this->entityManager->expects($this->never())
            ->method('flush');

        $task = $this->getAsyncTask();

        $this->expectException(AsyncTaskException::class);
        $this->expectExceptionMessage('Data does not contains "session_id" key.');

        $task->execute(new AsyncTask());
    }

    public function testSessionIdentifierNotFoundInDatabase(): void
    {
        $asyncTask = (new AsyncTask())
            ->setData([
                'session_id' => 123,
            ]);

        $this->repository->expects($this->once())
            ->method('find')
            ->with(123)
            ->willReturn(null);

        $this->entityManager->expects($this->never())
            ->method('flush');

        $task = $this->getAsyncTask();

        $this->expectException(AsyncTaskException::class);
        $this->expectExceptionMessage('Session with id #123 could not be find.');

        $task->execute($asyncTask);
    }

    public function testConnectorResponseFails(): void
    {
        $asyncTask = (new AsyncTask())
            ->setData([
                'session_id' => 123,
            ]);

        $session = (new MicrosoftTeamsSession((new MicrosoftTeamsConfigurationHolder())->setClient(new Client())));

        $this->repository->expects($this->once())
            ->method('find')
            ->with(123)
            ->willReturn($session);

        $this->entityManager->expects($this->never())
            ->method('flush');

        $this->microsoftTeamsConnector->expects($this->once())
            ->method('call')
            ->with($session->getAbstractProviderConfigurationHolder(), MicrosoftTeamsConnector::ACTION_SESSION_UPDATE, $session)
            ->willReturn(
                (new ProviderResponse())
                    ->setSuccess(false)
                    ->setThrown(new ProviderGenericException(MicrosoftTeamsConnector::class, 'Generic error.'))
            );

        $task = $this->getAsyncTask();

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->atLeastOnce())
            ->method('error');
        $task->setLogger($logger);

        $this->expectException(AsyncTaskException::class);
        $task->execute($asyncTask);
    }

    public function testConnectorResponseSucceeds(): void
    {
        $asyncTask = (new AsyncTask())
            ->setData([
                'session_id' => 123,
            ]);

        $role = new ProviderParticipantSessionRole();

        $session = (new MicrosoftTeamsSession((new MicrosoftTeamsConfigurationHolder())->setClient(new Client())))
            ->addParticipantRole($role);

        $this->repository->expects($this->once())
            ->method('find')
            ->with(123)
            ->willReturn($session);

        $this->entityManager->expects($this->once())
            ->method('flush');

        $this->microsoftTeamsConnector->expects($this->once())
            ->method('call')
            ->with($session->getAbstractProviderConfigurationHolder(), MicrosoftTeamsConnector::ACTION_SESSION_UPDATE, $session)
            ->willReturn(
                (new ProviderResponse())
                    ->setSuccess(true)
            );

        $task = $this->getAsyncTask();

        $task->execute($asyncTask);
        $this->assertTrue($role->isSyncedWithProvider());
    }
}
