<?php

namespace App\Tests\Unit\Service\AsyncWorker\Tasks;

use App\CustomerService\Service\HelpNeedService;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectTelephonyProfile;
use App\Entity\AsyncTask;
use App\Entity\CategorySessionType;
use App\Entity\Client\Client;
use App\Entity\Client\ClientEmailScheduling;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\Convocation;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Exception\AsyncWorker\AsyncTaskException;
use App\Exception\MiddlewareFailureException;
use App\Model\MiddlewareResponse;
use App\Model\ProviderResponse;
use App\Repository\AbstractProviderConfigurationHolderRepository;
use App\Repository\AsyncTaskRepository;
use App\Repository\ConvocationRepository;
use App\Repository\EvaluationRepository;
use App\Repository\ProviderConfigurationHolderRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\ActivityLogger;
use App\Service\ActivityLogService;
use App\Service\AdobeConnectService;
use App\Service\AdobeConnectWebinarService;
use App\Service\AsyncWorker\Tasks\ActionsFromImportSessionTask;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use App\Service\MailerService;
use App\Service\MicrosoftTeamsEventService;
use App\Service\MiddlewareService;
use App\Service\Provider\Adobe\AdobeConnectScoArrayHandler;
use App\Service\Provider\Adobe\AdobeConnectWebinarScoArrayHandler;
use App\Service\Provider\Adobe\AdobeSessionImporter;
use App\Service\Provider\Adobe\AdobeWebinarSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamAdobeSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamAdobeWebinarSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamMicrosoftTeamsEventSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamMicrosoftTeamsSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamWebexRestSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamWebexSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamWhiteSessionImporter;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\Provider\Microsoft\MicrosoftTeamsArrayHandler;
use App\Service\Provider\Microsoft\MicrosoftTeamsEventArrayHandler;
use App\Service\Provider\Microsoft\MicrosoftTeamsEventLicensingService;
use App\Service\Provider\Microsoft\MicrosoftTeamsEventSessionImporter;
use App\Service\Provider\Microsoft\MicrosoftTeamsLicensingService;
use App\Service\Provider\Microsoft\MicrosoftTeamsSessionImporter;
use App\Service\Provider\Model\LicenceChecker;
use App\Service\Provider\Webex\WebexLicensingService;
use App\Service\Provider\Webex\WebexMeetingArrayHandler;
use App\Service\Provider\Webex\WebexRestLicensingService;
use App\Service\Provider\Webex\WebexRestMeetingArrayHandler;
use App\Service\Provider\Webex\WebexRestSessionImporter;
use App\Service\Provider\Webex\WebexSessionImporter;
use App\Service\Provider\White\WhiteArrayHandler;
use App\Service\Provider\White\WhiteSessionImporter;
use App\Service\ProviderSessionService;
use App\Service\RessourceUploader\RessourceUploader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ActionsFromImportSessionTaskTest extends TestCase
{
    public const CREATE = 'createSession';
    public const UPDATE = 'updateSession';
    public const DELETE = 'deleteSession';
    public const CANCEL = 'cancelSession';

    private MockObject|EntityManagerInterface $entityManager;
    private MockObject|ProviderSessionRepository $providerSessionRepository;
    private MockObject|ConvocationRepository $convocationRepository;
    private MockObject|MiddlewareService $middlewareService;
    private MockObject|TranslatorInterface $translator;
    private MockObject|EventDispatcherInterface $eventDispatcher;
    private MockObject|ActivityLogService $activityLogService;
    private MockObject|ActivityLogger $activityLogger;
    private MockObject|ProviderConfigurationHolderRepository $providerConfigurationHolderRepository;
    private MockObject|AdobeConnectService $adobeConnectService;
    private MockObject|AdobeConnectWebinarService $adobeConnectWebinarService;
    private AdobeSessionImporter $adobeSessionImporter;
    private AdobeWebinarSessionImporter $adobeWebinarSessionImporter;
    private MockObject|WebexSessionImporter $webexSessionImporter;
    private MockObject|WebexRestSessionImporter $webexRestSessionImporter;
    private AdobeConnectScoArrayHandler $adobeArrayHandler;
    private AdobeConnectWebinarScoArrayHandler $adobeConnectWebinarScoArrayHandler;
    private WebexMeetingArrayHandler $webexArrayHandler;
    private WebexRestMeetingArrayHandler $webexRestMeetingArrayHandler;
    private MicrosoftTeamsArrayHandler $microsoftTeamsArrayHandler;
    private MicrosoftTeamsEventArrayHandler $microsoftTeamsEventArrayHandler;
    private WhiteArrayHandler $whiteArrayHandler;
    private MockObject|ProviderSessionService $providerSessionService;
    private MockObject|RouterInterface $router;
    private MockObject|LoggerInterface $logger;
    private MockObject|EvaluationRepository $evaluationRepository;
    private MockObject|ParticipantSessionSubscriber $participantSessionSubscriber;
    private MockObject|UserRepository $userRepository;
    private MockObject|MailerService $mailerService;
    private WebexLicensingService $webexLicensingService;
    private WebexRestLicensingService $webexRestLicensingService;
    private MockObject|RoleHierarchyInterface $roleHierarchy;
    private MicrosoftTeamsLicensingService $microsoftTeamsLicensingService;
    private MockObject|MicrosoftTeamsSessionImporter $microsoftTeamsSessionImporter;
    private MockObject|MicrosoftTeamsEventSessionImporter $microsoftTeamsEventSessionImporter;
    private MockObject|WhiteSessionImporter $whiteSessionImporter;
    private MockObject|HelpNeedService $helpNeedService;
    private MockObject|SubjectRepository $subjectRepository;
    private MockObject|AbstractProviderConfigurationHolderRepository $abstractProviderConfigurationHolderRepository;
    private MockObject|LicenceChecker $licenceChecker;
    private MockObject|IfcamAdobeSessionImporter $ifcamAdobeSessionImporter;
    private MockObject|IfcamAdobeWebinarSessionImporter $ifcamAdobeWebinarSessionImporter;
    private MockObject|IfcamWebexSessionImporter $ifcamWebexSessionImporter;
    private MockObject|IfcamWebexRestSessionImporter $ifcamWebexRestSessionImporter;
    private MockObject|IfcamMicrosoftTeamsSessionImporter $ifcamMicrosoftTeamsSessionImporter;
    private MockObject|IfcamMicrosoftTeamsEventSessionImporter $ifcamMicrosoftTeamsEventSessionImporter;
    private MockObject|IfcamWhiteSessionImporter $ifcamWhiteSessionImporter;
    private MockObject|RessourceUploader $ressourceUploader;
    private MockObject|ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository;
    private MockObject|MicrosoftTeamsEventService $microsoftTeamsEventService;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->entityManager->expects($this->any())->method('getRepository')
            ->willReturnMap([
                [AsyncTask::class, $this->createMock(AsyncTaskRepository::class)],
            ]);

        $this->providerSessionRepository = $this->createMock(ProviderSessionRepository::class);
        $this->convocationRepository = $this->createMock(ConvocationRepository::class);
        $this->middlewareService = $this->createMock(MiddlewareService::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $this->activityLogService = $this->createMock(ActivityLogService::class);
        $this->activityLogger = $this->createMock(ActivityLogger::class);
        $this->providerConfigurationHolderRepository = $this->createMock(ProviderConfigurationHolderRepository::class);
        $this->evaluationRepository = $this->createMock(EvaluationRepository::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->mailerService = $this->createMock(MailerService::class);
        $this->webexLicensingService = $this->createMock(WebexLicensingService::class);
        $this->webexRestLicensingService = $this->createMock(WebexRestLicensingService::class);
        $this->roleHierarchy = $this->createMock(RoleHierarchyInterface::class);
        $this->microsoftTeamsLicensingService = $this->createMock(MicrosoftTeamsLicensingService::class);
        $this->microsoftTeamsEventLicensingService = $this->createMock(MicrosoftTeamsEventLicensingService::class);
        $this->subjectRepository = $this->createMock(SubjectRepository::class);
        $this->abstractProviderConfigurationHolderRepository = $this->createMock(AbstractProviderConfigurationHolderRepository::class);
        $this->licenceChecker = $this->createMock(LicenceChecker::class);
        $this->ressourceUploader = $this->createMock(RessourceUploader::class);
        $this->providerParticipantSessionRoleRepository = $this->createMock(ProviderParticipantSessionRoleRepository::class);
        $this->participantSessionSubscriber = $this->createMock(ParticipantSessionSubscriber::class);
        $this->microsoftTeamsEventService = $this->createMock(MicrosoftTeamsEventService::class);

        $this->adobeConnectService = $this->createMock(AdobeConnectService::class);
        /*$this->adobeConnectService->expects($this->any())->method('getSharedMeetingsTemplate')
            ->willReturn(new ProviderResponse(true, null, [
                123 => ['uniq_identifier' => 123, 'name' => 'room exists'],
                456 => ['uniq_identifier' => 456, 'name' => 'client room'],
            ]));*/
        $this->adobeConnectService->expects($this->any())->method('findAllModelsRoom')
            ->willReturn(new ProviderResponse(true, null, [
                123 => ['uniq_identifier' => 123, 'name' => 'room exists'],
                456 => ['uniq_identifier' => 456, 'name' => 'client room'],
            ]));
        $this->adobeArrayHandler = new AdobeConnectScoArrayHandler($this->adobeConnectService);

        $this->adobeConnectWebinarService = $this->createMock(AdobeConnectWebinarService::class);
        $this->adobeConnectWebinarService->expects($this->any())->method('getSharedMeetingsTemplate')
            ->willReturn(new ProviderResponse(true, null, [
                123 => ['uniq_identifier' => 123, 'name' => 'room exists'],
                456 => ['uniq_identifier' => 456, 'name' => 'client room'],
            ]));
        $this->adobeConnectWebinarScoArrayHandler = new AdobeConnectWebinarScoArrayHandler($this->adobeConnectWebinarService);

        $this->webexArrayHandler = new WebexMeetingArrayHandler($this->webexLicensingService, $this->userRepository);
        $this->webexRestMeetingArrayHandler = new WebexRestMeetingArrayHandler($this->webexRestLicensingService, $this->userRepository);
        $this->microsoftTeamsArrayHandler = new MicrosoftTeamsArrayHandler($this->microsoftTeamsLicensingService, $this->userRepository);
        $this->microsoftTeamsEventArrayHandler = new MicrosoftTeamsEventArrayHandler($this->microsoftTeamsEventLicensingService, $this->userRepository, $this->microsoftTeamsEventService, $this->providerParticipantSessionRoleRepository, $this->participantSessionSubscriber);
        $this->whiteArrayHandler = new WhiteArrayHandler($this->userRepository);
        $this->router = $this->createMock(RouterInterface::class);
        $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->helpNeedService = $this->createMock(HelpNeedService::class);
        $this->providerSessionService = $this->getMockBuilder(ProviderSessionService::class)
            ->onlyMethods(['getConvocationAndRemindersDate', 'cancelSession'])
            ->setConstructorArgs([
                $this->middlewareService, $this->entityManager, $this->adobeArrayHandler, $this->adobeConnectWebinarScoArrayHandler, $this->webexArrayHandler, $this->webexRestMeetingArrayHandler, $this->microsoftTeamsArrayHandler, $this->microsoftTeamsEventArrayHandler, $this->whiteArrayHandler, $this->activityLogger, $this->router, $this->eventDispatcher, $this->userRepository, $this->mailerService, $this->translator, $this->logger, $this->helpNeedService, $this->abstractProviderConfigurationHolderRepository, $this->convocationRepository, $this->providerSessionRepository, $this->ressourceUploader, $this->providerParticipantSessionRoleRepository, 'path_convocation_attachment',
            ])->getMock();
        $this->participantSessionSubscriber = $this->createMock(ParticipantSessionSubscriber::class);

        $this->adobeSessionImporter = new AdobeSessionImporter(
            $this->providerSessionService,
            $this->providerSessionRepository,
            $this->convocationRepository,
            $this->evaluationRepository,
            $this->subjectRepository,
            $this->translator,
            $this->adobeConnectService,
            $this->activityLogger
        );

        $this->adobeWebinarSessionImporter = new AdobeWebinarSessionImporter(
            $this->providerSessionService,
            $this->providerSessionRepository,
            $this->convocationRepository,
            $this->evaluationRepository,
            $this->subjectRepository,
            $this->translator,
            $this->adobeConnectWebinarService,
            $this->activityLogger
        );

        $this->webexSessionImporter = $this->createMock(WebexSessionImporter::class);
        $this->webexRestSessionImporter = $this->createMock(WebexRestSessionImporter::class);
        $this->microsoftTeamsSessionImporter = $this->createMock(MicrosoftTeamsSessionImporter::class);
        $this->microsoftTeamsEventSessionImporter = $this->createMock(MicrosoftTeamsEventSessionImporter::class);
        $this->whiteSessionImporter = $this->createMock(WhiteSessionImporter::class);
        $this->logger = $this->createMock(LoggerInterface::class);

        $this->ifcamMicrosoftTeamsSessionImporter = $this->createMock(IfcamMicrosoftTeamsSessionImporter::class);
        $this->ifcamMicrosoftTeamsEventSessionImporter = $this->createMock(IfcamMicrosoftTeamsEventSessionImporter::class);
        $this->ifcamAdobeSessionImporter = $this->createMock(IfcamAdobeSessionImporter::class);
        $this->ifcamAdobeWebinarSessionImporter = $this->createMock(IfcamAdobeWebinarSessionImporter::class);
        $this->ifcamWebexSessionImporter = $this->createMock(IfcamWebexSessionImporter::class);
        $this->ifcamWebexRestSessionImporter = $this->createMock(IfcamWebexRestSessionImporter::class);
        $this->ifcamWhiteSessionImporter = $this->createMock(IfcamWhiteSessionImporter::class);

        $this->participantSessionSubscriber = $this->createMock(ParticipantSessionSubscriber::class);
    }

    public function getService(): ActionsFromImportSessionTask
    {
        $service = $this->getMockBuilder(ActionsFromImportSessionTask::class)
            ->setConstructorArgs([
                $this->entityManager,
                $this->activityLogService,
                $this->providerSessionRepository,
                $this->convocationRepository,
                $this->middlewareService,
                $this->translator,
                $this->eventDispatcher,
                $this->providerConfigurationHolderRepository,
                $this->adobeSessionImporter,
                $this->adobeWebinarSessionImporter,
                $this->webexSessionImporter,
                $this->webexRestSessionImporter,
                $this->microsoftTeamsSessionImporter,
                $this->microsoftTeamsEventSessionImporter,
                $this->whiteSessionImporter,
                $this->adobeArrayHandler,
                $this->adobeConnectWebinarScoArrayHandler,
                $this->webexArrayHandler,
                $this->webexRestMeetingArrayHandler,
                $this->microsoftTeamsArrayHandler,
                $this->microsoftTeamsEventArrayHandler,
                $this->whiteArrayHandler,
                $this->providerSessionService,
                $this->activityLogger,
                $this->participantSessionSubscriber,
                $this->roleHierarchy,
                $this->abstractProviderConfigurationHolderRepository,
                $this->licenceChecker,
                $this->adobeConnectService,
                $this->ifcamAdobeSessionImporter,
                $this->ifcamAdobeWebinarSessionImporter,
                $this->ifcamWebexSessionImporter,
                $this->ifcamWebexRestSessionImporter,
                $this->ifcamMicrosoftTeamsSessionImporter,
                $this->ifcamMicrosoftTeamsEventSessionImporter,
                $this->ifcamWhiteSessionImporter,
            ])
            ->onlyMethods(['extractSessionDataForLog'])
            ->getMock();

        $service->setLogger($this->logger);

        return $service;
    }

    private function createAsyncTask(array $data): AsyncTask
    {
        $client = (new Client())
            ->setId(2)
            ->setName('Fake client')
            ->setEmailScheduling(new ClientEmailScheduling());

        $configurationHolder = (new AdobeConnectConfigurationHolder())
            ->setDefaultRoom(456)
            ->setClient($client);

        $client->addConfigurationHolder($configurationHolder);

        $this->providerConfigurationHolderRepository->method('getAdobeConfigurationHolderForClient')
            ->with($client)->willReturn($configurationHolder);

        $asyncTask = new AsyncTask();
        $asyncTask->setId(rand(10000, 99999));
        $asyncTask->setClassName('Ma classe de session');
        $asyncTask->setData($data);
        $asyncTask->setFlagOrigin('ImportSessions_123456');
        $asyncTask->setClientOrigin($client);
        $asyncTask->setProcessingStartTime(new \DateTime());
        $asyncTask->setFileOrigin('fileorigin_123456');
        $asyncTask->setUserOrigin(new User());

        return $asyncTask;
    }

    public function nonExecutableTestCase(): \Generator
    {
        yield 'Missing rows' => [(new AsyncTask())->setClientOrigin(new Client())->setUserOrigin(new User()), 'The data does not contain the "row" key'];

        yield 'Missing client' => [(new AsyncTask())->setData(['rows' => ['something']])->setUserOrigin(new User()), 'Account is not specified'];

        yield 'Missing user' => [(new AsyncTask())->setData(['rows' => ['something']])->setClientOrigin(new Client()), 'The user was not found'];
    }

    /**
     * @dataProvider nonExecutableTestCase
     */
    public function testMissingRowsThrowException(AsyncTask $asyncTask, string $expectedMessageException): void
    {
        $actionsFromImportSessionTask = $this->getService();

        $this->translator->expects($this->once())->method('trans')
            ->with($expectedMessageException)->willReturn($expectedMessageException);

        $this->expectException(AsyncTaskException::class);
        $this->expectExceptionMessage($expectedMessageException);

        $this->providerConfigurationHolderRepository->expects($this->never())->method('getAdobeConfigurationHolderForClient');

        $actionsFromImportSessionTask->execute($asyncTask);
    }

    public function failExecuteDataProvider(): \Generator
    {
        yield 'Empty title' => [null, [AdobeSessionImporter::NAME_INDEX => null], new \Exception('The session\'s name is empty!')];

        yield 'Date start empty' => [null, [AdobeSessionImporter::DATE_START_INDEX => null], new \Exception('The session date "%title%" is empty!')];

        yield 'Duration empty' => [null, [AdobeSessionImporter::DURATION_INDEX => null], new \Exception('The duration for the "%title%" session is not correct!')];

        yield 'Convocation empty' => [null, [AdobeSessionImporter::CONVOCATION_NAME_INDEX => null], new \Exception('The "%title%" session convocation is empty!')];

        yield 'Convocation not found' => [null, [], new \Exception('The convocation does not exist!'), false];

        yield 'Category not found (WRONG TRANSLATION MESSAGE)' => [null, [AdobeSessionImporter::SESSION_TYPE_INDEX => 66], new \Exception('The "%title%" session category is not correct!'), true];

        yield 'Category not available for account (WRONG TRANSLATION MESSAGE)' => [null, [AdobeSessionImporter::SESSION_TYPE_INDEX => CategorySessionType::CATEGORY_SESSION_SERVICES->value], new \Exception('The "%title%" session category is not available in your account!'), true];

        yield 'Wrong text for action' => [null, [AdobeSessionImporter::ACTION_INDEX => 'NOT_CORRECT'], new \Exception('The requested action "%action%" for the "%title%" session is not correct!'), true];

        yield 'Trying to remove without reference' => [null, [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_DELETE, AdobeSessionImporter::REFERENCE_INDEX => null], new \Exception('You cannot delete a session "%title%" that does not exist!'), true];

        yield 'Trying to remove session that does not exist' => [null, [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_DELETE, AdobeSessionImporter::REFERENCE_INDEX => 'Ref #1'], new \Exception('You cannot delete a session "%title%" that does not exist!'), true, false];

        yield 'Trying to cancel without reference' => [null, [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_CANCEL, AdobeSessionImporter::REFERENCE_INDEX => null], new \Exception('You cannot cancel a session "%title%" that does not exist!'), true];

        yield 'Trying to cancel session that does not exist' => [null, [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_CANCEL, AdobeSessionImporter::REFERENCE_INDEX => 'Ref #1'], new \Exception('You cannot cancel a session "%title%" that does not exist!'), true, false];

        yield 'URL contains does not contains only alphanumerical char or trait' => [null, [AdobeSessionImporter::URL_INDEX => 'pas valide'], new \Exception('The url given for the session "%session%" is not valid!'), true];

        yield 'Update action and giving url is forbidden' => [null, [AdobeSessionImporter::URL_INDEX => 'est-valide', AdobeSessionImporter::REFERENCE_INDEX => 'Ref #1'], new \Exception('You do not need to enter a url when you update the "%title%" session!'), true, true];

//        yield 'CREATE : MiddlewareService will return failed response' => [self::CREATE, [], new MiddlewareFailureException('An error occurred while importing'), true];

//        yield 'UPDATE : MiddlewareService will return failed response' => [self::UPDATE, [AdobeSessionImporter::REFERENCE_INDEX => 'Ref #1'], new MiddlewareFailureException('An error occurred while importing'), true, true];

//        yield 'DELETE : MiddlewareService will return failed response' => [self::DELETE, [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_DELETE, AdobeSessionImporter::REFERENCE_INDEX => 'Ref #1'], new MiddlewareFailureException('An error occurred while importing'), true, true];
    }

    private function generateDataForCase(array $caseData): array
    {
        $data = [
            AdobeSessionImporter::NAME_INDEX => 'name',
            AdobeSessionImporter::DATE_START_INDEX => '21/10/2021 13:30',
            AdobeSessionImporter::DURATION_INDEX => '01:00',
            AdobeSessionImporter::CONVOCATION_NAME_INDEX => 'Convocation',
            AdobeSessionImporter::SESSION_TYPE_INDEX => strval(CategorySessionType::CATEGORY_SESSION_FORMATION->value),
            AdobeSessionImporter::REFERENCE_INDEX => '',
            AdobeSessionImporter::ACTION_INDEX => null,
            'numlinefile' => random_int(0, 1000),
        ];

        foreach ($caseData as $key => $datum) {
            $data[$key] = $datum;
        }

        return ['rows' => [$data]];
    }

    /**
     * @dataProvider failExecuteDataProvider
     */
    public function testFailExecute(?string $method, array $caseData, ?\Exception $exception, bool $convocationsExist = true, ?bool $sessionExist = null): void
    {
        $actionsFromImportSessionTask = $this->getService();
        $data = $this->generateDataForCase($caseData);
        $asyncTask = $this->createAsyncTask($data);

        $this->entityManager->expects($this->once())
            ->method('flush');

        $exceptionMessage = ($exception instanceof MiddlewareFailureException)
            ? $exception->getMessage().' for the session %name%'
            : $exception->getMessage();

        $this->translator->expects($this->atMost(2))
            ->method('trans')
            ->with($exceptionMessage)
            ->willReturn($exceptionMessage);

        $isCreateOrNot = !array_key_exists(AdobeSessionImporter::ACTION_INDEX, $caseData) &&
            !array_key_exists(AdobeSessionImporter::DATE_START_INDEX, $caseData) &&
            ($sessionExist === null || $sessionExist === false) ? self::CREATE : self::UPDATE;
        $this->handleGetConvocationAndRemindersDateExpectationFromMethod($isCreateOrNot);

        if (!empty($method)) {
            $response = (new MiddlewareResponse())
                ->setSuccess(false)
                ->setData($exception)
                ->setException($exception);

            $this->middlewareService->expects($this->once())
                ->method('call')
                ->with($method)
                ->willReturn($response);
            $this->handleRoleUser(true);
        } else {
            $this->middlewareService->expects($this->never())->method('call');
        }

        if (null === $sessionExist) {
            $this->providerSessionRepository->expects($this->never())->method('findOneBy');
        } else {
            $configurationHolder = new AdobeConnectConfigurationHolder();
            $configurationHolder->setClient(new Client());
            $this->providerSessionRepository->expects($this->once())
                ->method('findOneBy')
                ->willReturn($sessionExist ? new AdobeConnectSCO($configurationHolder) : null);
        }

        $this->convocationRepository->expects($this->once())
            ->method('findConvocation')
            ->withAnyParameters()
            ->willReturn($convocationsExist ? new Convocation() : null);

        $actionsFromImportSessionTask->execute($asyncTask);

        $asyncResult = $asyncTask->getResult();
        $this->assertSame('error', $asyncResult['rows'][0]['type']);
        $this->assertSame($exceptionMessage, $asyncResult['rows'][0]['message']);
    }

    public function executeDataProvider(): \Generator
    {
        yield 'Create' => [[], self::CREATE];

        yield 'Create with reference is allowed if not exist' => [[AdobeSessionImporter::REFERENCE_INDEX => 'Ref #1'], self::CREATE];

        yield 'Update' => [[AdobeSessionImporter::REFERENCE_INDEX => 'Ref #1'], self::UPDATE];

        yield 'Delete' => [[AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_DELETE, AdobeSessionImporter::REFERENCE_INDEX => 'Ref #1'], self::DELETE];

        yield 'Cancel' => [[AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_CANCEL, AdobeSessionImporter::REFERENCE_INDEX => 'Ref #1'], null];
    }

    public function createClient(array $data = []): Client
    {
        return (new Client())
            ->setId(418)
            ->setName($data['clientName'] ?? 'Client name');
    }

    public function createConfigurationHolder(array $data = []): AbstractProviderConfigurationHolder
    {
        return (new AdobeConnectConfigurationHolder())
            ->setClient($this->createClient($data));
    }

    private function getValidAdobeConnectProfileTelephony(): array
    {
        $arrayAudio = [];
        $arrayAudio[] = (new AdobeConnectTelephonyProfile())
            ->setProfileIdentifier(3330016363)
            ->setConnectionType('InterCall')
            ->setName('FR_LIVESESSION_AUDIO_02_FVN')
            ->setPhoneLang('fr');

        $arrayAudio[] = (new AdobeConnectTelephonyProfile())
            ->setProfileIdentifier(1878965615)
            ->setConnectionType('InterCall')
            ->setName('FR_LIVESESSION_AUDIO_INT_01_LJA')
            ->setPhoneLang('fr');

        return $arrayAudio;
    }

    /**
     * @dataProvider executeDataProvider
     */
    public function testExecute(array $dataCase, ?string $method): void
    {
        $actionsFromImportSessionTask = $this->getService();
        $data = $this->generateDataForCase($dataCase);
        $asyncTask = $this->createAsyncTask($data);

        if ($method === self::CREATE) {
            $this->entityManager->expects($this->exactly(2))
                ->method('flush');
        } else {
            $this->entityManager->expects($this->once())
                ->method('flush');
        }

        $this->convocationRepository->expects($this->once())
            ->method('findConvocation')
            ->withAnyParameters()
            ->willReturn(new Convocation());

        $this->handleGetConvocationAndRemindersDateExpectationFromMethod($method ?? self::CANCEL);
        $this->handleRoleUser(true);

        $providerSession = new AdobeConnectSCO($this->createConfigurationHolder());
        $providerSession->setStatus(ProviderSession::STATUS_SCHEDULED);
        $providerSession->setDateStart((new \DateTime())->modify('+ 1 hours'));
        $providerSession->setDuration(60);
        $providerSession->setId(123);
        $providerSession->setName('Fake session');
        $providerSession->setAbstractProviderConfigurationHolder($this->createConfigurationHolder());
        $providerSession->setProviderAudio($this->getValidAdobeConnectProfileTelephony()[0]);
        if (!array_key_exists(AdobeSessionImporter::REFERENCE_INDEX, $dataCase)) {
            $this->providerSessionRepository->expects($this->never())->method('findOneBy');
        } else {
            $providerSession->setRef($dataCase[AdobeSessionImporter::REFERENCE_INDEX]);
            $providerSession->setParticipantRoles(new ArrayCollection());
            $this->providerSessionRepository->expects($this->once())
                ->method('findOneBy')
                ->willReturn($method === self::CREATE ? null : $providerSession);
        }

        if (null === $method) {
            $this->middlewareService->expects($this->never())->method('call');
        } else {
            $response = (new MiddlewareResponse())->setSuccess(true)->setData($providerSession);
            $this->middlewareService->expects($this->once())
                ->method('call')
                ->with($method)
                ->willReturn($response);
        }

        $this->translator->expects($this->once())
            ->method('trans')
            ->willReturn('The method has been tested');

        $actionsFromImportSessionTask->execute($asyncTask);

        $asyncResult = $asyncTask->getResult();
        $this->assertSame('success', $asyncResult['rows'][0]['type']);
        $this->assertSame('The method has been tested', $asyncResult['rows'][0]['message']);
    }

    public function forbiddenTestCase(): \Generator
    {
        yield 'update forbidden if not manager' => [self::UPDATE, false, 'scheduled', 'You do not have the role needed to import data!', [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE, AdobeSessionImporter::REFERENCE_INDEX => 'toto']];
        yield 'create forbidden if not manager' => [self::CREATE, false, 'scheduled', 'You do not have the role needed to import data!', [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE]];
        yield 'delete forbidden if not manager' => [self::DELETE, false, 'scheduled', 'You do not have the role needed to import data!', [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_DELETE, AdobeSessionImporter::REFERENCE_INDEX => 'toto']];
        yield 'cancel forbidden if not manager' => [self::CANCEL, false, 'scheduled', 'You do not have the role needed to import data!', [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_CANCEL, AdobeSessionImporter::REFERENCE_INDEX => 'toto']];
        yield 'delete forbidden if session is finished' => [self::DELETE, true, 'finished', 'You can not delete a session "%title%" which is finished or running!', [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_DELETE, AdobeSessionImporter::REFERENCE_INDEX => 'toto']];
        yield 'delete forbidden if session is running' => [self::DELETE, true, 'running', 'You can not delete a session "%title%" which is finished or running!', [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_DELETE, AdobeSessionImporter::REFERENCE_INDEX => 'toto']];
        yield 'cancel forbidden if session is finished' => [self::CANCEL, true, 'finished', 'You can not cancel a session "%title%" which is finished or running!', [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_CANCEL, AdobeSessionImporter::REFERENCE_INDEX => 'toto']];
        yield 'cancel forbidden if session is running' => [self::CANCEL, true, 'running', 'You can not cancel a session "%title%" which is finished or running!', [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_CANCEL, AdobeSessionImporter::REFERENCE_INDEX => 'toto']];
        yield 'cancel forbidden if session is a draft' => [self::CANCEL, true, 'draft', 'You can not cancel a session "%title%" which is a draft!', [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_CANCEL, AdobeSessionImporter::REFERENCE_INDEX => 'toto']];
        yield 'update forbidden if session is finished' => [self::UPDATE, true, 'finished', 'You can not change the "%title%" session that is completed or in progress!', [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE, AdobeSessionImporter::REFERENCE_INDEX => 'toto']];
        yield 'update forbidden if session is running' => [self::UPDATE, true, 'running', 'You can not change the "%title%" session that is completed or in progress!', [AdobeSessionImporter::ACTION_INDEX => ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE, AdobeSessionImporter::REFERENCE_INDEX => 'toto']];
    }

    /**
     * @dataProvider forbiddenTestCase
     */
    public function testForbiddenAction(string $action, bool $isManager, string $status, string $expectedTrans, array $dataCase)
    {
        $actionsFromImportSessionTask = $this->getService();
        $data = $this->generateDataForCase($dataCase);
        $asyncTask = $this->createAsyncTask($data);

        $this->entityManager->expects($this->once())
            ->method('flush');

        $this->convocationRepository->expects($this->once())
            ->method('findConvocation')
            ->withAnyParameters()
            ->willReturn(new Convocation());

        $this->handleGetConvocationAndRemindersDateExpectationFromMethod($action);

        $providerSession = $this->createPartialMock(AdobeConnectSCO::class, ['getId']);
        if ($status === 'finished') {
            $providerSession->setStatus(ProviderSession::STATUS_SCHEDULED);
            $providerSession->setDateStart((new \DateTime())->modify('- 2 hours'));
            $providerSession->setDuration(60);
        } elseif ($status === 'running') {
            $providerSession->setStatus(ProviderSession::STATUS_SCHEDULED);
            $providerSession->setDateStart((new \DateTime())->modify('- 2 hours'));
            $providerSession->setDuration(240);
        } elseif ($status === 'draft') {
            $providerSession->setStatus(ProviderSession::STATUS_DRAFT);
            $providerSession->setDateStart((new \DateTime())->modify('+ 1 hours'));
            $providerSession->setDuration(60);
        } else { // scheduled
            $providerSession->setStatus(ProviderSession::STATUS_SCHEDULED);
            $providerSession->setDateStart((new \DateTime())->modify('+ 1 hours'));
            $providerSession->setDuration(60);
        }

        if (!array_key_exists(AdobeSessionImporter::REFERENCE_INDEX, $dataCase)) {
            $this->providerSessionRepository->expects($this->never())->method('findOneBy');
        } else {
            $providerSession->setRef($dataCase[AdobeSessionImporter::REFERENCE_INDEX]);
            $providerSession->setParticipantRoles(new ArrayCollection());
            $this->providerSessionRepository->expects($this->once())
                ->method('findOneBy')
                ->willReturn($providerSession);
        }

        $this->middlewareService->expects($this->never())->method('call');

        $this->translator->expects($this->exactly(2))
            ->method('trans')
            ->with($expectedTrans)
            ->willReturn('The method has been tested');

        $this->handleRoleUser($isManager);

        $actionsFromImportSessionTask->execute($asyncTask);

        $asyncResult = $asyncTask->getResult();
        $this->assertSame('error', $asyncResult['rows'][0]['type']);
        $this->assertSame('The method has been tested', $asyncResult['rows'][0]['message']);
    }

    public function handleRoleUser(bool $isAllowed): void
    {
        if ($isAllowed) {
            $this->roleHierarchy->expects($this->once())->method('getReachableRoleNames')->willReturn(['ROLE_MANAGER']);
        } else {
            $this->roleHierarchy->expects($this->once())->method('getReachableRoleNames')->willReturn([]);
        }
    }

    public function handleGetConvocationAndRemindersDateExpectationFromMethod(string $method): void
    {
        if ($method === self::CREATE) {
            $this->providerSessionService->expects($this->once())->method('getConvocationAndRemindersDate')
                ->willReturn([
                    'convocation' => new \DateTimeImmutable('2021/12/05 08:15:00'),
                    'reminders' => [
                        new \DateTimeImmutable('2021/12/10 20:15:00'),
                        new \DateTimeImmutable('2021/12/15 20:45:00'),
                    ],
                ]);
        } else {
            $this->providerSessionService->expects($this->never())->method('getConvocationAndRemindersDate');
        }
    }

    public function testExecuteAssertAllColumnSetProperly(): void
    {
        $providerSession = null;
        $asyncTask = $this->generateCaseForCreation([
            AdobeSessionImporter::NAME_INDEX => 'name',
            AdobeSessionImporter::DATE_START_INDEX => '21/10/2021 13:30',
            AdobeSessionImporter::DURATION_INDEX => '01:15',
            AdobeSessionImporter::CONVOCATION_NAME_INDEX => 'Convocation',
            AdobeSessionImporter::SESSION_TYPE_INDEX => strval(CategorySessionType::CATEGORY_SESSION_FORMATION->value),
            AdobeSessionImporter::LANG_INDEX => 'FR',
            AdobeSessionImporter::REFERENCE_INDEX => 'Ref #1',
            AdobeSessionImporter::URL_INDEX => 'valid-url',
            AdobeSessionImporter::ACCESS_TYPES_INDEX => ActionsFromImportSessionTask::ACCESS_VISITOR,
            AdobeSessionImporter::MODEL_INDEX => 'room exists',
            AdobeSessionImporter::CONVOCATION_SESSION_CONTENT_INDEX => 'session content in convocation',
            AdobeSessionImporter::BUSINESS_INDEX => 'BN001',
            AdobeSessionImporter::TAGS_INDEX => 'tags1;tag2;tag3;',
            AdobeSessionImporter::INFORMATION_INDEX => 'session information',
        ], $providerSession);

        $this->getService()->execute($asyncTask);

        /* @var AdobeConnectSCO $providerSession */
        $this->assertEquals('name', $providerSession->getName());
        $this->assertEquals('21/10/2021 13:30', $providerSession->getDateStart()->format('d/m/Y H:i'));
        $this->assertEquals('21/10/2021 14:45', $providerSession->getDateEnd()->format('d/m/Y H:i'));
        $this->assertEquals((new Convocation())->setName('Convocation'), $providerSession->getConvocation());
        $this->assertEquals(CategorySessionType::CATEGORY_SESSION_FORMATION, $providerSession->getCategory());
        $this->assertEquals('FR', $providerSession->getLanguage());
        $this->assertEquals('Ref #1', $providerSession->getRef());
        $this->assertEquals(123, $providerSession->getRoomModel());
        $this->assertEquals('valid-url', $providerSession->getUrlPath());
        $this->assertEquals(AdobeConnectConnector::GRANT_ACCESS_VISITOR, $providerSession->getAdmittedSession());
        $this->assertEquals('session content in convocation', $providerSession->getPersonalizedContent());
        $this->assertEquals('BN001', $providerSession->getBusinessNumber());
        $this->assertEquals('session information', $providerSession->getInformation());
        $this->assertEquals(['tags1', 'tag2', 'tag3'], $providerSession->getTags());
    }

    public function testExecuteCreateWithoutReferenceShouldAssignNewReference()
    {
        $providerSession = null;
        $asyncTask = $this->generateCaseForCreation([AdobeSessionImporter::MODEL_INDEX => 123], $providerSession);

        $this->getService()->execute($asyncTask);

        /* @var AdobeConnectSCO $providerSession */
        $this->assertNotEmpty($providerSession->getRef());
        $this->assertStringStartsWith('FA', $providerSession->getRef());
    }

    public function templateRoomDataProvider(): \Generator
    {
        yield 'We should use the given room if it exists ' => [123, 'room exists'];

        yield 'We should use default client room id given room does not exist' => [456, 'room does not exist'];

        yield 'We should use nothing if room does not exist and no default room' => [null, 'room does not exist', false];

        yield 'We should use nothing if default room not exist' => [null, 'room does not exist', null];
    }

    /**
     * @dataProvider templateRoomDataProvider
     */
    public function testTemplateRoom(?int $expectedId, string $givenTemplate, ?bool $hasDefaultRoom = true): void
    {
        $providerSession = null;
        $asyncTask = $this->generateCaseForCreation([AdobeSessionImporter::MODEL_INDEX => $givenTemplate], $providerSession);

        if ($hasDefaultRoom === false) {
            $asyncTask->getClientOrigin()->getConfigurationHolders()->first()->setDefaultRoom(null);
        } elseif ($hasDefaultRoom === null) {
            $asyncTask->getClientOrigin()->getConfigurationHolders()->first()->setDefaultRoom(789);
        }

        $this->getService()->execute($asyncTask);

        /* @var AdobeConnectSCO $providerSession */
        $this->assertEquals($expectedId, $providerSession->getRoomModel());
    }

    public function testTemplateRoomNoChangeOnUpdate(): void
    {
        [$asyncTask, $providerSession] = $this->generateCaseForUpdate([AdobeSessionImporter::MODEL_INDEX => 'room exists']);

        $this->getService()->execute($asyncTask);

        $this->assertSame(456, $providerSession->getRoomModel());
    }

    public function testOnCreationApplyReminderAndConvocationsPreferenceIsCalled()
    {
        /** @var ProviderSession $providerSession */
        $providerSession = null;
        $asyncTask = $this->generateCaseForCreation([], $providerSession);

        $this->getService()->execute($asyncTask);

        $this->assertEquals(new \DateTimeImmutable('2021/12/05 08:15:00'), $providerSession->getNotificationDate());
        $this->assertEquals([
            new \DateTimeImmutable('2021/12/10 20:15:00'),
            new \DateTimeImmutable('2021/12/15 20:45:00'),
        ], $providerSession->getDateOfSendingReminders());
    }

    private function generateCaseForCreation(array $caseData, ProviderSession &$providerSession = null): AsyncTask
    {
        if (!array_key_exists(AdobeSessionImporter::MODEL_INDEX, $caseData)) {
            $caseData[AdobeSessionImporter::MODEL_INDEX] = 123;
        }
        $data = $this->generateDataForCase($caseData);
        $asyncTask = $this->createAsyncTask($data);

        $convocation = (new Convocation())->setName('Convocation')->setCategoryTypeAudio(AbstractProviderAudio::AUDIO_CATEGORY_COMPUTER);

        $this->convocationRepository->expects($this->once())
            ->method('findConvocation')
            ->withAnyParameters()
            ->willReturn($convocation);

        if (array_key_exists(AdobeSessionImporter::REFERENCE_INDEX, $caseData)) {
            $this->providerSessionRepository->expects($this->once())->method('findOneBy')->willReturn(null);
        } else {
            $this->providerSessionRepository->expects($this->never())->method('findOneBy');
        }
        $this->handleRoleUser(true);

        $this->middlewareService->expects($this->once())->method('call')
            ->with(self::CREATE, $this->callback(function ($subject) use (&$providerSession) {
                $providerSession = $subject;

                return $subject instanceof AdobeConnectSCO;
            }))
            ->willReturn((new MiddlewareResponse())->setSuccess(true)->setData(new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client()))));

        $this->translator->expects($this->once())
            ->method('trans')
            ->willReturn('The method has been tested');

        $this->handleGetConvocationAndRemindersDateExpectationFromMethod(self::CREATE);

        return $asyncTask;
    }

    private function generateCaseForUpdate(array $caseData): array
    {
        if (!array_key_exists(AdobeSessionImporter::REFERENCE_INDEX, $caseData)) {
            $caseData[AdobeSessionImporter::REFERENCE_INDEX] = 'Ref #1';
        }
        $data = $this->generateDataForCase($caseData);
        $asyncTask = $this->createAsyncTask($data);

        $this->entityManager->expects($this->once())
            ->method('flush');

        $this->convocationRepository->expects($this->once())
            ->method('findConvocation')
            ->withAnyParameters()
            ->willReturn(new Convocation());
        $this->handleRoleUser(true);

        $providerSession = $this->createPartialMock(AdobeConnectSCO::class, ['getId']);
        $providerSession->method('getId')->willReturn(123);
        $providerSession->setName('Fake session')
            ->setRoomModel(456)
            ->setRef($caseData[AdobeSessionImporter::REFERENCE_INDEX]);
        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')
            ->willReturn($providerSession);
        $providerSession->setAbstractProviderConfigurationHolder($this->createConfigurationHolder());
        $providerSession->setClient($providerSession->getAbstractProviderConfigurationHolder()->getClient());
        $response = (new MiddlewareResponse())->setSuccess(true)->setData($providerSession);
        $this->middlewareService->expects($this->once())
            ->method('call')
            ->with(self::UPDATE)
            ->willReturn($response);

        $this->translator->expects($this->once())
            ->method('trans')
            ->willReturn('The method has been tested');

        $this->providerSessionService->expects($this->never())
            ->method('getConvocationAndRemindersDate')
            ->with(new ClientEmailScheduling(), $providerSession);

        return [$asyncTask, $providerSession];
    }
}
