<?php

namespace App\Tests\Unit\Service;

use App\Entity\ActivityLog;
use App\Entity\User;
use App\Repository\ActivityLogRepository;
use App\Repository\ClientRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogService;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use phpmock\phpunit\PHPMock;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ActivityLogServiceTest extends TestCase
{
    use PHPMock;

    private TranslatorInterface $translator;
    private ActivityLogRepository $activityLogRepository;
    private string $importsSavefilesDirectory = 'my-path/import';
    private string $exportsSavefilesDirectory = 'my-path/export';
    private EntityManagerInterface $entityManager;
    private MailerService $mailerService;
    private LoggerInterface $logger;
    private ProviderSessionRepository $providerSessionRepository;
    private ClientRepository $clientRepository;
    private UserRepository $userRepository;
    private SerializerInterface $serializer;

    private const ACTIVITY_LOG_FILENAME = 'my-file-name.me';

    protected function setUp(): void
    {
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->activityLogRepository = $this->createMock(ActivityLogRepository::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->mailerService = $this->createMock(MailerService::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->providerSessionRepository = $this->createMock(ProviderSessionRepository::class);
        $this->clientRepository = $this->createMock(ClientRepository::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->serializer = $this->createMock(SerializerInterface::class);
    }

    private function generateActivityLogService(array $onlyMockedMethods = []): ActivityLogService|MockObject
    {
        return $this->getMockBuilder(ActivityLogService::class)
            ->setConstructorArgs([
                $this->translator,
                $this->activityLogRepository,
                $this->importsSavefilesDirectory,
                $this->exportsSavefilesDirectory,
                $this->entityManager,
                $this->mailerService,
                $this->logger,
                $this->providerSessionRepository,
                $this->clientRepository,
                $this->userRepository,
                $this->serializer,
            ])
            ->onlyMethods($onlyMockedMethods)
            ->getMock();
    }

    private function generateActivityLog(array $data = []): ActivityLog
    {
        return (new ActivityLog())
            ->setId(rand(10000, 99999))
            ->setAction($data['action'] ?? ActivityLog::ACTION_ALERT_ANIMATOR_ON_SEVERAL_SESSIONS)
            ->setCreatedAt($data['createdAt'] ?? new \DateTime())
            ->setInfos($data['infos'] ?? []);
    }

    private function getActivityLogNamespace(): string
    {
        $reflection_class = new \ReflectionClass(ActivityLogService::class);

        return $reflection_class->getNamespaceName();
    }

    public function invalidActivityLogInfosData(): \Generator
    {
        yield 'With empty infos' => [$this->generateActivityLog()];
        yield 'Without links entry' => [$this->generateActivityLog([
            'infos' => ['data' => 'toto'],
        ])];
        yield 'Without routeKey on links entry' => [$this->generateActivityLog([
            'infos' => ['links' => ['other' => 'stuff']],
        ])];
        yield 'With invalid routeParam on links entry' => [$this->generateActivityLog([
            'infos' => ['links' => ['routeKey' => 'do', 'routeParam' => 'nothing']],
        ])];
        yield 'With invalid routeParam id for import link' => [$this->generateActivityLog([
            'action' => ActivityLog::ACTION_IMPORT_SESSIONS,
            'infos' => ['links' => ['routeKey' => 'download_import_file', 'routeParam' => ['wrong' => 'yolo']]],
        ])];
        yield 'With invalid routeParam filename for export link' => [$this->generateActivityLog([
            'action' => ActivityLog::ACTION_EXPORT_DATA,
            'infos' => ['links' => ['routeKey' => 'download_export_file', 'routeParam' => ['wrong' => 'yolo']]],
        ])];
        yield 'With valid info but not allowed to be cleaned up' => [$this->generateActivityLog([
            'infos' => ['links' => ['routeKey' => 'download_import_file', 'routeParam' => ['id' => self::ACTIVITY_LOG_FILENAME]]],
        ]), 'Import'];
    }

    public function validActivityLogInfoData(): \Generator
    {
        yield 'ACTION_IMPORT_SESSIONS' => [$this->generateActivityLog([
            'action' => ActivityLog::ACTION_IMPORT_SESSIONS,
            'infos' => ['links' => ['routeKey' => 'download_import_file', 'routeParam' => ['id' => self::ACTIVITY_LOG_FILENAME]]],
        ]), 'Import'];
        yield 'ACTION_IMPORT_USERS' => [$this->generateActivityLog([
            'action' => ActivityLog::ACTION_IMPORT_USERS,
            'infos' => ['links' => ['routeKey' => 'download_import_file', 'routeParam' => ['id' => self::ACTIVITY_LOG_FILENAME]]],
        ]), 'Import'];
        yield 'ACTION_EXPORT_DATA' => [$this->generateActivityLog([
            'action' => ActivityLog::ACTION_EXPORT_DATA,
            'infos' => ['links' => ['routeKey' => 'download_export_file', 'routeParam' => ['filename' => self::ACTIVITY_LOG_FILENAME]]],
        ]), 'Export'];
    }

    // <editor-folder> findActivityLogFileName
    /** @dataProvider invalidActivityLogInfosData */
    public function testActivityLogWithoutFilenameReturnNull(ActivityLog $activityLog): void
    {
        $activityLogService = $this->generateActivityLogService();
        $this->assertNull($activityLogService->findActivityLogFileName($activityLog));
    }

    /** @dataProvider validActivityLogInfoData */
    public function testReturnActivityLogAttachedFileNameSuccessfully(ActivityLog $activityLog, string $type): void
    {
        $activityLogService = $this->generateActivityLogService();
        $returnFilename = $activityLogService->findActivityLogFileName($activityLog);

        if ($type === 'Import') {
            $this->assertSame("$this->importsSavefilesDirectory/".self::ACTIVITY_LOG_FILENAME, $returnFilename);
        } else {
            $this->assertSame("$this->exportsSavefilesDirectory/".self::ACTIVITY_LOG_FILENAME, $returnFilename);
        }
    }
    // </editor-folder> findActivityLogFileName

    // <editor-folder> cleanGivenActivityLogs
    public function notAnActivityLogClassData(): \Generator
    {
        yield 'String' => ['string'];
        yield 'Number' => [418];
        yield 'Array' => [[]];
        yield 'Object' => [(object) []];
        yield 'Another class' => [new User()];
    }

    /** @dataProvider invalidActivityLogInfosData */
    /** @dataProvider notAnActivityLogClassData */
    public function testRemoveActivityLogsNeverBeCalled(mixed $activityLog): void
    {
        $this->getFunctionMock($this->getActivityLogNamespace(), 'unlink')
            ->expects($this->never());

        $this->entityManager
            ->expects($this->never())
            ->method('remove');

        $activityLogService = $this->generateActivityLogService();
        $activityLogService->cleanGivenActivityLogs([$activityLog]);
    }

    /** @dataProvider validActivityLogInfoData */
    public function testAllowedToBeCleanedUpRemoveSuccessfully(ActivityLog $activityLog): void
    {
        $this->getFunctionMock($this->getActivityLogNamespace(), 'is_null')
             ->expects($this->once())
            ->willReturn(false);
        $this->getFunctionMock($this->getActivityLogNamespace(), 'file_exists')
             ->expects($this->once())
            ->willReturn(true);
        $this->getFunctionMock($this->getActivityLogNamespace(), 'unlink')
             ->expects($this->once())
            ->willReturn(true);

        $this->entityManager
            ->expects($this->once())
            ->method('remove');

        $activityLogService = $this->generateActivityLogService(['findActivityLogFileName']);
        $activityLogService
            ->expects($this->once())
            ->method('findActivityLogFileName')
            ->willReturn('folder/'.self::ACTIVITY_LOG_FILENAME);

        $activityLogService->cleanGivenActivityLogs([$activityLog]);
    }
    // </editor-folder> cleanGivenActivityLogs
}
