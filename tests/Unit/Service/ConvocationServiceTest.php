<?php

namespace App\Tests\Unit\Service;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectTelephonyProfile;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\Convocation;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\ProviderSessionAccessToken;
use App\Entity\User;
use App\Service\ActivityLogger;
use App\Service\ConvocationService;
use App\Service\MailerService;
use App\Service\ReferrerService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class ConvocationServiceTest extends TestCase
{
    public function createConvocationService(array $onlyMockedMethods = []): ConvocationService
    {
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $translatorMock->expects($this->any())
            ->method('trans')
            ->willReturn('');

        if (empty($onlyMockedMethods)) {
            return new ConvocationService(
            $this->createMock(Environment::class),
            $translatorMock,
            $this->createMock(RouterInterface::class),
            'endPointDomain',
            $this->createMock(ActivityLogger::class),
            $this->createMock(MailerService::class),
                'just/a/path',
            $this->createMock(ReferrerService::class)
            );
        } else {
            $convocationService = $this->getMockBuilder(ConvocationService::class)
                        ->setConstructorArgs([
                            $this->createMock(Environment::class),
                            $translatorMock,
                            $this->createMock(RouterInterface::class),
                            'endPointDomain',
                            $this->createMock(ActivityLogger::class),
                            $this->createMock(MailerService::class),
                            'just/a/path',
                            $this->createMock(ReferrerService::class),
                        ])
                        ->onlyMethods($onlyMockedMethods)
                        ->getMock();

            $convocationService
                ->expects($this->any())
                ->method('transformShortsCodes')
                ->willReturn('a string');

            return $convocationService;
        }
    }

    private function createConvocationMock(): Convocation
    {
        $convocation = $this->createMock(Convocation::class);
        $convocation->expects($this->any())
            ->method('getSubjectMail')
            ->willReturn('a subject');

        $convocation->expects($this->any())
                    ->method('getContent')
                    ->willReturn('a content');

        return $convocation;
    }

    private function createAdobeConnectPrincipal(): AdobeConnectPrincipal
    {
        $userMock = $this->createMock(User::class);

        $userMock->method('getId')
            ->willReturn(rand(1, 6));

        $userMock->method('getFirstName')
            ->willReturn('Firstname');

        $userMock->method('getLastName')
            ->willReturn('LASTNAME');

        $userMock->method('getEmail')
            ->willReturn('myemail@plop.fr');

        return (new AdobeConnectPrincipal((new AdobeConnectConfigurationHolder())->setClient((new Client())->setName('client'))))
            ->setUser($userMock);
    }

    private function createProviderSession(ProviderSession $providerSession): ProviderSession
    {
        $datesStart = new \DateTime('2021-08-14 09:10:00');
        $dateEnd = new \DateTime('2021-08-14 19:05:00');

        $providerSession->getAbstractProviderConfigurationHolder()
            ->setClient((new Client())->setName('TEST'));

        $session = $providerSession
            ->setId('123')
            ->setName('My name')
            ->setDuration(65)
            ->setDateStart($datesStart)
            ->setDateEnd($dateEnd)
            ->setLanguage('fr')
            ->setRef('123654789')
            ->setConvocation($this->createConvocationMock());

        return $session;
    }

    public function getProviderSession(): \Generator
    {
        $client = (new Client())
            ->setName('client')
            ->setSlug('client');

        yield [$this->createProviderSession(new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient($client))), $this->createAdobeConnectPrincipal()];
        yield [$this->createProviderSession(new WebexRestMeeting((new WebexRestConfigurationHolder())->setClient(($client)))), $this->createAdobeConnectPrincipal()];
    }

    /**
     * @dataProvider getProviderSession
     */
    public function testTemplatesParametersMatchWithTypeOfProviderSessionSuccessfully(ProviderSession $providerSession, ProviderParticipant $participant): void
    {
        $audioProvider = (new AdobeConnectTelephonyProfile())
            ->setAdobeDefaultPhoneNumber('087546495647')
            ->setName('InterCall')
            ->setCodeParticipant(2132468)
            ->setCodeAnimator(641687987)
            ->setConnectionType(AbstractProviderAudio::AUDIO_INTERCALL)
            ->setPhoneNumber('0477885522');

        $providerSession->setProviderAudio($audioProvider);

        $providerParticipantSessionRole = (new ProviderParticipantSessionRole())
            ->setSession($providerSession)
            ->setParticipant($participant)
            ->setProviderSessionAccessToken((new ProviderSessionAccessToken())
                ->setToken('token')
                ->setIv('iv')
                ->setTag('tag')
            );
        $templateParameters = $this->createConvocationService()
            ->getTemplateParameters($providerParticipantSessionRole);

        $this->assertArrayHasKey('name', $templateParameters);
        $this->assertSame($templateParameters['name'], $providerSession->getName());
        $this->assertArrayHasKey('duration', $templateParameters);
        $this->assertSame($templateParameters['duration'], $providerSession->getDuration());
        $this->assertArrayHasKey('dateStart', $templateParameters);
        $this->assertSame($templateParameters['dateStart'], $providerSession->getDateStart());
        $this->assertArrayHasKey('dateEnd', $templateParameters);
        $this->assertSame($templateParameters['dateEnd'], $providerSession->getDateEnd());
        $this->assertArrayHasKey('langue', $templateParameters);
        $this->assertSame($templateParameters['langue'], $providerSession->getLanguage());
        $this->assertArrayHasKey('username', $templateParameters);
        $this->assertSame($templateParameters['username'], "{$participant->getFirstName()} {$participant->getLastName()}");

        switch (get_class($providerSession)) {
            case AdobeConnectSCO::class:
                $this->assertArrayHasKey('audioRealNumber', $templateParameters);
                $this->assertSame($templateParameters['audioRealNumber'], $audioProvider->getPhoneNumber());
                $this->assertArrayHasKey('audioSharedNumber', $templateParameters);
                $this->assertSame($templateParameters['audioSharedNumber'], $audioProvider->getAdobeDefaultPhoneNumber());
                $this->assertArrayHasKey('audioAnimatorCode', $templateParameters);
                $this->assertSame($templateParameters['audioAnimatorCode'], $audioProvider->getCodeAnimator());
                $this->assertArrayHasKey('audioParticipantCode', $templateParameters);
                $this->assertSame($templateParameters['audioParticipantCode'], $audioProvider->getCodeParticipant());
                break;

            /*case WebexMeeting::class:
                $this->assertArrayHasKey('audioRealNumber', $templateParameters);
                $this->assertSame($templateParameters['audioRealNumber'], $audioProvider->getPhoneNumber());
                $this->assertArrayHasKey('audioAnimatorCodeConcat', $templateParameters);
                $this->assertSame($templateParameters['audioAnimatorCodeConcat'], !empty($audioProvider->getCodeAnimator())
                    ? str_replace(' ', '', $audioProvider->getCodeAnimator())
                    : '');
                $this->assertArrayHasKey('audioAnimatorCode', $templateParameters);
                $this->assertSame($templateParameters['audioAnimatorCode'], $audioProvider->getCodeAnimator());
                $this->assertArrayHasKey('audioParticipantCode', $templateParameters);
                $this->assertSame($templateParameters['audioParticipantCode'], $audioProvider->getCodeParticipant());
                break;*/
        }
    }

    /**
     * @dataProvider getProviderSession
     */
    public function testAddADateOnHistoryOfSendingAsAConvocationPass(ProviderSession $providerSession, ProviderParticipant $participant): void
    {
        $providerSessionRole = (new ProviderParticipantSessionRole())
            ->setSession($providerSession)
            ->setParticipant($participant);
        $this->createConvocationService(['transformShortsCodes', 'getTemplateParameters'])
            ->sendConvocation($providerSessionRole);

        $this->assertInstanceOf(\DateTimeImmutable::class, $providerSessionRole->getLastConvocationSent());
        $this->assertNull($providerSessionRole->getLastReminderSent());
    }

    /**
     * @dataProvider getProviderSession
     */
    public function testAddADateOnHistoryOfSendingAsAReminderPass(ProviderSession $providerSession, ProviderParticipant $participant): void
    {
        $providerSessionRole = (new ProviderParticipantSessionRole())
            ->setSession($providerSession)
            ->setParticipant($participant);
        $this->createConvocationService(['transformShortsCodes', 'getTemplateParameters'])
             ->sendConvocation($providerSessionRole, true);

        $this->assertInstanceOf(\DateTimeImmutable::class, $providerSessionRole->getLastReminderSent());
        $this->assertNull($providerSessionRole->getLastConvocationSent());
    }
}
