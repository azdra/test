<?php

namespace App\Tests\Unit\Service;

use App\Entity\Client\Client;
use App\Entity\User;
use App\Repository\ParticipantSessionSignatureRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Security\UserSecurityService;
use App\Service\MailerService;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\UserService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Security;

class UserServiceTest extends TestCase
{
    protected function createUserServiceMock(?UserSecurityService $userSecurityService = null, ?UserRepository $userRepository = null, ?EntityManagerInterface $entityManager = null, ?Security $security = null, ?ParticipantSessionSignatureRepository $signatureRepository = null): UserService
    {
        $userSecurityService ??= $this->createUserSecurityService();
        $userRepository ??= $this->createMock(UserRepository::class);
        $providerSessionRepository ??= $this->createMock(ProviderSessionRepository::class);
        $participantSessionSubscriber ??= $this->createMock(ParticipantSessionSubscriber::class);
        $entityManager ??= $this->createEntityManagerMock();
        $security ??= $this->createMock(Security::class);
        $signatureRepository ??= $this->createMock(ParticipantSessionSignatureRepository::class);

        return new UserService($userSecurityService, $userRepository, $providerSessionRepository, $participantSessionSubscriber, $entityManager, $security, $signatureRepository, $this->createMock(MailerService::class));
    }

    protected function createEntityManagerMock(?ServiceEntityRepository $repository = null): EntityManagerInterface|MockObject
    {
        $mock = $this->createMock(EntityManagerInterface::class);

        if ($repository) {
            $mock->method('getRepository')
                ->willReturn($repository);
        }

        return $mock;
    }

    protected function createUserSecurityService(?EventDispatcherInterface $dispatcher = null, ?UserRepository $userRepository = null, ?EntityManagerInterface $entityManager = null): UserSecurityService
    {
        $dispatcher ??= $this->createMock(EventDispatcherInterface::class);
        $userRepository ??= $this->createMock(UserRepository::class);
        $entityManager ??= $this->createMock(EntityManagerInterface::class);

        return new UserSecurityService($dispatcher, $userRepository, $entityManager);
    }

    public function getDataCreateUser(): \Generator
    {
        yield [null];
        yield [[User::ROLE_TRAINEE]];
        yield [[User::ROLE_ADMIN]];
        yield [[User::ROLE_ADMIN, User::ROLE_MANAGER]];
    }

    /**
     * @dataProvider getDataCreateUser
     */
    public function testCreateUser($roles): void
    {
        $loggedUser = $this->createMock(User::class);
        $loggedUser
            ->method('getClient')
            ->willReturn(new Client());

        $security = $this->createMock(Security::class);
        $security->method('getUser')->willReturn($loggedUser);

        $userService = $this->createUserServiceMock(security: $security);

        $user = $userService->createUser($roles);

        if (null === $roles) {
            $roles = [User::ROLE_TRAINEE];
        }

        $this->assertSame($roles, $user->getRoles());
    }
}
