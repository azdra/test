<?php

namespace App\Tests\Unit\Service\Exporter;

use App\Entity\User;
use App\Exception\Exporter\ExporterException;
use App\Service\ActivityLogger;
use App\Service\Exporter\ExporterFactory;
use App\Service\Exporter\ExporterService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExporterServiceTest extends TestCase
{
    private ExporterFactory $exporterFactory;
    private ActivityLogger $activityLogger;
    private Security $security;
    private \Redis $redis;
    private TranslatorInterface $translator;
    private LoggerInterface $logger;

    protected function setUp(): void
    {
        $this->exporterFactory = $this->createMock(ExporterFactory::class);
        $this->security = $this->createMock(Security::class);
        $this->redis = $this->createMock(\Redis::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->activityLogger = $this->createMock(ActivityLogger::class);
    }

    private function generateExportService(): ExporterService
    {
        return new ExporterService(
            $this->exporterFactory,
            $this->activityLogger,
            $this->security,
            $this->redis,
            $this->translator,
            'path',
            $this->logger
        );
    }

    private function generateCurrentUser(array $data = []): User
    {
        return (new User())
            ->setEmail('myemail@gg.com')
            ->setId($data['id'] ?? 0);
    }

    public function testTokenValidationPassSuccessful(): void
    {
        $expectedUserId = 418;
        $expectedPath = 'my-awesome-token';
        $storedToken = "$expectedUserId|$expectedPath";

        $user = $this->generateCurrentUser(['id' => $expectedUserId]);

        $this->redis->expects($this->once())
            ->method('exists')
            ->willReturn(true);

        $this->redis->expects($this->once())
                    ->method('get')
                    ->willReturn($storedToken);

        $this->security->expects($this->once())
            ->method('getUser')
            ->willReturn($user);

        $exporterService = $this->generateExportService();
        $path = $exporterService->validateDownloadingToken('my-token');

        $this->assertSame($expectedPath, $path);
    }

    public function testTokenValidationFailureForNotFount(): void
    {
        $this->redis->expects($this->once())
                    ->method('exists')
                    ->willReturn(false);

        $this->redis->expects($this->never())
                    ->method('get');

        $this->redis->expects($this->never())
                    ->method('unlink');

        $this->expectException(ExporterException::class);
        $this->expectExceptionMessage('Invalid token. It is not valid or has not been found');

        $exporterService = $this->generateExportService();
        $exporterService->validateDownloadingToken('my-token');
    }

    public function testTokenValidationFailureForMismatchUser(): void
    {
        $user = $this->generateCurrentUser();

        $this->redis->expects($this->once())
                    ->method('exists')
                    ->willReturn(true);

        $this->redis->expects($this->once())
                    ->method('get')
            ->willReturn('418|my-awesome-path');

        $this->redis->expects($this->never())
                    ->method('unlink');

        $this->security->expects($this->once())
                       ->method('getUser')
                       ->willReturn($user);

        $this->expectException(ExporterException::class);
        $this->expectExceptionMessage('The current user do not match with the token');

        $exporterService = $this->generateExportService();
        $exporterService->validateDownloadingToken('my-token');
    }
}
