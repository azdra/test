<?php

namespace App\Tests\Unit\Service\Exporter\Spreadsheet\Xlsx;

use App\Entity\User;
use App\Repository\ActivityLogRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\Spreadsheet\Xlsx\XslxSpreadsheetUserRGPDDatasExporter;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class XslxSpreadsheetUserRGPDDatasExporterTest extends TestCase
{
    private User $user;
    private MockObject|TranslatorInterface $translator;
    private MockObject|SluggerInterface $slugger;
    private MockObject|ProviderSessionRepository $providerSessionRepository;
    private MockObject|ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository;
    private MockObject|ActivityLogRepository $activityLogRepository;
    private string $exportsSavefilesDirectory = 'data/exports_savefiles';

    public function setUp(): void
    {
        $this->user = (new User())
            ->setFirstName('test')
            ->setLastName('paul')
            ->setEmail('test.paul@test.fr')
            ->setRoles(['ROLE_MANAGER', 'CAN_SWITCH_USER']);

        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->slugger = $this->createMock(SluggerInterface::class);
        $this->providerSessionRepository = $this->createMock(ProviderSessionRepository::class);
        $this->providerParticipantSessionRoleRepository = $this->createMock(ProviderParticipantSessionRoleRepository::class);
        $this->activityLogRepository = $this->createMock(ActivityLogRepository::class);
    }

    public function getService(): XslxSpreadsheetUserRGPDDatasExporter
    {
        return new XslxSpreadsheetUserRGPDDatasExporter(
            $this->translator,
            $this->slugger,
            $this->exportsSavefilesDirectory,
            $this->providerSessionRepository,
            $this->providerParticipantSessionRoleRepository,
            $this->activityLogRepository
        );
    }

    public function testGetSupportFormat(): void
    {
        $sut = $this->getService();

        $this->assertSame(ExportFormat::XLSX, $sut->getSupportFormat());
    }

    public function testGetSupportType(): void
    {
        $sut = $this->getService();

        $this->assertSame('user_rgpd_datas', $sut->getSupportType());
    }
}
