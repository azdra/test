<?php

namespace App\Tests\Unit\Service;

use App\CustomerService\Service\HelpNeedService;
use App\Entity\Client\ClientEmailScheduling;
use App\Entity\ProviderSession;
use App\Repository\AbstractProviderConfigurationHolderRepository;
use App\Repository\ConvocationRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\MailerService;
use App\Service\MiddlewareService;
use App\Service\Provider\Adobe\AdobeConnectScoArrayHandler;
use App\Service\Provider\Adobe\AdobeConnectWebinarScoArrayHandler;
use App\Service\Provider\Microsoft\MicrosoftTeamsArrayHandler;
use App\Service\Provider\Microsoft\MicrosoftTeamsEventArrayHandler;
use App\Service\Provider\Webex\WebexMeetingArrayHandler;
use App\Service\Provider\Webex\WebexRestMeetingArrayHandler;
use App\Service\Provider\White\WhiteArrayHandler;
use App\Service\ProviderSessionService;
use App\Service\RessourceUploader\RessourceUploader;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProviderSessionServiceTest extends TestCase
{
    private MockObject|MiddlewareService $middlewareService;
    private MockObject|EntityManagerInterface $entityManager;
    private MockObject|ActivityLogger $activityLogger;
    private MockObject|RouterInterface $router;
    private MockObject|LoggerInterface $logger;
    private AdobeConnectScoArrayHandler $adobeArrayHandler;
    private AdobeConnectWebinarScoArrayHandler $adobeConnectWebinarScoArrayHandler;
    private WebexMeetingArrayHandler $webexArrayHandler;
    private WebexRestMeetingArrayHandler $webexRestMeetingArrayHandler;
    private MicrosoftTeamsArrayHandler $microsoftTeamsArrayHandler;
    private MicrosoftTeamsEventArrayHandler $microsoftTeamsEventArrayHandler;
    private WhiteArrayHandler $whiteArrayHandler;
    private EventDispatcherInterface $eventDispatcher;
    private MockObject|UserRepository $userRepository;
    private MockObject|MailerService $mailerService;
    private MockObject|TranslatorInterface $translator;
    private MockObject|HelpNeedService $helpNeedService;
    private MockObject|AbstractProviderConfigurationHolderRepository $configurationHolderRepository;
    private MockObject|ConvocationRepository $convocationRepository;
    private MockObject|ProviderSessionRepository $providerSessionRepository;
    private MockObject|RessourceUploader $ressourceUploader;
    private MockObject|ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->middlewareService = $this->createMock(MiddlewareService::class);
        $this->activityLogger = $this->createMock(ActivityLogger::class);
        $this->adobeArrayHandler = $this->createMock(AdobeConnectScoArrayHandler::class);
        $this->adobeConnectWebinarScoArrayHandler = $this->createMock(AdobeConnectWebinarScoArrayHandler::class);
        $this->webexArrayHandler = $this->createMock(WebexMeetingArrayHandler::class);
        $this->webexRestMeetingArrayHandler = $this->createMock(WebexRestMeetingArrayHandler::class);
        $this->microsoftTeamsArrayHandler = $this->createMock(MicrosoftTeamsArrayHandler::class);
        $this->microsoftTeamsEventArrayHandler = $this->createMock(MicrosoftTeamsEventArrayHandler::class);
        $this->whiteArrayHandler = $this->createMock(WhiteArrayHandler::class);
        $this->router = $this->createMock(RouterInterface::class);
        $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->mailerService = $this->createMock(MailerService::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->helpNeedService = $this->createMock(HelpNeedService::class);
        $this->configurationHolderRepository = $this->createMock(AbstractProviderConfigurationHolderRepository::class);
        $this->convocationRepository = $this->createMock(ConvocationRepository::class);
        $this->providerSessionRepository = $this->createMock(ProviderSessionRepository::class);
        $this->ressourceUploader = $this->createMock(RessourceUploader::class);
        $this->providerParticipantSessionRoleRepository = $this->createMock(ProviderParticipantSessionRoleRepository::class);
    }

    public function getService(): ProviderSessionService
    {
        return new ProviderSessionService(
            $this->middlewareService,
            $this->entityManager,
            $this->adobeArrayHandler,
            $this->adobeConnectWebinarScoArrayHandler,
            $this->webexArrayHandler,
            $this->webexRestMeetingArrayHandler,
            $this->microsoftTeamsArrayHandler,
            $this->microsoftTeamsEventArrayHandler,
            $this->whiteArrayHandler,
            $this->activityLogger,
            $this->router,
            $this->eventDispatcher,
            $this->userRepository,
            $this->mailerService,
            $this->translator,
            $this->logger,
            $this->helpNeedService,
            $this->configurationHolderRepository,
            $this->convocationRepository,
            $this->providerSessionRepository,
            $this->ressourceUploader,
            $this->providerParticipantSessionRoleRepository,
            'path_convocation_attachment'
        );
    }

    public function testValidReferenceSession(): void
    {
        $reference = ProviderSessionService::generateSessionReference('My Name');

        $this->assertEquals(18, strlen($reference));
        $this->assertStringStartsWith('MY', $reference);
        $this->assertMatchesRegularExpression('/\d{16}$/', $reference);
    }

    public function testApplyRemindersAndConvocationPreferenceNoUpdateOnEmptyConfig(): void
    {
        $clientEmailScheduling = (new ClientEmailScheduling())
            ->setTimeToSendConvocationBeforeSession(null)
            ->setTimesToSendReminderBeforeSession([]);
        $providerSession = $this->createMock(ProviderSession::class);
        $providerSession->expects($this->once())->method('getDateStart')->willReturn(new \DateTime('2021/12/20 20:45'));

        $providerSession->expects($this->never())->method('setNotificationDate');
        $providerSession->expects($this->never())->method('setDateOfSendingReminders');

        $this->getService()->applyRemindersAndConvocationPreference($clientEmailScheduling, $providerSession);
    }

    public function testApplyRemindersAndConvocationPreferenceUpdateProperly(): void
    {
        $clientEmailScheduling = (new ClientEmailScheduling())
            ->setTimeToSendConvocationBeforeSession(['days' => 15, 'hours' => 12, 'minutes' => 30])
            ->setTimesToSendReminderBeforeSession([
                ['days' => 10, 'hours' => 00, 'minutes' => 30],
                ['days' => 5, 'hours' => 00, 'minutes' => 00],
            ]);
        $providerSession = $this->createMock(ProviderSession::class);
        $providerSession->expects($this->once())->method('getDateStart')->willReturn(new \DateTime('2021/12/20 20:45'));

        $providerSession->expects($this->once())
            ->method('setNotificationDate')
            ->with(new \DateTimeImmutable('2021/12/05 08:15:00'));
        $providerSession->expects($this->once())
            ->method('setDateOfSendingReminders')
            ->with([
                new \DateTimeImmutable('2021/12/10 20:15:00'),
                new \DateTimeImmutable('2021/12/15 20:45:00'),
            ]);

        $this->getService()->applyRemindersAndConvocationPreference($clientEmailScheduling, $providerSession);
    }

    public function testGetConvocationAndRemindersDateReturnNullOnEmptyData(): void
    {
        $clientEmailScheduling = (new ClientEmailScheduling())
            ->setTimeToSendConvocationBeforeSession(null)
            ->setTimesToSendReminderBeforeSession([]);

        $dates = $this->getService()->getConvocationAndRemindersDate($clientEmailScheduling, new \DateTime('2021/12/20 20:45'));

        $this->assertCount(2, $dates);
        $this->assertNull($dates['convocation']);
        $this->assertNull($dates['reminders']);
    }

    public function testGetConvocationAndRemindersDateReturnCalculatedDateWithoutFormat(): void
    {
        $clientEmailScheduling = (new ClientEmailScheduling())
            ->setTimeToSendConvocationBeforeSession(['days' => 15, 'hours' => 12, 'minutes' => 30])
            ->setTimesToSendReminderBeforeSession([
                ['days' => 10, 'hours' => 00, 'minutes' => 30],
                ['days' => 5, 'hours' => 00, 'minutes' => 00],
            ]);

        $dates = $this->getService()->getConvocationAndRemindersDate($clientEmailScheduling, new \DateTime('2021/12/20 20:45'));

        $this->assertCount(2, $dates);
        $this->assertEquals(new \DateTimeImmutable('2021/12/05 08:15:00'), $dates['convocation']);
        $this->assertEquals([
            new \DateTimeImmutable('2021/12/10 20:15:00'),
            new \DateTimeImmutable('2021/12/15 20:45:00'),
        ], $dates['reminders']);
    }

    public function testGetConvocationAndRemindersDateReturnCalculatedDateWithFormat(): void
    {
        date_default_timezone_set('UTC');

        $clientEmailScheduling = (new ClientEmailScheduling())
            ->setTimeToSendConvocationBeforeSession(['days' => 15, 'hours' => 12, 'minutes' => 30])
            ->setTimesToSendReminderBeforeSession([
                ['days' => 10, 'hours' => 00, 'minutes' => 30],
                ['days' => 5, 'hours' => 00, 'minutes' => 00],
            ]);

        $dates = $this->getService()->getConvocationAndRemindersDate($clientEmailScheduling, new \DateTime('2021/12/20 20:45'), \DATE_ATOM);
        $dateActual = (new \DateTime())->format('Y-m-d').'T'.(new \DateTime())->format('H:i:s').'+00:00';

        $this->assertCount(2, $dates);
        $this->assertEquals($dateActual, $dates['convocation']);
        $this->assertEquals([
            '2021-12-10T20:15:00+00:00',
            '2021-12-15T20:45:00+00:00',
        ], $dates['reminders']);
        $this->assertCount(2, $dates['reminders']);
        $this->assertEquals('2021-12-10T20:15:00+00:00', $dates['reminders'][0]);
        $this->assertEquals('2021-12-15T20:45:00+00:00', $dates['reminders'][1]);
    }
}
