<?php

namespace App\Tests\Unit\Service;

use App\Entity\Client\Client;
use App\Exception\ClientNoMoreCreditAvailableException;
use App\Repository\ClientRepository;
use App\Service\ActivityLogger;
use App\Service\ClientService;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

class ClientServiceTest extends TestCase
{
    protected function createClientService(
        ?UserService $userService = null,
        ?ActivityLogger $activityLogger = null,
        ?ClientRepository $clientRepository = null,
        ?TranslatorInterface $translator = null,
        ?EntityManagerInterface $entityManager = null,
    ): ClientService {
        $userService ??= $this->createMock(UserService::class);
        $activityLogger ??= $this->createMock(ActivityLogger::class);
        $clientRepository ??= $this->createMock(ClientRepository::class);
        $translator ??= $this->createMock(TranslatorInterface::class);
        $entityManager ??= $this->createMock(EntityManagerInterface::class);

        return new ClientService($userService, $activityLogger, $clientRepository, $translator, $entityManager);
    }

    public function testClientUseCreditParticipantsThrowsExceptionWhenNoMoreCreditAvailable(): void
    {
        // Créez un objet Client avec un crédit participant de 0 et un mode de facturation participant
        $client = (new Client())
            ->setId(123)
            ->setName('ClientBillingModeParticipant')
            ->setBillingMode(Client::BILLING_MODE_PARTICIPANT);
        $client->setAllowedCreditParticipants(0);

        // Initialisez l'objet sous test
        $clientService = $this->createClientService();

        // Vérifiez que l'appel à clientUseCreditParticipants lance une exception ClientNoMoreCreditAvailableException
        $this->expectException(ClientNoMoreCreditAvailableException::class);
        $clientService->clientUseCreditParticipants($client);
    }

    public function testClientUseCreditParticipantsDoesNotThrowExceptionWhenNoMoreCreditAvailable(): void
    {
        // Créez un objet Client avec un crédit participant de 1 et un mode de facturation participant
        $client = (new Client())
            ->setId(123)
            ->setName('ClientBillingModeParticipant')
            ->setBillingMode(Client::BILLING_MODE_PARTICIPANT);
        $client->setAllowedCreditParticipants(1);

        // Initialisez l'objet sous test
        $clientService = $this->createClientService();

        // Vérifiez que l'appel à clientUseCreditParticipants ne lance pas d'exception
        $this->assertNull($clientService->clientUseCreditParticipants($client));
    }

    public function testClientUseCreditParticipantsDecrementsCreditParticipants(): void
    {
        // Créez un objet Client avec un crédit participant de 1 et un mode de facturation participant
        $client = (new Client())
            ->setId(123)
            ->setName('ClientBillingModeParticipant')
            ->setBillingMode(Client::BILLING_MODE_PARTICIPANT);
        $client->setAllowedCreditParticipants(1);

        // Initialisez l'objet sous test
        $clientService = $this->createClientService();

        // Vérifiez que l'appel à clientUseCreditParticipants décrémente le crédit participant
        $clientService->clientUseCreditParticipants($client);
        $this->assertEquals(0, $client->getRemainingCreditParticipants());
    }

    public function testClientUseCreditParticipantsDecrementCreditParticipantsWhenBillingModeIsNotParticipant(): void
    {
        // Créez un objet Client avec un crédit participant de 1 et un mode de facturation autre que participant
        $client = (new Client())
            ->setId(123)
            ->setName('ClientBillingModeParticipant')
            ->setBillingMode(Client::BILLING_MODE_SESSION);
        $client->setAllowedCreditParticipants(1);

        // Initialisez l'objet sous test
        $clientService = $this->createClientService();

        // Vérifiez que l'appel à clientUseCreditParticipants décrémente le crédit participant
        $clientService->clientUseCreditParticipants($client);
        $this->assertEquals(0, $client->getRemainingCreditParticipants());
    }

    public function testClientUseCreditParticipantsWithQuantity(): void
    {
        // Créez un objet Client avec un crédit participant de 1 et un mode de facturation autre que participant
        $client = (new Client())
            ->setId(123)
            ->setName('ClientBillingModeParticipant')
            ->setBillingMode(Client::BILLING_MODE_PARTICIPANT);
        $client->setAllowedCreditParticipants(10);

        // Initialisez l'objet sous test
        $clientService = $this->createClientService();

        // Vérifiez que l'appel à clientUseCreditParticipants décrémente le crédit participant
        $clientService->clientUseCreditParticipantsWithQuantity($client, 2);
        $this->assertEquals(8, $client->getRemainingCreditParticipants());
    }
}
