<?php

namespace App\Tests\Unit\SelfSubscription\Services;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\SelfSubscription\Entity\Module;
use App\SelfSubscription\Entity\Subject;
use App\SelfSubscription\Entity\Theme;
use App\SelfSubscription\Service\StatisticsService;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class StatisticsSelfSubscriptionServiceTest extends TestCase
{
    public function getService(): StatisticsService
    {
        return new StatisticsService();
    }

    public function testStatisticsModuleCountAllSessionModuleEmpty()
    {
        $client = (new Client())->setName('clientTest');
        $module = new Module($client);

        $actual = $this->getService()->statisticsModule($module);

        $this->assertEquals(0, $actual['sessionCreateNumber']);
    }

    public function testStatisticsModuleCountAllSessionNotEmpty()
    {
        [$module,$session] = $this->creatModule();

        $actual = $this->getService()->statisticsModule($module);

        $this->assertEquals(1, $actual['sessionCreateNumber']);
    }

    public function testStatisticsModuleCountUnlimitedSessionsWhenSubscriptionMaxIsEmpty()
    {
        [$module,$session] = $this->creatModule();
        $session->setSubscriptionMax(0);

        $actual = $this->getService()->statisticsModule($module);

        $this->assertEquals(1, $actual['sessionUnlimitedNumber']);
    }

    public function testStatisticsModuleCountUnlimitedSessionsWhenSubscriptionMaxIsNotEmpty()
    {
        [$module,$session] = $this->creatModule();
        $session->setSubscriptionMax(10);

        $actual = $this->getService()->statisticsModule($module);

        $this->assertEquals(0, $actual['sessionUnlimitedNumber']);
    }

    public function testStatisticsModuleCountSessionSentExist()
    {
        [$module,$session] = $this->creatModule();
        $session->setStatus(ProviderSession::STATUS_SCHEDULED)
                ->setDateStart((new \DateTime())->modify('+1 day'))
                ->setLastConvocationSent(new \DateTimeImmutable())
                ->setNotificationDate(new \DateTimeImmutable());

        $actual = $this->getService()->statisticsModule($module);
        $sessionReadableState = $session->getReadableState();

        $this->assertEquals(ProviderSession::FINAL_STATUS_SENT, $sessionReadableState);
        $this->assertEquals(1, $actual['sessionSentNumber']);
    }

    public function testStatisticsModuleCountSessionSentNotExist()
    {
        [$module,$session] = $this->creatModule();

        $actual = $this->getService()->statisticsModule($module);
        $sessionReadableState = $session->getReadableState();

        $this->assertNotEquals(ProviderSession::FINAL_STATUS_SENT, $sessionReadableState);
        $this->assertEquals(0, $actual['sessionSentNumber']);
    }

    public function testStatisticsModuleCountSessionSentComplet()
    {
        [$module,$session] = $this->creatModuleWithParticipantRoleRegistered();
        $session->setSubscriptionMax(1);

        $actual = $this->getService()->statisticsModule($module);
        $sessionReadableState = $session->getReadableState();

        $this->assertEquals(ProviderSession::FINAL_STATUS_SENT, $sessionReadableState);
        $this->assertEquals(1, $actual['sessionCompletedNumber']);
    }

    public function testStatisticsModuleCountSessionSentNotComplet()
    {
        [$module,$session] = $this->creatModuleWithParticipantRoleRegistered();
        $session->setSubscriptionMax(10);

        $actual = $this->getService()->statisticsModule($module);
        $sessionReadableState = $session->getReadableState();

        $this->assertEquals(ProviderSession::FINAL_STATUS_SENT, $sessionReadableState);
        $this->assertEquals(0, $actual['sessionCompletedNumber']);
    }

    public function testStatisticsModuleCountSessionSentNotAvailable()
    {
        [$module,$session] = $this->creatModuleWithParticipantRoleRegistered();
        $session->setSubscriptionMax(1);

        $actual = $this->getService()->statisticsModule($module);
        $sessionReadableState = $session->getReadableState();

        $this->assertEquals(ProviderSession::FINAL_STATUS_SENT, $sessionReadableState);
        $this->assertEquals(0, $actual['sessionAvailableNumber']);
    }

    public function testStatisticsModuleCountSessionSentAvailable()
    {
        [$module,$session] = $this->creatModuleWithParticipantRoleRegistered();
        $session->setSubscriptionMax(10);

        $actual = $this->getService()->statisticsModule($module);
        $sessionReadableState = $session->getReadableState();

        $this->assertEquals(ProviderSession::FINAL_STATUS_SENT, $sessionReadableState);
        $this->assertEquals(1, $actual['sessionAvailableNumber']);
    }

    public function testStatisticsModuleCountSessionFinishedExist()
    {
        [$module,$session] = $this->creatModule();
        $session->setStatus(ProviderSession::STATUS_SCHEDULED)
                ->setDateStart((new \DateTime())->modify('-1 day'))
                ->setLastConvocationSent(new \DateTimeImmutable())
                ->setNotificationDate(new \DateTimeImmutable());

        $actual = $this->getService()->statisticsModule($module);
        $sessionReadableState = $session->getReadableState();

        $this->assertEquals(ProviderSession::FINAL_STATUS_FINISHED, $sessionReadableState);
        $this->assertEquals(1, $actual['sessionFinishedNumber']);
    }

    public function testStatisticsModuleCountSessionFinishedNotExist()
    {
        [$module,$session] = $this->creatModule();

        $actual = $this->getService()->statisticsModule($module);
        $sessionReadableState = $session->getReadableState();

        $this->assertNotEquals(ProviderSession::FINAL_STATUS_FINISHED, $sessionReadableState);
        $this->assertEquals(0, $actual['sessionFinishedNumber']);
    }

    public function testStatisticsModuleCountParticipantsPlacesAvailableEmpty()
    {
        [$module,$session] = $this->creatModuleWithParticipantRoleRegistered();
        $session->setSubscriptionMax(1);

        $actual = $this->getService()->statisticsModule($module);
        $sessionReadableState = $session->getReadableState();

        $this->assertEquals(ProviderSession::FINAL_STATUS_SENT, $sessionReadableState);
        $this->assertEquals(0, $actual['participantsPlacesAvailableNumber']);
    }

    public function testStatisticsModuleCountParticipantsPlacesAvailableNotEmpty()
    {
        [$module,$session] = $this->creatModuleWithParticipantRoleRegistered();
        $session->setSubscriptionMax(10);

        $actual = $this->getService()->statisticsModule($module);
        $sessionReadableState = $session->getReadableState();

        $this->assertEquals(ProviderSession::FINAL_STATUS_SENT, $sessionReadableState);
        $this->assertEquals(9, $actual['participantsPlacesAvailableNumber']);
    }

    public function testStatisticsModuleCountParticipantsRegisteredEmpty()
    {
        [$module,$session] = $this->creatModule();
        $session->setStatus(ProviderSession::STATUS_SCHEDULED)
                ->setDateStart((new \DateTime())->modify('+1 day'))
                ->setLastConvocationSent(new \DateTimeImmutable())
                ->setNotificationDate(new \DateTimeImmutable())
                ->setSubscriptionMax(1);

        $actual = $this->getService()->statisticsModule($module);
        $sessionReadableState = $session->getReadableState();

        $this->assertEquals(ProviderSession::FINAL_STATUS_SENT, $sessionReadableState);
        $this->assertEquals(0, $actual['participantsRegisteredNumber']);
    }

    public function testStatisticsModuleCountParticipantsRegisteredNotEmpty()
    {
        [$module,$session] = $this->creatModuleWithParticipantRoleRegistered();
        $session->setSubscriptionMax(10);

        $actual = $this->getService()->statisticsModule($module);
        $sessionReadableState = $session->getReadableState();

        $this->assertEquals(ProviderSession::FINAL_STATUS_SENT, $sessionReadableState);
        $this->assertEquals(1, $actual['participantsRegisteredNumber']);
    }

    public function testStatisticsModuleCountParticipantsUniqueRegisteredEmpty()
    {
        [$module,$session] = $this->creatModule();
        $session->setStatus(ProviderSession::STATUS_SCHEDULED)
                ->setDateStart((new \DateTime())->modify('+1 day'))
                ->setLastConvocationSent(new \DateTimeImmutable())
                ->setNotificationDate(new \DateTimeImmutable())
                ->setSubscriptionMax(1);

        $actual = $this->getService()->statisticsModule($module);
        $sessionReadableState = $session->getReadableState();

        $this->assertEquals(ProviderSession::FINAL_STATUS_SENT, $sessionReadableState);
        $this->assertEquals(0, $actual['participantsUniqueRegisteredNumber']);
    }

    public function testStatisticsModuleCountParticipantsUniqueRegisteredNotEmpty()
    {
        $module = $this->creatModuleWithMultiplesParticipantRoleRegisteredAndSession();

        $actual = $this->getService()->statisticsModule($module);

        $this->assertEquals(2, $actual['participantsRegisteredNumber']);
        $this->assertEquals(1, $actual['participantsUniqueRegisteredNumber']);
    }

    private function creatModule(): array
    {
        $client = (new Client())->setName('new Client');
        $module = new Module($client);
        $theme = new Theme($module);
        $module->addTheme($theme);
        $subject = new Subject($theme);
        $theme->addSubject($subject);
        $configurationHolder = new AdobeConnectConfigurationHolder();
        $configurationHolder->setClient($client);
        $session = (new AdobeConnectSCO($configurationHolder))->setSubject($subject);
        $subject->addSession($session);

        return [$module, $session];
    }

    private function creatModuleWithParticipantRoleRegistered(): array
    {
        [$module,$session] = $this->creatModule();

        $participantRoles = new ArrayCollection();
        $configurationHolder = new AdobeConnectConfigurationHolder();
        $ProviderParticipant = new AdobeConnectPrincipal($configurationHolder);
        $participantRole = (new ProviderParticipantSessionRole())->setParticipant($ProviderParticipant)
                                                                 ->setRole(ProviderParticipantSessionRole::ROLE_TRAINEE);
        $participantRoles->add($participantRole);
        $session->setStatus(ProviderSession::STATUS_SCHEDULED)
                ->setDateStart((new \DateTime())->modify('+1 day'))
                ->setLastConvocationSent(new \DateTimeImmutable())
                ->setNotificationDate(new \DateTimeImmutable())
                ->setParticipantRoles($participantRoles);

        return [$module, $session];
    }

    private function creatModuleWithMultiplesParticipantRoleRegisteredAndSession(): Module
    {
        $client = (new Client())->setName('new Client');
        $module = new Module($client);
        $theme = new Theme($module);
        $module->addTheme($theme);
        $subject = new Subject($theme);
        $theme->addSubject($subject);
        $configurationHolder = new AdobeConnectConfigurationHolder();
        $configurationHolder->setClient($client);
        $session1 = (new AdobeConnectSCO($configurationHolder))->setSubject($subject);
        $session2 = (new AdobeConnectSCO($configurationHolder))->setSubject($subject);
        $subject->addSession($session1);
        $subject->addSession($session2);

        $user = new User();
        $participantRoles = new ArrayCollection();
        $configurationHolder = new AdobeConnectConfigurationHolder();
        $ProviderParticipant = (new AdobeConnectPrincipal($configurationHolder))->setUser($user);
        $participantRole = (new ProviderParticipantSessionRole())->setParticipant($ProviderParticipant)
                                                                 ->setRole(ProviderParticipantSessionRole::ROLE_TRAINEE)->setId(1);
        $participantRoles->add($participantRole);

        $session1->setStatus(ProviderSession::STATUS_SCHEDULED)
                ->setDateStart((new \DateTime())->modify('+1 day'))
                ->setLastConvocationSent(new \DateTimeImmutable())
                ->setNotificationDate(new \DateTimeImmutable())
                ->setParticipantRoles($participantRoles);
        $session2->setStatus(ProviderSession::STATUS_SCHEDULED)
                ->setDateStart((new \DateTime())->modify('+1 day'))
                ->setLastConvocationSent(new \DateTimeImmutable())
                ->setNotificationDate(new \DateTimeImmutable())
                ->setParticipantRoles($participantRoles);

        return $module;
    }
}
