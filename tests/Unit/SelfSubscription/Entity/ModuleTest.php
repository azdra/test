<?php

namespace App\Tests\Unit\SelfSubscription\Entity;

use App\Entity\Client\Client;
use App\SelfSubscription\Entity\Module;
use PHPUnit\Framework\TestCase;

class ModuleTest extends TestCase
{
    public function providerEmailAllowed(): \Generator
    {
        yield 'True when no change (default entry *)' => ['test@test.com', true];
        yield 'False if specific blacklisted' => ['test@test.com', false, null, 'test@test.com'];
        yield 'False is general blacklisted' => ['test@test.com', false, null, '*@test.com'];
        yield 'True if not blacklisted' => ['test@test.com', true, null, 'test@unknown.com'];
        yield 'False if not whitelisted' => ['test@test.com', false, 'test@unknown.com'];
        yield 'True if specific whitelisted' => ['test@test.com', true, 'test@test.com'];
        yield 'True if general whitelisted' => ['test@test.com', true, '*@test.com'];
    }

    /**
     * @dataProvider providerEmailAllowed
     */
    public function testIsEmailAllowed(string $testedMail, bool $expected, ?string $whiteList = null, ?string $blackList = null)
    {
        $module = new Module(new Client());

        if (null !== $blackList) {
            $module->addBlackListEntry($blackList);
        }

        if (null !== ($whiteList)) {
            $module->addWhiteListEntry($whiteList);
        }

        $actual = $module->isEmailAllowed($testedMail);

        $this->assertEquals($expected, $actual);
    }
}
