<?php

namespace App\Tests\Unit\Twig;

use App\Entity\Client\Client;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Twig\CurrentUserExtension;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\SerializerInterface;

class CurrentUserExtensionTest extends TestCase
{
    public function testGetDisplayNameWithNullFirstNameAndLastName(): void
    {
        $user = (new User())
            ->setEmail('adaubois@norsys.fr')
            ->setClient(new Client())
            ->setStatus(User::STATUS_ACTIVE);

        $extension = new CurrentUserExtension(
            $this->createMock(\Redis::class),
            $this->createMock(SerializerInterface::class),
            $this->createMock(UserRepository::class)
        );
        $this->assertSame('adaubois@norsys.fr', $extension->userDisplayName($user));
    }

    public function testGetDisplayNameWithValidFirstNameAndLastName(): void
    {
        $user = (new User())
            ->setFirstName('Alexandre')
            ->setLastName('Daubois')
            ->setEmail('adaubois@norsys.fr')
            ->setClient(new Client())
            ->setStatus(User::STATUS_ACTIVE);

        $extension = new CurrentUserExtension(
            $this->createMock(\Redis::class),
            $this->createMock(SerializerInterface::class),
            $this->createMock(UserRepository::class)
        );
        $this->assertSame('Alexandre Daubois', $extension->userDisplayName($user));
    }
}
