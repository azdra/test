<?php

namespace App\Tests\Unit\RenaultRobot\Service;

use App\RenaultRobot\Repository\RenaultImportSessionTempRepository;
use App\RenaultRobot\Service\RenaultService;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RenaultServiceTest extends TestCase
{
    private MockObject|EntityManagerInterface $entityManager;
    private MockObject|RenaultImportSessionTempRepository $renaultImportSessionTempRepository;
    private MockObject|ClientRepository $clientRepository;
    private MockObject|RenaultService $renaultService;
    private LoggerInterface $logger;
    private MockObject|TranslatorInterface $translator;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->renaultImportSessionTempRepository = $this->createMock(RenaultImportSessionTempRepository::class);
        $this->clientRepository = $this->createMock(ClientRepository::class);
        $this->renaultService = $this->createMock(RenaultService::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
    }

    private function getRenaultService(): RenaultService
    {
        return new RenaultService('', '', $this->entityManager, $this->renaultImportSessionTempRepository, $this->clientRepository, $this->logger, $this->translator);
    }

    public function getBasicLine(): array
    {
        return [
            1 => [
                1 => 'PROCHAIN',
                2 => 'Edmond',
                3 => 'edmond.prochain@renault.com',
                4 => 'Intern',
                5 => 'Coptère',
                6 => 'Elie',
                7 => 'AC00001',
                8 => 'elie.coptere@renault.com',
                9 => null,
                10 => 'Commercial VN Secteur_FR',
                11 => 'F7_R_FR',
                12 => 'LE MANS',
                13 => 'A00001152-RENAULT RETAIL GROUP',
                14 => 'LE MANS',
                15 => 'CF Chatellerault, CC Chatellerault',
                16 => 'CVC-1811 S2 Les clés de la vente +',
                17 => 'R_FR_00099520',
                18 => ( new \DateTimeImmutable() )->modify('+1 days')->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify('+1 days')->format('d/m/Y'),
                20 => '08:30',
                21 => '12:30',
                22 => 'CLASSE CCE VIRTUELLE TEST - CF TEST',
                23 => '01/07/2022',
                24 => 20,
                25 => 'R_FR - Lundi à Vendredi 08h30 - 12h30',
                26 => 'R_123456',
                27 => 'CF Rennes',
                28 => 'Registered',
                29 => 'Open - Normal',
                30 => 'R_FR',
                31 => '2500000000001',
                32 => 'A970001',
            ],
        ];
    }

    public function linesNoConsideration(): \Generator
    {
        yield 'Email_personne and IPN_personne and Code_Bir_Personne are empty' => [
                            [
                                7 => '', 8 => '', 31 => '',
                            ], 1, ];
        yield 'Number of days > 5' => [
                            [
                                18 => ( new \DateTimeImmutable() )->modify('+1 days')->format('d/m/Y'),
                                19 => ( new \DateTimeImmutable() )->modify('+7 days')->format('d/m/Y'),
                            ], 1, ];
        yield 'The end date is greater than the start date' => [
                            [
                                18 => ( new \DateTimeImmutable() )->modify('+2 days')->format('d/m/Y'),
                                19 => ( new \DateTimeImmutable() )->modify('+1 days')->format('d/m/Y'),
                            ], 2, ];
        yield 'Invalid duration' => [
                            [
                                18 => ( new \DateTimeImmutable() )->modify('+1 days')->format('d/m/Y'),
                                19 => ( new \DateTimeImmutable() )->modify('+1 days')->format('d/m/Y'),
                                20 => '10:30',
                                21 => '08:30',
                            ], 1, ];
        /*yield 'The date of session is not in the range dates' => [
                            [
                                18 => ( new \DateTimeImmutable() )->modify('+11 days')->format('d/m/Y'),
                                19 => ( new \DateTimeImmutable() )->modify('+11 days')->format('d/m/Y'),
                                20 => '10:30',
                                21 => '08:30',
                            ], 2, ];*/
        /*yield 'The session title not begin by "CVT" or "CVC"' => [
                            [
                                16 => 'JJJ-1234 - Session invalide',
                            ], 1, ];*/
        /*yield 'The field Email_person has a length of more than 60 characters' => [
                            [
                                8 => '123456789123456789123456789123456789123456789123456789123456789@renault.com',
                            ], 0, ];*/
        yield 'Email_personne and Nom_stagiaire are empty and IPN_personne and Code_Bir_Personne are not empty' => [
                            [
                                5 => '', 8 => '',
                            ], 1, ];
        /*yield 'Email_Formateur is not include in domain @renault.com' => [
                            [
                                3 => 'daisy.rable@otherdomain.com',
                            ], 1, ];*/
        yield 'Email_Fomateur and Nom_Formateur are empty' => [
                            [
                                1 => '', 3 => '',
                            ], 1, ];
    }

    /** @dataProvider linesNoConsideration */
    public function testLinesNoConsideration(array $keyModification, int $countErrors): void
    {
        $arrayLinesData = $this->getBasicLine();
        foreach ($keyModification as $key => $value) {
            $arrayLinesData[1][$key] = $value;
        }

        $dateMin = ( new \DateTime() )->setTime(00, 01);
        $dateMax = ( new \DateTime() )->modify('+10 days')->setTime(23, 59);

        $resultPrepareForPersist = $this->getRenaultService()->prepareDataForPersist($arrayLinesData, $dateMin, $dateMax);

        foreach ($resultPrepareForPersist['errorSessions'] as $k => $v) {
            $this->assertEquals($countErrors, count($v['errors']));
        }
    }

    public function linesConsideration(): \Generator
    {
        yield 'Basic generate' => [
                [],
                [
                    'nbrSessions' => 1, 'nbrAnimators' => 1, 'nbrTrainees' => 1, 'nbrDays' => 1,
                ],
        ];
        yield 'Multi days' => [
            [
                18 => ( new \DateTimeImmutable() )->modify('+1 days')->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify('+3 days')->format('d/m/Y'),
            ],
            [
                'nbrSessions' => 1, 'nbrAnimators' => 1, 'nbrTrainees' => 1, 'nbrDays' => 3,
            ],
        ];
    }

    /** @dataProvider linesConsideration */
    public function testLinesConsideration(array $keyModification, array $results): void
    {
        $arrayLinesData = $this->getBasicLine();
        foreach ($keyModification as $key => $value) {
            $arrayLinesData[1][$key] = $value;
        }
        $dateMin = ( new \DateTime() )->setTime(00, 01);
        $dateMax = ( new \DateTime() )->modify('+10 days')->setTime(23, 59);

        $resultPrepareForPersist = $this->getRenaultService()->prepareDataForPersist($arrayLinesData, $dateMin, $dateMax);

        $this->assertEquals($results['nbrSessions'], count($resultPrepareForPersist['arrSessions']));
        foreach ($resultPrepareForPersist['arrSessions'] as $session) {
            $this->assertNotEmpty($session['animator']);
            $this->assertEquals($results['nbrTrainees'], count($session['trainees']));
            $this->assertEquals($results['nbrDays'], $session['nbrDays']);
        }
    }

    // 1 session (CVC/CVT) future in scope | 1 animator | 1 trainee
    /*public function getLinesSession1(): array
    {
        return [
            1 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "TOUCHARD",
                6 => "MATTEO",
                7 => "AC09013",
                8 => "matteo.touchard@renault.com",
                9 => null,
                10 => "Commercial VN Secteur_FR",
                11 => "F7_R_FR",
                12 => "LE MANS",
                13 => "A00001152-RENAULT RETAIL GROUP",
                14 => "LE MANS",
                15 => "CF Chatellerault, CC Chatellerault",
                16 => "CVC-1811 S2 Les clés de la vente +",
                17 => "R_FR_00099520",
                18 => ( new \DateTimeImmutable() )->modify( '+1 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+1 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "12:30",
                22 => "CLASSE CCE VIRTUELLE RENNES - CF Rennes",
                23 => "01/07/2022",
                24 => 20,
                25 => "R_FR - Lundi à Vendredi 08h30 - 12h30",
                26 => "R_FRP112749202207011039",
                27 => "CF Rennes",
                28 => "Registered",
                29 => "Open - Normal",
                30 => "R_FR",
                31 => "2500000160924",
                32 => "A974158",
            ]
        ];
    }*/

    // 4 sessions (CVC/CVT) future in scope | 1 animator | 1 trainee
    /*public function getLinesSession2(): array
    {
        return [
            1 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "TOUCHARD",
                6 => "MATTEO",
                7 => "AC09013",
                8 => "matteo.touchard@renault.com",
                9 => null,
                10 => "Commercial VN Secteur_FR",
                11 => "F7_R_FR",
                12 => "LE MANS",
                13 => "A00001152-RENAULT RETAIL GROUP",
                14 => "LE MANS",
                15 => "CF Chatellerault, CC Chatellerault",
                16 => "CVC-1811 S2 Les clés de la vente +",
                17 => "R_FR_00099520",
                18 => ( new \DateTimeImmutable() )->modify( '+3 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "12:30",
                22 => "CLASSE CCE VIRTUELLE RENNES - CF Rennes",
                23 => "01/07/2022",
                24 => 20,
                25 => "R_FR - Lundi à Vendredi 08h30 - 12h30",
                26 => "R_FRP112749202207011039",
                27 => "CF Rennes",
                28 => "Registered",
                29 => "Open - Normal",
                30 => "R_FR",
                31 => "2500000160924",
                32 => "A974158",
            ]
        ];
    }*/

    // 2 sessions (CVC/CVT) futures in scope | 1 animator | 1 trainee
    /*public function getLinesSession3(): array
    {
        return [
            1 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "TOMASI",
                6 => "Theo",
                7 => "D156603",
                8 => "Theo.Tomasi@losangeautos.fr",
                9 => null,
                10 => "Chef unité récep méca_FR",
                11 => "S2_R_FR,S6_R_FR",
                12 => "SCEAUX",
                13 => "A00001336-COLIN AUTOMOBILES SAS",
                14 => "SCEAUX",
                15 => "CF Paris, CC Paris",
                16 => "CVC-0450 J1 Les différents systèmes multimédia, les services connectés et les ADAS",
                17 => "R_FR_00099001",
                18 => ( new \DateTimeImmutable() )->modify( '+2 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+2 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "12:30",
                22 => "CLASSE CCE VIRTUELLE PARIS - CF Paris",
                23 => "01/03/2022",
                24 => 4,
                25 => "R_FR - Mardi 08h30 - 17h00",
                26 => "R_FRP110098202203011757",
                27 => "CF Paris",
                28 => "Cancelled",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000107383",
                32 => "A974158",
            ],
            2 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "VICENTELLI",
                6 => "Elisa",
                7 => "D132808",
                8 => "elisa.vicentelli@rousseau-automobile.fr",
                9 => null,
                10 => "Commercial VN Dacia_FR",
                11 => "FO_R_FR,F4_R_FR,F6_R_FR,G6_R_FR",
                12 => "ARGENTEUIL ROUSSEAU",
                13 => "A00001286-ROUSSEAU ARGENTEUIL",
                14 => "ARGENTEUIL",
                15 => "CF Paris, CC Paris",
                16 => "CVC-0450 J1 Les différents systèmes multimédia, les services connectés et les ADAS",
                17 => "R_FR_00099001",
                18 => ( new \DateTimeImmutable() )->modify( '+3 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+3 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "12:30",
                22 => "CLASSE CCE VIRTUELLE PARIS - CF Paris",
                23 => "01/03/2022",
                24 => 4,
                25 => "R_FR - Mardi 08h30 - 17h00",
                26 => "R_FRP110098202203011757",
                27 => "CF Paris",
                28 => "Cancelled",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000082437",
                32 => "A974158",
            ]
        ];
    }*/

    // 2 sessions (CVC/CVT) future in scope | 1 animator | 3 trainees
    /*public function getLinesSession4(): array
    {
        return [
            1 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "VANDEVILLE",
                6 => "GAETAN",
                7 => "D212053",
                8 => "gaetan.vandeville@ggpauto.fr",
                9 => null,
                10 => "Commercial VO Marchand_FR",
                11 => "B7_R_FR",
                12 => "VALENCIENNES",
                13 => "A00000256-HAINAUT SERVICES AUTOMOBILES",
                14 => "VALENCIENNES",
                15 => "CF Lille, CC Lille",
                16 => "CVC-2807 Le consumérisme pour les collaborateurs Commerciaux",
                17 => "R_FR_00100686",
                18 => ( new \DateTimeImmutable() )->modify( '+4 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+5 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "12:30",
                22 => "CLASSE CCE VIRTUELLE - DFRH",
                23 => "23/02/2022",
                24 => 8,
                25 => "R_FR - Mardi Mercredi 08h30 - 12h30",
                26 => "R_FRP111585202202231016",
                27 => "DFRH",
                28 => "Registered",
                29 => "Open - Normal",
                30 => "R_FR",
                31 => "2500000161274",
                32 => "A974158",
            ],
            2 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "VIEIRA DA SILVA",
                6 => "Handy",
                7 => "250-2500000164711",
                8 => "handy.vieira-da-silva@renault.com",
                9 => null,
                10 => "Commercial VN Secteur_FR",
                11 => "F7_R_FR",
                12 => "LA DEFENSE",
                13 => "A00001293-RENAULT RETAIL GROUP",
                14 => "NANTERRE",
                15 => "CF Paris, CC Paris",
                16 => "CVC-2807 Le consumérisme pour les collaborateurs Commerciaux",
                17 => "R_FR_00100686",
                18 => ( new \DateTimeImmutable() )->modify( '+6 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "12:30",
                22 => "CLASSE CCE VIRTUELLE - DFRH",
                23 => "23/02/2022",
                24 => 8,
                25 => "R_FR - Mardi Mercredi 08h30 - 12h30",
                26 => "R_FRP111585202202231016",
                27 => "DFRH",
                28 => "Cancelled",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000164711",
                32 => "A974158"
            ],
            3 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "VOIRAND",
                6 => "Etienne",
                7 => "D209495",
                8 => "etienne.voirand@ggpauto.fr",
                9 => null,
                10 => "Commercial VO Magasin_FR",
                11 => "G4_R_FR",
                12 => "LILLE FACHES",
                13 => "A00000252-NOUVEAUX GARAGES LILLOIS",
                14 => "FACHES THUMESNIL",
                15 => "CF Lille, CC Lille",
                16 => "CVC-2807 Le consumérisme pour les collaborateurs Commerciaux",
                17 => "R_FR_00100686",
                18 => ( new \DateTimeImmutable() )->modify( '+6 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "12:30",
                22 => "CLASSE CCE VIRTUELLE - DFRH",
                23 => "23/02/2022",
                24 => 8,
                25 => "R_FR - Mardi Mercredi 08h30 - 12h30",
                26 => "R_FRP111585202202231016",
                27 => "DFRH",
                28 => "Registered",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000161476",
                32 => "A974158"
            ]
        ];
    }*/

    // 1 session (CVC/CVT) future in scope | 1 animator | 2 trainees
    /*public function getLinesSession5(): array
    {
        return [
            1 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "VICENTELLI",
                6 => "Elisa",
                7 => "D132808",
                8 => "elisa.vicentelli@rousseau-automobile.fr",
                9 => null,
                10 => "Commercial VN Dacia_FR",
                11 => "FO_R_FR,F4_R_FR,F6_R_FR,G6_R_FR",
                12 => "ARGENTEUIL ROUSSEAU",
                13 => "A00001286-ROUSSEAU ARGENTEUIL",
                14 => "ARGENTEUIL",
                15 => "CF Paris, CC Paris",
                16 => "CPC-0450 J2 Les différents systèmes multimédia, les services connectés et les ADAS",
                17 => "R_FR_00098961",
                18 => ( new \DateTimeImmutable() )->modify( '+4 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+4 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "17:00",
                22 => "AGORA (LE PLESSIS ROBINSON) - CF Paris",
                23 => "23/02/2022",
                24 => 8.5,
                25 => "R_FR - Mardi 08h30 - 17h00",
                26 => "R_FRP110098202202231526",
                27 => "CF Paris",
                28 => "Cancelled",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000082437",
                32 => "A974158",
            ],
            2 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "VICENTELLI",
                6 => "Elisa",
                7 => "D132808",
                8 => "elisa.vicentelli@rousseau-automobile.fr",
                9 => null,
                10 => "Commercial VN Dacia_FR",
                11 => "FO_R_FR,F4_R_FR,F6_R_FR,G6_R_FR",
                12 => "ARGENTEUIL ROUSSEAU",
                13 => "A00001286-ROUSSEAU ARGENTEUIL",
                14 => "ARGENTEUIL",
                15 => "CF Paris, CC Paris",
                16 => "CPC-0450 J2 Les différents systèmes multimédia, les services connectés et les ADAS",
                17 => "R_FR_00098961",
                18 => ( new \DateTimeImmutable() )->modify( '+4 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+4 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "17:00",
                22 => "AGORA (LE PLESSIS ROBINSON) - CF Paris",
                23 => "10/05/2022",
                24 => 8.5,
                25 => "R_FR - Mardi 08h30 - 17h00",
                26 => "R_FRP110098202202231526",
                27 => "CF Paris",
                28 => "Delivered",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000082437",
                32 => "A974158",
            ]
        ];
    }*/

    // 1 session (CVC/CVT) future in scope | 1 animator | 5 trainees
    /*public function getLinesSession6(): array
    {
        return [
            1 => [
                1 => "KUSIAK",
                2 => "Alexandre",
                3 => "alexandre.kusiak@renault.com",
                4 => null,
                5 => "ABADE",
                6 => "CHRISTOPHE",
                7 => "D137944",
                8 => "gestion.bonnaudautomobiles@gmail.com",
                9 => null,
                10 => "Technicien agent_FR",
                11 => "SD_R_FR,S6_R_FR",
                12 => "BONNAUD OLIVIER",
                13 => "A00015194-SARL SIMON BONNAUD",
                14 => "LIBOURNE",
                15 => "CF Agen, CC Albi",
                16 => "CVT-0247 - Sécurité et environnement VE",
                17 => "R_FR_ILTTEC111677",
                18 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "14:00",
                21 => "17:30",
                22 => "CLASSE TECH VIRTUELLE - DFRH",
                23 => "07/03/2022",
                24 => 3.5,
                25 => "R_FR - Mercredi 14h00 - 17h30",
                26 => "R_FRA186720070320221160",
                27 => "DFRH",
                28 => "Registered",
                29 => "Open - Normal",
                30 => "R_FR",
                31 => "2500000083900",
                32 => "A030249",
            ],
            2 => [
                1 => "KUSIAK",
                2 => "Alexandre",
                3 => "alexandre.kusiak@renault.com",
                4 => null,
                5 => "ABBA",
                6 => "YOUNES",
                7 => "D146442",
                8 => "younes.abba.viclecomte02@reseau.renault.fr",
                9 => null,
                10 => "Technicien agent_FR",
                11 => "SD_R_FR,S6_R_FR",
                12 => "BASTIDE SERGE",
                13 => "A00021121-GARAGE SERGE BASTIDE",
                14 => "VIC LE COMTE",
                15 => "CF Lyon, CC Bourg en Bresse",
                16 => "CVT-0247 - Sécurité et environnement VE",
                17 => "R_FR_ILTTEC111677",
                18 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "14:00",
                21 => "17:30",
                22 => "CLASSE TECH VIRTUELLE - DFRH",
                23 => "07/03/2022",
                24 => 3.5,
                25 => "R_FR - Lundi 14h00 - 17h30",
                26 => "R_FRA186720070320221160",
                27 => "DFRH",
                28 => "Registered",
                29 => "Open - Normal",
                30 => "R_FR",
                31 => "2500000071345",
                32 => "A030249"
            ],
            3 => [
                1 => "KUSIAK",
                2 => "Alexandre",
                3 => "alexandre.kusiak@renault.com",
                4 => null,
                5 => "ABBOU",
                6 => "CYRIL",
                7 => "D034259",
                8 => "cyril.abbou@gmail.com",
                9 => null,
                10 => "Technicien agent_FR",
                11 => "SD_R_FR",
                12 => "ABBOU JACOB",
                13 => "A00030046-GRAND GARAGE PELLEPORT",
                14 => "PARIS",
                15 => "CF Paris, CC Paris",
                16 => "CVT-0247 - Sécurité et environnement VE",
                17 => "R_FR_ILTTEC111677",
                18 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "14:00",
                21 => "17:30",
                22 => "CLASSE TECH VIRTUELLE - DFRH",
                23 => "07/03/2022",
                24 => 3.5,
                25 => "R_FR - Mercredi 08h30 - 12h00",
                26 => "R_FRA186720070320221160",
                27 => "DFRH",
                28 => "Cancelled with charge",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000000926",
                32 => "A030249"
            ],
            4 => [
                1 => "KUSIAK",
                2 => "Alexandre",
                3 => "alexandre.kusiak@renault.com",
                4 => null,
                5 => "ADNOT",
                6 => "PHILIPPE",
                7 => "D078383",
                8 => "adnot.philippe06@gmail.com",
                9 => null,
                10 => "Technicien agent_FR",
                11 => "SD_R_FR",
                12 => "VIDUSIN DANIEL",
                13 => "A00023669-GARAGE COTE BLEUE CARRY",
                14 => "CARRY LE ROUET",
                15 => "CF Marseille, CC Montpellier",
                16 => "CVT-0247 - Sécurité et environnement VE",
                17 => "R_FR_ILTTEC111677",
                18 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "14:00",
                21 => "17:30",
                22 => "CLASSE TECH VIRTUELLE - DFRH",
                23 => "07/03/2022",
                24 => 3.5,
                25 => "R_FR - Mercredi 08h30 - 12h00",
                26 => "R_FRA186720070320221160",
                27 => "DFRH",
                28 => "Registered",
                29 => "Open - Normal",
                30 => "R_FR",
                31 => "2500000030062",
                32 => "A030249"
            ],
            5 => [
                1 => "KUSIAK",
                2 => "Alexandre",
                3 => "alexandre.kusiak@renault.com",
                4 => null,
                5 => "ADVIELLE",
                6 => "ROMAIN",
                7 => "D225724",
                8 => "romain.advielle.izel02@reseau.renault.fr",
                9 => null,
                10 => "Mécanicien_FR",
                11 => "S6_R_FR",
                12 => "ADVIELLE DENIS",
                13 => "A00018035-SARL ADVIELLE DENIS",
                14 => "IZEL LES HAMEAUX",
                15 => "CF Lille, CC Lille",
                16 => "CVT-0247 - Sécurité et environnement VE",
                17 => "R_FR_ILTTEC111677",
                18 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "14:00",
                21 => "17:30",
                22 => "CLASSE TECH VIRTUELLE - DFRH",
                23 => "07/03/2022",
                24 => 3.5,
                25 => "R_FR - Lundi 14h00 - 17h30",
                26 => "R_FRA186720070320221160",
                27 => "DFRH",
                28 => "Registered",
                29 => "Open - Normal",
                30 => "R_FR",
                31 => "2500000125687",
                32 => "A030249"
            ]
        ];
    }*/

    /*public function getSourceLines(): array
    {
        return [
            1 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "TOMASI",
                6 => "Theo",
                7 => "D156603",
                8 => "Theo.Tomasi@losangeautos.fr",
                9 => null,
                10 => "Chef unité récep méca_FR",
                11 => "S2_R_FR,S6_R_FR",
                12 => "SCEAUX",
                13 => "A00001336-COLIN AUTOMOBILES SAS",
                14 => "SCEAUX",
                15 => "CF Paris, CC Paris",
                16 => "CVC-0450 J1 Les différents systèmes multimédia, les services connectés et les ADAS",
                17 => "R_FR_00099001",
                18 => ( new \DateTimeImmutable() )->modify( '+2 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+2 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "12:30",
                22 => "CLASSE CCE VIRTUELLE PARIS - CF Paris",
                23 => "01/03/2022",
                24 => 4,
                25 => "R_FR - Mardi 08h30 - 17h00",
                26 => "R_FRP110098202203011757",
                27 => "CF Paris",
                28 => "Cancelled",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000107383",
                32 => "A974158",
              ],
            2 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "TOUCHARD",
                6 => "MATTEO",
                7 => "AC09013",
                8 => "matteo.touchard@renault.com",
                9 => null,
                10 => "Commercial VN Secteur_FR",
                11 => "F7_R_FR",
                12 => "LE MANS",
                13 => "A00001152-RENAULT RETAIL GROUP",
                14 => "LE MANS",
                15 => "CF Chatellerault, CC Chatellerault",
                16 => "CVC-1811 S2 Les clés de la vente +",
                17 => "R_FR_00099520",
                18 => ( new \DateTimeImmutable() )->modify( '+3 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "12:30",
                22 => "CLASSE CCE VIRTUELLE RENNES - CF Rennes",
                23 => "01/07/2022",
                24 => 20,
                25 => "R_FR - Lundi à Vendredi 08h30 - 12h30",
                26 => "R_FRP112749202207011039",
                27 => "CF Rennes",
                28 => "Registered",
                29 => "Open - Normal",
                30 => "R_FR",
                31 => "2500000160924",
                32 => "A974158",
            ],
            3 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "VANDEVILLE",
                6 => "GAETAN",
                7 => "D212053",
                8 => "gaetan.vandeville@ggpauto.fr",
                9 => null,
                10 => "Commercial VO Marchand_FR",
                11 => "B7_R_FR",
                12 => "VALENCIENNES",
                13 => "A00000256-HAINAUT SERVICES AUTOMOBILES",
                14 => "VALENCIENNES",
                15 => "CF Lille, CC Lille",
                16 => "CVC-2807 Le consumérisme pour les collaborateurs Commerciaux",
                17 => "R_FR_00100686",
                18 => ( new \DateTimeImmutable() )->modify( '+4 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+5 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "12:30",
                22 => "CLASSE CCE VIRTUELLE - DFRH",
                23 => "23/02/2022",
                24 => 8,
                25 => "R_FR - Mardi Mercredi 08h30 - 12h30",
                26 => "R_FRP111585202202231016",
                27 => "DFRH",
                28 => "Registered",
                29 => "Open - Normal",
                30 => "R_FR",
                31 => "2500000161274",
                32 => "A974158",
            ],
            4 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "VICENTELLI",
                6 => "Elisa",
                7 => "D132808",
                8 => "elisa.vicentelli@rousseau-automobile.fr",
                9 => null,
                10 => "Commercial VN Dacia_FR",
                11 => "FO_R_FR,F4_R_FR,F6_R_FR,G6_R_FR",
                12 => "ARGENTEUIL ROUSSEAU",
                13 => "A00001286-ROUSSEAU ARGENTEUIL",
                14 => "ARGENTEUIL",
                15 => "CF Paris, CC Paris",
                16 => "CPC-0450 J2 Les différents systèmes multimédia, les services connectés et les ADAS",
                17 => "R_FR_00098961",
                18 => ( new \DateTimeImmutable() )->modify( '+4 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+4 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "17:00",
                22 => "AGORA (LE PLESSIS ROBINSON) - CF Paris",
                23 => "23/02/2022",
                24 => 8.5,
                25 => "R_FR - Mardi 08h30 - 17h00",
                26 => "R_FRP110098202202231526",
                27 => "CF Paris",
                28 => "Cancelled",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000082437",
                32 => "A974158",
            ],
            5 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "VICENTELLI",
                6 => "Elisa",
                7 => "D132808",
                8 => "elisa.vicentelli@rousseau-automobile.fr",
                9 => null,
                10 => "Commercial VN Dacia_FR",
                11 => "FO_R_FR,F4_R_FR,F6_R_FR,G6_R_FR",
                12 => "ARGENTEUIL ROUSSEAU",
                13 => "A00001286-ROUSSEAU ARGENTEUIL",
                14 => "ARGENTEUIL",
                15 => "CF Paris, CC Paris",
                16 => "CPC-0450 J2 Les différents systèmes multimédia, les services connectés et les ADAS",
                17 => "R_FR_00098961",
                18 => ( new \DateTimeImmutable() )->modify( '+4 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+4 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "17:00",
                22 => "AGORA (LE PLESSIS ROBINSON) - CF Paris",
                23 => "10/05/2022",
                24 => 8.5,
                25 => "R_FR - Mardi 08h30 - 17h00",
                26 => "R_FRP110098202202231526",
                27 => "CF Paris",
                28 => "Delivered",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000082437",
                32 => "A974158",
            ],
            6 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "VICENTELLI",
                6 => "Elisa",
                7 => "D132808",
                8 => "elisa.vicentelli@rousseau-automobile.fr",
                9 => null,
                10 => "Commercial VN Dacia_FR",
                11 => "FO_R_FR,F4_R_FR,F6_R_FR,G6_R_FR",
                12 => "ARGENTEUIL ROUSSEAU",
                13 => "A00001286-ROUSSEAU ARGENTEUIL",
                14 => "ARGENTEUIL",
                15 => "CF Paris, CC Paris",
                16 => "CVC-0450 J1 Les différents systèmes multimédia, les services connectés et les ADAS",
                17 => "R_FR_00099001",
                18 => ( new \DateTimeImmutable() )->modify( '+5 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+5 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "12:30",
                22 => "CLASSE CCE VIRTUELLE PARIS - CF Paris",
                23 => "01/03/2022",
                24 => 4,
                25 => "R_FR - Mardi 08h30 - 17h00",
                26 => "R_FRP110098202203011757",
                27 => "CF Paris",
                28 => "Cancelled",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000082437",
                32 => "A974158",
            ],
            7 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "VIEIRA DA SILVA",
                6 => "Handy",
                7 => "250-2500000164711",
                8 => "handy.vieira-da-silva@renault.com",
                9 => null,
                10 => "Commercial VN Secteur_FR",
                11 => "F7_R_FR",
                12 => "LA DEFENSE",
                13 => "A00001293-RENAULT RETAIL GROUP",
                14 => "NANTERRE",
                15 => "CF Paris, CC Paris",
                16 => "CVC-2807 Le consumérisme pour les collaborateurs Commerciaux",
                17 => "R_FR_00100686",
                18 => ( new \DateTimeImmutable() )->modify( '+6 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "12:30",
                22 => "CLASSE CCE VIRTUELLE - DFRH",
                23 => "23/02/2022",
                24 => 8,
                25 => "R_FR - Mardi Mercredi 08h30 - 12h30",
                26 => "R_FRP111585202202231016",
                27 => "DFRH",
                28 => "Cancelled",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000164711",
                32 => "A974158"
            ],
            8 => [
                1 => "JOUBERT",
                2 => "Béatrice",
                3 => "beatrice.joubert@renault.com",
                4 => "Intern",
                5 => "VOIRAND",
                6 => "Etienne",
                7 => "D209495",
                8 => "etienne.voirand@ggpauto.fr",
                9 => null,
                10 => "Commercial VO Magasin_FR",
                11 => "G4_R_FR",
                12 => "LILLE FACHES",
                13 => "A00000252-NOUVEAUX GARAGES LILLOIS",
                14 => "FACHES THUMESNIL",
                15 => "CF Lille, CC Lille",
                16 => "CVC-2807 Le consumérisme pour les collaborateurs Commerciaux",
                17 => "R_FR_00100686",
                18 => ( new \DateTimeImmutable() )->modify( '+6 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "08:30",
                21 => "12:30",
                22 => "CLASSE CCE VIRTUELLE - DFRH",
                23 => "23/02/2022",
                24 => 8,
                25 => "R_FR - Mardi Mercredi 08h30 - 12h30",
                26 => "R_FRP111585202202231016",
                27 => "DFRH",
                28 => "Registered",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000161476",
                32 => "A974158"
            ],
            9 => [
                1 => "KUSIAK",
                2 => "Alexandre",
                3 => "alexandre.kusiak@renault.com",
                4 => null,
                5 => "ABADE",
                6 => "CHRISTOPHE",
                7 => "D137944",
                8 => "gestion.bonnaudautomobiles@gmail.com",
                9 => null,
                10 => "Technicien agent_FR",
                11 => "SD_R_FR,S6_R_FR",
                12 => "BONNAUD OLIVIER",
                13 => "A00015194-SARL SIMON BONNAUD",
                14 => "LIBOURNE",
                15 => "CF Agen, CC Albi",
                16 => "CVT-0247 - Sécurité et environnement VE",
                17 => "R_FR_ILTTEC111677",
                18 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "14:00",
                21 => "17:30",
                22 => "CLASSE TECH VIRTUELLE - DFRH",
                23 => "07/03/2022",
                24 => 3.5,
                25 => "R_FR - Mercredi 14h00 - 17h30",
                26 => "R_FRA186720070320221160",
                27 => "DFRH",
                28 => "Registered",
                29 => "Open - Normal",
                30 => "R_FR",
                31 => "2500000083900",
                32 => "A030249",
            ],
            10 => [
                1 => "KUSIAK",
                2 => "Alexandre",
                3 => "alexandre.kusiak@renault.com",
                4 => null,
                5 => "ABBA",
                6 => "YOUNES",
                7 => "D146442",
                8 => "younes.abba.viclecomte02@reseau.renault.fr",
                9 => null,
                10 => "Technicien agent_FR",
                11 => "SD_R_FR,S6_R_FR",
                12 => "BASTIDE SERGE",
                13 => "A00021121-GARAGE SERGE BASTIDE",
                14 => "VIC LE COMTE",
                15 => "CF Lyon, CC Bourg en Bresse",
                16 => "CVT-0247 - Sécurité et environnement VE",
                17 => "R_FR_ILTTEC111677",
                18 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "14:00",
                21 => "17:30",
                22 => "CLASSE TECH VIRTUELLE - DFRH",
                23 => "07/03/2022",
                24 => 3.5,
                25 => "R_FR - Lundi 14h00 - 17h30",
                26 => "R_FRA186720070320221160",
                27 => "DFRH",
                28 => "Registered",
                29 => "Open - Normal",
                30 => "R_FR",
                31 => "2500000071345",
                32 => "A030249"
            ],
            11 => [
                1 => "KUSIAK",
                2 => "Alexandre",
                3 => "alexandre.kusiak@renault.com",
                4 => null,
                5 => "ABBOU",
                6 => "CYRIL",
                7 => "D034259",
                8 => "cyril.abbou@gmail.com",
                9 => null,
                10 => "Technicien agent_FR",
                11 => "SD_R_FR",
                12 => "ABBOU JACOB",
                13 => "A00030046-GRAND GARAGE PELLEPORT",
                14 => "PARIS",
                15 => "CF Paris, CC Paris",
                16 => "CVT-0247 - Sécurité et environnement VE",
                17 => "R_FR_ILTTEC111677",
                18 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "14:00",
                21 => "17:30",
                22 => "CLASSE TECH VIRTUELLE - DFRH",
                23 => "07/03/2022",
                24 => 3.5,
                25 => "R_FR - Mercredi 08h30 - 12h00",
                26 => "R_FRA186720070320221160",
                27 => "DFRH",
                28 => "Cancelled with charge",
                29 => "Delivered - Normal",
                30 => "R_FR",
                31 => "2500000000926",
                32 => "A030249"
            ],
            12 => [
                1 => "KUSIAK",
                2 => "Alexandre",
                3 => "alexandre.kusiak@renault.com",
                4 => null,
                5 => "ADNOT",
                6 => "PHILIPPE",
                7 => "D078383",
                8 => "adnot.philippe06@gmail.com",
                9 => null,
                10 => "Technicien agent_FR",
                11 => "SD_R_FR",
                12 => "VIDUSIN DANIEL",
                13 => "A00023669-GARAGE COTE BLEUE CARRY",
                14 => "CARRY LE ROUET",
                15 => "CF Marseille, CC Montpellier",
                16 => "CVT-0247 - Sécurité et environnement VE",
                17 => "R_FR_ILTTEC111677",
                18 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "14:00",
                21 => "17:30",
                22 => "CLASSE TECH VIRTUELLE - DFRH",
                23 => "07/03/2022",
                24 => 3.5,
                25 => "R_FR - Mercredi 08h30 - 12h00",
                26 => "R_FRA186720070320221160",
                27 => "DFRH",
                28 => "Registered",
                29 => "Open - Normal",
                30 => "R_FR",
                31 => "2500000030062",
                32 => "A030249"
            ],
            13 => [
                1 => "KUSIAK",
                2 => "Alexandre",
                3 => "alexandre.kusiak@renault.com",
                4 => null,
                5 => "ADVIELLE",
                6 => "ROMAIN",
                7 => "D225724",
                8 => "romain.advielle.izel02@reseau.renault.fr",
                9 => null,
                10 => "Mécanicien_FR",
                11 => "S6_R_FR",
                12 => "ADVIELLE DENIS",
                13 => "A00018035-SARL ADVIELLE DENIS",
                14 => "IZEL LES HAMEAUX",
                15 => "CF Lille, CC Lille",
                16 => "CVT-0247 - Sécurité et environnement VE",
                17 => "R_FR_ILTTEC111677",
                18 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                19 => ( new \DateTimeImmutable() )->modify( '+7 days' )->format('d/m/Y'),
                20 => "14:00",
                21 => "17:30",
                22 => "CLASSE TECH VIRTUELLE - DFRH",
                23 => "07/03/2022",
                24 => 3.5,
                25 => "R_FR - Lundi 14h00 - 17h30",
                26 => "R_FRA186720070320221160",
                27 => "DFRH",
                28 => "Registered",
                29 => "Open - Normal",
                30 => "R_FR",
                31 => "2500000125687",
                32 => "A030249"
            ]
        ];
    }*/

    /*public function testOneDayCorrectSession(): void
    {
        $arrayLinesData = $this->getLinesSession1();
        $dateMin = ( new \DateTime() )->setTime( 00, 01 );
        $dateMax = ( new \DateTime() )->modify( '+10 days' )->setTime( 23, 59 );

        $resultPrepareForPersist = $this->getRenaultService()->prepareDataForPersist($arrayLinesData, $dateMin, $dateMax);

        $this->assertEquals(1, count($resultPrepareForPersist['errorSessions']));

    }*/
}
