<?php

namespace App\Tests\Unit\RenaultRobot\Service;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\Convocation;
use App\Entity\ProviderSession;
use App\RenaultRobot\Entity\RenaultImportSessionTemp;
use App\RenaultRobot\Repository\RenaultImportSessionTempErrorsRepository;
use App\RenaultRobot\Repository\RenaultImportSessionTempRepository;
use App\RenaultRobot\Service\RenaultMeetingImportService;
use App\Repository\ClientRepository;
use App\Repository\ConvocationRepository;
use App\Repository\EvaluationRepository;
use App\Repository\ProviderSessionRepository;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\ActivityLogger;
use App\Service\AdobeConnectService;
use App\Service\AdobeConnectWebinarService;
use App\Service\ProviderSessionService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RenaultMeetingImportServiceTest extends TestCase
{
    private const MY_REF_TEST_RENAULT = 'test_ref_renault';
    private const MY_REF_TEST_LIVE_SESSION = 'test_ref_live_session';

    private string $renaultSavefilesDirectory = 'test';
    private MockObject|EntityManagerInterface $entityManager;
    private MockObject|ConvocationRepository $convocationRepository;
    private MockObject|EvaluationRepository $evaluationRepository;
    private MockObject|RenaultImportSessionTempRepository $renaultImportSessionTempRepository;
    private MockObject|RenaultImportSessionTempErrorsRepository $renaultImportSessionTempErrorsRepository;
    private MockObject|ClientRepository $clientRepository;
    private MockObject|ProviderSessionRepository $providerSessionRepository;
    private MockObject|SubjectRepository $subjectRepository;
    private MockObject|ActivityLogger $activityLogger;
    private MockObject|ProviderSessionService $providerSessionService;
    private MockObject|LoggerInterface $logger;
    private MockObject|TranslatorInterface $translator;
    private MockObject|AdobeConnectService $adobeConnectService;
    private MockObject|AdobeConnectWebinarService $adobeConnectWebinarService;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->convocationRepository = $this->createMock(ConvocationRepository::class);
        $this->evaluationRepository = $this->createMock(EvaluationRepository::class);
        $this->renaultImportSessionTempRepository = $this->createMock(RenaultImportSessionTempRepository::class);
        $this->renaultImportSessionTempErrorsRepository = $this->createMock(RenaultImportSessionTempErrorsRepository::class);
        $this->clientRepository = $this->createMock(ClientRepository::class);
        $this->providerSessionRepository = $this->createMock(ProviderSessionRepository::class);
        $this->subjectRepository = $this->createMock(SubjectRepository::class);
        $this->activityLogger = $this->createMock(ActivityLogger::class);
        $this->providerSessionService = $this->createMock(ProviderSessionService::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->adobeConnectService = $this->createMock(AdobeConnectService::class);
        $this->adobeConnectWebinarService = $this->createMock(AdobeConnectWebinarService::class);
    }

    private function getRenaultMeetingImportService(): RenaultMeetingImportService
    {
        return new RenaultMeetingImportService(
            $this->renaultSavefilesDirectory,
            $this->entityManager,
            $this->convocationRepository,
            $this->evaluationRepository,
            $this->renaultImportSessionTempRepository,
            $this->renaultImportSessionTempErrorsRepository,
            $this->clientRepository,
            $this->providerSessionRepository,
            $this->subjectRepository,
            $this->activityLogger,
            $this->providerSessionService,
            $this->logger,
            $this->translator,
            $this->adobeConnectService,
            $this->adobeConnectWebinarService
        );
    }

    public function createClient(): Client
    {
        return (new Client())
            ->setId(418)
            ->setName('Client Renault');
    }

    public function createConfigurationHolder(): AbstractProviderConfigurationHolder
    {
        /** @var AbstractProviderConfigurationHolder $configurationHolder */
        $configurationHolder = (new AdobeConnectConfigurationHolder())
            ->setName('Connecteur AC Renault')
            ->setClient($this->createClient());

        return $configurationHolder;
    }

    public function createConvocation(): Convocation
    {
        return $this->createPartialMock(Convocation::class, ['getId']);
    }

    public function createCurrentRow(): RenaultImportSessionTemp
    {
        $currentRow = new RenaultImportSessionTemp();
        $dateStart = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 0);
        $dateEnd = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 30);

        $currentRow
            ->setId(123)
            ->setName('TESTCCURLT_CVC-0450 J1 Les différents systèmes multimédia, les services connectés et les ADAS')
            ->setReferenceLiveSession(self::MY_REF_TEST_RENAULT)
            ->setReferenceRenault(self::MY_REF_TEST_RENAULT)
            ->setDateStart($dateStart)
            ->setDateEnd($dateEnd)
            ->setNumDay(4)
            ->setNumRow(12)
            ->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_OPEN);

        return $currentRow;
    }

    /*public function testIfSessionExistAndNotCancelledOrNotFinishedAndCurrentRowWithUnknowStatus(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow
            ->setStatusSession('Status - inconnu')
            ->setReferenceLiveSession(self::MY_REF_TEST_LIVE_SESSION);

        $meeting = (new AdobeConnectSCO($client->getConfigurationHolders()->first()))
            ->setName($currentRow->getName())
            ->setDateStart( (\DateTime::createFromImmutable($currentRow->getDateStart())) )
            ->setDateEnd( (\DateTime::createFromImmutable($currentRow->getDateEnd())) )
            ->setDescription($currentRow->getReferenceRenault())
            ->setDescription2($currentRow->getNumDay())
            ->setRef(self::MY_REF_TEST_LIVE_SESSION)
            ->setStatus(ProviderSession::STATUS_SCHEDULED);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultType')->willReturn([$currentRow]);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->getRenaultMeetingImportService()->importMeetingsFromTemp();
    }*/

    public function testIfSessionNotExistAndNotCancelledOrNotFinishedAndCurrentRowWithStatusBilledOrDelivred(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow
            ->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_BILLED)
            ->setReferenceLiveSession(self::MY_REF_TEST_LIVE_SESSION);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultType')->willReturn([$currentRow]);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn(null);

        $this->getRenaultMeetingImportService()->importMeetingsFromTemp();
    }

    public function testIfSessionExistAndNotCancelledOrNotFinishedAndCurrentRowWithStatusBilledOrDelivred(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow
            ->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_BILLED)
            ->setReferenceLiveSession(self::MY_REF_TEST_LIVE_SESSION);

        $meeting = (new AdobeConnectSCO($client->getConfigurationHolders()->first()))
            ->setName($currentRow->getName())
            ->setDateStart((\DateTime::createFromImmutable($currentRow->getDateStart())))
            ->setDateEnd((\DateTime::createFromImmutable($currentRow->getDateEnd())))
            ->setDescription($currentRow->getReferenceRenault())
            ->setDescription2($currentRow->getNumDay())
            ->setRef(self::MY_REF_TEST_LIVE_SESSION)
            ->setStatus(ProviderSession::STATUS_SCHEDULED);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultType')->willReturn([$currentRow]);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->getRenaultMeetingImportService()->importMeetingsFromTemp();
    }

    public function testCurrentRowIsCancelingAndMeetingNotExists(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow
            ->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_CANCELLED)
            ->setReferenceLiveSession(self::MY_REF_TEST_LIVE_SESSION);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultType')->willReturn([$currentRow]);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn(null);

        $this->getRenaultMeetingImportService()->importMeetingsFromTemp();
    }

    public function testCurrentRowIsCancelingAndMeetingExists(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow
            ->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_CANCELLED)
            ->setReferenceLiveSession(self::MY_REF_TEST_LIVE_SESSION);

        $meeting = (new AdobeConnectSCO($client->getConfigurationHolders()->first()))
            ->setName($currentRow->getName())
            ->setDateStart((\DateTime::createFromImmutable($currentRow->getDateStart())))
            ->setDateEnd((\DateTime::createFromImmutable($currentRow->getDateEnd())))
            ->setDescription($currentRow->getReferenceRenault())
            ->setDescription2($currentRow->getNumDay())
            ->setRef(self::MY_REF_TEST_LIVE_SESSION)
            ->setStatus(ProviderSession::STATUS_SCHEDULED);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultType')->willReturn([$currentRow]);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->getRenaultMeetingImportService()->importMeetingsFromTemp();
    }

    public function testCurrentRowIsSchedulingAndIfReferenceExistsWithChangeDatetime(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow
            ->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_OPEN)
            ->setReferenceLiveSession(self::MY_REF_TEST_LIVE_SESSION);

        $meeting = (new AdobeConnectSCO($client->getConfigurationHolders()->first()))
            ->setName($currentRow->getName())
            ->setDateStart((\DateTime::createFromImmutable($currentRow->getDateStart()))->modify('+2 days'))
            ->setDateEnd((\DateTime::createFromImmutable($currentRow->getDateEnd()))->modify('+2 days'))
            ->setDescription($currentRow->getReferenceRenault())
            ->setDescription2($currentRow->getNumDay())
            ->setRef(self::MY_REF_TEST_LIVE_SESSION)
            ->setStatus(ProviderSession::STATUS_SCHEDULED);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultType')->willReturn([$currentRow]);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->clientRepository->expects($this->once())
            ->method('findOneBy')->with(['name' => 'Renault'])->willReturn($client);

        $this->getRenaultMeetingImportService()->importMeetingsFromTemp();
    }

    public function testCurrentRowIsSchedulingAndIfReferenceExistsWithSameDateAndChangeTime(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow
            ->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_OPEN)
            ->setReferenceLiveSession(self::MY_REF_TEST_LIVE_SESSION);

        $meeting = (new AdobeConnectSCO($client->getConfigurationHolders()->first()))
            ->setName($currentRow->getName())
            ->setDateStart((\DateTime::createFromImmutable($currentRow->getDateStart()))->modify('+2 hours'))
            ->setDateEnd((\DateTime::createFromImmutable($currentRow->getDateEnd()))->modify('+2 hours'))
            ->setDescription($currentRow->getReferenceRenault())
            ->setDescription2($currentRow->getNumDay())
            ->setRef(self::MY_REF_TEST_LIVE_SESSION)
            ->setStatus(ProviderSession::STATUS_SCHEDULED);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultType')->willReturn([$currentRow]);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->getRenaultMeetingImportService()->importMeetingsFromTemp();
    }

    public function testCurrentRowIsSchedulingAndIfReferenceExistsWithSameDatetime(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow
            ->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_OPEN)
            ->setReferenceLiveSession(self::MY_REF_TEST_LIVE_SESSION);

        $meeting = (new AdobeConnectSCO($client->getConfigurationHolders()->first()))
            ->setName($currentRow->getName())
            ->setDateStart((\DateTime::createFromImmutable($currentRow->getDateStart())))
            ->setDateEnd((\DateTime::createFromImmutable($currentRow->getDateEnd())))
            ->setDescription($currentRow->getReferenceRenault())
            ->setDescription2($currentRow->getNumDay())
            ->setRef(self::MY_REF_TEST_LIVE_SESSION)
            ->setStatus(ProviderSession::STATUS_SCHEDULED);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultType')->willReturn([$currentRow]);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('postCreateRow')->with($currentRow);

        $this->getRenaultMeetingImportService()->importMeetingsFromTemp();
    }

    public function testCurrentRowIsSchedulingAndIsSessionIsCancelled(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_OPEN);

        $meeting = (new AdobeConnectSCO($client->getConfigurationHolders()->first()))
            ->setName($currentRow->getName())
            ->setDateStart((\DateTime::createFromImmutable($currentRow->getDateStart()))->modify('+4 days'))
            ->setDateEnd((\DateTime::createFromImmutable($currentRow->getDateEnd()))->modify('+4 days'))
            ->setDescription($currentRow->getReferenceRenault())
            ->setDescription2($currentRow->getNumDay())
            ->setRef(self::MY_REF_TEST_LIVE_SESSION)
            ->setStatus(ProviderSession::STATUS_CANCELED);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultType')->willReturn([$currentRow]);

        $this->clientRepository->expects($this->once())
            ->method('findOneBy')->willReturn($client);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->willReturn($meeting);

        $this->getRenaultMeetingImportService()->importMeetingsFromTemp();
    }

    public function testCreateSessionWithReferenceLiveSession(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = new RenaultImportSessionTemp();
        $dateStart = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 0);
        $dateEnd = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 30);

        $currentRow
            ->setId(123)
            ->setName('TESTCCURLT_CVC-0450 J1 Les différents systèmes multimédia, les services connectés et les ADAS')
            ->setReferenceLiveSession(self::MY_REF_TEST_RENAULT)
            ->setReferenceRenault(self::MY_REF_TEST_RENAULT)
            ->setDateStart($dateStart)
            ->setDateEnd($dateEnd)
            ->setNumDay(4)
            ->setNumRow(12)
            ->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_OPEN);

        $meeting = (new AdobeConnectSCO($client->getConfigurationHolders()->first()))
            ->setName($currentRow->getName())
            ->setDateStart((\DateTime::createFromImmutable($currentRow->getDateStart()))->modify('+4 days'))
            ->setDateEnd((\DateTime::createFromImmutable($currentRow->getDateEnd()))->modify('+4 days'))
            ->setDescription($currentRow->getReferenceRenault())
            ->setDescription2($currentRow->getNumDay())
            ->setRef(self::MY_REF_TEST_LIVE_SESSION);

        $this->renaultImportSessionTempRepository
            ->expects($this->once())
            ->method('findAllByResultType')
            ->willReturn([$currentRow]);

        $this->clientRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->willReturn($client);

        $this->providerSessionService->expects($this->once())
            ->method('createSessionFromArray')
            ->willReturn($meeting);

        $this->providerSessionService->expects($this->once())
            ->method('applyRemindersAndConvocationPreference')
            ->willReturn($meeting);

        $this->getRenaultMeetingImportService()->importMeetingsFromTemp();
    }

    public function testCreateSessionWithoutReferenceLiveSessionAndExistSessionWithAnotherDate(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = new RenaultImportSessionTemp();
        $dateStart = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 0);
        $dateEnd = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 30);

        $currentRow
            ->setId(123)
            ->setName('TESTCCURLT_CVC-0450 J1 Les différents systèmes multimédia, les services connectés et les ADAS')
            ->setReferenceRenault(self::MY_REF_TEST_RENAULT)
            ->setDateStart($dateStart)
            ->setDateEnd($dateEnd)
            ->setNumDay(4)
            ->setNumRow(12)
            ->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_OPEN);

        $meeting = (new AdobeConnectSCO($client->getConfigurationHolders()->first()))
            ->setName($currentRow->getName())
            ->setDateStart((\DateTime::createFromImmutable($currentRow->getDateStart()))->modify('+4 days'))
            ->setDateEnd((\DateTime::createFromImmutable($currentRow->getDateEnd()))->modify('+4 days'))
            ->setDescription($currentRow->getReferenceRenault())
            ->setDescription2($currentRow->getNumDay())
            ->setRef(self::MY_REF_TEST_LIVE_SESSION);

        $this->renaultImportSessionTempRepository
            ->expects($this->once())
            ->method('findAllByResultType')
            ->willReturn([$currentRow]);

        $this->providerSessionRepository
            ->expects($this->once())
            ->method('findBy')
            ->with(['description' => $currentRow->getReferenceRenault()])
            ->willReturn([$meeting]);

        $this->clientRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->willReturn($client);

        $this->renaultImportSessionTempRepository
            ->expects($this->once())
            ->method('postCreateInfos')
            ->with($currentRow, $meeting);

        $this->providerSessionService->expects($this->once())
            ->method('createSessionFromArray')
            ->willReturn($meeting);

        $this->providerSessionService->expects($this->once())
            ->method('applyRemindersAndConvocationPreference')
            ->willReturn($meeting);

        $this->getRenaultMeetingImportService()->importMeetingsFromTemp();
    }

    public function testCreateSessionWithoutReferenceLiveSessionAndExistSessionWithSameDate(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = new RenaultImportSessionTemp();
        $dateStart = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 0);
        $dateEnd = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 30);

        $currentRow
            ->setId(123)
            ->setName('TESTCCURLT_CVC-0450 J1 Les différents systèmes multimédia, les services connectés et les ADAS')
            ->setReferenceRenault(self::MY_REF_TEST_RENAULT)
            ->setDateStart($dateStart)
            ->setDateEnd($dateEnd)
            ->setNumDay(1)
            ->setNumRow(12)
            ->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_OPEN);

        $meeting = (new AdobeConnectSCO($client->getConfigurationHolders()->first()))
            ->setName($currentRow->getName())
            ->setDateStart(\DateTime::createFromImmutable($currentRow->getDateStart()))
            ->setDateEnd(\DateTime::createFromImmutable($currentRow->getDateEnd()))
            ->setDescription($currentRow->getReferenceRenault())
            ->setDescription2($currentRow->getNumDay())
            ->setRef(self::MY_REF_TEST_LIVE_SESSION);

        $this->renaultImportSessionTempRepository
            ->expects($this->once())
            ->method('findAllByResultType')
            ->willReturn([$currentRow]);

        $this->providerSessionRepository
            ->expects($this->once())
            ->method('findBy')
            ->with(['description' => $currentRow->getReferenceRenault()])
            ->willReturn([$meeting]);

        $this->getRenaultMeetingImportService()->importMeetingsFromTemp();
    }

    public function testCreateSessionWithoutReferenceLiveSessionAndNotExistSessionThenCreateSession(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = new RenaultImportSessionTemp();
        $dateStart = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 0);
        $dateEnd = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 30);

        $currentRow
            ->setId(123)
            ->setName('TESTCCURLT_CVC-0450 J1 Les différents systèmes multimédia, les services connectés et les ADAS')
            ->setReferenceRenault(self::MY_REF_TEST_RENAULT)
            ->setDateStart($dateStart)
            ->setDateEnd($dateEnd)
            ->setNumRow(1)
            ->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_OPEN);

        $meeting = (new AdobeConnectSCO($client->getConfigurationHolders()->first()))
            ->setName($currentRow->getName())
            ->setDateStart(\DateTime::createFromImmutable($currentRow->getDateStart()))
            ->setDateEnd(\DateTime::createFromImmutable($currentRow->getDateEnd()))
            ->setRef(self::MY_REF_TEST_LIVE_SESSION);

        $this->renaultImportSessionTempRepository
            ->expects($this->once())
            ->method('findAllByResultType')
            ->willReturn([$currentRow]);

        $this->providerSessionRepository
            ->expects($this->once())
            ->method('findBy')
            ->with(['description' => self::MY_REF_TEST_RENAULT])
            ->willReturn([]);

        $this->clientRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->willReturn($client);

        $this->renaultImportSessionTempRepository
            ->expects($this->once())
            ->method('postCreateInfos')
            ->with($currentRow, $meeting);

        $this->providerSessionService->expects($this->once())
            ->method('createSessionFromArray')
            ->willReturn($meeting);

        $this->providerSessionService->expects($this->once())
            ->method('applyRemindersAndConvocationPreference')
            ->willReturn($meeting);

        $this->getRenaultMeetingImportService()->importMeetingsFromTemp();
    }
}
