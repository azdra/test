<?php

namespace App\Tests\Unit\RenaultRobot\Service;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSessionAccessToken;
use App\Entity\User;
use App\RenaultRobot\Entity\RenaultImportSessionTemp;
use App\RenaultRobot\Repository\RenaultImportSessionTempErrorsRepository;
use App\RenaultRobot\Repository\RenaultImportSessionTempRepository;
use App\RenaultRobot\Service\RenaultTraineeImportService;
use App\Repository\ClientRepository;
use App\Repository\ConvocationRepository;
use App\Repository\EvaluationRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Security\UserSecurityService;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\ActivityLogger;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\ProviderSessionService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RenaultTraineeImportServiceTest extends TestCase
{
    private const MY_REF_TEST_RENAULT = 'test_ref_renault';
    private const MY_REF_TEST_LIVE_SESSION = 'test_ref_live_session';

    protected string $renaultSavefilesDirectory = 'test';
    private MockObject|EntityManagerInterface $entityManager;
    private MockObject|ConvocationRepository $convocationRepository;
    private MockObject|EvaluationRepository $evaluationRepository;
    private MockObject|RenaultImportSessionTempRepository $renaultImportSessionTempRepository;
    private MockObject|RenaultImportSessionTempErrorsRepository $renaultImportSessionTempErrorsRepository;
    private MockObject|ClientRepository $clientRepository;
    private MockObject|ProviderSessionRepository $providerSessionRepository;
    private MockObject|ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository;
    private MockObject|SubjectRepository $subjectRepository;
    private MockObject|UserRepository $userRepository;
    private MockObject|ActivityLogger $activityLogger;
    private MockObject|ProviderSessionService $providerSessionService;
    //private MockObject|UserSecurityService $userSecurityService;
    private MockObject|ParticipantSessionSubscriber $participantSessionSubscriber;
    private MockObject|LoggerInterface $logger;
    private MockObject|TranslatorInterface $translator;
    private MockObject|RouterInterface $router;
    protected string $endPointDomain = 'http://localhost';

    protected function createUserSecurityService(?EventDispatcherInterface $dispatcher = null, ?UserRepository $userRepository = null, ?EntityManagerInterface $entityManager = null): UserSecurityService
    {
        $dispatcher ??= $this->createMock(EventDispatcherInterface::class);
        $userRepository ??= $this->createMock(UserRepository::class);
        $entityManager ??= $this->createMock(EntityManagerInterface::class);

        return new UserSecurityService($dispatcher, $userRepository, $entityManager);
    }

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->convocationRepository = $this->createMock(ConvocationRepository::class);
        $this->evaluationRepository = $this->createMock(EvaluationRepository::class);
        $this->renaultImportSessionTempRepository = $this->createMock(RenaultImportSessionTempRepository::class);
        $this->renaultImportSessionTempErrorsRepository = $this->createMock(RenaultImportSessionTempErrorsRepository::class);
        $this->clientRepository = $this->createMock(ClientRepository::class);
        $this->providerSessionRepository = $this->createMock(ProviderSessionRepository::class);
        $this->providerParticipantSessionRoleRepository = $this->createMock(ProviderParticipantSessionRoleRepository::class);
        $this->subjectRepository = $this->createMock(SubjectRepository::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->activityLogger = $this->createMock(ActivityLogger::class);
        $this->providerSessionService = $this->createMock(ProviderSessionService::class);
        $this->userSecurityService = $this->createUserSecurityService();
        $this->participantSessionSubscriber = $this->createMock(ParticipantSessionSubscriber::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->router = $this->createMock(RouterInterface::class);
    }

    private function getRenaultTraineeImportService(): RenaultTraineeImportService
    {
        return new RenaultTraineeImportService(
            $this->renaultSavefilesDirectory,
            $this->entityManager,
            $this->convocationRepository,
            $this->evaluationRepository,
            $this->renaultImportSessionTempRepository,
            $this->renaultImportSessionTempErrorsRepository,
            $this->clientRepository,
            $this->providerSessionRepository,
            $this->providerParticipantSessionRoleRepository,
            $this->subjectRepository,
            $this->userRepository,
            $this->activityLogger,
            $this->providerSessionService,
            $this->userSecurityService,
            $this->participantSessionSubscriber,
            $this->logger,
            $this->translator,
            $this->router,
            $this->endPointDomain
        );
    }

    private function createClient(): Client
    {
        return (new Client())
            ->setId(418)
            ->setName('Renault');
    }

    private function createConfigurationHolder(): AbstractProviderConfigurationHolder
    {
        $configurationHolder = (new AdobeConnectConfigurationHolder())
            ->setName('Connecteur AC Renault')
            ->setClient($this->createClient());

        return $configurationHolder;
    }

    private function createCurrentRow(): RenaultImportSessionTemp
    {
        $currentRow = new RenaultImportSessionTemp();
        $dateStart = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 0);
        $dateEnd = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 30);

        $currentRow
            ->setId(142)
            ->setName('TESTCCURLT_CVC-0450 J1 Les différents systèmes multimédia, les services connectés et les ADAS')
            ->setReferenceLiveSession(self::MY_REF_TEST_RENAULT)
            ->setReferenceRenault(self::MY_REF_TEST_RENAULT)
            ->setResultType(2)
            ->setResultTrainee('')
            ->setRole(RenaultImportSessionTemp::ROLE_TRAINEE)
            ->setDateStart($dateStart)
            ->setDateEnd($dateEnd)
            ->setNumDay(4)
            ->setNumRow(12)
            ->setAnimatorEmail('animator1.atest@renault.com')
            ->setAnimatorName('Atest')
            ->setAnimatorForname('animator1')
            ->setTraineeEmail('trainee1.ttest@renault.com')
            ->setTraineeName('Ttest')
            ->setTraineeForname('Trainee1')
            ->setStatusTrainee(RenaultImportSessionTemp::TRAINEE_STATUS_REGISTERED)
            ->setStatusSession(RenaultImportSessionTemp::MEETING_STATUS_OPEN);

        return $currentRow;
    }

    private function createMeeting(): AdobeConnectSCO
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $dateStart = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 0);
        $dateEnd = ( new \DateTimeImmutable() )->modify('+2 days')->setTime(4, 30);

        $meeting = (new AdobeConnectSCO($client->getConfigurationHolders()->first()))
            ->setId('123')
            ->setName('Meeting name')
            ->setDateStart((\DateTime::createFromImmutable($dateStart))->modify('+4 days'))
            ->setDateEnd((\DateTime::createFromImmutable($dateEnd))->modify('+4 days'))
            ->setDescription(self::MY_REF_TEST_RENAULT)
            ->setDescription2(1)
            ->setRef(self::MY_REF_TEST_LIVE_SESSION);

        return $meeting;
    }

    public function testAnimatorNotEmptyCurrentRowRefLSOrMeetingIsWaitingSchedulingAndEqualsEmails(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow
            ->setRole(RenaultImportSessionTemp::ROLE_ANIMATOR)
            ->setResultAnimator('');

        $meeting = $this->createMeeting();

        $accessToken = new ProviderSessionAccessToken();
        $accessToken->setToken('123456');

        $animator = new User();
        $animator
            ->setId('123')
            ->setClient($client)
            ->setFirstName($currentRow->getAnimatorForname())
            ->setLastName($currentRow->getAnimatorName())
            ->setClearPassword($this->userSecurityService->randomPassword())
            ->setEmail($currentRow->getAnimatorEmail())
            ->setCompany($client->getName())
            ->setRoles(['ROLE_TRAINEE'])
            ->setPreferredLang('fr');

        $providerParticipant = (new AdobeConnectPrincipal($client->getConfigurationHolders()->first()))
            ->setUser($animator);
        $participant = (new ProviderParticipantSessionRole())
            ->setRole(ProviderParticipantSessionRole::ROLE_ANIMATOR)
            ->setParticipant(
                ($providerParticipant)
            )
            ->setProviderSessionAccessToken($accessToken);
        $meeting->addParticipantRole($participant);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultUser')->willReturn([$currentRow]);

        $this->clientRepository->expects($this->once())
            ->method('findOneBy')->with(['name' => 'Renault'])->willReturn($client);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->userRepository->expects($this->exactly(2))
            ->method('findOneBy')->with(['email' => $currentRow->getAnimatorEmail(), 'client' => $client])->willReturn($animator);

        $this->getRenaultTraineeImportService()->importTraineesFromTemp();
    }

    public function testAnimatorEmptyCurrentRowRefLSOrMeetingIsWaitingSchedulingAndEqualsEmails(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow
            ->setRole(RenaultImportSessionTemp::ROLE_ANIMATOR)
            ->setResultAnimator('');

        $meeting = $this->createMeeting();

        $animator = new User();
        $animator
            ->setId('123')
            ->setClient($client)
            ->setFirstName($currentRow->getAnimatorForname())
            ->setLastName($currentRow->getAnimatorName())
            ->setClearPassword($this->userSecurityService->randomPassword())
            ->setEmail($currentRow->getAnimatorEmail())
            ->setCompany($client->getName())
            ->setRoles(['ROLE_TRAINEE'])
            ->setPreferredLang('fr');

        $accessToken = new ProviderSessionAccessToken();
        $accessToken->setToken('123456');

        $providerParticipant = (new AdobeConnectPrincipal($client->getConfigurationHolders()->first()))
            ->setUser($animator);
        $participant = (new ProviderParticipantSessionRole())
            ->setRole(ProviderParticipantSessionRole::ROLE_ANIMATOR)
            ->setParticipant(
                ($providerParticipant)
            )
            ->setProviderSessionAccessToken($accessToken);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultUser')->willReturn([$currentRow]);

        $this->clientRepository->expects($this->once())
            ->method('findOneBy')->with(['name' => 'Renault'])->willReturn($client);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->userRepository->expects($this->once())
            ->method('findOneBy')->with(['email' => $currentRow->getAnimatorEmail(), 'client' => $client])->willReturn($animator);

        $this->participantSessionSubscriber->expects($this->once())
            ->method('subscribe')->with($meeting, $animator, ProviderParticipantSessionRole::ROLE_ANIMATOR)->willReturn($participant);

        $this->getRenaultTraineeImportService()->importTraineesFromTemp();
    }

    public function testAnimatorEmptyCurrentRowRefLSAndDifferentStatusTraineeRegisteredOrMeetingIsWaitingSchedulingAndEqualsEmails(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow
            ->setRole(RenaultImportSessionTemp::ROLE_ANIMATOR)
            ->setResultAnimator('');

        $meeting = $this->createMeeting();

        $animator = new User();
        $animator
            ->setId('123')
            ->setClient($client)
            ->setFirstName($currentRow->getAnimatorForname())
            ->setLastName($currentRow->getAnimatorName())
            ->setClearPassword($this->userSecurityService->randomPassword())
            ->setEmail($currentRow->getAnimatorEmail())
            ->setCompany($client->getName())
            ->setRoles(['ROLE_TRAINEE'])
            ->setPreferredLang('fr');

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultUser')->willReturn([$currentRow]);

        $this->clientRepository->expects($this->once())
            ->method('findOneBy')->with(['name' => 'Renault'])->willReturn($client);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->userRepository->expects($this->once())
            ->method('findOneBy')->with(['email' => $currentRow->getAnimatorEmail(), 'client' => $client])->willReturn($animator);

        $this->getRenaultTraineeImportService()->importTraineesFromTemp();
    }

    public function testAnimatorNotEmptyCurrentRowRefLSAndDifferentStatusTraineeRegisteredOrMeetingIsWaitingSchedulingAndEqualsEmails(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow
            ->setRole(RenaultImportSessionTemp::ROLE_ANIMATOR)
            ->setResultAnimator('')
            ->setStatusTrainee(RenaultImportSessionTemp::TRAINEE_STATUS_DELIVERED);

        $meeting = $this->createMeeting();

        $animator = new User();
        $animator
            ->setId('123')
            ->setClient($client)
            ->setFirstName($currentRow->getAnimatorForname())
            ->setLastName($currentRow->getAnimatorName())
            ->setClearPassword($this->userSecurityService->randomPassword())
            ->setEmail($currentRow->getAnimatorEmail())
            ->setCompany($client->getName())
            ->setRoles(['ROLE_TRAINEE'])
            ->setPreferredLang('fr');

        $accessToken = new ProviderSessionAccessToken();
        $accessToken->setToken('123456');

        $providerParticipant = (new AdobeConnectPrincipal($client->getConfigurationHolders()->first()))
            ->setUser($animator);
        $participant = (new ProviderParticipantSessionRole())
            ->setRole(ProviderParticipantSessionRole::ROLE_ANIMATOR)
            ->setParticipant(
                ($providerParticipant)
            )
            ->setProviderSessionAccessToken($accessToken);
        $meeting->addParticipantRole($participant);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultUser')->willReturn([$currentRow]);

        $this->clientRepository->expects($this->once())
            ->method('findOneBy')->with(['name' => 'Renault'])->willReturn($client);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->userRepository->expects($this->exactly(2))
            ->method('findOneBy')->with(['email' => $currentRow->getAnimatorEmail(), 'client' => $client])->willReturn($animator);

        $this->getRenaultTraineeImportService()->importTraineesFromTemp();
    }

    public function testEmptyCurrentRowRefLSOrNotMeetingIsWaitingScheduling(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow
            ->setStatusSession('Status - inconnu')
            ->setReferenceLiveSession(self::MY_REF_TEST_LIVE_SESSION);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultUser')->willReturn([$currentRow]);

        $this->clientRepository->expects($this->once())
            ->method('findOneBy')->with(['name' => 'Renault'])->willReturn($client);

        $this->getRenaultTraineeImportService()->importTraineesFromTemp();
    }

    public function testTraineeNotEmptyCurrentRowRefLSOrMeetingIsWaitingSchedulingAndEqualsEmails(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();

        $meeting = $this->createMeeting();

        $trainee = new User();
        $trainee
            ->setId('123')
            ->setClient($client)
            ->setFirstName($currentRow->getTraineeForname())
            ->setLastName($currentRow->getTraineeName())
            ->setClearPassword($this->userSecurityService->randomPassword())
            ->setEmail($currentRow->getTraineeEmail())
            ->setCompany($client->getName())
            ->setRoles(['ROLE_TRAINEE'])
            ->setPreferredLang('fr');

        $accessToken = new ProviderSessionAccessToken();
        $accessToken->setToken('123456');

        $providerParticipant = (new AdobeConnectPrincipal($client->getConfigurationHolders()->first()))
            ->setUser($trainee);
        $participant = (new ProviderParticipantSessionRole())
            ->setRole(ProviderParticipantSessionRole::ROLE_TRAINEE)
            ->setParticipant(
                ($providerParticipant)
            )
            ->setProviderSessionAccessToken($accessToken);
        $meeting->addParticipantRole($participant);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultUser')->willReturn([$currentRow]);

        $this->clientRepository->expects($this->once())
            ->method('findOneBy')->with(['name' => 'Renault'])->willReturn($client);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->userRepository->expects($this->exactly(2))
            ->method('findOneBy')->with(['email' => $currentRow->getTraineeEmail(), 'client' => $client])->willReturn($trainee);

        $this->getRenaultTraineeImportService()->importTraineesFromTemp();
    }

    public function testTraineeEmptyCurrentRowRefLSOrMeetingIsWaitingSchedulingAndEqualsEmails(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();

        $meeting = $this->createMeeting();

        $trainee = new User();
        $trainee
            ->setId('123')
            ->setClient($client)
            ->setFirstName($currentRow->getTraineeForname())
            ->setLastName($currentRow->getTraineeName())
            ->setClearPassword($this->userSecurityService->randomPassword())
            ->setEmail($currentRow->getTraineeEmail())
            ->setCompany($client->getName())
            ->setRoles(['ROLE_TRAINEE'])
            ->setPreferredLang('fr');

        $accessToken = new ProviderSessionAccessToken();
        $accessToken->setToken('123456');

        $providerParticipant = (new AdobeConnectPrincipal($client->getConfigurationHolders()->first()))
            ->setUser($trainee);
        $participant = (new ProviderParticipantSessionRole())
            ->setRole(ProviderParticipantSessionRole::ROLE_TRAINEE)
            ->setParticipant(
                ($providerParticipant)
            )
            ->setProviderSessionAccessToken($accessToken);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultUser')->willReturn([$currentRow]);

        $this->clientRepository->expects($this->once())
            ->method('findOneBy')->with(['name' => 'Renault'])->willReturn($client);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->userRepository->expects($this->once())
            ->method('findOneBy')->with(['email' => $currentRow->getTraineeEmail(), 'client' => $client])->willReturn($trainee);

        $this->participantSessionSubscriber->expects($this->once())
            ->method('subscribe')->with($meeting, $trainee, ProviderParticipantSessionRole::ROLE_TRAINEE)->willReturn($participant);

        $this->getRenaultTraineeImportService()->importTraineesFromTemp();
    }

    public function testTraineeEmptyCurrentRowRefLSAndDifferentStatusTraineeRegisteredOrMeetingIsWaitingSchedulingAndEqualsEmails(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow->setStatusTrainee(RenaultImportSessionTemp::TRAINEE_STATUS_DELIVERED);

        $meeting = $this->createMeeting();

        $trainee = new User();
        $trainee
            ->setId('123')
            ->setClient($client)
            ->setFirstName($currentRow->getTraineeForname())
            ->setLastName($currentRow->getTraineeName())
            ->setClearPassword($this->userSecurityService->randomPassword())
            ->setEmail($currentRow->getTraineeEmail())
            ->setCompany($client->getName())
            ->setRoles(['ROLE_TRAINEE'])
            ->setPreferredLang('fr');

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultUser')->willReturn([$currentRow]);

        $this->clientRepository->expects($this->once())
            ->method('findOneBy')->with(['name' => 'Renault'])->willReturn($client);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->userRepository->expects($this->once())
            ->method('findOneBy')->with(['email' => $currentRow->getTraineeEmail(), 'client' => $client])->willReturn($trainee);

        $this->getRenaultTraineeImportService()->importTraineesFromTemp();
    }

    public function testTraineeNotEmptyCurrentRowRefLSAndDifferentStatusTraineeRegisteredOrMeetingIsWaitingSchedulingAndEqualsEmails(): void
    {
        $client = $this->createConfigurationHolder()->getClient();
        $client->addConfigurationHolder($this->createConfigurationHolder());

        $currentRow = $this->createCurrentRow();
        $currentRow->setStatusTrainee(RenaultImportSessionTemp::TRAINEE_STATUS_DELIVERED);

        $meeting = $this->createMeeting();

        $trainee = new User();
        $trainee
            ->setId('123')
            ->setClient($client)
            ->setFirstName($currentRow->getTraineeForname())
            ->setLastName($currentRow->getTraineeName())
            ->setClearPassword($this->userSecurityService->randomPassword())
            ->setEmail($currentRow->getTraineeEmail())
            ->setCompany($client->getName())
            ->setRoles(['ROLE_TRAINEE'])
            ->setPreferredLang('fr');

        $accessToken = new ProviderSessionAccessToken();
        $accessToken->setToken('123456');

        $providerParticipant = (new AdobeConnectPrincipal($client->getConfigurationHolders()->first()))
            ->setUser($trainee);
        $participant = (new ProviderParticipantSessionRole())
            ->setRole(ProviderParticipantSessionRole::ROLE_TRAINEE)
            ->setParticipant(
                ($providerParticipant)
            )
            ->setProviderSessionAccessToken($accessToken);
        $meeting->addParticipantRole($participant);

        $this->renaultImportSessionTempRepository->expects($this->once())
            ->method('findAllByResultUser')->willReturn([$currentRow]);

        $this->clientRepository->expects($this->once())
            ->method('findOneBy')->with(['name' => 'Renault'])->willReturn($client);

        $this->providerSessionRepository->expects($this->once())
            ->method('findOneBy')->with(['ref' => $currentRow->getReferenceLiveSession()])->willReturn($meeting);

        $this->userRepository->expects($this->exactly(2))
            ->method('findOneBy')->with(['email' => $currentRow->getTraineeEmail(), 'client' => $client])->willReturn($trainee);

        $this->getRenaultTraineeImportService()->importTraineesFromTemp();
    }
}
