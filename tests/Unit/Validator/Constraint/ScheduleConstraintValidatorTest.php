<?php

namespace App\Tests\Unit\Validator\Constraint;

use App\Validator\Constraint\ScheduleConstraint;
use App\Validator\Constraint\ScheduleConstraintValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;

class ScheduleConstraintValidatorTest extends TestCase
{
    public function testBadConstraintSentToValidator(): void
    {
        $validator = new ScheduleConstraintValidator();

        $this->expectException(UnexpectedTypeException::class);
        $validator->validate(new \stdClass(), new Valid());
    }

    public function provideInvalidScheduleArray(): \Generator
    {
        yield ['toto'];
        yield [123];
        yield [new \stdClass()];
        yield [[]];
        yield [['days']];
        yield [['hours']];
        yield [['minutes']];
        yield [['days', 'hours', 'minutes']];
        yield [['days' => null]];
        yield [['hours' => null]];
        yield [['minutes' => null]];
        yield [[
            'days' => 12,
            'hours' => 12,
        ]];
        yield [[
            'hours' => 12,
            'minutes' => 12,
        ]];
        yield [[
            'days' => 12,
            'hours' => null,
            'minutes' => 12,
        ]];
        yield [[
            'days' => 12,
            'hours' => [],
            'minutes' => 12,
        ]];
        yield [[
            'days' => null,
            'hours' => null,
            'minutes' => null,
        ]];
        yield [[
            'days' => new \stdClass(),
            'hours' => 12,
            'minutes' => 12,
        ]];
        yield [[
            'days' => 12,
            'hours' => 12,
            'minutes' => 'toto',
        ]];
    }

    public function provideValidScheduleArray(): \Generator
    {
        yield [null];
        yield [[
            'days' => 12,
            'hours' => 12,
            'minutes' => 12,
        ]];
    }

    /**
     * @dataProvider provideInvalidScheduleArray
     */
    public function testScheduleIsInvalid($schedule): void
    {
        $validator = new ScheduleConstraintValidator();
        $context = $this->createMock(ExecutionContextInterface::class);

        $builder = $this->createMock(ConstraintViolationBuilderInterface::class);

        $context->expects($this->once())
                ->method('buildViolation')
                ->willReturn($builder);

        $validator->initialize($context);
        $validator->validate($schedule, new ScheduleConstraint());
    }

    /**
     * @dataProvider provideValidScheduleArray
     */
    public function testValidSchedule($schedule): void
    {
        $validator = new ScheduleConstraintValidator();
        $context = $this->createMock(ExecutionContextInterface::class);
        $context->expects($this->never())
                ->method('buildViolation');

        $validator->initialize($context);
        $validator->validate($schedule, new ScheduleConstraint());
    }
}
