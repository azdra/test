<?php

namespace App\Tests\Unit\Validator\Constraint;

use App\Validator\Constraint\PasswordSecurityPolicy;
use App\Validator\Constraint\PasswordSecurityPolicyValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;

class PasswordSecurityPolicyValidatorTest extends TestCase
{
    public function testBadConstraintSentToValidator(): void
    {
        $validator = new PasswordSecurityPolicyValidator();

        $this->expectException(UnexpectedTypeException::class);
        $validator->validate(new \stdClass(), new Valid());
    }

    public function provideInvalidPasswords(): \Generator
    {
        yield ['testtest'];
        yield ['TESTTEST'];
        yield ['12345678'];
        yield ['########'];

        // Lacks lowercase
        yield ['TEST#123'];
        // Lacks uppercase
        yield ['test#123'];
        // Lacks number
        yield ['Test####'];
        // Lacks special character
        yield ['Test1234'];

        // Too short
        yield ['Test#12'];
    }

    /**
     * @dataProvider provideInvalidPasswords
     */
    public function testPasswordIsInvalid($password): void
    {
        $validator = new PasswordSecurityPolicyValidator();
        $context = $this->createMock(ExecutionContextInterface::class);

        $builder = $this->createMock(ConstraintViolationBuilderInterface::class);

        $context->expects($this->once())
            ->method('buildViolation')
            ->willReturn($builder);

        $builder->expects($this->once())
            ->method('setParameter')
            ->willReturnSelf();

        $validator->initialize($context);
        $validator->validate($password, new PasswordSecurityPolicy());
    }

    public function testValidPassword(): void
    {
        $validator = new PasswordSecurityPolicyValidator();
        $context = $this->createMock(ExecutionContextInterface::class);
        $context->expects($this->never())
            ->method('buildViolation');

        $validator->initialize($context);
        $validator->validate('Test#1234', new PasswordSecurityPolicy());
    }
}
