<?php

namespace App\Tests\Unit\Validator;

use App\Service\ConvocationService;
use App\Validator\ConstraintShortCode;
use App\Validator\ConstraintShortCodeValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\SyntaxError;

class ConstraintShortCodeValidatorTest extends TestCase
{
    public function testBadConstraintSentToValidator(): void
    {
        $validator = new ConstraintShortCodeValidator(
            $this->createMock(ConvocationService::class),
            $this->createMock(TranslatorInterface::class)
        );

        $this->expectException(UnexpectedTypeException::class);
        $validator->validate(new \stdClass(), new Valid());
    }

    /*public function testScheduleIsInvalid(): void
    {
        $convocationService = $this->createMock(ConvocationService::class);
        $convocationService->expects($this->once())
                           ->method('transformWithFakeData')
                           ->willThrowException(new SyntaxError('message'));

        $validator = new ConstraintShortCodeValidator($convocationService);
        $context = $this->createMock(ExecutionContextInterface::class);

        $builder = $this->createMock(ConstraintViolationBuilderInterface::class);

        $context->expects($this->once())
                ->method('buildViolation')
                ->willReturn($builder);

        $validator->initialize($context);
        $validator->validate('toto', new ConstraintShortCode());
    }*/
}
