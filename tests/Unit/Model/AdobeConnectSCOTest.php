<?php

namespace App\Tests\Unit\Model;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use PHPUnit\Framework\TestCase;

class AdobeConnectSCOTest extends TestCase
{
    public function testPassingInvalidTypeToSCOThrowsException()
    {
        $sco = new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client()));

        $this->expectException(\LogicException::class);
        $sco->setType('Invalid type');
    }

    public function testPassingValidTypeToSCOWorksFine()
    {
        $sco = new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client()));
        $sco->setType(AdobeConnectSCO::TYPE_MEETING);
        $this->assertSame(AdobeConnectSCO::TYPE_MEETING, $sco->getType());
    }
}
