<?php

namespace App\Tests\Unit\Model;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Model\GeneratorSessionTrait;
use PHPUnit\Framework\TestCase;

class GeneratorSessionTraitTest extends TestCase
{
    public function testEncryptAndDecryptDataIsSame(): void
    {
        /** @var GeneratorSessionTrait $apiTrait */
        $generatorTrait = $this->getObjectForTrait(GeneratorSessionTrait::class);

        $appSecret = 'mySecret';
        $providerParticipantEmail = 'plop@gg.com';
        $providerSessionReference = 'AwesomeReference418';
        $role = ProviderParticipantSessionRole::ROLE_ANIMATOR;

        $configurationHolder = new AdobeConnectConfigurationHolder();
        $configurationHolder->setClient(new Client());

        $providerSession = (new AdobeConnectSCO($configurationHolder))->setRef($providerSessionReference);
        $providerParticipant = (new AdobeConnectPrincipal($configurationHolder))
            ->setUser((new User())->setEmail($providerParticipantEmail)->setClient((new Client())));
        $providerParticipantSessionRole = (new ProviderParticipantSessionRole())
            ->setSession($providerSession)
            ->setParticipant($providerParticipant)
            ->setRole($role);

        $generatorTrait->encryptSessionAccessToken($appSecret, $providerParticipantSessionRole);
        $data = $generatorTrait->decryptSessionAccessToken($appSecret, $providerParticipantSessionRole->getProviderSessionAccessToken());

        $this->assertIsArray($data);
        $this->assertCount(4, $data);
        $this->assertSame($providerParticipantEmail, $data[0]);
        $this->assertSame($providerSessionReference, $data[1]);
        $this->assertSame($role, $data[2]);
        $this->assertTrue(in_array($data[2], [
            ProviderParticipantSessionRole::ROLE_ANIMATOR,
            ProviderParticipantSessionRole::ROLE_TRAINEE,
        ]));
        $this->assertEquals(5, strlen($data[3]));
    }
}
