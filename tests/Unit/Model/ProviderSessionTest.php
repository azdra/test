<?php
/**
 * @author:    Emmanuel SMITH <emmanuel.smith@live-session.fr>
 * project:    m3
 * file:    ProviderSessionTest.php
 * Date:    24/09/2021
 * Time:    10:10
 */

namespace App\Tests\Unit\Model;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderSession;
use PHPUnit\Framework\TestCase;

class ProviderSessionTest extends TestCase
{
    private function createProviderSession(int $status, \DateTime $dateStart, \DateTime $dateEnd): ProviderSession
    {
        return (new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client())))
            ->setDateStart($dateStart)
            ->setDateEnd($dateEnd)
            ->setStatus($status);
    }

    private function createProviderSessionForReminder(): ProviderSession
    {
        return (new AdobeConnectSCO((new AdobeConnectConfigurationHolder())->setClient(new Client())))
            ->setDateStart(new \DateTime())
            ->setDateEnd(new \DateTime())
            ->setStatus(ProviderSession::STATUS_SCHEDULED);
    }

    private function createProviderSessionAdvanced($status, $scheduled, $convocationSent): ProviderSession
    {
        list($dateStart, $dateEnd) = $this->getDateFromScheduledString($scheduled);
        $providerSession = $this->createProviderSession($status, \DateTime::createFromImmutable($dateStart), \DateTime::createFromImmutable($dateEnd));
        if ($convocationSent) {
            $providerSession->setNotificationDate(new \DateTimeImmutable());
            $providerSession->setLastConvocationSent(new \DateTimeImmutable());
        }

        return $providerSession;
    }

    public function readableStateTestCase(): \Generator
    {
        yield 'Session is a draft while scheduled' => ['Draft', ProviderSession::STATUS_DRAFT, 'scheduled', false];
        yield 'Session is a draft while scheduled and convocation send' => ['Draft', ProviderSession::STATUS_DRAFT, 'scheduled', true];
        yield 'Session is a draft while running' => ['Draft', ProviderSession::STATUS_DRAFT, 'running', false];
        yield 'Session is a draft while finished' => ['Draft', ProviderSession::STATUS_DRAFT, 'finished', false];

        yield 'Session is a cancelled while scheduled' => ['Cancelled', ProviderSession::STATUS_CANCELED, 'scheduled', false];
        yield 'Session is a cancelled while scheduled and convocation send' => ['Cancelled', ProviderSession::STATUS_CANCELED, 'scheduled', true];
        yield 'Session is a cancelled while running' => ['Cancelled', ProviderSession::STATUS_CANCELED, 'running', false];
        yield 'Session is a cancelled while finished' => ['Cancelled', ProviderSession::STATUS_CANCELED, 'finished', false];

        yield 'Session is scheduled' => ['Scheduled', ProviderSession::STATUS_SCHEDULED, 'scheduled', false];
        yield 'Session is convocation sent' => ['Sent', ProviderSession::STATUS_SCHEDULED, 'scheduled', true];
        yield 'Session is running' => ['Running', ProviderSession::STATUS_SCHEDULED, 'running', false];
        yield 'Session is running even if convocation sent' => ['Running', ProviderSession::STATUS_SCHEDULED, 'running', true];
        yield 'Session is finished' => ['Finished', ProviderSession::STATUS_SCHEDULED, 'finished', false];
        yield 'Session is finished even if convocation sent' => ['Finished', ProviderSession::STATUS_SCHEDULED, 'finished', true];
    }

    private function getDateFromScheduledString(string $scheduled): array
    {
        $now = new \DateTimeImmutable();

        return match ($scheduled) {
            'scheduled' => [$now->add(new \DateInterval('PT4H')), $now->add(new \DateInterval('PT6H'))],
            'running' => [$now->sub(new \DateInterval('PT4H')), $now->add(new \DateInterval('PT6H'))],
            'finished' => [$now->sub(new \DateInterval('PT6H')), $now->sub(new \DateInterval('PT4H'))],
        };
    }

    /**
     * @dataProvider readableStateTestCase
     */
    public function testGetReadableState(string $expected, int $status, string $scheduled, bool $convocationSent): void
    {
        $session = $this->createProviderSessionAdvanced($status, $scheduled, $convocationSent);

        $actual = $session->getReadableState();

        $this->assertEquals($expected, $actual);
    }

    // <editor-folder> Draft
    public function getProviderSessionDraftData(): \Generator
    {
        $dateStart = new \DateTime();

        // Finished
        yield 'Finished < now()' => [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('-2 hours')),
        ];
        yield 'Finished === now()' => [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('-3 hours'),
                $dateStart),
        ];

        // Running
        yield 'Running < now()' => [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('+1 hours')),
        ];
        yield 'Running === now()' => [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, $dateStart,
                (clone $dateStart)->modify('+3 hours')),
        ];

        // Scheduled
        yield 'Scheduled > now()' => [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('+3 hours'),
                (clone $dateStart)->modify('+5 hours')),
        ];
    }

    /** @dataProvider getProviderSessionDraftData */
    public function testProviderSessionIsInDraft(ProviderSession $providerSession): void
    {
        $this->assertSame(ProviderSession::STATUS_DRAFT, $providerSession->getStatus());
        $this->assertFalse($providerSession->isScheduled());
        $this->assertFalse($providerSession->isRunning());
        $this->assertFalse($providerSession->isFinished());
        $this->assertSame('Draft', $providerSession->getReadableState());
    }

    // </editor-folder>

    // <editor-folder> Scheduled
    public function getProviderSessionScheduledData(): \Generator
    {
        $dateStart = new \DateTime();

        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, (clone $dateStart)->modify('+3 hours'),
                (clone $dateStart)->modify('+5 hours')),
        ];
    }

    public function getProviderSessionNotScheduledData(): \Generator
    {
        $dateStart = new \DateTime();

        // Finished
        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('-2 hours')),
        ];
        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, (clone $dateStart)->modify('-3 hours'),
                $dateStart),
        ];

        // Running
        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('+1 hours')),
        ];
        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, $dateStart,
                (clone $dateStart)->modify('+3 hours')),
        ];

        // Finished
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('-2 hours')),
        ];
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('-3 hours'),
                $dateStart),
        ];

        // Running
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('+1 hours')),
        ];
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, $dateStart,
                (clone $dateStart)->modify('+3 hours')),
        ];

        // Scheduled
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('+3 hours'),
                (clone $dateStart)->modify('+5 hours')),
        ];
    }

    /** @dataProvider getProviderSessionScheduledData */
    public function testProviderSessionIsScheduledPass(ProviderSession $providerSession): void
    {
        $this->assertSame(ProviderSession::STATUS_SCHEDULED, $providerSession->getStatus());
        $this->assertTrue($providerSession->isScheduled());
        $this->assertFalse($providerSession->isRunning());
        $this->assertFalse($providerSession->isFinished());
        $this->assertSame('Scheduled', $providerSession->getReadableState());
    }

    /** @dataProvider getProviderSessionNotScheduledData */
    public function testProviderSessionIsNotScheduledPass(ProviderSession $providerSession): void
    {
        $this->assertFalse($providerSession->isScheduled());
        $this->assertNotSame('Scheduled', $providerSession->getReadableState());
    }

    // </editor-folder>

    // <editor-folder> Running
    public function getProviderSessionRunningData(): \Generator
    {
        $dateStart = new \DateTime();

        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('+1 hours')),
        ];
        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, $dateStart,
                (clone $dateStart)->modify('+3 hours')),
        ];
    }

    public function getProviderSessionNotRunningData(): \Generator
    {
        $dateStart = new \DateTime();
        // Finished
        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('-2 hours')),
        ];
        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, (clone $dateStart)->modify('-3 hours'),
                $dateStart),
        ];

        // Scheduled
        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, (clone $dateStart)->modify('+3 hours'),
                (clone $dateStart)->modify('+5 hours')),
        ];

        // Finished
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('-2 hours')),
        ];
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('-3 hours'),
                $dateStart),
        ];

        // Running
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('+1 hours')),
        ];
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, $dateStart,
                (clone $dateStart)->modify('+3 hours')),
        ];

        // Scheduled
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('+3 hours'),
                (clone $dateStart)->modify('+5 hours')),
        ];
    }

    /** @dataProvider getProviderSessionRunningData */
    public function testProviderSessionIsRunningPass(ProviderSession $providerSession): void
    {
        $this->assertSame(ProviderSession::STATUS_SCHEDULED, $providerSession->getStatus());
        $this->assertFalse($providerSession->isScheduled());
        $this->assertTrue($providerSession->isRunning());
        $this->assertFalse($providerSession->isFinished());
        $this->assertSame('Running', $providerSession->getReadableState());
    }

    /** @dataProvider getProviderSessionNotRunningData */
    public function testProviderSessionIsNotRunningPass(ProviderSession $providerSession): void
    {
        $this->assertFalse($providerSession->isRunning());
        $this->assertNotSame('Running', $providerSession->getReadableState());
    }

    // </editor-folder>

    // <editor-folder> Finished
    public function getProviderSessionFinishedData(): \Generator
    {
        $dateStart = new \DateTime();

        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('-2 hours')),
        ];
        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, (clone $dateStart)->modify('-3 hours'),
                $dateStart),
        ];
    }

    public function getProviderSessionNotFinishedData(): \Generator
    {
        $dateStart = new \DateTime();
        // Running
        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('+1 hours')),
        ];
        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, $dateStart,
                (clone $dateStart)->modify('+3 hours')),
        ];

        // Scheduled
        yield [
            $this->createProviderSession(ProviderSession::STATUS_SCHEDULED, (clone $dateStart)->modify('+3 hours'),
                (clone $dateStart)->modify('+5 hours')),
        ];

        // Finished
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('-2 hours')),
        ];
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('-3 hours'),
                $dateStart),
        ];

        // Running
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('-3 hours'),
                (clone $dateStart)->modify('+1 hours')),
        ];
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, $dateStart,
                (clone $dateStart)->modify('+3 hours')),
        ];

        // Scheduled
        yield [
            $this->createProviderSession(ProviderSession::STATUS_DRAFT, (clone $dateStart)->modify('+3 hours'),
                (clone $dateStart)->modify('+5 hours')),
        ];
    }

    /** @dataProvider getProviderSessionFinishedData */
    public function testProviderSessionIsFinishedPass(ProviderSession $providerSession): void
    {
        $this->assertSame(ProviderSession::STATUS_SCHEDULED, $providerSession->getStatus());
        $this->assertFalse($providerSession->isScheduled());
        $this->assertFalse($providerSession->isRunning());
        $this->assertTrue($providerSession->isFinished());
        $this->assertSame('Finished', $providerSession->getReadableState());
    }

    /** @dataProvider getProviderSessionNotFinishedData */
    public function testProviderSessionIsNotFinishedPass(ProviderSession $providerSession): void
    {
        $this->assertFalse($providerSession->isFinished());
        $this->assertNotSame('Finished', $providerSession->getReadableState());
    }

    // </editor-folder>

    // <editor-folder> Next reminder
    public function testNullReturnWithEmptyReminders(): void
    {
        $providerSession = $this->createProviderSessionForReminder();

        $this->assertNull($providerSession->getNextReminder());
    }

    public function testFirstReminderReturnNullWithoutRemoversSent(): void
    {
        $providerSession = $this->createProviderSessionForReminder();

        $this->assertNull($providerSession->getNextReminder());
    }

    public function testFirstReminderReturnFirstReminderWithoutSent(): void
    {
        $reminder1 = new \DateTimeImmutable('2021-08-14 10:00:00');
        $reminder2 = new \DateTimeImmutable('2021-08-14 12:00:00');
        $reminder3 = new \DateTimeImmutable('2021-08-14 14:00:00');
        $providerSession = $this->createProviderSessionForReminder()
                                ->setDateOfSendingReminders([$reminder1, $reminder3, $reminder2]);

        $this->assertInstanceOf(\DateTimeImmutable::class, $providerSession->getNextReminder());
        $this->assertSame($reminder1, $providerSession->getNextReminder());
    }

    public function testNextReminderReturnedWithReminderSent(): void
    {
        $reminder1 = new \DateTimeImmutable('2021-08-14 10:00:00');
        $reminder2 = new \DateTimeImmutable('2022-08-14 12:00:00');
        $reminder3 = new \DateTimeImmutable('+2 hours');
        $providerSession = $this->createProviderSessionForReminder()
            ->setLastReminderSent(new \DateTimeImmutable())
                                ->setDateOfSendingReminders([$reminder1, $reminder3, $reminder2]);

        $this->assertInstanceOf(\DateTimeImmutable::class, $providerSession->getNextReminder());
        $this->assertSame($reminder3, $providerSession->getNextReminder());
    }

    public function testFirstReminderReturnedWithReminderSent(): void
    {
        $providerSession = $this->createProviderSessionForReminder()
                                ->setDateOfSendingReminders([new \DateTimeImmutable('2021-08-14 12:00:00')])
                                ->setLastReminderSent(new \DateTimeImmutable('2021-08-14 12:01:00'));

        $this->assertNull($providerSession->getNextReminder());
    }

    public function testNextReminderReturnedWithReminderAlreadySent(): void
    {
        $reminder1 = new \DateTimeImmutable('2021-08-14 10:00:00');
        $reminder2 = new \DateTimeImmutable('2021-08-14 12:00:00');
        $reminder3 = new \DateTimeImmutable('2021-08-14 13:00:00');
        $providerSession = $this->createProviderSessionForReminder()
                                ->setDateOfSendingReminders([$reminder1, $reminder3, $reminder2])
            ->setLastReminderSent(new \DateTimeImmutable('2021-08-14 10:01:00'));

        $this->assertInstanceOf(\DateTimeImmutable::class, $providerSession->getNextReminder());
        $this->assertSame($reminder2, $providerSession->getNextReminder());
    }

    // </editor-folder>
}
