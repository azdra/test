<?php

namespace App\Tests\Unit\Model;

use App\Exception\InvalidArgumentException;
use App\Exception\ProviderServiceValidationException;
use App\Model\MiddlewareResponse;
use App\Model\ProviderResponse;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class MiddlewareResponseTest extends TestCase
{
    public function testFromSuccessfulProvider()
    {
        $providerResponse = new ProviderResponse();
        $providerResponse->setData('Super data')
            ->setThrown(new InvalidArgumentException()) // This one shouldn't be kept because the response is a success
            ->setSuccess(true);

        $response = MiddlewareResponse::fromProvider($providerResponse);

        $this->assertInstanceOf(MiddlewareResponse::class, $response);
        $this->assertTrue($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
        $this->assertSame('Super data', $response->getData());
    }

    public function testFromFailedProvider()
    {
        $providerResponse = new ProviderResponse();
        $providerResponse->setData('Super data') // This one shouldn't be kept because the response is a failure
            ->setThrown(new InvalidArgumentException())
            ->setSuccess(false);

        $response = MiddlewareResponse::fromProvider($providerResponse);

        $this->assertInstanceOf(MiddlewareResponse::class, $response);
        $this->assertFalse($response->isSuccess());
        $this->assertTrue($response->isFromProvider());
        $this->assertInstanceOf(InvalidArgumentException::class, $response->getException());
    }

    public function testFromProviderServiceValidationException()
    {
        $list = new ConstraintViolationList();
        $list->add($this->createMock(ConstraintViolation::class));

        $exception = new ProviderServiceValidationException($list);
        $response = MiddlewareResponse::fromValidationException($exception);

        $this->assertInstanceOf(MiddlewareResponse::class, $response);
        $this->assertFalse($response->isSuccess());
        $this->assertSame($list, $response->getData());
    }
}
