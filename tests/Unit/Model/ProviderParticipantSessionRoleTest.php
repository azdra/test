<?php
/**
 * @author:    Emmanuel SMITH <emmanuel.smith@live-session.fr>
 * project:    m3
 * file:    ProviderParticipantSessionRoleTest.php
 * Date:    23/09/2021
 * Time:    10:39
 */

namespace App\Tests\Unit\Model;

use App\Entity\ProviderParticipantSessionRole;
use App\Model\ConvocationSendingHistory;
use PHPUnit\Framework\TestCase;

class ProviderParticipantSessionRoleTest extends TestCase
{
    public function testADateWithInvalidTypeFail(): void
    {
        $this->expectException(\LogicException::class);

        (new ProviderParticipantSessionRole())->addADateOfSendingConvocation('yellow');
    }

    public function testAddDateInConvocationDateListWithEmptyListPass(): void
    {
        $providerParticipantRole = (new ProviderParticipantSessionRole())
            ->setDatesForSendingConvocations(null)
            ->addADateOfSendingConvocation(ConvocationSendingHistory::TYPE_CONVOCATION);

        $this->assertCount(1, $providerParticipantRole->getDatesForSendingConvocations());
        $this->assertInstanceOf(ConvocationSendingHistory::class, $providerParticipantRole->getDatesForSendingConvocations()[0]);
    }

    public function testAddDateInConvocationDateListWithExistingListPass(): void
    {
        $date = new \DateTimeImmutable('2021-08-14 12:00:00');
        $convocationSendingHistory = (new ConvocationSendingHistory())
            ->setDate($date)
            ->setType(ConvocationSendingHistory::TYPE_CONVOCATION);

        $providerParticipantRole = (new ProviderParticipantSessionRole())
            ->setDatesForSendingConvocations([$convocationSendingHistory])
            ->addADateOfSendingConvocation(ConvocationSendingHistory::TYPE_REMINDER);

        $this->assertCount(2, $providerParticipantRole->getDatesForSendingConvocations());
        foreach ($providerParticipantRole->getDatesForSendingConvocations() as $datesForSendingConvocation) {
            $this->assertInstanceOf(ConvocationSendingHistory::class, $datesForSendingConvocation);
        }

        /** @var ConvocationSendingHistory $firstConvocationSendingHistory */
        $firstConvocationSendingHistory = $providerParticipantRole->getDatesForSendingConvocations()[0];
        $this->assertSame($date, $firstConvocationSendingHistory->getDate());
        $this->assertSame(ConvocationSendingHistory::TYPE_CONVOCATION, $firstConvocationSendingHistory->getType());
    }

    // </editor-folder>
}
