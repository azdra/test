<?php

namespace App\Tests\Unit\Model;

use App\Model\GeneratorTrait;
use PHPUnit\Framework\TestCase;

class GeneratorTraitTest extends TestCase
{
    public function testRandomHexStringIsValid(): void
    {
        /** @var GeneratorTrait $apiTrait */
        $generatorTrait = $this->getObjectForTrait(GeneratorTrait::class);

        $randomStr = $generatorTrait->randomHexString();

        $this->assertSame(6, strlen($randomStr));
        $this->assertMatchesRegularExpression('/^[a-f0-9]{6}$/i', $randomStr);
    }

    public function testRandomHexStringWithLength(): void
    {
        /** @var GeneratorTrait $apiTrait */
        $generatorTrait = $this->getObjectForTrait(GeneratorTrait::class);
        $length = 9;
        $randomStr = $generatorTrait->randomHexString($length);

        $this->assertSame($length, strlen($randomStr));
        $this->assertMatchesRegularExpression('/^[a-f0-9]{9}$/i', $randomStr);
    }
}
