<?php

namespace App\Tests\Unit\Model;

use App\Model\ApiResponseTrait;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ApiResponseTraitTest extends TestCase
{
    public function createFormMock(array $errors = null): MockObject|FormInterface
    {
        $form = $this->createMock(FormInterface::class);

        if (!is_null($errors)) {
            $form->expects($this->any())
                 ->method('getErrors')
                 ->willReturn($this->createFormErrorIterator(errors: $errors));
        }

        return $form;
    }

    public function createFormErrorIterator(?FormInterface $form = null, array $errors = []): FormErrorIterator
    {
        $formErrors = [];

        foreach ($errors as $error) {
            $formErrors[] = new FormError($error);
        }

        $form ??= $this->createFormMock();

        return new FormErrorIterator($form, $formErrors);
    }

    public function testSuccessfulResponseReturnAValidJsonResponse(): void
    {
        /** @var ApiResponseTrait $apiTrait */
        $apiTrait = $this->getObjectForTrait(ApiResponseTrait::class);

        $response = $apiTrait->apiResponse(['my-data'], Response::HTTP_ACCEPTED);

        $rawData = (array) json_decode($response->getContent());

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertArrayHasKey('code', $rawData);
        $this->assertArrayHasKey('message', $rawData);
        $this->assertSame($rawData['code'], Response::HTTP_ACCEPTED);
        $this->assertSame($rawData['message'], ['my-data']);
    }

    public function testErrorResponseReturnAValidJsonResponse(): void
    {
        /** @var ApiResponseTrait $apiTrait */
        $apiTrait = $this->getObjectForTrait(ApiResponseTrait::class);

        $response = $apiTrait->apiErrorResponse('my-message', Response::HTTP_CONFLICT, ['my-data']);

        $rawData = (array) json_decode($response->getContent());

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertArrayHasKey('code', $rawData);
        $this->assertArrayHasKey('message', $rawData);
        $this->assertSame($rawData['code'], Response::HTTP_CONFLICT);
        $this->assertEquals($rawData['message'], (object) ['text' => 'my-message', 'data' => ['my-data']]);
    }

    public function testRetrieveFormErrorMessageIsValid(): void
    {
        /** @var ApiResponseTrait $apiTrait */
        $apiTrait = $this->getObjectForTrait(ApiResponseTrait::class);

        $errors = ['my-error-1', 'my-error-2', 'my-error-3'];
        $formError = $this->createFormErrorIterator(errors: $errors);

        $this->assertSame($errors, $apiTrait->retrieveFormErrorMessage($formError));
    }

    public function testRetrieveFormErrorIsEmptyWithoutErrors(): void
    {
        /** @var ApiResponseTrait $apiTrait */
        $apiTrait = $this->getObjectForTrait(ApiResponseTrait::class);

        $formError = $this->createFormErrorIterator();

        $this->assertSame([], $apiTrait->retrieveFormErrorMessage($formError));
    }

    public function testFormErrorIsSuccessWithoutError(): void
    {
        /** @var ApiResponseTrait $apiTrait */
        $apiTrait = $this->getObjectForTrait(ApiResponseTrait::class);

        $form = $this->createFormMock();
        $form->expects($this->any())
             ->method('getErrors')
             ->willReturn($this->createFormErrorIterator());

        $result = $apiTrait->formErrorToArray($form);

        $this->assertArrayHasKey('form', $result);
        $this->assertArrayHasKey('fields', $result);
        $this->assertSame($result, [
            'form' => [],
            'fields' => [],
        ]);
    }

    public function testFormErrorIsSuccessWithoutFormError(): void
    {
        /** @var ApiResponseTrait $apiTrait */
        $apiTrait = $this->getObjectForTrait(ApiResponseTrait::class);

        $form = $this->createFormMock([]);
        $result = $apiTrait->formErrorToArray($form);

        $this->assertArrayHasKey('form', $result);
        $this->assertArrayHasKey('fields', $result);
        $this->assertSame($result, [
            'form' => [],
            'fields' => [],
        ]);
    }

    public function testFormErrorIsSuccessWithOnlyFormError(): void
    {
        /** @var ApiResponseTrait $apiTrait */
        $apiTrait = $this->getObjectForTrait(ApiResponseTrait::class);

        $form = $this->createFormMock(['my-errors-1', 'my-error-2']);
        $result = $apiTrait->formErrorToArray($form);

        $this->assertArrayHasKey('form', $result);
        $this->assertArrayHasKey('fields', $result);
        $this->assertSame($result, [
            'form' => ['my-errors-1', 'my-error-2'],
            'fields' => [],
        ]);
    }
}
