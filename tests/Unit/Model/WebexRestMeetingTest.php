<?php

namespace App\Tests\Unit\Model;

use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Cisco\WebexRestParticipant;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class WebexRestMeetingTest extends TestCase
{
    public function testWhite(): void
    {
        $this->assertSame('whitetest', 'whitetest');
    }

    private function createWebexRestMeeting(array $data = []): WebexRestMeeting
    {
        // @var WebexRestMeeting $webexRestMeeting
        $webexRestMeeting = ( new WebexRestMeeting((new WebexRestConfigurationHolder())->setClient(new Client())) )
        ->setName('My session');

        if (!empty($data['licence'])) {
            $webexRestMeeting->setLicence($data['licence']);
        }

        if (!empty($data['participants'])) {
            foreach ($data['participants'] as $participant) {
                $webexRestParticipant = ( new WebexRestParticipant($webexRestMeeting->getAbstractProviderConfigurationHolder()) )
                ->setUser(( new User() )->setEmail($participant['participant_email']));

                $webexRestMeeting->addParticipantRole(
                    (new ProviderParticipantSessionRole())
                    ->setSession($webexRestMeeting)
                    ->setParticipant($webexRestParticipant)
                    ->setRole($participant['role'] ?? ProviderParticipantSessionRole::ROLE_TRAINEE)
                );
            }
        }

        return $webexRestMeeting;
    }

    public function testFirstWebexParticipantIsReturned(): void
    {
        $webexRestMeeting = $this->createWebexRestMeeting([
        'licence' => 'email@gg.com',
        'participants' => [
            [
                'participant_email' => 'email@gg.com',
            ],
            [
                'participant_email' => 'licence@gg.com',
            ],
            [
                'participant_email' => 'other@gg.com',
            ],
        ],
      ]);

        $this->assertSame('email@gg.com', $webexRestMeeting->getLicence());
    }

    public function testGetLicenceParticipantRoleIfLicenceIsNotAnimatorThenNull()
    {
        $webexRestMeeting = $this->createWebexRestMeeting(
            [
                'licence' => 'licence@gg.com',
                'participants' => [['participant_email' => 'licence@gg.com']],
            ]
        );

        $this->assertNull($webexRestMeeting->getLicenceParticipantRole());
    }

    public function testGetLicenceParticipantRoleIfLicenceIsParticipantThenReturnParticipant()
    {
        $webexRestMeeting = $this->createWebexRestMeeting(
            [
                'licence' => 'licence@gg.com',
                'participants' => [['participant_email' => 'licence@gg.com', 'role' => ProviderParticipantSessionRole::ROLE_ANIMATOR]],
            ]
        );

        $this->assertInstanceOf(ProviderParticipantSessionRole::class, $webexRestMeeting->getLicenceParticipantRole());
        $this->assertEquals('licence@gg.com', $webexRestMeeting->getLicenceParticipantRole()->getEmail());
    }
}
