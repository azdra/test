<?php

namespace App\Tests\Unit\Model;

use App\Entity\User;
use LogicException;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testPassingInvalidOption()
    {
        $u = new User();

        $this->expectException(LogicException::class);
        $u->addOption('Invalid option');
    }

    public function testPassingCorrectOption()
    {
        $u = (new User())->addOption(User::OPTION_SKILL_AC);

        $this->assertTrue($u->hasOption(User::OPTION_SKILL_AC));
    }

    public function testPassingEmptyOption()
    {
        $u = new User();

        $this->assertSame(null, $u->getOption(User::OPTION_SKILL_AC));
    }

    public function testPassingOptionWithData()
    {
        $u = (new User())->addOption(User::OPTION_SKILL_AC, 'Plop');

        $this->assertSame('Plop', $u->getOption(User::OPTION_SKILL_AC));
    }
}
