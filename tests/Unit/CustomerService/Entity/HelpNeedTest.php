<?php

namespace App\Tests\Unit\CustomerService\Entity;

use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Entity\HelpTask;
use App\Entity\Client\Client;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class HelpNeedTest extends TestCase
{
    public function providerStatusCase(): \Generator
    {
        yield 'Not started' => [90, 180, null, HelpNeed::STATUS_NOT_STARTED];
        yield 'Ended' => [-180, -90, null, HelpNeed::STATUS_ENDED];
        yield 'Started not taken' => [-90, 90, null, HelpNeed::STATUS_STARTED_NOT_TAKEN];
        yield 'Started and taken' => [-90, 90, true, HelpNeed::STATUS_TAKEN];
        yield 'Started not taken with tasks' => [-90, 90, false, HelpNeed::STATUS_STARTED_NOT_TAKEN, false];
    }

    public function generateDate($duration): \DateTimeImmutable
    {
        if ($duration < 0) {
            $duration = abs($duration);

            return (new \DateTimeImmutable())->sub(new \DateInterval("PT{$duration}M"));
        }

        return (new \DateTimeImmutable())->add(new \DateInterval("PT{$duration}M"));
    }

    /** @dataProvider providerStatusCase */
    public function testGetStatus(int $minutesToStart, int $minutesToEnd, ?bool $hasTaskAndTaken, int $expectedStatus)
    {
        $helpNeed = new HelpNeed(new Client());
        $helpNeed->setStart($this->generateDate($minutesToStart));
        $helpNeed->setEnd($this->generateDate($minutesToEnd));

        if (null !== $hasTaskAndTaken) {
            $helpTaskOne = new HelpTask($helpNeed, new User());
            $helpTaskOne->setStart($helpNeed->getStart());
            $helpTaskOne->setEnd($helpNeed->getEnd());

            $helpTaskTwo = new HelpTask($helpNeed, new User());
            $helpTaskTwo->setStart($helpNeed->getStart());
            $helpTaskTwo->setEnd($helpNeed->getEnd());
            if ($hasTaskAndTaken) {
                $helpTaskTwo->setDateTakingAssistance(new \DateTimeImmutable());
            }
            $helpNeed->setHelpTasks(new ArrayCollection([$helpTaskOne, $helpTaskTwo]));
        }

        $status = $helpNeed->getStatus();

        $this->assertEquals($expectedStatus, $status);
    }
}
