<?php

namespace App\Tests\Unit\CustomerService\Service;

use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Repository\HelpNeedRepository;
use App\CustomerService\Service\HelpTaskService;
use App\CustomerService\Service\MassAssignService;
use App\Entity\Client\Client;
use App\Entity\User;
use App\Repository\ClientRepository;
use App\Repository\UserRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;

class MassAssignServiceTest extends TestCase
{
    public const MODIFICATION_UNSET = 'unset';

    private MockObject|HelpNeedRepository $helpNeedRepository;
    private MockObject|UserRepository $userRepository;
    private MockObject|HelpTaskService $helpTaskService;
    private MockObject|ClientRepository $clientRepository;

    public function setUp(): void
    {
        $this->helpNeedRepository = $this->createMock(HelpNeedRepository::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->helpTaskService = $this->createMock(HelpTaskService::class);
        $this->clientRepository = $this->createMock(ClientRepository::class);
    }

    private function getService(): MassAssignService
    {
        return new MassAssignService(
            $this->helpNeedRepository,
            $this->userRepository,
            $this->helpTaskService,
            $this->clientRepository,
        );
    }

    public function wrongAttributeProvider(): \Generator
    {
        yield 'Date start missing' => [['dateStart' => self::MODIFICATION_UNSET], MissingOptionsException::class, 'The required option "dateStart" is missing.'];
        yield 'Date end missing' => [['dateEnd' => self::MODIFICATION_UNSET], MissingOptionsException::class, 'The required option "dateEnd" is missing.'];
        yield 'Lang missing' => [['lang' => self::MODIFICATION_UNSET], MissingOptionsException::class, 'The required option "lang" is missing.'];
        yield 'ClientIds missing' => [['clientIds' => self::MODIFICATION_UNSET], MissingOptionsException::class, 'The required option "clientIds" is missing.'];
        yield 'UserIds missing' => [['userIds' => self::MODIFICATION_UNSET], MissingOptionsException::class, 'The required option "userIds" is missing.'];

        yield 'Date start must be well formatted' => [['dateStart' => 'invalid'], InvalidOptionsException::class, 'The option "dateStart" with value "invalid" is invalid.'];
        yield 'Date end must be well formatted' => [['dateEnd' => 'invalid'], InvalidOptionsException::class, 'The option "dateEnd" with value "invalid" is invalid.'];
        yield 'clientIds must be well formatted' => [['clientIds' => 'invalid'], InvalidOptionsException::class, 'The option "clientIds" with value "invalid" is invalid.'];
        yield 'userIds must be well formatted' => [['userIds' => 'invalid'], InvalidOptionsException::class, 'The option "userIds" with value "invalid" is invalid.'];
    }

    public function getDefaultAttributes(): array
    {
        return [
            'dateStart' => '2022-07-29T14:50:00.000+00:00',
            'dateEnd' => '2022-07-31T14:50:00.000+00:00',
            'lang' => 'lang',
            'clientIds' => '2,5',
            'userIds' => '12,42',
        ];
    }

    /** @dataProvider wrongAttributeProvider */
    public function testMassAddingThrowExceptionOnMissingAttribute(array $attributeModifications, string $exceptionType, string $errorMessage): void
    {
        $attributes = $this->getDefaultAttributes();
        foreach ($attributeModifications as $key => $value) {
            if ($value === self::MODIFICATION_UNSET) {
                unset($attributes[$key]);
            } else {
                $attributes[$key] = $value;
            }
        }

        $this->expectException($exceptionType);
        $this->expectExceptionMessage($errorMessage);

        $this->getService()->massAssign($attributes);
    }

    public function testMassAddingDoNotSearchUserIfNoHelpNeedsFound()
    {
        $attributes = $this->getDefaultAttributes();

        $this->helpNeedRepository->expects($this->once())
            ->method('findForMassAssign')
            ->with(new \DateTime('2022-07-29T14:50'), new \DateTime('2022-07-31T14:50'), 'lang', [2, 5])
            ->willReturn([]);

        $this->userRepository->expects($this->never())->method('findBy');

        $this->getService()->massAssign($attributes);
    }

    public function testMassAssignDoNothingIfNoUserFound()
    {
        $attributes = $this->getDefaultAttributes();

        $helpNeed = new HelpNeed(new Client());
        $this->helpNeedRepository->expects($this->once())
                                 ->method('findForMassAssign')
                                 ->with(new \DateTime('2022-07-29T14:50'), new \DateTime('2022-07-31T14:50'), 'lang', [2, 5])
                                 ->willReturn([$helpNeed]);

        $this->userRepository->expects($this->once())
                             ->method('findAllProviderServiceAccounts')
                             ->with([12, 42])
                             ->willReturn([]);

        $this->helpTaskService->expects($this->never())
                              ->method('assignedUserToHelpNeed');

        $this->getService()->massAssign($attributes);
    }

    public function testMassAssign()
    {
        $attributes = $this->getDefaultAttributes();

        $helpNeed = new HelpNeed(new Client());
        $userTest = new User();
        $this->helpNeedRepository->expects($this->once())
                                 ->method('findForMassAssign')
                                 ->with(new \DateTime('2022-07-29T14:50'), new \DateTime('2022-07-31T14:50'), 'lang', [2, 5])
                                 ->willReturn([$helpNeed]);

        $this->userRepository->expects($this->once())
                             ->method('findAllProviderServiceAccounts')
                             ->with([12, 42])
                             ->willReturn([$userTest]);

        $this->helpTaskService->expects($this->once())
                              ->method('assignedUserToHelpNeed')
                              ->with($helpNeed, $userTest);

        $this->getService()->massAssign($attributes);
    }

    public function testMassAssignMultiple()
    {
        $attributes = $this->getDefaultAttributes();

        $helpNeed1 = (new HelpNeed(new Client()))->setType(HelpNeed::TYPE_ASSISTANCE);
        $helpNeed2 = (new HelpNeed(new Client()))->setType(HelpNeed::TYPE_SUPPORT);
        $userTest1 = (new User())->setFirstName('Issam');
        $userTest2 = (new User())->setFirstName('Xavier');
        $this->helpNeedRepository->expects($this->once())
                                 ->method('findForMassAssign')
                                 ->with(new \DateTime('2022-07-29T14:50'), new \DateTime('2022-07-31T14:50'), 'lang', [2, 5])
                                 ->willReturn([$helpNeed1, $helpNeed2]);

        $this->userRepository->expects($this->once())
                             ->method('findAllProviderServiceAccounts')
                             ->with([12, 42])
                             ->willReturn([$userTest1, $userTest2]);

        $this->helpTaskService->expects($this->exactly(4))
                              ->method('assignedUserToHelpNeed')
                              ->withConsecutive(
                                  [$helpNeed1, $userTest1],
                                  [$helpNeed1, $userTest2],
                                  [$helpNeed2, $userTest1],
                                  [$helpNeed2, $userTest2]
                              );

        $this->getService()->massAssign($attributes);
    }
}
