<?php

namespace App\Tests\Unit\CustomerService\Service;

use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Entity\HelpTask;
use App\CustomerService\Repository\AvailabilityUserRepository;
use App\CustomerService\Service\CustomerServiceService;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\Client\ClientServices;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderSession;
use App\Entity\ServiceSchedule;
use App\Entity\User;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\MailerService;
use App\Service\SupportService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CustomerServiceFacturationServiceTest extends TestCase
{
    private ProviderSessionRepository $providerSessionRepository;
    private SupportService $supportService;
    private AvailabilityUserRepository $availabilityUserRepository;
    private UserRepository $userRepository;
    private TranslatorInterface $translator;
    private EntityManagerInterface $entityManager;
    private ActivityLogger $activityLogger;
    private MailerService $mailerService;
    private RouterInterface $router;

    public function setUp(): void
    {
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->providerSessionRepository = $this->createMock(ProviderSessionRepository::class);
        $this->supportService = $this->createMock(SupportService::class);
        $this->availabilityUserRepository = $this->createMock(AvailabilityUserRepository::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->activityLogger = $this->createMock(ActivityLogger::class);
        $this->mailerService = $this->createMock(MailerService::class);
        $this->router = $this->createMock(RouterInterface::class);
    }

    public function getService(): CustomerServiceService
    {
        return new CustomerServiceService(
            $this->translator,
            $this->providerSessionRepository,
            $this->supportService,
            $this->availabilityUserRepository,
            $this->userRepository
        );
    }

    private function createClient(): Client
    {
        $client = new Client();
        $client->setServices($this->createServicesConfiguration());
        $client->addConfigurationHolder($this->createConfigurationHolder());
        $client->getConfigurationHolders()->first()->setClient($client);

        return $client;
    }

    private function createSession(int $assistanceType, int $supportType, int $start, int $end, bool $outsideStandardHours = true): ProviderSession
    {
        $client = $this->createClient();
        $session = new AdobeConnectSCO($client->getConfigurationHolders()->first());
        $session->setAssistanceType($assistanceType)
            ->setSupportType($supportType)
            ->setDateStart((new \DateTime())->setTime($start, 0))
            ->setDateEnd((new \DateTime())->setTime($end, 0))
            ->setStandardHours(true)
            ->setOutsideStandardHours($outsideStandardHours);

        return $session;
    }

    private function createSessionsWithHelpTask(int $assistanceType, int $supportType, int $start, int $end, bool $outsideStandardHours = true): array
    {
        $userCostumerService = (new User())
            ->setId(1995)
            ->setReferenceClient('ILA')
            ->setFirstName('Issam')
            ->setLastName('LAMMRI')
            ->setEmail('serviceClient@live-ession.fr')
            ->setRoles(['ROLE_CUSTOMER_SERVICE']);
        $client = $this->createClient();
        $session = new AdobeConnectSCO($client->getConfigurationHolders()->first());
        $session->setAssistanceType($assistanceType)
            ->setSupportType($supportType)
            ->setDateStart((new \DateTime())->setTime($start, 0))
            ->setDateEnd((new \DateTime())->setTime($end, 0))
            ->setStandardHours(true)
            ->setOutsideStandardHours($outsideStandardHours);

        $helpNeed = (new HelpNeed($client))
            ->setSession($session)
            ->setType(HelpNeed::TYPE_SUPPORT)
        ;
        $helpTasks = new ArrayCollection();
        $helpTask = (new HelpTask($helpNeed, $userCostumerService))
            ->setStartReal((new \DateTimeImmutable())->setTime(10, 0))
            ->setEndReal((new \DateTimeImmutable())->setTime(11, 0));
        $helpTasks->add($helpTask);
        $helpNeed->setHelpTasks($helpTasks);
        $helpNeeds = new ArrayCollection();
        $helpNeeds->add($helpNeed);
        $session->setHelpNeeds($helpNeeds);

        return [$session];
    }

    private function createSessionWithExistingSlot(int $assistanceType, int $supportType, array $slots): ProviderSession
    {
        $session = $this->createSession($assistanceType, $supportType, 10, 11, false);

        foreach ($slots as $slot) {
            $helpNeed = (new HelpNeed($session->getClient()))
                ->setType($slot['type'])
                ->setSession($session)
                ->setStart($slot['start'])
                ->setEnd($slot['end']);
            $session->addHelpNeed($helpNeed);
        }

        return $session;
    }

    private function createConfigurationHolder(): AbstractProviderConfigurationHolder
    {
        return new AdobeConnectConfigurationHolder();
    }

    private function createServicesConfiguration(): ClientServices
    {
        $serviceConfiguration = new ClientServices();
        $serviceConfiguration->setStandardHours($this->createServiceSchedule(9, 12, 14, 18));
        $serviceConfiguration->setOutsideStandardHours($this->createServiceSchedule(7, 9, 18, 20, 12, 14));
        $serviceConfiguration->setAssistanceStart(15);
        $serviceConfiguration->setAssistanceDuration(30);
        $serviceConfiguration->setTimeBeforeSession(20);
        $serviceConfiguration->setTimeSupportDurationSession(40);

        return $serviceConfiguration;
    }

    private function createServiceSchedule(int $startAm, int $endAm, int $startPm, int $endPm, int $startLT = 0, int $endLT = 0): ServiceSchedule
    {
        $schedule = new ServiceSchedule();
        $schedule->setStartTimeAM((new \DateTime())->setTime($startAm, 0));
        $schedule->setEndTimeAM((new \DateTime())->setTime($endAm, 0));
        $schedule->setStartTimeLH((new \DateTime())->setTime($startLT, 0));
        $schedule->setEndTimeLH((new \DateTime())->setTime($endLT, 0));
        $schedule->setStartTimePM((new \DateTime())->setTime($startPm, 0));
        $schedule->setEndTimePM((new \DateTime())->setTime($endPm, 0));

        return $schedule;
    }

    private function createSlotGenerator(): \Generator
    {
        yield 'All disabled' => [$this->createSessionsWithHelpTask(ProviderSession::ENUM_NO, ProviderSession::ENUM_SUPPORT_TOTAL, 10, 11), []];
    }

    /** @dataProvider createSlotGenerator
     * @throws \Exception
     */
    public function testCreateSessionHelpNeed(array $sessions, array $expectedSlots): void
    {
        $this->supportService
            ->expects($this->once())
            ->method('getOverlappedCustomer')->willReturn(['slotsDetails' => [
                'beforeHS' => '00:00 > 08:30',
                'inHSStartAndHSEnd' => '08:30 > 17:30',
                'afterHS' => '17:30 > 23:59',
            ],
                'slotsResults' => [
                    'beforeHS' => 0,
                    'inHSStartAndHSEnd' => 210,
                    'afterHS' => 150,
                ], ]);

        $facturation = $this->getService()->getBillingClaimantFromSA($sessions);
        $this->assertIsArray($facturation);

        // Assuming $facturation['_1995'] is the part of the array you are interested in
        $this->assertEquals(1, $facturation['_1995']['support']['quantity']);
        $this->assertEquals(3.5, $facturation['_1995']['support']['hs']);
        $this->assertEquals(2.5, $facturation['_1995']['support']['hhs']);
    }

    private function assertSlotEquals(array $expectedSlots, array $actualSlots): void
    {
        /** @var HelpNeed[] $actualSlots */
        $actualSlots = array_values($actualSlots);
        $this->assertCount(count($expectedSlots), $actualSlots);
        if (!empty($expectedSlots)) {
            foreach ($expectedSlots as $index => $expectedSlot) {
                $expectedStart = $expectedSlot['start'];
                $expectedEnd = $expectedSlot['end'];
                $expectedType = $expectedSlot['type'];
                $this->assertEquals($expectedStart, $actualSlots[$index]->getStart()->format('H:i'));
                $this->assertEquals($expectedEnd, $actualSlots[$index]->getEnd()->format('H:i'));
                $this->assertEquals($expectedType, $actualSlots[$index]->getType());
            }
        }
    }
}
