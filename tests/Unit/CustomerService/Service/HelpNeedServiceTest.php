<?php

namespace App\Tests\Unit\CustomerService\Service;

use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Service\HelpNeedService;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\Client\ClientServices;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderSession;
use App\Entity\ServiceSchedule;
use App\Service\ActivityLogger;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class HelpNeedServiceTest extends TestCase
{
    private EntityManagerInterface $entityManager;
    private ActivityLogger $activityLogger;
    private MailerService $mailerService;
    private TranslatorInterface $translator;
    private RouterInterface $router;

    public function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->activityLogger = $this->createMock(ActivityLogger::class);
        $this->mailerService = $this->createMock(MailerService::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->router = $this->createMock(RouterInterface::class);
    }

    public function getService(): HelpNeedService
    {
        return new HelpNeedService(
            $this->entityManager,
            $this->activityLogger,
            $this->mailerService,
            $this->translator,
            $this->router
        );
    }

    private function createClient(): Client
    {
        $client = new Client();
        $client->setServices($this->createServicesConfiguration());
        $client->addConfigurationHolder($this->createConfigurationHolder());
        $client->getConfigurationHolders()->first()->setClient($client);

        return $client;
    }

    private function createSession(int $assistanceType, int $supportType, int $start, int $end, bool $outsideStandardHours = true): ProviderSession
    {
        $client = $this->createClient();
        $session = new AdobeConnectSCO($client->getConfigurationHolders()->first());
        $session->setAssistanceType($assistanceType)
            ->setSupportType($supportType)
            ->setDateStart((new \DateTime())->setTime($start, 0))
            ->setDateEnd((new \DateTime())->setTime($end, 0))
            ->setStandardHours(true)
            ->setOutsideStandardHours($outsideStandardHours);

        return $session;
    }

    private function createSessionWithExistingSlot(int $assistanceType, int $supportType, array $slots): ProviderSession
    {
        $session = $this->createSession($assistanceType, $supportType, 10, 11, false);

        foreach ($slots as $slot) {
            $helpNeed = (new HelpNeed($session->getClient()))
                ->setType($slot['type'])
                ->setSession($session)
                ->setStart($slot['start'])
                ->setEnd($slot['end']);
            $session->addHelpNeed($helpNeed);
        }

        return $session;
    }

    private function createConfigurationHolder(): AbstractProviderConfigurationHolder
    {
        return new AdobeConnectConfigurationHolder();
    }

    private function createServicesConfiguration(): ClientServices
    {
        $serviceConfiguration = new ClientServices();
        $serviceConfiguration->setStandardHours($this->createServiceSchedule(9, 12, 14, 18));
        $serviceConfiguration->setOutsideStandardHours($this->createServiceSchedule(7, 9, 18, 20, 12, 14));
        $serviceConfiguration->setAssistanceStart(15);
        $serviceConfiguration->setAssistanceDuration(30);
        $serviceConfiguration->setTimeBeforeSession(20);
        $serviceConfiguration->setTimeSupportDurationSession(40);

        return $serviceConfiguration;
    }

    private function createServiceSchedule(int $startAm, int $endAm, int $startPm, int $endPm, int $startLT = 0, int $endLT = 0): ServiceSchedule
    {
        $schedule = new ServiceSchedule();
        $schedule->setStartTimeAM((new \DateTime())->setTime($startAm, 0));
        $schedule->setEndTimeAM((new \DateTime())->setTime($endAm, 0));
        $schedule->setStartTimeLH((new \DateTime())->setTime($startLT, 0));
        $schedule->setEndTimeLH((new \DateTime())->setTime($endLT, 0));
        $schedule->setStartTimePM((new \DateTime())->setTime($startPm, 0));
        $schedule->setEndTimePM((new \DateTime())->setTime($endPm, 0));

        return $schedule;
    }

    private function createSlotGenerator(): \Generator
    {
        yield 'All disabled' => [$this->createSession(ProviderSession::ENUM_NO, ProviderSession::ENUM_SUPPORT_NO, 10, 11), []];
        yield 'Simple assistance on launch' => [
            $this->createSession(ProviderSession::ENUM_LAUNCH, ProviderSession::ENUM_SUPPORT_NO, 10, 11),
            [['type' => HelpNeed::TYPE_ASSISTANCE, 'start' => '09:45', 'end' => '10:15']],
        ];
        yield 'Simple pedagogue assistance' => [
            $this->createSession(ProviderSession::ENUM_PEDA, ProviderSession::ENUM_SUPPORT_NO, 10, 11),
            [['type' => HelpNeed::TYPE_ASSISTANCE, 'start' => '09:45', 'end' => '11:30']],
        ];
        yield 'Simple co animation assistance' => [
            $this->createSession(ProviderSession::ENUM_COANIM, ProviderSession::ENUM_SUPPORT_NO, 10, 11),
            [['type' => HelpNeed::TYPE_ASSISTANCE, 'start' => '09:45', 'end' => '11:00']],
        ];
        yield 'Simple total assistance' => [
            $this->createSession(ProviderSession::ENUM_TOTAL, ProviderSession::ENUM_SUPPORT_NO, 10, 11),
            [['type' => HelpNeed::TYPE_ASSISTANCE, 'start' => '09:45', 'end' => '11:00']],
        ];
        yield 'Simple support on launch' => [
            $this->createSession(ProviderSession::ENUM_NO, ProviderSession::ENUM_SUPPORT_LAUNCH, 10, 11),
            [['type' => HelpNeed::TYPE_SUPPORT, 'start' => '09:40', 'end' => '10:20']],
        ];
        yield 'Simple total support' => [
            $this->createSession(ProviderSession::ENUM_NO, ProviderSession::ENUM_SUPPORT_TOTAL, 10, 11),
            [['type' => HelpNeed::TYPE_SUPPORT, 'start' => '09:40', 'end' => '11:00']],
        ];
        yield 'Simple support & assistance on launch' => [
            $this->createSession(ProviderSession::ENUM_LAUNCH, ProviderSession::ENUM_SUPPORT_LAUNCH, 10, 11),
            [['type' => HelpNeed::TYPE_ASSISTANCE, 'start' => '09:45', 'end' => '10:15'], ['type' => HelpNeed::TYPE_SUPPORT, 'start' => '09:40', 'end' => '10:20']],
        ];
        yield 'Assistance start and end outside' => [
            $this->createSession(ProviderSession::ENUM_TOTAL, ProviderSession::ENUM_SUPPORT_NO, 4, 22),
            [['type' => HelpNeed::TYPE_ASSISTANCE, 'start' => '07:00', 'end' => '20:00']],
        ];
        yield 'Support start and end outside' => [
            $this->createSession(ProviderSession::ENUM_NO, ProviderSession::ENUM_SUPPORT_TOTAL, 4, 22),
            [['type' => HelpNeed::TYPE_SUPPORT, 'start' => '07:00', 'end' => '20:00']],
        ];
        yield 'Support - Outside hours disabled - all day - start early, end late, on lunch time' => [
            $this->createSession(ProviderSession::ENUM_NO, ProviderSession::ENUM_SUPPORT_TOTAL, 4, 22, false),
            [['type' => HelpNeed::TYPE_SUPPORT, 'start' => '09:00', 'end' => '12:00'], ['type' => HelpNeed::TYPE_SUPPORT, 'start' => '14:00', 'end' => '18:00']],
        ];
        yield 'Support - Outside hours disabled - morning - fit in' => [
            $this->createSession(ProviderSession::ENUM_NO, ProviderSession::ENUM_SUPPORT_LAUNCH, 10, 11, false),
            [['type' => HelpNeed::TYPE_SUPPORT, 'start' => '09:40', 'end' => '10:20']],
        ];
        yield 'Support - Outside hours disabled - morning - start early' => [
            $this->createSession(ProviderSession::ENUM_NO, ProviderSession::ENUM_SUPPORT_LAUNCH, 9, 10, false),
            [['type' => HelpNeed::TYPE_SUPPORT, 'start' => '09:00', 'end' => '09:20']],
        ];
        yield 'Support - Outside hours disabled - morning - end late' => [
            $this->createSession(ProviderSession::ENUM_NO, ProviderSession::ENUM_SUPPORT_LAUNCH, 12, 13, false),
            [['type' => HelpNeed::TYPE_SUPPORT, 'start' => '11:40', 'end' => '12:00']],
        ];
        yield 'Support - Outside hours disabled - afternoon - fit in' => [
            $this->createSession(ProviderSession::ENUM_NO, ProviderSession::ENUM_SUPPORT_LAUNCH, 15, 16, false),
            [['type' => HelpNeed::TYPE_SUPPORT, 'start' => '14:40', 'end' => '15:20']],
        ];
        yield 'Support - Outside hours disabled - afternoon - start early' => [
            $this->createSession(ProviderSession::ENUM_NO, ProviderSession::ENUM_SUPPORT_LAUNCH, 14, 15, false),
            [['type' => HelpNeed::TYPE_SUPPORT, 'start' => '14:00', 'end' => '14:20']],
        ];
        yield 'Support - Outside hours disabled - afternoon - end late' => [
            $this->createSession(ProviderSession::ENUM_NO, ProviderSession::ENUM_SUPPORT_LAUNCH, 18, 19, false),
            [['type' => HelpNeed::TYPE_SUPPORT, 'start' => '17:40', 'end' => '18:00']],
        ];
    }

    private function updateHelpNeedGenerator(): \Generator
    {
        yield 'Remove all Assistance if disabled' => [
            $this->createSessionWithExistingSlot(
                ProviderSession::ENUM_NO,
                ProviderSession::ENUM_SUPPORT_LAUNCH,
                [
                    ['type' => HelpNeed::TYPE_ASSISTANCE,
                        'start' => (new \DateTimeImmutable())->setTime(9, 45),
                        'end' => (new \DateTimeImmutable())->setTime(10, 15), ],
                    ['type' => HelpNeed::TYPE_SUPPORT,
                        'start' => (new \DateTimeImmutable())->setTime(9, 40),
                        'end' => (new \DateTimeImmutable())->setTime(10, 20), ],
                    ['type' => HelpNeed::TYPE_ASSISTANCE,
                        'start' => (new \DateTimeImmutable())->setTime(9, 45),
                        'end' => (new \DateTimeImmutable())->setTime(10, 15), ],
                ]
            ),
            [['type' => HelpNeed::TYPE_SUPPORT, 'start' => '09:40', 'end' => '10:20']],
        ];

        yield 'Update existings assistance' => [
            $this->createSessionWithExistingSlot(
                ProviderSession::ENUM_LAUNCH,
                ProviderSession::ENUM_SUPPORT_NO,
                [
                    ['type' => HelpNeed::TYPE_ASSISTANCE,
                        'start' => (new \DateTimeImmutable())->setTime(7, 45),
                        'end' => (new \DateTimeImmutable())->setTime(8, 15), ],
                ]
            ),
            [['type' => HelpNeed::TYPE_ASSISTANCE, 'start' => '09:45', 'end' => '10:15']],
        ];

        yield 'Remove all Support if disabled' => [
            $this->createSessionWithExistingSlot(
                ProviderSession::ENUM_LAUNCH,
                ProviderSession::ENUM_SUPPORT_NO,
                [
                    ['type' => HelpNeed::TYPE_SUPPORT,
                        'start' => (new \DateTimeImmutable())->setTime(9, 40),
                        'end' => (new \DateTimeImmutable())->setTime(10, 20), ],
                    ['type' => HelpNeed::TYPE_ASSISTANCE,
                        'start' => (new \DateTimeImmutable())->setTime(9, 45),
                        'end' => (new \DateTimeImmutable())->setTime(10, 15), ],
                    ['type' => HelpNeed::TYPE_SUPPORT,
                        'start' => (new \DateTimeImmutable())->setTime(9, 40),
                        'end' => (new \DateTimeImmutable())->setTime(10, 20), ],
                ]
            ),
            [['type' => HelpNeed::TYPE_ASSISTANCE, 'start' => '09:45', 'end' => '10:15']],
        ];

        yield 'Update existings support' => [
            $this->createSessionWithExistingSlot(
                ProviderSession::ENUM_NO,
                ProviderSession::ENUM_SUPPORT_LAUNCH,
                [
                    ['type' => HelpNeed::TYPE_SUPPORT,
                        'start' => (new \DateTimeImmutable())->setTime(7, 40),
                        'end' => (new \DateTimeImmutable())->setTime(8, 20), ],
                ]
            ),
            [['type' => HelpNeed::TYPE_SUPPORT, 'start' => '09:40', 'end' => '10:20']],
        ];
    }

    /** @dataProvider createSlotGenerator */
    public function testCreateSessionHelpNeed(ProviderSession $session, array $expectedSlots): void
    {
        $this->getService()->synchronizeSessionHelpNeed($session);

        $this->assertSlotEquals($expectedSlots, $session->getHelpNeeds()->toArray());
    }

    /** @dataProvider updateHelpNeedGenerator */
    public function testUpdateHelpNeed(ProviderSession $session, array $expectedSlots): void
    {
        $this->getService()->synchronizeSessionHelpNeed($session);

        $this->assertSlotEquals($expectedSlots, $session->getHelpNeeds()->toArray());
    }

    private function assertSlotEquals(array $expectedSlots, array $actualSlots): void
    {
        /** @var HelpNeed[] $actualSlots */
        $actualSlots = array_values($actualSlots);
        $this->assertCount(count($expectedSlots), $actualSlots);
        if (!empty($expectedSlots)) {
            foreach ($expectedSlots as $index => $expectedSlot) {
                $expectedStart = $expectedSlot['start'];
                $expectedEnd = $expectedSlot['end'];
                $expectedType = $expectedSlot['type'];
                $this->assertEquals($expectedStart, $actualSlots[$index]->getStart()->format('H:i'));
                $this->assertEquals($expectedEnd, $actualSlots[$index]->getEnd()->format('H:i'));
                $this->assertEquals($expectedType, $actualSlots[$index]->getType());
            }
        }
    }
}
