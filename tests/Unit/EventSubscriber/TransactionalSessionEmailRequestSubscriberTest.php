<?php

namespace App\Tests\Unit\EventSubscriber;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Event\TransactionalParticipantEmailRequestEvent;
use App\Event\TransactionalSessionEmailRequestEvent;
use App\EventSubscriber\TransactionalSessionEmailRequestSubscriber;
use App\Service\ActivityLogger;
use App\Service\ConvocationService;
use App\Service\MailerService;
use App\Service\ReferrerService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class TransactionalSessionEmailRequestSubscriberTest extends TestCase
{
    private ConvocationService $convocationService;
    private MailerService $mailerService;
    private LoggerInterface $logger;
    private TranslatorInterface $translator;
    private ReferrerService $referrerService;
    private ActivityLogger $activityLogger;
    private RouterInterface $router;
    protected string $endPointDomain = 'http://localhost';

    protected function setUp(): void
    {
        $this->convocationService = $this->createMock(ConvocationService::class);
        $this->mailerService = $this->createMock(MailerService::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->referrerService = $this->createMock(ReferrerService::class);
        $this->activityLogger = $this->createMock(ActivityLogger::class);
        $this->router = $this->createMock(RouterInterface::class);
    }

    public function createTransactionalSubscriber(): TransactionalSessionEmailRequestSubscriber
    {
        return new TransactionalSessionEmailRequestSubscriber(
            $this->convocationService, $this->mailerService, $this->logger, $this->translator, $this->referrerService, $this->activityLogger, $this->router, $this->endPointDomain
        );
    }

    public function createClient(array $data = []): Client
    {
        return (new Client())
            ->setId(418)
            ->setName($data['clientName'] ?? 'Client name');
    }

    public function createConfigurationHolder(array $data = []): AbstractProviderConfigurationHolder
    {
        return (new AdobeConnectConfigurationHolder())
            ->setClient($this->createClient($data));
    }

    public function createProviderSession(array $data = []): ProviderSession
    {
        return ( new AdobeConnectSCO($this->createConfigurationHolder($data)) )
            ->setId(418)
            ->setDateStart(new \DateTime($data['dateStart'] ?? 'now'))
            ->setDateEnd(new \DateTime($data['dateEnd'] ?? 'now'))
            ->setStatus($data['status'] ?? ProviderSession::STATUS_DRAFT)
            ->setNotificationDate($data['notification'] ?? null)
            ->setLastConvocationSent($data['lastConvocationSentSession'] ?? null)
            ->setDateOfSendingReminders($data['reminders'] ?? [])
            ->setLastReminderSent($data['lastReminderSentSession'] ?? null);
    }

    public function createProviderParticipantSessionRole(array $data = []): ProviderParticipantSessionRole
    {
        $user = (new User())
            ->setPreferredLang($data['participantLang'] ?? 'fr')
            ->setEmail($data['participantLang'] ?? 'my-emeil@gg.com')
            ->setId(418)
            ->setClient($this->createClient($data));

        $participant = ( new AdobeConnectPrincipal($this->createConfigurationHolder($data)) )
            ->setId(418)
            ->setUser($user);

        return ( new ProviderParticipantSessionRole() )
            ->setSession($this->createProviderSession($data))
            ->setParticipant($participant)
            ->setLastConvocationSent($data['lastConvocationSentParticipant'] ?? null)
            ->setLastReminderSent($data['lastReminderSentParticipant'] ?? null);
    }

    // <editor-folder> Convocation
    public function generateParticipantRoleForValidConvocation(): \Generator
    {
        yield 'Session in future' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'notification' => new \DateTimeImmutable('-2 hours'),
                ]
            ),
        ];
        yield 'Session in progress' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '-1 hours',
                    'dateEnd' => '+3 hours',
                    'notification' => new \DateTimeImmutable('-2 hours'),
                ]
            ),
        ];
        yield 'Scheduled w/ notification sent but not for participant' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'notification' => new \DateTimeImmutable('-2 hours'),
                    'lastConvocationSentSession' => new \DateTimeImmutable('-1 hours -30 minutes'),
                ]
            ),
        ];
    }

    public function generateParticipantRoleForInvalidConvocation(): \Generator
    {
        yield 'Draft session' => [$this->createProviderParticipantSessionRole()];
        yield 'Canceled session' => [
            $this->createProviderParticipantSessionRole(
                ['status' => ProviderSession::STATUS_CANCELED]
            ),
        ];
        yield 'Finished session' => [
            $this->createProviderParticipantSessionRole(
                [
                    'dateStart' => '-3 hours',
                    'dateEnd' => '+2 minutes',
                ]
            ),
        ];
        yield 'Scheduled w/o notification date' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                ]
            ),
        ];
        yield 'Scheduled w/ notification in future' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'notification' => new \DateTimeImmutable('+2 hours'),
                ]
            ),
        ];
        yield 'Scheduled w/ notification already sent' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'notification' => new \DateTimeImmutable('-2 hours'),
                    'lastConvocationSentSession' => new \DateTimeImmutable('-1 hours -30 minutes'),
                    'lastConvocationSentParticipant' => new \DateTimeImmutable('-1 hours -30 minutes'),
                ]
            ),
        ];
        yield 'Scheduled w/ notification to sent but already sent for participant' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'notification' => new \DateTimeImmutable('-2 hours'),
                    'lastConvocationSentParticipant' => new \DateTimeImmutable('-1 hours -30 minutes'),
                ]
            ),
        ];
    }

    /**
     * @dataProvider generateParticipantRoleForValidConvocation
     */
    public function testConvocationIsToBeSent(ProviderParticipantSessionRole $participantSessionRole): void
    {
        $this->assertTrue(TransactionalSessionEmailRequestSubscriber::participantWantToReceiveConvocation($participantSessionRole));
    }

    /**
     * @dataProvider generateParticipantRoleForValidConvocation
     * @dataProvider generateParticipantRoleForInvalidConvocation
     */
    public function testConvocationIsToBeSentIfItsForced(ProviderParticipantSessionRole $participantSessionRole): void
    {
        $this->assertTrue(TransactionalSessionEmailRequestSubscriber::participantWantToReceiveConvocation($participantSessionRole, true));
        $this->assertTrue(TransactionalSessionEmailRequestSubscriber::participantWantToReceiveConvocation($participantSessionRole, true));
    }

    /**
     * @dataProvider generateParticipantRoleForInvalidConvocation
     */
    public function testConvocationIsNotToBeSent(ProviderParticipantSessionRole $participantSessionRole): void
    {
        $this->assertFalse(TransactionalSessionEmailRequestSubscriber::participantWantToReceiveConvocation($participantSessionRole));
    }

    public function generateSessionValidConvocation(): \Generator
    {
        yield 'Session in future' => [
            $this->createProviderSession(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'notification' => new \DateTimeImmutable('-2 hours'),
                ]
            ),
        ];
        yield 'Session in progress' => [
            $this->createProviderSession(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '-1 hours',
                    'dateEnd' => '+3 hours',
                    'notification' => new \DateTimeImmutable('-2 hours'),
                ]
            ),
        ];
        yield 'Scheduled w/ notification sent but not for participant' => [
            $this->createProviderSession(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'notification' => new \DateTimeImmutable('-2 hours'),
                    'lastConvocationSentSession' => new \DateTimeImmutable('-1 hours -30 minutes'),
                ]
            ),
        ];
    }

    /** @dataProvider generateSessionValidConvocation */
    public function testSendingConvocationSuccessful(ProviderSession $session): void
    {
        $transactionalSubscriber = $this->createTransactionalSubscriber();
        $transactionalSubscriber->onConvocationRequestForAllParticipants(
            new TransactionalSessionEmailRequestEvent($session)
        );

        $this->assertNotNull($session->getLastConvocationSent());
    }

    public function generateSessionInvalidConvocation(): \Generator
    {
        yield 'Draft session' => [$this->createProviderSession()];
        yield 'Canceled session' => [
            $this->createProviderSession(
                ['status' => ProviderSession::STATUS_CANCELED]
            ),
        ];
        yield 'Finished session' => [
            $this->createProviderSession(
                [
                    'dateStart' => '-3 hours',
                    'dateEnd' => '+2 minutes',
                ]
            ),
        ];
        yield 'Scheduled w/o notification date' => [
            $this->createProviderSession(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                ]
            ),
        ];
        yield 'Scheduled w/ notification in future' => [
            $this->createProviderSession(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'notification' => new \DateTimeImmutable('+2 hours'),
                ]
            ),
        ];
        yield 'Scheduled w/ notification already sent' => [
            $this->createProviderSession(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'notification' => new \DateTimeImmutable('-2 hours'),
                    'lastConvocationSentSession' => new \DateTimeImmutable('-1 hours -30 minutes'),
                ]
            ),
        ];
    }

    /** @dataProvider generateSessionInvalidConvocation */
    public function testNotSendingConvocation(ProviderSession $session): void
    {
        $transactionalSubscriber = $this->createTransactionalSubscriber();

        $this->convocationService
            ->expects($this->never())
            ->method('sendConvocation');

        $transactionalSubscriber->onConvocationRequestForAllParticipants(
            new TransactionalSessionEmailRequestEvent($session)
        );
    }

    // </editor-folder> Convocation

    // <editor-folder> Reminders
    public function generateParticipantRoleForValidReminders(): \Generator
    {
        $remiders = [new \DateTimeImmutable('-3 hours'), new \DateTimeImmutable('-2 hours')];

        yield 'Session in future' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'lastConvocationSentSession' => new \DateTimeImmutable(),
                    'lastConvocationSentParticipant' => new \DateTimeImmutable(),
                    'reminders' => $remiders,
                ]
            ),
        ];
        yield 'Session in progress' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '-1 hours',
                    'dateEnd' => '+3 hours',
                    'lastConvocationSentSession' => new \DateTimeImmutable(),
                    'lastConvocationSentParticipant' => new \DateTimeImmutable(),
                    'reminders' => $remiders,
                ]
            ),
        ];
        yield 'Session in progress w/ one reminder sent' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '-1 hours',
                    'dateEnd' => '+3 hours',
                    'lastConvocationSentSession' => new \DateTimeImmutable('-3 hours'),
                    'lastConvocationSentParticipant' => new \DateTimeImmutable('-3 hours'),
                    'lastReminderSentSession' => new \DateTimeImmutable('-2 hours -30 minutes'),
                    'lastReminderSentParticipant' => new \DateTimeImmutable('-2 hours -30 minutes'),
                    'reminders' => $remiders,
                ]
            ),
        ];
    }

    public function generateParticipantRoleForInvalidReminders(): \Generator
    {
        yield 'Draft session' => [$this->createProviderParticipantSessionRole()];
        yield 'Canceled session' => [
            $this->createProviderParticipantSessionRole(
                ['status' => ProviderSession::STATUS_CANCELED]
            ),
        ];
        yield 'Finished session' => [
            $this->createProviderParticipantSessionRole(
                [
                    'dateStart' => '-3 hours',
                    'dateEnd' => '+2 minutes',
                ]
            ),
        ];
        yield 'Scheduled session w/o convocation sent' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                ]
            ),
        ];
        yield 'Scheduled session w/ convocation sent' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'lastConvocationSentSession' => new \DateTimeImmutable('-2 hours'),
                ]
            ),
        ];
        yield 'Scheduled session w/ reminder sent' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'lastConvocationSentSession' => new \DateTimeImmutable('-2 hours'),
                    'reminders' => [new \DateTimeImmutable('-2 hours')],
                    'lastReminderSentSession' => new \DateTimeImmutable('-2 hours'),
                ]
            ),
        ];
        yield 'Participant w/o convocation and reminder sent' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+3 hours',
                    'lastConvocationSentSession' => new \DateTimeImmutable('-2 hours'),
                    'reminders' => [new \DateTimeImmutable('+2 hours')],
                    'lastReminderSentSession' => new \DateTimeImmutable('-2 hours'),
                ]
            ),
        ];
        yield 'Participant w/ convocation and reminder sent' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+3 hours',
                    'lastConvocationSentSession' => new \DateTimeImmutable('-2 hours'),
                    'reminders' => [new \DateTimeImmutable('+2 hours')],
                    'lastReminderSentSession' => new \DateTimeImmutable('-2 hours'),
                    'lastConvocationSentParticipant' => new \DateTimeImmutable('-2 hours'),
                    'lastReminderSentParticipant' => new \DateTimeImmutable('-2 hours'),
                ]
            ),
        ];
    }

    /** @dataProvider generateParticipantRoleForValidReminders */
    public function testReminderIsToBeSent(ProviderParticipantSessionRole $participantSessionRole): void
    {
        $this->assertTrue(TransactionalSessionEmailRequestSubscriber::participantWantToReceiveReminder($participantSessionRole));
    }

    /**
     * @dataProvider generateParticipantRoleForValidReminders
     * @dataProvider generateParticipantRoleForInvalidReminders
     */
    public function testReminderIsToBeSentIfItsForced(ProviderParticipantSessionRole $participantSessionRole): void
    {
        $this->assertTrue(TransactionalSessionEmailRequestSubscriber::participantWantToReceiveReminder($participantSessionRole, true));
    }

    /**
     * @dataProvider generateParticipantRoleForInvalidReminders
     */
    public function testReminderIsNotToBeSent(ProviderParticipantSessionRole $participantSessionRole): void
    {
        $this->assertFalse(TransactionalSessionEmailRequestSubscriber::participantWantToReceiveReminder($participantSessionRole));
    }

    // </editor-folder> Reminders

    // <editor-folder> Cancel session

    public function generateNoSendingCancelMailData(): \Generator
    {
        yield 'Draft session' => [$this->createProviderParticipantSessionRole()];
        yield 'Canceled session' => [
            $this->createProviderParticipantSessionRole(
                ['status' => ProviderSession::STATUS_CANCELED]
            ),
        ];
        yield 'Finished session' => [
            $this->createProviderParticipantSessionRole(
                [
                    'dateStart' => '-3 hours',
                    'dateEnd' => '+2 minutes',
                ]
            ),
        ];
        yield 'Scheduled w/o notification date' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                ]
            ),
        ];
        yield 'Sent w/o notification date' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'lastConvocationSentSession' => new \DateTimeImmutable('-1 hours -30 minutes'),
                ]
            ),
        ];
        yield 'Scheduled w/ notification in future' => [
            $this->createProviderParticipantSessionRole(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateStart' => '+2 hours',
                    'notification' => new \DateTimeImmutable('+2 hours'),
                ]
            ),
        ];
    }

    /** @dataProvider generateNoSendingCancelMailData */
    public function testNoMailSentWithInvalidSessionStatus(ProviderParticipantSessionRole $participantSessionRole): void
    {
        $this->mailerService
            ->expects($this->never())
            ->method('sendTemplatedMail');
        $this->logger
            ->expects($this->never())
            ->method('info');
        $this->logger
            ->expects($this->never())
            ->method('error');

        $transactionalSubscriber = $this->createTransactionalSubscriber();
        $transactionalEvent = new TransactionalParticipantEmailRequestEvent($participantSessionRole);

        $transactionalSubscriber->onUnsubscribeRequest($transactionalEvent);
    }

    public function testSendEmailWithValidSessionStatus(): void
    {
        $this->mailerService
            ->expects($this->once())
            ->method('sendTemplatedMail');
        $this->logger
            ->expects($this->once())
            ->method('info');
        $this->logger
            ->expects($this->never())
            ->method('error');
        $this->translator
            ->expects($this->any())
            ->method('trans')
            ->willReturn('translated message');

        $transactionalSubscriber = $this->createTransactionalSubscriber();
        $participantSessionRole = $this->createProviderParticipantSessionRole(
            [
                'status' => ProviderSession::STATUS_SCHEDULED,
                'dateStart' => '+2 hours',
                'notification' => new \DateTimeImmutable('-2 hours'),
                'lastConvocationSentSession' => new \DateTimeImmutable('-1 hours -30 minutes'),
                'lastConvocationSentParticipant' => new \DateTimeImmutable('-1 hours -30 minutes'),
            ]
        );
        $participantSessionRole->setId(99)
                               ->getSession()->setRef('REFTEST000');
        $transactionalEvent = new TransactionalParticipantEmailRequestEvent($participantSessionRole);

        $transactionalSubscriber->onUnsubscribeRequest($transactionalEvent);
    }

    // </editor-folder> Cancel session
}
