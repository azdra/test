<?php

namespace App\Tests\Unit\Event;

use App\Event\MandrillMessageEvent;
use App\Event\MandrillMessageEvents;
use App\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class MandrillMessageEventTest extends TestCase
{
    public function testCreate(): void
    {
        $webhookData = [
            'event' => 'mandrill.message.deferral',
            'msg' => [
                'ts' => 1365109999,
                'subject' => 'This an example webhook message',
                'email' => 'example.webhook@mandrillapp.com',
                'sender' => 'example.sender@mandrillapp.com',
                'tags' => [
                    'webhook-example',
                ],
                'opens' => [
                ],
                'clicks' => [
                ],
                'state' => 'deferred',
                'metadata' => [
                    'user_id' => 111,
                ],
                '_id' => 'exampleaaaaaaaaaaaaaaaaaaaaaaaaa1',
                '_version' => 'exampleaaaaaaaaaaaaaaa',
                'smtp_events' => [
                    [
                        'destination_ip' => '127.0.0.1',
                        'diag' => '451 4.3.5 Temporarily unavailable, try again later.',
                        'source_ip' => '127.0.0.1',
                        'ts' => 1365111111,
                        'type' => 'deferred',
                        'size' => 0,
                    ],
                ],
            ],
            '_id' => 'exampleaaaaaaaaaaaaaaaaaaaaaaaaa1',
            'ts' => 1384954004,
        ];

        $event = MandrillMessageEvent::create($webhookData);

        $this->assertInstanceOf(MandrillMessageEvent::class, $event);
        $this->assertSame('mandrill.message.deferral', $event->getName());
        $this->assertSame('exampleaaaaaaaaaaaaaaaaaaaaaaaaa1', $event->getId());
        $this->assertSame([
            'ts' => 1365109999,
            'subject' => 'This an example webhook message',
            'email' => 'example.webhook@mandrillapp.com',
            'sender' => 'example.sender@mandrillapp.com',
            'tags' => [
                'webhook-example',
            ],
            'opens' => [
            ],
            'clicks' => [
            ],
            'state' => 'deferred',
            'metadata' => [
                'user_id' => 111,
            ],
            '_id' => 'exampleaaaaaaaaaaaaaaaaaaaaaaaaa1',
            '_version' => 'exampleaaaaaaaaaaaaaaa',
            'smtp_events' => [
                [
                    'destination_ip' => '127.0.0.1',
                    'diag' => '451 4.3.5 Temporarily unavailable, try again later.',
                    'source_ip' => '127.0.0.1',
                    'ts' => 1365111111,
                    'type' => 'deferred',
                    'size' => 0,
                ],
            ],
        ], $event->getMessage());

        $this->assertEquals(
            (new \DateTime())->setTimestamp('1384954004'),
            $event->getDate()
        );
    }

    public function testFailedCreate(): void
    {
        // Event name given is not in MandrillMessageEvents
        $webhookData = [
            'event' => 'deferral',
            'msg' => [
                'ts' => 1365109999,
                'subject' => 'This an example webhook message',
                'email' => 'example.webhook@mandrillapp.com',
                'sender' => 'example.sender@mandrillapp.com',
                'tags' => [
                    'webhook-example',
                ],
                'opens' => [
                ],
                'clicks' => [
                ],
                'state' => 'deferred',
                'metadata' => [
                    'user_id' => 111,
                ],
                '_id' => 'exampleaaaaaaaaaaaaaaaaaaaaaaaaa1',
                '_version' => 'exampleaaaaaaaaaaaaaaa',
                'smtp_events' => [
                    [
                        'destination_ip' => '127.0.0.1',
                        'diag' => '451 4.3.5 Temporarily unavailable, try again later.',
                        'source_ip' => '127.0.0.1',
                        'ts' => 1365111111,
                        'type' => 'deferred',
                        'size' => 0,
                    ],
                ],
            ],
            '_id' => 'exampleaaaaaaaaaaaaaaaaaaaaaaaaa1',
            'ts' => 1384954004,
        ];

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('The event name "deferral" is not valid. ("'.implode(', ', MandrillMessageEvents::ALL).'")');

        MandrillMessageEvent::create($webhookData);
    }
}
