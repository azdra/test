<?php

return [
    'APP_SECRET' => null,
    'DATABASE_PASSWORD' => null,
    'MAILER_DSN' => null,
    'MANDRILL_WEBHOOK_KEY' => null,
];
