const tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
const tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})

const popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
const popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
  return new bootstrap.Popover(popoverTriggerEl)
})


// Manipulation ( ouverture et fermeture ) de la user-nav
let userNav = document.querySelector('#user-nav'),
  userNavToggler = document.querySelectorAll('.user-nav-toggler');
let userNavToggle = () => userNav.classList.toggle('user-nav--is-open');
userNavToggler.forEach(function (i) {
  i.addEventListener('click', userNavToggle);
})


// Permet de switcher entre deux placeholders dans un champ input en
// activant/désactivant la checkbox associée à ce champ input
const switchTableResult = (btn, idfocus, str1, str2) => {
  let str;
  btn.checked ? str = str1 : str = str2;
  document.getElementById(idfocus).placeholder = str;
}


// Affichage du dropdown en écrivant 4 caractères dans la search top bar
const dropdownDisplayer = elt => {
  if (elt.value.length > 3) {
    elt.setAttribute('data-bs-toggle', 'dropdown');
    // const inputLeftPosition = elt.offsetLeft;
    // const inputWidth = elt.offsetWidth;

    globalSearchDropdown.classList.add('show');
    // globalSearchDropdown.style.left = `${inputLeftPosition - inputWidth}px`;
  } else {
    globalSearchDropdown.classList.remove('show');
    elt.removeAttribute('data-bs-toggle', 'dropdown');
  }
}

const globalSearch = document.getElementById('global-search');
const globalSearchDropdown = document.getElementById('global-search-dropdown');

if (globalSearch !== null) {
  globalSearch.addEventListener('click', function () { dropdownDisplayer(this); });
  globalSearch.addEventListener('keyup', function () { dropdownDisplayer(this); });
}



// Masque la dropdown si on clique en dehors de cette fenêtre
document.addEventListener("click", (e) => {
  let targetElement = e.target;

  do {
    if (targetElement == globalSearchDropdown) {
      return;
    }

    targetElement = targetElement.parentNode;
  } while (targetElement);

  globalSearchDropdown.classList.remove('show');
})


// Initialisation de la nav user tab
const triggerTabList = [].slice.call(document.querySelectorAll('#nav-tab-user a'))
triggerTabList.forEach(function (triggerEl) {
  const tabTrigger = new bootstrap.Tab(triggerEl)
  triggerEl.addEventListener('click', function (event) {
    event.preventDefault()
    tabTrigger.show()
  })
})


tinymce.init({
  selector: '#user-dedicated-support-rgpd-content',
  width: '100%',
  branding: false,
  fullpage_default_font_family: "'Nunito', sans-serif",

  menu: {
    file: {}
  }
})
