RM=rm -rf

.PHONY: list
list:
	@grep '^\.PHONY' Makefile

.PHONY: install
install: vendor/autoload.php yarn-install js-assets build

.PHONY: yarn-install
yarn-install:
	@bin/yarn install
	@bin/yarn dev

.PHONY: js-assets
js-assets:
	@bin/console bazinga:js-translation:dump --format=js --merge-domains public/translations
	@bin/console fos:js-routing:dump --format=json --target=public/js/fos_js_routes.json
	@bin/yarn run font:build

vendor/autoload.php: composer.lock
	@bin/composer install

.PHONY: public/bundles
public/bundles: composer.lock
	@bin/console assets:install --symlink

.PHONY: build
build: .build

.build: $(shell find docker/* -type f -name Dockerfile)
	@docker compose build
	> $@

.PHONY: push
push: build
	@docker compose push

.PHONY: start
start: build
	@docker compose up -d --remove-orphans

.PHONY: stop
stop:
	@docker compose down --remove-orphans

.PHONY: restart
restart:
	$(MAKE) stop
	$(MAKE) start

.PHONY: fix-php-cs
fix-php-cs:
	@bin/php-cs-fixer fix --diff --config .php-cs-fixer.dist.php

.PHONY: eslint
eslint:
	@bin/yarn run eslint --fix 'assets/vue/**/*' 'assets/js/**/*' --ignore-pattern 'assets/js/**/*.min.js'
.PHONY: static-analysis
static-analysis:
	@bin/psalm

.PHONY: lint-yaml
lint-yaml:
	@bin/console lint:yaml config/ --parse-tags

.PHONY: db
db: build
	@bin/console doctrine:database:drop --if-exists --force
	@bin/console doctrine:database:create
	@bin/console doctrine:migrations:migrate latest --allow-no-migration --no-interaction

.PHONY: fixtures
fixtures: db
	@bin/console hautelook:fixtures:load --no-interaction --append

.PHONY: functional-tests
functional-tests:
	@docker compose run -e APP_DEBUG=true -e APP_ENV=behat --rm packager sh -c " \
	    php bin/console.php doctrine:database:drop --if-exists --force && \
        php bin/console.php doctrine:database:create && \
        php bin/console.php doctrine:migrations:migrate latest --allow-no-migration --no-interaction && \
        php bin/console cache:clear && \
	    php bin/console.php hautelook:fixtures:load --no-interaction --append && \
	    php -d memory_limit=1G vendor/bin/behat \
	"

.PHONY: unit-tests
unit-tests:
	@docker compose run -e APP_DEBUG=true -e APP_ENV=test --rm packager php vendor/bin/phpunit --testsuite=unit

.PHONY: path-functional-tests
path-functional-tests:
	@docker compose run -e APP_DEBUG=true -e APP_ENV=test --rm packager sh -c " \
        php bin/console.php doctrine:database:drop --if-exists --force && \
        php bin/console.php doctrine:database:create && \
        php bin/console.php doctrine:migrations:migrate latest --allow-no-migration --no-interaction && \
        php bin/console cache:clear && \
        php bin/console.php hautelook:fixtures:load --no-interaction --append && \
	    php vendor/bin/phpunit --testsuite=path-functional \
	"

.PHONY: unit-tests-coverage
unit-tests-coverage: build
	@docker compose run -e APP_DEBUG=true -e APP_ENV=test --rm packager php vendor/bin/phpunit --testsuite=unit --coverage-html var/coverage

.PHONY: clean
clean:
	@docker compose run --rm packager sh -c "$(RM) vendor public/bundles .build public/build node_modules"

.PHONY: all-linter
all-linter: lint-yaml eslint fix-php-cs static-analysis

.PHONY: ci
ci: all-linter functional-tests unit-tests path-functional-tests
