#!/bin/bash

# If args are given, use them as command (backward compatibility)
if [ $# -ne 0 ]
then
  set -e
  exec "$@"
fi

if [ "$DB_MIGRATION" = "true" ]
then
  # Do db migration and exit
  php bin/console.php cache:clear
  php bin/console.php doctrine:migrations:migrate latest --allow-no-migration --no-interaction
  exit $?

elif [ "$DB_MIGRATION" = "false" ]
then
  # Start php-fpm
  php bin/console.php cache:clear
  php-fpm

else
  # Be backward compatible if DB_MIGRATION is not defined
  echo "DB_MIGRATION is not defined"

  php bin/console.php cache:clear
  chmod -R a+w var
  php bin/console.php doctrine:migrations:migrate latest --allow-no-migration --no-interaction
  php-fpm

fi
