const Encore = require('@symfony/webpack-encore')

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
  Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev')
}

Encore
// directory where compiled assets will be stored
  .setOutputPath('public/build/')
// public path used by the web server to access the output path
  .setPublicPath('/build')
// only needed for CDN's or sub-directory deploy
// .setManifestKeyPrefix('build/')

  /*
   * ENTRY CONFIG
   *
   * Each entry will result in one JavaScript file (e.g. app.js)
   * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
   */
  .addEntry('app', './assets/js/app.js')
  .addEntry('base', './assets/js/base.js')
  .addEntry('base-self-subscription-hub', './assets/js/base-self-subscription-hub.js')
  .addEntry('switch-account-select', './assets/js/switchAccountSelect.js')
  .addEntry('base-wizard', './assets/js/base-wizard.js')
  .addEntry('password-reset', './assets/js/password-reset.js')
  .addEntry('update-password', './assets/js/update-password.js')
  .addEntry('users-list', './assets/js/users-list.js')
  .addEntry('clients-list', './assets/js/clients-list.js')
  .addEntry('client-manager-list', './assets/js/client-manager-list.js')
  .addEntry('client-form-tab', './assets/js/client-form-tab.js')
  .addEntry('client-billing-tab', './assets/js/client/client-billing.js')
  .addEntry('client-tailored-tab', './assets/js/client/client-tailored.js')
  .addEntry('login', './assets/js/login.js')
  .addEntry('user-avatar', './assets/js/user-avatar.js')
  .addEntry('users-participants-list', './assets/js/users-participants-list.js')
  .addEntry('groups-list', './assets/js/groups-list.js')
  .addEntry('group-edit', './assets/js/group-edit.js')
  .addEntry('users-helper-list', './assets/js/customer-service/users-helper-list.js')
  .addEntry('help-need', './assets/js/customer-service/help-need.js')
  .addEntry('help-task-details', './assets/js/customer-service/help-task-details.js')
  .addEntry('evaluation-performance-logbook', './assets/js/customer-service/evaluation-performance-logbook.js')
  .addEntry('evaluation-performance-animator', './assets/js/customer-service/evaluation-performance-animator.js')
  .addEntry('licenses-available', './assets/js/licenses-available.js')
  .addEntry('evaluation-performance-animator-answer-list', './assets/js/customer-service/evaluation-performance-animator-answer-list.js')
  .addEntry('log-book-answer-list', './assets/js/customer-service/log-book-answer-list.js')
  .addEntry('tags-input', './assets/js/tags-input.js')
  .addEntry('collection-type', './assets/js/collection-type.js')
  .addEntry('client-convocation-list', './assets/js/client-convocation-list.js')
  .addEntry('client-evaluation-mail-list', './assets/js/client-evaluation-mail-list.js')
  .addEntry('configuration-holder-form', './assets/js/configuration-holder-form.js')
  .addEntry('tinymce-input', './assets/js/tinymce-input.js')
  .addEntry('bounces-list', './assets/js/bounces-list.js')
  .addEntry('styles-pdf', './assets/scss/styles_pdf.scss')
  .addEntry('styles-session-pdf', './assets/scss/styles_session_pdf.scss')
  .addEntry('create-session-wizard', './assets/js/create-session-wizard.js')
  .addEntry('webex-licence-form', './assets/js/webex-licence-form.js')
  .addEntry('webex-rest-licence-form', './assets/js/webex-rest-licence-form.js')
  .addEntry('adobe-licence-form', './assets/js/adobe-licence-form.js')
  .addEntry('activities', './assets/js/activities.js')
  .addEntry('unsubscribe-page', './assets/js/unsubscribe-page.js')
  .addEntry('statistics', './assets/js/statistics/statistics.js')
  .addEntry('statistics-evaluation', './assets/js/statistics/statistics-evaluation.js')
  .addEntry('evaluations-form', './assets/js/evaluations-form.js')
  .addEntry('presence-values-form', './assets/js/presence-values-form.js')
  .addEntry('preferences-form', './assets/js/preferences-form.js')
  .addEntry('participant-form', './assets/js/participant-form.js')
  .addEntry('participant-form-evaluation-session', './assets/js/participant-form-evaluation-session.js')
  .addEntry('main-global-searchbar', './assets/js/layout/main-global-search-bar.js')
  .addEntry('widget-news', '/assets/js/widget-news.js')
  .addEntry('dashboard-current-session', '/assets/js/dashboard-current-session.js')
  .addEntry('preferences-profil-form', './assets/js/preferences-profil-form.js')
  .addEntry('client-tab-services', './assets/js/client-tab-services.js')
  .addEntry('sessions-tabs', './assets/js/session/sessions-tabs.js')
  .addEntry('session-header', './assets/js/session/session-header.js')
  .addEntry('sessions-viewer', './assets/js/session/sessions-viewer.js')
  .addEntry('session-documents-attendee-list', './assets/js/session/session-documents-attendee-list.js')
  .addEntry('q-r-code-page', './assets/js/session/q-r-code-page.js')
  .addEntry('session-access-request-password-tooltip', './assets/js/session/session-access-request-password-tooltip.js')
  .addEntry('microsoft-teams-licence-form', './assets/js/microsoft-teams-licence-form.js')
  .addEntry('microsoft-teams-event-licence-form', './assets/js/microsoft-teams-event-licence-form.js')
  .addEntry('white-licence-form', './assets/js/white-licence-form.js')
  .addEntry('personal-calendar-help-task', './assets/js/customer-service/personal-calendar-help-task.js')
  .addEntry('personal-calendar-availability-user', './assets/js/customer-service/personal-calendar-availability-user.js')
  .addEntry('personal-calendar-select', './assets/js/customer-service/personal-calendar-select.js')
  .addEntry('module-viewer', './assets/js/self-subscription/module/module-viewer.js')
  .addEntry('module-editor', './assets/js/self-subscription/module/module-editor.js')
  .addEntry('module-edit-subject', './assets/js/self-subscription/module/module-edit-subject.js')
  .addEntry('ss-login-register', './assets/js/self-subscription/hub/login-register.js')
  .addEntry('ss-home', './assets/js/self-subscription/hub/home.js')
  .addEntry('evaluation-lobby', './assets/js/evaluation-lobby.js')
  .addEntry('evaluation-session-lobby', './assets/js/evaluation-session-lobby.js')
  .addEntry('link-export-sessions-viewer', './assets/js/customer-service/link-export-sessions-viewer.js')
  .addEntry('customer-service-dashboard', './assets/js/customer-service/dashboard.js')
  .addEntry('widget-news-list', './assets/js/widget-news/widget-news-list.js')
  .addEntry('widget-news-create-or-edit', './assets/js/widget-news/widget-news-create-or-edit.js')
  .addEntry('self-subscription-register', './assets/js/self-subscription/hub/register.js')
  .addEntry('self-subscription-login', './assets/js/self-subscription/hub/login.js')
  .addEntry('self-subscription-reset-password', './assets/js/self-subscription/hub/reset-password.js')
  .addEntry('billing-claimant-list', './assets/js/customer-service/billing-claimant-list.js')
  .addEntry('registration-page-index', './assets/js/registration-page/index.js')
  .addEntry('registration-page-template-editor', './assets/js/registration-page/template/template-editor.js')
  .addEntry('base-registration-page', './assets/js/registration-page/base-registration-page.js')
  .addEntry('registration-page', './assets/js/registration-page/registration-page.js')
  .addEntry('session-evaluation-synthesis', './assets/js/session/session-evaluation-synthesis.js')
  .addEntry('chatgpt-openai-home-page', './assets/js/chatgpt-openai-home-page.js')
  .addEntry('on-hold-provider-session-viewer', './assets/js/on-hold-provider-session/on-hold-provider-session-viewer.js')

// When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
  .splitEntryChunks()

// will require an extra script tag for runtime.js
// but, you probably want this, unless you're building a single-page app
  .enableSingleRuntimeChunk()

/*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
  .cleanupOutputBeforeBuild()
  .enableBuildNotifications()
  .enableSourceMaps(!Encore.isProduction())
// enables hashed filenames (e.g. app.abc123.css)
  .enableVersioning(Encore.isProduction())

  .configureBabel((config) => {
    config.plugins.push('@babel/plugin-proposal-class-properties')
  })

// enables @babel/preset-env polyfills
  .configureBabelPresetEnv((config) => {
    config.useBuiltIns = 'usage'
    config.corejs = 3
  })

// enables Sass/SCSS support
  .enableSassLoader()
  .enableVueLoader()

  .copyFiles({
    from: './assets/static',
    to: 'static/[path][name].[ext]'
  })

  .copyFiles({
    from: './assets/js/tinymce/langs',
    to: 'langs/[path]/[name].[ext]'
  })

if (Encore.isProduction()) {
  Encore.enablePostCssLoader()
}

module.exports = Encore.getWebpackConfig()
