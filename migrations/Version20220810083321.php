<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220810083321 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client ADD licenses_available_enabled TINYINT(1) NOT NULL, ADD licenses_available_threshold_middle INT DEFAULT NULL, ADD licenses_available_threshold_min INT DEFAULT NULL, ADD licenses_available_licenses_report_recipients JSON DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client DROP licenses_available_enabled, DROP licenses_available_threshold_middle, DROP licenses_available_threshold_min, DROP licenses_available_licenses_report_recipients');
    }
}
