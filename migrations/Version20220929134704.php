<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220929134704 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_session ADD client_id INT DEFAULT NULL');
        $this->addSql('
            UPDATE provider_session ps SET client_id = (
                SELECT c.id 
                FROM client c
                JOIN abstract_provider_configuration_holder ch ON ch.client_id = c.id
                WHERE ch.id = ps.abstract_provider_configuration_holder_id
            )
        ');
        $this->addSql('ALTER TABLE provider_session ADD CONSTRAINT FK_201C777319EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('CREATE INDEX IDX_201C777319EB6921 ON provider_session (client_id)');
        $this->addSql('DROP INDEX providersession_unicity_ref_and_configurationholder ON provider_session');
        $this->addSql('CREATE UNIQUE INDEX providersession_unicity_ref_and_client ON provider_session (ref, client_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_session DROP FOREIGN KEY FK_201C777319EB6921');
        $this->addSql('DROP INDEX IDX_201C777319EB6921 ON provider_session');
        $this->addSql('ALTER TABLE provider_session DROP client_id');
        $this->addSql('DROP INDEX providersession_unicity_ref_and_client ON provider_session');
        $this->addSql('CREATE UNIQUE INDEX providersession_unicity_ref_and_configurationholder ON provider_session (ref, abstract_provider_configuration_holder_id)');
    }
}
