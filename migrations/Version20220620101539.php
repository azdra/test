<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220620101539 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE help_task ADD start_real DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD end_real DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD duration_real INT DEFAULT NULL');
        $this->addSql('ALTER TABLE help_need DROP start_real, DROP end_real, DROP duration_real');
        $this->addSql('ALTER TABLE provider_session ADD evaluation_performance_sent DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE help_need ADD start_real DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD end_real DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD duration_real INT DEFAULT NULL');
        $this->addSql('ALTER TABLE help_task DROP start_real, DROP end_real, DROP duration_real');
        $this->addSql('ALTER TABLE provider_session DROP evaluation_performance_sent');
    }
}
