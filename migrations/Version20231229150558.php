<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231229150558 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE evaluation_provider_session (id INT AUTO_INCREMENT NOT NULL, provider_session_id INT DEFAULT NULL, evaluation_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', evaluation_type VARCHAR(255) NOT NULL, send_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', INDEX IDX_AB3726F74489A067 (provider_session_id), INDEX IDX_AB3726F7456C5646 (evaluation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evaluation_session_participant_answer (id INT AUTO_INCREMENT NOT NULL, provider_participant_session_role_id INT DEFAULT NULL, evaluation_provider_session_id INT DEFAULT NULL, reply_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', answer JSON NOT NULL, INDEX IDX_25CB4109F6CE1A0 (provider_participant_session_role_id), INDEX IDX_25CB4109ADA986E5 (evaluation_provider_session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE evaluation_provider_session ADD CONSTRAINT FK_AB3726F74489A067 FOREIGN KEY (provider_session_id) REFERENCES provider_session (id)');
        $this->addSql('ALTER TABLE evaluation_provider_session ADD CONSTRAINT FK_AB3726F7456C5646 FOREIGN KEY (evaluation_id) REFERENCES evaluation (id)');
        $this->addSql('ALTER TABLE evaluation_session_participant_answer ADD CONSTRAINT FK_25CB4109F6CE1A0 FOREIGN KEY (provider_participant_session_role_id) REFERENCES provider_participant_session_role (id)');
        $this->addSql('ALTER TABLE evaluation_session_participant_answer ADD CONSTRAINT FK_25CB4109ADA986E5 FOREIGN KEY (evaluation_provider_session_id) REFERENCES evaluation_provider_session (id)');
        $this->addSql('ALTER TABLE module CHANGE description description VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evaluation_session_participant_answer DROP FOREIGN KEY FK_25CB4109ADA986E5');
        $this->addSql('DROP TABLE evaluation_provider_session');
        $this->addSql('DROP TABLE evaluation_session_participant_answer');
    }
}
