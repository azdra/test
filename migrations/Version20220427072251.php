<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220427072251 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client ADD billing_mode INT DEFAULT 0 NOT NULL, ADD allowed_credit INT DEFAULT 0 NOT NULL, ADD current_session_count INT DEFAULT 0 NOT NULL');
        $this->addSql('
            UPDATE client c SET current_session_count = (
                SELECT COUNT(*) FROM provider_session s 
                JOIN abstract_provider_configuration_holder apch ON s.abstract_provider_configuration_holder_id = apch.id
                WHERE apch.client_id = c.id AND s.session_test = false
            )
        ');
        $this->addSql('CREATE TABLE credit_allocation (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, allowed_credit INT DEFAULT 0 NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', INDEX IDX_AF3C989419EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE credit_allocation ADD CONSTRAINT FK_AF3C989419EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client DROP billing_mode, DROP allowed_credit, DROP current_session_count');
        $this->addSql('DROP TABLE credit_allocation');
    }
}
