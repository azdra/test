<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230424135026 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE microsoft_teams_telephony_profile (id INT NOT NULL, linked_principal_id INT DEFAULT NULL, microsoft_teams_default_phone_number VARCHAR(255) DEFAULT NULL, code_animator VARCHAR(255) DEFAULT NULL, code_participant VARCHAR(255) DEFAULT NULL, audio_conferencing JSON DEFAULT NULL, INDEX IDX_FFC67B79E97135B4 (linked_principal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE microsoft_teams_telephony_profile ADD CONSTRAINT FK_FFC67B79E97135B4 FOREIGN KEY (linked_principal_id) REFERENCES microsoft_teams_session (id)');
        $this->addSql('ALTER TABLE microsoft_teams_telephony_profile ADD CONSTRAINT FK_FFC67B79BF396750 FOREIGN KEY (id) REFERENCES abstract_provider_audio (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE microsoft_teams_telephony_profile');
    }
}
