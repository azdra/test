<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231110081947 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE open_ai (id INT AUTO_INCREMENT NOT NULL, session_id VARCHAR(255) NOT NULL, total_granted DOUBLE PRECISION DEFAULT \'0\' NOT NULL, total_used DOUBLE PRECISION DEFAULT \'0\' NOT NULL, total_available DOUBLE PRECISION DEFAULT \'0\' NOT NULL, total_paid_available DOUBLE PRECISION DEFAULT \'0\' NOT NULL, credit_threshold DOUBLE PRECISION DEFAULT \'5\' NOT NULL, billing JSON DEFAULT NULL, error VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE open_ai');
    }
}
