<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230725120147 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE video_replay (id INT AUTO_INCREMENT NOT NULL, module_id INT DEFAULT NULL, platform VARCHAR(255) DEFAULT NULL, url_key VARCHAR(255) DEFAULT NULL, published TINYINT(1) DEFAULT 1, photo_replay VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, title VARCHAR(255) DEFAULT \'The trainings\', published_date DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_E160543FAFC2B591 (module_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE video_replay ADD CONSTRAINT FK_E160543FAFC2B591 FOREIGN KEY (module_id) REFERENCES module (id)');
        $this->addSql('ALTER TABLE module ADD personalization_faq_title LONGTEXT DEFAULT NULL, ADD personalization_replay_description LONGTEXT DEFAULT NULL, ADD personalization_replay_title VARCHAR(255) DEFAULT \'Replay\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE video_replay');
        $this->addSql('ALTER TABLE module DROP personalization_faq_title, DROP personalization_replay_description, DROP personalization_replay_title');
    }
}
