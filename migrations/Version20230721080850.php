<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230721080850 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module ADD banner VARCHAR(255) DEFAULT NULL, ADD personalization_faq_description LONGTEXT DEFAULT NULL, ADD personalization_training_description LONGTEXT DEFAULT NULL, ADD personalization_training_title VARCHAR(255) DEFAULT \'The trainings\', CHANGE logo logo VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module DROP banner, DROP personalization_faq_description, DROP personalization_training_description, DROP personalization_training_title, CHANGE logo logo VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
