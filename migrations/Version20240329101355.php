<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240329101355 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE registration_page_template_black_list_entry (id INT AUTO_INCREMENT NOT NULL, registration_page_template_id INT DEFAULT NULL, prohibited VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_FECCD5C4DD5986FD (registration_page_template_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE registration_page_template_white_list_entry (id INT AUTO_INCREMENT NOT NULL, registration_page_template_id INT DEFAULT NULL, allowed VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_FBD23E85DD5986FD (registration_page_template_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE registration_page_template_black_list_entry ADD CONSTRAINT FK_FECCD5C4DD5986FD FOREIGN KEY (registration_page_template_id) REFERENCES registration_page_template (id)');
        $this->addSql('ALTER TABLE registration_page_template_white_list_entry ADD CONSTRAINT FK_FBD23E85DD5986FD FOREIGN KEY (registration_page_template_id) REFERENCES registration_page_template (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE registration_page_template_black_list_entry DROP FOREIGN KEY FK_FECCD5C4DD5986FD');
        $this->addSql('ALTER TABLE registration_page_template_white_list_entry DROP FOREIGN KEY FK_FBD23E85DD5986FD');
        $this->addSql('DROP TABLE registration_page_template_black_list_entry');
        $this->addSql('DROP TABLE registration_page_template_white_list_entry');
    }
}
