<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230913100633 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE microsoft_teams_event_configuration_holder (id INT NOT NULL, token_endpoint VARCHAR(255) NOT NULL, tenant_id VARCHAR(255) NOT NULL, scope VARCHAR(255) NOT NULL, grant_type VARCHAR(255) NOT NULL, organizer_email VARCHAR(255) NOT NULL, organizer_unique_identifier VARCHAR(255) NOT NULL, client_id VARCHAR(255) NOT NULL, client_secret VARCHAR(255) NOT NULL, safety_time INT NOT NULL, licences JSON NOT NULL, automatic_licensing TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE microsoft_teams_event_participant (id INT NOT NULL, contact_identifier INT DEFAULT NULL, active TINYINT(1) DEFAULT 1 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE microsoft_teams_event_participant_microsoft_teams_event_session (microsoft_teams_event_participant_id INT NOT NULL, microsoft_teams_event_session_id INT NOT NULL, INDEX IDX_DCD5E1E96728DE75 (microsoft_teams_event_participant_id), INDEX IDX_DCD5E1E9978D8713 (microsoft_teams_event_session_id), PRIMARY KEY(microsoft_teams_event_participant_id, microsoft_teams_event_session_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE microsoft_teams_event_session (id INT NOT NULL, join_url LONGTEXT NOT NULL, event_identifier LONGTEXT NOT NULL, online_meeting_identifier LONGTEXT NOT NULL, licence VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE microsoft_teams_event_telephony_profile (id INT NOT NULL, linked_principal_id INT DEFAULT NULL, microsoft_teams_event_default_phone_number VARCHAR(255) DEFAULT NULL, code_animator VARCHAR(255) DEFAULT NULL, code_participant VARCHAR(255) DEFAULT NULL, audio_conferencing JSON DEFAULT NULL, INDEX IDX_66EF8C60E97135B4 (linked_principal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE microsoft_teams_event_configuration_holder ADD CONSTRAINT FK_F47604FBF396750 FOREIGN KEY (id) REFERENCES abstract_provider_configuration_holder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE microsoft_teams_event_participant ADD CONSTRAINT FK_45BB57E1BF396750 FOREIGN KEY (id) REFERENCES provider_participant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE microsoft_teams_event_participant_microsoft_teams_event_session ADD CONSTRAINT FK_DCD5E1E96728DE75 FOREIGN KEY (microsoft_teams_event_participant_id) REFERENCES microsoft_teams_event_participant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE microsoft_teams_event_participant_microsoft_teams_event_session ADD CONSTRAINT FK_DCD5E1E9978D8713 FOREIGN KEY (microsoft_teams_event_session_id) REFERENCES microsoft_teams_event_session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE microsoft_teams_event_session ADD CONSTRAINT FK_9118949EBF396750 FOREIGN KEY (id) REFERENCES provider_session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE microsoft_teams_event_telephony_profile ADD CONSTRAINT FK_66EF8C60E97135B4 FOREIGN KEY (linked_principal_id) REFERENCES microsoft_teams_event_session (id)');
        $this->addSql('ALTER TABLE microsoft_teams_event_telephony_profile ADD CONSTRAINT FK_66EF8C60BF396750 FOREIGN KEY (id) REFERENCES abstract_provider_audio (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE microsoft_teams_event_participant_microsoft_teams_event_session DROP FOREIGN KEY FK_DCD5E1E96728DE75');
        $this->addSql('ALTER TABLE microsoft_teams_event_participant_microsoft_teams_event_session DROP FOREIGN KEY FK_DCD5E1E9978D8713');
        $this->addSql('ALTER TABLE microsoft_teams_event_telephony_profile DROP FOREIGN KEY FK_66EF8C60E97135B4');
        $this->addSql('DROP TABLE microsoft_teams_event_configuration_holder');
        $this->addSql('DROP TABLE microsoft_teams_event_participant');
        $this->addSql('DROP TABLE microsoft_teams_event_participant_microsoft_teams_event_session');
        $this->addSql('DROP TABLE microsoft_teams_event_session');
        $this->addSql('DROP TABLE microsoft_teams_event_telephony_profile');
    }
}
