<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220407142753 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE session_convocation_attachment (id INT AUTO_INCREMENT NOT NULL, session_id INT DEFAULT NULL, original_name VARCHAR(255) NOT NULL, file_name VARCHAR(255) NOT NULL, token VARCHAR(255) NOT NULL, INDEX IDX_2623C0AF613FECDF (session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE session_convocation_attachment ADD CONSTRAINT FK_2623C0AF613FECDF FOREIGN KEY (session_id) REFERENCES provider_session (id)');
        $this->addSql('ALTER TABLE provider_session DROP attachment');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE session_convocation_attachment');
        $this->addSql('ALTER TABLE provider_session ADD attachment VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
