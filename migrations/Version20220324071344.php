<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220324071344 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client ADD services_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455AEF5A6C1 FOREIGN KEY (services_id) REFERENCES client_services (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C7440455AEF5A6C1 ON client (services_id)');
        $this->addSql('ALTER TABLE client_services DROP FOREIGN KEY FK_5DD4D1AC19EB6921');
        $this->addSql('DROP INDEX UNIQ_5DD4D1AC19EB6921 ON client_services');
        $this->addSql('ALTER TABLE client_services ADD standard_hours_start_time_lh DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', ADD standard_hours_end_time_lh DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', ADD outside_standard_hours_start_time_lh DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', ADD outside_standard_hours_end_time_lh DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', DROP client_id, CHANGE commitment commitment INT DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_session ADD support_type INT NOT NULL, ADD standard_hours TINYINT(1) NOT NULL, ADD outside_standard_hours TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C7440455AEF5A6C1');
        $this->addSql('DROP INDEX UNIQ_C7440455AEF5A6C1 ON client');
        $this->addSql('ALTER TABLE client DROP services_id');
        $this->addSql('ALTER TABLE client_services ADD CONSTRAINT FK_5DD4D1AC19EB6921 FOREIGN KEY (client_id) REFERENCES client (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5DD4D1AC19EB6921 ON client_services (client_id)');
        $this->addSql('ALTER TABLE client_services DROP standard_hours_start_time_lh, DROP standard_hours_end_time_lh, DROP outside_standard_hours_start_time_lh, DROP outside_standard_hours_end_time_lh');
        $this->addSql('ALTER TABLE provider_session DROP standard_hours, DROP outside_standard_hours');
    }
}
