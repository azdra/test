<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230130154312 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_participant ADD allow_to_send_canceled_mail TINYINT(1) DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE provider_participant_session_role DROP allow_to_send_canceled_mail');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_participant_session_role ADD allow_to_send_canceled_mail TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE provider_session CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE ref ref VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE business_number business_number VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE information information LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE language language VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE origin origin VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE date_of_sending_reminders date_of_sending_reminders LONGTEXT NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:object)\', CHANGE connexion_type_session connexion_type_session VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE personalized_content personalized_content LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE location location LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description2 description2 LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE replay_video_platform replay_video_platform VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE replay_url_key replay_url_key VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE discr discr VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
