<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220801090945 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE renault_import_session_temp (id INT AUTO_INCREMENT NOT NULL, num_row BIGINT NOT NULL, name VARCHAR(255) DEFAULT NULL, model VARCHAR(255) DEFAULT NULL, reference_live_session VARCHAR(255) DEFAULT NULL, reference_renault VARCHAR(255) DEFAULT NULL, result_type INT DEFAULT 1 NOT NULL, result_text LONGTEXT DEFAULT NULL, date_start DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', date_end DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', nbr_days INT DEFAULT 1 NOT NULL, num_day INT DEFAULT 1 NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', imported_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', role INT DEFAULT 2 NOT NULL, animator_name VARCHAR(255) DEFAULT NULL, animator_forname VARCHAR(255) DEFAULT NULL, animator_email VARCHAR(255) DEFAULT NULL, trainee_name VARCHAR(255) DEFAULT NULL, trainee_forname VARCHAR(255) DEFAULT NULL, trainee_email VARCHAR(255) DEFAULT NULL, trainee_ipn VARCHAR(255) DEFAULT NULL, employee_id VARCHAR(255) DEFAULT NULL, status_session VARCHAR(255) DEFAULT NULL, status_trainee VARCHAR(255) DEFAULT NULL, result_session VARCHAR(255) DEFAULT NULL, result_trainee VARCHAR(255) DEFAULT NULL, result_animator VARCHAR(255) DEFAULT NULL, url_connexion_trainee LONGTEXT DEFAULT NULL, url_session LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE renault_import_session_temp_errors (id INT AUTO_INCREMENT NOT NULL, num_row BIGINT NOT NULL, name VARCHAR(255) DEFAULT NULL, reference_renault VARCHAR(255) DEFAULT NULL, status_formation VARCHAR(255) DEFAULT NULL, row_ipn VARCHAR(255) DEFAULT NULL, row_employee_id VARCHAR(255) DEFAULT NULL, row_email_trainee VARCHAR(255) DEFAULT NULL, errors LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE renault_import_session_temp');
        $this->addSql('DROP TABLE renault_import_session_temp_errors');
    }
}
