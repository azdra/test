<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230705134926 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE webex_rest_configuration_holder (id INT NOT NULL, site_name VARCHAR(255) NOT NULL, client_id VARCHAR(255) NOT NULL, client_secret VARCHAR(255) NOT NULL, client_refresh_token VARCHAR(255) NOT NULL, client_access_token VARCHAR(255) NOT NULL, default_options JSON NOT NULL, licences JSON NOT NULL, automatic_licensing TINYINT(1) NOT NULL, safety_time INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE webex_rest_meeting (id INT NOT NULL, meeting_password VARCHAR(255) DEFAULT NULL, maximum_user_count INT DEFAULT NULL, meeting_rest_id VARCHAR(255) NOT NULL, meeting_rest_number BIGINT NOT NULL, meeting_rest_web_link VARCHAR(255) DEFAULT NULL, meeting_rest_host_email VARCHAR(255) DEFAULT NULL, meeting_rest_scheduled_type VARCHAR(255) DEFAULT NULL, open_time INT DEFAULT 900 NOT NULL, host_icalendar_url LONGTEXT DEFAULT NULL, attendee_icalendar_url LONGTEXT DEFAULT NULL, licence VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE webex_rest_meeting_telephony_profile (id INT NOT NULL, linked_principal_id INT DEFAULT NULL, webex_rest_meeting_default_phone_number VARCHAR(255) DEFAULT NULL, code_animator VARCHAR(255) DEFAULT NULL, code_participant VARCHAR(255) DEFAULT NULL, INDEX IDX_2AB46577E97135B4 (linked_principal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE webex_rest_participant (id INT NOT NULL, invitee_id VARCHAR(255) DEFAULT NULL, active TINYINT(1) DEFAULT 1 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE webex_rest_configuration_holder ADD CONSTRAINT FK_20A2040EBF396750 FOREIGN KEY (id) REFERENCES abstract_provider_configuration_holder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webex_rest_meeting ADD CONSTRAINT FK_4D3EA026BF396750 FOREIGN KEY (id) REFERENCES provider_session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webex_rest_meeting_telephony_profile ADD CONSTRAINT FK_2AB46577E97135B4 FOREIGN KEY (linked_principal_id) REFERENCES webex_rest_meeting (id)');
        $this->addSql('ALTER TABLE webex_rest_meeting_telephony_profile ADD CONSTRAINT FK_2AB46577BF396750 FOREIGN KEY (id) REFERENCES abstract_provider_audio (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webex_rest_participant ADD CONSTRAINT FK_1D770DF8BF396750 FOREIGN KEY (id) REFERENCES provider_participant (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE webex_rest_meeting_telephony_profile DROP FOREIGN KEY FK_2AB46577E97135B4');
        $this->addSql('DROP TABLE webex_rest_configuration_holder');
        $this->addSql('DROP TABLE webex_rest_meeting');
        $this->addSql('DROP TABLE webex_rest_meeting_telephony_profile');
        $this->addSql('DROP TABLE webex_rest_participant');
    }
}
