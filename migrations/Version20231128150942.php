<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231128150942 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE replay_category DROP FOREIGN KEY FK_B3587B3619EB6921');
        $this->addSql('DROP INDEX IDX_B3587B3619EB6921 ON replay_category');
        $this->addSql('ALTER TABLE replay_category DROP catalog_restricted');
        $this->addSql('ALTER TABLE replay_category CHANGE client_id module_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE replay_category ADD CONSTRAINT FK_B3587B36AFC2B591 FOREIGN KEY (module_id) REFERENCES module (id)');
        $this->addSql('CREATE INDEX IDX_B3587B36AFC2B591 ON replay_category (module_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE replay_category DROP FOREIGN KEY FK_B3587B36AFC2B591');
        $this->addSql('DROP INDEX IDX_B3587B36AFC2B591 ON replay_category');
        $this->addSql('ALTER TABLE replay_category CHANGE category_name category_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE module_id client_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE replay_category ADD CONSTRAINT FK_B3587B3619EB6921 FOREIGN KEY (client_id) REFERENCES client (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_B3587B3619EB6921 ON replay_category (client_id)');
        $this->addSql('ALTER TABLE replay_category ADD catalog_restricted JSON NOT NULL');
    }
}
