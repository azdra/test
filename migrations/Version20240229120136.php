<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240229120136 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE abstract_provider_configuration_holder ADD percentage_attendance INT NOT NULL DEFAULT 70');
        $this->addSql('ALTER TABLE provider_session ADD percentage_attendance INT NOT NULL DEFAULT 70');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE abstract_provider_configuration_holder DROP percentage_attendance');
        $this->addSql('ALTER TABLE provider_session DROP percentage_attendance');
    }
}
