<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230222083120 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module ADD company_label VARCHAR(255) DEFAULT \'Entreprise\' NOT NULL, ADD company_placeholder VARCHAR(255) DEFAULT \'Entreprise\' NOT NULL, ADD email_label VARCHAR(255) DEFAULT \'Adresse mail\' NOT NULL, ADD email_placeholder VARCHAR(255) DEFAULT \'Adresse mail\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module DROP company_label, DROP company_placeholder, DROP email_label, DROP email_placeholder');
    }
}
