<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220324104831 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE webex_configuration_holder ADD automatic_licensing TINYINT(1) NOT NULL');
        $this->addSql('UPDATE webex_configuration_holder SET automatic_licensing = FALSE');
        $this->addSql('ALTER TABLE webex_configuration_holder ADD safety_time INT NOT NULL');
        $this->addSql('UPDATE webex_configuration_holder SET safety_time = 0');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE webex_configuration_holder DROP automatic_licensing');
        $this->addSql('ALTER TABLE webex_configuration_holder DROP safety_time');
    }
}
