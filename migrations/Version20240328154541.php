<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240328154541 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE evaluation_modal_mail (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', evaluation_type VARCHAR(255) NOT NULL, subject_mail VARCHAR(255) DEFAULT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_6239E34E19EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE evaluation_modal_mail ADD CONSTRAINT FK_6239E34E19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE abstract_provider_configuration_holder CHANGE percentage_attendance percentage_attendance INT NOT NULL');
        $this->addSql('ALTER TABLE provider_session CHANGE percentage_attendance percentage_attendance INT NOT NULL');
        $this->addSql('ALTER TABLE session_convocation_attachment ADD evaluation_modal_mail_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE session_convocation_attachment ADD CONSTRAINT FK_2623C0AF36134DBF FOREIGN KEY (evaluation_modal_mail_id) REFERENCES evaluation_modal_mail (id)');
        $this->addSql('CREATE INDEX IDX_2623C0AF36134DBF ON session_convocation_attachment (evaluation_modal_mail_id)');
        $this->addSql('ALTER TABLE evaluation_modal_mail ADD language VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evaluation_modal_mail DROP FOREIGN KEY FK_6239E34E19EB6921');
        $this->addSql('DROP TABLE evaluation_modal_mail');
        $this->addSql('ALTER TABLE abstract_provider_configuration_holder CHANGE percentage_attendance percentage_attendance INT DEFAULT 70 NOT NULL');
        $this->addSql('ALTER TABLE provider_session CHANGE percentage_attendance percentage_attendance INT DEFAULT 70 NOT NULL');
        $this->addSql('ALTER TABLE session_convocation_attachment DROP FOREIGN KEY FK_2623C0AF36134DBF');
        $this->addSql('DROP INDEX IDX_2623C0AF36134DBF ON session_convocation_attachment');
        $this->addSql('ALTER TABLE evaluation_modal_mail DROP language');
    }
}
