<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220505105557 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE microsoft_teams_participant_microsoft_teams_session (microsoft_teams_participant_id INT NOT NULL, microsoft_teams_session_id INT NOT NULL, INDEX IDX_4563CCA04DB82D06 (microsoft_teams_participant_id), INDEX IDX_4563CCA0ABDDAD86 (microsoft_teams_session_id), PRIMARY KEY(microsoft_teams_participant_id, microsoft_teams_session_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE microsoft_teams_participant_microsoft_teams_session ADD CONSTRAINT FK_4563CCA04DB82D06 FOREIGN KEY (microsoft_teams_participant_id) REFERENCES microsoft_teams_participant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE microsoft_teams_participant_microsoft_teams_session ADD CONSTRAINT FK_4563CCA0ABDDAD86 FOREIGN KEY (microsoft_teams_session_id) REFERENCES microsoft_teams_session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE microsoft_teams_configuration_holder ADD safety_time INT NOT NULL');
        $this->addSql('ALTER TABLE microsoft_teams_participant ADD contact_identifier INT DEFAULT NULL, ADD active TINYINT(1) DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE microsoft_teams_session ADD licence VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE microsoft_teams_configuration_holder DROP safety_time');
        $this->addSql('ALTER TABLE microsoft_teams_session DROP licence');
        $this->addSql('DROP TABLE microsoft_teams_participant_microsoft_teams_session');
        $this->addSql('ALTER TABLE microsoft_teams_participant DROP contact_identifier, DROP active');
        $this->addSql('ALTER TABLE microsoft_teams_session DROP licence');
    }
}
