<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230426133432 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_participant_session_role ADD registration_form_responses JSON NOT NULL');
        $this->addSql('ALTER TABLE registration_page_template ADD registration_form JSON NOT NULL, CHANGE block_lower_banner_privacy_check block_lower_banner_privacy_check TINYINT(1) DEFAULT false NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_participant_session_role DROP registration_form_responses');
        $this->addSql('ALTER TABLE registration_page_template DROP registration_form');
    }
}
