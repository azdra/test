<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221014123808 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE adobe_connect_webinar_configuration_holder (id INT NOT NULL, subdomain VARCHAR(255) NOT NULL, default_connexion_type VARCHAR(255) NOT NULL, birthday DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', licence_type INT NOT NULL, default_room BIGINT DEFAULT NULL, standard_view_on_opening_room TINYINT(1) NOT NULL, licences JSON DEFAULT NULL, host_group_sco_id BIGINT DEFAULT NULL, username_sco_id BIGINT DEFAULT NULL, saving_meeting_folder_sco_id BIGINT NOT NULL, automatic_licensing TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adobe_connect_webinar_meeting_attendee (id INT AUTO_INCREMENT NOT NULL, adobe_connect_webinar_principal_id INT DEFAULT NULL, adobe_connect_webinar_sco_id INT DEFAULT NULL, session_start DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', session_end DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7748AE71C60B7C7 (adobe_connect_webinar_principal_id), INDEX IDX_7748AE71B0BE8DB2 (adobe_connect_webinar_sco_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adobe_connect_webinar_principal (id INT NOT NULL, principal_identifier BIGINT NOT NULL, name LONGTEXT NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_6ACCB8AAAE112047 (principal_identifier), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adobe_connect_webinar_principal_adobe_connect_webinar_sco (adobe_connect_webinar_principal_id INT NOT NULL, adobe_connect_webinar_sco_id INT NOT NULL, INDEX IDX_1010BDDAC60B7C7 (adobe_connect_webinar_principal_id), INDEX IDX_1010BDDAB0BE8DB2 (adobe_connect_webinar_sco_id), PRIMARY KEY(adobe_connect_webinar_principal_id, adobe_connect_webinar_sco_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adobe_connect_webinar_sco (id INT NOT NULL, unique_sco_identifier BIGINT NOT NULL, parent_sco_identifier BIGINT DEFAULT NULL, seminar_sco_identifier BIGINT DEFAULT NULL, url_path VARCHAR(255) NOT NULL, is_seminar TINYINT(1) NOT NULL, type VARCHAR(255) NOT NULL, admitted_session VARCHAR(255) NOT NULL, room_model BIGINT DEFAULT NULL, INDEX IDX_7B549A2C48E3E211 (unique_sco_identifier), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adobe_connect_webinar_telephony_profile (id INT NOT NULL, linked_principal_id INT DEFAULT NULL, adobe_default_phone_number VARCHAR(255) DEFAULT NULL, code_animator VARCHAR(255) DEFAULT NULL, code_participant VARCHAR(255) DEFAULT NULL, meeting_one_conference_id VARCHAR(255) DEFAULT NULL, INDEX IDX_C4F38E55E97135B4 (linked_principal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE adobe_connect_webinar_configuration_holder ADD CONSTRAINT FK_BD0795BBF396750 FOREIGN KEY (id) REFERENCES abstract_provider_configuration_holder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adobe_connect_webinar_meeting_attendee ADD CONSTRAINT FK_7748AE71C60B7C7 FOREIGN KEY (adobe_connect_webinar_principal_id) REFERENCES adobe_connect_webinar_principal (id)');
        $this->addSql('ALTER TABLE adobe_connect_webinar_meeting_attendee ADD CONSTRAINT FK_7748AE71B0BE8DB2 FOREIGN KEY (adobe_connect_webinar_sco_id) REFERENCES adobe_connect_webinar_sco (id)');
        $this->addSql('ALTER TABLE adobe_connect_webinar_principal ADD CONSTRAINT FK_6ACCB8AABF396750 FOREIGN KEY (id) REFERENCES provider_participant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adobe_connect_webinar_principal_adobe_connect_webinar_sco ADD CONSTRAINT FK_1010BDDAC60B7C7 FOREIGN KEY (adobe_connect_webinar_principal_id) REFERENCES adobe_connect_webinar_principal (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adobe_connect_webinar_principal_adobe_connect_webinar_sco ADD CONSTRAINT FK_1010BDDAB0BE8DB2 FOREIGN KEY (adobe_connect_webinar_sco_id) REFERENCES adobe_connect_webinar_sco (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adobe_connect_webinar_sco ADD CONSTRAINT FK_7B549A2CBF396750 FOREIGN KEY (id) REFERENCES provider_session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adobe_connect_webinar_telephony_profile ADD CONSTRAINT FK_C4F38E55E97135B4 FOREIGN KEY (linked_principal_id) REFERENCES adobe_connect_webinar_principal (id)');
        $this->addSql('ALTER TABLE adobe_connect_webinar_telephony_profile ADD CONSTRAINT FK_C4F38E55BF396750 FOREIGN KEY (id) REFERENCES abstract_provider_audio (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adobe_connect_webinar_meeting_attendee DROP FOREIGN KEY FK_7748AE71C60B7C7');
        $this->addSql('ALTER TABLE adobe_connect_webinar_principal_adobe_connect_webinar_sco DROP FOREIGN KEY FK_1010BDDAC60B7C7');
        $this->addSql('ALTER TABLE adobe_connect_webinar_telephony_profile DROP FOREIGN KEY FK_C4F38E55E97135B4');
        $this->addSql('ALTER TABLE adobe_connect_webinar_meeting_attendee DROP FOREIGN KEY FK_7748AE71B0BE8DB2');
        $this->addSql('ALTER TABLE adobe_connect_webinar_principal_adobe_connect_webinar_sco DROP FOREIGN KEY FK_1010BDDAB0BE8DB2');
        $this->addSql('DROP TABLE adobe_connect_webinar_configuration_holder');
        $this->addSql('DROP TABLE adobe_connect_webinar_meeting_attendee');
        $this->addSql('DROP TABLE adobe_connect_webinar_principal');
        $this->addSql('DROP TABLE adobe_connect_webinar_principal_adobe_connect_webinar_sco');
        $this->addSql('DROP TABLE adobe_connect_webinar_sco');
        $this->addSql('DROP TABLE adobe_connect_webinar_telephony_profile');
    }
}
