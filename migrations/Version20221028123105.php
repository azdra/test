<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221028123105 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE webinar_convocation (id INT AUTO_INCREMENT NOT NULL, convocation_id INT DEFAULT NULL, session_id INT DEFAULT NULL, subject_mail VARCHAR(255) NOT NULL, convocation_date DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', content LONGTEXT DEFAULT NULL, sent TINYINT(1) NOT NULL, convocation_type VARCHAR(255) NOT NULL, INDEX IDX_294EBFFDE8746F65 (convocation_id), INDEX IDX_294EBFFD613FECDF (session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE webinar_convocation ADD CONSTRAINT FK_294EBFFDE8746F65 FOREIGN KEY (convocation_id) REFERENCES convocation (id)');
        $this->addSql('ALTER TABLE webinar_convocation ADD CONSTRAINT FK_294EBFFD613FECDF FOREIGN KEY (session_id) REFERENCES provider_session (id)');
        $this->addSql('ALTER TABLE session_convocation_attachment ADD webinar_communication_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE session_convocation_attachment ADD CONSTRAINT FK_2623C0AF261586DF FOREIGN KEY (webinar_communication_id) REFERENCES webinar_convocation (id)');
        $this->addSql('CREATE INDEX IDX_2623C0AF261586DF ON session_convocation_attachment (webinar_communication_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE webinar_convocation');
        $this->addSql('ALTER TABLE session_convocation_attachment DROP FOREIGN KEY FK_2623C0AF261586DF');
        $this->addSql('DROP INDEX IDX_2623C0AF261586DF ON session_convocation_attachment');
        $this->addSql('ALTER TABLE session_convocation_attachment DROP webinar_communication_id, CHANGE original_name original_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE file_name file_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE token token VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
