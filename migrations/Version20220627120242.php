<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220627120242 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE black_list_entry (id INT AUTO_INCREMENT NOT NULL, module_id INT DEFAULT NULL, prohibited VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_54C45798AFC2B591 (module_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE white_list_entry (id INT AUTO_INCREMENT NOT NULL, module_id INT DEFAULT NULL, allowed VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_51DABCD9AFC2B591 (module_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE black_list_entry ADD CONSTRAINT FK_54C45798AFC2B591 FOREIGN KEY (module_id) REFERENCES module (id)');
        $this->addSql('ALTER TABLE white_list_entry ADD CONSTRAINT FK_51DABCD9AFC2B591 FOREIGN KEY (module_id) REFERENCES module (id)');
        $this->addSql('ALTER TABLE module DROP white_list, DROP black_list');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE black_list_entry');
        $this->addSql('DROP TABLE white_list_entry');
        $this->addSql('ALTER TABLE module ADD white_list LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', black_list LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\'');
    }
}
