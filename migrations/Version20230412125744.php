<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230412125744 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_session ADD registration_page_template_id INT DEFAULT NULL, ADD provider_session_registration_page_places INT DEFAULT NULL, ADD provider_session_registration_page_description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_session ADD CONSTRAINT FK_201C7773DD5986FD FOREIGN KEY (registration_page_template_id) REFERENCES registration_page_template (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_201C7773DD5986FD ON provider_session (registration_page_template_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_session DROP FOREIGN KEY FK_201C7773DD5986FD');
        $this->addSql('DROP INDEX UNIQ_201C7773DD5986FD ON provider_session');
        $this->addSql('ALTER TABLE provider_session DROP registration_page_template_id, DROP provider_session_registration_page_places, DROP provider_session_registration_page_description');
    }
}
