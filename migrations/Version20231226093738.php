<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231226093738 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_session ADD groups_participants JSON NOT NULL');
        $this->addSql("UPDATE provider_session SET groups_participants = '[]' WHERE groups_participants LIKE 'null'");
        $this->addSql('ALTER TABLE provider_participant_session_role ADD groups_participants JSON NOT NULL');
        $this->addSql("UPDATE provider_participant_session_role SET groups_participants = '[]' WHERE groups_participants LIKE 'null'");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_session DROP groups_participants');
        $this->addSql('ALTER TABLE provider_participant_session_role DROP groups_participants');
    }
}
