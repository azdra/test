<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220316104334 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE participant_session_signature (id INT AUTO_INCREMENT NOT NULL, provider_participant_session_role_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', signature_for_the_session_of DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', day_part_indicator VARCHAR(255) NOT NULL, signature LONGBLOB NOT NULL, INDEX IDX_497EF485F6CE1A0 (provider_participant_session_role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE participant_session_signature ADD CONSTRAINT FK_497EF485F6CE1A0 FOREIGN KEY (provider_participant_session_role_id) REFERENCES provider_participant_session_role (id)');
        $this->addSql('ALTER TABLE provider_session ADD assistance_type INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE participant_session_signature');
        $this->addSql('ALTER TABLE provider_session DROP assistance_type');
    }
}
