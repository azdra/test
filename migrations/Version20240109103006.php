<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240109103006 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evaluation_provider_session ADD subject_mail VARCHAR(255) DEFAULT NULL, ADD content LONGTEXT DEFAULT NULL, ADD sent TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE session_convocation_attachment ADD evaluation_provider_session_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE session_convocation_attachment ADD CONSTRAINT FK_2623C0AFADA986E5 FOREIGN KEY (evaluation_provider_session_id) REFERENCES evaluation_provider_session (id)');
        $this->addSql('CREATE INDEX IDX_2623C0AFADA986E5 ON session_convocation_attachment (evaluation_provider_session_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evaluation_provider_session DROP subject_mail, DROP content, DROP sent');
        $this->addSql('ALTER TABLE session_convocation_attachment DROP FOREIGN KEY FK_2623C0AFADA986E5');
        $this->addSql('DROP INDEX IDX_2623C0AFADA986E5 ON session_convocation_attachment');
        $this->addSql('ALTER TABLE session_convocation_attachment DROP evaluation_provider_session_id');
    }
}
