<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230605082733 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE provider_session_replay (id INT AUTO_INCREMENT NOT NULL, session_id INT DEFAULT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, seen INT NOT NULL, provider_replay_access_token_token VARCHAR(255) NOT NULL, provider_replay_access_token_tag VARCHAR(255) NOT NULL, provider_replay_access_token_iv VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_EE813103CD44097C (provider_replay_access_token_token), INDEX IDX_EE813103613FECDF (session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE provider_session_replay ADD CONSTRAINT FK_EE813103613FECDF FOREIGN KEY (session_id) REFERENCES provider_session (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE provider_session_replay');
    }
}
