<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231130152314 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module ADD personalization_training_banner VARCHAR(255) DEFAULT NULL, ADD personalization_training_banner_position VARCHAR(255) DEFAULT \'center\', ADD personalization_replay_banner VARCHAR(255) DEFAULT NULL, ADD personalization_replay_banner_position VARCHAR(255) DEFAULT \'center\', ADD personalization_sessions_banner VARCHAR(255) DEFAULT NULL, ADD personalization_sessions_banner_position VARCHAR(255) DEFAULT \'center\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module DROP personalization_training_banner, DROP personalization_training_banner_position, DROP personalization_replay_banner, DROP personalization_replay_banner_position, DROP personalization_sessions_banner, DROP personalization_sessions_banner_position');
    }
}
