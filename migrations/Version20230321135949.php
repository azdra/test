<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230321135949 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE registration_page_template (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, active TINYINT(1) NOT NULL, language VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', block_graphical_charter_font VARCHAR(255) DEFAULT NULL, block_graphical_charter_background_color_banner VARCHAR(255) DEFAULT NULL, block_graphical_charter_color_button VARCHAR(255) DEFAULT NULL, block_graphical_charter_logo VARCHAR(255) DEFAULT NULL, block_graphical_charter_banner VARCHAR(255) DEFAULT NULL, block_graphical_charter_drawing VARCHAR(255) DEFAULT NULL, block_description_description_check TINYINT(1) NOT NULL, block_description_speakers_check TINYINT(1) NOT NULL, block_description_description VARCHAR(255) DEFAULT NULL, block_description_speakers VARCHAR(255) DEFAULT NULL, block_upper_banner_catchphrase VARCHAR(255) DEFAULT NULL, block_upper_banner_logo VARCHAR(255) DEFAULT NULL, block_upper_banner_information_support TINYINT(1) NOT NULL, block_upper_banner_faq TINYINT(1) NOT NULL, block_upper_banner_needhelp VARCHAR(255) DEFAULT NULL, block_upper_banner_connexionhelp VARCHAR(255) DEFAULT NULL, block_upper_banner_phone_number VARCHAR(255) DEFAULT NULL, block_lower_banner_social_networks_check TINYINT(1) NOT NULL, block_lower_banner_privacy_check TINYINT(1) NOT NULL, block_lower_banner_privacy VARCHAR(255) DEFAULT NULL, block_lower_banner_privacy_text LONGTEXT DEFAULT NULL, block_lower_banner_privacy_file VARCHAR(255) DEFAULT NULL, block_lower_banner_legal_notice_check TINYINT(1) NOT NULL, block_lower_banner_legal_notice VARCHAR(255) DEFAULT NULL, block_lower_banner_legal_notice_text LONGTEXT DEFAULT NULL, block_lower_banner_legal_notice_file VARCHAR(255) DEFAULT NULL, block_lower_banner_facebook_link VARCHAR(255) DEFAULT NULL, block_lower_banner_linkedin_link VARCHAR(255) DEFAULT NULL, block_lower_banner_instagram_link VARCHAR(255) DEFAULT NULL, block_lower_banner_twitter_link VARCHAR(255) DEFAULT NULL, INDEX IDX_8B50E0D219EB6921 (client_id), INDEX IDX_8B50E0D2DE12AB56 (created_by), INDEX IDX_8B50E0D216FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE registration_page_template ADD CONSTRAINT FK_8B50E0D219EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE registration_page_template ADD CONSTRAINT FK_8B50E0D2DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE registration_page_template ADD CONSTRAINT FK_8B50E0D216FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE client ADD enable_registration_page TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE registration_page_template');
        $this->addSql('ALTER TABLE client DROP enable_registration_page');
    }
}
