<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230118134854 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE white_configuration_holder (id INT NOT NULL, safety_time INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE white_participant (id INT NOT NULL, contact_identifier INT DEFAULT NULL, active TINYINT(1) DEFAULT 1 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE white_participant_white_session (white_participant_id INT NOT NULL, white_session_id INT NOT NULL, INDEX IDX_366785939EA4E67E (white_participant_id), INDEX IDX_36678593C2575975 (white_session_id), PRIMARY KEY(white_participant_id, white_session_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE white_session (id INT NOT NULL, join_session_web_url LONGTEXT DEFAULT NULL, connection_information LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE white_configuration_holder ADD CONSTRAINT FK_E94B2217BF396750 FOREIGN KEY (id) REFERENCES abstract_provider_configuration_holder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE white_participant ADD CONSTRAINT FK_E13F4952BF396750 FOREIGN KEY (id) REFERENCES provider_participant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE white_participant_white_session ADD CONSTRAINT FK_366785939EA4E67E FOREIGN KEY (white_participant_id) REFERENCES white_participant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE white_participant_white_session ADD CONSTRAINT FK_36678593C2575975 FOREIGN KEY (white_session_id) REFERENCES white_session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE white_session ADD CONSTRAINT FK_E54C84DCBF396750 FOREIGN KEY (id) REFERENCES provider_session (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE white_participant_white_session DROP FOREIGN KEY FK_366785939EA4E67E');
        $this->addSql('ALTER TABLE white_participant_white_session DROP FOREIGN KEY FK_36678593C2575975');
        $this->addSql('DROP TABLE white_configuration_holder');
        $this->addSql('DROP TABLE white_participant');
        $this->addSql('DROP TABLE white_participant_white_session');
        $this->addSql('DROP TABLE white_session');
    }
}
