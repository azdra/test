<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240418085822 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE credit_allocation_sms (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, allowed_credit INT DEFAULT 0 NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', INDEX IDX_97A7BB019EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sms_bounce (id INT AUTO_INCREMENT NOT NULL, session_id INT DEFAULT NULL, user_id INT DEFAULT NULL, phone VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', date_send DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', INDEX IDX_97646AC9613FECDF (session_id), INDEX IDX_97646AC9A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE credit_allocation_sms ADD CONSTRAINT FK_97A7BB019EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE sms_bounce ADD CONSTRAINT FK_97646AC9613FECDF FOREIGN KEY (session_id) REFERENCES provider_session (id)');
        $this->addSql('ALTER TABLE sms_bounce ADD CONSTRAINT FK_97646AC9A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE client ADD credit_sms DOUBLE PRECISION DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE webinar_convocation ADD sender VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE credit_allocation_sms');
        $this->addSql('DROP TABLE sms_bounce');
        $this->addSql('ALTER TABLE client DROP credit_sms');
        $this->addSql('ALTER TABLE webinar_convocation DROP sender');
    }
}
