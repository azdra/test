<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220429090658 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adobe_connect_configuration_holder CHANGE host_group_sco_id host_group_sco_id BIGINT DEFAULT NULL, CHANGE username_sco_id username_sco_id BIGINT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adobe_connect_configuration_holder CHANGE host_group_sco_id host_group_sco_id INT DEFAULT NULL, CHANGE username_sco_id username_sco_id INT DEFAULT NULL');
    }
}
