<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230802091507 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module ADD personalization_enabled_faq TINYINT(1) DEFAULT 0 NOT NULL, ADD personalization_enabled_training TINYINT(1) DEFAULT 0 NOT NULL, ADD personalization_enabled_replay TINYINT(1) DEFAULT 0 NOT NULL, ADD personalization_sessions_title VARCHAR(255) DEFAULT \'My Sessions\', ADD personalization_sessions_description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_session ADD replay_layout_play_button VARCHAR(255) DEFAULT \'\'');
        $this->addSql('ALTER TABLE registration_page_template CHANGE block_upper_banner_connexion_help block_upper_banner_connexion_help VARCHAR(255) DEFAULT \'Foire aux questions\'');
        $this->addSql('ALTER TABLE webex_rest_participant CHANGE invitee_id invitee_id INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module DROP personalization_enabled_faq, DROP personalization_enabled_training, DROP personalization_enabled_replay, DROP personalization_sessions_title, DROP personalization_sessions_description');
        $this->addSql('ALTER TABLE provider_session DROP replay_layout_play_button');
        $this->addSql('ALTER TABLE registration_page_template CHANGE block_upper_banner_connexion_help block_upper_banner_connexion_help VARCHAR(255) DEFAULT \'Aide à la connexion\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE webex_rest_participant CHANGE invitee_id invitee_id VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
