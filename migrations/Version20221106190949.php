<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221106190949 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_participant_session_role ADD last_communication_sent DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD last_reminder_communication_sent DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD last_thanks_communication_sent DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_participant_session_role DROP last_communication_sent, DROP last_reminder_communication_sent, DROP last_thanks_communication_sent, CHANGE role role VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE dates_for_sending_convocations dates_for_sending_convocations LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:object)\', CHANGE provider_session_access_token_token provider_session_access_token_token VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE provider_session_access_token_tag provider_session_access_token_tag VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE provider_session_access_token_iv provider_session_access_token_iv VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
