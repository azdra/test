<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230413131928 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_session DROP INDEX UNIQ_201C7773DD5986FD, ADD INDEX IDX_201C7773DD5986FD (registration_page_template_id)');
        $this->addSql('ALTER TABLE provider_session ADD provider_session_registration_page_description2 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE registration_page_template ADD block_upper_banner_need_help_icon VARCHAR(255) DEFAULT NULL, CHANGE block_lower_banner_privacy_check block_lower_banner_privacy_check TINYINT(1) DEFAULT false NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE provider_session DROP INDEX IDX_201C7773DD5986FD, ADD UNIQUE INDEX UNIQ_201C7773DD5986FD (registration_page_template_id)');
        $this->addSql('ALTER TABLE provider_session DROP provider_session_registration_page_description2');
        $this->addSql('ALTER TABLE registration_page_template DROP block_upper_banner_need_help_icon, CHANGE block_lower_banner_privacy_check block_lower_banner_privacy_check TINYINT(1) DEFAULT 0 NOT NULL');
    }
}
