<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231129153847 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adobe_connect_webinar_principal DROP INDEX UNIQ_6ACCB8AAAE112047, ADD INDEX IDX_6ACCB8AAAE112047 (principal_identifier)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adobe_connect_webinar_principal DROP INDEX IDX_6ACCB8AAAE112047, ADD UNIQUE INDEX UNIQ_6ACCB8AAAE112047 (principal_identifier)');
    }
}
