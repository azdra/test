<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230807072710 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module CHANGE template_design template_design VARCHAR(255) DEFAULT \'square\' NOT NULL');
        $this->addSql('ALTER TABLE webex_rest_meeting CHANGE open_time open_time INT DEFAULT 15 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module CHANGE template_design template_design VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE webex_rest_meeting CHANGE open_time open_time INT DEFAULT 900 NOT NULL');
    }
}
