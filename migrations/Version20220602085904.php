<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220602085904 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE module (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, active TINYINT(1) NOT NULL, domain VARCHAR(255) NOT NULL, main_color VARCHAR(255) NOT NULL, secondary_color VARCHAR(255) NOT NULL, logo VARCHAR(255) NOT NULL, white_list LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', black_list LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_C24262819EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subject (id INT AUTO_INCREMENT NOT NULL, theme_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, private_access VARCHAR(255) NOT NULL, active TINYINT(1) NOT NULL, duration INT NOT NULL, category_session INT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_FBCE3E7A59027487 (theme_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE theme (id INT AUTO_INCREMENT NOT NULL, module_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, active TINYINT(1) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_9775E708AFC2B591 (module_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE module ADD CONSTRAINT FK_C24262819EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE subject ADD CONSTRAINT FK_FBCE3E7A59027487 FOREIGN KEY (theme_id) REFERENCES theme (id)');
        $this->addSql('ALTER TABLE theme ADD CONSTRAINT FK_9775E708AFC2B591 FOREIGN KEY (module_id) REFERENCES module (id)');
        $this->addSql('ALTER TABLE provider_session ADD subject_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE provider_session ADD CONSTRAINT FK_201C777323EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id)');
        $this->addSql('CREATE INDEX IDX_201C777323EDC87 ON provider_session (subject_id)');
        $this->addSql('ALTER TABLE client_selfsubscription_module_thema DROP FOREIGN KEY FK_9AD3A8F1CF16A43C');
        $this->addSql('ALTER TABLE client_selfsubscription_module_thema_subject DROP FOREIGN KEY FK_C71F07EAAB4369C2');
        $this->addSql('DROP TABLE client_selfsubscription_module');
        $this->addSql('DROP TABLE client_selfsubscription_module_thema');
        $this->addSql('DROP TABLE client_selfsubscription_module_thema_subject');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE theme DROP FOREIGN KEY FK_9775E708AFC2B591');
        $this->addSql('ALTER TABLE provider_session DROP FOREIGN KEY FK_201C777323EDC87');
        $this->addSql('ALTER TABLE subject DROP FOREIGN KEY FK_FBCE3E7A59027487');
        $this->addSql('DROP TABLE module');
        $this->addSql('DROP TABLE subject');
        $this->addSql('DROP TABLE theme');
        $this->addSql('DROP INDEX IDX_201C777323EDC87 ON provider_session');
        $this->addSql('ALTER TABLE provider_session DROP subject_id');
        $this->addSql('CREATE TABLE client_selfsubscription_module (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, domain VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', active TINYINT(1) NOT NULL, INDEX IDX_8CE6E3E19EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE client_selfsubscription_module_thema (id INT AUTO_INCREMENT NOT NULL, client_selfsubscription_module_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, code VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', active TINYINT(1) NOT NULL, INDEX IDX_9AD3A8F1CF16A43C (client_selfsubscription_module_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE client_selfsubscription_module_thema_subject (id INT AUTO_INCREMENT NOT NULL, client_selfsubscription_module_thema_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, code VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', active TINYINT(1) NOT NULL, INDEX IDX_C71F07EAAB4369C2 (client_selfsubscription_module_thema_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE client_selfsubscription_module ADD CONSTRAINT FK_8CE6E3E19EB6921 FOREIGN KEY (client_id) REFERENCES client (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE client_selfsubscription_module_thema ADD CONSTRAINT FK_9AD3A8F1CF16A43C FOREIGN KEY (client_selfsubscription_module_id) REFERENCES client_selfsubscription_module (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE client_selfsubscription_module_thema_subject ADD CONSTRAINT FK_C71F07EAAB4369C2 FOREIGN KEY (client_selfsubscription_module_thema_id) REFERENCES client_selfsubscription_module_thema (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
    }
}
