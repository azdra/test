<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230703080125 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module CHANGE lang lang VARCHAR(255) DEFAULT \'fr\' NOT NULL');
        $this->addSql('ALTER TABLE provider_session_replay CHANGE last_show_replay last_show_replay DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE registration_page_template CHANGE block_lower_banner_privacy_url_check block_lower_banner_privacy_url_check TINYINT(1) DEFAULT 1 NOT NULL, CHANGE block_lower_banner_legal_notice_url_check block_lower_banner_legal_notice_url_check TINYINT(1) DEFAULT 1 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module CHANGE lang lang VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE provider_session_replay CHANGE last_show_replay last_show_replay DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\'');
        $this->addSql('ALTER TABLE registration_page_template CHANGE block_lower_banner_privacy_url_check block_lower_banner_privacy_url_check TINYINT(1) DEFAULT 0 NOT NULL');
    }
}
