<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220920092813 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX providersession_unicity_ref_and_configurationholder ON provider_session (ref, abstract_provider_configuration_holder_id)');
        $this->addSql('DROP INDEX UNIQ_201C7773146F3EA3 ON provider_session');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX providersession_unicity_ref_and_configurationholder ON provider_session');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_201C7773146F3EA3 ON provider_session (ref)');
    }
}
