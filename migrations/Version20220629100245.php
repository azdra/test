<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220629100245 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE evaluation_performance_animator_answer (id INT AUTO_INCREMENT NOT NULL, provider_participant_session_role_id INT DEFAULT NULL, help_task_id INT DEFAULT NULL, survey_id VARCHAR(255) NOT NULL, reply_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', answer JSON NOT NULL, INDEX IDX_D0D72CBAF6CE1A0 (provider_participant_session_role_id), INDEX IDX_D0D72CBAD3ACFE5A (help_task_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE evaluation_performance_animator_answer ADD CONSTRAINT FK_D0D72CBAF6CE1A0 FOREIGN KEY (provider_participant_session_role_id) REFERENCES provider_participant_session_role (id)');
        $this->addSql('ALTER TABLE evaluation_performance_animator_answer ADD CONSTRAINT FK_D0D72CBAD3ACFE5A FOREIGN KEY (help_task_id) REFERENCES help_task (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE evaluation_performance_animator_answer');
    }
}
