<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231221151644 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE video_replay ADD subject_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE video_replay ADD CONSTRAINT FK_E160543F23EDC87 FOREIGN KEY (subject_id) REFERENCES subject (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E160543F23EDC87 ON video_replay (subject_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE video_replay DROP FOREIGN KEY FK_E160543F23EDC87');
        $this->addSql('DROP INDEX UNIQ_E160543F23EDC87 ON video_replay');
        $this->addSql('ALTER TABLE video_replay DROP subject_id');
    }
}
