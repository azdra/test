<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231213110243 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE on_hold_provider_participant_session_role (id INT AUTO_INCREMENT NOT NULL, session_id INT DEFAULT NULL, role VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, forname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, situation_mixte INT DEFAULT 1 NOT NULL, INDEX IDX_DAABA67D613FECDF (session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE on_hold_provider_session (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, name VARCHAR(255) NOT NULL, ref VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', date_start DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', date_end DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', category INT NOT NULL, duration INT DEFAULT NULL, information LONGTEXT DEFAULT NULL, language VARCHAR(255) DEFAULT NULL, status SMALLINT NOT NULL, restlunch INT DEFAULT 0 NOT NULL, nbrdays INT DEFAULT 1 NOT NULL, description LONGTEXT DEFAULT NULL, description2 LONGTEXT DEFAULT NULL, additional_timezones JSON NOT NULL, INDEX IDX_FFF5170019EB6921 (client_id), INDEX IDX_FFF51700DE12AB56 (created_by), INDEX IDX_FFF5170016FE72E1 (updated_by), INDEX idx_date_start (date_start), INDEX idx_category (category), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE on_hold_provider_participant_session_role ADD CONSTRAINT FK_DAABA67D613FECDF FOREIGN KEY (session_id) REFERENCES on_hold_provider_session (id)');
        $this->addSql('ALTER TABLE on_hold_provider_session ADD CONSTRAINT FK_FFF5170019EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE on_hold_provider_session ADD CONSTRAINT FK_FFF51700DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE on_hold_provider_session ADD CONSTRAINT FK_FFF5170016FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE on_hold_provider_participant_session_role DROP FOREIGN KEY FK_DAABA67D613FECDF');
        $this->addSql('DROP TABLE on_hold_provider_participant_session_role');
        $this->addSql('DROP TABLE on_hold_provider_session');
    }
}
