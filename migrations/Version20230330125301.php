<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230330125301 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE deferred_actions_task (id INT AUTO_INCREMENT NOT NULL, client_origin INT DEFAULT NULL, user_origin INT DEFAULT NULL, class_name LONGTEXT NOT NULL, origin VARCHAR(255) NOT NULL, action VARCHAR(255) NOT NULL, data JSON NOT NULL, result JSON NOT NULL, status VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', processing_start_time DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', processing_end_time DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', infos JSON NOT NULL, INDEX IDX_6904F38B571DAD (client_origin), INDEX IDX_6904F3BBA34D23 (user_origin), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE deferred_actions_task ADD CONSTRAINT FK_6904F38B571DAD FOREIGN KEY (client_origin) REFERENCES client (id)');
        $this->addSql('ALTER TABLE deferred_actions_task ADD CONSTRAINT FK_6904F3BBA34D23 FOREIGN KEY (user_origin) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE deferred_actions_task');
    }
}
