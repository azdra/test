<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220511081015 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE webex_participant_abstract_webex_session DROP FOREIGN KEY FK_9A7E734EE750A910');
        $this->addSql('ALTER TABLE webex_session_attendee DROP FOREIGN KEY FK_5CFB01BC8A4783D9');
        $this->addSql('DROP TABLE abstract_webex_session');
        $this->addSql('DROP TABLE webex_participant_abstract_webex_session');
        $this->addSql('DROP TABLE webex_session_attendee');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE abstract_webex_session (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE webex_participant_abstract_webex_session (webex_participant_id INT NOT NULL, abstract_webex_session_id INT NOT NULL, INDEX IDX_9A7E734E2BC22BD6 (webex_participant_id), INDEX IDX_9A7E734EE750A910 (abstract_webex_session_id), PRIMARY KEY(webex_participant_id, abstract_webex_session_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE webex_session_attendee (id INT AUTO_INCREMENT NOT NULL, webex_participant_id INT DEFAULT NULL, webex_session_id INT DEFAULT NULL, INDEX IDX_5CFB01BC2BC22BD6 (webex_participant_id), INDEX IDX_5CFB01BC8A4783D9 (webex_session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE abstract_webex_session ADD CONSTRAINT FK_9C6C2605BF396750 FOREIGN KEY (id) REFERENCES provider_session (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webex_participant_abstract_webex_session ADD CONSTRAINT FK_9A7E734E2BC22BD6 FOREIGN KEY (webex_participant_id) REFERENCES webex_participant (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webex_participant_abstract_webex_session ADD CONSTRAINT FK_9A7E734EE750A910 FOREIGN KEY (abstract_webex_session_id) REFERENCES abstract_webex_session (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webex_session_attendee ADD CONSTRAINT FK_5CFB01BC2BC22BD6 FOREIGN KEY (webex_participant_id) REFERENCES webex_participant (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE webex_session_attendee ADD CONSTRAINT FK_5CFB01BC8A4783D9 FOREIGN KEY (webex_session_id) REFERENCES abstract_webex_session (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
    }
}
