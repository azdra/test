<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230807091439 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module CHANGE personalization_enabled_faq personalization_enabled_faq TINYINT(1) DEFAULT 1 NOT NULL, CHANGE personalization_enabled_training personalization_enabled_training TINYINT(1) DEFAULT 1 NOT NULL, CHANGE personalization_enabled_sessions personalization_enabled_sessions TINYINT(1) DEFAULT 1 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE module CHANGE personalization_enabled_faq personalization_enabled_faq TINYINT(1) DEFAULT 0 NOT NULL, CHANGE personalization_enabled_training personalization_enabled_training TINYINT(1) DEFAULT 0 NOT NULL, CHANGE personalization_enabled_sessions personalization_enabled_sessions TINYINT(1) DEFAULT 0 NOT NULL');
    }
}
