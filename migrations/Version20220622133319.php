<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220622133319 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE help_task SET answer = "[]" WHERE answer LIKE "null";');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE help_task SET answer = "null" WHERE answer = "[]";');
    }
}
