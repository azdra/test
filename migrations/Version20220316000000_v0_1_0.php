<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220316000000_v0_1_0 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE abstract_provider_audio (id INT AUTO_INCREMENT NOT NULL, configuration_holder_id INT DEFAULT NULL, profile_identifier BIGINT DEFAULT NULL, connection_type VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, phone_number VARCHAR(255) DEFAULT NULL, phone_lang VARCHAR(255) NOT NULL, discr VARCHAR(255) NOT NULL, INDEX IDX_35CB1C1054EA4E8B (configuration_holder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE abstract_provider_configuration_holder (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, domain VARCHAR(255) DEFAULT NULL, username VARCHAR(255) DEFAULT NULL, password VARCHAR(255) DEFAULT NULL, discr VARCHAR(255) NOT NULL, INDEX IDX_C3B46EA019EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE abstract_webex_session (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activity_log (id INT AUTO_INCREMENT NOT NULL, client_origin INT DEFAULT NULL, user_origin INT DEFAULT NULL, origin VARCHAR(255) NOT NULL, action VARCHAR(255) NOT NULL, severity VARCHAR(255) NOT NULL, infos JSON NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', reports_email_sent TINYINT(1) DEFAULT 0 NOT NULL, INDEX IDX_FD06F6478B571DAD (client_origin), INDEX IDX_FD06F647BBA34D23 (user_origin), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adobe_connect_configuration_holder (id INT NOT NULL, subdomain VARCHAR(255) NOT NULL, default_connexion_type VARCHAR(255) NOT NULL, birthday DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', licence_type INT NOT NULL, default_room BIGINT DEFAULT NULL, standard_view_on_opening_room TINYINT(1) NOT NULL, licences JSON DEFAULT NULL, host_group_sco_id INT DEFAULT NULL, username_sco_id INT DEFAULT NULL, saving_meeting_folder_sco_id BIGINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adobe_connect_meeting_attendee (id INT AUTO_INCREMENT NOT NULL, adobe_connect_principal_id INT DEFAULT NULL, adobe_connect_sco_id INT DEFAULT NULL, session_start DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', session_end DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_54DA304DC8DCAD (adobe_connect_principal_id), INDEX IDX_54DA30F40E4192 (adobe_connect_sco_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adobe_connect_principal (id INT NOT NULL, principal_identifier BIGINT NOT NULL, name LONGTEXT NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_FEAFD5C0AE112047 (principal_identifier), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adobe_connect_principal_adobe_connect_sco (adobe_connect_principal_id INT NOT NULL, adobe_connect_sco_id INT NOT NULL, INDEX IDX_FFBE67954DC8DCAD (adobe_connect_principal_id), INDEX IDX_FFBE6795F40E4192 (adobe_connect_sco_id), PRIMARY KEY(adobe_connect_principal_id, adobe_connect_sco_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adobe_connect_sco (id INT NOT NULL, unique_sco_identifier BIGINT NOT NULL, parent_sco_identifier BIGINT DEFAULT NULL, url_path VARCHAR(255) NOT NULL, is_seminar TINYINT(1) NOT NULL, type VARCHAR(255) NOT NULL, admitted_session VARCHAR(255) NOT NULL, room_model BIGINT DEFAULT NULL, INDEX IDX_43485FA848E3E211 (unique_sco_identifier), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adobe_connect_telephony_profile (id INT NOT NULL, linked_principal_id INT DEFAULT NULL, adobe_default_phone_number VARCHAR(255) DEFAULT NULL, code_animator VARCHAR(255) DEFAULT NULL, code_participant VARCHAR(255) DEFAULT NULL, INDEX IDX_C55FE327E97135B4 (linked_principal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE async_task (id INT AUTO_INCREMENT NOT NULL, client_origin INT DEFAULT NULL, user_origin INT DEFAULT NULL, class_name LONGTEXT NOT NULL, data JSON NOT NULL, result JSON NOT NULL, status VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', processing_start_time DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', processing_end_time DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', flag_origin VARCHAR(255) DEFAULT NULL, file_origin VARCHAR(255) DEFAULT NULL, infos JSON NOT NULL, INDEX IDX_3B0E64FC8B571DAD (client_origin), INDEX IDX_3B0E64FCBBA34D23 (user_origin), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bounce (id INT AUTO_INCREMENT NOT NULL, session_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', active TINYINT(1) DEFAULT 1 NOT NULL, date_send DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', date_status DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', email VARCHAR(255) DEFAULT NULL, subject VARCHAR(255) DEFAULT NULL, state INT NOT NULL, reason VARCHAR(255) DEFAULT NULL, mandrill_event VARCHAR(255) DEFAULT NULL, mail_origin VARCHAR(255) DEFAULT NULL, INDEX IDX_823CF459613FECDF (session_id), INDEX IDX_823CF459A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', default_lang VARCHAR(255) NOT NULL, timezone VARCHAR(255) NOT NULL, categories LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', organisation_name VARCHAR(255) DEFAULT NULL, organisation_legal_representative VARCHAR(255) DEFAULT NULL, organisation_function_legal_representative VARCHAR(255) DEFAULT NULL, organisation_competent_commercial_court VARCHAR(255) DEFAULT NULL, organisation_default_training_action_nature INT DEFAULT NULL, organisation_place VARCHAR(255) DEFAULT NULL, organisation_training_activity_registration_number VARCHAR(255) DEFAULT NULL, organisation_city VARCHAR(255) DEFAULT NULL, organisation_district VARCHAR(255) DEFAULT NULL, enable_assessment_achievement TINYINT(1) NOT NULL, enable_shared_email TINYINT(1) NOT NULL, enable_remote_signature TINYINT(1) NOT NULL, enable_access_link_on_export TINYINT(1) NOT NULL, logo VARCHAR(255) DEFAULT NULL, preferences JSON NOT NULL, header VARCHAR(255) DEFAULT NULL, footer VARCHAR(255) DEFAULT NULL, organization_stamp VARCHAR(255) DEFAULT NULL, session_import_model VARCHAR(255) DEFAULT NULL, trainees_import_model VARCHAR(255) DEFAULT NULL, closed_room_message VARCHAR(255) DEFAULT NULL, color VARCHAR(7) DEFAULT NULL, sender_name VARCHAR(255) DEFAULT NULL, sender_email VARCHAR(255) DEFAULT NULL, replay_to_email VARCHAR(255) DEFAULT NULL, active TINYINT(1) NOT NULL, commercial_contact_email VARCHAR(255) DEFAULT NULL, commercial_contact_telephone VARCHAR(255) DEFAULT NULL, customer_contact_email VARCHAR(255) DEFAULT NULL, customer_contact_telephone VARCHAR(255) DEFAULT NULL, self_registering_enabled TINYINT(1) NOT NULL, self_registering_link VARCHAR(255) DEFAULT NULL, self_registering_gdpr_information VARCHAR(255) DEFAULT NULL, email_scheduling_time_to_send_convocation_before_session JSON DEFAULT NULL, email_scheduling_times_to_send_reminder_before_session JSON DEFAULT NULL, email_scheduling_npai_alert_recipients JSON DEFAULT NULL, email_scheduling_time_to_send_presence_report_after_session JSON DEFAULT NULL, email_scheduling_enable_send_reminders_to_animators TINYINT(1) NOT NULL, email_scheduling_presence_report_recipients JSON DEFAULT NULL, email_options_send_acancellation_email_to_trainees TINYINT(1) NOT NULL, email_options_send_an_unsubscribe_email_to_participants TINYINT(1) NOT NULL, email_options_send_an_attestation_automatically TINYINT(1) NOT NULL, email_options_enable_automatic_attestation_sending TINYINT(1) NOT NULL, email_options_send_presence_report_after_session TINYINT(1) DEFAULT 0 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_selfsubscription_module (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, domain VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', active TINYINT(1) NOT NULL, INDEX IDX_8CE6E3E19EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_selfsubscription_module_thema (id INT AUTO_INCREMENT NOT NULL, client_selfsubscription_module_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', active TINYINT(1) NOT NULL, INDEX IDX_9AD3A8F1CF16A43C (client_selfsubscription_module_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_selfsubscription_module_thema_subject (id INT AUTO_INCREMENT NOT NULL, client_selfsubscription_module_thema_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', active TINYINT(1) NOT NULL, INDEX IDX_C71F07EAAB4369C2 (client_selfsubscription_module_thema_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_services (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, assistance_type INT NOT NULL, assistance_duration INT DEFAULT NULL, assistance_start INT DEFAULT NULL, commitment INT NOT NULL, email_to_notify_for_assistance VARCHAR(255) DEFAULT NULL, support_type INT NOT NULL, outside_support_type INT NOT NULL, time_before_session INT DEFAULT NULL, time_support_duration_session INT DEFAULT NULL, email_to_notify_for_support VARCHAR(255) DEFAULT NULL, dedicated_support TINYINT(1) NOT NULL, mail_support VARCHAR(255) DEFAULT NULL, phone_support VARCHAR(255) DEFAULT NULL, dedicated_user_guide VARCHAR(255) DEFAULT NULL, personnalized_info_mail_transactionnal LONGTEXT DEFAULT NULL, standard_hours_active TINYINT(1) NOT NULL, standard_hours_start_time_am DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', standard_hours_end_time_am DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', standard_hours_start_time_pm DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', standard_hours_end_time_pm DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', outside_standard_hours_active TINYINT(1) NOT NULL, outside_standard_hours_start_time_am DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', outside_standard_hours_end_time_am DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', outside_standard_hours_start_time_pm DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', outside_standard_hours_end_time_pm DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', UNIQUE INDEX UNIQ_5DD4D1AC19EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE convocation (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, subject_mail VARCHAR(255) NOT NULL, category INT DEFAULT NULL, content LONGTEXT DEFAULT NULL, languages VARCHAR(255) NOT NULL, status TINYINT(1) NOT NULL, type_audio LONGTEXT DEFAULT NULL, category_type_audio VARCHAR(255) DEFAULT NULL, INDEX IDX_C03B3F5F19EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evaluation (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, survey_id VARCHAR(255) NOT NULL, organization_id VARCHAR(255) DEFAULT NULL, INDEX IDX_1323A57519EB6921 (client_id), UNIQUE INDEX users_idx (title, client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evaluation_participant_answer (id INT AUTO_INCREMENT NOT NULL, provider_participant_session_role_id INT DEFAULT NULL, evaluation_id INT DEFAULT NULL, reply_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', answer JSON NOT NULL, INDEX IDX_2B2243A2F6CE1A0 (provider_participant_session_role_id), INDEX IDX_2B2243A2456C5646 (evaluation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE microsoft_teams_configuration_holder (id INT NOT NULL, token_endpoint VARCHAR(255) NOT NULL, tenant_id VARCHAR(255) NOT NULL, scope VARCHAR(255) NOT NULL, grant_type VARCHAR(255) NOT NULL, organizer_email VARCHAR(255) NOT NULL, organizer_unique_identifier VARCHAR(255) NOT NULL, client_id VARCHAR(255) NOT NULL, client_secret VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE microsoft_teams_participant (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE microsoft_teams_session (id INT NOT NULL, join_web_url LONGTEXT NOT NULL, meeting_identifier LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE provider_participant (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, configuration_holder_id INT DEFAULT NULL, discr VARCHAR(255) NOT NULL, INDEX IDX_1200E6E3A76ED395 (user_id), INDEX IDX_1200E6E354EA4E8B (configuration_holder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE provider_participant_session_role (id INT AUTO_INCREMENT NOT NULL, participant_id INT DEFAULT NULL, session_id INT DEFAULT NULL, role VARCHAR(255) NOT NULL, synced_with_provider TINYINT(1) NOT NULL, dates_for_sending_convocations LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:object)\', last_convocation_sent DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', last_reminder_sent DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', presence_date_start DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', presence_date_end DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', slot_presence_duration INT DEFAULT NULL, presence_status TINYINT(1) NOT NULL, manual_presence_status TINYINT(1) NOT NULL, provider_session_access_token_token VARCHAR(255) NOT NULL, provider_session_access_token_tag VARCHAR(255) NOT NULL, provider_session_access_token_iv VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_5CE38CB3A0125C78 (provider_session_access_token_token), INDEX IDX_5CE38CB39D1C3019 (participant_id), INDEX IDX_5CE38CB3613FECDF (session_id), UNIQUE INDEX pprs_session_participant (participant_id, session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE provider_session (id INT AUTO_INCREMENT NOT NULL, abstract_provider_configuration_holder_id INT DEFAULT NULL, convocation_id INT DEFAULT NULL, created_by INT DEFAULT NULL, updated_by INT DEFAULT NULL, provider_audio_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, ref VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', date_start DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', date_end DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', business_number VARCHAR(255) DEFAULT NULL, category INT DEFAULT NULL, duration INT DEFAULT NULL, information VARCHAR(255) DEFAULT NULL, language VARCHAR(255) DEFAULT NULL, origin VARCHAR(255) DEFAULT NULL, tags JSON DEFAULT NULL, notification_date DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', date_of_sending_reminders LONGTEXT NOT NULL COMMENT \'(DC2Type:object)\', attachment VARCHAR(255) DEFAULT NULL, connexion_type_session VARCHAR(255) DEFAULT NULL, send_invitation_emails TINYINT(1) NOT NULL, enable_send_reminders_to_animators TINYINT(1) NOT NULL, session_test TINYINT(1) NOT NULL, personalized_content LONGTEXT DEFAULT NULL, last_convocation_sent DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', last_reminder_sent DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', status SMALLINT NOT NULL, nature INT DEFAULT NULL, objectives JSON DEFAULT NULL, discr VARCHAR(255) NOT NULL, INDEX IDX_201C7773884BA92F (abstract_provider_configuration_holder_id), INDEX IDX_201C7773E8746F65 (convocation_id), INDEX IDX_201C7773DE12AB56 (created_by), INDEX IDX_201C777316FE72E1 (updated_by), INDEX IDX_201C777372734D2E (provider_audio_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE provider_session_evaluation (provider_session_id INT NOT NULL, evaluation_id INT NOT NULL, INDEX IDX_B0BF2C2D4489A067 (provider_session_id), INDEX IDX_B0BF2C2D456C5646 (evaluation_id), PRIMARY KEY(provider_session_id, evaluation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, status SMALLINT NOT NULL, password VARCHAR(255) NOT NULL, reference VARCHAR(255) DEFAULT NULL, company VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, preferred_lang VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', last_login DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', options JSON NOT NULL, password_reset_token VARCHAR(255) DEFAULT NULL, last_password_reset DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', profile_picture_filename VARCHAR(255) DEFAULT NULL, preferred_initial_route VARCHAR(255) DEFAULT \'_home\' NOT NULL, information VARCHAR(255) DEFAULT NULL, reference_client VARCHAR(255) DEFAULT NULL, preferences JSON NOT NULL, INDEX IDX_8D93D64919EB6921 (client_id), UNIQUE INDEX users_idx (email, client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE webex_configuration_holder (id INT NOT NULL, site_name VARCHAR(255) NOT NULL, default_options JSON NOT NULL, licences JSON NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE webex_meeting (id INT NOT NULL, meeting_password VARCHAR(255) DEFAULT NULL, maximum_user_count INT DEFAULT NULL, meeting_key BIGINT NOT NULL, open_time INT DEFAULT 900 NOT NULL, host_icalendar_url LONGTEXT DEFAULT NULL, attendee_icalendar_url LONGTEXT DEFAULT NULL, licence VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE webex_meeting_telephony_profile (id INT NOT NULL, linked_principal_id INT DEFAULT NULL, webex_meeting_default_phone_number VARCHAR(255) DEFAULT NULL, code_animator VARCHAR(255) DEFAULT NULL, code_participant VARCHAR(255) DEFAULT NULL, INDEX IDX_98619BFE97135B4 (linked_principal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE webex_participant (id INT NOT NULL, contact_identifier INT DEFAULT NULL, active TINYINT(1) DEFAULT 1 NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE webex_participant_abstract_webex_session (webex_participant_id INT NOT NULL, abstract_webex_session_id INT NOT NULL, INDEX IDX_9A7E734E2BC22BD6 (webex_participant_id), INDEX IDX_9A7E734EE750A910 (abstract_webex_session_id), PRIMARY KEY(webex_participant_id, abstract_webex_session_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE webex_session_attendee (id INT AUTO_INCREMENT NOT NULL, webex_participant_id INT DEFAULT NULL, webex_session_id INT DEFAULT NULL, INDEX IDX_5CFB01BC2BC22BD6 (webex_participant_id), INDEX IDX_5CFB01BC8A4783D9 (webex_session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE abstract_provider_audio ADD CONSTRAINT FK_35CB1C1054EA4E8B FOREIGN KEY (configuration_holder_id) REFERENCES abstract_provider_configuration_holder (id)');
        $this->addSql('ALTER TABLE abstract_provider_configuration_holder ADD CONSTRAINT FK_C3B46EA019EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE abstract_webex_session ADD CONSTRAINT FK_9C6C2605BF396750 FOREIGN KEY (id) REFERENCES provider_session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE activity_log ADD CONSTRAINT FK_FD06F6478B571DAD FOREIGN KEY (client_origin) REFERENCES client (id)');
        $this->addSql('ALTER TABLE activity_log ADD CONSTRAINT FK_FD06F647BBA34D23 FOREIGN KEY (user_origin) REFERENCES user (id)');
        $this->addSql('ALTER TABLE adobe_connect_configuration_holder ADD CONSTRAINT FK_53963F4DBF396750 FOREIGN KEY (id) REFERENCES abstract_provider_configuration_holder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adobe_connect_meeting_attendee ADD CONSTRAINT FK_54DA304DC8DCAD FOREIGN KEY (adobe_connect_principal_id) REFERENCES adobe_connect_principal (id)');
        $this->addSql('ALTER TABLE adobe_connect_meeting_attendee ADD CONSTRAINT FK_54DA30F40E4192 FOREIGN KEY (adobe_connect_sco_id) REFERENCES adobe_connect_sco (id)');
        $this->addSql('ALTER TABLE adobe_connect_principal ADD CONSTRAINT FK_FEAFD5C0BF396750 FOREIGN KEY (id) REFERENCES provider_participant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adobe_connect_principal_adobe_connect_sco ADD CONSTRAINT FK_FFBE67954DC8DCAD FOREIGN KEY (adobe_connect_principal_id) REFERENCES adobe_connect_principal (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adobe_connect_principal_adobe_connect_sco ADD CONSTRAINT FK_FFBE6795F40E4192 FOREIGN KEY (adobe_connect_sco_id) REFERENCES adobe_connect_sco (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adobe_connect_sco ADD CONSTRAINT FK_43485FA8BF396750 FOREIGN KEY (id) REFERENCES provider_session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adobe_connect_telephony_profile ADD CONSTRAINT FK_C55FE327E97135B4 FOREIGN KEY (linked_principal_id) REFERENCES adobe_connect_principal (id)');
        $this->addSql('ALTER TABLE adobe_connect_telephony_profile ADD CONSTRAINT FK_C55FE327BF396750 FOREIGN KEY (id) REFERENCES abstract_provider_audio (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE async_task ADD CONSTRAINT FK_3B0E64FC8B571DAD FOREIGN KEY (client_origin) REFERENCES client (id)');
        $this->addSql('ALTER TABLE async_task ADD CONSTRAINT FK_3B0E64FCBBA34D23 FOREIGN KEY (user_origin) REFERENCES user (id)');
        $this->addSql('ALTER TABLE bounce ADD CONSTRAINT FK_823CF459613FECDF FOREIGN KEY (session_id) REFERENCES provider_session (id)');
        $this->addSql('ALTER TABLE bounce ADD CONSTRAINT FK_823CF459A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE client_selfsubscription_module ADD CONSTRAINT FK_8CE6E3E19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE client_selfsubscription_module_thema ADD CONSTRAINT FK_9AD3A8F1CF16A43C FOREIGN KEY (client_selfsubscription_module_id) REFERENCES client_selfsubscription_module (id)');
        $this->addSql('ALTER TABLE client_selfsubscription_module_thema_subject ADD CONSTRAINT FK_C71F07EAAB4369C2 FOREIGN KEY (client_selfsubscription_module_thema_id) REFERENCES client_selfsubscription_module_thema (id)');
        $this->addSql('ALTER TABLE client_services ADD CONSTRAINT FK_5DD4D1AC19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE convocation ADD CONSTRAINT FK_C03B3F5F19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE evaluation ADD CONSTRAINT FK_1323A57519EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE evaluation_participant_answer ADD CONSTRAINT FK_2B2243A2F6CE1A0 FOREIGN KEY (provider_participant_session_role_id) REFERENCES provider_participant_session_role (id)');
        $this->addSql('ALTER TABLE evaluation_participant_answer ADD CONSTRAINT FK_2B2243A2456C5646 FOREIGN KEY (evaluation_id) REFERENCES evaluation (id)');
        $this->addSql('ALTER TABLE microsoft_teams_configuration_holder ADD CONSTRAINT FK_5FF173FBF396750 FOREIGN KEY (id) REFERENCES abstract_provider_configuration_holder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE microsoft_teams_participant ADD CONSTRAINT FK_C5E26000BF396750 FOREIGN KEY (id) REFERENCES provider_participant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE microsoft_teams_session ADD CONSTRAINT FK_4192D711BF396750 FOREIGN KEY (id) REFERENCES provider_session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE provider_participant ADD CONSTRAINT FK_1200E6E3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE provider_participant ADD CONSTRAINT FK_1200E6E354EA4E8B FOREIGN KEY (configuration_holder_id) REFERENCES abstract_provider_configuration_holder (id)');
        $this->addSql('ALTER TABLE provider_participant_session_role ADD CONSTRAINT FK_5CE38CB39D1C3019 FOREIGN KEY (participant_id) REFERENCES provider_participant (id)');
        $this->addSql('ALTER TABLE provider_participant_session_role ADD CONSTRAINT FK_5CE38CB3613FECDF FOREIGN KEY (session_id) REFERENCES provider_session (id)');
        $this->addSql('ALTER TABLE provider_session ADD CONSTRAINT FK_201C7773884BA92F FOREIGN KEY (abstract_provider_configuration_holder_id) REFERENCES abstract_provider_configuration_holder (id)');
        $this->addSql('ALTER TABLE provider_session ADD CONSTRAINT FK_201C7773E8746F65 FOREIGN KEY (convocation_id) REFERENCES convocation (id)');
        $this->addSql('ALTER TABLE provider_session ADD CONSTRAINT FK_201C7773DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE provider_session ADD CONSTRAINT FK_201C777316FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE provider_session ADD CONSTRAINT FK_201C777372734D2E FOREIGN KEY (provider_audio_id) REFERENCES abstract_provider_audio (id)');
        $this->addSql('ALTER TABLE provider_session_evaluation ADD CONSTRAINT FK_B0BF2C2D4489A067 FOREIGN KEY (provider_session_id) REFERENCES provider_session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE provider_session_evaluation ADD CONSTRAINT FK_B0BF2C2D456C5646 FOREIGN KEY (evaluation_id) REFERENCES evaluation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64919EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE webex_configuration_holder ADD CONSTRAINT FK_D05909DABF396750 FOREIGN KEY (id) REFERENCES abstract_provider_configuration_holder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webex_meeting ADD CONSTRAINT FK_E870796BBF396750 FOREIGN KEY (id) REFERENCES provider_session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webex_meeting_telephony_profile ADD CONSTRAINT FK_98619BFE97135B4 FOREIGN KEY (linked_principal_id) REFERENCES webex_meeting (id)');
        $this->addSql('ALTER TABLE webex_meeting_telephony_profile ADD CONSTRAINT FK_98619BFBF396750 FOREIGN KEY (id) REFERENCES abstract_provider_audio (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webex_participant ADD CONSTRAINT FK_3E17B64BBF396750 FOREIGN KEY (id) REFERENCES provider_participant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webex_participant_abstract_webex_session ADD CONSTRAINT FK_9A7E734E2BC22BD6 FOREIGN KEY (webex_participant_id) REFERENCES webex_participant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webex_participant_abstract_webex_session ADD CONSTRAINT FK_9A7E734EE750A910 FOREIGN KEY (abstract_webex_session_id) REFERENCES abstract_webex_session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE webex_session_attendee ADD CONSTRAINT FK_5CFB01BC2BC22BD6 FOREIGN KEY (webex_participant_id) REFERENCES webex_participant (id)');
        $this->addSql('ALTER TABLE webex_session_attendee ADD CONSTRAINT FK_5CFB01BC8A4783D9 FOREIGN KEY (webex_session_id) REFERENCES abstract_webex_session (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adobe_connect_telephony_profile DROP FOREIGN KEY FK_C55FE327BF396750');
        $this->addSql('ALTER TABLE provider_session DROP FOREIGN KEY FK_201C777372734D2E');
        $this->addSql('ALTER TABLE webex_meeting_telephony_profile DROP FOREIGN KEY FK_98619BFBF396750');
        $this->addSql('ALTER TABLE abstract_provider_audio DROP FOREIGN KEY FK_35CB1C1054EA4E8B');
        $this->addSql('ALTER TABLE adobe_connect_configuration_holder DROP FOREIGN KEY FK_53963F4DBF396750');
        $this->addSql('ALTER TABLE microsoft_teams_configuration_holder DROP FOREIGN KEY FK_5FF173FBF396750');
        $this->addSql('ALTER TABLE provider_participant DROP FOREIGN KEY FK_1200E6E354EA4E8B');
        $this->addSql('ALTER TABLE provider_session DROP FOREIGN KEY FK_201C7773884BA92F');
        $this->addSql('ALTER TABLE webex_configuration_holder DROP FOREIGN KEY FK_D05909DABF396750');
        $this->addSql('ALTER TABLE webex_participant_abstract_webex_session DROP FOREIGN KEY FK_9A7E734EE750A910');
        $this->addSql('ALTER TABLE webex_session_attendee DROP FOREIGN KEY FK_5CFB01BC8A4783D9');
        $this->addSql('ALTER TABLE adobe_connect_meeting_attendee DROP FOREIGN KEY FK_54DA304DC8DCAD');
        $this->addSql('ALTER TABLE adobe_connect_principal_adobe_connect_sco DROP FOREIGN KEY FK_FFBE67954DC8DCAD');
        $this->addSql('ALTER TABLE adobe_connect_telephony_profile DROP FOREIGN KEY FK_C55FE327E97135B4');
        $this->addSql('ALTER TABLE adobe_connect_meeting_attendee DROP FOREIGN KEY FK_54DA30F40E4192');
        $this->addSql('ALTER TABLE adobe_connect_principal_adobe_connect_sco DROP FOREIGN KEY FK_FFBE6795F40E4192');
        $this->addSql('ALTER TABLE abstract_provider_configuration_holder DROP FOREIGN KEY FK_C3B46EA019EB6921');
        $this->addSql('ALTER TABLE activity_log DROP FOREIGN KEY FK_FD06F6478B571DAD');
        $this->addSql('ALTER TABLE async_task DROP FOREIGN KEY FK_3B0E64FC8B571DAD');
        $this->addSql('ALTER TABLE client_selfsubscription_module DROP FOREIGN KEY FK_8CE6E3E19EB6921');
        $this->addSql('ALTER TABLE client_services DROP FOREIGN KEY FK_5DD4D1AC19EB6921');
        $this->addSql('ALTER TABLE convocation DROP FOREIGN KEY FK_C03B3F5F19EB6921');
        $this->addSql('ALTER TABLE evaluation DROP FOREIGN KEY FK_1323A57519EB6921');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64919EB6921');
        $this->addSql('ALTER TABLE client_selfsubscription_module_thema DROP FOREIGN KEY FK_9AD3A8F1CF16A43C');
        $this->addSql('ALTER TABLE client_selfsubscription_module_thema_subject DROP FOREIGN KEY FK_C71F07EAAB4369C2');
        $this->addSql('ALTER TABLE provider_session DROP FOREIGN KEY FK_201C7773E8746F65');
        $this->addSql('ALTER TABLE evaluation_participant_answer DROP FOREIGN KEY FK_2B2243A2456C5646');
        $this->addSql('ALTER TABLE provider_session_evaluation DROP FOREIGN KEY FK_B0BF2C2D456C5646');
        $this->addSql('ALTER TABLE adobe_connect_principal DROP FOREIGN KEY FK_FEAFD5C0BF396750');
        $this->addSql('ALTER TABLE microsoft_teams_participant DROP FOREIGN KEY FK_C5E26000BF396750');
        $this->addSql('ALTER TABLE provider_participant_session_role DROP FOREIGN KEY FK_5CE38CB39D1C3019');
        $this->addSql('ALTER TABLE webex_participant DROP FOREIGN KEY FK_3E17B64BBF396750');
        $this->addSql('ALTER TABLE evaluation_participant_answer DROP FOREIGN KEY FK_2B2243A2F6CE1A0');
        $this->addSql('ALTER TABLE abstract_webex_session DROP FOREIGN KEY FK_9C6C2605BF396750');
        $this->addSql('ALTER TABLE adobe_connect_sco DROP FOREIGN KEY FK_43485FA8BF396750');
        $this->addSql('ALTER TABLE bounce DROP FOREIGN KEY FK_823CF459613FECDF');
        $this->addSql('ALTER TABLE microsoft_teams_session DROP FOREIGN KEY FK_4192D711BF396750');
        $this->addSql('ALTER TABLE provider_participant_session_role DROP FOREIGN KEY FK_5CE38CB3613FECDF');
        $this->addSql('ALTER TABLE provider_session_evaluation DROP FOREIGN KEY FK_B0BF2C2D4489A067');
        $this->addSql('ALTER TABLE webex_meeting DROP FOREIGN KEY FK_E870796BBF396750');
        $this->addSql('ALTER TABLE activity_log DROP FOREIGN KEY FK_FD06F647BBA34D23');
        $this->addSql('ALTER TABLE async_task DROP FOREIGN KEY FK_3B0E64FCBBA34D23');
        $this->addSql('ALTER TABLE bounce DROP FOREIGN KEY FK_823CF459A76ED395');
        $this->addSql('ALTER TABLE provider_participant DROP FOREIGN KEY FK_1200E6E3A76ED395');
        $this->addSql('ALTER TABLE provider_session DROP FOREIGN KEY FK_201C7773DE12AB56');
        $this->addSql('ALTER TABLE provider_session DROP FOREIGN KEY FK_201C777316FE72E1');
        $this->addSql('ALTER TABLE webex_meeting_telephony_profile DROP FOREIGN KEY FK_98619BFE97135B4');
        $this->addSql('ALTER TABLE webex_participant_abstract_webex_session DROP FOREIGN KEY FK_9A7E734E2BC22BD6');
        $this->addSql('ALTER TABLE webex_session_attendee DROP FOREIGN KEY FK_5CFB01BC2BC22BD6');
        $this->addSql('DROP TABLE abstract_provider_audio');
        $this->addSql('DROP TABLE abstract_provider_configuration_holder');
        $this->addSql('DROP TABLE abstract_webex_session');
        $this->addSql('DROP TABLE activity_log');
        $this->addSql('DROP TABLE adobe_connect_configuration_holder');
        $this->addSql('DROP TABLE adobe_connect_meeting_attendee');
        $this->addSql('DROP TABLE adobe_connect_principal');
        $this->addSql('DROP TABLE adobe_connect_principal_adobe_connect_sco');
        $this->addSql('DROP TABLE adobe_connect_sco');
        $this->addSql('DROP TABLE adobe_connect_telephony_profile');
        $this->addSql('DROP TABLE async_task');
        $this->addSql('DROP TABLE bounce');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE client_selfsubscription_module');
        $this->addSql('DROP TABLE client_selfsubscription_module_thema');
        $this->addSql('DROP TABLE client_selfsubscription_module_thema_subject');
        $this->addSql('DROP TABLE client_services');
        $this->addSql('DROP TABLE convocation');
        $this->addSql('DROP TABLE evaluation');
        $this->addSql('DROP TABLE evaluation_participant_answer');
        $this->addSql('DROP TABLE microsoft_teams_configuration_holder');
        $this->addSql('DROP TABLE microsoft_teams_participant');
        $this->addSql('DROP TABLE microsoft_teams_session');
        $this->addSql('DROP TABLE provider_participant');
        $this->addSql('DROP TABLE provider_participant_session_role');
        $this->addSql('DROP TABLE provider_session');
        $this->addSql('DROP TABLE provider_session_evaluation');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE webex_configuration_holder');
        $this->addSql('DROP TABLE webex_meeting');
        $this->addSql('DROP TABLE webex_meeting_telephony_profile');
        $this->addSql('DROP TABLE webex_participant');
        $this->addSql('DROP TABLE webex_participant_abstract_webex_session');
        $this->addSql('DROP TABLE webex_session_attendee');
    }
}
