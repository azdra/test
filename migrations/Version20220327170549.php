<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220327170549 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_participant_session_role ADD situation_mixte INT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE provider_session ADD restlunch INT DEFAULT 0 NOT NULL, ADD nbrdays INT DEFAULT 1 NOT NULL, ADD location LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE provider_participant_session_role DROP situation_mixte');
        $this->addSql('ALTER TABLE provider_session DROP restlunch, DROP nbrdays, DROP location');
    }
}
