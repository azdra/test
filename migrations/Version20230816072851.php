<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230816072851 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // Load the existing records and update the JSON column
        $rows = $this->connection->fetchAllAssociative('SELECT id, additional_timezones FROM provider_session');
        foreach ($rows as $row) {
            $id = $row['id'];
            $additionalTimezones = json_decode($row['additional_timezones'], true);
            if ($additionalTimezones === null) {
                $additionalTimezones = [];
            }
            $this->addSql('UPDATE provider_session SET additional_timezones = :additionalTimezones WHERE id = :id', [
                'id' => $id,
                'additionalTimezones' => json_encode($additionalTimezones),
            ]);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
