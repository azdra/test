<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240216154416 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE on_hold_provider_session DROP FOREIGN KEY FK_FFF5170016FE72E1');
        $this->addSql('DROP INDEX idx_category ON on_hold_provider_session');
        $this->addSql('DROP INDEX idx_date_start ON on_hold_provider_session');
        $this->addSql('DROP INDEX IDX_FFF5170016FE72E1 ON on_hold_provider_session');
        $this->addSql('ALTER TABLE on_hold_provider_session ADD updated_by_id INT DEFAULT NULL, ADD title VARCHAR(255) NOT NULL, ADD connector_type VARCHAR(255) NOT NULL, ADD nbr_max_participants INT NOT NULL, ADD participants JSON NOT NULL, DROP updated_by, DROP ref, DROP date_start, DROP date_end, DROP duration, DROP information, DROP language, DROP restlunch, DROP nbrdays, DROP description, DROP description2, CHANGE name mega_uuid VARCHAR(255) NOT NULL, CHANGE status status_validation SMALLINT NOT NULL, CHANGE category nbr_min_participants INT NOT NULL, CHANGE additional_timezones slots_dates JSON NOT NULL');
        $this->addSql('ALTER TABLE on_hold_provider_session ADD CONSTRAINT FK_FFF51700896DBBDE FOREIGN KEY (updated_by_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_FFF51700896DBBDE ON on_hold_provider_session (updated_by_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE on_hold_provider_session DROP FOREIGN KEY FK_FFF51700896DBBDE');
        $this->addSql('DROP INDEX IDX_FFF51700896DBBDE ON on_hold_provider_session');
        $this->addSql('ALTER TABLE on_hold_provider_session ADD name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD ref VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ADD date_start DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', ADD date_end DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime)\', ADD category INT NOT NULL, ADD duration INT DEFAULT NULL, ADD information LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ADD language VARCHAR(255) DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ADD restlunch INT DEFAULT 0 NOT NULL, ADD nbrdays INT DEFAULT 1 NOT NULL, ADD description LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ADD description2 LONGTEXT DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, ADD additional_timezones JSON NOT NULL, DROP mega_uuid, DROP title, DROP connector_type, DROP nbr_min_participants, DROP nbr_max_participants, DROP slots_dates, DROP participants, CHANGE updated_by_id updated_by INT DEFAULT NULL, CHANGE status_validation status SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE on_hold_provider_session ADD CONSTRAINT FK_FFF5170016FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
    }
}
