<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231120151656 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE video_replay_viewer (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, video_replay_id INT DEFAULT NULL, seen INT NOT NULL, last_show_replay DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_C4AA859CA76ED395 (user_id), INDEX IDX_C4AA859CDA139E74 (video_replay_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE video_replay_viewer ADD CONSTRAINT FK_C4AA859CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE video_replay_viewer ADD CONSTRAINT FK_C4AA859CDA139E74 FOREIGN KEY (video_replay_id) REFERENCES video_replay (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE video_replay_viewer DROP FOREIGN KEY FK_C4AA859CA76ED395');
        $this->addSql('ALTER TABLE video_replay_viewer DROP FOREIGN KEY FK_C4AA859CDA139E74');
        $this->addSql('DROP TABLE video_replay_viewer');
    }
}
