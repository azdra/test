<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230502093334 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client_services ADD lump_sum_contract TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE help_need ADD additional_information LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:object)\'');
        $this->addSql('ALTER TABLE registration_page_template CHANGE block_lower_banner_privacy_check block_lower_banner_privacy_check TINYINT(1) DEFAULT false NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE client_services DROP lump_sum_contract');
        $this->addSql('ALTER TABLE help_need DROP additional_information');
        $this->addSql('ALTER TABLE registration_page_template CHANGE block_lower_banner_privacy_check block_lower_banner_privacy_check TINYINT(1) DEFAULT 0 NOT NULL');
    }
}
