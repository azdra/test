<?php

namespace App\RenaultRobot\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class RenaultImportSessionTemp
{
    public const WAITING = 1; // En attente d'importation
    public const SUCCESS = 2; // Succès de l'import
    public const WARNING = 3; // Avertissement lors de l'import
    public const ERROR = 4;   // Erreur lors de l'import

    public const ROLE_ANIMATOR = 1; //Rôle animateur
    public const ROLE_TRAINEE = 2; //Rôle trainee

    public const MEETING_STATUS_OPEN = 'Open - Normal'; //'Open – Normal';
    public const MEETING_STATUS_CANCELLED = 'Cancelled - Normal'; //'Cancelled – Normal';
    public const MEETING_STATUS_DELIVERED = 'Delivered - Normal'; //'Delivered – Normal';
    public const MEETING_STATUS_BILLED = 'Billed - Normal'; //'Billed – Normal';

    public const TRAINEE_STATUS_REGISTERED = 'Registered';
    public const TRAINEE_STATUS_DELIVERED = 'Delivered';
    public const TRAINEE_STATUS_CANCELLED_FEES = 'Cancelled with charge';
    public const TRAINEE_STATUS_CANCELLED = 'Cancelled';

    public const EMAIL_RETAIL_STANDARD = '@renaultfictif.com';
    //public const EMAIL_RETAIL_TOSEND = 'rltlandingemail@mylivesession.com';
    //public const USER_GESTIONNAIRE_ID = 164610;    //rlt.importauto@mylivesession.com
    //public const RLT_CLIENT_ID = 5;   //Compte client Renault
    //public const TEMP_TEST = 'TESTCCURLT_';
    public const TEMP_TEST = '';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    protected int $id;

    #[ORM\Column(type: 'bigint')]
    protected int $numRow;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $model;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $referenceLiveSession = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $referenceRenault = null;

    #[ORM\Column(type: 'integer', nullable: false, options: ['default' => self::WAITING])]
    protected ?int $resultType = self::WAITING;

    #[ORM\Column(type: 'text', nullable: true)]
    protected string $resultText;

    #[ORM\Column(type: 'datetime_immutable')]
    protected \DateTimeImmutable $dateStart;

    #[ORM\Column(type: 'datetime_immutable')]
    protected \DateTimeImmutable $dateEnd;

    #[ORM\Column(type: 'integer', nullable: false, options: ['default' => 1])]
    protected ?int $nbrDays = 1;

    #[ORM\Column(type: 'integer', nullable: false, options: ['default' => 1])]
    protected ?int $numDay = 1;

    #[ORM\Column(type: 'datetime_immutable')]
    protected \DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    protected \DateTimeImmutable $importedAt;

    #[ORM\Column(type: 'integer', nullable: false, options: ['default' => self::ROLE_TRAINEE])]
    protected ?int $role = self::ROLE_TRAINEE;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $animatorName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $animatorForname;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $animatorEmail;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $traineeName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $traineeForname;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $traineeEmail;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $traineeIPN = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $employeeID = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $statusSession = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $statusTrainee = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $resultSession = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $resultTrainee = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected ?string $resultAnimator = null;

    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $urlConnexionTrainee = null;

    #[ORM\Column(type: 'text', nullable: true)]
    protected string $urlSession;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable('now');
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getNumRow(): int
    {
        return $this->numRow;
    }

    public function setNumRow(int $numRow): self
    {
        $this->numRow = $numRow;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getModel(): string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getReferenceLiveSession(): ?string
    {
        return $this->referenceLiveSession;
    }

    public function setReferenceLiveSession(?string $referenceLiveSession): self
    {
        $this->referenceLiveSession = $referenceLiveSession;

        return $this;
    }

    public function getReferenceRenault(): ?string
    {
        return $this->referenceRenault;
    }

    public function setReferenceRenault(?string $referenceRenault): self
    {
        $this->referenceRenault = $referenceRenault;

        return $this;
    }

    public function getResultType(): ?int
    {
        return $this->resultType;
    }

    public function setResultType(?int $resultType): self
    {
        $this->resultType = $resultType;

        return $this;
    }

    public function getResultText(): string
    {
        return $this->resultText;
    }

    public function setResultText(string $resultText): self
    {
        $this->resultText = $resultText;

        return $this;
    }

    public function setDateStart(\DateTimeImmutable $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateStart(): \DateTimeImmutable
    {
        return $this->dateStart;
    }

    public function setDateEnd(\DateTimeImmutable $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getDateEnd(): \DateTimeImmutable
    {
        return $this->dateEnd;
    }

    public function getNbrDays(): ?int
    {
        return $this->nbrDays;
    }

    public function setNbrDays(?int $nbrDays): self
    {
        $this->nbrDays = $nbrDays;

        return $this;
    }

    public function getNumDay(): ?int
    {
        return $this->numDay;
    }

    public function setNumDay(?int $numDay): self
    {
        $this->numDay = $numDay;

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getImportedAt(): \DateTimeImmutable
    {
        return $this->importedAt;
    }

    public function setImportedAt(\DateTimeImmutable $importedAt): self
    {
        $this->importedAt = $importedAt;

        return $this;
    }

    public function getRole(): ?int
    {
        return $this->role;
    }

    public function setRole(?int $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getAnimatorName(): string
    {
        return $this->animatorName;
    }

    public function setAnimatorName(string $animatorName): self
    {
        $this->animatorName = $animatorName;

        return $this;
    }

    public function getAnimatorForname(): string
    {
        return $this->animatorForname;
    }

    public function setAnimatorForname(string $animatorForname): self
    {
        $this->animatorForname = $animatorForname;

        return $this;
    }

    public function getAnimatorEmail(): string
    {
        return $this->animatorEmail;
    }

    public function setAnimatorEmail(string $animatorEmail): self
    {
        $this->animatorEmail = $animatorEmail;

        return $this;
    }

    public function getTraineeName(): string
    {
        return $this->traineeName;
    }

    public function setTraineeName(string $traineeName): self
    {
        $this->traineeName = $traineeName;

        return $this;
    }

    public function getTraineeForname(): string
    {
        return $this->traineeForname;
    }

    public function setTraineeForname(string $traineeForname): self
    {
        $this->traineeForname = $traineeForname;

        return $this;
    }

    public function getTraineeEmail(): string
    {
        return $this->traineeEmail;
    }

    public function setTraineeEmail(string $traineeEmail): self
    {
        $this->traineeEmail = $traineeEmail;

        return $this;
    }

    public function getTraineeIPN(): ?string
    {
        return $this->traineeIPN;
    }

    public function setTraineeIPN(?string $traineeIPN): self
    {
        $this->traineeIPN = $traineeIPN;

        return $this;
    }

    public function getEmployeeID(): ?string
    {
        return $this->employeeID;
    }

    public function setEmployeeID(?string $employeeID): self
    {
        $this->employeeID = $employeeID;

        return $this;
    }

    public function getStatusSession(): ?string
    {
        return $this->statusSession;
    }

    public function setStatusSession(?string $statusSession): self
    {
        $this->statusSession = $statusSession;

        return $this;
    }

    public function getStatusTrainee(): ?string
    {
        return $this->statusTrainee;
    }

    public function setStatusTrainee(?string $statusTrainee): self
    {
        $this->statusTrainee = $statusTrainee;

        return $this;
    }

    public function getResultSession(): ?string
    {
        return $this->resultSession;
    }

    public function setResultSession(?string $resultSession): self
    {
        $this->resultSession = $resultSession;

        return $this;
    }

    public function getResultTrainee(): ?string
    {
        return $this->resultTrainee;
    }

    public function setResultTrainee(?string $resultTrainee): self
    {
        $this->resultTrainee = $resultTrainee;

        return $this;
    }

    public function getResultAnimator(): ?string
    {
        return $this->resultAnimator;
    }

    public function setResultAnimator(?string $resultAnimator): self
    {
        $this->resultAnimator = $resultAnimator;

        return $this;
    }

    public function getUrlConnexionTrainee(): ?string
    {
        return $this->urlConnexionTrainee;
    }

    public function setUrlConnexionTrainee(?string $urlConnexionTrainee): self
    {
        $this->urlConnexionTrainee = $urlConnexionTrainee;

        return $this;
    }

    public function getUrlSession(): string
    {
        return $this->urlSession;
    }

    public function setUrlSession(string $urlSession): self
    {
        $this->urlSession = $urlSession;

        return $this;
    }

    public function meetingIsWaitingScheduling(): bool
    {
        //dump(trim($this->statusSession).' | '.self::MEETING_STATUS_OPEN);
        return trim($this->statusSession) === self::MEETING_STATUS_OPEN;
    }

    public function meetingIsCancelled(): bool
    {
        //dump(trim($this->statusSession).' | '.self::MEETING_STATUS_CANCELLED);
        return trim($this->statusSession) === self::MEETING_STATUS_CANCELLED;
    }

    public function meetingIsBilled(): bool
    {
        //dump(trim($this->statusSession).' | '.self::MEETING_STATUS_BILLED);
        return trim($this->statusSession) === self::MEETING_STATUS_BILLED;
    }

    public function meetingIsDelivered(): bool
    {
        //dump(trim($this->statusSession).' | '.self::MEETING_STATUS_DELIVERED);
        return trim($this->statusSession) === self::MEETING_STATUS_DELIVERED;
    }

    public function traineeIsRegistered(): bool
    {
        return trim($this->statusTrainee) === self::TRAINEE_STATUS_REGISTERED;
    }

    public function traineeIsDelivered(): bool
    {
        return trim($this->statusTrainee) === self::TRAINEE_STATUS_DELIVERED;
    }

    public function traineeIsCanceled(): bool
    {
        return trim($this->statusTrainee) === self::TRAINEE_STATUS_CANCELLED;
    }

    public function traineeIsCanceledFees(): bool
    {
        return trim($this->statusTrainee) === self::TRAINEE_STATUS_CANCELLED_FEES;
    }
}
