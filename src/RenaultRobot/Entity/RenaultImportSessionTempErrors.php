<?php

namespace App\RenaultRobot\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class RenaultImportSessionTempErrors
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    protected int $id;

    #[ORM\Column(type: 'bigint')]
    protected int $numRow;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $referenceRenault;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $statusFormation;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $rowIPN;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $rowEmployeeID;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $rowEmailTrainee;

    #[ORM\Column(type: 'json', nullable: true)]
    protected array $errors;

    #[ORM\Column(type: 'datetime_immutable')]
    protected \DateTimeImmutable $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable('now');
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getNumRow(): int
    {
        return $this->numRow;
    }

    public function setNumRow(int $numRow): self
    {
        $this->numRow = $numRow;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getReferenceRenault(): string
    {
        return $this->referenceRenault;
    }

    public function setReferenceRenault(string $referenceRenault): self
    {
        $this->referenceRenault = $referenceRenault;

        return $this;
    }

    public function getStatusFormation(): string
    {
        return $this->statusFormation;
    }

    public function setStatusFormation(string $statusFormation): self
    {
        $this->statusFormation = $statusFormation;

        return $this;
    }

    public function getRowIPN(): string
    {
        return $this->rowIPN;
    }

    public function setRowIPN(string $rowIPN): self
    {
        $this->rowIPN = $rowIPN;

        return $this;
    }

    public function getRowEmployeeID(): string
    {
        return $this->rowEmployeeID;
    }

    public function setRowEmployeeID(string $rowEmployeeID): self
    {
        $this->rowEmployeeID = $rowEmployeeID;

        return $this;
    }

    public function getRowEmailTrainee(): string
    {
        return $this->rowEmailTrainee;
    }

    public function setRowEmailTrainee(string $rowEmailTrainee): self
    {
        $this->rowEmailTrainee = $rowEmailTrainee;

        return $this;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function setErrors(array $errors): self
    {
        $this->errors = $errors;

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
