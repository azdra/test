<?php

namespace App\RenaultRobot\Exception;

use App\Exception\MiddlewareException;

class RenaultTraineeImportInvalidStatusException extends MiddlewareException
{
}
