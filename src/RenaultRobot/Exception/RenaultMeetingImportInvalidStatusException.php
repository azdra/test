<?php

namespace App\RenaultRobot\Exception;

use App\Exception\MiddlewareException;

class RenaultMeetingImportInvalidStatusException extends MiddlewareException
{
}
