<?php

namespace App\RenaultRobot\Service;

use App\Entity\ActivityLog;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\CategorySessionType;
use App\Entity\Client\Client;
use App\Entity\Client\ClientServices;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\Convocation;
use App\Entity\ProviderSession;
use App\Entity\WebinarConvocation;
use App\Exception\InvalidImportDataException;
use App\Exception\MiddlewareException;
use App\Exception\ProviderSessionRequestHandlerException;
use App\RenaultRobot\Entity\RenaultImportSessionTemp;
use App\RenaultRobot\Exception\RenaultMeetingImportInvalidStatusException;
use App\RenaultRobot\Repository\RenaultImportSessionTempErrorsRepository;
use App\RenaultRobot\Repository\RenaultImportSessionTempRepository;
use App\Repository\ClientRepository;
use App\Repository\ConvocationRepository;
use App\Repository\EvaluationRepository;
use App\Repository\ProviderSessionRepository;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\ActivityLogger;
use App\Service\AdobeConnectService;
use App\Service\AdobeConnectWebinarService;
use App\Service\AsyncWorker\Tasks\ActionsFromImportSessionTask;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use App\Service\ProviderSessionService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RenaultMeetingImportService
{
    public const MAX_CONSECUTIVE_ROWS = 250;

    public const RESULT_MSG_MEETING_CREATED = 'meeting_created';
    public const RESULT_MSG_MEETING_RECREATED = 'meeting_recreated';
    public const RESULT_MSG_MEETING_NOCHANGE = 'meeting_nochange';
    public const RESULT_MSG_MEETING_CHANGE_DURING = 'meeting_change_during';
    public const RESULT_MSG_MEETING_INVALID_STATUS = 'meeting-invalid-status';
    public const RESULT_MSG_MEETING_CANCELLED = 'meeting-cancelled';
    public const RESULT_MSG_NO_CONFIGURATION_HOLDER_AVAILABLE = 'no-configuration-holder-available';
    public const RESULT_MSG_MEETING_UNCANCELLED = 'meeting-uncancelled';

    public const RESULT_MSG_AC_UNKNOWN_ERROR = 'ac-unknown-error';
    public const RESULT_MSG_UNKNOWN_ERROR = 'unknown-error';

    public const GROUP_CREATE = 'renault:session:create';

    //** @var RenaultImportSessionTemp[] */
    //private $_importToApplyAtEnd = [];

    public function __construct(
        protected string $renaultSavefilesDirectory,
        protected EntityManagerInterface $entityManager,
        protected ConvocationRepository $convocationRepository,
        protected EvaluationRepository $evaluationRepository,
        protected RenaultImportSessionTempRepository $renaultImportSessionTempRepository,
        protected RenaultImportSessionTempErrorsRepository $renaultImportSessionTempErrorsRepository,
        protected ClientRepository $clientRepository,
        protected ProviderSessionRepository $providerSessionRepository,
        protected SubjectRepository $subjectRepository,
        protected ActivityLogger $activityLogger,
        protected ProviderSessionService $providerSessionService,
        protected LoggerInterface $logger,
        protected TranslatorInterface $translator,
        private AdobeConnectService $adobeConnectService,
        private AdobeConnectWebinarService $adobeConnectWebinarService,
    ) {
    }

    public function purgeDatatemp(): void
    {
        $removeOldDataTemp = $this->renaultImportSessionTempRepository->removeDataFromDate();
        $removeOldDataTempErrors = $this->renaultImportSessionTempErrorsRepository->removeDataFromDate();
    }

    public function importMeetingsFromTemp(): array
    {
        /** @var RenaultImportSessionTemp[] $allRLTRowToImport */
        $allRLTRowToImport = $this->renaultImportSessionTempRepository->findAllByResultType(
            RenaultImportSessionTemp::WAITING, self::MAX_CONSECUTIVE_ROWS
        );

        //$allRTLRowWaiting = $allRLTRowToImport;
        //$i                = 0;

        //do {
        //$this->_importToApplyAtEnd = [];

        /*$tmp = [];
        foreach ( $allRTLRowWaiting as $item )
            $tmp[ $item->getId() ] = $item;
        $allRTLRowWaiting = $tmp;*/

        //dump('src/RenaultRobot/Service/RenaultMeetingImportService.php');
        //dump('line 94 | Start >>>>>>>>>>>>>>>>>>>> A traiter = '.count( $allRTLRowWaiting ).' | Réalisé = '.count($this->_importToApplyAtEnd) );

        //foreach ( $allRTLRowWaiting as $rowTemp ) {
        foreach ($allRLTRowToImport as $rowTemp) {
            try {
                //dump($rowTemp->getId());
                if (substr($rowTemp->getName(), 0, 3) == 'WEB') {
                    $this->processForWebinar($rowTemp);
                } else {
                    $this->processForMeeting($rowTemp);
                }
            } catch (RenaultMeetingImportInvalidStatusException $e) {
                $this->updateResultText(
                        $rowTemp,
                        self::RESULT_MSG_MEETING_INVALID_STATUS,
                        ['e' => $e]
                    );
            } catch (MiddlewareException $e) {
                $this->updateResultText(
                        $rowTemp,
                        self::RESULT_MSG_AC_UNKNOWN_ERROR,
                        ['e' => $e]
                    );
            } catch (\Exception $e) {
                $this->updateResultText(
                        $rowTemp,
                        self::RESULT_MSG_UNKNOWN_ERROR,
                        ['e' => $e]
                    );
            } finally {
                //$i++;
                //dump($rowTemp->getId().' | '.$rowTemp->getResultType());
                //dump($rowTemp->getResultText());
                //dump($rowTemp->getResultSession());
                //dump($rowTemp->getResultTrainee());
                //dump($rowTemp->getResultAnimator());
                $this->entityManager->persist($rowTemp);
                $this->entityManager->flush($rowTemp);
            }
        }
        //$allRTLRowWaiting = $this->_importToApplyAtEnd;
        //} while ( !empty( $this->_importToApplyAtEnd ) );

        return $allRLTRowToImport;
    }

    private function processForWebinar(RenaultImportSessionTemp &$currentRow): RenaultImportSessionTemp
    {
        echo '-'.$currentRow->getId();

        /** @var AdobeConnectWebinarSCO $meeting */
        $meeting = null;
        if (empty($currentRow->getReferenceLiveSession())) {
            $params = [
                'description' => $currentRow->getReferenceRenault(),
            ];
            $meetingsFind = $this->providerSessionRepository->findBy($params, ['dateStart' => 'ASC']);
            $findReferenceLiveSession = null;
            $findReferenceLiveSessionSameDate = null;
            $findReferenceRenault = null;
            //dump(count($meetingsFind));
            //y'a t-il des sessions avec la référence Renault ?
            foreach ($meetingsFind as $m) {
                $findReferenceRenault = $m->getDescription();
                //Est-ce que les dates correspondent en fonction du jour cible ? Ceci évite une comparaison avec une valeur S1 : 2020-06-28 08:00:00 (j1)
                //dans le cadre initial suivant:
                //S1 : 2020-06-27 08:00:00 (j1) et S2 : 2020-06-28 08:00:00 (j2)
                if ($m->getDateStart() == $currentRow->getDateStart()) {
                    $findReferenceLiveSession = $m->getRef();
                    if ($m->getDescription2() == $currentRow->getNumDay()) {
                        $findReferenceLiveSessionSameDate = $m->getRef();  //On retient la référence LS trouvée
                        $meeting = $m;                          //On retient la session de la référence
                    }
                }

                //dump($findReferenceRenault .' ||| '. $findReferenceLiveSession .' ||| '. $findReferenceLiveSessionSameDate);
                //Si une session existe avec la même réf RLT sur la même date
                if (!empty($findReferenceRenault) && !empty($findReferenceLiveSession) && !empty($findReferenceLiveSessionSameDate)) {
                    //On ajoute la référence LS aux lignes correspondantes
                    $currentRow->setReferenceLiveSession($findReferenceLiveSessionSameDate);
                    //On applique la reference LS et l'url de session à toutes les lignes concernées
                    //dump('src/RenaultRobot/Service/RenaultMeetingImportService.php | Line 178');
                    $this->renaultImportSessionTempRepository->postCreateWebinarInfos($currentRow, $meeting);
                }

                //Si une session existe avec la même réf RLT sur une autre date
                if (!empty($findReferenceRenault) && !empty($findReferenceLiveSession) && empty($findReferenceLiveSessionSameDate)) {
                    //On annule la ou les sessions correspondants à réf RLT | Idéalement il faudrait également purger les champs referencesLS et UrlSession des lignes concernées du jour
                    foreach ($meetingsFind as $m) {
                        $this->cancelWebinar($m);
                        //dump('ANNULATION DE '.$findReferenceRenault.' >>> '.$m->getRef());
                    }
                    //Il faudra recréer une session nouvelle
                    $meeting = null;
                }
            }
        } else {
            $meeting = $this->providerSessionRepository->findOneBy(['ref' => $currentRow->getReferenceLiveSession()]);
        }

        //dump('src/RenaultRobot/Service/RenaultMeetingImportService.php');
        //dump('line 181 | '. $currentRow->getId().' - '.$currentRow->getReferenceLiveSession().' - '.$currentRow->getReferenceRenault().' - '.$currentRow->getStatusSession() );
        //dump( 'line 182 | '.'En attente de traitement ('.$currentRow->meetingIsWaitingScheduling().') || Annulation : ('.$currentRow->meetingIsCancelled().') || Facturé ('.$currentRow->meetingIsBilled().') || Dispensé ('.$currentRow->meetingIsDelivered().')' );

        if ($currentRow->meetingIsWaitingScheduling()) {
            if (!is_null($meeting)) {
                //Mise à jour de la session
                $diffDateStart = $meeting->getDateStart()->diff($currentRow->getDateStart());
                $diffDateEnd = $meeting->getDateEnd()->diff($currentRow->getDateEnd());
                $dateDayChanged = $diffDateStart->d > 0 || $diffDateEnd->d > 0;
                $dateTimeChanged = $diffDateStart->h > 0 || $diffDateStart->m > 0 || $diffDateEnd->h > 0 || $diffDateEnd->m > 0;

                if ($meeting->getStatus() == ProviderSession::STATUS_CANCELED) {
                    // 0/ Si la session est annulée
                    $this->uncancelWebinar($meeting);
                    $this->updateResultText(
                        $currentRow, self::RESULT_MSG_MEETING_UNCANCELLED, ['meeting' => $meeting]
                    );
                }

                if (!$dateDayChanged && !$dateTimeChanged) {
                    // 1/ Si référence existe avec date et heure identique
                    $this->renaultImportSessionTempRepository->postCreateRow($currentRow); //On met à jour la ligne
                    $this->updateResultText(
                        $currentRow, self::RESULT_MSG_MEETING_NOCHANGE
                    );
                }

                if (!$dateDayChanged && $dateTimeChanged) {
                    // 2/ Si référence session existe avec date identique, et changement d\'heure, modification de la session
                    $this->updateWebinar($meeting);
                    //$rltRepo->postUpdateTime( $currentRow, $meeting ); //On applique les nouvelles heures à toutes les lignes concernées
                    $this->updateResultText(
                        $currentRow, self::RESULT_MSG_MEETING_CHANGE_DURING, ['meeting' => $meeting]
                    );
                }

                if ($dateDayChanged) {
                    // 3/ Si référence session existe, et changement date, suppression de la session et création d\'une nouvelle session avec la même référence
                    /** @var Client $client */
                    //$client = $this->clientRepository->find( RenaultImportSessionTemp::RLT_CLIENT_ID );
                    $client = $this->clientRepository->findOneBy(['name' => 'Renault']);
                    $this->cancelWebinar($meeting);
                    $meeting = $this->createWebinar($client, $currentRow);
                    $this->updateResultText(
                        $currentRow, self::RESULT_MSG_MEETING_RECREATED, ['meeting' => $meeting]
                    );
                }
            } else {
                // *** CREATE SESSION ***
                /** @var Client $client */
                //$client = $this->clientRepository->find( RenaultImportSessionTemp::RLT_CLIENT_ID );
                $client = $this->clientRepository->findOneBy(['name' => 'Renault']);
                /** @var AdobeConnectWebinarSCO $meeting */
                $meeting = $this->createWebinar($client, $currentRow);
                if (is_null($meeting)) {
                    $this->updateResultText($currentRow, self::RESULT_MSG_NO_CONFIGURATION_HOLDER_AVAILABLE);
                } else {
                    $this->renaultImportSessionTempRepository->postCreateWebinarInfos($currentRow, $meeting);
                    $this->updateResultText(
                        $currentRow, self::RESULT_MSG_MEETING_CREATED, ['meeting' => $meeting]
                    );
                }
            }
        } elseif ($currentRow->meetingIsCancelled()) {
            if (!is_null($meeting)) {
                // 4/ Si session existe, et changement de status annulée
                // Cancel meeting
                $this->cancelWebinar($meeting);
                $this->updateResultText(
                    $currentRow, self::RESULT_MSG_MEETING_CANCELLED, ['meeting' => $meeting]
                );
            } else {
                // 5/ Si session n\'existe, et changement de status annulée
                $this->updateResultText(
                    $currentRow, self::RESULT_MSG_MEETING_CANCELLED, ['meeting' => $meeting]
                );
            }
        } elseif ($currentRow->meetingIsBilled() || $currentRow->meetingIsDelivered()) {
            if (!is_null($meeting) && (!$meeting->isCancelled() || !$meeting->isFinished())) {
                // 6/ Si session exist et n\'est pas annulé et non terminé le status de la ligne est facturé ou livré
                // Cancel meeting
                $this->cancelWebinar($meeting);
                $this->updateResultText(
                    $currentRow, self::RESULT_MSG_MEETING_CANCELLED, []
                );
            } else {
                // 7/ Si session n\'existe, et changement de status annulée
                // SKIPPED
                $this->updateResultText(
                    $currentRow, self::RESULT_MSG_MEETING_CANCELLED, ['meeting' => $meeting]
                );
            }
        } else {
            // 8/ Si session existe, et changement de status INCONNU
            throw new RenaultMeetingImportInvalidStatusException(sprintf('Unkown error %s', 'Invalid action'));
        }

        return $currentRow;
    }

    private function processForMeeting(RenaultImportSessionTemp &$currentRow): RenaultImportSessionTemp
    {
        echo '-'.$currentRow->getId();

        /** @var AdobeConnectSCO $meeting */
        $meeting = null;
        if (empty($currentRow->getReferenceLiveSession())) {
            $params = [
                'description' => $currentRow->getReferenceRenault(),
            ];
            $meetingsFind = $this->providerSessionRepository->findBy($params, ['dateStart' => 'ASC']);
            $findReferenceLiveSession = null;
            $findReferenceLiveSessionSameDate = null;
            $findReferenceRenault = null;
            //dump(count($meetingsFind));
            //y'a t-il des sessions avec la référence Renault ?
            foreach ($meetingsFind as $m) {
                $findReferenceRenault = $m->getDescription();
                //Est-ce que les dates correspondent en fonction du jour cible ? Ceci évite une comparaison avec une valeur S1 : 2020-06-28 08:00:00 (j1)
                //dans le cadre initial suivant:
                //S1 : 2020-06-27 08:00:00 (j1) et S2 : 2020-06-28 08:00:00 (j2)
                if ($m->getDateStart() == $currentRow->getDateStart()) {
                    $findReferenceLiveSession = $m->getRef();
                    if ($m->getDescription2() == $currentRow->getNumDay()) {
                        $findReferenceLiveSessionSameDate = $m->getRef();  //On retient la référence LS trouvée
                        $meeting = $m;                          //On retient la session de la référence
                    }
                }

                //dump($findReferenceRenault .' ||| '. $findReferenceLiveSession .' ||| '. $findReferenceLiveSessionSameDate);
                //Si une session existe avec la même réf RLT sur la même date
                if (!empty($findReferenceRenault) && !empty($findReferenceLiveSession) && !empty($findReferenceLiveSessionSameDate)) {
                    //On ajoute la référence LS aux lignes correspondantes
                    $currentRow->setReferenceLiveSession($findReferenceLiveSessionSameDate);
                    //On applique la reference LS et l'url de session à toutes les lignes concernées
                    //dump('src/RenaultRobot/Service/RenaultMeetingImportService.php | Line 178');
                    $this->renaultImportSessionTempRepository->postCreateInfos($currentRow, $meeting);
                }

                //Si une session existe avec la même réf RLT sur une autre date
                if (!empty($findReferenceRenault) && !empty($findReferenceLiveSession) && empty($findReferenceLiveSessionSameDate)) {
                    //On annule la ou les sessions correspondants à réf RLT | Idéalement il faudrait également purger les champs referencesLS et UrlSession des lignes concernées du jour
                    foreach ($meetingsFind as $m) {
                        $this->cancelMeeting($m);
                        //dump('ANNULATION DE '.$findReferenceRenault.' >>> '.$m->getRef());
                    }
                    //Il faudra recréer une session nouvelle
                    $meeting = null;
                }
            }
        } else {
            $meeting = $this->providerSessionRepository->findOneBy(['ref' => $currentRow->getReferenceLiveSession()]);
        }

        //dump('src/RenaultRobot/Service/RenaultMeetingImportService.php');
        //dump('line 181 | '. $currentRow->getId().' - '.$currentRow->getReferenceLiveSession().' - '.$currentRow->getReferenceRenault().' - '.$currentRow->getStatusSession() );
        //dump( 'line 182 | '.'En attente de traitement ('.$currentRow->meetingIsWaitingScheduling().') || Annulation : ('.$currentRow->meetingIsCancelled().') || Facturé ('.$currentRow->meetingIsBilled().') || Dispensé ('.$currentRow->meetingIsDelivered().')' );

        if ($currentRow->meetingIsWaitingScheduling()) {
            if (!is_null($meeting)) {
                //Mise à jour de la session
                $diffDateStart = $meeting->getDateStart()->diff($currentRow->getDateStart());
                $diffDateEnd = $meeting->getDateEnd()->diff($currentRow->getDateEnd());
                $dateDayChanged = $diffDateStart->d > 0 || $diffDateEnd->d > 0;
                $dateTimeChanged = $diffDateStart->h > 0 || $diffDateStart->m > 0 || $diffDateEnd->h > 0 || $diffDateEnd->m > 0;

                if ($meeting->getStatus() == ProviderSession::STATUS_CANCELED) {
                    // 0/ Si la session est annulée
                    $this->uncancelMeeting($meeting);
                    $this->updateResultText(
                        $currentRow, self::RESULT_MSG_MEETING_UNCANCELLED, ['meeting' => $meeting]
                    );
                }

                if (!$dateDayChanged && !$dateTimeChanged) {
                    // 1/ Si référence existe avec date et heure identique
                    $this->renaultImportSessionTempRepository->postCreateRow($currentRow); //On met à jour la ligne
                    $this->updateResultText(
                        $currentRow, self::RESULT_MSG_MEETING_NOCHANGE
                    );
                }

                if (!$dateDayChanged && $dateTimeChanged) {
                    // 2/ Si référence session existe avec date identique, et changement d\'heure, modification de la session
                    $this->updateMeeting($meeting);
                    //$rltRepo->postUpdateTime( $currentRow, $meeting ); //On applique les nouvelles heures à toutes les lignes concernées
                    $this->updateResultText(
                        $currentRow, self::RESULT_MSG_MEETING_CHANGE_DURING, ['meeting' => $meeting]
                    );
                }

                if ($dateDayChanged) {
                    // 3/ Si référence session existe, et changement date, suppression de la session et création d\'une nouvelle session avec la même référence
                    /** @var Client $client */
                    //$client = $this->clientRepository->find( RenaultImportSessionTemp::RLT_CLIENT_ID );
                    $client = $this->clientRepository->findOneBy(['name' => 'Renault']);
                    $this->cancelMeeting($meeting);
                    $meeting = $this->createMeeting($client, $currentRow);
                    $this->updateResultText(
                        $currentRow, self::RESULT_MSG_MEETING_RECREATED, ['meeting' => $meeting]
                    );
                }
            } else {
                // *** CREATE SESSION ***
                /** @var Client $client */
                //$client = $this->clientRepository->find( RenaultImportSessionTemp::RLT_CLIENT_ID );
                $client = $this->clientRepository->findOneBy(['name' => 'Renault']);
                /** @var AdobeConnectSCO $meeting */
                $meeting = $this->createMeeting($client, $currentRow);
                $this->renaultImportSessionTempRepository->postCreateInfos($currentRow, $meeting);
                $this->updateResultText(
                    $currentRow, self::RESULT_MSG_MEETING_CREATED, ['meeting' => $meeting]
                );
            }
        } elseif ($currentRow->meetingIsCancelled()) {
            if (!is_null($meeting)) {
                // 4/ Si session existe, et changement de status annulée
                // Cancel meeting
                $this->cancelMeeting($meeting);
                $this->updateResultText(
                    $currentRow, self::RESULT_MSG_MEETING_CANCELLED, ['meeting' => $meeting]
                );
            } else {
                // 5/ Si session n\'existe, et changement de status annulée
                $this->updateResultText(
                    $currentRow, self::RESULT_MSG_MEETING_CANCELLED, ['meeting' => $meeting]
                );
            }
        } elseif ($currentRow->meetingIsBilled() || $currentRow->meetingIsDelivered()) {
            if (!is_null($meeting) && (!$meeting->isCancelled() || !$meeting->isFinished())) {
                // 6/ Si session exist et n\'est pas annulé et non terminé le status de la ligne est facturé ou livré
                // Cancel meeting
                $this->cancelMeeting($meeting);
                $this->updateResultText(
                    $currentRow, self::RESULT_MSG_MEETING_CANCELLED, []
                );
            } else {
                // 7/ Si session n\'existe, et changement de status annulée
                // SKIPPED
                $this->updateResultText(
                    $currentRow, self::RESULT_MSG_MEETING_CANCELLED, ['meeting' => $meeting]
                );
            }
        } else {
            // 8/ Si session existe, et changement de status INCONNU
            throw new RenaultMeetingImportInvalidStatusException(sprintf('Unkown error %s', 'Invalid action'));
        }

        return $currentRow;
    }

    private function updateResultText(RenaultImportSessionTemp &$currentRow, $key, array $params = []): RenaultImportSessionTemp
    {
        $resultMessage = '';
        $resultSuccess = true;

        switch ($key) {
            // -------------------------------
            // --- MEETING CREATED
            case self::RESULT_MSG_MEETING_CREATED:
                $resultSuccess = true;

                $resultMessage = $this->translator->trans(
                    '(%row%) Session %meeting% (reference Renault "%ref_meeting%") created !',
                    [
                        '%row%' => $currentRow->getNumRow(),
                        '%meeting%' => $currentRow->getName(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );
                break;
            // -------------------------------

            // -------------------------------
            // --- MEETING NO CHANGE
            case self::RESULT_MSG_MEETING_NOCHANGE:
                //dump('src/RenaultRobot/Service/RenaultMeetingImportService.php | Line 312');
                $resultSuccess = true;

                $resultMessage = $this->translator->trans(
                    '(%row%) Session %meeting% (reference Renault "%ref_meeting%") no change for the row !',
                    [
                        '%row%' => $currentRow->getNumRow(),
                        '%meeting%' => $currentRow->getName(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );

                break;
            // -------------------------------

            // -------------------------------
            // --- MEETING CHANGE DURING
            case self::RESULT_MSG_MEETING_CHANGE_DURING:
                //dump('src/RenaultRobot/Service/RenaultMeetingImportService.php | Line 329');
                $resultSuccess = true;

                $resultMessage = $this->translator->trans(
                    '(%row%) Session %meeting% (reference Renault "%ref_meeting%") change during from the row !',
                    [
                        '%row%' => $currentRow->getNumRow(),
                        '%meeting%' => $currentRow->getName(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );
                break;
            // -------------------------------

            // -------------------------------
            // --- MEETING CANCELLED
            case self::RESULT_MSG_MEETING_CANCELLED:
                //dump('src/RenaultRobot/Service/RenaultMeetingImportService.php | Line 345');
                $resultSuccess = true;

                $resultMessage = $this->translator->trans(
                    '(%row%) Session %meeting% (reference Renault "%ref_meeting%") canceled !',
                    [
                        '%row%' => $currentRow->getNumRow(),
                        '%meeting%' => $currentRow->getName(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );
                break;
            // -------------------------------

            // -------------------------------
            // --- MEETING UNCANCELLED
            case self::RESULT_MSG_MEETING_UNCANCELLED:
                //dump('src/RenaultRobot/Service/RenaultMeetingImportService.php | Line 361');
                $resultSuccess = true;

                $resultMessage = $this->translator->trans(
                    '(%row%) Session %meeting% (reference Renault "%ref_meeting%") uncanceled !',
                    [
                        '%row%' => $currentRow->getNumRow(),
                        '%meeting%' => $currentRow->getName(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );
                break;
            // -------------------------------

            // -------------------------------
            // --- RLTMeetingImportInvalidStatusException exception
            case self::RESULT_MSG_NO_CONFIGURATION_HOLDER_AVAILABLE:
                $resultSuccess = false;
                $message = $this->translator->trans(
                    '(%row%) %meeting% session (Renault reference "%ref_meeting%") | No connector is available (%reference%)!',
                    [
                        '%row%' => $currentRow->getNumRow(),
                        '%reference%' => $currentRow->getReferenceLiveSession(),
                        '%meeting%' => $currentRow->getName(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );
                $resultMessage = $message;

                $this->logger->error(
                    sprintf(
                        '[RLTMeetingImportService][Error][#%s] Meeting: %s | Row: %s | Message: %s',
                        $currentRow->getId(),
                        $currentRow->getReferenceRenault(), $currentRow->getNumRow(),
                        $message
                    )
                );
                break;
            case self::RESULT_MSG_MEETING_INVALID_STATUS:
                //dump('src/RenaultRobot/Service/RenaultMeetingImportService.php | Line 378');
                $e = $params['e'];
                $resultSuccess = false;
                $resultMessage = $this->translator->trans(
                    '(%row%) Session %meeting% (reference Renault "%ref_meeting%") | Unknown AC mistake (%reference%) !',
                    [
                        '%row%' => $currentRow->getNumRow(),
                        '%reference%' => $currentRow->getReferenceLiveSession(),
                        '%meeting%' => $currentRow->getName(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );

                $this->logger->error(
                    sprintf(
                        '[RLTMeetingImportService][Error][#%s] Meeting: %s | Row: %s | Message: %s',
                        $currentRow->getId(),
                        $currentRow->getReferenceRenault(), $currentRow->getNumRow(),
                        $e->getMessage()
                    )
                );

                break;
            // -------------------------------

            // -------------------------------
            // --- AdobeConnectException exception
            case self::RESULT_MSG_AC_UNKNOWN_ERROR:
                //dump('src/RenaultRobot/Service/RenaultMeetingImportService.php | Line 405');
                $e = $params['e'];
                $resultSuccess = false;
                $resultMessage = $this->translator->trans(
                    '(%row%) Session %meeting% (reference Renault "%ref_meeting%") | Unknown AC mistake (%reference%) !',
                    [
                        '%row%' => $currentRow->getNumRow(),
                        '%reference%' => $currentRow->getReferenceLiveSession(),
                        '%meeting%' => $currentRow->getName(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );

                $this->logger->error(
                    sprintf(
                        '[RLTMeetingImportService][Error][#%s] Meeting: %s | Row: %s | Message: %s',
                        $currentRow->getId(),
                        $currentRow->getReferenceRenault(), $currentRow->getNumRow(),
                        $e->getMessage()
                    )
                );
                break;
            // -------------------------------

            // -------------------------------
            // --- Exception exception
            case self::RESULT_MSG_UNKNOWN_ERROR:
                //dump('src/RenaultRobot/Service/RenaultMeetingImportService.php | Line 431');
                $e = $params['e'];
                $resultSuccess = false;
                $resultMessage = $this->translator->trans(
                    '(%row%) Session %meeting% (reference Renault "%ref_meeting%") | Unknown mistake (%reference%) !',
                    [
                        '%row%' => $currentRow->getNumRow(),
                        '%reference%' => $currentRow->getReferenceLiveSession(),
                        '%meeting%' => $currentRow->getName(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );

                $this->logger->error(
                    sprintf(
                        '[RLTMeetingImportService][Error][#%s] Meeting: %s | Row: %s | Message: %s',
                        $currentRow->getId(),
                        $currentRow->getReferenceRenault(), $currentRow->getNumRow(),
                        $e->getMessage()
                    )
                );
                break;
            // -------------------------------
        }

        $currentRow->setImportedAt(new \DateTimeImmutable('now'));
        $currentRow->setResultType($resultSuccess ? RenaultImportSessionTemp::SUCCESS : RenaultImportSessionTemp::ERROR);
        $currentRow->setResultText($resultMessage);
        $currentRow->setResultSession($resultMessage);

        return $currentRow;
    }

    protected function fulfillData(array &$data, Client $client, AbstractProviderConfigurationHolder $configurationHolder): void
    {
        $data['providerSession'] = !empty($data['reference']) ? $this->providerSessionRepository->findOneBy(['ref' => $data['reference']]) : null;
        $data['convocationObj'] = $this->convocationRepository->findConvocation(client: $client, convocationName: $data['convocation'], configurationHolderType: $configurationHolder::getProvider());
        $data['category'] = empty($data['category']) ? CategorySessionType::CATEGORY_SESSION_FORMATION : intval($data['category']);
        $data['action'] = empty($data['action']) ? ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE : (int) $data['action'];
        $data['reference'] = empty($data['reference']) ? ProviderSessionService::generateSessionReference($client->getName()) : $data['reference'];
        $data['nature'] = !empty($data['nature']) ? intval($data['nature']) : null;
        $data['subject'] = !empty($data['subjectImportCode']) ? $this->subjectRepository->findOneBy(['importCode' => $data['subjectImportCode']]) : null;
        $data['subscriptionMax'] = empty($data['subscriptionMax']) ? 0 : intval($data['subscriptionMax']);
        $data['alertLimit'] = intval($data['alertLimit']);
        $data['immediateSend'] = intval($data['immediateSend']);

        if (!empty($data['dateStart']) && !empty($data['duration'])) {
            $data['dateEnd'] = clone $data['dateStart'];
            $data['dateEnd']->modify('+ '.$data['duration']->format('H').' hours')->modify('+ '.$data['duration']->format('i').' min');
        }

        $data['tags'] = empty($data['tags']) ? null : explode(';', $data['tags']);
        $data['objectives'] = empty($data['objectives']) ? null : explode(';', $data['objectives']);
        $data['automaticSendingTrainingCertificates'] = $client->getEmailOptions()->isSendAnAttestationAutomatically();

        if ($data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE && empty($data['providerSession']) && !empty($data['dateStart'])) {
            $dates = $this->providerSessionService->getConvocationAndRemindersDate($client->getEmailScheduling(), $data['dateStart']);
            if ($data['immediateSend'] == 1) {
                $data['notificationDate'] = new \DateTimeImmutable();
                $data['lastConvocationSent'] = new \DateTimeImmutable();
            } else {
                $data['notificationDate'] = $dates['convocation'];
            }

            $data['dateOfSendingReminders'] = $dates['reminders'];
        }

        $data['logAction'] = match ($data['action']) {
            ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE => empty($data['providerSession']) ? ActivityLog::ACTION_SESSION_CREATION : ActivityLog::ACTION_SESSION_UPDATE,
            ActionsFromImportSessionTask::ACTION_SESSION_DELETE => ActivityLog::ACTION_SESSION_DELETE,
            ActionsFromImportSessionTask::ACTION_SESSION_CANCEL => ActivityLog::ACTION_SESSION_CANCEL,
            default => ActivityLog::ACTION_IMPORT_SESSIONS
        };

        if (!empty($data['evaluation'])) {
            $evaluation = $this->evaluationRepository->findOneBy([
                'title' => $data['evaluation'],
                'client' => $client,
            ]);

            $data['evaluationsObj'] = ($evaluation !== null ? new ArrayCollection([$evaluation]) : -1);
        }

        $services = $client->getServices();
        $data['assistanceType'] = $services->getAssistanceType();
        $data['supportType'] = $services->getSupportType() === ClientServices::ENUM_SUPPORT_FULL ? ProviderSession::ENUM_SUPPORT_TOTAL : ProviderSession::ENUM_NO;
        $data['standardHoursActive'] = $services->getStandardHours()->isActive();
        $data['outsideStandardHoursActive'] = $services->getOutsideStandardHours()->isActive();
    }

    private function validData(?Client $client, ?AbstractProviderConfigurationHolder $configurationHolder, array $data): void
    {
        if (empty($configurationHolder)) {
            throw new ProviderSessionRequestHandlerException('The connector cannot be found', [], false, $this->translator->trans('The connector cannot be found'));
        }

        if ($client->getId() !== $configurationHolder->getClient()->getId()) {
            throw new ProviderSessionRequestHandlerException('Connector does not belong to the account', [], false, $this->translator->trans('Connector does not belong to the account'));
        }

        if (empty($data['title'])) {
            throw new ProviderSessionRequestHandlerException('The session\'s name is empty!', [], true, $this->translator->trans('The session\'s name is empty!'));
        }

        if (empty($data['reference'])) {
            throw new ProviderSessionRequestHandlerException('The session reference "%title%" is empty!', ['%title%' => $data['title']], true, $this->translator->trans('The session reference "%title%" is empty!', ['%title%' => $data['title']]));
        }

        if (array_key_exists('id', $data) && empty($data['session'])) {
            throw new ProviderSessionRequestHandlerException('the session %title% does not exist', ['%title%' => $data['title']], true, $this->translator->trans('the session %title% does not exist', ['%title%' => $data['title']]));
        }

        if (array_key_exists('id', $data) && $data['session']->getRef() !== $data['reference']) {
            throw new ProviderSessionRequestHandlerException('The session reference %ref% cannot be updated', ['%title%' => $data['title']], true, $this->translator->trans('The session reference %ref% cannot be updated', ['%title%' => $data['title']]));
        }

        if (array_key_exists('id', $data) && $client->getId() !== $data['session']->getClient()->getId()) {
            throw new ProviderSessionRequestHandlerException('Session %title% to update does not belong to the same account', ['%title%' => $data['title']], false, $this->translator->trans('Session %title% to update does not belong to the same account', ['%title%' => $data['title']]));
        }

        if (!array_key_exists('id', $data) && !empty($this->sessionRepository->findOneBy(['ref' => $data['reference']]))) {
            throw new ProviderSessionRequestHandlerException('A session with the reference %ref% already exists', ['%ref%' => $data['reference']], true, $this->translator->trans('A session with the reference %ref% already exists', ['%ref%' => $data['reference']]));
        }

        if (!is_null($data['category']) && is_null(CategorySessionType::from($data['category']))) {
            throw new ProviderSessionRequestHandlerException('The session category %title% is invalid !', ['%title%' => $data['title']], true, $this->translator->trans('The session category %title% is invalid !', ['%title%' => $data['title']]));
        }

        if (empty($data['lang']) || !in_array($data['lang'], ['fr', 'en'])) {
            throw new ProviderSessionRequestHandlerException('The session %title% language is invalid!', ['%title%' => $data['title']], true, $this->translator->trans('The session %title% language is invalid!', ['%title%' => $data['title']]));
        }

        if (empty($data['convocationObj'])) {
            throw new ProviderSessionRequestHandlerException('The session %title% language is invalid!', ['%title%' => $data['title']], true, $this->translator->trans('The session %title% convocation does not exist!', ['%title%' => $data['title']]));
        }

        if (!$configurationHolder->getClient()->categoryAvailable(CategorySessionType::from($data['category']))) {
            throw new InvalidImportDataException('The "%title%" session category is not available in your account!', ['%title%' => $data['title']], $this->translator->trans('The "%title%" session category is not available in your account!', ['%title%' => $data['title']]));
        }
    }

    protected function createWebinar(Client $client, RenaultImportSessionTemp &$currentRow): ?ProviderSession
    {
        $this->activityLogger->initActivityLogContext($client, null, ActivityLog::ORIGIN_CRON);
        try {
            $convocationObj = $client->getConvocations()->first();  //Sera écrasé par la boucle foreach
            foreach ($client->getConvocations() as $convocation) {
                if ($convocation->getConnectorType() == Convocation::TYPE_ADOBE_CONNECT_WEBINAR) {
                    $convocationObj = $convocation;
                    break;
                }
            }
            $defaultModelId = null;
            $allConfigurationHolderWebinar = $this->getConfigurationHoldersWebinar($client);
            $configurationHolder = null;
            foreach ($allConfigurationHolderWebinar as $confHolder) {
                $sessionInSlot = $this->providerSessionRepository->findConfigurationHolderWithSessionForPeriod($currentRow->getDateStart(), $currentRow->getDateEnd(), $confHolder->getId());
                if (empty($sessionInSlot)) {
                    /** @var AdobeConnectWebinarConfigurationHolder $configurationHolder */
                    $configurationHolder = $confHolder;
                    break;
                }
            }
            if ($configurationHolder == null) {
                return null;
            }

            $defaultModelId = $this->getRoomModelWebinarId($configurationHolder, substr($currentRow->getName(), 0, 11));

            $data = [
                'dateStart' => \DateTime::createFromImmutable($currentRow->getDateStart()),
                'dateEnd' => \DateTime::createFromImmutable($currentRow->getDateEnd()),
                //'name' => $currentRow->getName(),
                'url' => '',
                'model' => '',
                'reference' => ProviderSessionService::generateSessionReference($client->getName()),
                'lang' => 'fr',
                'businessNumber' => '',
                'information' => '',
                'tags' => [],
                'personalizedContent' => '',
                'category' => (CategorySessionType::CATEGORY_SESSION_WEBINAR)->value,
                'automaticSendingTrainingCertificates' => '',
                'sessionTest' => '',
                'convocationObj' => $convocationObj, //'Convocation Renault VOIP',
                'title' => $currentRow->getName(),
                'defaultModelId' => $defaultModelId,
                'description' => $currentRow->getReferenceRenault(),
                'description2' => $currentRow->getNumDay(),
                'nature' => null,
                'objectives' => null,
                'accessType' => AdobeConnectConnector::GRANT_ACCESS_USER_ONLY,
                'replayVideoPlatform' => null,
                'replayUrlKey' => null,
                'publishedReplay' => false,
                'accessParticipantReplay' => false,
                'replayLayoutPlayButton' => false,
            ];

            //$this->validData($client, $configurationHolder, $data);
            $session = $this->providerSessionService->createSessionFromArray($configurationHolder, null, $data);
            if ($session->getCategory() == CategorySessionType::CATEGORY_SESSION_WEBINAR) {
                $communicationWebinarObj = new WebinarConvocation($convocationObj, $session);
                $communicationWebinarObj->setConvocationType(WebinarConvocation::TYPE_CONFIRMATION);
                $communicationWebinarObj->setConvocationDate((new \DateTimeImmutable())->setTime(date('H', $session->getDateStart()->getTimestamp()), date('i', $session->getDateStart()->getTimestamp())));
                $this->entityManager->persist($communicationWebinarObj);

                $communicationReminderWebinarObj = new WebinarConvocation($convocationObj, $session);
                $communicationReminderWebinarObj->setConvocationType(WebinarConvocation::TYPE_REMINDER);
                $communicationReminderWebinarObj->setConvocationDate((\DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $session->getDateStart()->format('Y-m-d H:i:s')))->modify('-1 day'));
                $this->entityManager->persist($communicationReminderWebinarObj);
            }
            $session = $this->providerSessionService->applyRemindersAndConvocationPreference($client->getEmailScheduling(), $session);

            $this->entityManager->flush();
            //$this->activityLogger->flushActivityLogs();
        } catch (\Exception $exception) {
            $session = null;
            $this->logger->error('An unknown error occurred during session add : ', [
                'ID row' => $currentRow->getId(),
                'message' => $exception->getMessage(),
            ]);
        }

        return $session;
    }

    protected function createMeeting(Client $client, RenaultImportSessionTemp &$currentRow): ?ProviderSession
    {
        $this->activityLogger->initActivityLogContext($client, null, ActivityLog::ORIGIN_CRON);
        try {
            $convocationObj = $client->getConvocations()->first();  //Sera écrasé par la boucle foreach
            foreach ($client->getConvocations() as $convocation) {
                if ($convocation->getConnectorType() == Convocation::TYPE_ADOBE_CONNECT) {
                    $convocationObj = $convocation;
                    break;
                }
            }
            $defaultModelId = null;
            /** @var AbstractProviderConfigurationHolder|AdobeConnectConfigurationHolder $configurationHolder */
            $configurationHolder = $client->getConfigurationHolders()[0];
            if ($configurationHolder instanceof AdobeConnectConfigurationHolder) {
                $defaultModelId = $this->getRoomModelId($configurationHolder, substr($currentRow->getName(), 0, 11));
            }

            $data = [
                    'dateStart' => \DateTime::createFromImmutable($currentRow->getDateStart()),
                    'dateEnd' => \DateTime::createFromImmutable($currentRow->getDateEnd()),
                    //'name' => $currentRow->getName(),
                    'url' => '',
                    'model' => '',
                    'reference' => ProviderSessionService::generateSessionReference($client->getName()),
                    'lang' => 'fr',
                    'businessNumber' => '',
                    'information' => '',
                    'tags' => [],
                    'personalizedContent' => '',
                    'category' => (CategorySessionType::CATEGORY_SESSION_FORMATION)->value,
                    'automaticSendingTrainingCertificates' => '',
                    'sessionTest' => '',
                    'convocationObj' => $convocationObj, //'Convocation Renault VOIP',
                    'title' => $currentRow->getName(),
                    'defaultModelId' => $defaultModelId,
                    'description' => $currentRow->getReferenceRenault(),
                    'description2' => $currentRow->getNumDay(),
                    'nature' => null,
                    'objectives' => null,
                    'accessType' => AdobeConnectConnector::GRANT_ACCESS_USER_ONLY,
                    'replayVideoPlatform' => null,
                    'replayUrlKey' => null,
                    'publishedReplay' => false,
                    'accessParticipantReplay' => false,
                    'replayLayoutPlayButton' => false,
                 ];

            //$this->validData($client, $configurationHolder, $data);
            $session = $this->providerSessionService->createSessionFromArray($configurationHolder, null, $data);
            if ($session->getCategory() == CategorySessionType::CATEGORY_SESSION_WEBINAR) {
                $communicationWebinarObj = new WebinarConvocation($convocationObj, $session);
                $communicationWebinarObj->setConvocationType(WebinarConvocation::TYPE_CONFIRMATION);
                $communicationWebinarObj->setConvocationDate((new \DateTimeImmutable())->setTime(date('H', $session->getDateStart()->getTimestamp()), date('i', $session->getDateStart()->getTimestamp())));
                $this->entityManager->persist($communicationWebinarObj);

                $communicationReminderWebinarObj = new WebinarConvocation($convocationObj, $session);
                $communicationReminderWebinarObj->setConvocationType(WebinarConvocation::TYPE_REMINDER);
                $communicationReminderWebinarObj->setConvocationDate((\DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $session->getDateStart()->format('Y-m-d H:i:s')))->modify('-1 day'));
                $this->entityManager->persist($communicationReminderWebinarObj);
            }
            $session = $this->providerSessionService->applyRemindersAndConvocationPreference($client->getEmailScheduling(), $session);

            $this->entityManager->flush();
            //$this->activityLogger->flushActivityLogs();
        } catch (\Exception $exception) {
            $session = null;
            $this->logger->error('An unknown error occurred during session add : ', [
                'ID row' => $currentRow->getId(),
                'message' => $exception->getMessage(),
            ]);
        }

        return $session;
    }

    protected function cancelWebinar(ProviderSession &$meeting): void
    {
        $this->activityLogger->initActivityLogContext($meeting->getClient(), null, ActivityLog::ORIGIN_CRON);
        try {
            $this->providerSessionService->cancelSession($meeting);

            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();
        } catch (\Exception $exception) {
            $this->logger->error('An unknown error occurred during session cancellation : ', [
                'ID session' => $meeting->getId(),
                'message' => $exception->getMessage(),
            ]);
        }
    }

    protected function cancelMeeting(ProviderSession &$meeting): void
    {
        $this->activityLogger->initActivityLogContext($meeting->getClient(), null, ActivityLog::ORIGIN_CRON);
        try {
            $this->providerSessionService->cancelSession($meeting);

            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();
        } catch (\Exception $exception) {
            $this->logger->error('An unknown error occurred during session cancellation : ', [
                'ID session' => $meeting->getId(),
                'message' => $exception->getMessage(),
            ]);
        }
    }

    protected function uncancelWebinar(ProviderSession &$meeting): void
    {
        $this->activityLogger->initActivityLogContext($meeting->getClient(), null, ActivityLog::ORIGIN_CRON);
        try {
            $this->providerSessionService->uncancelSession($meeting);

            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();
        } catch (\Exception $exception) {
            $this->logger->error('An unknown error occurred during session uncancellation : ', [
                'ID session' => $meeting->getId(),
                'message' => $exception->getMessage(),
            ]);
        }
    }

    protected function uncancelMeeting(ProviderSession &$meeting): void
    {
        $this->activityLogger->initActivityLogContext($meeting->getClient(), null, ActivityLog::ORIGIN_CRON);
        try {
            $this->providerSessionService->uncancelSession($meeting);

            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();
        } catch (\Exception $exception) {
            $this->logger->error('An unknown error occurred during session uncancellation : ', [
                'ID session' => $meeting->getId(),
                'message' => $exception->getMessage(),
            ]);
        }
    }

    protected function updateWebinar(ProviderSession &$meeting): void
    {
        $this->activityLogger->initActivityLogContext($meeting->getClient(), null, ActivityLog::ORIGIN_CRON);
        try {
            $this->providerSessionService->updateSession($meeting);

            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();
        } catch (\Exception $exception) {
            $this->logger->error('An unknown error occurred during session update : ', [
                'ID session' => $meeting->getId(),
                'message' => $exception->getMessage(),
            ]);
        }
    }

    protected function updateMeeting(ProviderSession &$meeting): void
    {
        $this->activityLogger->initActivityLogContext($meeting->getClient(), null, ActivityLog::ORIGIN_CRON);
        try {
            $this->providerSessionService->updateSession($meeting);

            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();
        } catch (\Exception $exception) {
            $this->logger->error('An unknown error occurred during session update : ', [
                'ID session' => $meeting->getId(),
                'message' => $exception->getMessage(),
            ]);
        }
    }

    public function getRoomModelWebinarId(AdobeConnectWebinarConfigurationHolder $configurationHolder, string $roomName): ?int
    {
        $response = $this->adobeConnectWebinarService->getSharedMeetingsTemplate($configurationHolder);
        if ($response->getData() !== null) {
            foreach ($response->getData() as $room) {
                if ($room['name'] == $roomName) {
                    return $room['uniq_identifier'];
                }
            }
        }

        return $configurationHolder->getDefaultRoom();
    }

    public function getRoomModelId(AdobeConnectConfigurationHolder $configurationHolder, string $roomName): ?int
    {
        $response = $this->adobeConnectService->getSharedMeetingsTemplate($configurationHolder);
        if ($response->getData() !== null) {
            foreach ($response->getData() as $room) {
                if ($room['name'] == $roomName) {
                    return $room['uniq_identifier'];
                }
            }
        }

        return $configurationHolder->getDefaultRoom();
    }

    public function getConfigurationHoldersWebinar(Client $client): array
    {
        $configurationHolders = [];
        foreach ($client->getConfigurationHolders() as $configurationHolder) {
            if ($configurationHolder instanceof AdobeConnectWebinarConfigurationHolder) {
                $configurationHolders[] = $configurationHolder;
            }
        }

        return $configurationHolders;
    }
}
