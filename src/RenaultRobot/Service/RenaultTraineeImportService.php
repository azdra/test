<?php

namespace App\RenaultRobot\Service;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Exception\MiddlewareException;
use App\RenaultRobot\Entity\RenaultImportSessionTemp;
use App\RenaultRobot\Exception\RenaultTraineeImportInvalidStatusException;
use App\RenaultRobot\Repository\RenaultImportSessionTempErrorsRepository;
use App\RenaultRobot\Repository\RenaultImportSessionTempRepository;
use App\Repository\ClientRepository;
use App\Repository\ConvocationRepository;
use App\Repository\EvaluationRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Security\UserSecurityService;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\ActivityLogger;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\ProviderSessionService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RenaultTraineeImportService
{
    public const MAX_CONSECUTIVE_ROWS = 200;

    public const STATE_PROCESS_ANIMATOR_REGISTERED = 1;
    public const STATE_PROCESS_ANIMATOR_ALREADY_REGISTERED = 2;
    public const RESULT_MSG_ANIMATOR_REGISTERED = 'animator_registered';
    public const RESULT_MSG_ANIMATOR_ALREADY_REGISTERED = 'animator_already_registered';

    public const STATE_PROCESS_TRAINEE_REGISTERED = 1;
    public const STATE_PROCESS_TRAINEE_ALREADY_REGISTERED = 2;
    public const STATE_PROCESS_TRAINEE_CANCELLED = 3;
    public const STATE_PROCESS_TRAINEE_NOACTION = 4;
    public const RESULT_MSG_TRAINEE_REGISTERED = 'trainee_registered';
    public const RESULT_MSG_TRAINEE_ALREADY_REGISTERED = 'trainee_already_registered';
    public const RESULT_MSG_TRAINEE_CANCELLED = 'trainee_cancelled';
    public const RESULT_MSG_TRAINEE_NOACTION = 'trainee_noaction';

    public const STATE_PROCESS_TRAINEE_ERROR = 'ERROR';
    public const RESULT_MSG_TRAINEE_INVALID_STATUS = 'trainee_invalid_status';
    public const RESULT_MSG_AC_UNKNOWN_ERROR = 'ac-unknown-error';
    public const RESULT_MSG_UNKNOWN_ERROR = 'unknown-error';

    public const GROUP_CREATE = 'renault:trainee:create';

    //** @var RenaultImportSessionTemp[] */
    //private $_importToApplyAtEnd = [];

    public function __construct(
        protected string $renaultSavefilesDirectory,
        protected EntityManagerInterface $entityManager,
        protected ConvocationRepository $convocationRepository,
        protected EvaluationRepository $evaluationRepository,
        protected RenaultImportSessionTempRepository $renaultImportSessionTempRepository,
        protected RenaultImportSessionTempErrorsRepository $renaultImportSessionTempErrorsRepository,
        protected ClientRepository $clientRepository,
        protected ProviderSessionRepository $providerSessionRepository,
        protected ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        protected SubjectRepository $subjectRepository,
        protected UserRepository $userRepository,
        protected ActivityLogger $activityLogger,
        protected ProviderSessionService $providerSessionService,
        protected UserSecurityService $userSecurityService,
        protected ParticipantSessionSubscriber $participantSessionSubscriber,
        protected LoggerInterface $logger,
        protected TranslatorInterface $translator,
        protected RouterInterface $router,
        protected string $endPointDomain,
    ) {
    }

    public function importTraineesFromTemp(): array
    {
        /** @var RenaultImportSessionTemp[] $allRLTRowToImport */
        $allRLTRowToImport = $this->renaultImportSessionTempRepository->findAllByResultUser(
            RenaultImportSessionTemp::WAITING, self::MAX_CONSECUTIVE_ROWS
        );

        //$allRTLRowWaiting = $allRLTRowToImport;
        //$i                = 0;
        //dump(count($allRLTRowToImport));
        //do {
        //$this->_importToApplyAtEnd = [];

        //$tmp = [];
        //foreach ( $allRTLRowWaiting as $item )
        //    $tmp[ $item->getId() ] = $item;
        //$allRTLRowWaiting = $tmp;

        //dump('src/RenaultRobot/Service/RenaultTraineeImportService.php');
        //dump('line 96 | Start >>>>>>>>>>>>>>>>>>>> A traiter = '.count( $allRTLRowWaiting ).' | Réalisé = '.count($this->_importToApplyAtEnd) );

        //foreach ( $allRTLRowWaiting as $rowTemp ) {
        foreach ($allRLTRowToImport as $rowTemp) {
            try {
                if (substr($rowTemp->getName(), 0, 3) == 'WEB') {
                    $this->processForTraineeWebinar($rowTemp);
                } else {
                    $this->processForTrainee($rowTemp);
                }
            } catch (RenaultTraineeImportInvalidStatusException $e) {
                $this->updateResultTrainee(
                        $rowTemp,
                        self::RESULT_MSG_TRAINEE_INVALID_STATUS,
                        ['e' => $e]
                    );
            } catch (MiddlewareException $e) {
                $this->updateResultTrainee(
                        $rowTemp,
                        self::RESULT_MSG_AC_UNKNOWN_ERROR,
                        ['e' => $e]
                    );
            } catch (\Exception $e) {
                $this->updateResultTrainee(
                        $rowTemp,
                        self::RESULT_MSG_UNKNOWN_ERROR,
                        ['e' => $e]
                    );
            } finally {
                //$i++;
                $this->entityManager->persist($rowTemp);
                $this->entityManager->flush($rowTemp);
            }
        }
        //$allRTLRowWaiting = $this->_importToApplyAtEnd;
        //} while ( !empty( $this->_importToApplyAtEnd ) );

        return $allRLTRowToImport;
    }

    private function processForTraineeWebinar(RenaultImportSessionTemp &$currentRow): RenaultImportSessionTemp
    {
        echo '-'.$currentRow->getId();

        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Renault']);

        $configurationHolder = $client->getConfigurationHolders()[0]; //Sera écrasé par la boucle foreach
        foreach ($client->getConfigurationHolders() as $confHolder) {
            if ($confHolder instanceof AdobeConnectWebinarConfigurationHolder) {
                //TODO Vérifier le connecteur webinar avec une licence disponible
                /** @var AbstractProviderConfigurationHolder|AdobeConnectWebinarConfigurationHolder $configurationHolder */
                $configurationHolder = $confHolder;
                break;
            }
        }

        /** @var AdobeConnectWebinarSCO $meeting */
        $meeting = null;
        if (empty($currentRow->getReferenceLiveSession()) || !$currentRow->meetingIsWaitingScheduling()) {
            //echo '1 / Ligne ref LS est vide ou line n\'est pas meetingIsWaitingScheduling';
            //Aucun traitement car la ligne n'est associée à aucune session ou la session n'est pas avec un statut Open
            $params = [];
            $this->updateResultTrainee(
                $currentRow, self::RESULT_MSG_TRAINEE_NOACTION, $params
            );
            throw new \Exception('Aucun traitement à effectuer sur le traitement de l\'utilisateur de la ligne "'.$currentRow->getId().'"');
        } else {
            //echo '2 / Ligne ref LS n\'est pas vide ou line est meetingIsWaitingScheduling';
            $meeting = $this->providerSessionRepository->findOneBy(['ref' => $currentRow->getReferenceLiveSession()]);
        }

        if ($currentRow->getRole() == RenaultImportSessionTemp::ROLE_ANIMATOR) {
            $stateTrainee = $currentRow->getResultAnimator();
        } else {
            $stateTrainee = $currentRow->getResultTrainee();
        }
        /*dump( $currentRow->getId().' - '.$currentRow->getReferenceLiveSession().' - '.$currentRow->getReferenceRenault().' - '.$currentRow->getStatusTrainee() );
        dump( 'En attente de traitement ('.$currentRow->meetingIsWaitingScheduling().') || Annulation : ('.$currentRow->meetingIsCancelled().') || Facturé ('.$currentRow->meetingIsBilled().') || Dispensé ('.$currentRow->meetingIsDelivered().')' );
        dump( 'Role ('.$currentRow->getRole().') || Traitement effectué ('.$stateTrainee.') || Enregistré ('.$currentRow->traineeIsRegistered().') || Annulation : ('.$currentRow->traineeIsCanceled().') || Annulation facturée ('.$currentRow->traineeIsCanceledFees().') || Dispensé ('.$currentRow->traineeIsDelivered().')' );*/

        $ppsrAnimator = null;
        $ppsrTrainee = null;

        if ($currentRow->getRole() == RenaultImportSessionTemp::ROLE_ANIMATOR) {
            //echo ' ANIMATOR 1/ ---';
            $userClient = $this->userRepository->findOneBy(['email' => $currentRow->getAnimatorEmail(), 'client' => $client]);
            $findAnimator = false;
            $animator = null;
            foreach ($meeting->getParticipantRoles() as $participant) {
                if ($participant->getRole() == ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                    //echo 'ANIMATOR 2/ L\'animateur associé à cette session';
                    if ($participant->getParticipant()->getEmail() == $currentRow->getAnimatorEmail()) {
                        //echo 'ANIMATOR 3/ L\'email de l\'animateur associé à cette session est identique à l\'email du trainee de la ligne';
                        $ppsrAnimator = $participant;
                        $findAnimator = true;
                        //On récupère l'utilisateur s'il existe déjà en base (pour le compte)
                        /** @var User $animator */
                        $animator = $this->userRepository->findOneBy(['email' => $participant->getParticipant()->getEmail(), 'client' => $client]);
                        break;
                    }
                }
            }
            if (!$findAnimator && empty($stateTrainee)) {
                //echo 'ANIMATOR non trouvé et le status du participant est vide';
                //L'animateur n'a pas été associé à cette session et le status trainee de la ligne n'a pas été traitée
                if (empty($animator) && empty($userClient)) { //Si l'animateur n'existe pas et que l'utilisateur ne fait pas partie du client
                    //echo 'ANIMATOR 5 / L\'animateur n\'a pas été associé à cette session et n\'est lié à aucun client';
                    $animator = new User();
                    $animator->setClient($client)
                        ->setFirstName($currentRow->getAnimatorForname())
                        ->setLastName($currentRow->getAnimatorName())
                        ->setClearPassword($this->userSecurityService->randomPassword())
                        ->setEmail($currentRow->getAnimatorEmail())
                        //->setReference($currentRow->getTraineeIPN())
                        //->setReferenceClient($currentRow->getEmployeeID())
                        ->setCompany($client->getName())
                        ->setRoles(['ROLE_TRAINEE'])
                        ->setPreferredLang('fr');
                    $this->entityManager->persist($animator);
                    $this->entityManager->flush();
                } elseif (empty($animator) && !empty($userClient)) { //Si l'animateur n'est pas associé à la session et que l'utilisateur fait partie du client
                    //echo 'ANIMATOR 6 / L\'animateur n\'a pas été associé à cette session et est lié à un client';
                    $animator = $userClient;
                }
                $ppsrAnimator = $this->participantSessionSubscriber->subscribe($meeting, $animator, ProviderParticipantSessionRole::ROLE_ANIMATOR);
                $params = [
                    'idUser' => $animator->getId(),
                    'idMeeting' => $meeting->getId(),
                    'subdomainClient' => $configurationHolder->getDomain(),
                    'urlConnexionTrainee' => true,
                    'meeting' => $meeting,
                    'ppsr' => $ppsrAnimator,
                ];
                $this->updateResultTrainee(
                    $currentRow, self::RESULT_MSG_ANIMATOR_REGISTERED, $params
                );
            //$this->setUrlConnexion($currentRow, $ppsrAnimator);
            } elseif ($findAnimator && empty($stateTrainee)) {
                //echo 'ANIMATOR trouvé et le status du participant est vide';
                $params = [
                    'idUser' => $animator->getId(),
                    'idMeeting' => $meeting->getId(),
                    'subdomainClient' => $configurationHolder->getDomain(),
                    'urlConnexionTrainee' => true,
                    'meeting' => $meeting,
                    'ppsr' => $ppsrAnimator,
                ];
                $this->updateResultTrainee(
                    $currentRow, self::RESULT_MSG_ANIMATOR_ALREADY_REGISTERED, $params
                );
            }
        } else {  //RenaultImportSessionTemp::ROLE_TRAINEE
            //echo ' TRAINEE 1/ ---';
            $userClient = $this->userRepository->findOneBy(['email' => $currentRow->getTraineeEmail(), 'client' => $client]);
            $findTrainee = false;
            foreach ($meeting->getParticipantRoles() as $participant) {
                if ($participant->getRole() == ProviderParticipantSessionRole::ROLE_TRAINEE) {
                    //echo 'TRAINEE 2/ Le trainee associé à cette session';
                    if ($participant->getParticipant()->getEmail() == $currentRow->getTraineeEmail()) {
                        //echo 'TRAINEE 3/ L\'email du trainee associé à cette session est identique à l\'email du trainee de la ligne';
                        $ppsrTrainee = $participant;
                        $findTrainee = true;
                        //On récupère l'utilisateur s'il existe déjà en base (pour le compte)
                        /** @var User $trainee */
                        $trainee = $this->userRepository->findOneBy(['email' => $participant->getParticipant()->getEmail(), 'client' => $client]);
                        break;
                    }
                }
            }
            //Si l'email est a besoin du "retail" : @renaultfictif.com
            /*if(substr($currentRow->getTraineeEmail(), -18) == RenaultImportSessionTemp::EMAIL_RETAIL_STANDARD){
                $isRetail = true;
            }*/

            if (!$findTrainee && empty($stateTrainee)) {
                //echo 'TRAINEE non trouvé et le status du participant est vide';
                //Le trainee n'a pas été associé à cette session et le status trainee de la ligne n'a pas été traitée
                //echo '(' . $currentRow->getId() . ') >>> ' . $currentRow->getTraineeEmail() . ' || ' . $currentRow->getStatusTrainee() ;

                //Est-ce le participant doit toujours être inscrit à la session ?
                if ($currentRow->getStatusTrainee() == RenaultImportSessionTemp::TRAINEE_STATUS_REGISTERED) {
                    //echo 'TRAINEE 4 / Le trainee n\'a pas été associé à cette session et le status trainee (Registrered) de la ligne n\'a pas été traitée';
                    if (empty($trainee) && empty($userClient)) { //Si le participant n'est pas associé à la session et que l'utilisateur ne fait pas partie du client
                        //echo 'TRAINEE 5 / Le trainee n\'a pas été associé à cette session et n\'est lié à aucun client';
                        $trainee = new User();
                        $trainee->setClient($client)
                            ->setFirstName($currentRow->getTraineeForname())
                            ->setLastName($currentRow->getTraineeName())
                            ->setClearPassword($this->userSecurityService->randomPassword())
                            ->setEmail($currentRow->getTraineeEmail())
                            ->setCompany($client->getName())
                            ->setRoles(['ROLE_TRAINEE'])
                            ->setPreferredLang('fr');
                        if (!empty($currentRow->getTraineeIPN())) {
                            $trainee->setReference($currentRow->getTraineeIPN());
                        }
                        if (!empty($currentRow->getEmployeeID())) {
                            $trainee->setReferenceClient($currentRow->getEmployeeID());
                        }
                        $this->entityManager->persist($trainee);
                        $this->entityManager->flush();
                    } elseif (empty($trainee) && !empty($userClient)) { //Si le participant n'est pas associé à la session et que l'utilisateur fait partie du client
                        //echo 'TRAINEE 6 / Le trainee n\'a pas été associé à cette session et est lié à un client';
                        $trainee = $userClient;
                    }
                    $ppsrTrainee = $this->participantSessionSubscriber->subscribe($meeting, $trainee, ProviderParticipantSessionRole::ROLE_TRAINEE);
                    $params = [
                        'idUser' => $trainee->getId(),
                        'idMeeting' => $meeting->getId(),
                        'subdomainClient' => $configurationHolder->getDomain(),
                        'urlConnexionTrainee' => true,
                        'meeting' => $meeting,
                        'ppsr' => $ppsrTrainee,
                    ];
                    $this->updateResultTrainee(
                        $currentRow, self::RESULT_MSG_TRAINEE_REGISTERED, $params
                    );
                //$this->setUrlConnexion($currentRow, $ppsr);
                } else {
                    //echo 'TRAINEE 7 / Le trainee n\'a pas été associé à cette session et le status trainee (Unregistrered) de la ligne n\'a pas été traitée';
                    //On ne fait rien puisque le statut est une désassociation à la session
                    $params = [];
                    $this->updateResultTrainee(
                        $currentRow, self::RESULT_MSG_TRAINEE_NOACTION, $params
                    );
                    //echo $currentRow->getTraineeEmail().' aucune action à effectuer car le participant appelle une annulation alors qu\'il n\'est pas encore inscrit ('.$meeting->getId().')';
                }
            } elseif ($findTrainee && empty($stateTrainee)) {
                //echo 'TRAINEE trouvé et le status du participant est vide';
                $params = [
                    'idUser' => $trainee->getId(),
                    'idMeeting' => $meeting->getId(),
                    'subdomainClient' => $configurationHolder->getDomain(),
                    'urlConnexionTrainee' => true,
                    'meeting' => $meeting,
                    'ppsr' => $ppsrTrainee,
                ];
                $this->updateResultTrainee(
                    $currentRow, self::RESULT_MSG_ANIMATOR_ALREADY_REGISTERED, $params
                );
            }
        }

        // -------------------------------
        // --- Update current trainee row

        /*if( $currentRow->getRole() == 1 ){
            $currentRow->setResultAnimator( $resultMessage );
            $user = $this->userRepository->findOneBy(['email' => $currentRow->getAnimatorEmail(), 'client' => ])
            $ppsr = $this->providerParticipantSessionRoleRepository->findOneBy(['session' => $sessionId, 'participant' => $participantId])
        }else{
            $currentRow->setResultTrainee( $resultMessage );
        }

        if( !empty($params) ) {
            $currentRow->setUrlConnexionTrainee(null);
            if(!empty($params['urlConnexionTrainee'])) {
                if ($params['urlConnexionTrainee']) {
                    $login_url = ''; //$this->endPointDomain.$this->router->generate('_session_access_plateforme', ['token' => $providerParticipantSessionRole->getProviderSessionAccessToken()->getToken()]);
                    $currentRow->setUrlConnexionTrainee($login_url);
                }
            }
        }*/

        if (!empty($params)) {
            if (!empty($params['urlConnexionTrainee'])) {
                if ($params['urlConnexionTrainee']) {
                    if ($currentRow->getRole() == 2) {
                        $this->setUrlConnexion($currentRow, $ppsrTrainee);
                    } else {
                        $this->setUrlConnexion($currentRow, $ppsrAnimator);
                    }
                }
            }
        }

        return $currentRow;
    }

    private function processForTrainee(RenaultImportSessionTemp &$currentRow): RenaultImportSessionTemp
    {
        echo '-'.$currentRow->getId();

        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Renault']);

        /** @var AbstractProviderConfigurationHolder $configurationHolder */
        $configurationHolder = $client->getConfigurationHolders()[0];

        /** @var AdobeConnectSCO $meeting */
        $meeting = null;
        if (empty($currentRow->getReferenceLiveSession()) || !$currentRow->meetingIsWaitingScheduling()) {
            //echo '1 / Ligne ref LS est vide ou line n\'est pas meetingIsWaitingScheduling';
            //Aucun traitement car la ligne n'est associée à aucune session ou la session n'est pas avec un statut Open
            $params = [];
            $this->updateResultTrainee(
                $currentRow, self::RESULT_MSG_TRAINEE_NOACTION, $params
            );
            throw new \Exception('Aucun traitement à effectuer sur le traitement de l\'utilisateur de la ligne "'.$currentRow->getId().'"');
        } else {
            //echo '2 / Ligne ref LS n\'est pas vide ou line est meetingIsWaitingScheduling';
            $meeting = $this->providerSessionRepository->findOneBy(['ref' => $currentRow->getReferenceLiveSession()]);
        }

        if ($currentRow->getRole() == RenaultImportSessionTemp::ROLE_ANIMATOR) {
            $stateTrainee = $currentRow->getResultAnimator();
        } else {
            $stateTrainee = $currentRow->getResultTrainee();
        }
        /*dump( $currentRow->getId().' - '.$currentRow->getReferenceLiveSession().' - '.$currentRow->getReferenceRenault().' - '.$currentRow->getStatusTrainee() );
        dump( 'En attente de traitement ('.$currentRow->meetingIsWaitingScheduling().') || Annulation : ('.$currentRow->meetingIsCancelled().') || Facturé ('.$currentRow->meetingIsBilled().') || Dispensé ('.$currentRow->meetingIsDelivered().')' );
        dump( 'Role ('.$currentRow->getRole().') || Traitement effectué ('.$stateTrainee.') || Enregistré ('.$currentRow->traineeIsRegistered().') || Annulation : ('.$currentRow->traineeIsCanceled().') || Annulation facturée ('.$currentRow->traineeIsCanceledFees().') || Dispensé ('.$currentRow->traineeIsDelivered().')' );*/

        $ppsrAnimator = null;
        $ppsrTrainee = null;

        if ($currentRow->getRole() == RenaultImportSessionTemp::ROLE_ANIMATOR) {
            //echo ' ANIMATOR 1/ ---';
            $userClient = $this->userRepository->findOneBy(['email' => $currentRow->getAnimatorEmail(), 'client' => $client]);
            $findAnimator = false;
            $animator = null;
            foreach ($meeting->getParticipantRoles() as $participant) {
                if ($participant->getRole() == ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                    //echo 'ANIMATOR 2/ L\'animateur associé à cette session';
                    if ($participant->getParticipant()->getEmail() == $currentRow->getAnimatorEmail()) {
                        //echo 'ANIMATOR 3/ L\'email de l\'animateur associé à cette session est identique à l\'email du trainee de la ligne';
                        $ppsrAnimator = $participant;
                        $findAnimator = true;
                        //On récupère l'utilisateur s'il existe déjà en base (pour le compte)
                        /** @var User $animator */
                        $animator = $this->userRepository->findOneBy(['email' => $participant->getParticipant()->getEmail(), 'client' => $client]);
                        break;
                    }
                }
            }
            if (!$findAnimator && empty($stateTrainee)) {
                //echo 'ANIMATOR non trouvé et le status du participant est vide';
                //L'animateur n'a pas été associé à cette session et le status trainee de la ligne n'a pas été traitée
                if (empty($animator) && empty($userClient)) { //Si l'animateur n'existe pas et que l'utilisateur ne fait pas partie du client
                    //echo 'ANIMATOR 5 / L\'animateur n\'a pas été associé à cette session et n\'est lié à aucun client';
                    $animator = new User();
                    $animator->setClient($client)
                        ->setFirstName($currentRow->getAnimatorForname())
                        ->setLastName($currentRow->getAnimatorName())
                        ->setClearPassword($this->userSecurityService->randomPassword())
                        ->setEmail($currentRow->getAnimatorEmail())
                        //->setReference($currentRow->getTraineeIPN())
                        //->setReferenceClient($currentRow->getEmployeeID())
                        ->setCompany($client->getName())
                        ->setRoles(['ROLE_TRAINEE'])
                        ->setPreferredLang('fr');
                    $this->entityManager->persist($animator);
                    $this->entityManager->flush();
                } elseif (empty($animator) && !empty($userClient)) { //Si l'animateur n'est pas associé à la session et que l'utilisateur fait partie du client
                    //echo 'ANIMATOR 6 / L\'animateur n\'a pas été associé à cette session et est lié à un client';
                    $animator = $userClient;
                }
                $ppsrAnimator = $this->participantSessionSubscriber->subscribe($meeting, $animator, ProviderParticipantSessionRole::ROLE_ANIMATOR);
                $params = [
                    'idUser' => $animator->getId(),
                    'idMeeting' => $meeting->getId(),
                    'subdomainClient' => $configurationHolder->getDomain(),
                    'urlConnexionTrainee' => true,
                    'meeting' => $meeting,
                    'ppsr' => $ppsrAnimator,
                ];
                $this->updateResultTrainee(
                    $currentRow, self::RESULT_MSG_ANIMATOR_REGISTERED, $params
                );
            //$this->setUrlConnexion($currentRow, $ppsrAnimator);
            } elseif ($findAnimator && empty($stateTrainee)) {
                //echo 'ANIMATOR trouvé et le status du participant est vide';
                $params = [
                    'idUser' => $animator->getId(),
                    'idMeeting' => $meeting->getId(),
                    'subdomainClient' => $configurationHolder->getDomain(),
                    'urlConnexionTrainee' => true,
                    'meeting' => $meeting,
                    'ppsr' => $ppsrAnimator,
                ];
                $this->updateResultTrainee(
                    $currentRow, self::RESULT_MSG_ANIMATOR_ALREADY_REGISTERED, $params
                );
            }
        } else {  //RenaultImportSessionTemp::ROLE_TRAINEE
            //echo ' TRAINEE 1/ ---';
            $userClient = $this->userRepository->findOneBy(['email' => $currentRow->getTraineeEmail(), 'client' => $client]);
            $findTrainee = false;
            foreach ($meeting->getParticipantRoles() as $participant) {
                if ($participant->getRole() == ProviderParticipantSessionRole::ROLE_TRAINEE) {
                    //echo 'TRAINEE 2/ Le trainee associé à cette session';
                    if ($participant->getParticipant()->getEmail() == $currentRow->getTraineeEmail()) {
                        //echo 'TRAINEE 3/ L\'email du trainee associé à cette session est identique à l\'email du trainee de la ligne';
                        $ppsrTrainee = $participant;
                        $findTrainee = true;
                        //On récupère l'utilisateur s'il existe déjà en base (pour le compte)
                        /** @var User $trainee */
                        $trainee = $this->userRepository->findOneBy(['email' => $participant->getParticipant()->getEmail(), 'client' => $client]);
                        break;
                    }
                }
            }
            //Si l'email est a besoin du "retail" : @renaultfictif.com
            /*if(substr($currentRow->getTraineeEmail(), -18) == RenaultImportSessionTemp::EMAIL_RETAIL_STANDARD){
                $isRetail = true;
            }*/

            if (!$findTrainee && empty($stateTrainee)) {
                //echo 'TRAINEE non trouvé et le status du participant est vide';
                //Le trainee n'a pas été associé à cette session et le status trainee de la ligne n'a pas été traitée
                //echo '(' . $currentRow->getId() . ') >>> ' . $currentRow->getTraineeEmail() . ' || ' . $currentRow->getStatusTrainee() ;

                //Est-ce le participant doit toujours être inscrit à la session ?
                if ($currentRow->getStatusTrainee() == RenaultImportSessionTemp::TRAINEE_STATUS_REGISTERED) {
                    //echo 'TRAINEE 4 / Le trainee n\'a pas été associé à cette session et le status trainee (Registrered) de la ligne n\'a pas été traitée';
                    if (empty($trainee) && empty($userClient)) { //Si le participant n'est pas associé à la session et que l'utilisateur ne fait pas partie du client
                        //echo 'TRAINEE 5 / Le trainee n\'a pas été associé à cette session et n\'est lié à aucun client';
                        $trainee = new User();
                        $trainee->setClient($client)
                            ->setFirstName($currentRow->getTraineeForname())
                            ->setLastName($currentRow->getTraineeName())
                            ->setClearPassword($this->userSecurityService->randomPassword())
                            ->setEmail($currentRow->getTraineeEmail())
                            ->setCompany($client->getName())
                            ->setRoles(['ROLE_TRAINEE'])
                            ->setPreferredLang('fr');
                        if (!empty($currentRow->getTraineeIPN())) {
                            $trainee->setReference($currentRow->getTraineeIPN());
                        }
                        if (!empty($currentRow->getEmployeeID())) {
                            $trainee->setReferenceClient($currentRow->getEmployeeID());
                        }
                        $this->entityManager->persist($trainee);
                        $this->entityManager->flush();
                    } elseif (empty($trainee) && !empty($userClient)) { //Si le participant n'est pas associé à la session et que l'utilisateur fait partie du client
                        //echo 'TRAINEE 6 / Le trainee n\'a pas été associé à cette session et est lié à un client';
                        $trainee = $userClient;
                    }
                    $ppsrTrainee = $this->participantSessionSubscriber->subscribe($meeting, $trainee, ProviderParticipantSessionRole::ROLE_TRAINEE);
                    $params = [
                        'idUser' => $trainee->getId(),
                        'idMeeting' => $meeting->getId(),
                        'subdomainClient' => $configurationHolder->getDomain(),
                        'urlConnexionTrainee' => true,
                        'meeting' => $meeting,
                        'ppsr' => $ppsrTrainee,
                    ];
                    $this->updateResultTrainee(
                        $currentRow, self::RESULT_MSG_TRAINEE_REGISTERED, $params
                    );
                //$this->setUrlConnexion($currentRow, $ppsr);
                } else {
                    //echo 'TRAINEE 7 / Le trainee n\'a pas été associé à cette session et le status trainee (Unregistrered) de la ligne n\'a pas été traitée';
                    //On ne fait rien puisque le statut est une désassociation à la session
                    $params = [];
                    $this->updateResultTrainee(
                        $currentRow, self::RESULT_MSG_TRAINEE_NOACTION, $params
                    );
                    //echo $currentRow->getTraineeEmail().' aucune action à effectuer car le participant appelle une annulation alors qu\'il n\'est pas encore inscrit ('.$meeting->getId().')';
                }
            } elseif ($findTrainee && empty($stateTrainee)) {
                //echo 'TRAINEE trouvé et le status du participant est vide';
                $params = [
                    'idUser' => $trainee->getId(),
                    'idMeeting' => $meeting->getId(),
                    'subdomainClient' => $configurationHolder->getDomain(),
                    'urlConnexionTrainee' => true,
                    'meeting' => $meeting,
                    'ppsr' => $ppsrTrainee,
                ];
                $this->updateResultTrainee(
                    $currentRow, self::RESULT_MSG_ANIMATOR_ALREADY_REGISTERED, $params
                );
            }
        }

        // -------------------------------
        // --- Update current trainee row

        /*if( $currentRow->getRole() == 1 ){
            $currentRow->setResultAnimator( $resultMessage );
            $user = $this->userRepository->findOneBy(['email' => $currentRow->getAnimatorEmail(), 'client' => ])
            $ppsr = $this->providerParticipantSessionRoleRepository->findOneBy(['session' => $sessionId, 'participant' => $participantId])
        }else{
            $currentRow->setResultTrainee( $resultMessage );
        }

        if( !empty($params) ) {
            $currentRow->setUrlConnexionTrainee(null);
            if(!empty($params['urlConnexionTrainee'])) {
                if ($params['urlConnexionTrainee']) {
                    $login_url = ''; //$this->endPointDomain.$this->router->generate('_session_access_plateforme', ['token' => $providerParticipantSessionRole->getProviderSessionAccessToken()->getToken()]);
                    $currentRow->setUrlConnexionTrainee($login_url);
                }
            }
        }*/

        if (!empty($params)) {
            if (!empty($params['urlConnexionTrainee'])) {
                if ($params['urlConnexionTrainee']) {
                    if ($currentRow->getRole() == 2) {
                        $this->setUrlConnexion($currentRow, $ppsrTrainee);
                    } else {
                        $this->setUrlConnexion($currentRow, $ppsrAnimator);
                    }
                }
            }
        }

        return $currentRow;
    }

    private function setUrlConnexion(RenaultImportSessionTemp $currentRow, ProviderParticipantSessionRole $providerParticipantSessionRole): void
    {
        /*if(!empty($currentRow->getUrlConnexionTrainee())) {
            if ($currentRow->getUrlConnexionTrainee()) {*/
        $login_url = $this->endPointDomain.$this->router->generate('_session_access_plateforme', ['token' => $providerParticipantSessionRole->getProviderSessionAccessToken()->getToken()]);
        $currentRow->setUrlConnexionTrainee($login_url);
        /*}
        }*/
    }

    private function updateResultTrainee(RenaultImportSessionTemp &$currentRow, $key, array $params = []): RenaultImportSessionTemp
    {
        $resultMessage = '';
        $resultSuccess = true;

        switch ($key) {
            // -------------------------------
            // --- ANIMATOR REGISTERED
            case self::RESULT_MSG_ANIMATOR_REGISTERED:
                $resultSuccess = true;
                $resultMessage = $this->translator->trans(
                    '(%state%) Session %meeting% (reference Renault "%ref_meeting%") animator registered !',
                    [
                        '%state%' => self::STATE_PROCESS_ANIMATOR_REGISTERED,
                        '%meeting%' => $currentRow->getName(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );
                break;
            // -------------------------------

            // -------------------------------
            // --- ANIMATOR ALREADY REGISTERED
            case self::RESULT_MSG_ANIMATOR_ALREADY_REGISTERED:
                $resultSuccess = true;
                $resultMessage = $this->translator->trans(
                    '(%state%) Session %meeting% (reference Renault "%ref_meeting%") animator already registered !',
                    [
                        '%state%' => self::STATE_PROCESS_ANIMATOR_ALREADY_REGISTERED,
                        '%meeting%' => $currentRow->getName(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );
                break;
            // -------------------------------

            // -------------------------------
            // --- TRAINEE REGISTERED
            case self::RESULT_MSG_TRAINEE_REGISTERED:
                $resultSuccess = true;
                $resultMessage = $this->translator->trans(
                    '(%state%) Trainee %trainee% (reference Renault "%ref_meeting%") registered !',
                    [
                        '%state%' => self::STATE_PROCESS_TRAINEE_REGISTERED,
                        '%trainee%' => $currentRow->getTraineeEmail(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );
                break;
            // -------------------------------

            // -------------------------------
            // --- TRAINEE ALREADY REGISTERED
            case self::RESULT_MSG_TRAINEE_ALREADY_REGISTERED:
                $resultSuccess = true;
                $resultMessage = $this->translator->trans(
                    '(%state%) Trainee %trainee% (reference Renault "%ref_meeting%") already registered !',
                    [
                        '%state%' => self::STATE_PROCESS_TRAINEE_ALREADY_REGISTERED,
                        '%trainee%' => $currentRow->getTraineeEmail(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );
                break;
            // -------------------------------

            // -------------------------------
            // --- TRAINEE CANCELLED
            case self::RESULT_MSG_TRAINEE_CANCELLED:
                $resultSuccess = true;
                $resultMessage = $this->translator->trans(
                    '(%state%) Trainee %trainee% (reference Renault "%ref_meeting%") unsubscribe !',
                    [
                        '%state%' => self::STATE_PROCESS_TRAINEE_CANCELLED,
                        '%trainee%' => $currentRow->getTraineeEmail(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );
                break;
            // -------------------------------

            // -------------------------------
            // --- TRAINEE NOACTION
            case self::RESULT_MSG_TRAINEE_NOACTION:
                $resultSuccess = true;
                $resultMessage = $this->translator->trans(
                    '(%state%) Trainee %trainee% (reference Renault "%ref_meeting%") no action !',
                    [
                        '%state%' => self::STATE_PROCESS_TRAINEE_NOACTION,
                        '%trainee%' => $currentRow->getTraineeEmail(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );
//                    dump($resultMessage);
                break;
            // -------------------------------

            // -------------------------------
            // --- RLTMeetingImportInvalidStatusException exception
            case self::RESULT_MSG_TRAINEE_INVALID_STATUS:
                $e = $params['e'];
                $resultSuccess = false;
                $resultMessage = $this->translator->trans(
                    '(%state%) Trainee %idrow% (reference Renault "%ref_meeting%") | Mistake status (%name%) !',
                    [
                        '%state%' => self::STATE_PROCESS_TRAINEE_ERROR,
                        '%name%' => $currentRow->getName(),
                        '%idrow%' => $currentRow->getId(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );

                $this->logger->error(
                    sprintf(
                        '[RLTTraineeImportService][Error][#%s] Meeting: %s | Message: %s',
                        $currentRow->getId(),
                        $currentRow->getReferenceRenault(),
                        $e->getMessage()
                    )
                );
                break;
            // -------------------------------

            // -------------------------------
            // --- AdobeConnectException exception
            case self::RESULT_MSG_AC_UNKNOWN_ERROR:
                $e = $params['e'];
                $resultSuccess = false;
                $resultMessage = $this->translator->trans(
                    '(%state%) Trainee %idrow% (reference Renault "%ref_meeting%") | Unknown AC mistake (%name%) !',
                    [
                        '%state%' => self::STATE_PROCESS_TRAINEE_ERROR,
                        '%name%' => $currentRow->getName(),
                        '%idrow%' => $currentRow->getId(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );

                $this->logger->error(
                    sprintf(
                        '[RLTTraineeImportService][Error][#%s] Meeting: %s | Message: %s',
                        $currentRow->getId(),
                        $currentRow->getReferenceRenault(),
                        $e->getMessage()
                    )
                );
                break;
            // -------------------------------

            // -------------------------------
            // --- Exception exception
            case self::RESULT_MSG_UNKNOWN_ERROR:
                $e = $params['e'];
                $resultSuccess = false;
                $resultMessage = $this->translator->trans(
                    '(%state%) Trainee %idrow% (reference Renault "%ref_meeting%") | Unknown mistake (%name%) !',
                    [
                        '%state%' => self::STATE_PROCESS_TRAINEE_ERROR,
                        '%name%' => $currentRow->getName(),
                        '%idrow%' => $currentRow->getId(),
                        '%ref_meeting%' => $currentRow->getReferenceRenault(),
                    ]
                );

                $this->logger->error(
                    sprintf(
                        '[RLTTraineeImportService][Error][#%s] Meeting: %s | Message: %s',
                        $currentRow->getId(),
                        $currentRow->getReferenceRenault(),
                        $e->getMessage()
                    )
                );
                break;
            // -------------------------------
        }

        // -------------------------------
        // --- Update current trainee row

        if ($currentRow->getRole() == 1) {
            $currentRow->setResultAnimator($resultMessage);
        } else {
            $currentRow->setResultTrainee($resultMessage);
        }

        /*if( !empty($params) ) {
            $currentRow->setUrlConnexionTrainee(null);
            if(!empty($params['urlConnexionTrainee'])) {
                if ($params['urlConnexionTrainee']) {
                    $login_url = ''; //$this->endPointDomain.$this->router->generate('_session_access_plateforme', ['token' => $providerParticipantSessionRole->getProviderSessionAccessToken()->getToken()]);
                    $currentRow->setUrlConnexionTrainee($login_url);
                }
            }
        }*/

        return $currentRow;
    }
}
