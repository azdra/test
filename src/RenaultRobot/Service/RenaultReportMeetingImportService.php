<?php

namespace App\RenaultRobot\Service;

use App\RenaultRobot\Entity\RenaultImportSessionTemp;
use App\RenaultRobot\Repository\RenaultImportSessionTempRepository;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Contracts\Translation\TranslatorInterface;

class RenaultReportMeetingImportService
{
    public function __construct(
        protected string $renaultSavefilesDirectory,
        protected EntityManagerInterface $entityManager,
        protected RenaultImportSessionTempRepository $renaultImportSessionTempRepository,
        protected ClientRepository $clientRepository,
        protected LoggerInterface $logger,
        protected TranslatorInterface $translator,
    ) {
    }

    public function reportImportSessions(): array
    {
        $sessionsImport = $this->renaultImportSessionTempRepository->getImportDDay();

        $result = $this->generateExportResultImport($sessionsImport, 'xls', false);

        //export_access_ddmmyyyyhhiiss.xlsx
        $dday = new \DateTime();
        $key = 'export_access_'.$dday->format('dmYhis');

        $fs = new Filesystem();
        if (!$fs->exists($this->renaultSavefilesDirectory)) {
            $fs->mkdir($this->renaultSavefilesDirectory);
        }

        if ($result) {
            (new Xlsx($result))->save($this->renaultSavefilesDirectory.'/'.$key.'.xlsx');
        }

        $response = [
            'link' => $this->renaultSavefilesDirectory.'/'.$key.'.xlsx', // __DIR__ . self::TMP_DIR . $key . '.xlsx',
            'name' => $key.'.xlsx',
        ];

        return $response;
    }

    public function generateExportResultImport($linesImported, $format = 'xls'): ?Spreadsheet
    {
        switch ($format) {
            case 'xls':
                $row = 0;
                $excel = $this->createExcelRenault($row);
                foreach ($linesImported as $line) {
                    if ($line->traineeIsCanceled() != RenaultImportSessionTemp::TRAINEE_STATUS_CANCELLED) {
                        $excel = $this->generateRowLine($excel, $row, $line);
                    }
                }

                return $excel;
                break;
        }

        return null;
    }

    private function createExcelRenault(&$row = 0): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('Mylivesession Manager')
            ->setLastModifiedBy(('Mylivesession Manager'))
            ->setTitle('Importation Renault');
        $spreadsheet->setActiveSheetIndex(0);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getDefaultRowDimension(1)->setRowHeight(20);

        ++$row;
        $spreadsheet->getActiveSheet()
            ->setCellValue('A'.$row, 'Nom de la session')
            ->setCellValue('B'.$row, 'Référence de la session')   //(= référence Live Session)
            ->setCellValue('C'.$row, 'Numéro de la formation')    //(= référence Renault)
            ->setCellValue('D'.$row, 'Email')
            ->setCellValue('E'.$row, 'Prénom')
            ->setCellValue('F'.$row, 'Nom')
            ->setCellValue('G'.$row, 'IPN utilisateur')           //IPN utilisateur
            ->setCellValue('H'.$row, 'Employee_ID')               //IPN utilisateur
            ->setCellValue('I'.$row, 'Rôle')                      //(Animateur/Participant)
            ->setCellValue('J'.$row, 'Date début session')        //(dd/mm/yyyy)
            ->setCellValue('K'.$row, 'Date fin session')          //(dd/mm/yyyy)
            ->setCellValue('L'.$row, 'Heure début session')       //(HH:ii)
            ->setCellValue('M'.$row, 'Heure fin session')
            ->setCellValue('N'.$row, 'URL de connexion du participant');
        ++$row;

        return $spreadsheet;
    }

    protected function generateRowLine(Spreadsheet $excel, &$row, $line): Spreadsheet
    {
        //if ($excel instanceof Spreadsheet){
        //1. Nom de la session
        //2. Référence de la session (= référence Live Session)
        //3. Numéro de la formation (= référence Renault)
        //4. Email
        //5. Prénom
        //6. Nom
        //7. IPN utilisateur
        //8. Employee_id
        //9. Rôle (Animateur/Participant)
        //10. Date début session (dd/mm/yyyy)
        //11. Date fin session (dd/mm/yyyy)
        //12. Heure début session (HH:ii)
        //13. Heure fin session (HH:ii)
        //14. URL de connexion du participant
        //15. URL session

        $emailUser = null;
        $nameUser = null;
        $fornameUser = null;
        $ipnUser = null;
        $roleUser = null;
        $employeeIDUser = null;
        if ($line->getRole() == 1) {   //Animateur
            $emailUser = $line->getAnimatorEmail();
            $nameUser = $line->getAnimatorName();
            $fornameUser = $line->getAnimatorForname();
            $roleUser = 'Animateur';
        } else {  //Participant
            $emailUser = $line->getTraineeEmail();
            $nameUser = $line->getTraineeName();
            $fornameUser = $line->getTraineeForname();
            $ipnUser = $line->getTraineeIPN();
            $roleUser = 'Participant';
            $employeeIDUser = $line->getEmployeeID();
        }

        $dateTimeStartOrigin = \DateTimeImmutable::createFromFormat('d/m/Y H:i', $line->getDateStart()->format('d/m/Y H:i'), new \DateTimeZone('UTC'));
        $dateTimeEndOrigin = \DateTimeImmutable::createFromFormat('d/m/Y H:i', $line->getDateEnd()->format('d/m/Y H:i'), new \DateTimeZone('UTC'));
        $dateTimeStartConvert = (clone $dateTimeStartOrigin)->setTimezone(new \DateTimeZone('Europe/Paris'));
        $dateTimeEndConvert = (clone $dateTimeEndOrigin)->setTimezone(new \DateTimeZone('Europe/Paris'));
        $lineDateStart = \DateTime::createFromImmutable($dateTimeStartConvert);
        $lineDateEnd = \DateTime::createFromImmutable($dateTimeEndConvert);
        $excel->getActiveSheet()
                ->setCellValue('A'.$row, $line->getName())
                ->setCellValueExplicit('B'.$row, (!empty($line->getReferenceLiveSession())) ? $line->getReferenceLiveSession() : 'En attente de référence (session MLSM pas encore créée)', \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
                ->setCellValueExplicit('C'.$row, $line->getReferenceRenault(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
                ->setCellValue('D'.$row, $emailUser)
                ->setCellValue('E'.$row, $fornameUser)
                ->setCellValue('F'.$row, $nameUser)
                ->setCellValueExplicit('G'.$row, $ipnUser, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
                ->setCellValueExplicit('H'.$row, $employeeIDUser, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
                ->setCellValue('I'.$row, $roleUser)
                ->setCellValue('J'.$row, $lineDateStart->format('Y-m-d'))
                ->setCellValue('K'.$row, $lineDateEnd->format('Y-m-d'))
                ->setCellValue('L'.$row, $lineDateStart->format('H:i'))
                ->setCellValue('M'.$row, $lineDateEnd->format('H:i'))
                ->setCellValue('N'.$row, (!empty($line->getUrlConnexionTrainee())) ? $line->getUrlConnexionTrainee() : '');
        ++$row;

        return $excel;
    }
}
