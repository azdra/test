<?php

namespace App\RenaultRobot\Service;

use App\RenaultRobot\Entity\RenaultImportSessionTemp;
use App\RenaultRobot\Entity\RenaultImportSessionTempErrors;
use App\RenaultRobot\Repository\RenaultImportSessionTempRepository;
use App\Repository\ClientRepository;
use App\Service\Utils\SpreadsheetUtils;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RenaultService
{
    public const MAX_CONSECUTIVE_ROWS = 500;

    public const RESULT_MSG_MEETING_CREATED = 'meeting_created';
    public const RESULT_MSG_MEETING_NOCHANGE = 'meeting_nochange';
    public const RESULT_MSG_MEETING_CHANGE_DURING = 'meeting_change_during';
    public const RESULT_MSG_MEETING_INVALID_STATUS = 'meeting-invalid-status';
    public const RESULT_MSG_MEETING_CANCELLED = 'meeting-cancelled';
    public const RESULT_MSG_MEETING_UNCANCELLED = 'meeting-uncancelled';

    public const RESULT_MSG_AC_UNKNOWN_ERROR = 'ac-unknown-error';
    public const RESULT_MSG_UNKNOWN_ERROR = 'unknown-error';

    public function __construct(
        protected string $renaultSavefilesDirectory,
        protected string $pythonScriptsDirectory,
        protected EntityManagerInterface $entityManager,
        protected RenaultImportSessionTempRepository $renaultImportSessionTempRepository,
        protected ClientRepository $clientRepository,
        protected LoggerInterface $logger,
        protected TranslatorInterface $translator
    ) {
    }

    public function getExtractDataFromFileSFTP(): array
    {
        $scriptPythonPath = $this->pythonScriptsDirectory.'/Renault/rcampus_data_cleanup.py';
        $scriptPythonPathEscaped = escapeshellarg($scriptPythonPath);
        $command = 'python3 '.$scriptPythonPathEscaped;
        /** @psalm-suppress ForbiddenCode */
        shell_exec($command);

        $ddaynumber = ( new \DateTime() )->format('w');
        $file = $this->renaultSavefilesDirectory.'/RCAMPUS_filtered_'.$ddaynumber.'.xlsx';
        //Pour dev :
        //$file = $this->renaultSavefilesDirectory.'/RCAMPUS_6c.xlsx';
        //echo '------------------ | '.$file.' |-----------------';
        $arrayMeetings = $this->sessionsToArray($file, [0], 50, null);

        return $arrayMeetings;
    }

    public function sessionsToArray($inputFileName, $ignore_ligne = [], $delimiter = null, $encoding = null): array
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '3600');
        $this->logger->info(sprintf('%s', '### RLT sessionsToArray ### : Start !'));
        //echo '------------------ | ### RLT sessionsToArray ### : Start |-----------------';
        $rowsData = [];
        $rowNum = 0;

        try {
            $inputFileType = IOFactory::identify($inputFileName);
            $objReaderxls = IOFactory::createReader($inputFileType);
            $objReaderxls->setReadDataOnly(true);
            $objReaderxls->setReadEmptyCells(false);
            $objPHPExcel = $objReaderxls->load($inputFileName);

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            if ($highestRow > 200000) {
                $highestRow = 200000;
            }

            if (strlen($highestColumn) > 1) {
                $highestColumn = 'AF';
            }

            $client = $this->clientRepository->findOneBy(['name' => 'Renault']);
            //echo '------------------ | '.$client->getName().' |-----------------';

            foreach ($sheet->getRowIterator(2, $highestRow) as $row) {
                ++$rowNum;
                $colNum = 0;
                //echo '------------------ | '.$rowNum.' |-----------------';
                foreach ($row->getCellIterator('A', $highestColumn) as $cell) {
                    ++$colNum;
                    $invDate = $cell->getValue();
                    $letterColumn = SpreadsheetUtils::numberToLetter($colNum);
                    //echo '------------------ | '.$letterColumn.' |-----------------';
                    if ($letterColumn == 'R' || $letterColumn == 'S' || $letterColumn == 'W') {
                        $value = SpreadsheetUtils::parseExcelDate($invDate, 'd/m/Y');
                    } elseif ($letterColumn == 'T' || $letterColumn == 'U') {
                        $value = SpreadsheetUtils::parseExcelDate($invDate, 'H:i', new \DateTimeZone($client->getTimezone()));
                    } else {
                        $value = $cell->getValue();
                    }
                    $rowsData[$rowNum][$colNum] = $value;
                }
            }
        } catch (\Exception $e) {
            return [];
        }
        $this->logger->info(sprintf('%s', '### RLT sessionsToArray ### : End !'));

        return $rowsData;
    }

    public function prepareDataForPersist(array $response, \DateTime $dateMin, \DateTime $dateMax): array
    {
        $numRow = 0;
        $torec = 0;
        $arrSessions = [];
        $errorSessions = [];
        $radicalTest = RenaultImportSessionTemp::TEMP_TEST;

        foreach ($response as $k => $v) {
            $errors = [];
            ++$numRow;
            $dateTimeStart = \DateTime::createFromFormat('d/m/Y H:i', $v[18].' '.$v[20]);
            $dateTimeEnd = \DateTime::createFromFormat('d/m/Y H:i', $v[19].' '.$v[21]);

            $dateStart1 = \DateTime::createFromFormat('d/m/Y H:i', $v[18].' 00:00');
            $dateEnd2 = \DateTime::createFromFormat('d/m/Y H:i', $v[19].' 00:00');

            if ($dateStart1 < $dateEnd2 || $dateStart1 == $dateEnd2) {
                $nbrDays = $dateEnd2->diff($dateStart1)->format('%a');
                $nbrDays = intval($nbrDays) + 1;
                if ($nbrDays > 5) {
                    //$errors[$v[26]]['nbrDaysMore'] = [
                    $errors[$radicalTest.$v[26]]['nbrDaysMore'] = [
                        'message' => 'Nombre de jours supérieur à 5 en ligne '.$k.' ('.$nbrDays.' jours || DS : '.$dateTimeStart->format('d/m/Y').' jusqu\'à DE '.$dateTimeEnd->format('d/m/Y').')',
                    ];
                }
            } else {
                //$errors[$v[26]]['diffDayDate'] = [
                $errors[$radicalTest.$v[26]]['diffDayDate'] = [
                    'message' => 'Comparaison de dates impossible en ligne '.$k.' (DE : '.$dateTimeEnd->format('d/m/Y').' est avant DS '.$dateTimeStart->format('d/m/Y').')',
                ];
                $nbrDays = 1;
            }

            $duration = $this->diffMinDate($dateTimeStart, $dateTimeEnd);
            if (empty($duration)) {
                //$errors[$v[26]]['diffMinDate'] = [
                $errors[$radicalTest.$v[26]]['diffMinDate'] = [
                    'message' => 'Différence de dates impossible en ligne '.$k,
                ];
            }

            $rejectDayMin = \DateTime::createFromFormat('Y-m-d H:i:s', '2022-10-10 00:00:00');
            $rejectDayMax = \DateTime::createFromFormat('Y-m-d H:i:s', '2022-10-10 23:59:59');
            if ($this->betweenDates($dateTimeStart, $dateMin, $dateMax) && !$this->betweenDates($dateTimeStart, $rejectDayMin, $rejectDayMax) && (substr($v[16], 0, 3) == 'CVT' || substr($v[16], 0, 3) == 'CVC' || substr($v[16], 0, 3) == 'WEB')) {    //Compris dans les 15 jours suivants
                ++$torec;
                $dateTimeStart = \DateTimeImmutable::createFromFormat('d/m/Y H:i', $v[18].' '.$v[20], new \DateTimeZone('Europe/Paris'));
                $dateTimeEnd = \DateTimeImmutable::createFromFormat('d/m/Y H:i', $v[19].' '.$v[21], new \DateTimeZone('Europe/Paris'));
                $dateTimeStart2 = (clone $dateTimeStart)->setTimezone(new \DateTimeZone('UTC'));
                $dateTimeEnd2 = (clone $dateTimeEnd)->setTimezone(new \DateTimeZone('UTC'));

                $dateStart1 = \DateTimeImmutable::createFromFormat('d/m/Y H:i', $v[18].' 00:00');
                $dateEnd2 = \DateTimeImmutable::createFromFormat('d/m/Y H:i', $v[19].' 00:00');
                //TRAITEMENT DES INFORMATIONS PARTICIPANTS
                $traineeEmail = '';
                $traineeName = '';
                $traineeForname = '';
                if (!empty($v[8])) {
                    if (strlen($v[8]) > 59 and !empty($v[31])) {
                        //$output->writeln( "\t> ------".$v[8]." (".strlen($v[8]).") -------------");
                        $v[8] = $v[31].RenaultImportSessionTemp::EMAIL_RETAIL_STANDARD;
                    }
                }

                if (empty($v[8]) && empty($v[7]) && empty($v[31])) { //SI “Email personne” == Vide  ET “IPN personne” == Vide ET "Employee ID" == Vide ALORS
                    //$errors[$v[26]]['noActionTrainee'] = [
                    $errors[$radicalTest.$v[26]]['noActionTrainee'] = [
                        'message' => '“Email personne” == Vide  ET “IPN personne” == Vide ET "Code Bir Personne" == Vide',
                    ];
                } elseif (empty($v[8]) && !empty($v[7]) && !empty($v[31]) && !empty($v[5])) {  //SINON SI “Email personne” == Vide  ET “IPN personne” != Vide ET "Employee ID" != Vide ET “Nom stagiaire” != Vide ALORS
                    $traineeEmail = $v[31].RenaultImportSessionTemp::EMAIL_RETAIL_STANDARD;
                    $traineeName = $v[5];
                    $traineeForname = $v[6];
                } elseif (!empty($v[8]) && empty($v[7]) && empty($v[31]) && !empty($v[5])) {  //SINON SI “Email personne” != Vide ET “IPN personne” == Vide ET "Employee ID" == Vide ET “Nom stagiaire” != Vide ALORS
                    if (strlen($v[8]) < 60) {
                        //Ajout  participant
                        $traineeEmail = $this->enleveaccents($v[8]);
                        $traineeName = $v[5];
                        $traineeForname = $v[6];
                        if (!preg_match('/^.+\@\S+\.\S+$/', $v[8])) {
                            $errors[$radicalTest.$v[26]]['noCorrectEmail'] = ['message' => '“Email trainee” == '.$traineeEmail.' n\'est pas un email valide'];
                        }
                    } else {
                        //$errors[$v[26]]['noactiontrainee'] = [
                        $errors[$radicalTest.$v[26]]['noactiontrainee'] = [
                            'message' => '“Email personne” > 59 ET “IPN personne” == Vide ET “Employee ID” == Vide ET “Nom stagiaire” != Vide',
                        ];
                    }
                } elseif (empty($v[8]) && !empty($v[7]) && !empty($v[31]) && empty($v[5])) {   //SINON SI “Email personne” == Vide  ET “IPN personne” != Vide ET "Employee ID" != Vide ET “Nom stagiaire” == Vide ALORS
                    //$errors[$v[26]]['noactiontrainee'] = [
                    $errors[$radicalTest.$v[26]]['noactiontrainee'] = [
                        'message' => '“Email personne” == Vide  ET “IPN personne” != Vide ET “Nom stagiaire” == Vide',
                    ];
                } elseif (!empty($v[8]) && !empty($v[7]) && empty($v[31]) && !empty($v[5])) {
                    $traineeName = $radicalTest.$v[5];
                    $traineeForname = $radicalTest.$v[6];
                    $traineeEmail = $this->enleveaccents($radicalTest.$v[8]);
                    if (strlen($v[8]) > 59) {
                        $v[8] = $v[7].RenaultImportSessionTemp::EMAIL_RETAIL_STANDARD;
                        $traineeEmail = $radicalTest.$v[8];
                    } else {
                        if (!preg_match('/^.+\@\S+\.\S+$/', $traineeEmail)) {
                            $errors[$radicalTest.$v[26]]['noCorrectEmail'] = ['message' => '“Email trainee” == '.$traineeEmail.' n\'est pas un email valide'];
                        }
                    }
                } elseif (!empty($v[8]) && !empty($v[7]) && !empty($v[31]) && !empty($v[5])) {
                    $traineeName = $radicalTest.$v[5];
                    $traineeForname = $radicalTest.$v[6];
                    $traineeEmail = $this->enleveaccents($radicalTest.$v[8]);
                    if (strlen($v[8]) > 59) {
                        $v[8] = $v[31].RenaultImportSessionTemp::EMAIL_RETAIL_STANDARD;
                        $traineeEmail = $radicalTest.$v[8];
                    } else {
                        if (!preg_match('/^.+\@\S+\.\S+$/', $traineeEmail)) {
                            $errors[$radicalTest.$v[26]]['noCorrectEmail'] = ['message' => '“Email trainee” == '.$traineeEmail.' n\'est pas un email valide'];
                        }
                    }
                }

                if (empty($traineeForname) && !empty($traineeEmail)) {
                    $traineeForname = $radicalTest.'fornamertl'.strval(random_int(10000, 99999999));
                }

                //TRAITEMENT DES INFORMATIONS ANIMATEURS
                if (!empty($v[1]) && !empty($v[3])) {
                    /*if (substr($v[3], -12) !== '@renault.com') {
                        //$errors[$v[26]]['emailAnimatorNotRenault'] = [
                        $errors[$radicalTest.$v[26]]['emailAnimatorNotRenault'] = [
                            'message' => '“Email Formateur” == '.$v[3].' n\'est pas @renault.com',
                        ];
                    }*/
                    if (!preg_match('/^.+\@\S+\.\S+$/', $v[3])) {
                        $errors[$radicalTest.$v[26]]['noCorrectEmail'] = ['message' => '“Email animateur” == '.$v[3].' n\'est pas un email valide'];
                    }
                } else {
                    //$errors[$v[26]]['noInfoAnimator'] = [
                    /*if (substr($v[16].$radicalTest, 0, 3) == 'WEB') {
                        $nameF = (empty($v[1])) ? 'Barthellemy' : $v[1];
                        $emailF = (empty($v[3])) ? 'yannbarthellemy@gmail.com' : $v[3];
                    } else {*/
                    $nameF = (empty($v[1])) ? 'vide' : $v[1];
                    $emailF = (empty($v[3])) ? 'vide' : $v[3];
                    $errors[$radicalTest.$v[26]]['noInfoAnimator'] = [
                            'message' => 'Nom Formateur == '.$nameF.'  ET Email Formateur == '.$emailF,
                        ];
                    //}
                }

                if (empty($errors[$radicalTest.$v[26]])) {
                    //TRAITEMENT DU TABLEAU DE PRE-STOCKAGE DES FORMATIONS
                    $sessionExist = false;
                    $keySession = null;
                    foreach ($arrSessions as $keyS => $valS) {
                        if ($valS['referenceRLT'] == $radicalTest.$v[26]) {
                            $sessionExist = true;
                            $keySession = $radicalTest.$v[26]; //$keyS;
                        }
                    }
                    $traitementFormation = 'Nothing';
                    if (trim($v[29]) == RenaultImportSessionTemp::MEETING_STATUS_OPEN) {
                        $traitementFormation = 'Create';
                    } elseif (trim($v[29]) == RenaultImportSessionTemp::MEETING_STATUS_CANCELLED) {
                        $traitementFormation = 'Cancel';
                    } elseif (trim($v[29]) == RenaultImportSessionTemp::MEETING_STATUS_BILLED) {
                        $traitementFormation = 'Cancel';
                    } elseif (trim($v[29]) == RenaultImportSessionTemp::MEETING_STATUS_DELIVERED) {
                        $traitementFormation = 'Cancel';
                    }

                    if (!$sessionExist) {
                        //Si la session n'existe pas on entre les nouvelles valeurs
                        $arrSessions[$radicalTest.$v[26]] = [
                            'numRow' => $numRow,
                            'name' => $v[16].$radicalTest,  //P
                            'model' => substr($v[16], 0, 11),
                            'referenceLS' => '',
                            'referenceRLT' => $radicalTest.$v[26],  //S
                            'resultType' => RenaultImportSessionTemp::WAITING,
                            'resultText' => null,
                            'statusFormation' => trim($v[29]),  //AC
                            'traitementFormation' => $traitementFormation,
                            'dateStart' => $dateTimeStart2,
                            'dateEnd' => $dateTimeEnd2,
                            'nbrDays' => $nbrDays,
                            'numDay' => 1,
                            'trainees' => [],
                            'animator' => [],
                        ];
                    } else {
                        //Si la session existe on modifie par les dernières valeurs
                        //$oldReferenceLS = $arrSessions[$keySession]['referenceLS'];
                        $arrSessions[$keySession] = [
                            'numRow' => $numRow,
                            'name' => $v[16].$radicalTest,  //P
                            'model' => substr($v[16], 0, 11),
                            'referenceLS' => '',
                            'referenceRLT' => $radicalTest.$v[26],  //S
                            'resultType' => RenaultImportSessionTemp::WAITING,
                            'resultText' => null,
                            'statusFormation' => trim($v[29]),  //AC
                            'traitementFormation' => $traitementFormation,
                            'dateStart' => $dateTimeStart2,
                            'dateEnd' => $dateTimeEnd2,
                            'nbrDays' => $nbrDays,
                            'numDay' => 1,
                            'trainees' => $arrSessions[$keySession]['trainees'],
                            'animator' => $arrSessions[$keySession]['animator'],
                        ];
                    }

                    //TRAITEMENT DU TABLEAU DE PRE-STOCKAGE DES PARTICIPANTS
                    if (!empty($traineeEmail)) {
                        if (!$sessionExist) {
                            $arrSessions[$radicalTest.$v[26]]['trainees'][$k] = [
                                'numRow' => $numRow,
                                'email' => $traineeEmail,
                                'forname' => $traineeForname,
                                'name' => $traineeName,
                                'IPN' => $v[7], //G
                                'employeeID' => $v[31], //AE
                                'statusTrainee' => trim($v[28]), //AB
                            ];
                        } else {
                            $arrSessions[$keySession]['trainees'][$k] = [
                                'numRow' => $numRow,
                                'email' => $traineeEmail,
                                'forname' => $traineeForname,
                                'name' => $traineeName,
                                'IPN' => $v[7], //G
                                'employeeID' => $v[31], //AE
                                'statusTrainee' => trim($v[28]), //AB
                            ];
                        }
                    }

                    //TRAITEMENT DU TABLE DE PRE-STOKAGE DES ANIMATEURS
                    if (!$sessionExist) {
                        if (!empty($v[1]) && !empty($v[3])) {
                            $arrSessions[$radicalTest.$v[26]]['animator'] = [
                                'numRow' => $numRow,
                                'email' => $radicalTest.$v[3],
                                'forname' => $radicalTest.$v[2],
                                'name' => $radicalTest.$v[1],
                            ];
                        } else {
                            if (substr($v[16].$radicalTest, 0, 3) == 'WEB') {
                                $nameF = (empty($v[1])) ? 'Berthellemy' : $v[1];
                                $emailF = (empty($v[3])) ? 'yannbarthellemy@gmail.com' : $v[3];
                                $arrSessions[$radicalTest.$v[26]]['animator'] = [
                                    'numRow' => $numRow,
                                    'email' => (empty($v[3])) ? 'yannbarthellemy@gmail.com' : $v[3],
                                    'forname' => (empty($v[2])) ? 'Yann' : $v[2],
                                    'name' => (empty($v[1])) ? 'Barthellemy' : $v[1],
                                ];
                            }
                        }
                    } else {
                        //Si la session existe on modifie par les dernières valeurs
                        if (!empty($v[1]) && !empty($v[3])) {
                            $arrSessions[$keySession]['animator'] = [
                                'numRow' => $numRow,
                                'email' => $radicalTest.$v[3],
                                'forname' => $radicalTest.$v[2],
                                'name' => $radicalTest.$v[1],
                            ];
                        } else {
                            if (substr($v[16].$radicalTest, 0, 3) == 'WEB') {
                                $nameF = (empty($v[1])) ? 'Barthellemy' : $v[1];
                                $emailF = (empty($v[3])) ? 'yannbarthellemy@gmail.com' : $v[3];
                                $arrSessions[$radicalTest.$v[26]]['animator'] = [
                                    'numRow' => $numRow,
                                    'email' => (empty($v[3])) ? 'yannbarthellemy@gmail.com' : $v[3],
                                    'forname' => (empty($v[2])) ? 'Yann' : $v[2],
                                    'name' => (empty($v[1])) ? 'Barthellemy' : $v[1],
                                ];
                            }
                        }
                    }
                } else {
                    $errorSessions[$radicalTest.$v[26]] = [
                        'numRow' => $numRow,
                        'name' => $v[16].$radicalTest,  //P
                        'referenceRLT' => $radicalTest.$v[26],  //S
                        'statusFormation' => $v[29],  //AC
                        'rowIPN' => $v[7],   //G
                        'rowEmployeeID' => $v[31],  //AE
                        'rowEmailTrainee' => $v[8],    //H
                    ];
                    $errorSessions[$radicalTest.$v[26]]['errors'] = $errors[$radicalTest.$v[26]];
                }
                /*} else {
                    $errors[$radicalTest.$v[26]]['noCorrectTitle'] = [
                        'message' => 'The date of session is not in the range dates or the formation title not begin by "CVT" or "CVC"',
                    ];
                    $errorSessions[$radicalTest.$v[26]] = [
                        'numRow' => $numRow,
                        'name' => $radicalTest.$v[16],  //P
                        'referenceRLT' => $radicalTest.$v[26],  //S
                        'statusFormation' => $v[29],  //AC
                        'rowIPN' => $v[7],   //G
                        'rowEmployeeID' => $v[31],  //AE
                        'rowEmailTrainee' => $v[8],    //H
                    ];
                    $errorSessions[$radicalTest.$v[26]]['errors'] = $errors[$radicalTest.$v[26]];*/
            }
        }

        $resultPrepareForPersist = [
            'arrSessions' => $arrSessions,
            'errorSessions' => $errorSessions,
        ];

        return $resultPrepareForPersist;
    }

    public function addSessionsTemp(array $arrSessions = []): void
    {
        try {
            $iSess = 0;

//            echo 'Lignes : ';
            foreach ($arrSessions as $ses) {
                ++$iSess;
//                echo $iSess.'-';
//                echo $ses['statusFormation'];
                if ($ses['nbrDays'] == 1) {
                    //Ajoute l'animateur unique
                    $newAnimatorLine = new RenaultImportSessionTemp();
                    echo $ses['referenceRLT'].'-';
                    $newAnimatorLine
                        ->setNumRow(rand(1000000000, 9999999999))
                        ->setName($ses['name'])
                        ->setModel($ses['model'])
                        ->setReferenceRenault($ses['referenceRLT'])
                        ->setResultType($ses['resultType'])
                        ->setDateStart($ses['dateStart'])
                        ->setDateEnd($ses['dateEnd'])
                        ->setNbrDays($ses['nbrDays'])
                        ->setNumDay($ses['numDay'])
                        ->setCreatedAt(new \DateTimeImmutable('now'))
                        ->setImportedAt(new \DateTimeImmutable('now'))
                        ->setRole(RenaultImportSessionTemp::ROLE_ANIMATOR)
                        ->setAnimatorName((!empty($ses['animator'])) ? $ses['animator']['name'] : null)
                        ->setAnimatorForname((!empty($ses['animator'])) ? $ses['animator']['forname'] : null)
                        ->setAnimatorEmail((!empty($ses['animator'])) ? $ses['animator']['email'] : null)
                        ->setStatusSession($ses['statusFormation']);
                    $this->entityManager->persist($newAnimatorLine);

                    //Ajoute les participants
                    foreach ($ses['trainees'] as $k => $v) {
                        $newTraineeLine = new RenaultImportSessionTemp();
                        $newTraineeLine
                            ->setNumRow(rand(1000000000, 9999999999))
                            ->setName($ses['name'])
                            ->setModel($ses['model'])
                            ->setReferenceRenault($ses['referenceRLT'])
                            ->setResultType($ses['resultType'])
                            ->setDateStart($ses['dateStart'])
                            ->setDateEnd($ses['dateEnd'])
                            ->setNbrDays($ses['nbrDays'])
                            ->setNumDay($ses['numDay'])
                            ->setCreatedAt(new \DateTimeImmutable('now'))
                            ->setImportedAt(new \DateTimeImmutable('now'))
                            ->setRole(RenaultImportSessionTemp::ROLE_TRAINEE)
                            ->setTraineeName($v['name'])
                            ->setTraineeForname($v['forname'])
                            ->setTraineeEmail($v['email'])
                            ->setStatusSession($ses['statusFormation'])
                            ->setStatusTrainee($v['statusTrainee']);
                        if (!empty($v['IPN'])) {
                            $newTraineeLine->setTraineeIPN($v['IPN']);
                        }
                        if (!empty($v['employeeID'])) {
                            $newTraineeLine->setEmployeeID($v['employeeID']);
                        }
                        $this->entityManager->persist($newTraineeLine);
                    }
                } else { //Sessions sur plusieurs jours
                    $dateStartTemp = $ses['dateStart'];
                    $dateEndTemp = $ses['dateStart'];
                    for ($i = 0; $i <= $ses['nbrDays'] - 1; ++$i) {
                        $cloneDateStart = clone $dateStartTemp;
                        $cloneDateEnd = clone $dateEndTemp;
                        $ses['dateStart'] = $cloneDateStart->modify('+'.$i.' days');
                        $ses['dateEnd'] = $cloneDateEnd->modify('+'.$i.' days')->setTime($ses['dateEnd']->format('H'), $ses['dateEnd']->format('i'));

                        //Ajoute l'animateur unique
                        $newAnimatorLine = new RenaultImportSessionTemp();
                        $newAnimatorLine
                            ->setNumRow(rand(1000000000, 9999999999))
                            ->setName($ses['name'])
                            ->setModel($ses['model'])
                            ->setReferenceRenault($ses['referenceRLT'])
                            ->setResultType($ses['resultType'])
                            ->setDateStart($ses['dateStart'])
                            ->setDateEnd($ses['dateEnd'])
                            ->setNbrDays($ses['nbrDays'])
                            ->setNumDay($ses['numDay'] + $i)
                            ->setCreatedAt(new \DateTimeImmutable('now'))
                            ->setImportedAt(new \DateTimeImmutable('now'))
                            ->setRole(RenaultImportSessionTemp::ROLE_ANIMATOR)
                            ->setAnimatorName((!empty($ses['animator'])) ? $ses['animator']['name'] : null)
                            ->setAnimatorForname((!empty($ses['animator'])) ? $ses['animator']['forname'] : null)
                            ->setAnimatorEmail((!empty($ses['animator'])) ? $ses['animator']['email'] : null)
                            ->setStatusSession($ses['statusFormation']);
                        $this->entityManager->persist($newAnimatorLine);

                        //Ajoute les participants
                        foreach ($ses['trainees'] as $k => $v) {
                            $newTraineeLine = new RenaultImportSessionTemp();
                            $newTraineeLine
                                ->setNumRow(rand(1000000000, 9999999999))
                                ->setName($ses['name'])
                                ->setModel($ses['model'])
                                ->setReferenceRenault($ses['referenceRLT'])
                                ->setResultType($ses['resultType'])
                                ->setDateStart($ses['dateStart'])
                                ->setDateEnd($ses['dateEnd'])
                                ->setNbrDays($ses['nbrDays'])
                                ->setNumDay($ses['numDay'] + $i)
                                ->setCreatedAt(new \DateTimeImmutable('now'))
                                ->setImportedAt(new \DateTimeImmutable('now'))
                                ->setRole(RenaultImportSessionTemp::ROLE_TRAINEE)
                                ->setTraineeName($v['name'])
                                ->setTraineeForname($v['forname'])
                                ->setTraineeEmail($v['email'])
                                ->setStatusSession($ses['statusFormation'])
                                ->setStatusTrainee($v['statusTrainee']);
                            if (!empty($v['IPN'])) {
                                $newTraineeLine->setTraineeIPN($v['IPN']);
                            }
                            if (!empty($v['employeeID'])) {
                                $newTraineeLine->setEmployeeID($v['employeeID']);
                            }
                            $this->entityManager->persist($newTraineeLine);
                        }
                    }
                }
            }

            $this->entityManager->flush();
        } catch (\Exception $e) {
//            echo '----------ERROR-----------------------------';
        }
    }

    public function addErrorsSessionsTemp(array $errorSessions = []): void
    {
        try {
            $iErr = 0;
            foreach ($errorSessions as $session) {
                ++$iErr;
                $newErrorLine = new RenaultImportSessionTempErrors();
                $newErrorLine
                    ->setNumRow(rand(1000000000, 9999999999))
                    ->setName($session['name'])
                    ->setReferenceRenault($session['referenceRLT'])
                    ->setStatusFormation($session['statusFormation'])
                    ->setErrors($session['errors'])
                    ->setCreatedAt(new \DateTimeImmutable('now'));
                if (!empty($session['rowEmailTrainee'])) {
                    $newErrorLine->setRowEmailTrainee($session['rowEmailTrainee']);
                }
                if (!empty($session['rowIPN'])) {
                    $newErrorLine->setRowIPN($session['rowIPN']);
                }
                if (!empty($session['rowEmployeeID'])) {
                    $newErrorLine->setRowEmployeeID($session['rowEmployeeID']);
                }
                $this->entityManager->persist($newErrorLine);
            }

            $this->entityManager->flush();
        } catch (\Exception $e) {
        }
    }

    protected function betweenDates($cmpDate, $startDate, $endDate): bool
    {
        return ($cmpDate > $startDate) && ($cmpDate < $endDate);
    }

    protected function diffMinDate(\DateTime $date1 = null, \DateTime $date2 = null): ?int
    {
        try {
            if ($date1 > $date2) {
                return null;
            }

            $since_start = $date1->diff($date2);
            $minutes = $since_start->days * 24 * 60;
            $minutes += $since_start->h * 60;
            $minutes += $since_start->i;

            return $minutes;
        } catch (\Exception $e) {
            return null;
        }
    }

    protected function enleveaccents($chaine, $charset = 'utf-8'): string
    {
        /*$string= strtr($chaine,
            ["ÀÁÂàÄÅàáâàäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ" => "aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn"]
        );*/

        $string = htmlentities($chaine, ENT_NOQUOTES, $charset);

        $string = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $string);
        $string = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $string); // pour les ligatures e.g. '&oelig;'
        $string = preg_replace('#&[^;]+;#', '', $string); // supprime les autres caractères

        return $string;
    }
}
