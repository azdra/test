<?php

namespace App\RenaultRobot\Service;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\Client\Client;
use App\Entity\ProviderParticipantSessionRole;
use App\RenaultRobot\Repository\RenaultImportSessionTempRepository;
use App\Repository\ClientRepository;
use App\Repository\ProviderSessionRepository;
use App\Service\ProviderSessionService;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RenaultReportPresencesSessionsService
{
    public function __construct(
        protected string $renaultSavefilesDirectory,
        protected EntityManagerInterface $entityManager,
        protected RenaultImportSessionTempRepository $renaultImportSessionTempRepository,
        protected ClientRepository $clientRepository,
        protected ProviderSessionRepository $providerSessionRepository,
        protected LoggerInterface $logger,
        protected TranslatorInterface $translator,
        protected RouterInterface $router,
        private ProviderSessionService $providerSessionService,
        protected string $endPointDomain,
    ) {
    }

    public function reportPresencesSessions(string $dateStartPerso, string $dateEndPerso): array
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Renault']);

        // - 9 jours
        $from = ( new \DateTime() )
            ->modify('-96 hours')
            ->setTime(0, 0);
        // - 2 jours
        $to = ( new \DateTime() )
            ->modify('-48 hours')
            ->setTime(23, 59);

        if (!empty($dateStartPerso)) {
            $from = ( new \DateTime() )
                ->modify($dateStartPerso)
                ->setTime(0, 0);
        }

        if (!empty($dateEndPerso)) {
            $to = ( new \DateTime() )
                ->modify($dateEndPerso)
                ->setTime(23, 59);
        }

        //Test
        //$from = ( new \DateTime() )->modify('-48 hours')->setTime(0, 0);
        //$to = ( new \DateTime() )->modify('+10 days')->setTime(23, 59);

        $presencesSessions = $this->providerSessionRepository->findClientSessionForPeriod($client, $from, $to);

        $oldMeetingId = 0;
        $arrPresence = [];
        foreach ($presencesSessions as $m) {
            if ($oldMeetingId != $m->getId()) {
                $animAbsent = true;
                $allAbsent = true;
                $arrPresence[$m->getId()] = [
                    'animAbsent' => $animAbsent,
                    'allAbsent' => $allAbsent,
                ];
                $oldMeetingId == $m->getId();
            }
            // 25 minutes pour les webinars, 90 minutes pour les classes virtuelles
            $miniTimePresent = ($m instanceof AdobeConnectSCO) ? 90 : 25;
            foreach ($m->getAnimatorsOnly() as $participantRole) {
                if ($participantRole->getPresenceStatus() && $participantRole->getSlotPresenceDuration() > $miniTimePresent) {
                    $animAbsent = false;
                    $allAbsent = false;
                    $this->logger->info(sprintf('%s', '### RLT - RP Animateur '.$participantRole->getPresenceStatus().' 1 - '.$m->getRef().' ('.$m->getId().') >>> '.$participantRole->getParticipant()->getUser()->getEmail().' ###'));
                } else {
                    $this->logger->info(sprintf('%s', '### RLT - RP Animateur '.$participantRole->getPresenceStatus().' 2 - '.$m->getRef().' ('.$m->getId().') >>> '.$participantRole->getParticipant()->getUser()->getEmail().' ###'));
                }
            }
            foreach ($m->getTraineesOnly() as $participantRole) {
                if ($participantRole->getPresenceStatus() && $participantRole->getSlotPresenceDuration() > $miniTimePresent) {
                    $this->logger->info(sprintf('%s', '### RLT - RP Participant '.$participantRole->getPresenceStatus().' 1 - '.$m->getRef().' ('.$m->getId().') >>> '.$participantRole->getParticipant()->getUser()->getEmail().' ###'));
                } else {
                    $this->logger->info(sprintf('%s', '### RLT - RP Participant '.$participantRole->getPresenceStatus().' 2 - '.$m->getRef().' ('.$m->getId().') >>> '.$participantRole->getParticipant()->getUser()->getEmail().' ###'));
                }
            }
            $arrPresence[$m->getId()] = [
                'animAbsent' => $animAbsent,
                'allAbsent' => $allAbsent,
            ];
        }

        foreach ($arrPresence as $key => $check) {
            if ($check['animAbsent'] == true || $check['allAbsent'] == true) {
                $this->logger->info(sprintf('%s', '### RLT - Non retenue des présences pour la session '.$key.' ### : End !'));
            } else {
                $this->logger->info(sprintf('%s', '### RLT - Retenue des présences pour la session '.$key.' ### : End !'));
            }
        }

        $result = $this->generateExportResultPresences($presencesSessions, 'xls', false);

        //export_access_ddmmyyyyhhiiss.xlsx
        $dday = new \DateTime();
        $key = 'export_presences_'.$dday->format('dmYhis');

        if ($result) {
            (new Xlsx($result))->save($this->renaultSavefilesDirectory.'/'.$key.'.xlsx');
        }

        $response = [
            'link' => $this->renaultSavefilesDirectory.'/'.$key.'.xlsx', // __DIR__ . self::TMP_DIR . $key . '.xlsx',
            'name' => $key.'.xlsx',
        ];

        return $response;
    }

    public function generateExportResultPresences($linesImported, $format = 'xls'): ?Spreadsheet
    {
        switch ($format) {
            case 'xls':
                $row = 0;
                $excel = $this->createExcelRenaultPresences($row);
                foreach ($linesImported as $line) {
                    if ($line instanceof AdobeConnectWebinarSCO) {
                        foreach ($line->getAnimatorsOnly() as $participantRole) {
                            $excel = $this->generateRowLinePresencesWebinar($excel, $row, $line, $participantRole);
                        }
                        foreach ($line->getTraineesOnly() as $participantRole) {
                            $excel = $this->generateRowLinePresencesWebinar($excel, $row, $line, $participantRole);
                        }
                    } else {
                        foreach ($line->getAnimatorsOnly() as $participantRole) {
                            $excel = $this->generateRowLinePresences($excel, $row, $line, $participantRole);
                        }
                        foreach ($line->getTraineesOnly() as $participantRole) {
                            $excel = $this->generateRowLinePresences($excel, $row, $line, $participantRole);
                        }
                    }

                    /*foreach ($line->getAnimatorsOnly() as $participantRole) {
                        $excel = $this->generateRowLinePresences($excel, $row, $line, $participantRole);
                    }
                    foreach ($line->getTraineesOnly() as $participantRole) {
                        $excel = $this->generateRowLinePresences($excel, $row, $line, $participantRole);
                    }*/
                }

                return $excel;
                break;
        }

        return null;
    }

    private function createExcelRenaultPresences(&$row = 0): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('Mylivesession Manager')
            ->setLastModifiedBy(('Mylivesession Manager'))
            ->setTitle('Rapport de présences Renault');
        $spreadsheet->setActiveSheetIndex(0);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getDefaultRowDimension(1)->setRowHeight(20);

        ++$row;
        $spreadsheet->getActiveSheet()
            ->setCellValue('A'.$row, 'Nom de la session')
            ->setCellValue('B'.$row, 'Référence de la session')   //(= référence Live Session)
            ->setCellValue('C'.$row, 'Numéro de la formation')    //(= référence Renault)
            ->setCellValue('D'.$row, 'Email')
            ->setCellValue('E'.$row, 'Prénom')
            ->setCellValue('F'.$row, 'Nom')
            ->setCellValue('G'.$row, 'Compte')
            ->setCellValue('H'.$row, 'Actif')                     //(Statut interne du participant)
            ->setCellValue('I'.$row, 'Champ personnalisé 1')      //(= IPN utilisateur)
            ->setCellValue('J'.$row, 'Champ personnalisé 2')      //(Restara vide sauf si besoin)
            ->setCellValue('K'.$row, 'Rôle')                      //(Animateur/Participant)
            ->setCellValue('L'.$row, 'Statut')                    //(Présent/Absent)
            ->setCellValue('M'.$row, 'Date')                      //(dd/mm/yyyy)
            ->setCellValue('N'.$row, 'Heure')                     //(HH:ii:ss) (= heure de démarrage de la session)
            ->setCellValue('O'.$row, 'Début')                     //(HH:ii:ss) (= heure du début de connexion de l'utilisateur)
            ->setCellValue('P'.$row, 'Fin')                       //(HH:ii:ss) (= heure du fin de connexion de l'utilisateur)
            ->setCellValue('Q'.$row, 'Durée');             //(HH:ii:ss) (= total cumulé du temps de présence)
            //->setCellValue('R'.$row, 'Durée réel');                     //(HH:ii:ss) (= total cumulé du temps de présence)
        //->setCellValue('O' . $row, 'URL session');
        ++$row;

        return $spreadsheet;
    }

    protected function generateRowLinePresences(Spreadsheet $excel, &$row, AdobeConnectSCO $line, ProviderParticipantSessionRole $ppsr): Spreadsheet
    {
        //if ($excel instanceof Spreadsheet){
        /*
        1. Nom de la session
        2. Référence de la session (= référence Live Session)
        3. Numéro de la formation (= référence Renault)
        4. Email
        5. Prénom
        6. Nom
        7. Compte (Compte interne au système : ici Renault)
        8. Actif ? (Statut interne du participant)
        9. Champ personnalisé 1 (= IPN utilisateur)
        10. Champ personnalisé 2 (= Employee_ID)
        11. Rôle (Animateur/Participant)
        12. Statut (Present/Absent)
        13. Date (dd/mm/yyyy)
        14. Heure (HH:ii:ss) (= heure de démarrage de la session)
        15. Début (HH:ii:ss) (= heure de début de connexion de l’utilisateur)
        16. Fin (HH:ii:ss) (=heure de fin de connexion de l’utilisateur)
        17. Durée (HH:ii:ss) (= total cumulé temps de présence)
        */

        $ipnUser = null;
        $employeeIDUser = null;

        $emailUser = $ppsr->getEmail();
        $nameUser = $ppsr->getLastName();
        $fornameUser = $ppsr->getFirstName();
        if ($ppsr->getRole() == ProviderParticipantSessionRole::ROLE_ANIMATOR) {   //Animateur
            $roleUser = 'Animateur';
        } else {  //Participant
            $roleUser = 'Participant';
            $ipnUser = $ppsr->getParticipant()->getUser()->getReference();
            $employeeIDUser = $ppsr->getParticipant()->getUser()->getReferenceClient();
        }

        $login_url = $this->endPointDomain.$this->router->generate('_session_access_plateforme', ['token' => $ppsr->getProviderSessionAccessToken()->getToken()]);

        $lineDateStart = (clone $line->getDateStart())->setTimezone(new \DateTimeZone('Europe/Paris'));
        $lineDateEnd = (clone $line->getDateEnd())->setTimezone(new \DateTimeZone('Europe/Paris'));

        $statutPresence = ($ppsr->getSlotPresenceDuration() >= $this->providerSessionService->getDurationMinimumForPresence($ppsr->getSession())) ? 'Present' : 'Absent';
        /*if ($statutPresence == 'Present') {
            $startPresence = (clone $ppsr->getPresenceDateStart())->setTimezone(new \DateTimeZone('Europe/Paris'));
            $endPresence = (clone $ppsr->getPresenceDateEnd())->setTimezone(new \DateTimeZone('Europe/Paris'));
            $interval = $endPresence->diff($startPresence);
            $duringPresence = $interval->format('%H:%I:%S');
            $startPresence = $startPresence->format('H:i:s');
            $endPresence = $endPresence->format('H:i:s');
        } else {
            $startPresence = '';
            $endPresence = '';
            $duringPresence = '';
        }*/

        if ($ppsr->getPresenceDateStart() !== null) {
            $startPresence = (clone $ppsr->getPresenceDateStart())->setTimezone(new \DateTimeZone('Europe/Paris'));
            $endPresence = (clone $ppsr->getPresenceDateEnd())->setTimezone(new \DateTimeZone('Europe/Paris'));
            $interval = $endPresence->diff($startPresence);
            $duringPresence = $interval->format('%H:%I:%S');
            $startPresence = $startPresence->format('H:i:s');
            $endPresence = $endPresence->format('H:i:s');
        } else {
            $startPresence = '';
            $endPresence = '';
            $duringPresence = '';
        }

        $excel->getActiveSheet()
            ->setCellValue('A'.$row, $line->getName())
            ->setCellValueExplicit('B'.$row, (!empty($line->getRef())) ? $line->getRef() : 'En attente de référence (session MLSM pas encore créée)', \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
            ->setCellValueExplicit('C'.$row, $line->getDescription(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)

            ->setCellValue('D'.$row, $emailUser)
            ->setCellValue('E'.$row, $fornameUser)
            ->setCellValue('F'.$row, $nameUser)
            ->setCellValue('G'.$row, 'Renault')
            ->setCellValue('H'.$row, 'yes')
            ->setCellValueExplicit('I'.$row, $ipnUser, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
            ->setCellValueExplicit('J'.$row, $employeeIDUser, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
            ->setCellValue('K'.$row, $roleUser)
            ->setCellValue('L'.$row, $statutPresence)
            ->setCellValueExplicit('M'.$row, $lineDateStart->format('Y-m-d'), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
            ->setCellValue('N'.$row, $lineDateStart->format('H:i:s'))
            ->setCellValue('O'.$row, $startPresence)
            ->setCellValue('P'.$row, $endPresence)
            //->setCellValue('Q'.$row, $duringPresence)
            ->setCellValue('Q'.$row, $this->formatDuration($ppsr->getSlotPresenceDuration()));
        //->setCellValue('R'.$row, $this->formatDuration($ppsr->getSlotPresenceDuration()));
        ++$row;

        //}
        return $excel;
    }

    protected function generateRowLinePresencesWebinar(Spreadsheet $excel, &$row, AdobeConnectWebinarSCO $line, ProviderParticipantSessionRole $ppsr): Spreadsheet
    {
        //if ($excel instanceof Spreadsheet){
        /*
        1. Nom de la session
        2. Référence de la session (= référence Live Session)
        3. Numéro de la formation (= référence Renault)
        4. Email
        5. Prénom
        6. Nom
        7. Compte (Compte interne au système : ici Renault)
        8. Actif ? (Statut interne du participant)
        9. Champ personnalisé 1 (= IPN utilisateur)
        10. Champ personnalisé 2 (= Employee_ID)
        11. Rôle (Animateur/Participant)
        12. Statut (Present/Absent)
        13. Date (dd/mm/yyyy)
        14. Heure (HH:ii:ss) (= heure de démarrage de la session)
        15. Début (HH:ii:ss) (= heure de début de connexion de l’utilisateur)
        16. Fin (HH:ii:ss) (=heure de fin de connexion de l’utilisateur)
        17. Durée (HH:ii:ss) (= total cumulé temps de présence)
        */

        $ipnUser = null;
        $employeeIDUser = null;

        $emailUser = $ppsr->getEmail();
        $nameUser = $ppsr->getLastName();
        $fornameUser = $ppsr->getFirstName();
        if ($ppsr->getRole() == ProviderParticipantSessionRole::ROLE_ANIMATOR) {   //Animateur
            $roleUser = 'Animateur';
        } else {  //Participant
            $roleUser = 'Participant';
            $ipnUser = $ppsr->getParticipant()->getUser()->getReference();
            $employeeIDUser = $ppsr->getParticipant()->getUser()->getReferenceClient();
        }

        $login_url = $this->endPointDomain.$this->router->generate('_session_access_plateforme', ['token' => $ppsr->getProviderSessionAccessToken()->getToken()]);

        $lineDateStart = (clone $line->getDateStart())->setTimezone(new \DateTimeZone('Europe/Paris'));
        $lineDateEnd = (clone $line->getDateEnd())->setTimezone(new \DateTimeZone('Europe/Paris'));

        $statutPresence = ($ppsr->getSlotPresenceDuration() >= $this->providerSessionService->getDurationMinimumForPresence($ppsr->getSession())) ? 'Present' : 'Absent';
        /*if ($statutPresence == 'Present') {
            $startPresence = (clone $ppsr->getPresenceDateStart())->setTimezone(new \DateTimeZone('Europe/Paris'));
            $endPresence = (clone $ppsr->getPresenceDateEnd())->setTimezone(new \DateTimeZone('Europe/Paris'));
            $interval = $endPresence->diff($startPresence);
            $duringPresence = $interval->format('%H:%I:%S');
            $startPresence = $startPresence->format('H:i:s');
            $endPresence = $endPresence->format('H:i:s');
        } else {
            $startPresence = '';
            $endPresence = '';
            $duringPresence = '';
        }*/
        if ($ppsr->getPresenceDateStart() !== null) {
            $startPresence = (clone $ppsr->getPresenceDateStart())->setTimezone(new \DateTimeZone('Europe/Paris'));
            $endPresence = (clone $ppsr->getPresenceDateEnd())->setTimezone(new \DateTimeZone('Europe/Paris'));
            $interval = $endPresence->diff($startPresence);
            $duringPresence = $interval->format('%H:%I:%S');
            $startPresence = $startPresence->format('H:i:s');
            $endPresence = $endPresence->format('H:i:s');
        } else {
            $startPresence = '';
            $endPresence = '';
            $duringPresence = '';
        }

        $excel->getActiveSheet()
            ->setCellValue('A'.$row, $line->getName())
            ->setCellValueExplicit('B'.$row, (!empty($line->getRef())) ? $line->getRef() : 'En attente de référence (session MLSM pas encore créée)', \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
            ->setCellValueExplicit('C'.$row, $line->getDescription(), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)

            ->setCellValue('D'.$row, $emailUser)
            ->setCellValue('E'.$row, $fornameUser)
            ->setCellValue('F'.$row, $nameUser)
            ->setCellValue('G'.$row, 'Renault')
            ->setCellValue('H'.$row, 'yes')
            ->setCellValueExplicit('I'.$row, $ipnUser, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
            ->setCellValueExplicit('J'.$row, $employeeIDUser, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
            ->setCellValue('K'.$row, $roleUser)
            ->setCellValue('L'.$row, $statutPresence)
            ->setCellValueExplicit('M'.$row, $lineDateStart->format('Y-m-d'), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING)
            ->setCellValue('N'.$row, $lineDateStart->format('H:i:s'))
            ->setCellValue('O'.$row, $startPresence)
            ->setCellValue('P'.$row, $endPresence)
            //->setCellValue('Q'.$row, $duringPresence)
            ->setCellValue('Q'.$row, $this->formatDuration($ppsr->getSlotPresenceDuration()));
        ++$row;

        //}
        return $excel;
    }

    public function formatDuration(?int $minutes): string
    {
        if ($minutes === null) {
            return '00:00:00';
        }

        $seconds = $minutes * 60;
        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds / 60) % 60);
        $seconds = $seconds % 60;

        return sprintf('%02d:%02d:%02d', $hours, $minutes, $seconds);
    }
}
