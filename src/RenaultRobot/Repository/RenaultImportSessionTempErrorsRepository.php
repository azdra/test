<?php

namespace App\RenaultRobot\Repository;

use App\RenaultRobot\Entity\RenaultImportSessionTempErrors;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class RenaultImportSessionTempErrorsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RenaultImportSessionTempErrors::class);
    }

    public function removeDataFromDate(): bool
    {
        $oldLinesDate = ( new \DateTimeImmutable('now') )->modify('-7 days')->setTime(0, 0)->format('Y-m-d H:i:s');

        $qb = $this->createQueryBuilder('line')
            ->delete('line.created <= :oldLinesDate')
            ->setParameter('oldLinesDate', $oldLinesDate);

        return true;
    }
}
