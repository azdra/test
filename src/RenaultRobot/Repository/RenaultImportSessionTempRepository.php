<?php

namespace App\RenaultRobot\Repository;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\RenaultRobot\Entity\RenaultImportSessionTemp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class RenaultImportSessionTempRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RenaultImportSessionTemp::class);
    }

    public function findAllByResultType($resultType, $maxResult = null): array
    {
        $ddayStart = ( new \DateTimeImmutable('now') )->setTime(0, 0);
        $ddayEnd = ( new \DateTimeImmutable('now') )->setTime(23, 59);

        $qb = $this->createQueryBuilder('line')
            ->where('line.resultType = :resultType')
            ->andWhere('line.createdAt >= :start')
            ->andWhere('line.createdAt <= :end')
            ->setParameter('resultType', $resultType)
            ->setParameter('start', $ddayStart)
            ->setParameter('end', $ddayEnd)
            ->addOrderBy('line.id', 'ASC');

        if (is_numeric($maxResult)) {
            $qb->setMaxResults($maxResult);
        }

        return $qb->getQuery()->getResult();
    }

    public function findAllByResultUser($resultType, $maxResult = null): array
    {
        $ddayStart = (new \DateTimeImmutable('now'))->setTime(0, 0);
        $ddayEnd = (new \DateTimeImmutable('now'))->setTime(23, 59);

        $qb = $this->createQueryBuilder('line')
            ->where('line.resultType > :resultType')
            ->andWhere('line.resultType <>'.RenaultImportSessionTemp::ERROR)
            ->andWhere('line.createdAt >= :start')
            ->andWhere('line.createdAt <= :end')
            ->andWhere('line.resultTrainee is null')
            ->andWhere('line.resultAnimator is null')
            ->setParameter('resultType', $resultType)
            ->setParameter('start', $ddayStart)
            ->setParameter('end', $ddayEnd)
            ->addOrderBy('line.id', 'ASC');

        if (is_numeric($maxResult)) {
            $qb->setMaxResults($maxResult);
        }

        return $qb->getQuery()->getResult();
    }

    public function getImportDDay(): array
    {
        $ddayStart = ( new \DateTimeImmutable() )->setTime(0, 0);
        $ddayEnd = ( new \DateTimeImmutable() )->setTime(23, 59);
        //Pour les tests
        //$ddayStart = ( new \DateTimeImmutable() )->modify('-1 day')->setTime(0, 0);
        //$ddayEnd = ( new \DateTimeImmutable() )->modify('-1 day')->setTime(23,59);

        $qb = $this->createQueryBuilder('line')
            ->where('line.createdAt >= :start')
            ->andWhere('line.createdAt <= :end')
            ->setParameter('start', $ddayStart)
            ->setParameter('end', $ddayEnd);

        return $qb->getQuery()->getResult(); //->getArrayResult();
    }

    public function removeDataFromDate(): bool
    {
        $oldLinesDate = ( new \DateTimeImmutable('now') )->modify('-7 days')->setTime(0, 0)->format('Y-m-d H:i:s');

        $qb = $this->createQueryBuilder('line')
            ->delete('line.created <= :oldLinesDate')
            ->setParameter('oldLinesDate', $oldLinesDate);

        return true;
    }

    public function postCreateInfos(RenaultImportSessionTemp $row, AdobeConnectSCO $meeting): void
    {
        $this->createQueryBuilder('line')
            ->update()
            ->set('line.referenceLiveSession', ':referenceLiveSession')
            ->set('line.urlSession', ':urlSession')
            ->where('line.referenceRenault = :referenceRenault')
            ->andWhere('line.numDay = :numDay')
            ->setParameter('referenceLiveSession', $meeting->getRef())
            ->setParameter('urlSession', $meeting->getUrlPath())
            ->setParameter('referenceRenault', $row->getReferenceRenault())
            ->setParameter('numDay', $row->getNumDay())
            ->getQuery()
            ->execute();
    }

    public function postCreateWebinarInfos(RenaultImportSessionTemp $row, AdobeConnectWebinarSCO $meeting): void
    {
        $this->createQueryBuilder('line')
            ->update()
            ->set('line.referenceLiveSession', ':referenceLiveSession')
            ->set('line.urlSession', ':urlSession')
            ->where('line.referenceRenault = :referenceRenault')
            ->andWhere('line.numDay = :numDay')
            ->setParameter('referenceLiveSession', $meeting->getRef())
            ->setParameter('urlSession', $meeting->getUrlPath())
            ->setParameter('referenceRenault', $row->getReferenceRenault())
            ->setParameter('numDay', $row->getNumDay())
            ->getQuery()
            ->execute();
    }

    public function postCreateRow(RenaultImportSessionTemp $row): void
    {
        $this->createQueryBuilder('line')
            ->update()
            ->set('line.importedAt', ':dateImported')
            ->where('line.id = :idRow')
            ->setParameter('dateImported', ( new \DateTimeImmutable('now') )->format('Y-m-d H:i:s'))
            ->setParameter('idRow', $row->getId())
            ->getQuery()
            ->execute();
    }
}
