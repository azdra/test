<?php

namespace App\RenaultRobot\Command;

use App\RenaultRobot\Service\RenaultReportPresencesSessionsService;
use League\Flysystem\Filesystem;
use League\Flysystem\PhpseclibV3\SftpAdapter;
use League\Flysystem\PhpseclibV3\SftpConnectionProvider;
use League\Flysystem\UnixVisibility\PortableVisibilityConverter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class RenaultReportPresencesSessionsCommand extends Command implements ContainerAwareInterface
{
    protected static $defaultName = 'renault:report-presences-sessions';

    private ?ContainerInterface $container;

    public function __construct(
        protected string $certificatsOrca,
        protected RenaultReportPresencesSessionsService $renaultReportPresencesSessionsService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        //bin/console renault:report-presences-sessions
        //bin/console renault:report-presences-sessions --dateStartPerso="-5 days"
        //bin/console renault:report-presences-sessions --dateStartPerso="-5 days" --dateEndPerso="-1 day"
        $this->addOption('dateStartPerso', '--dateStartPerso', InputOption::VALUE_OPTIONAL, 'Date start personnalized for export');
        $this->addOption('dateEndPerso', '--dateEndPerso', InputOption::VALUE_OPTIONAL, 'Date end personnalized for export');
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $output->writeln("\t> ------Début du traitement-------------");
        $dateStartPerso = (!empty($input->getOption('dateStartPerso'))) ? $input->getOption('dateStartPerso') : '';
        $dateEndPerso = (!empty($input->getOption('dateEndPerso'))) ? $input->getOption('dateEndPerso') : '';

        $response = $this->renaultReportPresencesSessionsService->reportPresencesSessions($dateStartPerso, $dateEndPerso);

        $output->writeln("\t> ------------------ Fin création fichier rapport présences | ".$response['link'].' -----------------------');
        //exit;
        $privateKey = $this->certificatsOrca;

        $filesystem = new Filesystem(new SftpAdapter(
            new SftpConnectionProvider(
                '149.202.66.53',
                'livesession',
                null,
                $privateKey,
                null,
                22,
                false,
                10,
                4,
                null,
                null
            ),
            '/CLA',
            PortableVisibilityConverter::fromArray([
                'file' => [
                    'public' => 0640,
                    'private' => 0604,
                ],
                'dir' => [
                    'public' => 0740,
                    'private' => 7604,
                ],
            ])
        ));

        $stream = fopen($response['link'], 'r+');
        if ($filesystem->writeStream($response['name'], $stream)) {
            $output->writeln("\t> -------------EXPORT CLA/".$response['name'].' OK-----------------------------');
        } else {
            $output->writeln("\t> -------------EXPORT CLA/".$response['name'].' NOK-----------------------------');
        }

        $output->writeln("\t> ------------------------------------------");

        $io->success('All done !');

        return Command::SUCCESS;
    }
}
