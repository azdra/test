<?php

namespace App\RenaultRobot\Command;

use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\RenaultRobot\Entity\RenaultImportSessionTemp;
use App\RenaultRobot\Repository\RenaultImportSessionTempRepository;
use App\RenaultRobot\Service\RenaultService;
use App\Repository\AdobeConnectWebinarPrincipalRepository;
use App\Repository\ProviderSessionRepository;
use App\Service\AdobeConnectWebinarService;
use App\Service\MailerService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouterInterface;

final class RenaultOpenSeminarWithLicenceCommand extends Command implements ContainerAwareInterface
{
    protected static $defaultName = 'renault:open-seminar-with-licence';

    private ?ContainerInterface $container;

    public function __construct(
        private RenaultImportSessionTempRepository $renaultImportSessionTempRepository,
        private ProviderSessionRepository $providerSessionRepository,
        private AdobeConnectWebinarPrincipalRepository $adobeConnectWebinarPrincipalRepository,
        private AdobeConnectWebinarService $adobeConnectWebinarService,
        private RenaultService $renaultService,
        private MailerService $mailerService,
        protected RouterInterface $router,
        protected string $endPointDomain,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $output->writeln("\t> ------Début du traitement-------------");
        $executionStartTime = microtime(true);

        $sessionsToOpen = $this->providerSessionRepository->findSessionsWebinarsRenaultToOpen();
        //dump($sessionsToOpen);
        foreach ($sessionsToOpen as $session) {
            /** @var AdobeConnectWebinarSCO $session */
            /*dump($session->getRoomModel());
            dump($session->getAbstractProviderConfigurationHolder()->getDomain().' | '.$session->getAbstractProviderConfigurationHolder()->getUsername().' | '.$session->getAbstractProviderConfigurationHolder()->getPassword());
            $getLinkAccessSeminarWithLicence = $this->adobeConnectWebinarService->redirectLicence($session);
            $linkAccessSeminarWithLicence = ( $getLinkAccessSeminarWithLicence->isSuccess() ) ? $getLinkAccessSeminarWithLicence->getData() : '';
            dump('LICENCE <'.$session->getAbstractProviderConfigurationHolder()->getUsername().'> | '.$linkAccessSeminarWithLicence);*/
            foreach ($session->getAnimatorsOnly() as $animator) {
                //$getLinkAccessSeminarAsAnimator = $this->adobeConnectWebinarService->redirectParticipantSessionRole($animator);
                //$linkAccessSeminarAsAnimator = ( $getLinkAccessSeminarAsAnimator->isSuccess() ) ? $getLinkAccessSeminarAsAnimator->getData() : '';
                //dump('ANIMATOR <'.$animator->getEmail().'> | '.$linkAccessSeminarAsAnimator);
                $login_url = $this->endPointDomain.$this->router->generate('_session_access_plateforme', ['token' => $animator->getProviderSessionAccessToken()->getToken()]);
                //dump($session->getName().' | URL de connexion de l\'animateur <'.$animator->getEmail().'> : '.$login_url);
                $test = $this->adobeConnectWebinarPrincipalRepository->getPrincipalWithConfigurationHolder($animator->getParticipant()->getPrincipalIdentifier(), $session->getAbstractProviderConfigurationHolder());
                //dump($test);
            }
        }

        /*        $allRLTRowToImport = $this->renaultImportSessionTempRepository->findAllByResultType(
                    RenaultImportSessionTemp::WAITING
                );

                if (count($allRLTRowToImport) > 0) {
                    $infos = [];
                    foreach ($allRLTRowToImport as $row) {
                        $infos[] = [
                            'numRow' => $row->getNumRow(),
                            'referenceRenault' => $row->getReferenceRenault(),
                            'dateStart' => $row->getDateStart()->format('d/m/Y H:i'),
                        ];
                    }
                    $this->mailerService->sendTemplatedMail(
                        ['ingenierie@live-session.fr', 'serviceclient@live-session.fr'],
                        'Lignes non traitées pour Renault',
                        'emails/alert/send_alert_not_treated_renault.html.twig',
                        context: [
                            'infos' => $infos,
                        ],
                        metadatas: [
                            'origin' => MailerService::ORIGIN_EMAIL_ALERT_NO_TREATED_RENAULT,
                        ]
                    );
                }

                $executionEndTime = microtime(true);
                $seconds = $executionEndTime - $executionStartTime;
                $output->writeln("\t> ------Durée après chargement-------------".round($seconds, 3).' secs---| '.count($allRLTRowToImport).' lignes  |---------------------');*/

        $io->success('All done !');

        return Command::SUCCESS;
    }
}
