<?php

namespace App\RenaultRobot\Command;

use League\Flysystem\Filesystem;
//use League\Flysystem\Sftp\SftpAdapter;
use League\Flysystem\PhpseclibV3\SftpAdapter;
use League\Flysystem\PhpseclibV3\SftpConnectionProvider;
use League\Flysystem\UnixVisibility\PortableVisibilityConverter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class RenaultImportRCampusFileSFTPCommand extends Command implements ContainerAwareInterface
{
    protected static $defaultName = 'renault:import-rcampus-file-sftp';

    private ?ContainerInterface $container;

    public function __construct(
        protected string $certificatsOrca,
        protected string $renaultSavefilesDirectory
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $output->writeln("\t> ------Début du traitement-------------");
        $executionStartTime = microtime(true);

        $privateKey = $this->certificatsOrca;

        $source = new Filesystem(new SftpAdapter(
            new SftpConnectionProvider(
                '149.202.66.53',
                'livesession',
                null,
                $privateKey,
                null,
                22,
                false,
                10,
                4,
                null,
                null
            ),
            '/CLA',
            PortableVisibilityConverter::fromArray([
                'file' => [
                    'public' => 0640,
                    'private' => 0604,
                ],
                'dir' => [
                    'public' => 0740,
                    'private' => 7604,
                ],
            ])
        ));

        $contents = $source->listContents('/'); // get file lists
        foreach ($contents as $object) {
            if ($object['path'] == 'RCAMPUS.xlsx') {
                $output->writeln("\t> -------------DEBUT IMPORT RCAMPUS.xls-----------------------------");
                $ddaynumber = ( new \DateTime() )->format('w');
                $file = $this->renaultSavefilesDirectory.'/RCAMPUS_'.$ddaynumber.'.xlsx';
                $stream = $source->readStream($object['path']);

                file_put_contents($file, $stream);

                $output->writeln("\t> -------------FIN IMPORT RCAMPUS.xls OK-----------------------------");
            }
        }

        $executionEndTime = microtime(true);
        $seconds = $executionEndTime - $executionStartTime;
        $output->writeln("\t> ------Durée de traitement complet-------------".round($seconds, 3).' secs pour le téléchargement du fichier dans media/rltshare ---------------------');
        $io->success('All done !');

        return Command::SUCCESS;
    }
}
