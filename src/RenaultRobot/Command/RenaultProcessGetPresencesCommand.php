<?php

namespace App\RenaultRobot\Command;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\Client\Client;
use App\Repository\ClientRepository;
use App\Repository\ProviderSessionRepository;
use App\Service\Provider\Adobe\ReportAdobeConnectService;
use App\Service\Provider\Adobe\ReportAdobeConnectWebinarService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class RenaultProcessGetPresencesCommand extends Command implements ContainerAwareInterface
{
    public const ARG_MEETING_ID = 'meetingID';
    public const OPT_DELTA_DAYS = 'deltaDays';

    private ?ContainerInterface $container;

    public function __construct(
        private ClientRepository $clientRepository,
        private ProviderSessionRepository $providerSessionRepository,
        private ReportAdobeConnectService $reportAdobeConnectService,
        private ReportAdobeConnectWebinarService $reportAdobeConnectWebinarService,
        private EntityManagerInterface $entityManager,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('renault:process-get-presences')
            ->setDescription('Récupère les présences depuis Adobe Connect pour Renault')
            ->addArgument(
                RenaultProcessGetPresencesCommand::ARG_MEETING_ID, InputArgument::OPTIONAL, 'Meeting ID'
            )
            ->addOption(self::OPT_DELTA_DAYS, 'd', InputOption::VALUE_OPTIONAL, '', 3)
        ;
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("\t> ------Début du traitement-------------");
        $executionStartTime = microtime(true);

        $deltaDays = intval($input->getOption(self::OPT_DELTA_DAYS));
        $start = ( new \DateTime(date('Y-m-d')) )->modify("-$deltaDays days")->setTime(0, 0);
        $end = ( new \DateTime(date('Y-m-d')) )->modify('-2 days')->setTime(23, 59);

        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => 'Renault']);

        if (!empty($input->getArgument(RenaultProcessGetPresencesCommand::ARG_MEETING_ID))) {
            $meetings = $this->providerSessionRepository->findBy(['client' => $client, 'id' => $input->getArgument(RenaultProcessGetPresencesCommand::ARG_MEETING_ID), 'sessionTest' => false]);
        } else {
            $meetings = $this->providerSessionRepository->findClientSessionForPeriod($client, $start, $end);
        }

        foreach ($meetings as $meeting) {
            if ($meeting instanceof AdobeConnectSCO) {
                /** @var AdobeConnectSCO $m */
                $m = $meeting;
                try {
                    $reportAdobeConnectSCO = $this->reportAdobeConnectService->getReportAdobeConnect($m);
                    $this->entityManager->flush();
                    $output->writeln("\t> Result meeting AdobeConnectSCO : ".count($reportAdobeConnectSCO).' OK');
                } catch (\Exception $exception) {
                    $output->writeln("\t> Result meeting AdobeConnectSCO : ".$m->getId().' ERROR');
                }
            } else {
                /** @var AdobeConnectWebinarSCO $m */
                $m = $meeting;
                try {
                    $output->writeln("\t> Result meeting AdobeConnectWebinarSCO : ".$m->getId().' In progress');
                    $reportAdobeConnectSCO = $this->reportAdobeConnectWebinarService->getReportAdobeConnect($m);

                    $output->writeln("\t> Result meeting AdobeConnectWebinarSCO : ".count($reportAdobeConnectSCO).' OK');
                } catch (\Exception $exception) {
                    $output->writeln("\t> Result meeting AdobeConnectWebinarSCO : ".$m->getId().' ERROR');
                }
            }
        }

        $io = new SymfonyStyle($input, $output);

        $output->writeln("\t> ------------------------------------------");

        $executionEndTime = microtime(true);
        $seconds = $executionEndTime - $executionStartTime;
        $output->writeln("\t> ------Durée après chargement-------------".round($seconds, 3).' secs');

        $io->success('All done !');

        return Command::SUCCESS;
    }
}
