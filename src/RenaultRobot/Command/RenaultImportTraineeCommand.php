<?php

namespace App\RenaultRobot\Command;

use App\RenaultRobot\Service\RenaultTraineeImportService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class RenaultImportTraineeCommand extends Command implements ContainerAwareInterface
{
    protected static $defaultName = 'renault:import-trainee';

    private ?ContainerInterface $container;

    public function __construct(
        private RenaultTraineeImportService $renaultTraineeImportService,
        private EntityManagerInterface $entityManager
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        //$radicalTest = self::TEMP_TEST;

        $output->writeln("\t> ------Début du traitement-------------");
        $executionStartTime = microtime(true);
        $response = $this->renaultTraineeImportService->importTraineesFromTemp();
        $this->entityManager->flush();
        /*foreach ($response as $k => $v) {
            if (empty($v[17]) || trim($v[17]) == '') {
                unset($response[$k]);
            }
        }*/

        $executionEndTime = microtime(true);
        $seconds = $executionEndTime - $executionStartTime;
        $output->writeln("\t> ------Durée de traitement complet -------------".round($seconds, 3).' secs pour l\'association des participants ---| '.count($response).' lignes  |---------------------');

        $io->success('All done !');

        return Command::SUCCESS;
    }
}
