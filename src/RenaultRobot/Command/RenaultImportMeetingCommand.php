<?php

namespace App\RenaultRobot\Command;

use App\RenaultRobot\Service\RenaultMeetingImportService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class RenaultImportMeetingCommand extends Command implements ContainerAwareInterface
{
    protected static $defaultName = 'renault:import-meeting';

    private ?ContainerInterface $container;

    public function __construct(
        private RenaultMeetingImportService $renaultMeetingImportService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        //$radicalTest = self::TEMP_TEST;

        $output->writeln("\t> ------Début du traitement-------------");
        $executionStartTime = microtime(true);
        $response = $this->renaultMeetingImportService->importMeetingsFromTemp();
        /*foreach ($response as $k => $v) {
            if (empty($v[17]) || trim($v[17]) == '') {
                unset($response[$k]);
            }
        }*/

        $executionEndTime = microtime(true);
        $seconds = $executionEndTime - $executionStartTime;
        $output->writeln("\t> ------Durée après chargement-------------".round($seconds, 3).' secs---| '.count($response).' lignes  |---------------------');

        $io->success('All done !');

        return Command::SUCCESS;
    }
}
