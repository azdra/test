<?php

namespace App\RenaultRobot\Command;

use App\RenaultRobot\Service\RenaultService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class RenaultImportSessionFileCommand extends Command implements ContainerAwareInterface
{
    public const ARG_OUT_BY_PASS = 'outbypass';

    private ?ContainerInterface $container;

    public function __construct(
        private RenaultService $renaultService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('renault:import-session-file')
            ->setDescription('Récupère les présences depuis Adobe Connect pour Renault')
            ->addArgument(
                RenaultImportSessionFileCommand::ARG_OUT_BY_PASS, InputArgument::OPTIONAL, 'outbypass'
            )
        ;
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $dateMin = ( new \DateTime() )->setTime(00, 01);
        $dateMax = ( new \DateTime() )->modify('+10 days')->setTime(23, 59);

        $beginDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2022-10-08 00:00:00');
        if ($dateMin < $beginDate && empty($input->getArgument(RenaultImportSessionFileCommand::ARG_OUT_BY_PASS))) { //Avant le 08/10/2022 00:01:00, nous ne faisons rien !
            $io->success('By pass!');

            return Command::SUCCESS;
        }
        //$radicalTest = self::TEMP_TEST;

        $output->writeln("\t> ------Début du traitement-------------");
        $executionStartTime = microtime(true);
        $response = $this->renaultService->getExtractDataFromFileSFTP();
        foreach ($response as $k => $v) {
            if (empty($v[17]) || trim($v[17]) == '') {
                unset($response[$k]);
            }
        }

        $executionEndTime = microtime(true);
        $seconds = $executionEndTime - $executionStartTime;
        $output->writeln("\t> ------Durée après chargement-------------".round($seconds, 3).' secs---| '.count($response).' lignes  |---------------------');

        $executionStartTime = microtime(true);

        $resultPrepareForPersist = $this->renaultService->prepareDataForPersist($response, $dateMin, $dateMax);

        $output->writeln("\t> ------------------".count($resultPrepareForPersist['arrSessions']).' session.s à importer -----------------------');
        $output->writeln("\t> ------------------".count($resultPrepareForPersist['errorSessions']).' session.s avec anomalies -----------------------');
        $this->renaultService->addSessionsTemp($resultPrepareForPersist['arrSessions']);

        $output->writeln("\n\t> ------------------Fin de l'ajout des sessions temporaires  -----------------------");

        $this->renaultService->addErrorsSessionsTemp($resultPrepareForPersist['errorSessions']);

        $output->writeln("\n\t> ------------------Fin de l'ajout des erreurs des sessions temporaires  -----------------------");

        $executionEndTime = microtime(true);
        $seconds = $executionEndTime - $executionStartTime;

        $output->writeln("\t> ------------------Durée après insertion---------".round($seconds, 3).' secs-----------------------');

        $output->writeln("\t> ------------------------------------------");

        $io->success('All done !');

        return Command::SUCCESS;
    }
}
