<?php

namespace App\RenaultRobot\Command;

use App\RenaultRobot\Entity\RenaultImportSessionTemp;
use App\RenaultRobot\Repository\RenaultImportSessionTempRepository;
use App\Service\MailerService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class RenaultCheckTreatedCommand extends Command implements ContainerAwareInterface
{
    protected static $defaultName = 'renault:check-not-treated';

    private ?ContainerInterface $container;

    public function __construct(
        private RenaultImportSessionTempRepository $renaultImportSessionTempRepository,
        private MailerService $mailerService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $output->writeln("\t> ------Début du traitement-------------");
        $executionStartTime = microtime(true);

        $allRLTRowToImport = $this->renaultImportSessionTempRepository->findAllByResultType(
            RenaultImportSessionTemp::WAITING
        );

        if (count($allRLTRowToImport) > 0) {
            $infos = [];
            foreach ($allRLTRowToImport as $row) {
                $infos[] = [
                    'numRow' => $row->getNumRow(),
                    'referenceRenault' => $row->getReferenceRenault(),
                    'dateStart' => $row->getDateStart()->format('d/m/Y H:i'),
                ];
            }
            $this->mailerService->sendTemplatedMail(
                ['ingenierie@live-session.fr', 'serviceclient@live-session.fr'],
                'Lignes non traitées pour Renault',
                'emails/alert/send_alert_not_treated_renault.html.twig',
                context: [
                    'infos' => $infos,
                ],
                metadatas: [
                    'origin' => MailerService::ORIGIN_EMAIL_ALERT_NO_TREATED_RENAULT,
                ]
            );
        }

        $executionEndTime = microtime(true);
        $seconds = $executionEndTime - $executionStartTime;
        $output->writeln("\t> ------Durée après chargement-------------".round($seconds, 3).' secs---| '.count($allRLTRowToImport).' lignes  |---------------------');

        $io->success('All done !');

        return Command::SUCCESS;
    }
}
