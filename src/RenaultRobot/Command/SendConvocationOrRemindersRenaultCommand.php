<?php

namespace App\RenaultRobot\Command;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Event\TransactionalEmailRequestEventInterface;
use App\Event\TransactionalParticipantEmailRequestEvent;
use App\Event\TransactionalSessionEmailRequestEvent;
use App\Exception\ConvocationSendingFailureException;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Service\ActivityLogger;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Throwable;

class SendConvocationOrRemindersRenaultCommand extends Command
{
    protected static $defaultName = 'email:session:convocations-reminders-renault:send';

    private ProviderSessionRepository $providerSessionRepository;

    private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository;

    private EntityManagerInterface $entityManager;

    private EventDispatcherInterface $eventDispatcher;

    private ActivityLogger $activityLogger;

    private LoggerInterface $logger;

    public function __construct(
        ProviderSessionRepository $providerSessionRepository,
        ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        private MailerService $mailerService,
        LoggerInterface $cronLogger,
        ActivityLogger $activityLogger
    ) {
        parent::__construct();

        $this->providerSessionRepository = $providerSessionRepository;
        $this->providerParticipantSessionRoleRepository = $providerParticipantSessionRoleRepository;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $cronLogger;
        $this->activityLogger = $activityLogger;
    }

    protected function configure(): void
    {
        $this->addOption('session', 's', InputOption::VALUE_OPTIONAL, 'Session ID')
             ->addOption('participant', 'p', InputOption::VALUE_OPTIONAL, 'Participant ID')
             ->addOption('with-animators', 'a', InputOption::VALUE_OPTIONAL, 'Send email to all animators', true)
             ->addOption('with-trainees', 't', InputOption::VALUE_OPTIONAL, 'Send email to all trainees', true);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $sessionId = $input->getOption('session');
            $participantId = $input->getOption('participant');
            $withAnimators = $input->getOption('with-animators');
            $withTrainees = $input->getOption('with-trainees');

            if (!is_null($sessionId) && !is_null($participantId)) {
                /** @var ProviderParticipantSessionRole $participantRole */
                $participantRole = $this->providerParticipantSessionRoleRepository->findOneBy(['session' => $sessionId, 'participant' => $participantId]);

                if (!$participantRole) {
                    throw new ConvocationSendingFailureException("Unable to find the participant #$participantId on the session #$sessionId");
                }

                $output->writeln(
                    "Sent a convocation to session #{$participantRole->getSession()->getId()} for the participant #{$participantRole->getParticipant()->getId()}"
                );

                $this->eventDispatcher->dispatch(new TransactionalParticipantEmailRequestEvent($participantRole, force: true), TransactionalEmailRequestEventInterface::CONVOCATION_ONE_PARTICIPANT);
            } elseif (!is_null($sessionId) && is_null($participantId)) {
                /** @var ProviderSession $session */
                $session = $this->providerSessionRepository->find($sessionId);

                if (!$session) {
                    throw new ConvocationSendingFailureException("Session #$sessionId not found");
                }

                if (!is_bool($withTrainees) || !is_bool($withAnimators)) {
                    throw new ConvocationSendingFailureException("Invalid option 'with-trainees' or 'with-animators'");
                }

                if ($withAnimators) {
                    /** @var ProviderParticipantSessionRole $participantRole */
                    foreach ($session->getAnimatorsOnly() as $participantRole) {
                        $output->writeln(
                            "Sent a convocation to session #{$session->getId()} for the participant #{$participantRole->getId()}"
                        );
                        $this->eventDispatcher->dispatch(new TransactionalParticipantEmailRequestEvent($participantRole, force: true), TransactionalEmailRequestEventInterface::CONVOCATION_ONE_PARTICIPANT);
                    }
                }

                if ($withTrainees) {
                    /** @var ProviderParticipantSessionRole $participantRole */
                    foreach ($session->getTraineesOnly() as $participantRole) {
                        $output->writeln(
                            "Sent a convocation to session #{$session->getId()} for the participant #{$participantRole->getId()}"
                        );
                        $this->eventDispatcher->dispatch(new TransactionalParticipantEmailRequestEvent($participantRole, force: true), TransactionalEmailRequestEventInterface::CONVOCATION_ONE_PARTICIPANT);
                    }
                }
            } else {
                $sessions = $this->providerSessionRepository->findAllSessionsWithPastNotificationSendingDate();

                /** @var ProviderSession $session */
                foreach ($sessions as $session) {
                    if ($session->getClient()->getName() === 'Renault') {
                        $output->write("> #{$session->getId()} [{$session->getRef()}]");

                        $this->eventDispatcher->dispatch(new TransactionalSessionEmailRequestEvent($session), TransactionalEmailRequestEventInterface::CONVOCATION_ALL_PARTICIPANTS);
                        $this->eventDispatcher->dispatch(new TransactionalSessionEmailRequestEvent($session), TransactionalEmailRequestEventInterface::REMINDER_ALL_PARTICIPANTS);

                        $this->entityManager->flush();

                        $output->writeln("\t >> All done");
                    }
                }
            }

            $io->success('All done !');
            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();

            return Command::SUCCESS;
        } catch (ConvocationSendingFailureException $exception) {
            $this->logger->error($exception, [
                'sessionId' => $sessionId,
                'participantId' => $participantId,
                'withAnimators' => $withAnimators,
                'withTrainees' => $withTrainees,
            ]);
            $io->error($exception->getMessage());

            return Command::FAILURE;
        } catch (Throwable $exception) {
            $io->error('Unknown error: '.$exception->getMessage());
            $this->logger->error($exception, [
                'sessionId' => $sessionId,
                'participantId' => $participantId,
                'withAnimators' => $withAnimators,
                'withTrainees' => $withTrainees,
            ]);

            return Command::FAILURE;
        }
    }
}
