<?php

namespace App\Model;

use App\Exception\MiddlewareException;
use App\Exception\ProviderServiceValidationException;

final class MiddlewareResponse
{
    private mixed $data = null;

    private bool $success = true;

    private ?\Throwable $exception = null;

    /**
     * Is this response is coming directly from the third-party provider?
     * Validation exceptions does NOT count as a response from the third-party provider.
     */
    private bool $fromProvider = false;

    public static function fromProvider(ProviderResponse $providerResponse): MiddlewareResponse
    {
        $response = new self();
        $response->setFromProvider(true)
            ->setSuccess($providerResponse->isSuccess())
            ->setData($providerResponse->getData())
            ->setException($providerResponse->getThrown());

        return $response;
    }

    public static function fromValidationException(ProviderServiceValidationException $exception): MiddlewareResponse
    {
        $response = new self();
        $response->setSuccess(false)
            ->setData($exception->violationList)
            ->setException($exception);

        return $response;
    }

    public static function fromMiddlewareException(MiddlewareException $exception): MiddlewareResponse
    {
        return (new self())
            ->setSuccess(false)
            ->setData($exception)
            ->setException($exception);
    }

    public function getData(): mixed
    {
        return $this->data;
    }

    public function setData(mixed $data): MiddlewareResponse
    {
        $this->data = $data;

        if ($data instanceof \Throwable) {
            $this->setException($data);
        }

        return $this;
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function getException(): ?\Throwable
    {
        return $this->exception;
    }

    public function setException(?\Throwable $exception): MiddlewareResponse
    {
        $this->exception = $exception;

        return $this;
    }

    public function setSuccess(bool $success): MiddlewareResponse
    {
        $this->success = $success;

        return $this;
    }

    public function isFromProvider(): bool
    {
        return $this->fromProvider;
    }

    public function setFromProvider(bool $fromProvider): MiddlewareResponse
    {
        $this->fromProvider = $fromProvider;

        return $this;
    }
}
