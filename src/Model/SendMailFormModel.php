<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class SendMailFormModel
{
    public function __construct(
        private array $recipients = [],
        private array $cc = [],
        private array $bcc = []
    ) {
    }

    #[Callback]
    public function validEmail(ExecutionContextInterface $context): void
    {
        foreach ($this->recipients['tags'] as $email) {
            if (!preg_match('/^.+\@\S+\.\S+$/', $email)) {
                $context->buildViolation('This email "{{ email }}" is not a valid')
                    ->setParameter('{{ email }}', $email)
                    ->atPath('recipients')
                    ->addViolation();
            }
        }

        foreach ($this->cc['tags'] as $email) {
            if (!preg_match('/^.+\@\S+\.\S+$/', $email)) {
                $context->buildViolation('This email "{{ email }}" is not a valid')
                    ->setParameter('{{ email }}', $email)
                    ->atPath('recipients')
                    ->addViolation();
            }
        }

        foreach ($this->bcc['tags'] as $email) {
            if (!preg_match('/^.+\@\S+\.\S+$/', $email)) {
                $context->buildViolation('This email "{{ email }}" is not a valid')
                    ->setParameter('{{ email }}', $email)
                    ->atPath('recipients')
                    ->addViolation();
            }
        }
    }

    public function getRecipients(): array
    {
        return $this->recipients;
    }

    public function setRecipients(array $recipients): void
    {
        $this->recipients = $recipients;
    }

    public function getCc(): array
    {
        return $this->cc;
    }

    public function setCc(array $cc): void
    {
        if (count($cc['tags']) === 1 && $cc['tags'][0] === '') {
            $this->cc = ['tags' => []];
        } else {
            $this->cc = $cc;
        }
    }

    public function getBcc(): array
    {
        return $this->bcc;
    }

    public function setBcc(array $bcc): void
    {
        if (count($bcc['tags']) === 1 && $bcc['tags'][0] === '') {
            $this->bcc = ['tags' => []];
        } else {
            $this->bcc = $bcc;
        }
    }
}
