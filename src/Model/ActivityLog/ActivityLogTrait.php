<?php

namespace App\Model\ActivityLog;

use App\Entity\ActivityLog;
use App\Entity\Client\Client;
use App\Entity\User;

trait ActivityLogTrait
{
    public function generateActivityLog(Client $client, User $user, string $severity, string $action, array $info = []): void
    {
        $this->activityLogger->initActivityLogContext($client, $user, ActivityLog::ORIGIN_USER_INTERFACE, $info);
        $this->activityLogger->addActivityLog(new ActivityLogModel($action, $severity));
        $this->activityLogger->flushActivityLogs();
    }

    public function initActivitLogContextAndFlushLog($client, $user): void
    {
        $this->activityLogger->initActivityLogContext($client, $user, ActivityLog::ORIGIN_USER_INTERFACE);
        $this->activityLogger->flushActivityLogs();
    }
}
