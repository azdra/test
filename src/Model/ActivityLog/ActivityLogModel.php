<?php

namespace App\Model\ActivityLog;

class ActivityLogModel
{
    public function __construct(
        public string $action,
        public string $severity,
        public array $data = []
    ) {
    }
}
