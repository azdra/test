<?php

namespace App\Model\ActivityLog;

use App\Entity\Client\Client;
use Symfony\Component\Security\Core\User\UserInterface;

class ActivityLogContext
{
    public function __construct(
        public Client $client,
        public ?UserInterface $user,
        public string $origin,
        public array $infos = [],
    ) {
    }
}
