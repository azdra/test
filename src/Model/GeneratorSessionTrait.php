<?php

namespace App\Model;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSessionAccessToken;
use App\Entity\ProviderSessionReplay;
use App\Exception\ProviderSessionAccessToken\DecryptProviderSessionAccessTokenException;

trait GeneratorSessionTrait
{
    use GeneratorTrait;

    public function encryptToken(string $appSecret, string $token): array
    {
        $cipher = 'aes-128-gcm';
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);

        $ciphertext = openssl_encrypt(sprintf('%s;%s', $token, $this->randomString(5)), $cipher, $appSecret, $options = 0, $iv, $tags);

        return [
            'token' => base64_encode($ciphertext),
            'tag' => base64_encode($tags),
            'iv' => base64_encode($iv),
        ];
    }

    public function encryptSessionReplayToken(string $appSecret, ProviderSessionReplay $providerSessionReplay): ProviderSessionAccessToken
    {
        $providerParticipantEmail = $providerSessionReplay->getEmail();
        $providerSessionReference = $providerSessionReplay->getSession()->getRef();

        $toEncrypt = "$providerParticipantEmail;$providerSessionReference";

        $token = $this->encryptToken($appSecret, $toEncrypt);

        $providerSessionReplay->setProviderReplayAccessToken(
            (new ProviderSessionAccessToken())
                ->setToken($token['token'])
                ->setTag($token['tag'])
                ->setIv($token['iv'])
        );

        return $providerSessionReplay->getProviderReplayAccessToken();
    }

    public function encryptSessionAccessToken(string $appSecret, ProviderParticipantSessionRole $providerParticipantSessionRole): ProviderSessionAccessToken
    {
        $providerParticipantEmail = $providerParticipantSessionRole->getParticipant()->getEmail();
        $providerSessionReference = $providerParticipantSessionRole->getSession()->getRef();
        $role = $providerParticipantSessionRole->getRole();

        $toEncrypt = "$providerParticipantEmail;$providerSessionReference;$role";

        $token = $this->encryptToken($appSecret, $toEncrypt);

        $providerParticipantSessionRole->setProviderSessionAccessToken(
            (new ProviderSessionAccessToken())
                ->setToken($token['token'])
                ->setTag($token['tag'])
                ->setIv($token['iv'])
        );

        return $providerParticipantSessionRole->getProviderSessionAccessToken();
    }

    /**
     * @throws DecryptProviderSessionAccessTokenException
     */
    public function decryptSessionAccessToken(string $appSecret, ProviderSessionAccessToken $providerSessionAccessToken): array
    {
        $cipher = 'aes-128-gcm';

        $data = openssl_decrypt(base64_decode($providerSessionAccessToken->getToken()), $cipher, $appSecret, $options = 0, base64_decode($providerSessionAccessToken->getIv()), base64_decode($providerSessionAccessToken->getTag()));

        if ($data === false) {
            throw new DecryptProviderSessionAccessTokenException($providerSessionAccessToken, null);
        }

        return explode(';', $data);
    }
}
