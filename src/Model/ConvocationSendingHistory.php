<?php
/**
 * @author:    Emmanuel SMITH <emmanuel.smith@live-session.fr>
 * project:    mls_m3
 * file:    ConvocationSendingHistory.php
 * Date:    21/09/2021
 * Time:    15:07
 */

namespace App\Model;

class ConvocationSendingHistory
{
    public const TYPE_CONVOCATION = 'convocation';
    public const TYPE_REMINDER = 'reminder';
    public const TYPE_LIST = [
        self::TYPE_CONVOCATION,
        self::TYPE_REMINDER,
    ];

    private string $type = self::TYPE_CONVOCATION;

    private \DateTimeImmutable $date;

    private ?int $origin = null;

    public function __construct()
    {
        $this->date = new \DateTimeImmutable();
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        if (!in_array($type, self::TYPE_LIST)) {
            throw new \LogicException('Invalid type ');
        }

        $this->type = $type;

        return $this;
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getOrigin(): ?int
    {
        return $this->origin;
    }

    public function setOrigin(?int $origin): self
    {
        $this->origin = $origin;

        return $this;
    }
}
