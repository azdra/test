<?php

namespace App\Model;

trait JsonUtilsTrait
{
    public function decode(string $data): array
    {
        if (false === $result = @json_decode($data, true)) {
            throw new \RuntimeException('Unable to decode the data');
        }

        return $result;
    }
}
