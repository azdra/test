<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ResetAdobePasswordModel
{
    #[Assert\Length(
        min: 8,
        max: 32,
        minMessage: 'Password lenght must be between 8 and 32 characters',
        maxMessage: 'Password lenght must be between 8 and 32 characters',
    )]
    protected string $password;

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): ResetAdobePasswordModel
    {
        $this->password = $password;

        return $this;
    }
}
