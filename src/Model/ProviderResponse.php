<?php

namespace App\Model;

use Exception;

class ProviderResponse
{
    public function __construct(
        protected bool $success = true,
        protected ?Exception $thrown = null,
        protected mixed $data = null
    ) {
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): ProviderResponse
    {
        $this->success = $success;

        return $this;
    }

    public function getThrown(): ?Exception
    {
        return $this->thrown;
    }

    public function setThrown(?Exception $thrown): ProviderResponse
    {
        $this->thrown = $thrown;

        return $this;
    }

    public function getData(): mixed
    {
        return $this->data;
    }

    public function setData(mixed $data): ProviderResponse
    {
        $this->data = $data;

        return $this;
    }
}
