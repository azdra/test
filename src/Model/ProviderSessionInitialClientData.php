<?php

namespace App\Model;

use App\Entity\Client\Client;
use App\Entity\ProviderSession;
use App\Service\ProviderSessionService;

class ProviderSessionInitialClientData
{
    public string $reference;

    public function toArray(): array
    {
        return [
            'ref' => $this->reference,
        ];
    }

    public static function fromClient(Client $client): self
    {
        $initialData = new self();

        $initialData->reference = ProviderSessionService::generateSessionReference($client->getName());

        return $initialData;
    }

    public function apply(ProviderSession $session): void
    {
        $session->setRef($this->reference);
    }
}
