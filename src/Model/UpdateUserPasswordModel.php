<?php

namespace App\Model;

use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class UpdateUserPasswordModel extends ResetUserPasswordModel
{
    #[UserPassword]
    protected string $oldPassword;

    public function getOldPassword(): string
    {
        return $this->oldPassword;
    }

    public function setOldPassword(string $oldPassword): UpdateUserPasswordModel
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }
}
