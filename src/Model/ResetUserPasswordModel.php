<?php

namespace App\Model;

use App\Validator\Constraint\PasswordSecurityPolicy;

class ResetUserPasswordModel
{
    #[PasswordSecurityPolicy]
    protected ?string $password = null;

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): ResetUserPasswordModel
    {
        $this->password = $password;

        return $this;
    }
}
