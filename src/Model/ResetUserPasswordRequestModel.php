<?php

namespace App\Model;

class ResetUserPasswordRequestModel
{
    private string $email;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): ResetUserPasswordRequestModel
    {
        $this->email = $email;

        return $this;
    }
}
