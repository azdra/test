<?php

namespace App\Model\Provider\AdobeConnect;

    class AdobeConnectWebinarCommonInfo
    {
        public function __construct(
            private int $userId,
            private string $userLogin
        ) {
        }

        public function getUserId(): int
        {
            return $this->userId;
        }

        public function getUserLogin(): string
        {
            return $this->userLogin;
        }
    }
