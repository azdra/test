<?php

namespace App\Model;

use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

trait ValidatorTrait
{
    /**
     * @deprecated /!\ Attention au comportement de ce getter qui génère des erreurs à l'usage
     *             passer par l'injection de dependance plutôt
     * @see ==> https://github.com/symfony/symfony/issues/29870#issuecomment-453993982
     */
    public function getValidator(): ?ValidatorInterface
    {
        return Validation::createValidatorBuilder()
                  ->enableAnnotationMapping(true)
                  ->addDefaultDoctrineAnnotationReader()
                  ->getValidator();
    }

    public function retreiveViolationByFields(ConstraintViolationListInterface $constraintViolationList): array
    {
        $violations = [];

        /** @var ConstraintViolation $item */
        foreach ($constraintViolationList as $item) {
            $violations[$item->getPropertyPath()] = [$item->getMessage()];
        }

        return $violations;
    }
}
