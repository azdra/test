<?php

namespace App\Model;

trait GeneratorTrait
{
    public function randomHexString(int $length = 6): string
    {
        $rand = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];

        $res = '';
        for ($i = 0; $i < $length; ++$i) {
            $res .= $rand[rand(0, 15)];
        }

        return $res;

        return $rand[rand(0, 15)].$rand[rand(0, 15)].$rand[rand(0, 15)].$rand[rand(0, 15)].$rand[rand(0, 15)].$rand[rand(0, 15)];
    }

    public function randomString(int $lenght = 15): string
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $string = [];
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $lenght; ++$i) {
            $n = rand(0, $alphaLength);
            $string[] = $alphabet[$n];
        }

        return implode($string);
    }
}
