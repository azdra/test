<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class EvaluationSessionParticipantAnswer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'read_lobby_signature', 'lite_participant'])]
    private int $id;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $replyDate;

    #[ORM\ManyToOne(targetEntity: ProviderParticipantSessionRole::class, inversedBy: 'evaluationSessionAnswers')]
    private ProviderParticipantSessionRole $providerParticipantSessionRole;

    #[ORM\ManyToOne(targetEntity: EvaluationProviderSession::class, inversedBy: 'evaluationSessionAnswers')]
    #[Groups(['read_lobby_signature', 'lite_participant', 'session:evaluation:synthesis', 'read_session'])]
    private EvaluationProviderSession $evaluationProviderSession;

    #[ORM\Column(type: 'json')]
    #[Groups(['read_session', 'session:evaluation:synthesis', 'read_lobby_signature', 'lite_participant'])]
    private array $answer = [];

    public function __construct(EvaluationProviderSession $evaluationProviderSession, ProviderParticipantSessionRole $providerParticipantSessionRole)
    {
        $this->providerParticipantSessionRole = $providerParticipantSessionRole;
        $this->evaluationProviderSession = $evaluationProviderSession;
        $this->replyDate = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getReplyDate(): \DateTime
    {
        return $this->replyDate;
    }

    public function setReplyDate(\DateTime $replyDate): EvaluationSessionParticipantAnswer
    {
        $this->replyDate = $replyDate;

        return $this;
    }

    public function getProviderParticipantSessionRole(): ProviderParticipantSessionRole
    {
        return $this->providerParticipantSessionRole;
    }

    public function setProviderParticipantSessionRole(ProviderParticipantSessionRole $providerParticipantSessionRole): EvaluationSessionParticipantAnswer
    {
        $this->providerParticipantSessionRole = $providerParticipantSessionRole;

        return $this;
    }

    public function getEvaluationProviderSession(): EvaluationProviderSession
    {
        return $this->evaluationProviderSession;
    }

    public function setEvaluationProviderSession(EvaluationProviderSession $evaluationProviderSession): EvaluationSessionParticipantAnswer
    {
        $this->evaluationProviderSession = $evaluationProviderSession;

        return $this;
    }

    public function getAnswer(): array
    {
        return $this->answer;
    }

    public function setAnswer(array $answer): EvaluationSessionParticipantAnswer
    {
        $this->answer = $answer;

        return $this;
    }
}
