<?php

namespace App\Entity;

use App\Entity\Client\EvaluationModalMail;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class SessionConvocationAttachment
{
    public const AUTHORIZED_EXTENSIONS = ['xls', 'xlsx', 'csv', 'ods', 'doc', 'docx', 'txt', 'pdf', 'jpg', 'png', 'ppt', 'pptx', 'odp', 'zip', 'rar'];
    public const MAX_FILE_SIZE = 20000000;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column]
    #[Groups(['read_session', 'read_evaluation_modal'])]
    private int $id;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Groups(['read_session', 'read_evaluation_modal'])]
    private string $originalName;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Groups(['read_session', 'read_evaluation_modal'])]
    private string $fileName;

    #[ORM\Column]
    #[Groups(['read_session', 'read_evaluation_modal'])]
    private string $token;

    #[ORM\ManyToOne(targetEntity: ProviderSession::class, inversedBy: 'attachments')]
    #[ORM\JoinColumn(name: 'session_id', referencedColumnName: 'id', nullable: true)]
    private ?ProviderSession $session;

    #[ORM\ManyToOne(targetEntity: WebinarConvocation::class, inversedBy: 'attachments')]
    #[ORM\JoinColumn(name: 'webinar_communication_id', referencedColumnName: 'id', nullable: true)]
    private ?WebinarConvocation $webinarConvocation;

    #[ORM\ManyToOne(targetEntity: EvaluationProviderSession::class, inversedBy: 'attachments')]
    #[ORM\JoinColumn(name: 'evaluation_provider_session_id', referencedColumnName: 'id', nullable: true)]
    private ?EvaluationProviderSession $evaluationProviderSession;

    #[ORM\ManyToOne(targetEntity: EvaluationModalMail::class, inversedBy: 'attachments')]
    #[ORM\JoinColumn(name: 'evaluation_modal_mail_id', referencedColumnName: 'id', nullable: true)]
    private ?EvaluationModalMail $evaluationModalMail;

    public function __construct()
    {
        $this->token = \md5(\random_bytes(32));
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getOriginalName(): string
    {
        return $this->originalName;
    }

    public function setOriginalName(string $originalName): self
    {
        $this->originalName = $originalName;

        return $this;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getSession(): ?ProviderSession
    {
        return $this->session;
    }

    public function setSession(?ProviderSession $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getWebinarConvocation(): ?WebinarConvocation
    {
        return $this->webinarConvocation;
    }

    public function setWebinarConvocation(?WebinarConvocation $webinarConvocation): self
    {
        $this->webinarConvocation = $webinarConvocation;

        return $this;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getEvaluationProviderSession(): ?EvaluationProviderSession
    {
        return $this->evaluationProviderSession;
    }

    public function setEvaluationProviderSession(?EvaluationProviderSession $evaluationProviderSession): self
    {
        $this->evaluationProviderSession = $evaluationProviderSession;

        return $this;
    }

    public function getEvaluationModalMail(): ?EvaluationModalMail
    {
        return $this->evaluationModalMail;
    }

    public function setEvaluationModalMail(?EvaluationModalMail $evaluationModalMail): SessionConvocationAttachment
    {
        $this->evaluationModalMail = $evaluationModalMail;

        return $this;
    }
}
