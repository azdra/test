<?php

namespace App\Entity;

use App\Controller\Api\ProviderSessionApiController;
use App\Controller\Api\UserApiController;
use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectWebinarPrincipal;
use App\Entity\Cisco\WebexParticipant;
use App\Entity\Cisco\WebexRestParticipant;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsEventParticipant;
use App\Entity\Microsoft\MicrosoftTeamsParticipant;
use App\Entity\White\WhiteParticipant;
use App\Exception\InvalidArgumentException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'discr', type: 'string')]
#[ORM\DiscriminatorMap(['adobe_connect_principal' => AdobeConnectPrincipal::class, 'adobe_connect_webinar_principal' => AdobeConnectWebinarPrincipal::class, 'cisco_webex_participant' => WebexParticipant::class, 'cisco_webex_rest_participant' => WebexRestParticipant::class, 'microsoft_teams_participant' => MicrosoftTeamsParticipant::class, 'microsoft_teams_event_participant' => MicrosoftTeamsEventParticipant::class, 'white_participant' => WhiteParticipant::class])]
abstract class ProviderParticipant implements MiddlewareManagedEntityInterface
{
    protected static string $provider = 'none';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'read_webex_user', 'read_webex_rest_user', 'read_microsoft_teams_user', 'read_microsoft_teams_event_user', 'lite_participant'])]
    protected int $id;

    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    protected bool $allowToSendCanceledMail = true;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'participants')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    #[Groups(['read_webex_user', 'read_webex_rest_user', 'read_microsoft_teams_user', 'read_microsoft_teams_event_user', 'read_white_connector_user', 'read_user', 'read_lobby_signature', ProviderSessionApiController::GROUP_CREATE, 'lite_participant'])]
    protected User $user;

    #[ORM\ManyToOne(targetEntity: AbstractProviderConfigurationHolder::class)]
    #[Groups(['read_webex_user', 'read_webex_rest_user', 'read_microsoft_teams_user', 'read_microsoft_teams_event_user', 'read_white_connector_user'])]
    protected AbstractProviderConfigurationHolder $configurationHolder;

    #[ORM\OneToMany(mappedBy: 'participant', targetEntity: ProviderParticipantSessionRole::class, cascade: ['all'])]
    #[Groups([UserApiController::GROUP_VIEW, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    protected Collection $sessionRoles;

    public function __construct(AbstractProviderConfigurationHolder $configurationHolder)
    {
        if (!$this->supportConfigurationHolder($configurationHolder)) {
            throw new InvalidArgumentException();
        }
        $this->configurationHolder = $configurationHolder;
        $this->sessionRoles = new ArrayCollection();
    }

    abstract public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): ProviderParticipant
    {
        $this->id = $id;

        return $this;
    }

    public static function getProvider(): string
    {
        return static::$provider;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): ProviderParticipant
    {
        $this->user = $user;

        return $this;
    }

    public function getConfigurationHolder(): AbstractProviderConfigurationHolder
    {
        return $this->configurationHolder;
    }

    public function setConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): self
    {
        $this->configurationHolder = $configurationHolder;

        return $this;
    }

    public function getAbstractProviderConfigurationHolder(): AbstractProviderConfigurationHolder
    {
        return $this->configurationHolder;
    }

    public function getSessionRoles(): ArrayCollection|Collection
    {
        return $this->sessionRoles;
    }

    public function setSessionRoles(ArrayCollection|Collection $sessionRoles): ProviderParticipant
    {
        $this->sessionRoles = $sessionRoles;

        return $this;
    }

    public function addSessionRoles(ProviderParticipantSessionRole $role): ProviderParticipant
    {
        $this->sessionRoles->add($role);

        return $this;
    }

    #[Groups(['read_session'])]
    public function getLastName(): ?string
    {
        return $this->getUser()->getLastName();
    }

    #[Groups(['read_session'])]
    public function getFirstName(): ?string
    {
        return $this->getUser()->getFirstName();
    }

    public function getClient(): Client
    {
        return $this->getUser()->getClient();
    }

    #[Groups(['read_session', 'lite_participant'])]
    public function getEmail(): ?string
    {
        return $this->getUser()->getEmail();
    }

    #[Groups(['read_session', 'lite_participant'])]
    public function getFullName(): string
    {
        return "{$this->getLastName()} {$this->getFirstName()}";
    }

    #[Groups(['read_session'])]
    public function getUserId(): int
    {
        return $this->user->getId();
    }

    public function __toString(): string
    {
        return $this->getFullName();
    }

    public function isAllowToSendCanceledMail(): bool
    {
        return $this->allowToSendCanceledMail;
    }

    public function setAllowToSendCanceledMail(bool $allowToSendCanceledMail): void
    {
        $this->allowToSendCanceledMail = $allowToSendCanceledMail;
    }
}
