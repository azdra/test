<?php

namespace App\Entity;

use App\Validator as AppAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class EvaluationProviderSession
{
    public const EVALUATION_KNOWLEDGE = 'knowledge';
    public const EVALUATION_COLD = 'cold';
    public const EVALUATION_HOT = 'hot';
    public const EVALUATION_ACQUIRED = 'acquired';
    public const EVALUATION_POSITIONING = 'positioning';
    public const EVALUATION_OTHER = 'other';

    public const reverseEvaluationType = [
        self::EVALUATION_KNOWLEDGE => 'Knowledge',
        self::EVALUATION_COLD => 'Cold',
        self::EVALUATION_HOT => 'Hot',
        self::EVALUATION_ACQUIRED => 'Acquired',
        self::EVALUATION_POSITIONING => 'Positioning',
        self::EVALUATION_OTHER => 'Other',
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'read_lobby_evaluation', 'read_lobby_signature', 'lite_participant', 'session:evaluation:synthesis'])]
    private int $id;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_session'])]
    private \DateTime $createdAt;

    #[ORM\ManyToOne(targetEntity: ProviderSession::class, inversedBy: 'evaluationsProviderSession')]
    private ProviderSession $providerSession;

    #[ORM\ManyToOne(targetEntity: Evaluation::class, inversedBy: 'evaluationsProviderSession')]
    #[Groups(['read_session', 'session:evaluation:synthesis'])]
    private Evaluation $evaluation;

    #[ORM\OneToMany(mappedBy: 'evaluationProviderSession', targetEntity: EvaluationSessionParticipantAnswer::class, cascade: ['all'])]
    protected Collection $evaluationSessionAnswers;

    #[ORM\Column(type: 'string', nullable: false)]
    #[
        Assert\Choice(
            choices: [
                self::EVALUATION_KNOWLEDGE,
                self::EVALUATION_COLD,
                self::EVALUATION_HOT,
                self::EVALUATION_ACQUIRED,
                self::EVALUATION_POSITIONING,
                self::EVALUATION_OTHER,
            ])
    ]
    #[Groups(['read_session'])]
    private string $evaluationType = self::EVALUATION_KNOWLEDGE;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['read_session'])]
    private ?\DateTime $sendAt = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[AppAssert\ConstraintShortCode]
    #[Groups(['read_session'])]
    private ?string $subjectMail;

    #[ORM\Column(type: 'text', nullable: true)]
    #[AppAssert\ConstraintShortCode]
    #[Groups(['read_session'])]
    private ?string $content;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Groups(['read_session'])]
    private bool $sent = false;

    #[ORM\OneToMany(mappedBy: 'evaluationProviderSession', targetEntity: SessionConvocationAttachment::class, cascade: ['all'])]
    #[Groups(['read_session'])]
    protected Collection $attachments;

    public function __construct(Evaluation $evaluation, ProviderSession $providerSession)
    {
        $this->providerSession = $providerSession;
        $this->evaluation = $evaluation;
        $this->createdAt = new \DateTime();
        $this->attachments = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): EvaluationProviderSession
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getProviderSession(): ProviderSession
    {
        return $this->providerSession;
    }

    public function setProviderSession(ProviderSession $providerSession): EvaluationProviderSession
    {
        $this->providerSession = $providerSession;

        return $this;
    }

    public function getEvaluation(): Evaluation
    {
        return $this->evaluation;
    }

    public function setEvaluation(Evaluation $evaluation): EvaluationProviderSession
    {
        $this->evaluation = $evaluation;

        return $this;
    }

    public function getEvaluationType(): string
    {
        return $this->evaluationType;
    }

    public function setEvaluationType(string $evaluationType): EvaluationProviderSession
    {
        $this->evaluationType = $evaluationType;

        return $this;
    }

    public function getSendAt(): ?\DateTime
    {
        return $this->sendAt;
    }

    public function setSendAt(?\DateTime $sendAt): EvaluationProviderSession
    {
        $this->sendAt = $sendAt;

        return $this;
    }

    public function getEvaluationSessionAnswers(): Collection
    {
        return $this->evaluationSessionAnswers;
    }

    public function setEvaluationSessionAnswers(Collection $evaluationSessionAnswers): self
    {
        $this->evaluationSessionAnswers = $evaluationSessionAnswers;

        return $this;
    }

    public function getSubjectMail(): ?string
    {
        return $this->subjectMail;
    }

    public function setSubjectMail(?string $subjectMail): self
    {
        $this->subjectMail = $subjectMail;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function isSent(): bool
    {
        return $this->sent;
    }

    public function setSent(bool $sent): self
    {
        $this->sent = $sent;

        return $this;
    }

    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function setAttachments(Collection $attachments): self
    {
        $this->attachments = $attachments;

        return $this;
    }
}
