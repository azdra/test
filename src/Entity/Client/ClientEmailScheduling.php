<?php

namespace App\Entity\Client;

use App\Form\Client\Tab\ClientEmailTabFormType;
use App\Validator\Constraint\ScheduleConstraint;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Embeddable]
class ClientEmailScheduling
{
    #[ORM\Column(nullable: true)]
    #[ScheduleConstraint(groups: [ClientEmailTabFormType::FORM_NAME])]
    #[Assert\NotBlank(groups: [ClientEmailTabFormType::FORM_NAME])]
    private ?array $timeToSendConvocationBeforeSession = null;

    #[ORM\Column(nullable: true)]
    //#[Assert\NotBlank(groups: [ClientEmailTabFormType::FORM_NAME])]
    private ?array $timesToSendReminderBeforeSession = null;

    #[ORM\Column(nullable: true)]
//    #[Assert\NotBlank(groups: [ClientEmailTabFormType::FORM_NAME])]
    private ?array $npaiAlertRecipients = null;

    #[ORM\Column(nullable: true)]
    #[ScheduleConstraint(options: ['useMinutes' => false], groups: [ClientEmailTabFormType::FORM_NAME])]
    #[Assert\NotBlank(groups: [ClientEmailTabFormType::FORM_NAME])]
    private ?array $timeToSendPresenceReportAfterSession = null;

    #[ORM\Column]
    private bool $enableSendRemindersToAnimators = false;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(groups: [ClientEmailTabFormType::FORM_NAME])]
    private ?array $presenceReportRecipients = null;

    public function getTimeToSendConvocationBeforeSession(): ?array
    {
        return $this->timeToSendConvocationBeforeSession;
    }

    public function setTimeToSendConvocationBeforeSession(?array $timeToSendConvocationBeforeSession): ClientEmailScheduling
    {
        $this->timeToSendConvocationBeforeSession = $timeToSendConvocationBeforeSession;

        return $this;
    }

    public function getTimesToSendReminderBeforeSession(): ?array
    {
        return $this->timesToSendReminderBeforeSession;
    }

    public function setTimesToSendReminderBeforeSession(?array $timesToSendReminderBeforeSession): ClientEmailScheduling
    {
        $this->timesToSendReminderBeforeSession = $timesToSendReminderBeforeSession;

        return $this;
    }

    public function getNpaiAlertRecipients(): ?array
    {
        return $this->npaiAlertRecipients;
    }

    public function setNpaiAlertRecipients(?array $npaiAlertRecipients): ClientEmailScheduling
    {
        $this->npaiAlertRecipients = $npaiAlertRecipients;

        return $this;
    }

    public function getTimeToSendPresenceReportAfterSession(): ?array
    {
        return $this->timeToSendPresenceReportAfterSession;
    }

    public function setTimeToSendPresenceReportAfterSession(?array $timeToSendPresenceReportAfterSession): ClientEmailScheduling
    {
        $this->timeToSendPresenceReportAfterSession = $timeToSendPresenceReportAfterSession;

        return $this;
    }

    public function isEnableSendRemindersToAnimators(): bool
    {
        return $this->enableSendRemindersToAnimators;
    }

    public function setEnableSendRemindersToAnimators(bool $enableSendRemindersToAnimators): ClientEmailScheduling
    {
        $this->enableSendRemindersToAnimators = $enableSendRemindersToAnimators;

        return $this;
    }

    public function getPresenceReportRecipients(): ?array
    {
        return $this->presenceReportRecipients;
    }

    public function setPresenceReportRecipients(?array $presenceReportRecipients): ClientEmailScheduling
    {
        $this->presenceReportRecipients = $presenceReportRecipients;

        return $this;
    }

    #[Callback(groups: [ClientEmailTabFormType::FORM_NAME])]
    public function validateDates(ExecutionContextInterface $context, $payload): void
    {
        if (count($this->npaiAlertRecipients['tags']) > 1 || $this->npaiAlertRecipients['tags'][0] !== '') {
            foreach ($this->npaiAlertRecipients['tags'] as $npaiAlertRecipient) {
                if (!preg_match('/^.+\@\S+\.\S+$/', $npaiAlertRecipient)) {
                    $context->buildViolation('This email "{{ email }}" is not a valid')
                        ->setParameter('{{ email }}', $npaiAlertRecipient)
                        ->atPath('npaiAlertRecipients')
                        ->addViolation();

                    break;
                }
            }
        }

        foreach ($this->presenceReportRecipients['tags'] as $presenceReportRecipient) {
            if (!preg_match('/^.+\@\S+\.\S+$/', $presenceReportRecipient)) {
                $context->buildViolation('This email "{{ email }}" is not a valid')
                    ->setParameter('{{ email }}', $presenceReportRecipient)
                        ->atPath('presenceReportRecipients')
                        ->addViolation();

                break;
            }
        }
    }
}
