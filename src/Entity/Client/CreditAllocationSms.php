<?php

namespace App\Entity\Client;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class CreditAllocationSms
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_client'])]
    protected int $id;

    #[ORM\Column(options: ['default' => 0])]
    #[Groups(['read_client'])]
    private int $allowedCredit = 0;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_client'])]
    private \DateTime $createdAt;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'creditAllocationsSms')]
    protected Client $client;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getAllowedCredit(): int
    {
        return $this->allowedCredit;
    }

    public function setAllowedCredit(int $allowedCredit): self
    {
        $this->allowedCredit = $allowedCredit;
        $this->client->addCreditSms($allowedCredit);

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
