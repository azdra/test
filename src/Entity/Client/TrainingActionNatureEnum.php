<?php

namespace App\Entity\Client;

enum TrainingActionNatureEnum: int
{
    case PRE_TRAINING_AND_PREPARATION_PROFESSIONAL_LIFE = 1;
    case ADAPT_AND_DEVELOP_EMPLOYEES = 2;
    case PROMOTE_GENDER_DIVERSITY = 3;
    case AIMED_AT_ACQUIRING_KNOWLEDGE = 4;
    case PROFESSIONAL_PROMOTION = 5;
    case PREVENTION = 6;
    case CONVERSION = 7;
    case RADIATION_PROJECTION = 8;
    case ECONOMY_AND_MANAGEMENT_OF_COMPANY = 9;
    case PROFIT_SAVING_AND_EMPLOYEE_SAVING = 10;
    case ACCOMPANIMENT_INFORMATION_AND_ADVISE = 11;
    case FIGHT_OF_ILLITERACY = 12;
    case BACK_OFFICE_MANAGERS = 13;
    public function label(): string
    {
        return match ($this) {
            TrainingActionNatureEnum::PRE_TRAINING_AND_PREPARATION_PROFESSIONAL_LIFE => 'Action of pre-training and preparation for working life',
            TrainingActionNatureEnum::ADAPT_AND_DEVELOP_EMPLOYEES => 'Action of adaptation and  skills development  for employees',
            TrainingActionNatureEnum::PROMOTE_GENDER_DIVERSITY => 'Action to promote  gender diversity in companies, awareness-raising on the fight against sexist stereotypes and professional equlity between women and men at work action',
            TrainingActionNatureEnum::AIMED_AT_ACQUIRING_KNOWLEDGE => 'Action of acquisition, maintenance or improvement of knowledge',
            TrainingActionNatureEnum::PROFESSIONAL_PROMOTION => 'Action to promote employment',
            TrainingActionNatureEnum::PREVENTION => 'Preventive action',
            TrainingActionNatureEnum::CONVERSION => 'Conversion action',
            TrainingActionNatureEnum::RADIATION_PROJECTION => 'Training action on radiation protection for professionals exposed to ionising radiation',
            TrainingActionNatureEnum::ECONOMY_AND_MANAGEMENT_OF_COMPANY => 'Training action on the economy and management of the company',
            TrainingActionNatureEnum::PROFIT_SAVING_AND_EMPLOYEE_SAVING => 'Training action on profit-sharing, participation and employee savings plans and employee share ownership schemes',
            TrainingActionNatureEnum::ACCOMPANIMENT_INFORMATION_AND_ADVISE => 'Action of support, information and advice provided to creators or buyers of agricultural, craft, commercial or liberal enterprises',
            TrainingActionNatureEnum::FIGHT_OF_ILLITERACY => 'Action to prevent illiteracy and to learn the French language',
            TrainingActionNatureEnum::BACK_OFFICE_MANAGERS => 'Training action related to the activity of Back-Office managers',
        };
    }
}
