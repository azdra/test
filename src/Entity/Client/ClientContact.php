<?php

namespace App\Entity\Client;

use App\Form\Client\Tab\ClientGeneralTabFormType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Embeddable]
class ClientContact
{
    #[ORM\Column(nullable: true)]
    #[
        Assert\Email(groups: [ClientGeneralTabFormType::FORM_NAME]),
        Assert\NotBlank(allowNull: true, groups: [ClientGeneralTabFormType::FORM_NAME]),
    ]
    private ?string $email = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientGeneralTabFormType::FORM_NAME])]
    private ?string $telephone = null;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): void
    {
        $this->telephone = $telephone;
    }
}
