<?php

namespace App\Entity\Client;

use App\Form\Client\Tab\ClientOptionsTabFormType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Embeddable]
class ClientSelfRegistering
{
    #[ORM\Column]
    #[Groups(['read_session', 'read_client'])]
    private bool $enabled = false;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientOptionsTabFormType::FORM_NAME])]
    private ?string $link = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientOptionsTabFormType::FORM_NAME])]
    private ?string $gdprInformation = null;

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getGdprInformation(): ?string
    {
        return $this->gdprInformation;
    }

    public function setGdprInformation(?string $gdprInformation): self
    {
        $this->gdprInformation = $gdprInformation;

        return $this;
    }
}
