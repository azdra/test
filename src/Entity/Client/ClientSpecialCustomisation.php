<?php

namespace App\Entity\Client;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Embeddable]
class ClientSpecialCustomisation
{
    #[ORM\Column]
    #[Groups(['read_client'])]
    private bool $enabledSalesForce = false;

    public function isEnabledSalesForce(): bool
    {
        return $this->enabledSalesForce;
    }

    public function setEnabledSalesForce(bool $enabledSalesForce): self
    {
        $this->enabledSalesForce = $enabledSalesForce;

        return $this;
    }
}
