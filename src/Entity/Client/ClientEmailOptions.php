<?php

namespace App\Entity\Client;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
class ClientEmailOptions
{
    #[ORM\Column]
    private bool $sendACancellationEmailToTrainees = true;

    #[ORM\Column]
    private bool $sendAnUnsubscribeEmailToParticipants = true;

    #[ORM\Column]
    private bool $sendAnAttestationAutomatically = false;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    private bool $sendPresenceReportAfterSession = false;

    public function isSendPresenceReportAfterSession(): bool
    {
        return $this->sendPresenceReportAfterSession;
    }

    public function setSendPresenceReportAfterSession(bool $sendPresenceReportAfterSession): self
    {
        $this->sendPresenceReportAfterSession = $sendPresenceReportAfterSession;

        return $this;
    }

    public function isSendACancellationEmailToTrainees(): bool
    {
        return $this->sendACancellationEmailToTrainees;
    }

    public function setSendACancellationEmailToTrainees(bool $sendACancellationEmailToTrainees): void
    {
        $this->sendACancellationEmailToTrainees = $sendACancellationEmailToTrainees;
    }

    public function isSendAnUnsubscribeEmailToParticipants(): bool
    {
        return $this->sendAnUnsubscribeEmailToParticipants;
    }

    public function setSendAnUnsubscribeEmailToParticipants(bool $sendAnUnsubscribeEmailToParticipants): void
    {
        $this->sendAnUnsubscribeEmailToParticipants = $sendAnUnsubscribeEmailToParticipants;
    }

    public function isSendAnAttestationAutomatically(): bool
    {
        return $this->sendAnAttestationAutomatically;
    }

    public function setSendAnAttestationAutomatically(bool $sendAnAttestationAutomatically): void
    {
        $this->sendAnAttestationAutomatically = $sendAnAttestationAutomatically;
    }
}
