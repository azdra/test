<?php

namespace App\Entity\Client;

use App\Entity\EvaluationProviderSession;
use App\Entity\SessionConvocationAttachment;
use App\Validator as AppAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class EvaluationModalMail
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_evaluation_modal'])]
    private int $id;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_evaluation_modal'])]
    private \DateTime $createdAt;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'evaluationsModalMail')]
    private Client $client;

    #[ORM\Column(type: 'string', nullable: false)]
    #[
        Assert\Choice(
            choices: [
                EvaluationProviderSession::EVALUATION_KNOWLEDGE,
                EvaluationProviderSession::EVALUATION_COLD,
                EvaluationProviderSession::EVALUATION_HOT,
                EvaluationProviderSession::EVALUATION_ACQUIRED,
                EvaluationProviderSession::EVALUATION_POSITIONING,
                EvaluationProviderSession::EVALUATION_OTHER,
            ])
    ]
    #[Groups(['read_evaluation_modal'])]
    private string $evaluationType;

    #[ORM\Column(type: 'string', nullable: false)]
    #[
        Assert\Choice(
            choices: [
                Client::FRENCH_LANGUAGE,
                Client::ENGLISH_LANGUAGE,
            ])
    ]
    #[Groups(['read_evaluation_modal'])]
    private string $language;

    #[ORM\Column(type: 'string', nullable: true)]
    #[AppAssert\ConstraintShortCode]
    #[Groups(['read_evaluation_modal'])]
    private ?string $subjectMail;

    #[ORM\Column(type: 'text', nullable: true)]
    #[AppAssert\ConstraintShortCode]
    #[Groups(['read_evaluation_modal'])]
    private ?string $content;

    #[ORM\OneToMany(mappedBy: 'evaluationModalMail', targetEntity: SessionConvocationAttachment::class, cascade: ['all'])]
    #[Groups(['read_evaluation_modal'])]
    protected Collection $attachments;

    public function __construct(Client $client, string $evaluationType, string $language = Client::FRENCH_LANGUAGE)
    {
        $this->client = $client;
        $this->language = $language;
        $this->evaluationType = $evaluationType;
        $this->createdAt = new \DateTime();
        $this->attachments = new ArrayCollection();
        $this->subjectMail = $this->getSubjectMailProposal();
        $this->content = $this->getContentMailProposal();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): EvaluationModalMail
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getEvaluationType(): string
    {
        return $this->evaluationType;
    }

    public function setEvaluationType(string $evaluationType): EvaluationModalMail
    {
        $this->evaluationType = $evaluationType;

        return $this;
    }

    public function getSubjectMail(): ?string
    {
        return $this->subjectMail;
    }

    public function setSubjectMail(?string $subjectMail): self
    {
        $this->subjectMail = $subjectMail;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function setAttachments(Collection $attachments): self
    {
        $this->attachments = $attachments;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): EvaluationModalMail
    {
        $this->client = $client;

        return $this;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function setLanguage(string $language): EvaluationModalMail
    {
        $this->language = $language;

        return $this;
    }

    public function getSubjectMailProposal(): string
    {
        if ($this->language === Client::ENGLISH_LANGUAGE) {
            return match ($this->evaluationType) {
                EvaluationProviderSession::EVALUATION_COLD => 'Initial Impression Evaluation of __name__ on __date_start__ from __hour_start__ to __hour_end__',
                EvaluationProviderSession::EVALUATION_ACQUIRED => 'Acquired Skills Assessment for __name__ - __date_start__ from __hour_start__ to __hour_end__',
                EvaluationProviderSession::EVALUATION_HOT => 'Immediate Feedback Request for __name__ Session on __date_start__ from __hour_start__ to __hour_end__',
                EvaluationProviderSession::EVALUATION_POSITIONING => 'Positioning Evaluation for __name__ - __date_start__ from __hour_start__ to __hour_end__',
                EvaluationProviderSession::EVALUATION_KNOWLEDGE => 'Knowledge Check for __name__ Session - __date_start__ from __hour_start__ to __hour_end__',
                EvaluationProviderSession::EVALUATION_OTHER => 'Special Evaluation for __name__ - __date_start__ from __hour_start__ to __hour_end__',
                default => 'General Session Evaluation for __name__ - __date_start__ from __hour_start__ to __hour_end__',
            };
        } else {
            return match ($this->evaluationType) {
                EvaluationProviderSession::EVALUATION_COLD => 'Évaluation d\'impression initiale de __name__ le __date_start__ de __hour_start__ à __hour_end__',
                EvaluationProviderSession::EVALUATION_ACQUIRED => 'Évaluation des compétences acquises pour __name__ - __date_start__ de __hour_start__ à __hour_end__',
                EvaluationProviderSession::EVALUATION_HOT => 'Demande de retour immédiat pour la session __name__ le __date_start__ de __hour_start__ à __hour_end__',
                EvaluationProviderSession::EVALUATION_POSITIONING => 'Évaluation de positionnement pour __name__ - __date_start__ de __hour_start__ à __hour_end__',
                EvaluationProviderSession::EVALUATION_KNOWLEDGE => 'Vérification des connaissances pour la session __name__ - __date_start__ de __hour_start__ à __hour_end__',
                EvaluationProviderSession::EVALUATION_OTHER => 'Évaluation spéciale pour __name__ - __date_start__ de __hour_start__ à __hour_end__',
                default => 'Évaluation générale de la session __name__ - __date_start__ de __hour_start__ à __hour_end__',
            };
        }
    }

    public function getContentMailProposal(): string
    {
        if ($this->language === Client::ENGLISH_LANGUAGE) {
            return match ($this->evaluationType) {
                EvaluationProviderSession::EVALUATION_COLD => '<p>Hello,</p><p>Your initial impression of the __name__ session is important to us. Help us evaluate your first experience on __date_start_day__ __date_start_month_text__ __date_start_year__.</p><p>Please fill out our "cold" evaluation survey.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments</p><p>Best regards.</p>',
                EvaluationProviderSession::EVALUATION_ACQUIRED => '<p>Hello,</p><p>We hope you found the __name__ session enriching. Your feedback on the skills and knowledge acquired is of interest to us.</p><p>Please take a moment to evaluate the skills acquired during this session.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments.</p><p>Best regards.</p>',
                EvaluationProviderSession::EVALUATION_HOT => '<p>Hello,</p><p>Thank you for participating in our dynamic __name__ session. Your immediate feedback is crucial to us.</p><p>Share your experience and immediate impressions via our "hot" survey.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44"/></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments.</p><p>Best regards.</p>',
                EvaluationProviderSession::EVALUATION_POSITIONING => '<p>Hello,</p><p>Your positioning in the __name__ session is a key element for us. Help us understand where you are.</p><p>Please fill out the positioning survey linked to this session.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments.</p><p>Best regards.</p>',
                EvaluationProviderSession::EVALUATION_KNOWLEDGE => '<p>Hello,</p><p>After participating in the __name__ session, we are curious to know your level of understanding and retention.</p><p>Please evaluate your assimilation of knowledge in our dedicated survey.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments.</p><p>Best regards.</p>',
                EvaluationProviderSession::EVALUATION_OTHER => '<p>Hello,</p><p>Your participation in the __name__ session was appreciated. For the specific needs of this session, we have a custom survey for you.</p><p>Please take the time to complete this specific survey.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments.</p><p>Best regards.</p>',
                default => '<p>Hello,</p><p>Thank you for participating in the __name__ session on __date_start_day__ __date_start_month_text__ __date_start_year__. We would appreciate your feedback to improve our sessions.</p><p>Please take a moment to fill out our evaluation survey.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44"/></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments.</p><p>Best regards.</p>',
            };
        } else {
            return match ($this->evaluationType) {
                EvaluationProviderSession::EVALUATION_COLD => '<p>Bonjour,</p><p>Votre impression initiale de la session __name__ est importante pour nous. Aidez-nous à évaluer votre première expérience le __date_start_day__ __date_start_month_text__ __date_start_year__.</p><p>Merci de remplir notre enquête d\'évaluation "à froid".</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
                EvaluationProviderSession::EVALUATION_ACQUIRED => '<p>Bonjour,</p><p>Nous espérons que vous avez trouvé la session __name__ enrichissante. Votre avis sur les compétences et connaissances acquises nous intéresse.</p><p>Merci de prendre un moment pour évaluer les compétences acquises lors de cette session.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
                EvaluationProviderSession::EVALUATION_HOT => '<p>Bonjour,</p><p>Merci d\'avoir participé à notre session dynamique __name__. Vos retours immédiats sont cruciaux pour nous.</p><p>Partagez votre expérience et vos impressions immédiates via notre enquête "à chaud".</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
                EvaluationProviderSession::EVALUATION_POSITIONING => '<p>Bonjour,</p><p>Votre positionnement dans la session __name__ est un élément clé pour nous. Aidez-nous à comprendre où vous vous situez.</p><p>Merci de remplir l\'enquête de positionnement liée à cette session.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
                EvaluationProviderSession::EVALUATION_KNOWLEDGE => '<p>Bonjour,</p><p>Après avoir participé à la session __name__, nous sommes curieux de connaître votre niveau de compréhension et de rétention.</p><p>Veuillez évaluer votre assimilation des connaissances dans notre enquête dédiée.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
                EvaluationProviderSession::EVALUATION_OTHER => '<p>Bonjour,</p><p>Votre participation à la session __name__ a été appréciée. Pour les besoins spécifiques de cette session, nous avons une enquête sur mesure pour vous.</p><p>Merci de prendre le temps de compléter cette enquête spécifique.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
                default => '<p>Bonjour,</p><p>Merci d\'avoir participé à la session __name__ le __date_start_day__ __date_start_month_text__ __date_start_year__. Nous apprécierions vos commentaires pour améliorer nos sessions.</p><p>Veuillez prendre un moment pour remplir notre enquête d\'évaluation.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
            };
        }
    }
}
