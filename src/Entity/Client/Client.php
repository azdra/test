<?php

namespace App\Entity;

namespace App\Entity\Client;

use App\Controller\Api\ClientApiController;
use App\CustomerService\Entity\HelpNeed;
use App\Entity\ActivityLog;
use App\Entity\AsyncTask;
use App\Entity\CategorySessionType;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Entity\Convocation;
use App\Entity\DeferredActionsTask;
use App\Entity\Evaluation;
use App\Entity\GroupParticipant;
use App\Entity\Guest;
use App\Entity\GuestUnsubscribe;
use App\Entity\User;
use App\Form\Client\Tab\ClientCustomizationTabFormType;
use App\Form\Client\Tab\ClientEmailTabFormType;
use App\Form\Client\Tab\ClientGeneralTabFormType;
use App\Form\Client\Tab\ClientOrganizationTabFormType;
use App\RegistrationPage\Entity\RegistrationPageTemplate;
use App\SelfSubscription\Entity\Module;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 * When VichUploader is updated and handle php attribute (next version)
 * #[Vich\Uploadable]
 */
#[ORM\Entity]
class Client
{
    public const ID_CLIENT_IFCAM = 6;
    public const TIPPING_DATE_CLIENT_IFCAM = '2023-12-31 23:59:59';
    public const ALIAS = 'client';

    public const TRANSPORTER_EMAIL_MANDRILL = 'mandrill';
    public const TRANSPORTER_EMAIL_IDFUSE = 'idfuse';
    public const TRANSPORTER_EMAIL_BREVO = 'brevo';
    public const TRANSPORTER_ALL = [
        self::TRANSPORTER_EMAIL_MANDRILL,
        self::TRANSPORTER_EMAIL_IDFUSE,
        self::TRANSPORTER_EMAIL_BREVO,
    ];

    public const CATEGORY_TRAINING = 'Virtual classroom';
    public const CATEGORY_MIXED = 'Blended training';
    public const CATEGORY_PRESENTIAL = 'face-to-face training';
    public const CATEGORY_MEETING = 'Meeting';
    public const CATEGORY_TEST = 'Test';
    public const CATEGORY_WEBINAR = 'Webinar';
    public const CATEGORY_ALL = [
        self::CATEGORY_TRAINING,
        self::CATEGORY_MIXED,
        self::CATEGORY_PRESENTIAL,
        self::CATEGORY_MEETING,
        self::CATEGORY_WEBINAR,
    ];
    public const SENDER_EMAIL_SUFFIX_MANDRILL = '@mylivesession.com';
    public const SENDER_EMAIL_SUFFIX_BREVO = '@mylivesession.app';

    public const NOTIFICATION_SESSION_WITHOUT_ANIMATOR = 'NotificationSessionWithoutAnimator';
    public const NOTIFICATION_SESSION_WITHOUT_TRAINEE = 'NotificationSessionWithoutTrainee';
    public const NOTIFICATION_SESSION_WITHOUT_ANIMATOR_OPTION_TIME_BEFORE = 'NotificationSessionWithoutAnimatorOptionTimeBefore';
    public const NOTIFICATION_SESSION_WITHOUT_TRAINEE_OPTION_TIME_BEFORE = 'NotificationSessionWithoutTraineeOptionTimeBefore';
    public const NOTIFICATION_ANIMATOR_ON_SEVERAL_SESSIONS = 'NotificationAnimatorOnSeveralSessions';
    public const NOTIFICATION_TRAINEE_ON_SEVERAL_SESSIONS = 'NotificationTraineeOnSeveralSessions';
    public const NOTIFICATION_NO_ANIMATOR_WITH_HOST_ON_SESSION = 'NotificationNoAnimatorWithHostOnSession';
    public const NOTIFICATION_NO_ANIMATOR_WITH_HOST_ON_SESSION_OPTION_TIME_BEFORE = 'NotificationNoAnimatorWithHostOnSessionOptionTimeBefore';
    public const NOTIFICATION_SEVERAL_ANIMATORS_ON_SAME_SESSION = 'NotificationSeveralAnimatorsOnSameSession';
    public const NOTIFICATION_SEVERAL_ANIMATORS_ON_SAME_SESSION_OPTION_TIME_BEFORE = 'NotificationSeveralAnimatorsOnSameSessionOptionTimeBefore';
    public const NOTIFICATION_NO_MORE_CREDIT = 'NotificationNoMoreCredit';
    public const NOTIFICATION_NO_MORE_CREDIT_OPTION_LIMIT = 'NotificationNoMoreCreditOptionLimit';

    public const NOTIFICATIONS_OPTIONS_ALL = [
        self::NOTIFICATION_SESSION_WITHOUT_ANIMATOR => true,
        self::NOTIFICATION_SESSION_WITHOUT_TRAINEE => true,
        self::NOTIFICATION_ANIMATOR_ON_SEVERAL_SESSIONS => true,
        self::NOTIFICATION_TRAINEE_ON_SEVERAL_SESSIONS => true,
        self::NOTIFICATION_NO_ANIMATOR_WITH_HOST_ON_SESSION => true,
        self::NOTIFICATION_NO_ANIMATOR_WITH_HOST_ON_SESSION_OPTION_TIME_BEFORE => 1,
        self::NOTIFICATION_SEVERAL_ANIMATORS_ON_SAME_SESSION => true,
        self::NOTIFICATION_SEVERAL_ANIMATORS_ON_SAME_SESSION_OPTION_TIME_BEFORE => 7,
        self::NOTIFICATION_SESSION_WITHOUT_ANIMATOR_OPTION_TIME_BEFORE => 7,
        self::NOTIFICATION_SESSION_WITHOUT_TRAINEE_OPTION_TIME_BEFORE => 7,
        self::NOTIFICATION_NO_MORE_CREDIT_OPTION_LIMIT => 10,
    ];

    public const FRENCH_LANGUAGE = 'fr';
    public const ENGLISH_LANGUAGE = 'en';

    public const LANGUAGES_LIST = [
        self::FRENCH_LANGUAGE, self::ENGLISH_LANGUAGE,
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups([
        'read_client', 'read_session', 'read_session_list', 'read_grouped_activities', ClientApiController::GROUP_VIEW,
        'filter:view', 'help_need:read', 'ultra:lite:read', 'read_groups',
    ])]
    protected int $id;

    #[ORM\Column(type: 'string')]
    #[Groups([
        'read_user', 'read_client', 'collection:vuejs', 'read_session', 'read_session_list', 'read_grouped_activities',
        'read_webex_user', 'read_webex_rest_user', 'read_microsoft_teams_user', 'read_microsoft_teams_event_user', 'read_white_connector_user', 'switch_user', ClientApiController::GROUP_VIEW, 'filter:view',
        'ui:module:read', 'help_need:read', 'read_adobe_user', 'ui:module:listing', 'ui:registration_page_template:read', 'read_session_calendar', 'ultra:lite:read', 'read_groups', 'ui:module:hub',
    ])]
    #[Assert\NotBlank(groups: [ClientGeneralTabFormType::FORM_NAME, 'ui:registration_page_template:read'])]
    protected string $name;

    #[ORM\Column(type: 'string')]
    #[Groups([
        'read_user', 'read_client', 'collection:vuejs', 'read_session', 'read_session_list', 'read_grouped_activities',
        'read_webex_user', 'read_webex_rest_user', 'read_microsoft_teams_user', 'read_microsoft_teams_event_user', 'read_white_connector_user', 'switch_user', ClientApiController::GROUP_VIEW, 'filter:view',
        'ui:module:read', 'help_need:read', 'read_adobe_user', 'ui:module:listing', 'ui:registration_page_template:read', 'read_session_calendar',
    ])]
    protected string $slug;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_client'])]
    private \DateTime $createdAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['read_client'])]
    private \DateTime $updatedAt;

    #[ORM\Column(type: 'string')]
    #[
        Assert\NotBlank(groups: ['create', 'update', ClientGeneralTabFormType::FORM_NAME]),
        Assert\Language(groups: [ClientGeneralTabFormType::FORM_NAME])
    ]
    #[Groups(['ui:edit:subject'])]
    private string $defaultLang = self::FRENCH_LANGUAGE;

    #[ORM\Column(type: 'string')]
    #[
        Assert\NotBlank(groups: ['create', 'update', ClientGeneralTabFormType::FORM_NAME]),
        Assert\Timezone(groups: [ClientGeneralTabFormType::FORM_NAME])
    ]
    #[Groups(['read_client', 'read_session'])]
    private string $timezone = 'Europe/Paris';

    #[ORM\Column(type: 'simple_array')]
    #[
        Assert\NotBlank(groups: ['create', 'update', ClientGeneralTabFormType::FORM_NAME]),
        Assert\Choice(choices: [...self::CATEGORY_ALL, self::CATEGORY_TEST], multiple: true, groups: [ClientGeneralTabFormType::FORM_NAME])
    ]
    #[Groups(['read_session', ClientApiController::GROUP_VIEW])]
    private array $categories = self::CATEGORY_ALL;

    #[Assert\Valid]
    #[ORM\Embedded(class: ClientContact::class)]
    private ClientContact $commercialContact;

    #[Assert\Valid]
    #[ORM\Embedded(class: ClientContact::class)]
    private ClientContact $customerContact;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientOrganizationTabFormType::FORM_NAME])]
    private ?string $organisationName = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientOrganizationTabFormType::FORM_NAME])]
    private ?string $organisationLegalRepresentative = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientOrganizationTabFormType::FORM_NAME])]
    private ?string $organisationFunctionLegalRepresentative = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientOrganizationTabFormType::FORM_NAME])]
    private ?string $organisationCompetentCommercialCourt = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['read_session'])]
    #[Assert\NotBlank(allowNull: true, groups: [ClientOrganizationTabFormType::FORM_NAME])]
    private ?TrainingActionNatureEnum $organisationDefaultTrainingActionNature = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientOrganizationTabFormType::FORM_NAME])]
    private ?string $organisationTrainingActivityRegistrationNumber = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientOrganizationTabFormType::FORM_NAME])]
    private ?string $organisationCity = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientOrganizationTabFormType::FORM_NAME])]
    private ?string $organisationDistrict = null;

    #[ORM\Column]
    private bool $enableAssessmentAchievement = false;

    #[ORM\Column]
    private bool $enableSharedEmail = false;

    #[ORM\Column]
    #[Groups(['read_session'])]
    private bool $enableSendEmail = true;

    #[ORM\Column]
    #[Groups(['read_session'])]
    private bool $enableRemoteSignature = false;

    #[ORM\Column(nullable: true)]
    #[Groups(['read_session'])]
    private ?bool $fillEmail = false;

    #[ORM\Column]
    private bool $enableAccessLinkOnExport = false;

    #[ORM\Column]
    private bool $enableSendToReferrer = false;

    #[ORM\Column]
    #[Groups(['read_client', 'read_session'])]
    private bool $enableRegistrationPage = false;

    #[Assert\Valid]
    #[ORM\Embedded(class: ClientSelfRegistering::class)]
    #[Groups(['read_session'])]
    private ClientSelfRegistering $selfRegistering;

    #[Assert\Valid]
    #[ORM\Embedded(class: ClientLicensesAvailable::class)]
    #[Groups(['read_session'])]
    private ClientLicensesAvailable $licensesAvailable;

    #[Assert\Valid]
    #[ORM\Embedded(class: ClientSpecialCustomisation::class)]
    #[Groups(['read_client'])]
    private ClientSpecialCustomisation $specialCustomisation;

    #[ORM\Column(nullable: true)]
    private ?string $logo = null;

    #[ORM\Column(type: 'json')]
    #[Groups(['read_client'])]
    private array $preferences = self::NOTIFICATIONS_OPTIONS_ALL;

    public const BILLING_MODE_SESSION = 0;
    public const BILLING_MODE_CREDIT = 1;
    public const BILLING_MODE_PARTICIPANT = 2;

    #[ORM\Column(options: ['default' => self::BILLING_MODE_SESSION])]
    #[Assert\Choice(choices: [self::BILLING_MODE_SESSION, self::BILLING_MODE_CREDIT, self::BILLING_MODE_PARTICIPANT])]
    #[Groups(['read_client'])]
    private int $billingMode = self::BILLING_MODE_SESSION;

    #[ORM\Column(options: ['default' => 0])]
    #[Groups(['read_client'])]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private int $allowedCredit = 0;

    #[ORM\Column(options: ['default' => 0])]
    #[Groups(['read_client'])]
    private int $currentSessionCount = 0;

    #[ORM\Column(options: ['default' => 0])]
    #[Groups(['read_client'])]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private int $allowedCreditParticipants = 0;

    #[ORM\Column(type: 'float', options: ['default' => 0])]
    #[Groups(['read_client', 'read_session'])]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private float $creditSms = 0;

    #[ORM\Column(options: ['default' => 0])]
    #[Groups(['read_client'])]
    private int $currentParticipantsCount = 0;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['read_client'])]
    private ?\DateTime $expirationDateCredits = null;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: CreditAllocation::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['read_client'])]
    private Collection $creditAllocations;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: CreditAllocationParticipants::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['read_client'])]
    private Collection $creditAllocationsParticipants;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: CreditAllocationSms::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['read_client'])]
    private Collection $creditAllocationsSms;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: GroupParticipant::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['read_session'])]
    private Collection $groups;

    /**
     * @Vich\UploadableField(mapping="client_logo", fileNameProperty="logo")
     * When VichUploader is updated and handle php attribute (next version)
     * #[Vich\UploadableField(mapping: 'client_logo', fileNameProperty: 'logo')]
     */
    private ?File $logoFile = null;

    #[ORM\Column(nullable: true)]
    private ?string $header = null;

    /**
     * @Vich\UploadableField(mapping="client_header", fileNameProperty="header")
     * When VichUploader is updated and handle php attribute (next version)
     * #[Vich\UploadableField(mapping: 'client_header', fileNameProperty: 'header')]
     */
    private ?File $headerFile = null;

    #[ORM\Column(nullable: true)]
    private ?string $footer = null;

    /**
     * @Vich\UploadableField(mapping="client_footer", fileNameProperty="footer")
     * When VichUploader is updated and handle php attribute (next version)
     * #[Vich\UploadableField(mapping: 'client_footer', fileNameProperty: 'footer')]
     */
    private ?File $footerFile = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientCustomizationTabFormType::FORM_NAME])]
    private ?string $organizationStamp = null;

    /**
     * @Vich\UploadableField(mapping="client_organization_stamp", fileNameProperty="organizationStamp")
     * When VichUploader is updated and handle php attribute (next version)
     * #[Vich\UploadableField(mapping: 'client_organization_stamp', fileNameProperty: 'organizationStamp')]
     */
    private ?File $organizationStampFile = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientCustomizationTabFormType::FORM_NAME])]
    private ?string $sessionImportModel = null;

    /**
     * @Vich\UploadableField(mapping="client_session_import_model", fileNameProperty="sessionImportModel")
     * When VichUploader is updated and handle php attribute (next version)
     * #[Vich\UploadableField(mapping: 'client_session_import_model', fileNameProperty: 'sessionImportModel')]
     */
    private ?File $sessionImportModelFile = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientCustomizationTabFormType::FORM_NAME])]
    private ?string $traineesImportModel = null;

    /**
     * @Vich\UploadableField(mapping="client_trainee_import_model", fileNameProperty="traineesImportModel")
     * When VichUploader is updated and handle php attribute (next version)
     * #[Vich\UploadableField(mapping: 'client_trainee_import_model', fileNameProperty: 'traineesImportModel')]
     */
    private ?File $traineesImportModelFile = null;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientCustomizationTabFormType::FORM_NAME])]
    private ?string $closedRoomMessage = null;

    #[ORM\Column(type: 'string', length: 7, nullable: true)]
    #[Groups(['read_client', 'help_need:read'])]
    #[
        Assert\NotBlank(allowNull: true, groups: [ClientCustomizationTabFormType::FORM_NAME]),
        Assert\Regex(pattern: '/^#[a-f0-9]{6}$/i', groups: [ClientCustomizationTabFormType::FORM_NAME])
    ]
    private ?string $color = '#3289c7';

    #[ORM\Column(nullable: true)]
    #[
        Assert\NotBlank(allowNull: true, groups: [ClientEmailTabFormType::FORM_NAME]),
        Assert\Choice(choices: [...self::TRANSPORTER_ALL, self::TRANSPORTER_EMAIL_MANDRILL], multiple: false, groups: [ClientEmailTabFormType::FORM_NAME])
    ]
    #[Groups(['read_session'])]
    private ?string $senderTransporter = self::TRANSPORTER_EMAIL_MANDRILL;

    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientEmailTabFormType::FORM_NAME])]
    #[Groups(['read_session'])]
    private ?string $senderName = null;

    #[ORM\Column(nullable: true)]
    #[
        Assert\NotBlank(allowNull: true, groups: [ClientEmailTabFormType::FORM_NAME]),
        Assert\Email(groups: [ClientEmailTabFormType::FORM_NAME])
    ]
    #[Groups(['read_session'])]
    private ?string $senderEmail = null;

    #[ORM\Column(nullable: true)]
    #[
        Assert\NotBlank(allowNull: true, groups: [ClientEmailTabFormType::FORM_NAME]),
        Assert\Email(groups: [ClientEmailTabFormType::FORM_NAME]),
    ]
    private ?string $replayToEmail = null;

    #[Assert\Valid]
    #[ORM\Embedded(class: ClientEmailScheduling::class)]
    private ClientEmailScheduling $emailScheduling;

    #[Assert\Valid]
    #[ORM\Embedded(class: ClientEmailOptions::class)]
    private ClientEmailOptions $emailOptions;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: Convocation::class, cascade: ['all'])]
    #[Groups(['read_session', 'read_configuration_holder', ClientApiController::GROUP_VIEW])]
    private Collection $convocations;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: EvaluationModalMail::class, cascade: ['persist'])]
    protected Collection $evaluationsModalMail;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: AbstractProviderConfigurationHolder::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['read_configuration_holder', ClientApiController::GROUP_VIEW, 'read_user', 'read_session_list', 'read_client'])]
    private Collection $configurationHolders;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read_client'])]
    private bool $active = true;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: User::class, cascade: ['all'])]
    private Collection $users;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: Guest::class, cascade: ['all'])]
    private Collection $guests;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: GuestUnsubscribe::class, cascade: ['all'])]
    private Collection $guestUnsubscribe;

    #[ORM\OneToMany(mappedBy: 'clientOrigin', targetEntity: AsyncTask::class, cascade: ['all'])]
    private Collection $asyncTasks;

    #[ORM\OneToMany(mappedBy: 'clientOrigin', targetEntity: DeferredActionsTask::class, cascade: ['all'])]
    private Collection $deferredActionsTasks;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: ActivityLog::class, cascade: ['all'])]
    private Collection $activityLogs;

    #[ORM\OneToOne(targetEntity: ClientServices::class, cascade: ['all'])]
    #[Groups(['filter:view', 'ui:registration_page_template:read'])]
    private ClientServices $services;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: Evaluation::class, cascade: ['all'])]
    #[Groups(['read_session', 'read_client'])]
    private Collection $evaluations;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: HelpNeed::class, cascade: ['all'])]
    private Collection $helpNeeds;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: Module::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['read_client'])]
    private Collection $selfSubscriptionModules;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: RegistrationPageTemplate::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['read_client', 'read_session'])]
    private Collection $registrationPageTemplates;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->commercialContact = new ClientContact();
        $this->customerContact = new ClientContact();
        $this->selfRegistering = new ClientSelfRegistering();
        $this->licensesAvailable = new ClientLicensesAvailable();
        $this->specialCustomisation = new ClientSpecialCustomisation();
        $this->emailScheduling = new ClientEmailScheduling();
        $this->emailOptions = new ClientEmailOptions();
        $this->convocations = new ArrayCollection();
        $this->configurationHolders = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->asyncTasks = new ArrayCollection();
        $this->deferredActionsTasks = new ArrayCollection();
        $this->activityLogs = new ArrayCollection();
        $this->evaluations = new ArrayCollection();
        $this->services = new ClientServices();
        $this->creditAllocations = new ArrayCollection();
        $this->creditAllocationsParticipants = new ArrayCollection();
        $this->creditAllocationsSms = new ArrayCollection();
        $this->helpNeeds = new ArrayCollection();
        $this->selfSubscriptionModules = new ArrayCollection();
        $this->registrationPageTemplates = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Client
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Client
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): Client
    {
        $this->slug = $slug;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): Client
    {
        $this->color = $color;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): Client
    {
        $this->active = $active;

        return $this;
    }

    public function getDefaultLang(): string
    {
        return $this->defaultLang;
    }

    public function setDefaultLang(string $defaultLang): self
    {
        $this->defaultLang = $defaultLang;

        return $this;
    }

    public function getTimezone(): string
    {
        return $this->timezone;
    }

    public function setTimezone(string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    #[Groups(['read_session'])]
    public function getTimezoneWithGMTInfos(): string
    {
        $tz = new \DateTimeZone($this->timezone);
        $offset = $tz->getOffset(new \DateTime());
        $offsetHours = floor(abs($offset) / 3600);
        $offsetMinutes = abs($offset) % 3600 / 60;
        $offsetSign = ($offset < 0) ? '-' : '+';
        $offsetFormatted = sprintf('GMT %s%02d:%02d', $offsetSign, $offsetHours, $offsetMinutes);

        return sprintf('%s (%s)', $this->timezone, $offsetFormatted);
    }

    public function getCategories(): array
    {
        return $this->categories;
    }

    public function setCategories(array $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function getCommercialContact(): ClientContact
    {
        return $this->commercialContact;
    }

    public function setCommercialContact(ClientContact $commercialContact): self
    {
        $this->commercialContact = $commercialContact;

        return $this;
    }

    public function getExpirationDateCredits(): ?\DateTime
    {
        return $this->expirationDateCredits;
    }

    public function setExpirationDateCredits(?\DateTime $expirationDateCredits): self
    {
        $this->expirationDateCredits = $expirationDateCredits;

        return $this;
    }

    public function getCustomerContact(): ClientContact
    {
        return $this->customerContact;
    }

    public function setCustomerContact(ClientContact $customerContact): self
    {
        $this->customerContact = $customerContact;

        return $this;
    }

    public function getOrganisationName(): ?string
    {
        return $this->organisationName;
    }

    public function setOrganisationName(?string $organisationName): void
    {
        $this->organisationName = $organisationName;
    }

    public function getOrganisationLegalRepresentative(): ?string
    {
        return $this->organisationLegalRepresentative;
    }

    public function setOrganisationLegalRepresentative(?string $organisationLegalRepresentative): void
    {
        $this->organisationLegalRepresentative = $organisationLegalRepresentative;
    }

    public function getOrganisationFunctionLegalRepresentative(): ?string
    {
        return $this->organisationFunctionLegalRepresentative;
    }

    public function setOrganisationFunctionLegalRepresentative(?string $organisationFunctionLegalRepresentative): void
    {
        $this->organisationFunctionLegalRepresentative = $organisationFunctionLegalRepresentative;
    }

    public function getOrganisationCompetentCommercialCourt(): ?string
    {
        return $this->organisationCompetentCommercialCourt;
    }

    public function setOrganisationCompetentCommercialCourt(?string $organisationCompetentCommercialCourt): void
    {
        $this->organisationCompetentCommercialCourt = $organisationCompetentCommercialCourt;
    }

    public function getOrganisationDefaultTrainingActionNature(): ?TrainingActionNatureEnum
    {
        return $this->organisationDefaultTrainingActionNature;
    }

    public function setOrganisationDefaultTrainingActionNature(?TrainingActionNatureEnum $organisationDefaultTrainingActionNature): void
    {
        $this->organisationDefaultTrainingActionNature = $organisationDefaultTrainingActionNature;
    }

    public function getOrganisationTrainingActivityRegistrationNumber(): ?string
    {
        return $this->organisationTrainingActivityRegistrationNumber;
    }

    public function setOrganisationTrainingActivityRegistrationNumber(
        ?string $organisationTrainingActivityRegistrationNumber
    ): void {
        $this->organisationTrainingActivityRegistrationNumber = $organisationTrainingActivityRegistrationNumber;
    }

    public function getOrganisationCity(): ?string
    {
        return $this->organisationCity;
    }

    public function setOrganisationCity(?string $organisationCity): void
    {
        $this->organisationCity = $organisationCity;
    }

    public function getOrganisationDistrict(): ?string
    {
        return $this->organisationDistrict;
    }

    public function setOrganisationDistrict(?string $organisationDistrict): void
    {
        $this->organisationDistrict = $organisationDistrict;
    }

    public function isEnableAssessmentAchievement(): bool
    {
        return $this->enableAssessmentAchievement;
    }

    public function setEnableAssessmentAchievement(bool $enableAssessmentAchievement): self
    {
        $this->enableAssessmentAchievement = $enableAssessmentAchievement;

        return $this;
    }

    public function isEnableSharedEmail(): bool
    {
        return $this->enableSharedEmail;
    }

    public function setEnableSharedEmail(bool $enableSharedEmail): self
    {
        $this->enableSharedEmail = $enableSharedEmail;

        return $this;
    }

    public function isEnableSendEmail(): bool
    {
        return $this->enableSendEmail;
    }

    public function setEnableSendEmail(bool $enableSendEmail): self
    {
        $this->enableSendEmail = $enableSendEmail;

        return $this;
    }

    public function isEnableRemoteSignature(): bool
    {
        return $this->enableRemoteSignature;
    }

    public function setEnableRemoteSignature(bool $enableRemoteSignature): self
    {
        $this->enableRemoteSignature = $enableRemoteSignature;

        return $this;
    }

    public function isEnableAccessLinkOnExport(): bool
    {
        return $this->enableAccessLinkOnExport;
    }

    public function setEnableAccessLinkOnExport(bool $enableAccessLinkOnExport): self
    {
        $this->enableAccessLinkOnExport = $enableAccessLinkOnExport;

        return $this;
    }

    public function getSelfRegistering(): ClientSelfRegistering
    {
        return $this->selfRegistering;
    }

    public function setSelfRegistering(ClientSelfRegistering $selfRegistering): self
    {
        $this->selfRegistering = $selfRegistering;

        return $this;
    }

    public function getLicensesAvailable(): ClientLicensesAvailable
    {
        return $this->licensesAvailable;
    }

    public function setLicensesAvailable(ClientLicensesAvailable $licensesAvailable): self
    {
        $this->licensesAvailable = $licensesAvailable;

        return $this;
    }

    public function getSpecialCustomisation(): ClientSpecialCustomisation
    {
        return $this->specialCustomisation;
    }

    public function setSpecialCustomisation(ClientSpecialCustomisation $specialCustomisation): self
    {
        $this->specialCustomisation = $specialCustomisation;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): void
    {
        $this->logo = $logo;
    }

    public function getLogoFile(): ?File
    {
        return $this->logoFile;
    }

    public function setLogoFile(?File $logoFile): void
    {
        $this->logoFile = $logoFile;

        if (null !== $logoFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getHeader(): ?string
    {
        return $this->header;
    }

    public function setHeader(?string $header): void
    {
        $this->header = $header;
    }

    public function getHeaderFile(): ?File
    {
        return $this->headerFile;
    }

    public function setHeaderFile(?File $headerFile): void
    {
        $this->headerFile = $headerFile;

        if (null !== $headerFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getFooter(): ?string
    {
        return $this->footer;
    }

    public function setFooter(?string $footer): void
    {
        $this->footer = $footer;
    }

    public function setFooterFile(?File $footerFile = null): void
    {
        $this->footerFile = $footerFile;

        if (null !== $footerFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getFooterFile(): ?File
    {
        return $this->footerFile;
    }

    public function getOrganizationStamp(): ?string
    {
        return $this->organizationStamp;
    }

    public function setOrganizationStamp(?string $organizationStamp): void
    {
        $this->organizationStamp = $organizationStamp;
    }

    public function getOrganizationStampFile(): ?File
    {
        return $this->organizationStampFile;
    }

    public function setOrganizationStampFile(?File $organizationStampFile): void
    {
        $this->organizationStampFile = $organizationStampFile;

        if (null !== $organizationStampFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getSessionImportModel(): ?string
    {
        return $this->sessionImportModel;
    }

    public function setSessionImportModel(?string $sessionImportModel): void
    {
        $this->sessionImportModel = $sessionImportModel;
    }

    public function getSessionImportModelFile(): ?File
    {
        return $this->sessionImportModelFile;
    }

    public function setSessionImportModelFile(?File $sessionImportModelFile): void
    {
        $this->sessionImportModelFile = $sessionImportModelFile;

        if (null !== $sessionImportModelFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getTraineesImportModel(): ?string
    {
        return $this->traineesImportModel;
    }

    public function setTraineesImportModel(?string $traineesImportModel): void
    {
        $this->traineesImportModel = $traineesImportModel;
    }

    public function getTraineesImportModelFile(): ?File
    {
        return $this->traineesImportModelFile;
    }

    public function setTraineesImportModelFile(?File $traineesImportModelFile): void
    {
        $this->traineesImportModelFile = $traineesImportModelFile;

        if (null !== $traineesImportModelFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getClosedRoomMessage(): ?string
    {
        return $this->closedRoomMessage;
    }

    public function setClosedRoomMessage(?string $closedRoomMessage): void
    {
        $this->closedRoomMessage = $closedRoomMessage;
    }

    public function getSenderTransporter(): ?string
    {
        return $this->senderTransporter;
    }

    public function setSenderTransporter(?string $senderTransporter): void
    {
        $this->senderTransporter = $senderTransporter;
    }

    public function getSenderName(): ?string
    {
        return $this->senderName;
    }

    public function setSenderName(?string $senderName): void
    {
        $this->senderName = $senderName;
    }

    public function getSenderEmail(): ?string
    {
        return $this->senderEmail;
    }

    public function setSenderEmail(?string $senderEmail): void
    {
        $this->senderEmail = $senderEmail;
    }

    public function getReplayToEmail(): ?string
    {
        return $this->replayToEmail;
    }

    public function setReplayToEmail(?string $replayToEmail): void
    {
        $this->replayToEmail = $replayToEmail;
    }

    public function getEmailOptions(): ClientEmailOptions
    {
        return $this->emailOptions;
    }

    public function setEmailOptions(ClientEmailOptions $emailOptions): void
    {
        $this->emailOptions = $emailOptions;
    }

    public function getEmailScheduling(): ClientEmailScheduling
    {
        return $this->emailScheduling;
    }

    public function setEmailScheduling(ClientEmailScheduling $emailScheduling): Client
    {
        $this->emailScheduling = $emailScheduling;

        return $this;
    }

    public function getConvocations(): Collection
    {
        return $this->convocations;
    }

    public function setConvocations(Collection $convocations): void
    {
        $this->convocations = $convocations;
    }

    public function getConfigurationHolders(): Collection
    {
        return $this->configurationHolders;
    }

    public function setConfigurationHolders(Collection $configurationHolders): self
    {
        $this->configurationHolders = $configurationHolders;

        return $this;
    }

    public function addConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): self
    {
        if (!$this->configurationHolders->contains($configurationHolder)) {
            $configurationHolder->setClient($this);
            $this->configurationHolders->add($configurationHolder);
        }

        return $this;
    }

    public function getWebexConfigurationHolders(): Collection
    {
        return $this->getConfigurationHolders()
            ->filter(fn (AbstractProviderConfigurationHolder $configurationHolder) => $configurationHolder instanceof WebexConfigurationHolder);
    }

    public function getWebexRestConfigurationHolders(): Collection
    {
        return $this->getConfigurationHolders()
            ->filter(fn (AbstractProviderConfigurationHolder $configurationHolder) => $configurationHolder instanceof WebexRestConfigurationHolder);
    }

    public function getAdobeConfigurationHolders(): Collection
    {
        return $this->getConfigurationHolders()
            ->filter(fn (AbstractProviderConfigurationHolder $configurationHolder) => $configurationHolder instanceof AdobeConnectConfigurationHolder);
    }

    public function getMicrosoftTeamsConfigurationHolders(): Collection
    {
        return $this->getConfigurationHolders()
            ->filter(fn (AbstractProviderConfigurationHolder $configurationHolder) => $configurationHolder instanceof MicrosoftTeamsConfigurationHolder);
    }

    public function getMicrosoftTeamsEventConfigurationHolders(): Collection
    {
        return $this->getConfigurationHolders()
            ->filter(fn (AbstractProviderConfigurationHolder $configurationHolder) => $configurationHolder instanceof MicrosoftTeamsEventConfigurationHolder);
    }

    public function getWhiteConfigurationHolders(): Collection
    {
        return $this->getConfigurationHolders()
            ->filter(fn (AbstractProviderConfigurationHolder $configurationHolder) => $configurationHolder instanceof WhiteConfigurationHolder);
    }

    public function getAsyncTasks(): Collection
    {
        return $this->asyncTasks;
    }

    public function getDeferredActionsTasks(): Collection
    {
        return $this->deferredActionsTasks;
    }

    public function getActivityLogs(): Collection
    {
        return $this->activityLogs;
    }

    public function getUsers(): ArrayCollection|Collection
    {
        return $this->users;
    }

    public function setUsers(ArrayCollection|Collection $users): Client
    {
        $this->users = $users;

        return $this;
    }

    public function getGuests(): Collection
    {
        return $this->guests;
    }

    public function setGuests(Collection $guests): self
    {
        $this->guests = $guests;

        return $this;
    }

    public function addUser(User $user): Client
    {
        $this->users->add($user);

        return $this;
    }

    public function removeUser(User $user): Client
    {
        $this->users->removeElement($user);

        return $this;
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    public function getServices(): ClientServices
    {
        return $this->services;
    }

    public function setServices(ClientServices $services): Client
    {
        $this->services = $services;

        return $this;
    }

    public function getEvaluations(): Collection
    {
        return $this->evaluations;
    }

    public function setEvaluations(Collection $evaluations): self
    {
        $this->evaluations = $evaluations;

        return $this;
    }

    public function getPreferences(): array
    {
        return $this->preferences;
    }

    public function setPreferences(array $preferences): Client
    {
        $this->preferences = $preferences;

        return $this;
    }

    public function cleanUploadedFile(): void
    {
        $this->footerFile = null;
        $this->headerFile = null;
        $this->logoFile = null;
        $this->organizationStampFile = null;
        $this->sessionImportModelFile = null;
        $this->traineesImportModelFile = null;
    }

    /**
     * Return default true.
     */
    public function isSubscribeToAlert(string $typeAlert): bool
    {
        return !array_key_exists($typeAlert, $this->preferences) || $this->preferences[$typeAlert];
    }

    public function getOptionFromAlert(string $typeOptionAlert): mixed
    {
        if (array_key_exists($typeOptionAlert, $this->preferences)) {
            $value = $this->preferences[$typeOptionAlert];
        } else {
            $value = self::NOTIFICATIONS_OPTIONS_ALL[$typeOptionAlert];
        }

        return $value;
    }

    public function getBillingMode(): int
    {
        return $this->billingMode;
    }

    public function setBillingMode(int $billingMode): self
    {
        $this->billingMode = $billingMode;

        return $this;
    }

    public function getAllowedCredit(): int
    {
        return $this->allowedCredit;
    }

    public function setAllowedCredit(int $allowedCredit): self
    {
        $this->allowedCredit = $allowedCredit;

        return $this;
    }

    public function addAllowedCredit(int $allowedCredit): self
    {
        $this->allowedCredit += $allowedCredit;

        return $this;
    }

    public function addAllowedCreditParticipants(int $allowedCreditParticipants): self
    {
        $this->allowedCreditParticipants += $allowedCreditParticipants;

        return $this;
    }

    public function getCurrentSessionCount(): int
    {
        return $this->currentSessionCount;
    }

    public function setCurrentSessionCount(int $currentSessionCount): self
    {
        $this->currentSessionCount = $currentSessionCount;

        return $this;
    }

    public function incrementSessionCount(): self
    {
        ++$this->currentSessionCount;

        return $this;
    }

    public function decrementSessionCount(): self
    {
        if ($this->currentSessionCount > 0) {
            --$this->currentSessionCount;
        }

        return $this;
    }

    public function decrementCreditClient(int $quantity): void
    {
        switch ($this->getBillingMode()) {
            case Client::BILLING_MODE_CREDIT:
                $this->decrementSessionCount();
                break;
            case Client::BILLING_MODE_PARTICIPANT:
                $this->decrementParticipantCountWithQuantity($quantity);
                break;
        }
    }

    public function incrementParticipantCount(): self
    {
        ++$this->currentParticipantsCount;

        return $this;
    }

    public function incrementParticipantCountWithQuantity(int $quantity): self
    {
        $this->currentParticipantsCount = $this->currentParticipantsCount + $quantity;

        return $this;
    }

    public function decrementParticipantCount(): self
    {
        if ($this->currentParticipantsCount > 0) {
            --$this->currentParticipantsCount;
        }

        return $this;
    }

    public function decrementParticipantCountWithQuantity(int $quantity): self
    {
        if ($this->currentParticipantsCount > 0) {
            $this->currentParticipantsCount = $this->currentParticipantsCount - $quantity;
        }

        return $this;
    }

    #[Groups(['read_client'])]
    public function getRemainingCredit(): int
    {
        return $this->allowedCredit - $this->currentSessionCount;
    }

    #[Groups(['read_client'])]
    public function getRemainingCreditParticipants(): int
    {
        return $this->allowedCreditParticipants - $this->currentParticipantsCount;
    }

    public function getCreditAllocations(): Collection
    {
        return $this->creditAllocations;
    }

    public function setCreditAllocations(Collection $creditAllocations): self
    {
        $this->creditAllocations = $creditAllocations;

        return $this;
    }

    public function getCreditAllocationsParticipants(): Collection
    {
        return $this->creditAllocationsParticipants;
    }

    public function setCreditAllocationsParticipants(Collection $creditAllocationsParticipants): self
    {
        $this->creditAllocationsParticipants = $creditAllocationsParticipants;

        return $this;
    }

    public function getAllowedCreditParticipants(): int
    {
        return $this->allowedCreditParticipants;
    }

    public function setAllowedCreditParticipants(int $allowedCreditParticipants): self
    {
        $this->allowedCreditParticipants = $allowedCreditParticipants;

        return $this;
    }

    public function getCurrentParticipantsCount(): int
    {
        return $this->currentParticipantsCount;
    }

    public function setCurrentParticipantsCount(int $currentParticipantsCount): self
    {
        $this->currentParticipantsCount = $currentParticipantsCount;

        return $this;
    }

    public function getHelpNeeds(): ArrayCollection|Collection
    {
        return $this->helpNeeds;
    }

    public function setHelpNeeds(ArrayCollection|Collection $helpNeeds): self
    {
        $this->helpNeeds = $helpNeeds;

        return $this;
    }

    public function isEnableSendToReferrer(): bool
    {
        return $this->enableSendToReferrer;
    }

    public function setEnableSendToReferrer(bool $enableSendToReferrer): void
    {
        $this->enableSendToReferrer = $enableSendToReferrer;
    }

    public function isEnableRegistrationPage(): bool
    {
        return $this->enableRegistrationPage;
    }

    public function setEnableRegistrationPage(bool $enableRegistrationPage): void
    {
        $this->enableRegistrationPage = $enableRegistrationPage;
    }

    public function isFillEmail(): ?bool
    {
        return $this->fillEmail;
    }

    public function setFillEmail(?bool $fillEmail): self
    {
        $this->fillEmail = $fillEmail;

        return $this;
    }

    public function getManagerUsers(): array
    {
        $usersManager = [];

        foreach ($this->getUsers() as $userManager) {
            if (in_array(User::ROLE_MANAGER, $userManager->getRoles())) {
                $usersManager[$userManager->getId()] = $userManager;
            }
        }

        return $usersManager;
    }

    public function getSelfSubscriptionModules(): ArrayCollection|Collection
    {
        return $this->selfSubscriptionModules;
    }

    public function setSelfSubscriptionModules(ArrayCollection|Collection $selfSubscriptionModules): self
    {
        $this->selfSubscriptionModules = $selfSubscriptionModules;

        return $this;
    }

    public function addSelfSubscriptionModule(Module $selfSubscriptionModule): self
    {
        if (!$this->selfSubscriptionModules->contains($selfSubscriptionModule)) {
            $this->selfSubscriptionModules->add($selfSubscriptionModule);
        }

        return $this;
    }

    public function removeSelfSubscriptionModule(Module $selfSubscriptionModule): self
    {
        if ($this->selfSubscriptionModules->contains($selfSubscriptionModule)) {
            $this->selfSubscriptionModules->removeElement($selfSubscriptionModule);
        }

        return $this;
    }

    public function getRegistrationPageTemplates(): ArrayCollection|Collection
    {
        return $this->registrationPageTemplates;
    }

    public function setRegistrationPageTemplates(ArrayCollection|Collection $registrationPageTemplates): self
    {
        $this->registrationPageTemplates = $registrationPageTemplates;

        return $this;
    }

    public function addRegistrationPageTemplate(Module $registrationPageTemplate): self
    {
        if (!$this->registrationPageTemplates->contains($registrationPageTemplate)) {
            $this->registrationPageTemplates->add($registrationPageTemplate);
        }

        return $this;
    }

    public function removeRegistrationPageTemplate(Module $registrationPageTemplate): self
    {
        if ($this->registrationPageTemplates->contains($registrationPageTemplate)) {
            $this->registrationPageTemplates->removeElement($registrationPageTemplate);
        }

        return $this;
    }

    public function getGuestUnsubscribe(): Collection
    {
        return $this->guestUnsubscribe;
    }

    public function setGuestUnsubscribe(Collection $guestUnsubscribe): self
    {
        $this->guestUnsubscribe = $guestUnsubscribe;

        return $this;
    }

    public function categoryAvailable(CategorySessionType $categoryId): bool
    {
        return match ($categoryId) {
            CategorySessionType::CATEGORY_SESSION_FORMATION => in_array(self::CATEGORY_TRAINING, $this->categories),
            CategorySessionType::CATEGORY_SESSION_REUNION => in_array(self::CATEGORY_MEETING, $this->categories),
            CategorySessionType::CATEGORY_SESSION_PRESENTIAL => in_array(self::CATEGORY_PRESENTIAL, $this->categories),
            CategorySessionType::CATEGORY_SESSION_MIXTE => in_array(self::CATEGORY_MIXED, $this->categories),
            CategorySessionType::CATEGORY_SESSION_WEBINAR => in_array(self::CATEGORY_WEBINAR, $this->categories),
            default => false,
        };
    }

    public function getFirstConfigurationHolderActive(): ?AbstractProviderConfigurationHolder
    {
        if ($this->getConfigurationHoldersActive()->count() > 0) {
            return $this->getConfigurationHoldersActive()->first();
        }

        return null;
    }

    public function getConfigurationHoldersActive(): Collection
    {
        return $this->configurationHolders->filter(function ($configurationHolder) {
            return $configurationHolder->isArchived() === false;
        });
    }

    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function setGroups(Collection $groups): Client
    {
        $this->groups = $groups;

        return $this;
    }

    public function getEvaluationsModalMail(): Collection
    {
        return $this->evaluationsModalMail;
    }

    public function setEvaluationsModalMail(Collection $evaluationsModalMail): Client
    {
        $this->evaluationsModalMail = $evaluationsModalMail;

        return $this;
    }

    public function getCreditSms(): float
    {
        return $this->creditSms;
    }

    public function setCreditSms(float $allowedCreditSms): void
    {
        $this->creditSms = $allowedCreditSms;
    }

    public function addCreditSms(int $allowedCreditSms): self
    {
        $this->creditSms += $allowedCreditSms;

        return $this;
    }

    public function decrementCreditsSms(int $quantity): self
    {
        $this->creditSms -= $quantity;

        return $this;
    }

    public function getCreditAllocationsSms(): Collection
    {
        return $this->creditAllocationsSms;
    }

    public function setCreditAllocationsSms(Collection $creditAllocationsSms): void
    {
        $this->creditAllocationsSms = $creditAllocationsSms;
    }
}
