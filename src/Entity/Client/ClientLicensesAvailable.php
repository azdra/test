<?php

namespace App\Entity\Client;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Embeddable]
class ClientLicensesAvailable
{
    #[ORM\Column]
    #[Groups(['read_client'])]
    private bool $enabled = false;

    #[ORM\Column(nullable: true)]
    #[Groups(['read_client'])]
    private ?int $thresholdMiddle;

    #[ORM\Column(nullable: true)]
    #[Groups(['read_client'])]
    private ?int $thresholdMin;

    #[ORM\Column(nullable: true)]
    #[Groups(['read_client'])]
    private ?array $licensesReportRecipients = null;

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getThresholdMiddle(): ?int
    {
        return $this->thresholdMiddle;
    }

    public function setThresholdMiddle(?int $thresholdMiddle): self
    {
        $this->thresholdMiddle = $thresholdMiddle;

        return $this;
    }

    public function getThresholdMin(): ?int
    {
        return $this->thresholdMin;
    }

    public function setThresholdMin(?int $thresholdMin): self
    {
        $this->thresholdMin = $thresholdMin;

        return $this;
    }

    public function getLicensesReportRecipients(): ?array
    {
        return $this->licensesReportRecipients;
    }

    public function setLicensesReportRecipients(?array $licensesReportRecipients): self
    {
        $this->licensesReportRecipients = $licensesReportRecipients;

        return $this;
    }
}
