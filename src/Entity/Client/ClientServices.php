<?php

namespace App\Entity\Client;

use App\Entity\ServiceSchedule;
use App\Form\Client\Tab\ClientServicesTabFormType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class ClientServices
{
    public const ENUM_NO = 0;
    public const ENUM_ON_DEMAND = 1;
    public const ENUM_FULL = 2;

    public const ENUM_SUPPORT_NO = 0;
    public const ENUM_SUPPORT_ON_DEMAND = 1;
    public const ENUM_SUPPORT_FULL = 2;
    public const ENUM_SUPPORT_LAUNCH = 3;

    public const ENUM_SUPPORT_FOR_START_SESSION = 0;
    public const ENUM_SUPPORT_FOR_ALL_SESSION = 1;

    public const SUPPORT_DURATION_DEFAULT = 30;
    public const SUPPORT_START_DEFAULT = 30;

    public const ASSISTANCE_DURATION_DEFAULT = 30;
    public const ASSISTANCE_START_DEFAULT = 30;

    public const ASSISTANCE_PEDA_DEFAULT_END_DURATION = 30; // in minutes

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'integer')]
    #[
        Assert\NotBlank(
            groups: [ClientServicesTabFormType::FORM_NAME]
        ),
        Assert\Choice(
            choices: [
                self::ENUM_NO,
                self::ENUM_ON_DEMAND,
                self::ENUM_FULL,
            ],
            groups: [ClientServicesTabFormType::FORM_NAME]
        )
    ]
    #[Groups(['read_client_services'])]
    private ?int $assistanceType = self::ENUM_NO;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[
        Assert\NotBlank(
            groups: [ClientServicesTabFormType::FORM_NAME]
        ),
        Assert\Type('integer'),
        Assert\Range(min: 0, max: 60)
    ]
    #[Groups(['read_client_services'])]
    private ?int $assistanceDuration = self::ASSISTANCE_DURATION_DEFAULT;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[
        Assert\NotBlank(
            groups: [ClientServicesTabFormType::FORM_NAME]
        ),
        Assert\Type('integer'),
        Assert\Range(min: 0, max: 60)
    ]
    #[Groups(['read_client_services'])]
    private ?int $assistanceStart = self::ASSISTANCE_START_DEFAULT;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[
        Assert\NotBlank(groups: [ClientServicesTabFormType::FORM_NAME])
    ]
    #[Groups(['read_client_services'])]
    private ?int $commitment = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[
        Assert\Email(groups: [ClientServicesTabFormType::FORM_NAME]),
        Assert\NotBlank(allowNull: true, groups: [ClientServicesTabFormType::FORM_NAME])
    ]
    #[Groups(['read_client_services'])]
    private ?string $emailToNotifyForAssistance = null;

    #[ORM\Column(type: 'integer')]
    #[
        Assert\NotBlank(
            groups: [ClientServicesTabFormType::FORM_NAME]
        ),
        Assert\Choice(
            choices: [
                self::ENUM_SUPPORT_NO,
                self::ENUM_SUPPORT_ON_DEMAND,
                self::ENUM_SUPPORT_LAUNCH,
                self::ENUM_SUPPORT_FULL,
            ],
            groups: [ClientServicesTabFormType::FORM_NAME]
        )
    ]
    #[Groups(['read_client_services'])]
    private int $supportType = self::ENUM_SUPPORT_ON_DEMAND;

    #[Assert\Valid]
    #[ORM\Embedded(class: ServiceSchedule::class)]
    #[Groups(['read_client_services'])]
    private ServiceSchedule $standardHours;

    #[Assert\Valid]
    #[ORM\Embedded(class: ServiceSchedule::class)]
    #[Groups(['read_client_services'])]
    private ServiceSchedule $outsideStandardHours;

    #[ORM\Column(type: 'integer')]
    #[
        Assert\NotBlank(groups: [ClientServicesTabFormType::FORM_NAME]),
        Assert\Choice(
            choices: [
                self::ENUM_SUPPORT_FOR_START_SESSION,
                self::ENUM_SUPPORT_FOR_ALL_SESSION,
            ],
            groups: [ClientServicesTabFormType::FORM_NAME]
        )
    ]
    #[Groups(['read_client_services'])]
    private int $outsideSupportType = self::ENUM_SUPPORT_FOR_START_SESSION;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[
        Assert\NotBlank(
            allowNull: true,
            groups: [ClientServicesTabFormType::FORM_NAME]
        ),
        Assert\Type('integer'),
        Assert\Range(min: 0, max: 60)
    ]
    #[Groups(['read_client_services'])]
    private ?int $timeBeforeSession = self::SUPPORT_START_DEFAULT;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[
        Assert\NotBlank(
            allowNull: true,
            groups: [ClientServicesTabFormType::FORM_NAME]
        ),
        Assert\Type('integer'),
        Assert\Range(min: 0, max: 60)
    ]
    #[Groups(['read_client_services'])]
    private ?int $timeSupportDurationSession = self::SUPPORT_DURATION_DEFAULT;

    #[ORM\Column(type: 'string', nullable: true)]
    #[
        Assert\Email(groups: [ClientServicesTabFormType::FORM_NAME]),
        Assert\NotBlank(allowNull: true, groups: [ClientServicesTabFormType::FORM_NAME])
    ]
    #[Groups(['read_client_services'])]
    private ?string $emailToNotifyForSupport = null;

    #[ORM\Column()]
    #[Groups(['read_client_services'])]
    private bool $dedicatedSupport = false;

    #[ORM\Column()]
    #[Groups(['read_client_services', 'filter:view'])]
    private bool $lumpSumContract = false;

    #[ORM\Column(type: 'string', nullable: true)]
    #[
        Assert\Email(groups: [ClientServicesTabFormType::FORM_NAME]),
        Assert\NotBlank(allowNull: true, groups: [ClientServicesTabFormType::FORM_NAME])
    ]
    #[Groups(['read_client_services', 'ui:registration_page_template:read'])]
    private ?string $mailSupport = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientServicesTabFormType::FORM_NAME])]
    #[Groups(['read_client_services', 'ui:registration_page_template:read'])]
    private ?string $phoneSupport = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[
        Assert\Email(groups: [ClientServicesTabFormType::FORM_NAME]),
        Assert\NotBlank(allowNull: true, groups: [ClientServicesTabFormType::FORM_NAME])
    ]
    #[Groups(['read_client_services'])]
    private ?string $mailManager = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientServicesTabFormType::FORM_NAME])]
    #[Groups(['read_client_services'])]
    private ?string $phoneManager = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientServicesTabFormType::FORM_NAME])]
    #[Groups(['read_client_services'])]
    private ?string $dedicatedUserGuide = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Assert\NotBlank(allowNull: true, groups: [ClientServicesTabFormType::FORM_NAME])]
    #[Groups(['read_client_services'])]
    private ?string $personnalizedInfoMailTransactionnal = null;

    public function __construct()
    {
        $this->standardHours = new ServiceSchedule();
        $this->outsideStandardHours = new ServiceSchedule();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): ClientServices
    {
        $this->id = $id;

        return $this;
    }

    public function getAssistanceType(): ?int
    {
        return $this->assistanceType;
    }

    public function setAssistanceType(?int $assistanceType): ClientServices
    {
        $this->assistanceType = $assistanceType;

        return $this;
    }

    public function getReadableAssistanceType(): string
    {
        return match ($this->assistanceType) {
            self::ENUM_NO => 'No assistance',
            self::ENUM_ON_DEMAND => 'Available on demand',
            self::ENUM_FULL => 'Launch assistance for each session',
        };
    }

    public function getAssistanceDuration(): ?int
    {
        return $this->assistanceDuration;
    }

    public function setAssistanceDuration(?int $assistanceDuration): ClientServices
    {
        $this->assistanceDuration = $assistanceDuration;

        return $this;
    }

    public function getAssistanceStart(): ?int
    {
        return $this->assistanceStart;
    }

    public function setAssistanceStart(?int $assistanceStart): ClientServices
    {
        $this->assistanceStart = $assistanceStart;

        return $this;
    }

    public function getCommitment(): ?int
    {
        return $this->commitment;
    }

    public function setCommitment(int $commitment): ClientServices
    {
        $this->commitment = $commitment;

        return $this;
    }

    public function getEmailToNotifyForAssistance(): ?string
    {
        return $this->emailToNotifyForAssistance;
    }

    public function setEmailToNotifyForAssistance(?string $emailToNotifyForAssistance): ClientServices
    {
        $this->emailToNotifyForAssistance = $emailToNotifyForAssistance;

        return $this;
    }

    public function getSupportType(): ?int
    {
        return $this->supportType;
    }

    public function setSupportType(?int $supportType): ClientServices
    {
        $this->supportType = $supportType;

        return $this;
    }

    public function getReadableSupportType(): string
    {
        return match ($this->supportType) {
            self::ENUM_NO => 'No support',
            self::ENUM_ON_DEMAND => 'Available on demand',
            self::ENUM_FULL => 'Total support'
        };
    }

    public function getStandardHours(): ServiceSchedule
    {
        return $this->standardHours;
    }

    public function setStandardHours(ServiceSchedule $standardHours): ClientServices
    {
        $this->standardHours = $standardHours;

        return $this;
    }

    public function getOutsideStandardHours(): ServiceSchedule
    {
        return $this->outsideStandardHours;
    }

    public function setOutsideStandardHours(ServiceSchedule $outsideStandardHours): ClientServices
    {
        $this->outsideStandardHours = $outsideStandardHours;

        return $this;
    }

    public function getOutsideSupportType(): int
    {
        return $this->outsideSupportType;
    }

    public function setOutsideSupportType(int $outsideSupportType): ClientServices
    {
        $this->outsideSupportType = $outsideSupportType;

        return $this;
    }

    public function getTimeBeforeSession(): ?int
    {
        return $this->timeBeforeSession;
    }

    public function setTimeBeforeSession(?int $timeBeforeSession): ClientServices
    {
        $this->timeBeforeSession = $timeBeforeSession;

        return $this;
    }

    public function getTimeSupportDurationSession(): ?int
    {
        return $this->timeSupportDurationSession;
    }

    public function setTimeSupportDurationSession(?int $timeSupportDurationSession): ClientServices
    {
        $this->timeSupportDurationSession = $timeSupportDurationSession;

        return $this;
    }

    public function getEmailToNotifyForSupport(): ?string
    {
        return $this->emailToNotifyForSupport;
    }

    public function setEmailToNotifyForSupport(?string $emailToNotifyForSupport): ClientServices
    {
        $this->emailToNotifyForSupport = $emailToNotifyForSupport;

        return $this;
    }

    public function isDedicatedSupport(): bool
    {
        return $this->dedicatedSupport;
    }

    public function setDedicatedSupport(bool $dedicatedSupport): ClientServices
    {
        $this->dedicatedSupport = $dedicatedSupport;

        return $this;
    }

    public function getMailSupport(): ?string
    {
        return $this->mailSupport;
    }

    public function setMailSupport(?string $mailSupport): ClientServices
    {
        $this->mailSupport = $mailSupport;

        return $this;
    }

    public function getPhoneSupport(): ?string
    {
        return $this->phoneSupport;
    }

    public function setPhoneSupport(?string $phoneSupport): ClientServices
    {
        $this->phoneSupport = $phoneSupport;

        return $this;
    }

    public function getMailManager(): ?string
    {
        return $this->mailManager;
    }

    public function setMailManager(?string $mailManager): void
    {
        $this->mailManager = $mailManager;
    }

    public function getPhoneManager(): ?string
    {
        return $this->phoneManager;
    }

    public function setPhoneManager(?string $phoneManager): void
    {
        $this->phoneManager = $phoneManager;
    }

    public function getDedicatedUserGuide(): ?string
    {
        return $this->dedicatedUserGuide;
    }

    public function setDedicatedUserGuide(?string $dedicatedUserGuide): ClientServices
    {
        $this->dedicatedUserGuide = $dedicatedUserGuide;

        return $this;
    }

    public function getPersonnalizedInfoMailTransactionnal(): ?string
    {
        return $this->personnalizedInfoMailTransactionnal;
    }

    public function setPersonnalizedInfoMailTransactionnal(?string $personnalizedInfoMailTransactionnal): ClientServices
    {
        $this->personnalizedInfoMailTransactionnal = $personnalizedInfoMailTransactionnal;

        return $this;
    }

    public function isLumpSumContract(): bool
    {
        return $this->lumpSumContract;
    }

    public function setLumpSumContract(bool $lumpSumContract): self
    {
        $this->lumpSumContract = $lumpSumContract;

        return $this;
    }
}
