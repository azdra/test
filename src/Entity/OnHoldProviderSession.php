<?php

namespace App\Entity;

use App\Controller\Api\OnHoldProviderSessionApiController;
use App\Entity\Client\Client;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;

#[ORM\Entity]
class OnHoldProviderSession
{
    public const GROUP_VIEW = 'caa:session:view';
    public const GROUP_VIEW_PRESENCES = 'caa:session:view:presences';
    public const GROUP_CREATE = 'caa:session:create';
    public const GROUP_UPDATE = 'caa:session:update';

    public const ALIAS = 'on_hold_provider_session';

    public const STATUS_WAIT_FOR_VALIDATION = 1;
    public const STATUS_VALIDATED = 2;

    public const FINAL_STATUS_SCHEDULED = 'Scheduled';
    public const FINAL_STATUS_TO_FINALIZE = 'To finalize';
    public const FINAL_STATUS_FINALIZED = 'Finalized';

    public const CONNECTOR_TYPE_ADOBE_CONNECT_SCO = 'adobe_connect_sco';
    public const CONNECTOR_TYPE_MICROSOFT_TEAMS_EVENT = 'microsoft_teams_event';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_on_hold_session', 'read_on_hold_session_list', 'read_on_hold_participant',
        OnHoldProviderSessionApiController::GROUP_VIEW,
    ])]
    protected int $id;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_on_hold_session', 'read_on_hold_session_list',
        OnHoldProviderSessionApiController::GROUP_VIEW, OnHoldProviderSessionApiController::GROUP_UPDATE, OnHoldProviderSessionApiController::GROUP_CREATE,
    ])]
    #[NotBlank(groups: ['create', 'update', OnHoldProviderSessionApiController::GROUP_CREATE])]
    protected string $megaUUID = '';

    #[ORM\Column(type: 'string')]
    #[Groups(['read_on_hold_session', 'read_on_hold_session_list',
        OnHoldProviderSessionApiController::GROUP_VIEW, OnHoldProviderSessionApiController::GROUP_UPDATE, OnHoldProviderSessionApiController::GROUP_CREATE,
    ])]
    #[NotBlank(groups: ['create', 'update', OnHoldProviderSessionApiController::GROUP_CREATE])]
    protected string $title;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_on_hold_session', 'read_on_hold_session_list',
        OnHoldProviderSessionApiController::GROUP_VIEW, OnHoldProviderSessionApiController::GROUP_UPDATE, OnHoldProviderSessionApiController::GROUP_CREATE,
    ])]
    #[NotBlank(groups: ['create', 'update', OnHoldProviderSessionApiController::GROUP_CREATE])]
    protected string $connectorType = self::CONNECTOR_TYPE_ADOBE_CONNECT_SCO;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_on_hold_session', 'read_on_hold_session_list',
        OnHoldProviderSessionApiController::GROUP_VIEW, OnHoldProviderSessionApiController::GROUP_UPDATE, OnHoldProviderSessionApiController::GROUP_CREATE,
    ])]
    protected \DateTime $createdAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['read_on_hold_session', 'read_on_hold_session_list',
        OnHoldProviderSessionApiController::GROUP_VIEW, OnHoldProviderSessionApiController::GROUP_UPDATE, OnHoldProviderSessionApiController::GROUP_CREATE,
    ])]
    protected ?\DateTime $updatedAt = null;

    #[ORM\ManyToOne(targetEntity: Client::class)]
    protected Client $client;

    #[ORM\OneToMany(mappedBy: 'session', targetEntity: OnHoldProviderParticipantSessionRole::class, cascade: ['persist'])]
    protected Collection $onHoldParticipantRoles;

    #[ORM\Column(type: 'smallint')]
    #[Groups(['read_on_hold_session', 'read_on_hold_session_list',
        OnHoldProviderSessionApiController::GROUP_VIEW, OnHoldProviderSessionApiController::GROUP_UPDATE, OnHoldProviderSessionApiController::GROUP_CREATE,
    ])]
    protected int $statusValidation = self::STATUS_WAIT_FOR_VALIDATION;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read_on_hold_session', 'read_on_hold_session_list',
        OnHoldProviderSessionApiController::GROUP_VIEW, OnHoldProviderSessionApiController::GROUP_UPDATE, OnHoldProviderSessionApiController::GROUP_CREATE,
    ])]
    protected int $nbrMinParticipants = 0;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read_on_hold_session', 'read_on_hold_session_list',
        OnHoldProviderSessionApiController::GROUP_VIEW, OnHoldProviderSessionApiController::GROUP_UPDATE, OnHoldProviderSessionApiController::GROUP_CREATE,
    ])]
    protected int $nbrMaxParticipants = 0;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'created_by', referencedColumnName: 'id')]
    protected ?User $createdBy = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    protected ?User $updatedBy = null;

    #[ORM\Column(type: 'json')]
    #[Groups(['read_on_hold_session', 'read_on_hold_session_list',
        OnHoldProviderSessionApiController::GROUP_VIEW, OnHoldProviderSessionApiController::GROUP_UPDATE, OnHoldProviderSessionApiController::GROUP_CREATE,
    ])]
    protected array $slotsDates = [];

    #[ORM\Column(type: 'json')]
    #[Groups(['read_on_hold_session', 'read_on_hold_session_list',
        OnHoldProviderSessionApiController::GROUP_VIEW, OnHoldProviderSessionApiController::GROUP_UPDATE, OnHoldProviderSessionApiController::GROUP_CREATE,
    ])]
    protected array $participants = [];

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->onHoldParticipantRoles = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getMegaUUID(): string
    {
        return $this->megaUUID;
    }

    public function setMegaUUID(string $megaUUID): self
    {
        $this->megaUUID = $megaUUID;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getNbrMinParticipants(): int
    {
        return $this->nbrMinParticipants;
    }

    public function setNbrMinParticipants(int $nbrMinParticipants): self
    {
        $this->nbrMinParticipants = $nbrMinParticipants;

        return $this;
    }

    public function getNbrMaxParticipants(): int
    {
        return $this->nbrMaxParticipants;
    }

    public function setNbrMaxParticipants(int $nbrMaxParticipants): self
    {
        $this->nbrMaxParticipants = $nbrMaxParticipants;

        return $this;
    }

    public function getConnectorType(): string
    {
        return $this->connectorType;
    }

    public function setConnectorType(string $connectorType): self
    {
        $this->connectorType = $connectorType;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    #[Groups(['read_session', 'read_session_list', 'help_need:read', 'read_session_calendar'])]
    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    #[Groups('read_on_hold_session')]
    public function getOnHoldParticipantRoles(): ArrayCollection|Collection
    {
        return $this->onHoldParticipantRoles;
    }

    public function setOnHoldParticipantRoles(ArrayCollection|Collection $onHoldParticipantRoles): self
    {
        $this->onHoldParticipantRoles = $onHoldParticipantRoles;

        return $this;
    }

    public function addOnHoldParticipantRole(OnHoldProviderParticipantSessionRole $role): self
    {
        $this->onHoldParticipantRoles->add($role);

        return $this;
    }

    public function removeOnHoldParticipantRole(OnHoldProviderParticipantSessionRole $role): self
    {
        if ($this->onHoldParticipantRoles->contains($role)) {
            $this->onHoldParticipantRoles->removeElement($role);
        }

        return $this;
    }

    public function getStatusValidation(): int
    {
        return $this->statusValidation;
    }

    public function setStatusValidation(int $statusValidation): self
    {
        /*if (!in_array($statusValidation, [self::STATUS_WAIT_FOR_VALIDATION])) {
            throw new \LogicException();
        }*/

        $this->statusValidation = $statusValidation;

        return $this;
    }

    #[Groups(['read_session', 'read_session_list'])]
    public function isAFirstStatus(): bool
    {
        return $this->statusValidation === self::STATUS_WAIT_FOR_VALIDATION;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?UserInterface $user): OnHoldProviderSession
    {
        $this->createdBy = $user;

        return $this;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?UserInterface $updatedBy): OnHoldProviderSession
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function __toString(): string
    {
        return "{$this->getId()} - {$this->getTitle()}";
    }

    public function getSlotsDates(): array
    {
        sort($this->slotsDates);

        return $this->slotsDates;
    }

    public function setSlotsDates(array $slotsDates): OnHoldProviderSession
    {
        $this->slotsDates = $slotsDates;
        sort($this->slotsDates);

        return $this;
    }

    public function getParticipants(): array
    {
        return $this->participants;
    }

    public function setParticipants(array $participants): OnHoldProviderSession
    {
        $this->participants = $participants;

        return $this;
    }

    /*#[Groups(['read_on_hold_session', 'read_on_hold_session_list'])]
    public function getAnimatorsCompleted(): array
    {
        $animatorsCompleted = [];
        foreach($this->getParticipants()['animators'] as $animator) {
            $animatorsCompleted[] = [
                'megaId' => $animator['megaId'],
                'name' => $animator['name'],
                'forname' => $animator['forname'],
                'email' => $animator['email'],
                'principalAnimator' => false
            ];
        }

        return $animatorsCompleted;

    }*/

    #[Groups(['read_on_hold_session', 'read_on_hold_session_list'])]
    public function getAnimatorsCount(): int
    {
        return count($this->getParticipants()['animators']);
    }

    #[Groups(['read_on_hold_session', 'read_on_hold_session_list'])]
    public function getTraineesCount(): int
    {
        return count($this->getParticipants()['trainees']);
    }

    /*#[Groups(['read_on_hold_session', 'read_on_hold_session_list'])]
    public function isSlotToFinalize(?int $statusValidation = 1): bool
    {
        return $statusValidation === self::STATUS_WAIT_FOR_VALIDATION;
    }

    #[Groups(['read_on_hold_session', 'read_on_hold_session_list'])]
    public function isSlotScheduled(?int $statusValidation = 2): bool
    {
        return $statusValidation === self::STATUS_VALIDATED;
    }

    #[Groups(['read_on_hold_session', 'read_on_hold_session_list'])]
    public function getSlotReadableState(?int $statusValidation = 1): string
    {
        if ($this->isSlotScheduled($statusValidation)) {
            return self::FINAL_STATUS_SCHEDULED;
        } elseif ($this->isSlotToFinalize($statusValidation)) {
            return self::FINAL_STATUS_TO_FINALIZE;
        } else {
            return self::FINAL_STATUS_SCHEDULED;
        }
    }*/
}
