<?php

namespace App\Entity;

use App\Entity\Client\Client;
use App\Model\ActivityLog\ActivityLogContext;
use App\Model\ActivityLog\ActivityLogModel;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ORM\Index(columns: ['created_at'], name: 'idx_createdAt')]
class ActivityLog
{
    public const ORIGIN_USER_INTERFACE = 'UserInterface';
    public const ORIGIN_CRON = 'Cron';
    public const ORIGIN_IMPORT = 'Import';
    public const ORIGIN_EXPORT = 'Export';

    public const SEVERITY_NOTIFICATION = 'notification';
    public const SEVERITY_INFORMATION = 'information';
    public const SEVERITY_WARNING = 'alert';
    public const SEVERITY_ERROR = 'error';

    public const ACTION_EXPORT_DATA = 'ExportData';
    public const ACTION_IMPORT_SESSIONS = 'ImportSessions';
    public const ACTION_IMPORT_USERS = 'ImportUsers';
    public const ACTION_SESSION_CREATION = 'SessionCreation';
    public const ACTION_SESSION_UPDATE = 'SessionUpdate';
    public const ACTION_SESSION_DELETE = 'SessionDelete';
    public const ACTION_SESSION_CANCEL = 'SessionCancel';
    public const ACTION_SESSION_UNCANCEL = 'SessionUncancel';
    public const ACTION_USER_ADD = 'UserAdd';
    public const ACTION_USER_ANONYMIZE = 'UserAnonymize';
    public const ACTION_USER_UPDATE = 'UserUpdate';
    public const ACTION_USER_DELETE = 'UserDeleted';
    public const ACTION_PARTICIPANT_ADD = 'ParticipantAdd';
    public const ACTION_PARTICIPANT_UNSUBSCRIBE = 'ParticipantUnsubscribe';
    public const ACTION_PARTICIPANT_REGISTRATION = 'ParticipantRegistration';
    public const ACTION_PARTICIPANT_UPDATE_ROLE = 'ParticipantUpdateRole';
    public const ACTION_PARTICIPANT_UPDATE_SITUATION = 'ParticipantUpdateSituation';
    public const ACTION_CLIENT_CREATION = 'ClientCreation';
    public const ACTION_CLIENT_UPDATED = 'ClientUpdated';
    public const ACTION_SEND_CONVOCATION = 'sendConvocation';
    public const ACTION_SEND_EVALUATION_PARTICIPANT = 'sendEvaluationParticipant';
    public const ACTION_SEND_EVALUATION_PARTICIPANTS = 'sendEvaluationParticipants';
    public const ACTION_CREATE_CONVOCATION = 'createConvocation';
    public const ACTION_UPDATE_CONVOCATION = 'updateConvocation';
    public const ACTION_DELETE_CONVOCATION = 'deleteConvocation';
    public const ACTION_SEND_REMINDER = 'sendReminder';
    public const ACTION_ALERT_SESSION_WITHOUT_ANIMATOR = 'AlertSessionWithoutAnimator';
    public const ACTION_ALERT_SESSION_WITHOUT_TRAINEE = 'AlertSessionWithoutTrainee';
    public const ACTION_ALERT_ANIMATOR_ON_SEVERAL_SESSIONS = 'AlertAnimatorOnSeveralSessions';
    public const ACTION_ALERT_TRAINEE_ON_SEVERAL_SESSIONS = 'AlertTraineeOnSeveralSessions';
    public const ACTION_NO_ANIMATOR_WITH_HOST_ON_SESSION = 'AlertNoAnimatorWithHostOnSession';
    public const ACTION_ALERT_SEVERAL_ANIMATORS_ON_SAME_SESSION = 'AlertSeveralAnimatorsOnSameSession';
    public const ACTION_SEND_PRESENCE_REPORT = 'sendPresenceReport';
    public const ACTION_SEND_EVALUATION_PERFORMANCE = 'sendEvaluationPerformance';
    public const ACTION_SEND_TRAINING_CERTIFICATE = 'sendTrainingCertificate';
    public const ACTION_ALERT_NEW_SUPPORT_REQUEST = 'AlertNewSupportRequest';
    public const ACTION_ADD_NEW_COMMUNICATION_WEBINAR = 'AddNewCommunicationWebinar';
    public const ACTION_DELETE_COMMUNICATION_WEBINAR = 'DeleteNewCommunicationWebinar';
    public const ACTION_EXPIRED_CLIENT_CREDIT = 'ExpiredClientCredit';
    public const ACTION_DELETE_SEND_EMAIL_IDFUSE = 'SendEmailIDFuse';
    public const ACTION_DELETE_SEND_EMAIL_BREVO = 'SendEmailBrevo';
    public const ACTION_SEND_EMAIL_TO_PARTICIPANT = 'SendEmailToParticipant';
    public const ACTION_ADD_PARTICIPANT_TO_SESSION = 'AddParticipantToSession';
    public const ACTION_PARTICIPANT_JOIN_SESSION_FROM_CATALOG = 'ParticipantJoinSessionFromCatalog';

    public const ALLOWED_ACTION_TO_BE_CLEANED = [
        ActivityLog::ACTION_EXPORT_DATA,
        ActivityLog::ACTION_IMPORT_SESSIONS,
        ActivityLog::ACTION_IMPORT_USERS,
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups('read_grouped_activities')]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'activityLogs')]
    #[Groups('read_grouped_activities')]
    #[ORM\JoinColumn(name: 'client_origin', referencedColumnName: 'id')]
    private Client $client;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'activityLogs')]
    #[Groups('read_grouped_activities')]
    #[ORM\JoinColumn(name: 'user_origin', referencedColumnName: 'id', nullable: true)]
    private ?User $user = null;

    #[ORM\Column(type: 'string')]
    #[Groups('read_grouped_activities')]
    #[Assert\Choice([
       self::ORIGIN_USER_INTERFACE,
       self::ORIGIN_CRON,
       self::ORIGIN_IMPORT,
       self::ORIGIN_EXPORT,
    ])]
    private string $origin;

    #[ORM\Column(type: 'string')]
    #[Groups('read_grouped_activities')]
    #[Assert\Choice([
        self::ACTION_EXPORT_DATA,
        self::ACTION_IMPORT_SESSIONS,
        self::ACTION_IMPORT_USERS,
        self::ACTION_SESSION_CREATION,
        self::ACTION_SESSION_UPDATE,
        self::ACTION_SESSION_DELETE,
        self::ACTION_SESSION_CANCEL,
        self::ACTION_SESSION_UNCANCEL,
        self::ACTION_USER_ADD,
        self::ACTION_USER_ANONYMIZE,
        self::ACTION_USER_UPDATE,
        self::ACTION_PARTICIPANT_ADD,
        self::ACTION_PARTICIPANT_UNSUBSCRIBE,
        self::ACTION_PARTICIPANT_REGISTRATION,
        self::ACTION_PARTICIPANT_UPDATE_ROLE,
        self::ACTION_PARTICIPANT_UPDATE_SITUATION,
        self::ACTION_USER_DELETE,
        self::ACTION_CLIENT_CREATION,
        self::ACTION_CLIENT_UPDATED,
        self::ACTION_SEND_CONVOCATION,
        self::ACTION_CREATE_CONVOCATION,
        self::ACTION_UPDATE_CONVOCATION,
        self::ACTION_DELETE_CONVOCATION,
        self::ACTION_SEND_REMINDER,
        self::ACTION_ALERT_SESSION_WITHOUT_ANIMATOR,
        self::ACTION_ALERT_SESSION_WITHOUT_TRAINEE,
        self::ACTION_ALERT_ANIMATOR_ON_SEVERAL_SESSIONS,
        self::ACTION_ALERT_TRAINEE_ON_SEVERAL_SESSIONS,
        self::ACTION_NO_ANIMATOR_WITH_HOST_ON_SESSION,
        self::ACTION_ALERT_SEVERAL_ANIMATORS_ON_SAME_SESSION,
        self::ACTION_SEND_PRESENCE_REPORT,
        self::ACTION_SEND_EVALUATION_PERFORMANCE,
        self::ACTION_SEND_EVALUATION_PARTICIPANT,
        self::ACTION_SEND_EVALUATION_PARTICIPANTS,
        self::ACTION_SEND_TRAINING_CERTIFICATE,
        self::ACTION_ALERT_NEW_SUPPORT_REQUEST,
        self::ACTION_ADD_NEW_COMMUNICATION_WEBINAR,
        self::ACTION_DELETE_COMMUNICATION_WEBINAR,
        self::ACTION_EXPIRED_CLIENT_CREDIT,
        self::ACTION_DELETE_SEND_EMAIL_IDFUSE,
        self::ACTION_DELETE_SEND_EMAIL_BREVO,
        self::ACTION_SEND_EMAIL_TO_PARTICIPANT,
        self::ACTION_ADD_PARTICIPANT_TO_SESSION,
        self::ACTION_PARTICIPANT_JOIN_SESSION_FROM_CATALOG,
    ])]
    private string $action;

    #[ORM\Column(type: 'string')]
    #[Groups('read_grouped_activities')]
    #[Assert\Choice([
        self::SEVERITY_NOTIFICATION,
        self::SEVERITY_INFORMATION,
        self::SEVERITY_ERROR,
        self::SEVERITY_WARNING,
    ])]
    private string $severity;

    #[ORM\Column(type: 'json')]
    #[Groups('read_grouped_activities')]
    private array $infos = [];

    #[ORM\Column(type: 'datetime')]
    #[Groups('read_grouped_activities')]
    private \DateTime $createdAt;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    protected bool $reportsEmailSent = false;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public static function createFromContext(ActivityLogContext $activityLogContext, ActivityLogModel $activityLogModel): ActivityLog
    {
        $activityLog = new ActivityLog();
        $activityLog->setClient($activityLogContext->client)
            ->setUser($activityLogContext->user)
            ->setOrigin($activityLogContext->origin)
            ->setAction($activityLogModel->action)
            ->setSeverity($activityLogModel->severity)
            ->setInfos(array_merge($activityLogContext->infos, $activityLogModel->data))
        ;

        return $activityLog;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): ActivityLog
    {
        $this->id = $id;

        return $this;
    }

    public function getOrigin(): string
    {
        return $this->origin;
    }

    public function setOrigin(string $origin): ActivityLog
    {
        if (!\in_array($origin, self::getAvailableOrigins())) {
            throw new \LogicException(sprintf('ORIGIN %s does not exist, or has not yet been implemented.', $origin));
        }

        $this->origin = $origin;

        return $this;
    }

    public static function getAvailableOrigins(): array
    {
        return [
            self::ORIGIN_USER_INTERFACE,
            self::ORIGIN_CRON,
            self::ORIGIN_IMPORT,
            self::ORIGIN_EXPORT,
        ];
    }

    public function getSeverity(): string
    {
        return $this->severity;
    }

    public static function getAvailableSeverity(): array
    {
        return [
            self::SEVERITY_NOTIFICATION,
            self::SEVERITY_INFORMATION,
            self::SEVERITY_ERROR,
            self::SEVERITY_WARNING,
        ];
    }

    public function setSeverity(string $severity): ActivityLog
    {
        if (!\in_array($severity, self::getAvailableSeverity())) {
            throw new \LogicException(sprintf('Severity %s does not exist, or has not yet been implemented.', $severity));
        }

        $this->severity = $severity;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): ActivityLog
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public static function getAvailableActions(): array
    {
        return [
            self::ACTION_EXPORT_DATA,
            self::ACTION_IMPORT_SESSIONS,
            self::ACTION_IMPORT_USERS,
            self::ACTION_SESSION_CREATION,
            self::ACTION_SESSION_UPDATE,
            self::ACTION_SESSION_DELETE,
            self::ACTION_SESSION_CANCEL,
            self::ACTION_SESSION_UNCANCEL,
            self::ACTION_USER_ADD,
            self::ACTION_USER_ANONYMIZE,
            self::ACTION_USER_UPDATE,
            self::ACTION_PARTICIPANT_ADD,
            self::ACTION_PARTICIPANT_UNSUBSCRIBE,
            self::ACTION_PARTICIPANT_REGISTRATION,
            self::ACTION_PARTICIPANT_UPDATE_ROLE,
            self::ACTION_PARTICIPANT_UPDATE_SITUATION,
            self::ACTION_USER_DELETE,
            self::ACTION_CLIENT_CREATION,
            self::ACTION_CLIENT_UPDATED,
            self::ACTION_SEND_CONVOCATION,
            self::ACTION_UPDATE_CONVOCATION,
            self::ACTION_CREATE_CONVOCATION,
            self::ACTION_DELETE_CONVOCATION,
            self::ACTION_SEND_REMINDER,
            self::ACTION_ALERT_SESSION_WITHOUT_ANIMATOR,
            self::ACTION_ALERT_SESSION_WITHOUT_TRAINEE,
            self::ACTION_ALERT_ANIMATOR_ON_SEVERAL_SESSIONS,
            self::ACTION_ALERT_TRAINEE_ON_SEVERAL_SESSIONS,
            self::ACTION_ALERT_SEVERAL_ANIMATORS_ON_SAME_SESSION,
            self::ACTION_NO_ANIMATOR_WITH_HOST_ON_SESSION,
            self::ACTION_SEND_PRESENCE_REPORT,
            self::ACTION_SEND_EVALUATION_PERFORMANCE,
            self::ACTION_SEND_EVALUATION_PARTICIPANT,
            self::ACTION_SEND_EVALUATION_PARTICIPANTS,
            self::ACTION_SEND_TRAINING_CERTIFICATE,
            self::ACTION_ALERT_NEW_SUPPORT_REQUEST,
            self::ACTION_ADD_NEW_COMMUNICATION_WEBINAR,
            self::ACTION_DELETE_COMMUNICATION_WEBINAR,
            self::ACTION_EXPIRED_CLIENT_CREDIT,
            self::ACTION_DELETE_SEND_EMAIL_IDFUSE,
            self::ACTION_DELETE_SEND_EMAIL_BREVO,
            self::ACTION_SEND_EMAIL_TO_PARTICIPANT,
            self::ACTION_ADD_PARTICIPANT_TO_SESSION,
            self::ACTION_PARTICIPANT_JOIN_SESSION_FROM_CATALOG,
        ];
    }

    public function setAction(string $action): ActivityLog
    {
        if (!\in_array($action, self::getAvailableActions())) {
            throw new \LogicException(sprintf('Action %s does not exist, or has not yet been implemented.', $action));
        }

        $this->action = $action;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): ActivityLog
    {
        $this->client = $client;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): ActivityLog
    {
        $this->user = $user;

        return $this;
    }

    public function isReportsEmailSent(): bool
    {
        return $this->reportsEmailSent;
    }

    public function setReportsEmailSent(bool $reportsEmailSent): ActivityLog
    {
        $this->reportsEmailSent = $reportsEmailSent;

        return $this;
    }

    public function getInfos(): array
    {
        return $this->infos;
    }

    public function setInfos(array $infos): ActivityLog
    {
        $this->infos = $infos;

        return $this;
    }

    #[Groups('read_grouped_activities')]
    public function getDayOfCreation(): string
    {
        return $this->createdAt->format('Y-m-d');
    }
}
