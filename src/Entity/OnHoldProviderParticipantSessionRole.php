<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class OnHoldProviderParticipantSessionRole
{
    public const ROLE_ANIMATOR = 'animator';
    public const ROLE_TRAINEE = 'trainee';
    public const ROLE_REMOVE = 'remove';

    public const SITUATION_MIXTE_DISTANTIAL = 1;
    public const SITUATION_MIXTE_PRESENTIAL = 2;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_on_hold_session', 'lite_on_hold_participant'])]
    protected int $id;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_on_hold_session', 'lite_on_hold_participant'])]
    protected string $role = self::ROLE_TRAINEE;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_on_hold_session', 'lite_on_hold_participant'])]
    protected string $name;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_on_hold_session', 'lite_on_hold_participant'])]
    protected string $forname;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_on_hold_session', 'lite_on_hold_participant'])]
    protected string $email;

    #[ORM\ManyToOne(targetEntity: OnHoldProviderSession::class, inversedBy: 'onHoldParticipantRoles')]
    #[Groups(['read_on_hold_session', 'lite_on_hold_participant'])]
    protected OnHoldProviderSession $session;

    #[ORM\Column(type: 'integer', options: ['default' => 1])]
    #[Groups(['read_on_hold_session', 'lite_on_hold_participant'])]
    private ?int $situationMixte = 1;

    public function __construct()
    {
    }

    public function setRole(string $role): OnHoldProviderParticipantSessionRole
    {
        $this->role = $role;

        return $this;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function setName(string $name): OnHoldProviderParticipantSessionRole
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setForname(string $forname): OnHoldProviderParticipantSessionRole
    {
        $this->forname = $forname;

        return $this;
    }

    public function getForname(): string
    {
        return $this->forname;
    }

    public function setEmail(string $email): OnHoldProviderParticipantSessionRole
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): OnHoldProviderParticipantSessionRole
    {
        $this->id = $id;

        return $this;
    }

    public function getSession(): OnHoldProviderSession
    {
        return $this->session;
    }

    public function setSession(OnHoldProviderSession $session): OnHoldProviderParticipantSessionRole
    {
        $this->session = $session;

        return $this;
    }

    public function getSituationMixte(): ?int
    {
        return $this->situationMixte;
    }

    public function setSituationMixte(?int $situationMixte): OnHoldProviderParticipantSessionRole
    {
        $this->situationMixte = $situationMixte;

        return $this;
    }

    #[Groups(['read_on_hold_session'])]
    public function isDistantial(): bool
    {
        return $this->situationMixte == self::SITUATION_MIXTE_DISTANTIAL;
    }

    #[Groups(['read_on_hold_session'])]
    public function isPresential(): bool
    {
        return $this->situationMixte == self::SITUATION_MIXTE_PRESENTIAL;
    }

    public static function getSituationMixteHumanized(int $situation): string
    {
        return match ($situation) {
            self::SITUATION_MIXTE_DISTANTIAL => 'Distancial',
            self::SITUATION_MIXTE_PRESENTIAL => 'Presential',
            default => 'Unknown',
        };
    }
}
