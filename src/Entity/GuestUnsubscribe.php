<?php

namespace App\Entity;

use App\Entity\Client\Client;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[UniqueEntity(fields: ['email', 'client'], message: 'This address email already exist with this client', errorPath: 'email')]
class GuestUnsubscribe
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_user', 'read_session'])]
    private int $id;

    #[ORM\Column(type: 'string', length: 180)]
    #[Groups(['read_user', 'read_session'])]
    private string $email;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_user', 'read_session'])]
    private DateTimeInterface $unsubscribeAt;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'guestUnsubscribe')]
    #[Groups(['read_user'])]
    private Client $client;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->unsubscribeAt = new DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getUnsubscribeAt(): DateTimeInterface
    {
        return $this->unsubscribeAt;
    }

    public function setUnsubscribeAt(DateTimeInterface $unsubscribeAt): self
    {
        $this->unsubscribeAt = $unsubscribeAt;

        return $this;
    }
}
