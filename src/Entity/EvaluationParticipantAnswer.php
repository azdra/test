<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity()]
class EvaluationParticipantAnswer
{
    public const EVALUATION_LOGBOOK_FEELING_ASSISTANCE_HARD = 1;
    public const EVALUATION_LOGBOOK_FEELING_ASSISTANCE_NOT_EASY = 2;
    public const EVALUATION_LOGBOOK_FEELING_ASSISTANCE_FINE = 3;
    public const EVALUATION_LOGBOOK_FEELING_ASSISTANCE_GREAT = 4;

    public const EVALUATION_LOGBOOK_POSITIVE = [self::EVALUATION_LOGBOOK_FEELING_ASSISTANCE_FINE, self::EVALUATION_LOGBOOK_FEELING_ASSISTANCE_GREAT];
    public const EVALUATION_LOGBOOK_NEGATIVE = [self::EVALUATION_LOGBOOK_FEELING_ASSISTANCE_NOT_EASY, self::EVALUATION_LOGBOOK_FEELING_ASSISTANCE_HARD];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'read_lobby_signature', 'lite_participant'])]
    private int $id;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $replyDate;

    #[ORM\ManyToOne(targetEntity: ProviderParticipantSessionRole::class, inversedBy: 'answers')]
    private ProviderParticipantSessionRole $providerParticipantSessionRole;

    #[ORM\ManyToOne(targetEntity: Evaluation::class)]
    #[ORM\JoinColumn(name: 'evaluation_id', referencedColumnName: 'id')]
    #[Groups(['lite_participant'])]
    private Evaluation $evaluation;

    #[ORM\Column(type: 'json')]
    #[Groups(['read_session', 'session:evaluation:synthesis', 'lite_participant'])]
    private array $answer = [];

    public function __construct(Evaluation $evaluation, ProviderParticipantSessionRole $providerParticipantSessionRole)
    {
        $this->providerParticipantSessionRole = $providerParticipantSessionRole;
        $this->evaluation = $evaluation;
        $this->replyDate = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getReplyDate(): \DateTime
    {
        return $this->replyDate;
    }

    public function getProviderParticipantSessionRole(): ProviderParticipantSessionRole
    {
        return $this->providerParticipantSessionRole;
    }

    public function setProviderParticipantSessionRole(ProviderParticipantSessionRole $providerParticipantSessionRole): self
    {
        $this->providerParticipantSessionRole = $providerParticipantSessionRole;

        return $this;
    }

    public function getEvaluation(): Evaluation
    {
        return $this->evaluation;
    }

    public function setEvaluation(Evaluation $evaluation): self
    {
        $this->evaluation = $evaluation;

        return $this;
    }

    public function getAnswer(): array
    {
        return $this->answer;
    }

    public function setAnswer(array $answer): self
    {
        $this->answer = $answer;

        return $this;
    }
}
