<?php

namespace App\Entity;

use App\Controller\Api\ProviderSessionApiController;
use App\Controller\Api\UserApiController;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Model\ConvocationSendingHistory;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[UniqueEntity(['participant', 'session'], ignoreNull: false)]
#[ORM\UniqueConstraint('pprs_session_participant', ['participant_id', 'session_id'])]
class ProviderParticipantSessionRole implements MiddlewareManagedEntityInterface
{
    public const ROLE_ANIMATOR = 'animator';
    public const ROLE_TRAINEE = 'trainee';
    public const ROLE_REMOVE = 'remove';

    public const SITUATION_MIXTE_DISTANTIAL = 1;
    public const SITUATION_MIXTE_PRESENTIAL = 2;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'read_lobby_signature', ProviderSessionApiController::GROUP_VIEW_PRESENCES, 'lite_participant'])]
    protected int $id;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_session',
        ProviderSessionApiController::GROUP_VIEW_PRESENCES,
        UserApiController::GROUP_VIEW_SUBSCRIPTIONS, 'lite_participant',
    ])]
    protected string $role = self::ROLE_TRAINEE;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_session',
        ProviderSessionApiController::GROUP_VIEW_PRESENCES,
        UserApiController::GROUP_VIEW_SUBSCRIPTIONS,
    ])]
    protected string $oldRole = self::ROLE_TRAINEE;

    #[ORM\ManyToOne(targetEntity: ProviderParticipant::class, cascade: ['persist'], inversedBy: 'sessionRoles')]
    #[Groups(['read_session', 'read_lobby_signature', ProviderSessionApiController::GROUP_CREATE, 'lite_participant'])]
    protected ProviderParticipant $participant;

    #[ORM\ManyToOne(targetEntity: ProviderSession::class, inversedBy: 'participantRoles')]
    #[Groups(['read_lobby_signature', UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    protected ProviderSession $session;

    #[ORM\Column(type: 'boolean')]
    protected bool $syncedWithProvider = false;

    #[ORM\Column(type: 'object', nullable: true)]
    protected ?array $datesForSendingConvocations = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['read_session'])]
    protected ?\DateTimeImmutable $lastConvocationSent = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['read_session'])]
    protected ?\DateTimeImmutable $lastReminderSent = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['read_session'])]
    protected ?\DateTimeImmutable $lastCommunicationSent = null;

    #[ORM\Column(type: 'json')]
    #[Groups(['read_session'])]
    private ?array $registrationFormResponses = [];

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['read_session'])]
    protected ?\DateTimeImmutable $lastReminderCommunicationSent = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['read_session'])]
    protected ?\DateTimeImmutable $lastThanksCommunicationSent = null;

    #[ORM\Embedded(class: ProviderSessionAccessToken::class)]
    #[Groups(['read_lobby_signature', ProviderSessionApiController::GROUP_VIEW_PRESENCES, 'read_session'])]
    protected ProviderSessionAccessToken $providerSessionAccessToken;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW_PRESENCES, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    protected ?\DateTimeImmutable $presenceDateStart = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW_PRESENCES, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    protected ?\DateTimeImmutable $presenceDateEnd = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW_PRESENCES, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    protected ?int $slotPresenceDuration = null;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read_session'])]
    protected bool $presenceStatus = false;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW_PRESENCES, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    protected bool $manualPresenceStatus = false;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW_PRESENCES, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    protected bool $remotePresenceStatus = false;

    #[ORM\Column(type: 'integer', options: ['default' => 1])]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW_PRESENCES])]
    private ?int $situationMixte = 1;

    #[ORM\OneToMany(mappedBy: 'providerParticipantSessionRole', targetEntity: EvaluationParticipantAnswer::class, cascade: ['all'], indexBy: 'evaluation_id')]
    #[Groups(['read_session', 'read_lobby_signature', 'session:evaluation:synthesis', 'lite_participant'])]
    protected Collection $answers;

    #[ORM\OneToMany(mappedBy: 'providerParticipantSessionRole', targetEntity: EvaluationSessionParticipantAnswer::class, cascade: ['all'])]
    #[Groups(['read_session', 'read_lobby_signature', 'session:evaluation:synthesis', 'lite_participant'])]
    protected Collection $evaluationSessionAnswers;

    #[ORM\OneToMany(mappedBy: 'providerParticipantSessionRole', targetEntity: ParticipantSessionSignature::class, cascade: ['all'], indexBy: 'signature_id')]
    #[Groups(['read_session', 'read_lobby_signature', UserApiController::GROUP_VIEW_SUBSCRIPTIONS, ProviderSessionApiController::GROUP_VIEW_PRESENCES])]
    #[OA\Property(
        property: 'signatures',
        type: 'array',
        items: new OA\Items(
            ref: new Model(type: ParticipantSessionSignature::class, groups: [UserApiController::GROUP_VIEW_SUBSCRIPTIONS, ProviderSessionApiController::GROUP_VIEW_PRESENCES])
        )
    )]
    protected Collection $signatures;

    #[ORM\Column(nullable: false)]
    #[Groups(['read_session'])]
    protected array $groupsParticipants = [];

    public function __construct()
    {
        $this->providerSessionAccessToken = new ProviderSessionAccessToken();
        $this->answers = new ArrayCollection();
        $this->signatures = new ArrayCollection();
    }

    public function setRole(string $role): ProviderParticipantSessionRole
    {
        $this->role = $role;

        return $this;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function getOldRole(): string
    {
        return $this->oldRole;
    }

    public function setOldRole(string $oldRole): self
    {
        $this->oldRole = $oldRole;

        return $this;
    }

    public static function getAvailableRoles(): array
    {
        return [
            self::ROLE_ANIMATOR,
            self::ROLE_TRAINEE,
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): ProviderParticipantSessionRole
    {
        $this->id = $id;

        return $this;
    }

    public function getParticipant(): ProviderParticipant
    {
        return $this->participant;
    }

    public function setParticipant(ProviderParticipant $participant): ProviderParticipantSessionRole
    {
        $this->participant = $participant;

        return $this;
    }

    public function getSession(): ProviderSession
    {
        return $this->session;
    }

    public function setSession(ProviderSession $session): ProviderParticipantSessionRole
    {
        $this->session = $session;

        return $this;
    }

    public function getAbstractProviderConfigurationHolder(): AbstractProviderConfigurationHolder
    {
        return $this->getSession()->getAbstractProviderConfigurationHolder();
    }

    public function isSyncedWithProvider(): bool
    {
        return $this->syncedWithProvider;
    }

    public function setSyncedWithProvider(bool $syncedWithProvider): ProviderParticipantSessionRole
    {
        $this->syncedWithProvider = $syncedWithProvider;

        return $this;
    }

    public function getDatesForSendingConvocations(): ?array
    {
        return $this->datesForSendingConvocations;
    }

    public function setDatesForSendingConvocations(?array $datesForSendingConvocations): self
    {
        $this->datesForSendingConvocations = $datesForSendingConvocations;

        return $this;
    }

    public function addADateOfSendingConvocation(string $type, ?User $origin = null): self
    {
        if (!in_array($type, ConvocationSendingHistory::TYPE_LIST)) {
            throw new \LogicException('Invalid type ');
        }

        $this->datesForSendingConvocations ??= [];
        $this->datesForSendingConvocations[] = (new ConvocationSendingHistory())
            ->setType($type)
            ->setOrigin($origin?->getId());

        if ($type === ConvocationSendingHistory::TYPE_CONVOCATION) {
            $this->lastConvocationSent = new \DateTimeImmutable();
        } else {
            $this->lastReminderSent = new \DateTimeImmutable();
        }

        return $this;
    }

    public function getLastConvocationSent(): ?\DateTimeImmutable
    {
        return $this->lastConvocationSent;
    }

    public function setLastConvocationSent(?\DateTimeImmutable $lastConvocationSent): self
    {
        $this->lastConvocationSent = $lastConvocationSent;

        return $this;
    }

    public function getLastReminderSent(): ?\DateTimeImmutable
    {
        return $this->lastReminderSent;
    }

    public function setLastReminderSent(?\DateTimeImmutable $lastReminderSent): self
    {
        $this->lastReminderSent = $lastReminderSent;

        return $this;
    }

    public function getLastCommunicationSent(): ?\DateTimeImmutable
    {
        return $this->lastCommunicationSent;
    }

    public function setLastCommunicationSent(?\DateTimeImmutable $lastCommunicationSent): self
    {
        $this->lastCommunicationSent = $lastCommunicationSent;

        return $this;
    }

    public function getLastReminderCommunicationSent(): ?\DateTimeImmutable
    {
        return $this->lastReminderCommunicationSent;
    }

    public function setLastReminderCommunicationSent(?\DateTimeImmutable $lastReminderCommunicationSent): self
    {
        $this->lastReminderCommunicationSent = $lastReminderCommunicationSent;

        return $this;
    }

    public function getLastThanksCommunicationSent(): ?\DateTimeImmutable
    {
        return $this->lastThanksCommunicationSent;
    }

    public function setLastThanksCommunicationSent(?\DateTimeImmutable $lastThanksCommunicationSent): self
    {
        $this->lastThanksCommunicationSent = $lastThanksCommunicationSent;

        return $this;
    }

    public function getProviderSessionAccessToken(): ProviderSessionAccessToken
    {
        return $this->providerSessionAccessToken;
    }

    public function setProviderSessionAccessToken(ProviderSessionAccessToken $providerSessionAccessToken): self
    {
        $this->providerSessionAccessToken = $providerSessionAccessToken;

        return $this;
    }

    public function getPresenceDateStart(): ?\DateTimeImmutable
    {
        return $this->presenceDateStart;
    }

    public function setPresenceDateStart(?\DateTimeImmutable $presenceDateStart): self
    {
        $this->presenceDateStart = $presenceDateStart;

        return $this;
    }

    public function getPresenceDateEnd(): ?\DateTimeImmutable
    {
        return $this->presenceDateEnd;
    }

    public function setPresenceDateEnd(?\DateTimeImmutable $presenceDateEnd): self
    {
        $this->presenceDateEnd = $presenceDateEnd;

        return $this;
    }

    public function getSlotPresenceDuration(): ?int
    {
        return $this->slotPresenceDuration;
    }

    public function setSlotPresenceDuration(?int $slotPresenceDuration): self
    {
        $this->slotPresenceDuration = $slotPresenceDuration;

        return $this;
    }

    public function setPresenceStatus(bool $presenceStatus): ProviderParticipantSessionRole
    {
        $this->presenceStatus = $presenceStatus;

        return $this;
    }

    public function getPresenceStatus(): ?bool
    {
        return $this->presenceStatus;
    }

    public function isManualPresenceStatus(): bool
    {
        return $this->manualPresenceStatus;
    }

    public function setManualPresenceStatus(bool $manualPresenceStatus): ProviderParticipantSessionRole
    {
        $this->manualPresenceStatus = $manualPresenceStatus;

        if (!$this->isRemotePresenceStatus()) {
            if ($this->manualPresenceStatus) {
                $this->presenceStatus = true;
                $this->presenceDateStart = \DateTimeImmutable::createFromMutable($this->session->getDateStart());
                $this->presenceDateEnd = \DateTimeImmutable::createFromMutable($this->session->getDateEnd());
                $this->slotPresenceDuration = $this->session->getDuration();
            } else {
                $this->presenceStatus = false;
                $this->presenceDateStart = null;
                $this->presenceDateEnd = null;
                $this->slotPresenceDuration = null;
            }
        }

        return $this;
    }

    public function getAnswers(): ArrayCollection|Collection
    {
        return $this->answers;
    }

    public function setAnswers(ArrayCollection|Collection $answers): self
    {
        $this->answers = $answers;

        return $this;
    }

    public function getSignatures(): ArrayCollection|Collection
    {
        return $this->signatures;
    }

    public function getOrderedSignatures(): ArrayCollection|Collection
    {
        $iterator = $this->signatures->getIterator();
        $iterator->uasort(fn ($a) => ($a->getDayPartIndicator() === 'AM') ? -1 : 1);

        return new ArrayCollection(iterator_to_array($iterator));
    }

    public function addSignature(ParticipantSessionSignature $participantSessionSignature): self
    {
        if (!$this->signatures->contains($participantSessionSignature)) {
            $this->signatures->add($participantSessionSignature);
        }

        return $this;
    }

    public function setSignatures(ArrayCollection|Collection $signatures): self
    {
        $this->signatures = $signatures;

        return $this;
    }

    #[Groups(['read_lobby_signature'])]
    public function getCountSignatures(): ?int
    {
        return count($this->signatures);
    }

    public function convocationIsToBeSent(): bool
    {
        return !empty($this->session->getNotificationDate())
            && ($this->session->isScheduled() || $this->session->isRunning())
            && empty($this->lastConvocationSent);
    }

    public function reminderIsToBeSent(): bool
    {
        $dateOfSend = $this->session->getNextReminder();

        return !$this->convocationIsToBeSent()
            && !is_null($dateOfSend)
            && ($this->session->isScheduled() || $this->session->isRunning())
            && (
                empty($this->lastReminderSent)
                || $this->lastReminderSent >= $dateOfSend
               );
    }

    public function isAnAnimator(): bool
    {
        return $this->role == self::ROLE_ANIMATOR;
    }

    public function isATrainee(): bool
    {
        return $this->role == self::ROLE_TRAINEE;
    }

    #[Groups([ProviderSessionApiController::GROUP_VIEW_PRESENCES, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    public function getUserId(): ?int
    {
        return $this->participant->getUserId();
    }

    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW_PRESENCES, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    public function getLastName(): ?string
    {
        return $this->participant->getLastName();
    }

    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW_PRESENCES, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    public function getFirstName(): ?string
    {
        return $this->participant->getFirstName();
    }

    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW_PRESENCES, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    public function getFullName(): ?string
    {
        return $this->participant->getFullName();
    }

    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW_PRESENCES, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    public function getEmail(): ?string
    {
        return $this->participant->getEmail();
    }

    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW_PRESENCES, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    public function isPresent(): bool
    {
        return $this->presenceStatus;
    }

    public function getSituationMixte(): ?int
    {
        return $this->situationMixte;
    }

    public function setSituationMixte(?int $situationMixte): ProviderParticipantSessionRole
    {
        $this->situationMixte = $situationMixte;

        return $this;
    }

    public function getRegistrationFormResponses(): ?array
    {
        return $this->registrationFormResponses;
    }

    public function setRegistrationFormResponses(?array $registrationFormResponses): self
    {
        $this->registrationFormResponses = $registrationFormResponses;

        return $this;
    }

    #[Groups(['read_session', 'read_lobby_signature'])]
    public function isDistantial(): bool
    {
        return $this->situationMixte == self::SITUATION_MIXTE_DISTANTIAL;
    }

    #[Groups(['read_session', 'read_lobby_signature'])]
    public function isPresential(): bool
    {
        return $this->situationMixte == self::SITUATION_MIXTE_PRESENTIAL;
    }

    #[Groups([ProviderSessionApiController::GROUP_VIEW_PRESENCES, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    public function getPresenceType(): PresenceType
    {
        if ($this->isPresent() && $this->isManualPresenceStatus() && $this->signatures->isEmpty()) {
            return PresenceType::Manual;
        } elseif ($this->isPresent() && $this->isManualPresenceStatus() && !$this->signatures->isEmpty()) {
            return PresenceType::Signatures;
        } elseif ($this->isPresent() && !$this->isManualPresenceStatus()) {
            return PresenceType::Automatic;
        } else {
            return PresenceType::None;
        }
    }

    public static function getSituationMixteHumanized(int $situation): string
    {
        return match ($situation) {
            self::SITUATION_MIXTE_DISTANTIAL => 'Distancial',
            self::SITUATION_MIXTE_PRESENTIAL => 'Presential',
            default => 'Unknown',
        };
    }

    public function getGroupsParticipants(): array
    {
        return $this->groupsParticipants;
    }

    public function setGroupsParticipants(array $groupsParticipants): self
    {
        $this->groupsParticipants = $groupsParticipants;

        return $this;
    }

    public function getEvaluationSessionAnswers(): Collection
    {
        return $this->evaluationSessionAnswers;
    }

    public function setEvaluationSessionAnswers(Collection $evaluationSessionAnswers): ProviderParticipantSessionRole
    {
        $this->evaluationSessionAnswers = $evaluationSessionAnswers;

        return $this;
    }

    public function isRemotePresenceStatus(): bool
    {
        return $this->remotePresenceStatus;
    }

    public function setRemotePresenceStatus(bool $remotePresenceStatus): ProviderParticipantSessionRole
    {
        $this->remotePresenceStatus = $remotePresenceStatus;

        return $this;
    }
}
