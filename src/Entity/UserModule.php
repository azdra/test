<?php

namespace App\Entity;

use App\SelfSubscription\Entity\Module;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class UserModule
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_user_module'])]
    private int $id;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_user_module'])]
    private \DateTime $createdAt;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_user_module'])]
    private \DateTime $lastAuthenticationAt;

    #[ORM\Column(type: 'string')]
    private string $password;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'userModules')]
    #[Groups(['read_user_module'])]
    private User $user;

    #[ORM\ManyToOne(targetEntity: Module::class, inversedBy: 'userModules')]
    private Module $module;

    #[ORM\Column(type: 'json')]
    #[Groups(['read_user_module'])]
    private ?array $registrationFormCatalogResponses = [];

    public function __construct(User $user, Module $module)
    {
        $this->user = $user;
        $this->module = $module;
        $this->createdAt = new \DateTime();
        $this->lastAuthenticationAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): UserModule
    {
        $this->id = $id;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): UserModule
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getLastAuthenticationAt(): \DateTime
    {
        return $this->lastAuthenticationAt;
    }

    public function setLastAuthenticationAt(\DateTime $lastAuthenticationAt): UserModule
    {
        $this->lastAuthenticationAt = $lastAuthenticationAt;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): UserModule
    {
        $this->user = $user;

        return $this;
    }

    public function getModule(): Module
    {
        return $this->module;
    }

    public function setModule(Module $module): UserModule
    {
        $this->module = $module;

        return $this;
    }

    public function getRegistrationFormCatalogResponses(): ?array
    {
        return $this->registrationFormCatalogResponses;
    }

    public function setRegistrationFormCatalogResponses(?array $registrationFormCatalogResponses): UserModule
    {
        $this->registrationFormCatalogResponses = $registrationFormCatalogResponses;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): UserModule
    {
        $this->password = $password;

        return $this;
    }
}
