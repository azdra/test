<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class ProviderSessionReplay
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['provider_session_replay'])]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['provider_session_replay'])]
    private string $firstname;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['provider_session_replay'])]
    private string $lastname;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['provider_session_replay'])]
    private string $email;

    #[ORM\Column(type: 'integer')]
    #[Groups(['provider_session_replay'])]
    private int $seen = 0;

    #[ORM\Column(type: 'json')]
    #[Groups(['read_session'])]
    private ?array $registrationFormResponses = [];

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['read_session'])]
    private ?\DateTimeImmutable $lastShowReplay = null;

    #[ORM\Embedded(class: ProviderSessionAccessToken::class)]
    #[Groups(['provider_session_replay'])]
    protected ProviderSessionAccessToken $providerReplayAccessToken;

    #[ORM\ManyToOne(targetEntity: ProviderSession::class, inversedBy: 'replays')]
    protected ProviderSession $session;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSeen(): int
    {
        return $this->seen;
    }

    public function setSeen(int $seen): self
    {
        $this->seen = $seen;

        return $this;
    }

    public function incrementSeen(): void
    {
        ++$this->seen;
    }

    public function getProviderReplayAccessToken(): ProviderSessionAccessToken
    {
        return $this->providerReplayAccessToken;
    }

    public function setProviderReplayAccessToken(ProviderSessionAccessToken $providerReplayAccessToken): self
    {
        $this->providerReplayAccessToken = $providerReplayAccessToken;

        return $this;
    }

    public function getSession(): ProviderSession
    {
        return $this->session;
    }

    public function setSession(ProviderSession $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getRegistrationFormResponses(): ?array
    {
        return $this->registrationFormResponses;
    }

    public function setRegistrationFormResponses(?array $registrationFormResponses): self
    {
        $this->registrationFormResponses = $registrationFormResponses;

        return $this;
    }

    public function getLastShowReplay(): ?\DateTimeImmutable
    {
        return $this->lastShowReplay;
    }

    public function setLastShowReplay(?\DateTimeImmutable $lastShowReplay): self
    {
        $this->lastShowReplay = $lastShowReplay;

        return $this;
    }
}
