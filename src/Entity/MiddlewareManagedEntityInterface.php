<?php

namespace App\Entity;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;

interface MiddlewareManagedEntityInterface
{
    public function getAbstractProviderConfigurationHolder(): AbstractProviderConfigurationHolder;
}
