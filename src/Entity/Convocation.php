<?php

namespace App\Entity;

use App\Controller\Api\ClientApiController;
use App\Controller\Api\ProviderSessionApiController;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Validator as AppAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[UniqueEntity(fields: ['name', 'client'], message: 'This convocation name already exists with this client', errorPath: 'name')]
#[ORM\UniqueConstraint(name: 'convocation_unicity_name_and_client', columns: ['name', 'client_id'])]
class Convocation
{
    public const STATUS_DISABLED = false;
    public const STATUS_ENABLED = true;

    public const TYPE_ADOBE_CONNECT = 'adobe-connect';
    public const TYPE_ADOBE_CONNECT_WEBINAR = 'adobe-connect-webinar';
    public const TYPE_CISCO_WEBEX = 'cisco-webex';
    public const TYPE_CISCO_WEBEX_REST = 'cisco-webex-rest';
    public const TYPE_MICROSOFT_TEAMS = 'microsoft-teams';
    public const TYPE_MICROSOFT_TEAMS_EVENT = 'microsoft-teams-event';
    public const TYPE_WHITE_CONNECTOR = 'white-connector';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column]
    #[Groups(['read_session', 'read_convocation', ProviderSessionApiController::GROUP_VIEW, ClientApiController::GROUP_VIEW])]
    private int $id;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'convocations')]
    private Client $client;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Groups(['read_convocation', 'read_session', ClientApiController::GROUP_VIEW, ProviderSessionApiController::GROUP_VIEW])]
    private string $name;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank]
    #[AppAssert\ConstraintShortCode]
    #[Groups(['read_convocation'])]
    private string $subjectMail;

    #[ORM\Column(nullable: true)]
    #[Groups(['read_convocation'])]
    private ?CategorySessionType $category;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Assert\NotBlank(allowNull: true)]
    #[AppAssert\ConstraintShortCode]
    #[Groups(['read_convocation'])]
    private ?string $content = null;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank]
    #[Groups(['read_convocation', 'read_session'])]
    private string $languages = 'fr';

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Groups(['read_convocation', 'read_session'])]
    private bool $status = self::STATUS_ENABLED;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Choice([
        AbstractProviderAudio::AUDIO_INTERCALL,
        AbstractProviderAudio::AUDIO_PGI,
        AbstractProviderAudio::AUDIO_VOIP,
        AbstractProviderAudio::AUDIO_CALLIN,
        AbstractProviderAudio::AUDIO_PGI_EMEA,
        AbstractProviderAudio::AUDIO_MEETINGONE_EMEA,
        AbstractProviderAudio::AUDIO_MEETINGONE_NA,
    ])]
    #[Groups(['read_convocation', 'read_session'])]
    private ?string $typeAudio = AbstractProviderAudio::AUDIO_VOIP;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Assert\Choice([
        self::TYPE_ADOBE_CONNECT,
        self::TYPE_ADOBE_CONNECT_WEBINAR,
        self::TYPE_CISCO_WEBEX,
        self::TYPE_CISCO_WEBEX_REST,
        self::TYPE_MICROSOFT_TEAMS,
        self::TYPE_MICROSOFT_TEAMS_EVENT,
        self::TYPE_WHITE_CONNECTOR,
    ])]
    #[Groups(['read_convocation', 'read_session', ClientApiController::GROUP_VIEW])]
    private ?string $connectorType = self::TYPE_ADOBE_CONNECT;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Choice([
        AbstractProviderAudio::AUDIO_CATEGORY_PHONE,
        AbstractProviderAudio::AUDIO_CATEGORY_COMPUTER,
        AbstractProviderAudio::AUDIO_CATEGORY_HYBRIDE,
        AbstractProviderAudio::AUDIO_CATEGORY_PRESENTIAL,
    ])]
    #[Groups(['read_convocation', 'read_session'])]
    private ?string $categoryTypeAudio = AbstractProviderAudio::AUDIO_CATEGORY_COMPUTER;

    #[ORM\OneToMany(mappedBy: 'convocation', targetEntity: WebinarConvocation::class, cascade: ['all'])]
    #[Groups(['read_session', ClientApiController::GROUP_VIEW])]
    private Collection $webinarConvocation;

    #[ORM\Column]
    #[Assert\NotNull]
    #[Groups(['read_convocation', 'read_session'])]
    private bool $ics = false;

    public function __construct()
    {
        $this->webinarConvocation = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): void
    {
        $this->client = $client;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSubjectMail(): string
    {
        return $this->subjectMail;
    }

    public function setSubjectMail(string $subjectMail): self
    {
        $this->subjectMail = $subjectMail;

        return $this;
    }

    public function getCategory(): ?CategorySessionType
    {
        return $this->category;
    }

    public function setCategory(?CategorySessionType $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getLanguages(): string
    {
        return $this->languages;
    }

    public function setLanguages(string $languages): void
    {
        $this->languages = $languages;
    }

    public function getStatus(): bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    public function getTypeAudio(): ?string
    {
        return $this->typeAudio;
    }

    public function setTypeAudio(?string $typeAudio): Convocation
    {
        $this->typeAudio = $typeAudio;

        return $this;
    }

    public function getCategoryTypeAudio(): ?string
    {
        return $this->categoryTypeAudio;
    }

    public function setCategoryTypeAudio(?string $categoryTypeAudio): Convocation
    {
        $this->categoryTypeAudio = $categoryTypeAudio;

        return $this;
    }

    public function getConnectorType(): ?string
    {
        return $this->connectorType;
    }

    public function setConnectorType(?string $connectorType): self
    {
        $this->connectorType = $connectorType;

        return $this;
    }

    public function getWebinarConvocation(): Collection
    {
        return $this->webinarConvocation;
    }

    public function setWebinarConvocation(Collection $webinarConvocation): self
    {
        $this->webinarConvocation = $webinarConvocation;

        return $this;
    }

    public function getIcs(): bool
    {
        return $this->ics;
    }

    public function setIcs(bool $ics): void
    {
        $this->ics = $ics;
    }
}
