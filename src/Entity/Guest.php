<?php

namespace App\Entity;

use App\Entity\Client\Client;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[UniqueEntity(fields: ['email', 'client'], message: 'This address email already exist with this client', errorPath: 'email')]
class Guest
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_user', 'read_session'])]
    private int $id;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_user', 'read_session'])]
    private ?string $firstName = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_user', 'read_session'])]
    private ?string $lastName = null;

    #[ORM\Column(type: 'string', length: 180)]
    #[Groups(['read_user', 'read_session'])]
    private string $email;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_user', 'read_session'])]
    private DateTimeInterface $createdAt;

    #[ORM\Column(type: 'boolean', nullable: false)]
    #[Groups(['read_session'])]
    private bool $emailSent = false;

    #[ORM\Column(type: 'boolean', nullable: false)]
    #[Groups(['read_user', 'read_session'])]
    private bool $unsubscribe = false;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read_user', 'read_session'])]
    private int $opened = 0;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read_user', 'read_session'])]
    private int $clicked = 0;

    #[ORM\Column]
    #[Groups(['read_session'])]
    private string $token;

    #[ORM\Column(type: 'boolean', nullable: false)]
    #[Groups(['read_session'])]
    private bool $isBounce = false;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'guests')]
    #[Groups(['read_user'])]
    private Client $client;

    #[ORM\ManyToOne(targetEntity: WebinarConvocation::class, inversedBy: 'guests')]
    private WebinarConvocation $webinarConvocation;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->token = \md5(\random_bytes(64));
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function isEmailSent(): bool
    {
        return $this->emailSent;
    }

    public function setEmailSent(bool $emailSent): self
    {
        $this->emailSent = $emailSent;

        return $this;
    }

    public function isUnsubscribe(): bool
    {
        return $this->unsubscribe;
    }

    public function setUnsubscribe(bool $unsubscribe): self
    {
        $this->unsubscribe = $unsubscribe;

        return $this;
    }

    public function getWebinarConvocation(): WebinarConvocation
    {
        return $this->webinarConvocation;
    }

    public function setWebinarConvocation(WebinarConvocation $webinarConvocation): self
    {
        $this->webinarConvocation = $webinarConvocation;

        return $this;
    }

    public function getOpened(): int
    {
        return $this->opened;
    }

    public function setOpened(int $opened): self
    {
        $this->opened = $opened;

        return $this;
    }

    public function getClicked(): int
    {
        return $this->clicked;
    }

    public function setClicked(int $clicked): self
    {
        $this->clicked = $clicked;

        return $this;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getFullName(): string
    {
        return "{$this->getLastName()} {$this->getFirstName()}";
    }

    public function isBounce(): bool
    {
        return $this->isBounce;
    }

    public function setIsBounce(bool $isBounce): self
    {
        $this->isBounce = $isBounce;

        return $this;
    }
}
