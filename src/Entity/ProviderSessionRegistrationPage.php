<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Embeddable]
class ProviderSessionRegistrationPage
{
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?int $places = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $description = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $description2 = null;

    public function getPlaces(): ?int
    {
        return $this->places;
    }

    public function setPlaces(?int $places): self
    {
        $this->places = $places;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription2(): ?string
    {
        return $this->description2;
    }

    public function setDescription2(?string $description2): self
    {
        $this->description2 = $description2;

        return $this;
    }
}
