<?php

namespace App\Entity;

enum CategorySessionType : int {
    case CATEGORY_SESSION_FORMATION = 1;
    case CATEGORY_SESSION_REUNION = 2;
    case CATEGORY_SESSION_TEST = 3;
    case CATEGORY_SESSION_PRESENTIAL = 4;
    case CATEGORY_SESSION_SERVICES = 5;
    case CATEGORY_SESSION_MIXTE = 6;
    case CATEGORY_SESSION_WEBINAR = 7;
    public function label(): string
    {
        return match ($this) {
            CategorySessionType::CATEGORY_SESSION_FORMATION => 'Formation',
            CategorySessionType::CATEGORY_SESSION_REUNION => 'Meeting',
            CategorySessionType::CATEGORY_SESSION_TEST => 'Test',
            CategorySessionType::CATEGORY_SESSION_PRESENTIAL => 'Face-to-face',
            CategorySessionType::CATEGORY_SESSION_SERVICES => 'Services',
            CategorySessionType::CATEGORY_SESSION_MIXTE => 'Mixed',
            CategorySessionType::CATEGORY_SESSION_WEBINAR => 'Webinar',
        };
    }
}
