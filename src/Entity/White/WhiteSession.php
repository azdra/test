<?php

namespace App\Entity\White;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Entity\ProviderSession;
use App\Service\MiddlewareService;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class WhiteSession extends ProviderSession
{
    protected static string $provider = MiddlewareService::SERVICE_TYPE_WHITE;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['read_session'])]
    private ?string $joinSessionWebUrl = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['read_session'])]
    private ?string $connectionInformation = null;

    #[ORM\ManyToMany(targetEntity: WhiteParticipant::class, mappedBy: 'meetings')]
    protected Collection $participants;

    protected array $candidates = [];

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof WhiteConfigurationHolder;
    }

    #[Groups(['read_session'])]
    public function getJoinSessionWebUrl(): ?string
    {
        return $this->joinSessionWebUrl;
    }

    public function setJoinSessionWebUrl(?string $joinSessionWebUrl): WhiteSession
    {
        $this->joinSessionWebUrl = $joinSessionWebUrl;

        return $this;
    }

    #[Groups(['read_session'])]
    public function getConnectionInformation(): ?string
    {
        return $this->connectionInformation;
    }

    public function setConnectionInformation(?string $connectionInformation): WhiteSession
    {
        $this->connectionInformation = $connectionInformation;

        return $this;
    }

    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function setParticipants(Collection $participants): WhiteSession
    {
        $this->participants = $participants;

        return $this;
    }

    public function getProviderDistantID(): ?string
    {
        return null;
    }

    public function getCandidates(): array
    {
        return $this->candidates;
    }

    public function setCandidates(array $candidates): WhiteSession
    {
        $this->candidates = $candidates;

        return $this;
    }
}
