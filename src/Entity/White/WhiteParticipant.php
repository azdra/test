<?php

namespace App\Entity\White;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Service\MiddlewareService;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity]
class WhiteParticipant extends ProviderParticipant
{
    protected static string $provider = MiddlewareService::SERVICE_TYPE_WHITE;

    public const ROLE_ATTENDEE = 'attendee';

    public const ROLE_PRESENTER = 'presenter';

    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $contactIdentifier = null;

    #[ORM\ManyToMany(targetEntity: WhiteSession::class, inversedBy: 'participants')]
    #[Ignore]
    protected Collection $meetings;

    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    #[Groups(['read_white_connector_user'])]
    protected bool $active = true;

    /**
     * Unmapped. Used to create a participant. We don't store user password.
     */
    protected string $password = '';

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof WhiteConfigurationHolder;
    }

    #[Groups(['read_session'])]
    public function getName(): string
    {
        return $this->getUser()->getFirstName().' '.$this->getUser()->getLastName();
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): WhiteParticipant
    {
        $this->password = $password;

        return $this;
    }

    public function getContactIdentifier(): ?int
    {
        return $this->contactIdentifier;
    }

    public function setContactIdentifier(?int $contactIdentifier): WhiteParticipant
    {
        $this->contactIdentifier = $contactIdentifier;

        return $this;
    }

    public function getMeetings(): Collection
    {
        return $this->meetings;
    }

    public function setMeetings(Collection $meetings): WhiteParticipant
    {
        $this->meetings = $meetings;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): WhiteParticipant
    {
        $this->active = $active;

        return $this;
    }

    public function getRole(): string
    {
        return is_null($this->contactIdentifier)
            ? self::ROLE_ATTENDEE
            : self::ROLE_PRESENTER;
    }
}
