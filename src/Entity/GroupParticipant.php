<?php

namespace App\Entity;

use App\Entity\Client\Client;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class GroupParticipant
{
    public const MAX_RESULT_LIST = 10;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['ui:group:read', 'read_groups', 'read_session'])]
    private int $id;

    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(['ui:group:read', 'read_groups', 'read_session'])]
    private string $name;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['ui:group:read', 'read_groups'])]
    private DateTimeInterface $createdAt;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['ui:group:read', 'read_groups'])]
    private DateTimeInterface $lastUpdate;

    #[ORM\Column(type: 'boolean', nullable: false)]
    #[Groups(['ui:group:read', 'read_groups', 'read_session'])]
    private bool $active = true;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'groups')]
    #[Groups(['read_groups'])]
    protected Client $client;

    #[ORM\ManyToOne(targetEntity: GroupParticipant::class, inversedBy: 'subgroups')]
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id')]
    #[Groups(['ui:group:read', 'read_groups'])]
    private ?GroupParticipant $parent;

    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: GroupParticipant::class)]
    private Collection $subgroups;

    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'groups')]
    #[Groups(['ui:group:read', 'read_groups', 'read_users'])]
    private Collection $users;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->lastUpdate = new DateTime();
        $this->subgroups = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getLastUpdate(): DateTimeInterface
    {
        return $this->lastUpdate;
    }

    public function setLastUpdate(DateTimeInterface $lastUpdate): self
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getParent(): ?GroupParticipant
    {
        return $this->parent;
    }

    public function setParent(?GroupParticipant $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getSubgroups(): Collection
    {
        return $this->subgroups;
    }

    public function setSubgroups(Collection $subgroups): self
    {
        $this->subgroups = $subgroups;

        return $this;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function setUsers(Collection $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function addUser(User $user): self
    {
        $this->users->add($user);

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeGroup($this); // S'assure que le groupe est également retiré de l'utilisateur
        }

        return $this;
    }

    #[Groups(['read_session'])]
    public function getNumberUsers(): int
    {
        return $this->users->count();
    }

    public function getNumberSubgroups(): int
    {
        return $this->subgroups->count();
    }

    #[Groups(['read_session'])]
    public function getEmailsUsers(): array
    {
        $emails = [];
        foreach ($this->users as $user) {
            $emails[] = $user->getEmail();
        }

        return $emails;
    }

    #[Groups(['read_session'])]
    public function getIdsUsers(): array
    {
        $ids = [];
        foreach ($this->users as $user) {
            $ids[] = $user->getId();
        }

        return $ids;
    }
}
