<?php

namespace App\Entity;

use App\Validator as AppAssert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class WidgetNews
{
    public const COLOR_RED = 'red';
    public const COLOR_GREEN = 'green';
    public const COLOR_BLUE = 'blue';

    public const FRENCH = 'fr';
    public const ENGLISH = 'en';

    public const LANGUAGES_LIST = [
        self::FRENCH,
        self::ENGLISH,
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column]
    #[Groups(['read_widget_news'])]
    private int $id;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank]
    #[AppAssert\ConstraintShortCode]
    #[Groups(['read_widget_news'])]
    private string $title;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['read_widget_news'])]
    protected \DateTimeImmutable $date;

    #[ORM\Column(type: 'text')]
    #[AppAssert\ConstraintShortCode]
    #[Groups(['read_widget_news'])]
    private string $content;

    #[ORM\Column(type: 'string')]
    #[Assert\Choice([
        self::COLOR_RED,
        self::COLOR_GREEN,
        self::COLOR_BLUE,
    ])]
    #[Groups(['read_widget_news'])]
    private string $color = self::COLOR_BLUE;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank]
    #[Groups(['read_widget_news'])]
    private string $languages = 'fr';

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'widgetNews')]
    #[Groups(['read_widget_news'])]
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getLanguages(): string
    {
        return $this->languages;
    }

    public function setLanguages(string $languages): self
    {
        $this->languages = $languages;

        return $this;
    }
}
