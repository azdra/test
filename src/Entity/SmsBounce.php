<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class SmsBounce
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_bounce', 'read_bounce_session_list'])]
    private int $id;

    #[ORM\Column(type: 'string')]
    #[Assert\Email()]
    #[Groups(['read_bounce', 'read_participant'])]
    private string $phone;

    #[ORM\Column(type: 'string')]
    #[Assert\Email()]
    #[Groups(['read_bounce', 'read_participant'])]
    private string $status;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_bounce'])]
    private \DateTimeInterface $createdAt;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_bounce'])]
    private \DateTimeInterface $dateSend;

    #[ORM\ManyToOne(targetEntity: ProviderSession::class, cascade: ['persist'], inversedBy: 'smsBounces')]
    #[ORM\JoinColumn(name: 'session_id', referencedColumnName: 'id', nullable: true)]
    #[Groups(['read_bounce', 'read_participant', 'read_bounce_session_list'])]
    private ?ProviderSession $session = null;

    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['persist'], inversedBy: 'smsBounces')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    #[Groups(['read_bounce', 'read_participant'])]
    private User $user;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getSession(): ?ProviderSession
    {
        return $this->session;
    }

    public function setSession(?ProviderSession $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDateSend(): \DateTimeInterface
    {
        return $this->dateSend;
    }

    public function setDateSend(\DateTimeInterface $dateSend): self
    {
        $this->dateSend = $dateSend;

        return $this;
    }
}
