<?php

namespace App\Entity;

use App\Controller\Api\ProviderSessionApiController;
use App\Controller\Api\UserApiController;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class ParticipantSessionSignature
{
    public const AM = 'AM';
    public const PM = 'PM';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'read_lobby_signature'])]
    private int $id;

    #[ORM\ManyToOne(targetEntity: ProviderParticipantSessionRole::class, inversedBy: 'signatures')]
    private ProviderParticipantSessionRole $providerParticipantSessionRole;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_session', 'read_lobby_signature', ProviderSessionApiController::GROUP_VIEW, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    private \DateTime $createdAt;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_session', 'read_lobby_signature', ProviderSessionApiController::GROUP_VIEW, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    private \DateTime $signatureForTheSessionOf;

    #[ORM\Column(type: 'string')]
    #[Assert\Choice([
        self::AM,
        self::PM,
    ])]
    #[Groups(['read_session', 'read_lobby_signature', ProviderSessionApiController::GROUP_VIEW, UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    private string $dayPartIndicator;

    #[ORM\Column(type: 'blob')]
    #[Groups(['read_session', 'read_lobby_signature'])]
    private mixed $signature;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getProviderParticipantSessionRole(): ProviderParticipantSessionRole
    {
        return $this->providerParticipantSessionRole;
    }

    public function setProviderParticipantSessionRole(ProviderParticipantSessionRole $providerParticipantSessionRole): self
    {
        $this->providerParticipantSessionRole = $providerParticipantSessionRole;

        return $this;
    }

    public function getSignature(): mixed
    {
        if (is_resource($this->signature)) {
            return stream_get_contents($this->signature);
        }

        return $this->signature;
    }

    public function setSignature(string $signature): self
    {
        $this->signature = $signature;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getSignatureForTheSessionOf(): \DateTime
    {
        return $this->signatureForTheSessionOf;
    }

    public function setSignatureForTheSessionOf(\DateTime $signatureForTheSessionOf): self
    {
        $this->signatureForTheSessionOf = $signatureForTheSessionOf;

        return $this;
    }

    public function getDayPartIndicator(): string
    {
        return $this->dayPartIndicator;
    }

    public function setDayPartIndicator(string $dayPartIndicator): self
    {
        $this->dayPartIndicator = $dayPartIndicator;

        return $this;
    }
}
