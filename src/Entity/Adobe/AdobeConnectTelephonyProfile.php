<?php

namespace App\Entity\Adobe;

use App\Controller\Api\ProviderSessionApiController;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class AdobeConnectTelephonyProfile extends AbstractProviderAudio
{
    #[ORM\ManyToOne(targetEntity: AdobeConnectPrincipal::class, inversedBy: 'telephonyProfiles')]
    protected ?AdobeConnectPrincipal $linkedPrincipal;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_audio', ProviderSessionApiController::GROUP_VIEW])]
    protected ?string $adobeDefaultPhoneNumber = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_audio', ProviderSessionApiController::GROUP_VIEW])]
    private ?string $codeAnimator = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_audio', ProviderSessionApiController::GROUP_VIEW])]
    private ?string $codeParticipant = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_audio', ProviderSessionApiController::GROUP_VIEW])]
    private ?string $meetingOneConferenceId = null;

    public function getLinkedPrincipal(): ?AdobeConnectPrincipal
    {
        return $this->linkedPrincipal;
    }

    public function setLinkedPrincipal(?AdobeConnectPrincipal $linkedPrincipal): AdobeConnectTelephonyProfile
    {
        $this->linkedPrincipal = $linkedPrincipal;

        return $this;
    }

    public static function getAvailableConnectionType(): array
    {
        return [
            self::AUDIO_INTERCALL,
            self::AUDIO_PGI,
            self::AUDIO_VOIP,
            self::AUDIO_PGI_EMEA,
            self::AUDIO_MEETINGONE_EMEA,
            self::AUDIO_MEETINGONE_NA,
        ];
    }

    public function getAdobeDefaultPhoneNumber(): ?string
    {
        return $this->adobeDefaultPhoneNumber;
    }

    public function setAdobeDefaultPhoneNumber(?string $adobeDefaultPhoneNumber): AdobeConnectTelephonyProfile
    {
        $this->adobeDefaultPhoneNumber = $adobeDefaultPhoneNumber;

        return $this;
    }

    public function getCodeAnimator(): ?string
    {
        return $this->codeAnimator;
    }

    public function setCodeAnimator(?string $codeAnimator): AdobeConnectTelephonyProfile
    {
        $this->codeAnimator = $codeAnimator;

        return $this;
    }

    public function getCodeParticipant(): ?string
    {
        return $this->codeParticipant;
    }

    public function setCodeParticipant(?string $codeParticipant): AdobeConnectTelephonyProfile
    {
        $this->codeParticipant = $codeParticipant;

        return $this;
    }

    public function getMeetingOneConferenceId(): ?string
    {
        return $this->meetingOneConferenceId;
    }

    public function setMeetingOneConferenceId(?string $meetingOneConferenceId): AdobeConnectTelephonyProfile
    {
        $this->meetingOneConferenceId = $meetingOneConferenceId;

        return $this;
    }
}
