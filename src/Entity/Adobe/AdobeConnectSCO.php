<?php

namespace App\Entity\Adobe;

use App\Controller\Api\ProviderSessionApiController;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use App\Service\MiddlewareService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity]
#[ORM\Index(columns: ['unique_sco_identifier'])]
#[UniqueEntity('name')]
class AdobeConnectSCO extends ProviderSession
{
    protected static string $provider = MiddlewareService::SERVICE_TYPE_ADOBE_CONNECT;

    public const TYPE_SESSION = 'session';
    public const TYPE_MEETING = 'meeting';
    public const TYPE_FOLDER = 'folder';
    public const TYPE_CONTENT = 'content';

    /**
     * Nullable type declaration in order to create a new one through AdobeConnect and fetch this identifier, but
     * forbidden to store a SCO without an unique identifier.
     */
    #[ORM\Column(type: 'bigint', nullable: false)]
    #[Groups(['read_session'])]
    #[NotNull(groups: ['update'])]
    private ?int $uniqueScoIdentifier = null;

    #[ORM\Column(type: 'bigint', nullable: true)]
    #[Groups(['read_session'])]
    #[NotNull(groups: ['update'])]
    #[Assert\NotBlank(allowNull: true)]
    private ?int $parentScoIdentifier = null;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW])]
    #[NotNull(groups: ['update'])]
    private ?string $urlPath = null;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read_session'])]
    private bool $isSeminar = false;

    #[ORM\Column(type: 'string')]
    #[Groups(['read'])]
    #[Choice(choices: [
        self::TYPE_CONTENT,
        self::TYPE_FOLDER,
        self::TYPE_SESSION,
        self::TYPE_MEETING,
        ], groups: ['create', 'update']
    )]
    protected string $type = self::TYPE_CONTENT;

    #[ORM\Column(type: 'string')]
    #[Groups(['read', 'read_session', ProviderSessionApiController::GROUP_VIEW])]
    #[Choice(choices: [
        AdobeConnectConnector::GRANT_ACCESS_USER_ONLY,
        AdobeConnectConnector::GRANT_ACCESS_VISITOR,
        AdobeConnectConnector::GRANT_ACCESS_ALL,
    ], groups: ['create', 'update']
    )]
    private string $admittedSession = AdobeConnectConnector::GRANT_ACCESS_USER_ONLY;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW])]
    protected ?string $licence = null;

    #[ORM\Column(type: 'bigint', nullable: true)]
    #[Groups(['read_session'])]
    private ?int $roomModel = null;

    #[ORM\ManyToMany(targetEntity: AdobeConnectPrincipal::class, mappedBy: 'linkedSCOs', cascade: ['all'])]
    protected Collection $principals;

    #[ORM\OneToMany(mappedBy: 'adobeConnectSCO', targetEntity: AdobeConnectMeetingAttendee::class, cascade: ['all'])]
    protected Collection $attendees;

    public function __construct(AbstractProviderConfigurationHolder $configurationHolder)
    {
        parent::__construct($configurationHolder);
        $this->principals = new ArrayCollection();
        $this->attendees = new ArrayCollection();
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof AdobeConnectConfigurationHolder;
    }

    public function getUniqueScoIdentifier(): ?int
    {
        return $this->uniqueScoIdentifier;
    }

    public function setUniqueScoIdentifier(?int $uniqueScoIdentifier): AdobeConnectSCO
    {
        $this->uniqueScoIdentifier = $uniqueScoIdentifier;

        return $this;
    }

    public function getParentScoIdentifier(): ?int
    {
        return $this->parentScoIdentifier;
    }

    public function setParentScoIdentifier($parentScoIdentifier): AdobeConnectSCO
    {
        $this->parentScoIdentifier = $parentScoIdentifier;

        return $this;
    }

    public function getUrlPath(): ?string
    {
        return $this->urlPath;
    }

    public function setUrlPath(?string $urlPath): AdobeConnectSCO
    {
        $this->urlPath = $urlPath;

        return $this;
    }

    #[Groups(['read_session'])]
    public function getFullUrlpath(): ?string
    {
        return 'https://'.$this->getAbstractProviderConfigurationHolder()->getDomain().$this->urlPath;
    }

    public function isSeminar(): bool
    {
        return $this->isSeminar;
    }

    public function setIsSeminar(bool $isSeminar): AdobeConnectSCO
    {
        $this->isSeminar = $isSeminar;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): AdobeConnectSCO
    {
        if (!\in_array($type, self::getAvailableTypes())) {
            throw new \LogicException(sprintf('SCO type %s does not exist, or has not yet been implemented.', $type));
        }

        $this->type = $type;

        return $this;
    }

    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_SESSION,
            self::TYPE_FOLDER,
            self::TYPE_MEETING,
            self::TYPE_CONTENT,
        ];
    }

    public function getRoomModel(): ?int
    {
        return $this->roomModel;
    }

    public function setRoomModel(?int $roomModel): AdobeConnectSCO
    {
        $this->roomModel = $roomModel;

        return $this;
    }

    public function getAdmittedSession(): string
    {
        return $this->admittedSession;
    }

    public function setAdmittedSession(string $admittedSession): AdobeConnectSCO
    {
        if (!\in_array($admittedSession, self::getAvailableAdmittedSession())) {
            throw new \LogicException(sprintf('SCO admitted %s does not exist, or has not yet been implemented.', $admittedSession));
        }

        $this->admittedSession = $admittedSession;

        return $this;
    }

    public static function getAvailableAdmittedSession(): array
    {
        return [
            AdobeConnectConnector::GRANT_ACCESS_USER_ONLY,
            AdobeConnectConnector::GRANT_ACCESS_VISITOR,
            AdobeConnectConnector::GRANT_ACCESS_ALL,
        ];
    }

    public function getPrincipals(): ArrayCollection|Collection
    {
        return $this->principals;
    }

    public function setPrincipals(ArrayCollection|Collection $principals): AdobeConnectSCO
    {
        $this->principals = $principals;

        return $this;
    }

    public function addPrincipal(AdobeConnectPrincipal $principal): AdobeConnectSCO
    {
        if (!$this->principals->contains($principal)) {
            $this->principals->add($principal);
            $principal->addLinkedSCO($this);
        }

        return $this;
    }

    public function removePrincipal(AdobeConnectPrincipal $principal): AdobeConnectSCO
    {
        if ($this->principals->contains($principal)) {
            $this->principals->add($principal);
            $principal->removeLinkedSCO($this);
        }

        return $this;
    }

    public function getAttendees(): Collection
    {
        return $this->attendees;
    }

    public function setAttendees(Collection $attendees): AdobeConnectSCO
    {
        $this->attendees = $attendees;

        return $this;
    }

    public function addAttendee(AdobeConnectMeetingAttendee $attendee): AdobeConnectSCO
    {
        if (!$this->attendees->contains($attendee)) {
            $this->attendees->add($attendee);
            $attendee->setAdobeConnectSCO($this);
        }

        return $this;
    }

    public function removeAttendee(AdobeConnectMeetingAttendee $attendee): AdobeConnectSCO
    {
        if ($this->attendees->contains($attendee)) {
            $this->attendees->add($attendee);
            $attendee->setAdobeConnectSCO(null);
        }

        return $this;
    }

    public function getProviderDistantID(): ?string
    {
        return (string) $this->parentScoIdentifier;
    }

    public function getLicenceShared(): ?array
    {
        /** @var AdobeConnectConfigurationHolder $configurationHolder */
        $configurationHolder = $this->abstractProviderConfigurationHolder;
        $licences = $configurationHolder->getLicences();
        if (is_array($licences)) {
            $sharedLicenceEmails = array_keys(array_filter($licences, function (array $licence) { return $licence['shared']; }));
            $animatorEmails = array_map(function (ProviderParticipantSessionRole $animator) {return $animator->getEmail(); }, $this->getAnimatorsOnly());
            $subscribedLicences = array_values(array_intersect($sharedLicenceEmails, $animatorEmails));
            if (count($subscribedLicences) > 0) {
                return ['email' => $subscribedLicences[0], 'password' => $licences[$subscribedLicences[0]]['password']];
            }
        }

        return null;
    }

    public function getLicence(): ?string
    {
        return $this->licence;
    }

    public function setLicence(?string $licence): AdobeConnectSCO
    {
        $this->licence = $licence;

        return $this;
    }
}
