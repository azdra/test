<?php

namespace App\Entity\Adobe;

use App\Controller\Api\ProviderSessionApiController;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Service\MiddlewareService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity]
#[ORM\Index(columns: ['unique_sco_identifier'])]
#[UniqueEntity('name')]
class AdobeConnectWebinarSCO extends ProviderSession
{
    protected static string $provider = MiddlewareService::SERVICE_TYPE_ADOBE_CONNECT_WEBINAR;

    public const TYPE_WEBINAR = 'seminar';
    public const TYPE_MEETING = 'meeting';
    public const TYPE_FOLDER = 'folder';
    public const TYPE_CONTENT = 'content';

    public const AC_TYPE_NAME_SEMINAR = 'seminarsession';
    public const AC_TYPE_NAME_MEETING = 'meetings';

    public const AUDIO_OPERATION_VOIP = 'audio-operation-voip';
    public const AUDIO_OPERATION_TEL_ONLY = 'audio-operation-tel-only';
    public const AUDIO_OPERATION_HYBRID = 'audio-operation-hybrid';

    //public const GRANT_ACCESS_USER_ONLY = 'denied';
    //public const GRANT_ACCESS_VISITOR = 'remove';
    //public const GRANT_ACCESS_ALL = 'view-hidden';

    public const PERMISSION_VERB_VIEW = 'view';
    public const PERMISSION_VERB_HOST = 'host';
    public const PERMISSION_VERB_MINI_HOST = 'mini-host';
    public const PERMISSION_VERB_REMOVE = 'remove';
    public const PERMISSION_VERB_PUBLISH = 'publish';
    public const PERMISSION_VERB_MANAGE = 'manage';
    public const PERMISSION_VERB_DENIED = 'denied';
    public const PERMISSION_VERB_USER = 'user';
    public const PERMISSION_VERB_GUEST = 'guest';
    public const PERMISSION_VERB_VIEW_HIDDEN = 'view-hidden';

    /**
     * Nullable type declaration in order to create a new one through AdobeConnect and fetch this identifier, but
     * forbidden to store a SCO without an unique identifier.
     */
    #[ORM\Column(type: 'bigint', nullable: false)]
    #[Groups(['read_session'])]
    #[NotNull(groups: ['update'])]
    private ?int $uniqueScoIdentifier = null;

    #[ORM\Column(type: 'bigint', nullable: true)]
    #[Groups(['read_session'])]
    #[NotNull(groups: ['update'])]
    #[Assert\NotBlank(allowNull: true)]
    private ?int $parentScoIdentifier = null;

    #[ORM\Column(type: 'bigint', nullable: true)]
    #[Groups(['read_session'])]
    #[NotNull(groups: ['update'])]
    #[Assert\NotBlank(allowNull: true)]
    private ?int $seminarScoIdentifier = null;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW])]
    #[NotNull(groups: ['update'])]
    private ?string $urlPath = null;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read_session'])]
    private bool $isSeminar = false;

    #[ORM\Column(type: 'string')]
    #[Groups(['read'])]
    #[Choice(choices: [
        self::TYPE_CONTENT,
        self::TYPE_FOLDER,
        self::TYPE_WEBINAR,
        self::TYPE_MEETING,
        ], groups: ['create', 'update']
    )]
    protected string $type = self::TYPE_CONTENT;

    #[ORM\Column(type: 'string')]
    #[Groups(['read', 'read_session', ProviderSessionApiController::GROUP_VIEW])]
    #[Choice(choices: [
        self::PERMISSION_VERB_DENIED,
        self::PERMISSION_VERB_REMOVE,
        self::PERMISSION_VERB_VIEW_HIDDEN,
    ], groups: ['create', 'update']
    )]
    private string $admittedSession = AdobeConnectWebinarSCO::PERMISSION_VERB_VIEW_HIDDEN;

    #[ORM\Column(type: 'bigint', nullable: true)]
    #[Groups(['read_session'])]
    private ?int $roomModel = null;

    #[ORM\ManyToMany(targetEntity: AdobeConnectWebinarPrincipal::class, mappedBy: 'linkedSCOs', cascade: ['all'])]
    protected Collection $principals;

    #[ORM\OneToMany(mappedBy: 'adobeConnectWebinarSCO', targetEntity: AdobeConnectWebinarMeetingAttendee::class, cascade: ['all'])]
    protected Collection $attendees;

    public function __construct(AbstractProviderConfigurationHolder $configurationHolder)
    {
        parent::__construct($configurationHolder);
        $this->principals = new ArrayCollection();
        $this->attendees = new ArrayCollection();
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof AdobeConnectWebinarConfigurationHolder;
    }

    public function getUniqueScoIdentifier(): ?int
    {
        return $this->uniqueScoIdentifier;
    }

    public function setUniqueScoIdentifier(?int $uniqueScoIdentifier): AdobeConnectWebinarSCO
    {
        $this->uniqueScoIdentifier = $uniqueScoIdentifier;

        return $this;
    }

    public function getParentScoIdentifier(): ?int
    {
        return $this->parentScoIdentifier;
    }

    public function setParentScoIdentifier($parentScoIdentifier): AdobeConnectWebinarSCO
    {
        $this->parentScoIdentifier = $parentScoIdentifier;

        return $this;
    }

    public function getSeminarScoIdentifier(): ?int
    {
        return $this->seminarScoIdentifier;
    }

    public function setSeminarScoIdentifier($seminarScoIdentifier): AdobeConnectWebinarSCO
    {
        $this->seminarScoIdentifier = $seminarScoIdentifier;

        return $this;
    }

    public function getUrlPath(): ?string
    {
        return $this->urlPath;
    }

    public function setUrlPath(?string $urlPath): AdobeConnectWebinarSCO
    {
        $this->urlPath = $urlPath;

        return $this;
    }

    #[Groups(['read_session'])]
    public function getFullUrlpath(): ?string
    {
        return 'https://'.$this->getAbstractProviderConfigurationHolder()->getDomain().$this->urlPath;
    }

    public function isSeminar(): bool
    {
        return $this->isSeminar;
    }

    public function setIsSeminar(bool $isSeminar): AdobeConnectWebinarSCO
    {
        $this->isSeminar = $isSeminar;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): AdobeConnectWebinarSCO
    {
        if (!\in_array($type, self::getAvailableTypes())) {
            throw new \LogicException(sprintf('SCO type %s does not exist, or has not yet been implemented.', $type));
        }

        $this->type = $type;

        return $this;
    }

    public static function getAvailableTypes(): array
    {
        return [
            self::TYPE_WEBINAR,
            self::TYPE_FOLDER,
            self::TYPE_MEETING,
            self::TYPE_CONTENT,
        ];
    }

    public function getRoomModel(): ?int
    {
        return $this->roomModel;
    }

    public function setRoomModel(?int $roomModel): AdobeConnectWebinarSCO
    {
        $this->roomModel = $roomModel;

        return $this;
    }

    public function getAdmittedSession(): string
    {
        return $this->admittedSession;
    }

    public function setAdmittedSession(string $admittedSession): AdobeConnectWebinarSCO
    {
        if (!\in_array($admittedSession, self::getAvailableAdmittedSession())) {
            throw new \LogicException(sprintf('SCO admitted %s does not exist, or has not yet been implemented.', $admittedSession));
        }

        $this->admittedSession = $admittedSession;

        return $this;
    }

    public static function getAvailableAdmittedSession(): array
    {
        return [
            AdobeConnectWebinarSCO::PERMISSION_VERB_DENIED,
            AdobeConnectWebinarSCO::PERMISSION_VERB_REMOVE,
            AdobeConnectWebinarSCO::PERMISSION_VERB_VIEW_HIDDEN,
        ];
    }

    public function getPrincipals(): ArrayCollection|Collection
    {
        return $this->principals;
    }

    public function setPrincipals(ArrayCollection|Collection $principals): AdobeConnectWebinarSCO
    {
        $this->principals = $principals;

        return $this;
    }

    public function addPrincipal(AdobeConnectWebinarPrincipal $principal): AdobeConnectWebinarSCO
    {
        if (!$this->principals->contains($principal)) {
            $this->principals->add($principal);
            $principal->addLinkedSCO($this);
        }

        return $this;
    }

    public function removePrincipal(AdobeConnectWebinarPrincipal $principal): AdobeConnectWebinarSCO
    {
        if ($this->principals->contains($principal)) {
            $this->principals->add($principal);
            $principal->removeLinkedSCO($this);
        }

        return $this;
    }

    public function getAttendees(): Collection
    {
        return $this->attendees;
    }

    public function setAttendees(Collection $attendees): AdobeConnectWebinarSCO
    {
        $this->attendees = $attendees;

        return $this;
    }

    public function addAttendee(AdobeConnectWebinarMeetingAttendee $attendee): AdobeConnectWebinarSCO
    {
        if (!$this->attendees->contains($attendee)) {
            $this->attendees->add($attendee);
            $attendee->setAdobeConnectWebinarSCO($this);
        }

        return $this;
    }

    public function removeAttendee(AdobeConnectWebinarMeetingAttendee $attendee): AdobeConnectWebinarSCO
    {
        if ($this->attendees->contains($attendee)) {
            $this->attendees->add($attendee);
            $attendee->setAdobeConnectWebinarSCO(null);
        }

        return $this;
    }

    public function getProviderDistantID(): ?string
    {
        return (string) $this->parentScoIdentifier;
    }

    public function getLicenceShared(): ?array
    {
        /** @var AdobeConnectWebinarConfigurationHolder $configurationHolder */
        $configurationHolder = $this->abstractProviderConfigurationHolder;
        $licences = $configurationHolder->getLicences();
        if (is_array($licences)) {
            $sharedLicenceEmails = array_keys(array_filter($licences, function (array $licence) { return $licence['shared']; }));
            $animatorEmails = array_map(function (ProviderParticipantSessionRole $animator) {return $animator->getEmail(); }, $this->getAnimatorsOnly());
            $subscribedLicences = array_values(array_intersect($sharedLicenceEmails, $animatorEmails));
            if (count($subscribedLicences) > 0) {
                return ['email' => $subscribedLicences[0], 'password' => $licences[$subscribedLicences[0]]['password']];
            }
        }

        return null;
    }
}
