<?php

namespace App\Entity\Adobe;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AdobeConnectMeetingAttendee
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: AdobeConnectPrincipal::class)]
    protected AdobeConnectPrincipal $adobeConnectPrincipal;

    #[ORM\ManyToOne(targetEntity: AdobeConnectSCO::class, inversedBy: 'attendees')]
    protected ?AdobeConnectSCO $adobeConnectSCO;

    #[ORM\Column(type: 'datetime_immutable')]
    protected \DateTimeImmutable $sessionStart;

    #[ORM\Column(type: 'datetime_immutable')]
    protected \DateTimeImmutable $sessionEnd;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): AdobeConnectMeetingAttendee
    {
        $this->id = $id;

        return $this;
    }

    public function getAdobeConnectPrincipal(): AdobeConnectPrincipal
    {
        return $this->adobeConnectPrincipal;
    }

    public function setAdobeConnectPrincipal(AdobeConnectPrincipal $adobeConnectPrincipal): AdobeConnectMeetingAttendee
    {
        $this->adobeConnectPrincipal = $adobeConnectPrincipal;

        return $this;
    }

    public function getAdobeConnectSCO(): ?AdobeConnectSCO
    {
        return $this->adobeConnectSCO;
    }

    public function setAdobeConnectSCO(?AdobeConnectSCO $adobeConnectSCO): AdobeConnectMeetingAttendee
    {
        $this->adobeConnectSCO = $adobeConnectSCO;

        return $this;
    }

    public function getSessionStart(): \DateTimeImmutable
    {
        return $this->sessionStart;
    }

    public function setSessionStart(\DateTimeImmutable $sessionStart): AdobeConnectMeetingAttendee
    {
        $this->sessionStart = $sessionStart;

        return $this;
    }

    public function getSessionEnd(): \DateTimeImmutable
    {
        return $this->sessionEnd;
    }

    public function setSessionEnd(\DateTimeImmutable $sessionEnd): AdobeConnectMeetingAttendee
    {
        $this->sessionEnd = $sessionEnd;

        return $this;
    }
}
