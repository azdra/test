<?php

namespace App\Entity\Adobe;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Model\GeneratorTrait;
use App\Service\MiddlewareService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity]
#[ORM\Index(columns: ['principal_identifier'])]
#[ORM\UniqueConstraint(columns: ['principal_identifier'])]
class AdobeConnectPrincipal extends ProviderParticipant
{
    use GeneratorTrait;

    public const TYPE_LIVE_ADMIN = 'live-admins';
    public const TYPE_ADMIN = 'admins';

    protected static string $provider = MiddlewareService::SERVICE_TYPE_ADOBE_CONNECT;

    #[ORM\Column(type: 'bigint', nullable: false)]
    #[NotNull(groups: ['update'])]
    #[Groups(['read_principal'])]
    protected ?int $principalIdentifier = null;

    #[ORM\Column(type: 'text')]
    #[Groups(['read_principal'])]
    private string $name;

    #[ORM\Column]
    #[NotBlank]
    #[Encrypted]
    private string $password;

    #[ORM\ManyToMany(targetEntity: AdobeConnectSCO::class, inversedBy: 'principals')]
    protected Collection $linkedSCOs;

    #[ORM\OneToMany(mappedBy: 'linkedPrincipal', targetEntity: AdobeConnectTelephonyProfile::class)]
    protected Collection $telephonyProfiles;

    public function __construct(AbstractProviderConfigurationHolder $configurationHolder)
    {
        parent::__construct($configurationHolder);
        $this->linkedSCOs = new ArrayCollection();
        $this->telephonyProfiles = new ArrayCollection();
        $this->password = $this->randomString();
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof AdobeConnectConfigurationHolder;
    }

    public function getPrincipalIdentifier(): ?int
    {
        return $this->principalIdentifier;
    }

    public function setPrincipalIdentifier(?int $principalIdentifier): AdobeConnectPrincipal
    {
        $this->principalIdentifier = $principalIdentifier;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): AdobeConnectPrincipal
    {
        $this->name = $name;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getLinkedSCOs(): ArrayCollection|Collection
    {
        return $this->linkedSCOs;
    }

    public function setLinkedSCOs(ArrayCollection|Collection $linkedSCOs): AdobeConnectPrincipal
    {
        $this->linkedSCOs = $linkedSCOs;

        return $this;
    }

    public function addLinkedSCO(AdobeConnectSCO $adobeConnectSCO): AdobeConnectPrincipal
    {
        if (!$this->linkedSCOs->contains($adobeConnectSCO)) {
            $this->linkedSCOs->add($adobeConnectSCO);
        }

        return $this;
    }

    public function removeLinkedSCO(AdobeConnectSCO $adobeConnectSCO): AdobeConnectPrincipal
    {
        if ($this->linkedSCOs->contains($adobeConnectSCO)) {
            $this->linkedSCOs->removeElement($adobeConnectSCO);
        }

        return $this;
    }

    public function getTelephonyProfiles(): ArrayCollection|Collection
    {
        return $this->telephonyProfiles;
    }

    public function setTelephonyProfiles(ArrayCollection|Collection $telephonyProfiles): AdobeConnectPrincipal
    {
        $this->telephonyProfiles = $telephonyProfiles;

        return $this;
    }

    public function addTelephonyProfile(AdobeConnectTelephonyProfile $adobeConnectTelephonyProfile): AdobeConnectPrincipal
    {
        if (!$this->telephonyProfiles->contains($adobeConnectTelephonyProfile)) {
            $this->telephonyProfiles->add($adobeConnectTelephonyProfile);
            $adobeConnectTelephonyProfile->setLinkedPrincipal($this);
        }

        return $this;
    }

    public function removeTelephonyProfile(AdobeConnectTelephonyProfile $adobeConnectTelephonyProfile): AdobeConnectPrincipal
    {
        if ($this->telephonyProfiles->contains($adobeConnectTelephonyProfile)) {
            $this->telephonyProfiles->removeElement($adobeConnectTelephonyProfile);
            $adobeConnectTelephonyProfile->setLinkedPrincipal(null);
        }

        return $this;
    }
}
