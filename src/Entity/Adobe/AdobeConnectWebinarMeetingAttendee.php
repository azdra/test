<?php

namespace App\Entity\Adobe;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AdobeConnectWebinarMeetingAttendee
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: AdobeConnectWebinarPrincipal::class)]
    protected AdobeConnectWebinarPrincipal $adobeConnectWebinarPrincipal;

    #[ORM\ManyToOne(targetEntity: AdobeConnectWebinarSCO::class, inversedBy: 'attendees')]
    protected ?AdobeConnectWebinarSCO $adobeConnectWebinarSCO;

    #[ORM\Column(type: 'datetime_immutable')]
    protected \DateTimeImmutable $sessionStart;

    #[ORM\Column(type: 'datetime_immutable')]
    protected \DateTimeImmutable $sessionEnd;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): AdobeConnectWebinarMeetingAttendee
    {
        $this->id = $id;

        return $this;
    }

    public function getAdobeConnectWebinarPrincipal(): AdobeConnectWebinarPrincipal
    {
        return $this->adobeConnectWebinarPrincipal;
    }

    public function setAdobeConnectWebinarPrincipal(AdobeConnectWebinarPrincipal $adobeConnectWebinarPrincipal): AdobeConnectWebinarMeetingAttendee
    {
        $this->adobeConnectWebinarPrincipal = $adobeConnectWebinarPrincipal;

        return $this;
    }

    public function getAdobeConnectWebinarSCO(): ?AdobeConnectWebinarSCO
    {
        return $this->adobeConnectWebinarSCO;
    }

    public function setAdobeConnectWebinarSCO(?AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarMeetingAttendee
    {
        $this->adobeConnectWebinarSCO = $adobeConnectWebinarSCO;

        return $this;
    }

    public function getSessionStart(): \DateTimeImmutable
    {
        return $this->sessionStart;
    }

    public function setSessionStart(\DateTimeImmutable $sessionStart): AdobeConnectWebinarMeetingAttendee
    {
        $this->sessionStart = $sessionStart;

        return $this;
    }

    public function getSessionEnd(): \DateTimeImmutable
    {
        return $this->sessionEnd;
    }

    public function setSessionEnd(\DateTimeImmutable $sessionEnd): AdobeConnectWebinarMeetingAttendee
    {
        $this->sessionEnd = $sessionEnd;

        return $this;
    }
}
