<?php

namespace App\Entity\Adobe;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Model\GeneratorTrait;
use App\Service\MiddlewareService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity]
#[ORM\Index(columns: ['principal_identifier'])]
class AdobeConnectWebinarPrincipal extends ProviderParticipant
{
    use GeneratorTrait;

    public const TYPE_ADMIN = 'admins';
    public const TYPE_LIVE_ADMIN = 'live-admins';
    //public const TYPE_ENTERPRISE_WEBINAR_HOSTS = 'enterprise-webinar-hosts';
    //public const TYPE_EVENT_ADMIN = 'event-admins';
    //public const TYPE_NAMED_UPGRADE_500_HOSTS = 'named-upgrade-500-hosts';
    //public const TYPE_NAMED_UPGRADE_1000_HOSTS = 'named-upgrade-1000-hosts';
    //public const TYPE_NAMED_UPGRADE_1500_HOSTS = 'named-upgrade-1500-hosts';
    public const TYPE_SEMINAR_ADMINS = 'seminar-admins';
    //public const TYPE_STANDARD_WEBINAR_HOSTS = 'standard-webminar-hosts';
    //public const TYPE_WEBINAR_PRO_PACK_USERS = 'webinar-pro-pack-users';

    protected static string $provider = MiddlewareService::SERVICE_TYPE_ADOBE_CONNECT_WEBINAR;

    #[ORM\Column(type: 'bigint', nullable: false)]
    #[NotNull(groups: ['update'])]
    #[Groups(['read_principal'])]
    protected ?int $principalIdentifier = null;

    #[ORM\Column(type: 'text')]
    #[Groups(['read_principal'])]
    private string $name;

    #[ORM\Column]
    #[NotBlank]
    #[Encrypted]
    private string $password;

    #[ORM\ManyToMany(targetEntity: AdobeConnectWebinarSCO::class, inversedBy: 'principals')]
    protected Collection $linkedSCOs;

    #[ORM\OneToMany(mappedBy: 'linkedPrincipal', targetEntity: AdobeConnectWebinarTelephonyProfile::class)]
    protected Collection $telephonyProfiles;

    public function __construct(AbstractProviderConfigurationHolder $configurationHolder)
    {
        parent::__construct($configurationHolder);
        $this->linkedSCOs = new ArrayCollection();
        $this->telephonyProfiles = new ArrayCollection();
        $this->password = $this->randomString();
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof AdobeConnectWebinarConfigurationHolder;
    }

    public function getPrincipalIdentifier(): ?int
    {
        return $this->principalIdentifier;
    }

    public function setPrincipalIdentifier(?int $principalIdentifier): AdobeConnectWebinarPrincipal
    {
        $this->principalIdentifier = $principalIdentifier;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): AdobeConnectWebinarPrincipal
    {
        $this->name = $name;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getLinkedSCOs(): ArrayCollection|Collection
    {
        return $this->linkedSCOs;
    }

    public function setLinkedSCOs(ArrayCollection|Collection $linkedSCOs): AdobeConnectWebinarPrincipal
    {
        $this->linkedSCOs = $linkedSCOs;

        return $this;
    }

    public function addLinkedSCO(AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarPrincipal
    {
        if (!$this->linkedSCOs->contains($adobeConnectWebinarSCO)) {
            $this->linkedSCOs->add($adobeConnectWebinarSCO);
        }

        return $this;
    }

    public function removeLinkedSCO(AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarPrincipal
    {
        if ($this->linkedSCOs->contains($adobeConnectWebinarSCO)) {
            $this->linkedSCOs->removeElement($adobeConnectWebinarSCO);
        }

        return $this;
    }

    public function getTelephonyProfiles(): ArrayCollection|Collection
    {
        return $this->telephonyProfiles;
    }

    public function setTelephonyProfiles(ArrayCollection|Collection $telephonyProfiles): AdobeConnectWebinarPrincipal
    {
        $this->telephonyProfiles = $telephonyProfiles;

        return $this;
    }

    public function addTelephonyProfile(AdobeConnectWebinarTelephonyProfile $adobeConnectWebinarTelephonyProfile): AdobeConnectWebinarPrincipal
    {
        if (!$this->telephonyProfiles->contains($adobeConnectWebinarTelephonyProfile)) {
            $this->telephonyProfiles->add($adobeConnectWebinarTelephonyProfile);
            $adobeConnectWebinarTelephonyProfile->setLinkedPrincipal($this);
        }

        return $this;
    }

    public function removeTelephonyProfile(AdobeConnectWebinarTelephonyProfile $adobeConnectWebinarTelephonyProfile): AdobeConnectWebinarPrincipal
    {
        if ($this->telephonyProfiles->contains($adobeConnectWebinarTelephonyProfile)) {
            $this->telephonyProfiles->removeElement($adobeConnectWebinarTelephonyProfile);
            $adobeConnectWebinarTelephonyProfile->setLinkedPrincipal(null);
        }

        return $this;
    }
}
