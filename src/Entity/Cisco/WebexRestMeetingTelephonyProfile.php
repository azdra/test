<?php

namespace App\Entity\Cisco;

use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Model\GeneratorTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class WebexRestMeetingTelephonyProfile extends AbstractProviderAudio
{
    use GeneratorTrait;

    // FIXME: Remove this. It's redundant with the declaration on AbstractProviderAudio
    #[ORM\ManyToOne(targetEntity: WebexRestMeeting::class, inversedBy: 'telephonyProfiles')]
    private ?WebexRestMeeting $linkedPrincipal;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_audio'])]
    private ?string $webexRestMeetingDefaultPhoneNumber = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session'])]
    private ?string $codeAnimator = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session'])]
    private ?string $codeParticipant = null;

    public function __construct()
    {
        parent::__construct();

        // TODO: Refactor some stuff to match with AbstractProviderAudio
        $this->name = $this->randomString();
    }

    public function getLinkedPrincipal(): ?WebexRestMeeting
    {
        return $this->linkedPrincipal;
    }

    public function setLinkedPrincipal(?WebexRestMeeting $linkedPrincipal): WebexRestMeetingTelephonyProfile
    {
        $this->linkedPrincipal = $linkedPrincipal;

        return $this;
    }

    public static function getAvailableConnectionType(): array
    {
        return [
            self::AUDIO_CALLIN,
            self::AUDIO_VOIP,
        ];
    }

    public function getWebexRestMeetingDefaultPhoneNumber(): ?string
    {
        return $this->webexRestMeetingDefaultPhoneNumber;
    }

    public function setWebexRestMeetingDefaultPhoneNumber(?string $webexRestMeetingDefaultPhoneNumber): WebexRestMeetingTelephonyProfile
    {
        $this->webexRestMeetingDefaultPhoneNumber = $webexRestMeetingDefaultPhoneNumber;

        return $this;
    }

    public function getCodeAnimator(): ?string
    {
        return $this->codeAnimator;
    }

    public function setCodeAnimator(?string $codeAnimator): WebexRestMeetingTelephonyProfile
    {
        $this->codeAnimator = $codeAnimator;

        return $this;
    }

    public function getCodeParticipant(): ?string
    {
        return $this->codeParticipant;
    }

    public function setCodeParticipant(?string $codeParticipant): WebexRestMeetingTelephonyProfile
    {
        $this->codeParticipant = $codeParticipant;

        return $this;
    }
}
