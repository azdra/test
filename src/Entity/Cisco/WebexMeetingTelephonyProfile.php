<?php

namespace App\Entity\Cisco;

use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Model\GeneratorTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class WebexMeetingTelephonyProfile extends AbstractProviderAudio
{
    use GeneratorTrait;

    // FIXME: Remove this. It's redundant with the declaration on AbstractProviderAudio
    #[ORM\ManyToOne(targetEntity: WebexMeeting::class, inversedBy: 'telephonyProfiles')]
    private ?WebexMeeting $linkedPrincipal;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_audio'])]
    private ?string $webexMeetingDefaultPhoneNumber = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session'])]
    private ?string $codeAnimator = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session'])]
    private ?string $codeParticipant = null;

    public function __construct()
    {
        parent::__construct();

        // TODO: Refactor some stuff to match with AbstractProviderAudio
        $this->name = $this->randomString();
    }

    public function getLinkedPrincipal(): ?WebexMeeting
    {
        return $this->linkedPrincipal;
    }

    public function setLinkedPrincipal(?WebexMeeting $linkedPrincipal): WebexMeetingTelephonyProfile
    {
        $this->linkedPrincipal = $linkedPrincipal;

        return $this;
    }

    public static function getAvailableConnectionType(): array
    {
        return [
            self::AUDIO_CALLIN,
            self::AUDIO_VOIP,
        ];
    }

    public function getWebexMeetingDefaultPhoneNumber(): ?string
    {
        return $this->webexMeetingDefaultPhoneNumber;
    }

    public function setWebexMeetingDefaultPhoneNumber(?string $webexMeetingDefaultPhoneNumber): WebexMeetingTelephonyProfile
    {
        $this->webexMeetingDefaultPhoneNumber = $webexMeetingDefaultPhoneNumber;

        return $this;
    }

    public function getCodeAnimator(): ?string
    {
        return $this->codeAnimator;
    }

    public function setCodeAnimator(?string $codeAnimator): WebexMeetingTelephonyProfile
    {
        $this->codeAnimator = $codeAnimator;

        return $this;
    }

    public function getCodeParticipant(): ?string
    {
        return $this->codeParticipant;
    }

    public function setCodeParticipant(?string $codeParticipant): WebexMeetingTelephonyProfile
    {
        $this->codeParticipant = $codeParticipant;

        return $this;
    }
}
