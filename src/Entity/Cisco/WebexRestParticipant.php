<?php

namespace App\Entity\Cisco;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Service\MiddlewareService;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class WebexRestParticipant extends ProviderParticipant
{
    public const ROLE_VISITOR = 'VISITOR';
    public const ROLE_MEMBER = 'MEMBER';

    protected static string $provider = MiddlewareService::SERVICE_TYPE_CISCO_WEBEX_REST;

    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?string $inviteeId = null;

    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    #[Groups(['read_webex_rest_user'])]
    protected bool $active = true;

    /**
     * Unmapped. Used to create a participant. We don't store user password.
     */
    protected string $password = '';

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof WebexRestConfigurationHolder;
    }

    #[Groups(['read_session'])]
    public function getName(): string
    {
        return $this->getUser()->getFirstName().' '.$this->getUser()->getLastName();
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): WebexRestParticipant
    {
        $this->password = $password;

        return $this;
    }

    public function getInviteeId(): ?string
    {
        return $this->inviteeId;
    }

    public function setInviteeId(?string $inviteeId): WebexRestParticipant
    {
        $this->inviteeId = $inviteeId;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): WebexRestParticipant
    {
        $this->active = $active;

        return $this;
    }

    public function getRole(): string
    {
        return is_null($this->inviteeId)
            ? self::ROLE_VISITOR
            : self::ROLE_MEMBER;
    }
}
