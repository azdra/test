<?php

namespace App\Entity\Cisco;

use App\Controller\Api\ProviderSessionApiController;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Service\MiddlewareService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity]
#[UniqueEntity('meetingRestId')]
#[ORM\HasLifecycleCallbacks]
class WebexRestMeeting extends ProviderSession
{
    protected static string $provider = MiddlewareService::SERVICE_TYPE_CISCO_WEBEX_REST;
    public const DEFAULT_OPEN_TIME = 15;

    public const SCHEDULED_TYPE_MEETING = 'meeting';
    public const SCHEDULED_TYPE_WEBINAR = 'webinar';
    public const SCHEDULED_TYPE_PERSONAL_ROOM_MEETING = 'personalRoomMeeting';

    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $maximumUserCount = null;

    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(['read_session'])]
    #[NotNull(groups: ['update'])]
    protected string $meetingRestId;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session'])]
    #[NotBlank(allowNull: true)]
    #[NotNull(groups: ['update'])]
    protected ?string $meetingRestPassword = null;

    #[ORM\Column(type: 'bigint', nullable: false)]
    #[Groups(['read_session'])]
    #[NotNull(groups: ['update'])]
    protected int $meetingRestNumber;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW])]
    private ?string $meetingRestWebLink = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW])]
    private ?string $meetingRestHostEmail = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Assert\NotBlank(allowNull: true)]
    #[Assert\Choice([
        self::SCHEDULED_TYPE_MEETING,
        self::SCHEDULED_TYPE_WEBINAR,
        self::SCHEDULED_TYPE_PERSONAL_ROOM_MEETING,
    ])]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW])]
    private ?string $meetingRestScheduledType = self::SCHEDULED_TYPE_MEETING;

    #[ORM\Column(type: 'integer', options: ['default' => self::DEFAULT_OPEN_TIME])]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW])]
    #[NotNull(groups: ['create', 'update', ProviderSessionApiController::GROUP_CREATE, ProviderSessionApiController::GROUP_UPDATE])]
    protected int $openTime = self::DEFAULT_OPEN_TIME;

    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $hostICalendarUrl = null;

    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $attendeeICalendarUrl = null;

    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW])]
    #[NotNull(groups: ['create', 'update', ProviderSessionApiController::GROUP_CREATE])]
    protected ?string $licence = null;

    #[ORM\OneToMany(mappedBy: 'linkedPrincipal', targetEntity: WebexRestMeetingTelephonyProfile::class)]
    protected Collection $telephonyProfiles;

    public function __construct(AbstractProviderConfigurationHolder $configurationHolder)
    {
        parent::__construct($configurationHolder);
        $this->telephonyProfiles = new ArrayCollection();
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof WebexRestConfigurationHolder;
    }

    public function getMaximumUserCount(): ?int
    {
        return $this->maximumUserCount;
    }

    public function setMaximumUserCount(?int $maximumUserCount): WebexRestMeeting
    {
        $this->maximumUserCount = $maximumUserCount;

        return $this;
    }

    public function getMeetingRestNumber(): int
    {
        return $this->meetingRestNumber;
    }

    public function setMeetingRestNumber(int $meetingRestNumber): WebexRestMeeting
    {
        $this->meetingRestNumber = $meetingRestNumber;

        return $this;
    }

    public function getMeetingRestId(): string
    {
        return $this->meetingRestId;
    }

    public function setMeetingRestId(string $meetingRestId): WebexRestMeeting
    {
        $this->meetingRestId = $meetingRestId;

        return $this;
    }

    public function getMeetingRestPassword(): ?string
    {
        return $this->meetingRestPassword;
    }

    public function setMeetingRestPassword(?string $meetingRestPassword): WebexRestMeeting
    {
        $this->meetingRestPassword = $meetingRestPassword;

        return $this;
    }

    public function getMeetingRestWebLink(): ?string
    {
        return $this->meetingRestWebLink;
    }

    public function setMeetingRestWebLink(?string $meetingRestWebLink): WebexRestMeeting
    {
        $this->meetingRestWebLink = $meetingRestWebLink;

        return $this;
    }

    public function getMeetingRestHostEmail(): ?string
    {
        return $this->meetingRestHostEmail;
    }

    public function setMeetingRestHostEmail(?string $meetingRestHostEmail): WebexRestMeeting
    {
        $this->meetingRestHostEmail = $meetingRestHostEmail;

        return $this;
    }

    public function getMeetingRestScheduledType(): ?string
    {
        return $this->meetingRestScheduledType;
    }

    public function setMeetingRestScheduledType(?string $meetingRestScheduledType): WebexRestMeeting
    {
        $this->meetingRestScheduledType = $meetingRestScheduledType;

        return $this;
    }

    public function getOpenTime(): int
    {
        return $this->openTime;
    }

    public function setOpenTime(int $openTime): WebexRestMeeting
    {
        $this->openTime = $openTime;

        return $this;
    }

    public function getHostICalendarUrl(): ?string
    {
        return $this->hostICalendarUrl;
    }

    public function setHostICalendarUrl(?string $hostICalendarUrl): WebexRestMeeting
    {
        $this->hostICalendarUrl = $hostICalendarUrl;

        return $this;
    }

    public function getAttendeeICalendarUrl(): ?string
    {
        return $this->attendeeICalendarUrl;
    }

    public function setAttendeeICalendarUrl(?string $attendeeICalendarUrl): WebexRestMeeting
    {
        $this->attendeeICalendarUrl = $attendeeICalendarUrl;

        return $this;
    }

    public function getLicence(): ?string
    {
        return $this->licence;
    }

    public function setLicence(?string $licence): WebexRestMeeting
    {
        $this->licence = $licence;

        return $this;
    }

    public function getTelephonyProfiles(): ArrayCollection|Collection
    {
        return $this->telephonyProfiles;
    }

    public function setTelephonyProfiles(ArrayCollection|Collection $telephonyProfiles): WebexRestMeeting
    {
        $this->telephonyProfiles = $telephonyProfiles;

        return $this;
    }

    public function addTelephonyProfile(WebexRestMeetingTelephonyProfile $webexRestMeetingTelephonyProfile): WebexRestMeeting
    {
        if (!$this->telephonyProfiles->contains($webexRestMeetingTelephonyProfile)) {
            $this->telephonyProfiles->add($webexRestMeetingTelephonyProfile);
            $webexRestMeetingTelephonyProfile->setLinkedPrincipal($this);
        }

        return $this;
    }

    public function removeTelephonyProfile(WebexRestMeetingTelephonyProfile $webexRestMeetingTelephonyProfile): WebexRestMeeting
    {
        if ($this->telephonyProfiles->contains($webexRestMeetingTelephonyProfile)) {
            $this->telephonyProfiles->removeElement($webexRestMeetingTelephonyProfile);
            $webexRestMeetingTelephonyProfile->setLinkedPrincipal(null);
        }

        return $this;
    }

    public function getProviderDistantID(): ?string
    {
        return (string) $this->meetingRestNumber;
    }

    public function getLicenceUser(): ?User
    {
        return $this->getLicenceParticipantRole()?->getParticipant()->getUser();
    }

    public function getLicenceParticipantRole(): ?ProviderParticipantSessionRole
    {
        /** @var ProviderParticipantSessionRole $animatorParticipantRole */
        foreach ($this->getAnimatorsOnly() as $animatorParticipantRole) {
            if ($animatorParticipantRole->getEmail() === $this->licence) {
                return $animatorParticipantRole;
            }
        }

        return null;
    }

    #[Callback(groups: ['create', 'update', ProviderSessionApiController::GROUP_CREATE])]
    public function validateDates(ExecutionContextInterface $context, $payload): void
    {
        /** @var WebexRestConfigurationHolder $configurationHolder */
        $configurationHolder = $this->abstractProviderConfigurationHolder;

        if (!$configurationHolder->isAutomaticLicensing() && empty($this->licence)) {
            $context->buildViolation('The licence can not be null')
                    ->atPath('licence')
                    ->addViolation();
        }
    }
}
