<?php

namespace App\Entity\Cisco;

use App\Controller\Api\ProviderSessionApiController;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Service\MiddlewareService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity]
#[UniqueEntity('meetingKey')]
#[ORM\HasLifecycleCallbacks]
class WebexMeeting extends ProviderSession
{
    protected static string $provider = MiddlewareService::SERVICE_TYPE_CISCO_WEBEX;
    protected const DEFAULT_OPEN_TIME = 900;

    #[ORM\Column(type: 'string', nullable: true)]
    protected ?string $meetingPassword = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $maximumUserCount = null;

    #[ORM\Column(type: 'bigint', nullable: false)]
    #[Groups(['read_session'])]
    #[NotNull(groups: ['update'])]
    protected ?int $meetingKey = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW])]
    private ?string $meetingLink = null;

    #[ORM\Column(type: 'integer', options: ['default' => self::DEFAULT_OPEN_TIME])]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW])]
    #[NotNull(groups: ['create', 'update', ProviderSessionApiController::GROUP_CREATE, ProviderSessionApiController::GROUP_UPDATE])]
    protected int $openTime = self::DEFAULT_OPEN_TIME;

    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $hostICalendarUrl = null;

    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $attendeeICalendarUrl = null;

    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW])]
    #[NotNull(groups: ['create', 'update', ProviderSessionApiController::GROUP_CREATE])]
    protected ?string $licence = null;

    #[ORM\OneToMany(mappedBy: 'linkedPrincipal', targetEntity: WebexMeetingTelephonyProfile::class)]
    protected Collection $telephonyProfiles;

    public function __construct(AbstractProviderConfigurationHolder $configurationHolder)
    {
        parent::__construct($configurationHolder);
        $this->telephonyProfiles = new ArrayCollection();
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof WebexConfigurationHolder;
    }

    public function getMeetingPassword(): ?string
    {
        return $this->meetingPassword;
    }

    public function setMeetingPassword(?string $meetingPassword): WebexMeeting
    {
        $this->meetingPassword = $meetingPassword;

        return $this;
    }

    public function getMaximumUserCount(): ?int
    {
        return $this->maximumUserCount;
    }

    public function setMaximumUserCount(?int $maximumUserCount): WebexMeeting
    {
        $this->maximumUserCount = $maximumUserCount;

        return $this;
    }

    public function getMeetingKey(): ?int
    {
        return $this->meetingKey;
    }

    public function setMeetingKey(?int $meetingKey): WebexMeeting
    {
        $this->meetingKey = $meetingKey;

        return $this;
    }

    public function getMeetingLink(): ?string
    {
        return $this->meetingLink;
    }

    public function setMeetingLink(?string $meetingLink): WebexMeeting
    {
        $this->meetingLink = $meetingLink;

        return $this;
    }

    public function getOpenTime(): int
    {
        return $this->openTime;
    }

    public function setOpenTime(int $openTime): WebexMeeting
    {
        $this->openTime = $openTime;

        return $this;
    }

    public function getHostICalendarUrl(): ?string
    {
        return $this->hostICalendarUrl;
    }

    public function setHostICalendarUrl(?string $hostICalendarUrl): WebexMeeting
    {
        $this->hostICalendarUrl = $hostICalendarUrl;

        return $this;
    }

    public function getAttendeeICalendarUrl(): ?string
    {
        return $this->attendeeICalendarUrl;
    }

    public function setAttendeeICalendarUrl(?string $attendeeICalendarUrl): WebexMeeting
    {
        $this->attendeeICalendarUrl = $attendeeICalendarUrl;

        return $this;
    }

    public function getLicence(): ?string
    {
        return $this->licence;
    }

    public function setLicence(?string $licence): WebexMeeting
    {
        $this->licence = $licence;

        return $this;
    }

    public function getTelephonyProfiles(): ArrayCollection|Collection
    {
        return $this->telephonyProfiles;
    }

    public function setTelephonyProfiles(ArrayCollection|Collection $telephonyProfiles): WebexMeeting
    {
        $this->telephonyProfiles = $telephonyProfiles;

        return $this;
    }

    public function addTelephonyProfile(WebexMeetingTelephonyProfile $webexMeetingTelephonyProfile): WebexMeeting
    {
        if (!$this->telephonyProfiles->contains($webexMeetingTelephonyProfile)) {
            $this->telephonyProfiles->add($webexMeetingTelephonyProfile);
            $webexMeetingTelephonyProfile->setLinkedPrincipal($this);
        }

        return $this;
    }

    public function removeTelephonyProfile(WebexMeetingTelephonyProfile $webexMeetingTelephonyProfile): WebexMeeting
    {
        if ($this->telephonyProfiles->contains($webexMeetingTelephonyProfile)) {
            $this->telephonyProfiles->removeElement($webexMeetingTelephonyProfile);
            $webexMeetingTelephonyProfile->setLinkedPrincipal(null);
        }

        return $this;
    }

    public function getProviderDistantID(): ?string
    {
        return (string) $this->meetingKey;
    }

    public function getLicenceUser(): ?User
    {
        return $this->getLicenceParticipantRole()?->getParticipant()->getUser();
    }

    public function getLicenceParticipantRole(): ?ProviderParticipantSessionRole
    {
        /** @var ProviderParticipantSessionRole $animatorParticipantRole */
        foreach ($this->getAnimatorsOnly() as $animatorParticipantRole) {
            if ($animatorParticipantRole->getEmail() === $this->licence) {
                return $animatorParticipantRole;
            }
        }

        return null;
    }

    #[Callback(groups: ['create', 'update', ProviderSessionApiController::GROUP_CREATE])]
    public function validateDates(ExecutionContextInterface $context, $payload): void
    {
        /** @var WebexConfigurationHolder $configurationHolder */
        $configurationHolder = $this->abstractProviderConfigurationHolder;

        if (!$configurationHolder->isAutomaticLicensing() && empty($this->licence)) {
            $context->buildViolation('The licence can not be null')
                    ->atPath('licence')
                    ->addViolation();
        }
    }
}
