<?php

namespace App\Entity\Cisco;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Service\MiddlewareService;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class WebexParticipant extends ProviderParticipant
{
    public const ROLE_VISITOR = 'VISITOR';
    public const ROLE_MEMBER = 'MEMBER';

    protected static string $provider = MiddlewareService::SERVICE_TYPE_CISCO_WEBEX;

    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $contactIdentifier = null;

    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    #[Groups(['read_webex_user'])]
    protected bool $active = true;

    /**
     * Unmapped. Used to create a participant. We don't store user password.
     */
    protected string $password = '';

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof WebexConfigurationHolder;
    }

    #[Groups(['read_session'])]
    public function getName(): string
    {
        return $this->getUser()->getFirstName().' '.$this->getUser()->getLastName();
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): WebexParticipant
    {
        $this->password = $password;

        return $this;
    }

    public function getContactIdentifier(): ?int
    {
        return $this->contactIdentifier;
    }

    public function setContactIdentifier(?int $contactIdentifier): WebexParticipant
    {
        $this->contactIdentifier = $contactIdentifier;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): WebexParticipant
    {
        $this->active = $active;

        return $this;
    }

    public function getRole(): string
    {
        return is_null($this->contactIdentifier)
            ? self::ROLE_VISITOR
            : self::ROLE_MEMBER;
    }
}
