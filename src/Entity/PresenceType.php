<?php

namespace App\Entity;

enum PresenceType : string {
    case None = 'none';
    case Automatic = 'automatic';
    case Manual = 'manual';
    case Signatures = 'signatures';
}
