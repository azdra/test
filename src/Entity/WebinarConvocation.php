<?php

namespace App\Entity;

use App\Validator as AppAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class WebinarConvocation
{
    public const TYPE_REMINDER = 'reminder';
    public const TYPE_CONFIRMATION = 'confirmation';
    public const TYPE_THANK_YOU = 'thank_you';
    public const TYPE_INVITATION = 'invitation';
    public const TYPE_SMS = 'sms';

    public const SEND_TO_ALL = 'all';
    public const SEND_TO_PRESENCES = 'presences';
    public const SEND_TO_ABSENCES = 'absences';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column]
    #[Groups(['read_session'])]
    private int $id;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank]
    #[AppAssert\ConstraintShortCode]
    #[Groups(['read_session'])]
    private string $subjectMail;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['read_session'])]
    protected ?\DateTimeImmutable $convocationDate = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Assert\NotBlank(allowNull: true)]
    #[AppAssert\ConstraintShortCode]
    #[Groups(['read_session'])]
    private ?string $content;

    #[ORM\OneToMany(mappedBy: 'webinarConvocation', targetEntity: SessionConvocationAttachment::class, cascade: ['all'])]
    #[Groups(['read_session'])]
    protected Collection $attachments;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Groups(['read_session'])]
    private bool $sent = false;

    #[ORM\Column(type: 'string', nullable: false)]
    #[Assert\Choice([
        self::TYPE_CONFIRMATION,
        self::TYPE_REMINDER,
        self::TYPE_THANK_YOU,
        self::TYPE_INVITATION,
        self::TYPE_SMS,
    ])]
    #[Groups(['read_session', 'read_session_list'])]
    private string $convocationType = self::TYPE_CONFIRMATION;

    #[ORM\Column(type: 'string', nullable: false)]
    #[Assert\Choice([
        self::SEND_TO_ALL,
        self::SEND_TO_PRESENCES,
        self::SEND_TO_ABSENCES,
    ])]
    #[Groups(['read_session'])]
    private string $sendCommunicationTo = self::SEND_TO_ALL;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_session_list'])]
    private ?string $sender = null;

    #[ORM\ManyToOne(targetEntity: Convocation::class, inversedBy: 'webinarConvocation')]
    #[ORM\JoinColumn(name: 'convocation_id', referencedColumnName: 'id', nullable: true)]
    private ?Convocation $convocation = null;

    #[ORM\ManyToOne(targetEntity: ProviderSession::class, inversedBy: 'webinarConvocation')]
    private ProviderSession $session;

    #[ORM\OneToMany(mappedBy: 'webinarConvocation', targetEntity: Guest::class, cascade: ['all'])]
    #[Groups(['read_session', 'read_session_list'])]
    private Collection $guests;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Assert\NotNull]
    #[Groups(['read_convocation', 'read_session'])]
    private bool $ics = false;

    public function __construct(?Convocation $convocation, ProviderSession $session)
    {
        $this->convocation = $convocation;
        $this->session = $session;
        $this->attachments = new ArrayCollection();
        $this->guests = new ArrayCollection();
        if ($convocation !== null) {
            $this->subjectMail = $convocation->getSubjectMail();
            $this->content = $convocation->getContent();
        } else {
            $this->subjectMail = 'Send SMS';
            $this->content = '';
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSubjectMail(): string
    {
        return $this->subjectMail;
    }

    public function setSubjectMail(string $subjectMail): self
    {
        $this->subjectMail = $subjectMail;

        return $this;
    }

    public function getConvocationDate(): ?\DateTimeImmutable
    {
        return $this->convocationDate;
    }

    public function setConvocationDate(?\DateTimeImmutable $convocationDate): self
    {
        $this->convocationDate = $convocationDate;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function setAttachments(Collection $attachments): self
    {
        $this->attachments = $attachments;

        return $this;
    }

    public function isSent(): bool
    {
        return $this->sent;
    }

    public function setSent(bool $sent): self
    {
        $this->sent = $sent;

        return $this;
    }

    public function getConvocationType(): string
    {
        return $this->convocationType;
    }

    public function setConvocationType(string $convocationType): self
    {
        $this->convocationType = $convocationType;

        return $this;
    }

    public function getConvocation(): ?Convocation
    {
        return $this->convocation;
    }

    public function setConvocation(?Convocation $convocation): self
    {
        $this->convocation = $convocation;

        return $this;
    }

    public function getSession(): ProviderSession
    {
        return $this->session;
    }

    public function setSession(ProviderSession $session): self
    {
        $this->session = $session;

        return $this;
    }

    #[Groups(['read_session'])]
    public function getModalConvocationName(): string
    {
        if (is_null($this->convocation)) {
            return 'SMS';
        }

        return $this->convocation->getName();
    }

    public function getSendCommunicationTo(): string
    {
        return $this->sendCommunicationTo;
    }

    public function setSendCommunicationTo(string $sendCommunicationTo): self
    {
        $this->sendCommunicationTo = $sendCommunicationTo;

        return $this;
    }

    public function getGuests(): Collection
    {
        return $this->guests;
    }

    public function setGuests(Collection $guests): self
    {
        $this->guests = $guests;

        return $this;
    }

    public function getConvocationTextType(): string
    {
        return match ($this->convocationType) {
            self::TYPE_CONFIRMATION => 'Confirmation',
            self::TYPE_THANK_YOU => 'Thank You',
            self::TYPE_INVITATION => 'Invitation',
            self::TYPE_REMINDER => 'Reminder',
            self::TYPE_SMS => 'SMS'
        };
    }

    public function getGuestByEmail(string $email): ?Guest
    {
        foreach ($this->guests as $guest) {
            if ($guest->getEmail() === $email) {
                return $guest;
            }
        }

        return null;
    }

    public function getIcs(): bool
    {
        return $this->ics;
    }

    public function setIcs(bool $ics): self
    {
        $this->ics = $ics;

        return $this;
    }

    public function getSender(): ?string
    {
        return $this->sender;
    }

    public function setSender(?string $sender): self
    {
        $this->sender = $sender;

        return $this;
    }
}
