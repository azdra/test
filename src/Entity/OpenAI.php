<?php

namespace App\Entity;

use App\Repository\OpenAIRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OpenAIRepository::class)]
#[ORM\Table(name: 'open_ai')]
class OpenAI
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column()]
    private ?string $sessionId = null;

    #[ORM\Column(type: 'float', options: ['default' => 0.0])]
    private float $totalGranted = 0.0;

    #[ORM\Column(type: 'float', options: ['default' => 0.0])]
    private float $totalUsed = 0.0;

    #[ORM\Column(type: 'float', options: ['default' => 0.0])]
    private float $totalAvailable = 0.0;

    #[ORM\Column(type: 'float', options: ['default' => 0.0])]
    private float $totalPaidAvailable = 0.0;

    #[ORM\Column(type: 'float', options: ['default' => 5.0])]
    private float $creditThreshold = 5.0;

    #[ORM\Column(type: 'json', nullable: true)]
    private ?array $billing = [];

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $error = null;

    public function toArray(): array
    {
        return [
            'session_id' => $this->sessionId,
            'total_granted' => $this->totalGranted,
            'total_used' => $this->totalUsed,
            'total_available' => $this->totalAvailable,
            'total_paid_available' => $this->totalPaidAvailable,
            'error' => $this->error,
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    public function setSessionId(string $sessionId): OpenAI
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    public function getTotalGranted(): float
    {
        return $this->totalGranted;
    }

    public function setTotalGranted(float $totalGranted): OpenAI
    {
        $this->totalGranted = $totalGranted;

        return $this;
    }

    public function getTotalUsed(): float
    {
        return $this->totalUsed;
    }

    public function setTotalUsed(float $totalUsed): OpenAI
    {
        $this->totalUsed = $totalUsed;

        return $this;
    }

    public function getTotalAvailable(): float
    {
        return $this->totalAvailable;
    }

    public function setTotalAvailable(float $totalAvailable): OpenAI
    {
        $this->totalAvailable = $totalAvailable;

        return $this;
    }

    public function getTotalPaidAvailable(): float
    {
        return $this->totalPaidAvailable;
    }

    public function setTotalPaidAvailable(float $totalPaidAvailable): OpenAI
    {
        $this->totalPaidAvailable = $totalPaidAvailable;

        return $this;
    }

    public function getCreditThreshold(): float
    {
        return $this->creditThreshold;
    }

    public function setCreditThreshold(float $creditThreshold): OpenAI
    {
        $this->creditThreshold = $creditThreshold;

        return $this;
    }

    public function getBilling(): ?array
    {
        return $this->billing;
    }

    public function setBilling(?array $billing): OpenAI
    {
        $this->billing = $billing;

        return $this;
    }

    public function getError(): ?string
    {
        return $this->error;
    }

    public function setError(?string $error): OpenAI
    {
        $this->error = $error;

        return $this;
    }
}
