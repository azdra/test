<?php

namespace App\Entity;

use App\SelfSubscription\Entity\Module;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class ReplayCategory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['ui:module:read', 'read_session', 'read_client', 'ui:module:hub', 'ui:module:update'])]
    private int $id;

    #[ORM\Column(type: 'string', nullable: false)]
    #[Groups(['ui:module:read', 'read_session', 'read_client', 'ui:module:hub', 'ui:module:update'])]
    private string $categoryName;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['ui:module:read', 'read_session', 'read_client', 'ui:module:hub', 'ui:module:update'])]
    private DateTimeInterface $createdAt;

    #[ORM\Column(type: 'boolean', nullable: false)]
    #[Groups(['ui:module:read', 'ui:module:hub'])]
    private bool $active = true;

    #[ORM\ManyToOne(targetEntity: Module::class, inversedBy: 'catalogsReplay')]
    protected Module $module;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCategoryName(): string
    {
        return $this->categoryName;
    }

    public function setCategoryName(string $categoryName): self
    {
        $this->categoryName = $categoryName;

        return $this;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getModule(): Module
    {
        return $this->module;
    }

    public function setModule(Module $module): ReplayCategory
    {
        $this->module = $module;

        return $this;
    }
}
