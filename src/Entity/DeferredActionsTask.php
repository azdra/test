<?php

namespace App\Entity;

use App\Entity\Client\Client;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class DeferredActionsTask
{
    public const ORIGIN_EXPORT = 'Export';

    public const ACTION_EXPORT_EVALUATIONS = 'Action_export_evaluations';

    public const STATUS_READY = 'ready';
    public const STATUS_PROCESSING = 'processing';
    public const STATUS_DONE = 'done';
    public const STATUS_FAILED = 'failed';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'text')]
    private string $className;

    #[ORM\Column(type: 'string')]
    #[Groups('read_deferred_actions_task')]
    #[Assert\Choice([
        self::ORIGIN_EXPORT,
    ])]
    private string $origin;

    #[ORM\Column(type: 'string')]
    #[Groups('read_deferred_actions_task')]
    #[Assert\Choice([
        self::ACTION_EXPORT_EVALUATIONS,
    ])]
    private string $action;

    #[ORM\Column(type: 'json')]
    private array $data = [];

    #[ORM\Column(type: 'json')]
    private array $result = [];

    #[ORM\Column(type: 'string')]
    private string $status = DeferredActionsTask::STATUS_READY;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $createdAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTime $processingStartTime = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTime $processingEndTime = null;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'deferredActionsTasks')]
    #[ORM\JoinColumn(name: 'client_origin', referencedColumnName: 'id', nullable: true)]
    private ?Client $clientOrigin = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'deferredActionsTasks')]
    #[ORM\JoinColumn(name: 'user_origin', referencedColumnName: 'id', nullable: true)]
    private ?User $userOrigin = null;

    #[ORM\Column(type: 'json')]
    private array $infos = [];

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): DeferredActionsTask
    {
        $this->id = $id;

        return $this;
    }

    public function getClassName(): string
    {
        return $this->className;
    }

    public function setClassName(string $className): DeferredActionsTask
    {
        $this->className = $className;

        return $this;
    }

    public function getOrigin(): string
    {
        return $this->origin;
    }

    public function setOrigin(string $origin): DeferredActionsTask
    {
        if (!\in_array($origin, self::getAvailableOrigins())) {
            throw new \LogicException(sprintf('ORIGIN %s does not exist, or has not yet been implemented.', $origin));
        }

        $this->origin = $origin;

        return $this;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function setAction(string $action): DeferredActionsTask
    {
        $this->action = $action;

        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): DeferredActionsTask
    {
        $this->data = $data;

        return $this;
    }

    public function getResult(): array
    {
        return $this->result;
    }

    public function setResult(array $result): DeferredActionsTask
    {
        $this->result = $result;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): DeferredActionsTask
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): DeferredActionsTask
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getProcessingStartTime(): ?\DateTime
    {
        return $this->processingStartTime;
    }

    public function setProcessingStartTime(?\DateTime $processingStartTime): DeferredActionsTask
    {
        $this->processingStartTime = $processingStartTime;

        return $this;
    }

    public function getProcessingEndTime(): ?\DateTime
    {
        return $this->processingEndTime;
    }

    public function setProcessingEndTime(?\DateTime $processingEndTime): DeferredActionsTask
    {
        $this->processingEndTime = $processingEndTime;

        return $this;
    }

    public function getClientOrigin(): ?Client
    {
        return $this->clientOrigin;
    }

    public function setClientOrigin(?Client $clientOrigin): DeferredActionsTask
    {
        $this->clientOrigin = $clientOrigin;

        return $this;
    }

    public function getUserOrigin(): ?User
    {
        return $this->userOrigin;
    }

    public function setUserOrigin(?User $userOrigin): DeferredActionsTask
    {
        $this->userOrigin = $userOrigin;

        return $this;
    }

    public function getInfos(): array
    {
        return $this->infos;
    }

    public function setInfos(array $infos): DeferredActionsTask
    {
        $this->infos = $infos;

        return $this;
    }

    public static function getAvailableOrigins(): array
    {
        return [
            self::ORIGIN_EXPORT,
        ];
    }
}
