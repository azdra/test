<?php

namespace App\Entity;

use App\Entity\Client\Client;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity()]
#[UniqueEntity(fields: ['title', 'client'], message: 'This evaluation form title already exists with this client', errorPath: 'title')]
#[ORM\UniqueConstraint(name: 'users_idx', columns: ['title', 'client_id'])]
class Evaluation
{
    public const EVALUATION_OF_ASSISTANCE = 'e967c06a-db21-4f12-9aab-76c08b094d88';
    public const EVALUATION_LOGBOOK = '4a5b78e2-2691-442d-8b2a-c77b9303dea0';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_evaluation', 'read_session', 'read_client', 'read_lobby_signature', 'session:evaluation:synthesis', 'lite_participant'])]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read_evaluation', 'read_session', 'read_client'])]
    private string $title;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read_evaluation', 'read_session', 'read_client', 'session:evaluation:synthesis'])]
    private string $surveyId;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups('read_evaluation')]
    private string $organizationId;

    #[ORM\ManyToMany(targetEntity: ProviderSession::class, mappedBy: 'evaluations', cascade: ['persist'])]
    private Collection $sessions;

    #[ORM\OneToMany(mappedBy: 'evaluation', targetEntity: EvaluationProviderSession::class, cascade: ['persist'])]
    protected Collection $evaluationsProviderSession;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'evaluations')]
    #[ORM\JoinColumn(name: 'client_id', referencedColumnName: 'id')]
    private Client $client;

    public function __construct()
    {
        $this->sessions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSurveyId(): ?string
    {
        return $this->surveyId;
    }

    public function setSurveyId(string $surveyId): self
    {
        $this->surveyId = $surveyId;

        return $this;
    }

    public function getOrganizationId(): ?string
    {
        return $this->organizationId;
    }

    public function setOrganizationId(?string $organizationId): self
    {
        $this->organizationId = $organizationId;

        return $this;
    }

    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function setSessions(Collection $sessions): self
    {
        $this->sessions = $sessions;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getEvaluationsProviderSession(): Collection
    {
        return $this->evaluationsProviderSession;
    }

    public function setEvaluationsProviderSession(Collection $evaluationsProviderSession): Evaluation
    {
        $this->evaluationsProviderSession = $evaluationsProviderSession;

        return $this;
    }
}
