<?php

namespace App\Entity;

use App\Entity\Client\Client;
use App\Service\AsyncWorker\TaskInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class AsyncTask
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'text')]
    private string $className;

    #[ORM\Column(type: 'json')]
    private array $data = [];

    #[ORM\Column(type: 'json')]
    private array $result = [];

    #[ORM\Column(type: 'string')]
    private string $status = TaskInterface::STATUS_READY;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $createdAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTime $processingStartTime = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTime $processingEndTime = null;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $flagOrigin;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'asyncTasks')]
    #[ORM\JoinColumn(name: 'client_origin', referencedColumnName: 'id', nullable: true)]
    private ?Client $clientOrigin = null;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $fileOrigin = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'asyncTasks')]
    #[ORM\JoinColumn(name: 'user_origin', referencedColumnName: 'id', nullable: true)]
    private ?User $userOrigin = null;

    #[ORM\Column(type: 'json')]
    private array $infos = [];

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): AsyncTask
    {
        $this->id = $id;

        return $this;
    }

    public function getClassName(): string
    {
        return $this->className;
    }

    public function setClassName(string $className): AsyncTask
    {
        $this->className = $className;

        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): AsyncTask
    {
        $this->data = $data;

        return $this;
    }

    public function getResult(): array
    {
        return $this->result;
    }

    public function setResult(array $result): AsyncTask
    {
        $this->result = $result;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): AsyncTask
    {
        $this->status = $status;

        return $this;
    }

    public function getProcessingStartTime(): ?\DateTime
    {
        return $this->processingStartTime;
    }

    public function setProcessingStartTime(?\DateTime $processingStartTime): AsyncTask
    {
        $this->processingStartTime = $processingStartTime;

        return $this;
    }

    public function getProcessingEndTime(): ?\DateTime
    {
        return $this->processingEndTime;
    }

    public function setProcessingEndTime(?\DateTime $processingEndTime): AsyncTask
    {
        $this->processingEndTime = $processingEndTime;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getFlagOrigin(): ?string
    {
        return $this->flagOrigin;
    }

    public function setFlagOrigin(?string $flagOrigin): AsyncTask
    {
        $this->flagOrigin = $flagOrigin;

        return $this;
    }

    public function getClientOrigin(): ?Client
    {
        return $this->clientOrigin;
    }

    public function setClientOrigin(?Client $clientOrigin): AsyncTask
    {
        $this->clientOrigin = $clientOrigin;

        return $this;
    }

    public function getFileOrigin(): ?string
    {
        return $this->fileOrigin;
    }

    public function setFileOrigin(?string $fileOrigin): AsyncTask
    {
        $this->fileOrigin = $fileOrigin;

        return $this;
    }

    public function getUserOrigin(): ?User
    {
        return $this->userOrigin;
    }

    public function setUserOrigin(?User $userOrigin): AsyncTask
    {
        $this->userOrigin = $userOrigin;

        return $this;
    }

    public function getInfos(): array
    {
        return $this->infos;
    }

    public function setInfos(array $infos): AsyncTask
    {
        $this->infos = $infos;

        return $this;
    }
}
