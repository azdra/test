<?php

namespace App\Entity;

use App\Form\Client\Tab\ClientServicesTabFormType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Embeddable]
class ServiceSchedule
{
    #[ORM\Column()]
    #[Groups(['read_client_services'])]
    private bool $active = false;

    #[ORM\Column(nullable: true)]
    #[
        Assert\NotBlank(
            allowNull: true,
            groups: [ClientServicesTabFormType::FORM_NAME,
            ])
    ]
    #[Groups(['read_client_services'])]
    private ?\DateTime $startTimeAM;

    #[ORM\Column(nullable: true)]
    #[
        Assert\NotBlank(
            allowNull: true,
            groups: [ClientServicesTabFormType::FORM_NAME]
        ),
        Assert\GreaterThan(propertyPath: 'startTimeAM', groups: [ClientServicesTabFormType::FORM_NAME])
    ]
    #[Groups(['read_client_services'])]
    private ?\DateTime $endTimeAM;

    #[ORM\Column(nullable: true)]
    #[
        Assert\NotBlank(
            allowNull: true,
            groups: [ClientServicesTabFormType::FORM_NAME,
        ])
    ]
    #[Groups(['read_client_services'])]
    private ?\DateTime $startTimeLH;

    #[ORM\Column(nullable: true)]
    #[
        Assert\NotBlank(
            allowNull: true,
            groups: [ClientServicesTabFormType::FORM_NAME]
        ),
        Assert\GreaterThan(propertyPath: 'startTimeAM', groups: [ClientServicesTabFormType::FORM_NAME])
    ]
    #[Groups(['read_client_services'])]
    private ?\DateTime $endTimeLH;

    #[ORM\Column(nullable: true)]
    #[
        Assert\NotBlank(
            allowNull: true,
            groups: [ClientServicesTabFormType::FORM_NAME,
            ]
        )
    ]
    #[Groups(['read_client_services'])]
    private ?\DateTime $startTimePM;

    #[ORM\Column(nullable: true)]
    #[
        Assert\NotBlank(
            allowNull: true,
            groups: [ClientServicesTabFormType::FORM_NAME]
        ),
        Assert\GreaterThan(propertyPath: 'startTimePM', groups: [ClientServicesTabFormType::FORM_NAME])
    ]
    #[Groups(['read_client_services'])]
    private ?\DateTime $endTimePM;

    public function __construct()
    {
        $this->startTimeAM = new \DateTime('09:00');
        $this->endTimeAM = new \DateTime('12:30');
        $this->startTimeLH = new \DateTime('12:30');
        $this->endTimeLH = new \DateTime('14:00');
        $this->startTimePM = new \DateTime('14:00');
        $this->endTimePM = new \DateTime('17:30');
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): ServiceSchedule
    {
        $this->active = $active;

        return $this;
    }

    public function getStartTimeAM(): ?\DateTime
    {
        return $this->startTimeAM;
    }

    public function setStartTimeAM(?\DateTime $startTimeAM): ServiceSchedule
    {
        $this->startTimeAM = $startTimeAM;

        return $this;
    }

    public function getStartTimeLH(): ?\DateTime
    {
        return $this->startTimeLH;
    }

    public function setStartTimeLH(?\DateTime $startTimeLH): ServiceSchedule
    {
        $this->startTimeLH = $startTimeLH;

        return $this;
    }

    public function getEndTimeLH(): ?\DateTime
    {
        return $this->endTimeLH;
    }

    public function setEndTimeLH(?\DateTime $endTimeLH): ServiceSchedule
    {
        $this->endTimeLH = $endTimeLH;

        return $this;
    }

    public function getEndTimeAM(): ?\DateTime
    {
        return $this->endTimeAM;
    }

    public function setEndTimeAM(?\DateTime $endTimeAM): ServiceSchedule
    {
        $this->endTimeAM = $endTimeAM;

        return $this;
    }

    public function getStartTimePM(): ?\DateTime
    {
        return $this->startTimePM;
    }

    public function setStartTimePM(?\DateTime $startTimePM): ServiceSchedule
    {
        $this->startTimePM = $startTimePM;

        return $this;
    }

    public function getEndTimePM(): ?\DateTime
    {
        return $this->endTimePM;
    }

    public function setEndTimePM(?\DateTime $endTimePM): ServiceSchedule
    {
        $this->endTimePM = $endTimePM;

        return $this;
    }
}
