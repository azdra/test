<?php

namespace App\Entity;

use App\Controller\Api\ProviderSessionApiController;
use App\Controller\Api\UserApiController;
use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Entity\HelpTask;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Client\Client;
use App\Entity\Client\TrainingActionNatureEnum;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\White\WhiteSession;
use App\Exception\InvalidArgumentException;
use App\RegistrationPage\Entity\RegistrationPageTemplate;
use App\SelfSubscription\Entity\Subject;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity]
#[ORM\InheritanceType('JOINED')]
#[ORM\HasLifecycleCallbacks]
#[ORM\DiscriminatorColumn(name: 'discr', type: 'string')]
#[ORM\DiscriminatorMap([
    'adobe_connect_sco' => AdobeConnectSCO::class,
    'adobe_connect_webinar_sco' => AdobeConnectWebinarSCO::class,
    'cisco_webex_meeting' => WebexMeeting::class,
    'cisco_webex_rest_meeting' => WebexRestMeeting::class,
    'microsoft_teams' => MicrosoftTeamsSession::class,
    'microsoft_teams_event' => MicrosoftTeamsEventSession::class,
    'white_connector' => WhiteSession::class,
])]
#[UniqueEntity('ref', 'abstractProviderConfigurationHolder')]
#[ORM\UniqueConstraint(name: 'providersession_unicity_ref_and_client', columns: ['ref', 'client_id'])]
#[DiscriminatorMap(typeProperty: 'type', mapping: [
    'adobe_connect_sco' => AdobeConnectSCO::class,
    'adobe_connect_webinar_sco' => AdobeConnectWebinarSCO::class,
    'cisco_webex_meeting' => WebexMeeting::class,
    'cisco_webex_rest_meeting' => WebexRestMeeting::class,
    'microsoft_teams_session' => MicrosoftTeamsSession::class,
    'microsoft_teams_event_session' => MicrosoftTeamsEventSession::class,
    'white_connector_session' => WhiteSession::class,
])]
#[ORM\Index(columns: ['date_start'], name: 'idx_date_start')]
#[ORM\Index(columns: ['category'], name: 'idx_category')]
abstract class ProviderSession implements MiddlewareManagedEntityInterface
{
    public const ALIAS = 'provider_session';

    public const AUTHORIZED_EXTENSIONS = ['jpg', 'png', 'pdf', 'odt', 'ods', 'doc', 'docx', 'txt'];
    public const MAX_FILE_SIZE = 2000000;

    public const STATUS_DRAFT = -1;
    public const STATUS_SCHEDULED = 0;
    public const STATUS_CANCELED = 1;

    public const FINAL_STATUS_DRAFT = 'Draft';
    public const FINAL_STATUS_SCHEDULED = 'Scheduled';
    public const FINAL_STATUS_SENT = 'Sent';
    public const FINAL_STATUS_RUNNING = 'Running';
    public const FINAL_STATUS_FINISHED = 'Finished';
    public const FINAL_STATUS_CANCELED = 'Cancelled';

    public const FINAL_CATEGORY_FORMATION = 'Formation';
    public const FINAL_CATEGORY_MEETING = 'Meeting';
    public const FINAL_CATEGORY_TEST = 'Test';
    public const FINAL_CATEGORY_FACE_TO_FACE = 'Face-to-face';
    public const FINAL_CATEGORY_MIXTE = 'Mixed';

    public const FINAL_CATEGORY_WEBINAR = 'Webinar';
    public const FINAL_CATEGORY_UNKNOWN = 'Unkonwn';

    protected static string $provider = 'none';

    public const ENUM_NO = 0;
    public const ENUM_LAUNCH = 2;
    public const ENUM_TOTAL = 3;
    public const ENUM_PEDA = 4;
    public const ENUM_COANIM = 5;

    public const ENUM_SUPPORT_NO = 0;
    public const ENUM_SUPPORT_LAUNCH = 2;
    public const ENUM_SUPPORT_TOTAL = 3;

    public const HELP_TASK_PROGRAMMED = 0;
    public const HELP_TASK_OPEN = 1;
    public const HELP_TASK_TAKEN = 2;
    public const HELP_TASK_CLOSED = 3;

    public const MAX_RESULT_LIST = 50;

    public const WEBINAR_REPLAY_PLATFORM_VIMEO = '1';
    public const WEBINAR_REPLAY_PLATFORM_AC = '2';
    public const WEBINAR_REPLAY_PLATFORM_OTHER = '3';
    public const WEBINAR_REPLAY_YOUTUBE = '4';
    public const WEBINAR_REPLAY_DAILYMOTION = '5';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'read_session_list', 'read_participant', 'read_bounce_session_list', 'read_lobby_signature', 'read_session_calendar',
        ProviderSessionApiController::GROUP_VIEW,
        UserApiController::GROUP_VIEW,
        'ui:module:read', 'ui:subjects:hub', 'help_need:read', 'ui:registration_page_template:read', 'session:evaluation:synthesis',
    ])]
    protected int $id;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_session', 'read_session_list', 'read_session_calendar',
        ProviderSessionApiController::GROUP_VIEW, ProviderSessionApiController::GROUP_UPDATE, ProviderSessionApiController::GROUP_CREATE,
        UserApiController::GROUP_VIEW,
        'ui:module:read', 'help_need:read', 'ui:registration_page_template:read', 'ui:subjects:hub',
    ])]
    #[NotBlank(groups: ['create', 'update', ProviderSessionApiController::GROUP_CREATE])]
    protected string $name = '';

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_session_list', 'read_bounce', 'read_lobby_signature',
        ProviderSessionApiController::GROUP_VIEW,  ProviderSessionApiController::GROUP_CREATE,
        'ui:module:read', 'ui:subjects:hub', 'help_need:read', 'session:evaluation:synthesis',
    ])]
    #[NotBlank(groups: ['create', 'update', ProviderSessionApiController::GROUP_CREATE])]
    protected string $ref;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_session', 'help_need:read', 'ui:registration_page_template:read'])]
    protected \DateTime $createdAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['read_session', 'help_need:read'])]
    protected ?\DateTime $updatedAt = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['read_session', 'read_session_list', 'read_lobby_signature', 'read_session_calendar',
        ProviderSessionApiController::GROUP_VIEW, ProviderSessionApiController::GROUP_UPDATE, ProviderSessionApiController::GROUP_CREATE,
        'ui:subjects:hub', 'help_need:read', 'ui:module:read', 'session:evaluation:synthesis', 'ui:registration_page_template:read',
    ])]
    #[NotNull(groups: ['create', 'update', ProviderSessionApiController::GROUP_CREATE])]
    protected ?\DateTime $dateStart = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['read_session', 'read_session_list', 'read_lobby_signature', 'read_session_calendar',
        ProviderSessionApiController::GROUP_VIEW, ProviderSessionApiController::GROUP_UPDATE, ProviderSessionApiController::GROUP_CREATE,
        'ui:subjects:hub', 'help_need:read',
    ])]
    #[NotNull(groups: ['create', 'update', ProviderSessionApiController::GROUP_CREATE])]
    protected ?\DateTime $dateEnd = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session'])]
    protected ?string $businessNumber = null;

    #[ORM\Column()]
    #[Groups(['read_session', 'read_session_list', ProviderSessionApiController::GROUP_VIEW, 'ui:subjects:hub'])]
    protected CategorySessionType $category = CategorySessionType::CATEGORY_SESSION_FORMATION;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(['read_session', 'read_session_list', 'ui:registration_page_template:read',
        ProviderSessionApiController::GROUP_VIEW, ProviderSessionApiController::GROUP_UPDATE, ProviderSessionApiController::GROUP_CREATE, 'session:evaluation:synthesis',
    ])]
    protected ?int $duration = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['read_session'])]
    protected ?string $information = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'help_need:read', ProviderSessionApiController::GROUP_VIEW])]
    protected ?string $language = 'fr';

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session'])]
    protected ?string $origin = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['read_session'])]
    protected ?array $tags = null;

    #[ORM\OneToMany(mappedBy: 'session', targetEntity: ProviderParticipantSessionRole::class, cascade: ['persist'])]
    #[Groups([
        ProviderSessionApiController::GROUP_CREATE, ProviderSessionApiController::GROUP_VIEW_PRESENCES, 'session:evaluation:synthesis',
    ])]
    protected Collection $participantRoles;

    #[ORM\OneToMany(mappedBy: 'session', targetEntity: ProviderSessionReplay::class, cascade: ['persist'])]
    private Collection $replays;

    #[ORM\ManyToOne(targetEntity: AbstractProviderConfigurationHolder::class)]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW, 'read_session_list'])]
    protected AbstractProviderConfigurationHolder $abstractProviderConfigurationHolder;

    #[ORM\ManyToOne(targetEntity: Client::class)]
    #[Groups(['ui:registration_page_template:read'])]
    protected Client $client;

    #[ORM\ManyToOne(targetEntity: Convocation::class)]
    #[Groups([
        'read_session',
        ProviderSessionApiController::GROUP_VIEW, ProviderSessionApiController::GROUP_UPDATE, ProviderSessionApiController::GROUP_CREATE,
    ])]
    #[NotBlank(groups: ['create', 'update', ProviderSessionApiController::GROUP_CREATE])]
    protected ?Convocation $convocation = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['read_session'])]
    protected ?DateTimeImmutable $notificationDate = null;

    #[ORM\Column(type: 'object')]
    #[Groups(['read_session'])]
    protected array $dateOfSendingReminders = [];

    #[ORM\OneToMany(mappedBy: 'session', targetEntity: SessionConvocationAttachment::class, cascade: ['all'])]
    #[Groups(['read_session'])]
    protected Collection $attachments;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session'])]
    protected ?string $connexionTypeSession = null;

    #[ORM\Column()]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW])]
    protected bool $sendInvitationEmails = false;

    #[ORM\Column()]
    #[Groups(['read_session'])]
    protected bool $enableSendRemindersToAnimators = false;

    #[ORM\Column()]
    #[Groups(['read_session'])]
    protected bool $sessionTest = false;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Groups(['read_session'])]
    protected bool $sessionCanceledLessOneDay = false;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['read_session'])]
    protected ?string $personalizedContent = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    protected ?DateTimeImmutable $lastConvocationSent = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    protected ?DateTimeImmutable $lastReminderSent = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    protected ?DateTimeImmutable $evaluationPerformanceSent = null;

    #[ORM\Column(type: 'smallint')]
    #[Groups(['read_session', 'read_session_list'])]
    protected int $status = self::STATUS_DRAFT;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'created_by', referencedColumnName: 'id')]
    #[Groups(['read_session'])]
    protected ?User $createdBy = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'updated_by', referencedColumnName: 'id')]
    protected ?User $updatedBy = null;

    #[ORM\OneToMany(mappedBy: 'session', targetEntity: Bounce::class, cascade: ['all'])]
    protected ?Collection $bounces = null;

    #[ORM\ManyToOne(targetEntity: AbstractProviderAudio::class, cascade: ['persist'], inversedBy: 'linkedSCOs')]
    #[Groups(['read_session', ProviderSessionApiController::GROUP_VIEW])]
    protected ?AbstractProviderAudio $providerAudio = null;

    #[ORM\ManyToMany(targetEntity: Evaluation::class, inversedBy: 'sessions', cascade: ['persist'])]
    #[Groups(['read_session', 'read_lobby_signature', 'session:evaluation:synthesis'])]
    protected Collection $evaluations;

    #[ORM\OneToMany(mappedBy: 'providerSession', targetEntity: EvaluationProviderSession::class, cascade: ['persist'])]
    #[Groups(['read_session', 'read_lobby_signature', 'session:evaluation:synthesis'])]
    protected Collection $evaluationsProviderSession;

    protected ?Collection $oldEvaluations = null;

    #[ORM\Column(nullable: true)]
    #[NotBlank(allowNull: true)]
    #[Groups(['read_session'])]
    protected ?TrainingActionNatureEnum $nature = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['read_session'])]
    protected ?array $objectives = null;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session'])]
    private int $percentageAttendance = 70;

    #[ORM\Column(type: 'integer', nullable: false)]
    #[
        Assert\Choice(
            choices: [
            self::ENUM_NO,
            self::ENUM_LAUNCH,
            self::ENUM_TOTAL,
            self::ENUM_PEDA,
            self::ENUM_COANIM,
        ])
    ]
    #[Groups(['read_session', 'read_session_list'])]
    private ?int $assistanceType = self::ENUM_NO;

    #[ORM\Column(type: 'integer', nullable: false)]
    #[
        Assert\Choice(
            choices: [
            self::ENUM_SUPPORT_NO,
            self::ENUM_SUPPORT_LAUNCH,
            self::ENUM_SUPPORT_TOTAL,
        ])
    ]
    #[Groups(['read_session', 'read_session_list'])]
    private ?int $supportType = self::ENUM_SUPPORT_NO;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read_session'])]
    private bool $standardHours = false;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read_session'])]
    private bool $outsideStandardHours = false;

    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    #[Groups(['read_session'])]
    private bool $automaticSendingTrainingCertificates = true;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Groups(['read_session'])]
    private bool $sentPresenceReport = false;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    protected ?DateTimeImmutable $sentTrainingCertificates = null;

    #[ORM\Column(type: 'integer', options: ['default' => 0])]
    #[Groups(['read_session'])]
    private ?int $restlunch = 0;

    #[ORM\Column(type: 'integer', options: ['default' => 1])]
    #[Groups(['read_session'])]
    private ?int $nbrdays = 1;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['read_session'])]
    private ?string $location = null;

    #[ORM\OneToMany(mappedBy: 'session', targetEntity: HelpNeed::class, cascade: ['all'], orphanRemoval: true)]
    #[Groups(['read_session'])]
    private Collection $helpNeeds;

    #[ORM\ManyToOne(targetEntity: Subject::class, inversedBy: 'sessions')]
    #[Groups(['read_session', 'help_need:read'])]
    protected ?Subject $subject = null;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'read_client', 'ui:subjects:hub', 'ui:module:read'])]
    protected int $subscriptionMax = 0;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'read_client'])]
    protected int $alertLimit = 0;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['read_session'])]
    protected ?DateTimeImmutable $lastPresenceStatusSynchronization;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Assert\NotBlank()]
    protected ?string $description = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Assert\NotBlank()]
    protected ?string $description2 = null;

    #[ORM\OneToMany(mappedBy: 'session', targetEntity: WebinarConvocation::class, cascade: ['all'])]
    #[Groups(['read_session', 'read_session_list'])]
    private Collection $webinarConvocation;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'ui:registration_page_template:read'])]
    protected ?string $replayVideoPlatform = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'ui:registration_page_template:read'])]
    protected ?string $replayUrlKey = null;

    #[ORM\Column(type: 'boolean', nullable: true, options: ['default' => false])]
    #[Groups(['read_session', 'ui:registration_page_template:read'])]
    protected ?bool $publishedReplay = false;

    #[ORM\Column(type: 'boolean', nullable: true, options: ['default' => false])]
    #[Groups(['read_session', 'ui:registration_page_template:read'])]
    protected ?bool $accessParticipantReplay = false;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'ui:registration_page_template:read'])]
    private ?string $photoReplay = null;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => false])]
    #[Groups(['read_session', 'ui:registration_page_template:read'])]
    private ?bool $replayLayoutPlayButton = false;

    #[ORM\ManyToOne(targetEntity: RegistrationPageTemplate::class, inversedBy: 'sessions')]
    #[Groups(['read_session', 'read_session_list'])]
    protected ?RegistrationPageTemplate $registrationPageTemplate = null;

    #[Assert\Valid]
    #[ORM\Embedded(class: ProviderSessionRegistrationPage::class)]
    #[Groups(['read_session', 'ui:registration_page_template:read', 'ui:registration_page_template:update'])]
    protected ?ProviderSessionRegistrationPage $providerSessionRegistrationPage = null;

    #[ORM\OneToMany(mappedBy: 'providerSession', targetEntity: ProviderSessionSpeaker::class, cascade: ['all'])]
    #[Groups(['read_session', 'ui:registration_page_template:read', 'ui:registration_page_template:update'])]
    private Collection $speakers;

    #[ORM\Column(type: 'json')]
    #[Groups(['read_session', 'read_session_list'])]
    protected array $additionalTimezones = [];

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_session', 'update_session'])]
    private ?string $registrationPageTemplateBanner = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_session'])]
    private ?string $registrationPageTemplateIllustration = null;

    #[ORM\Column(nullable: false)]
    #[Groups(['read_session'])]
    protected array $groupsParticipants = [];

    #[ORM\OneToMany(mappedBy: 'session', targetEntity: SmsBounce::class, cascade: ['all'])]
    protected ?Collection $smsBounces = null;

    public function __construct(AbstractProviderConfigurationHolder $configurationHolder)
    {
        if (!$this->supportConfigurationHolder($configurationHolder)) {
            throw new InvalidArgumentException('The given configuration holder is not compatible with this session');
        }
        $this->abstractProviderConfigurationHolder = $configurationHolder;
        $this->client = $this->getAbstractProviderConfigurationHolder()->getClient();
        $this->createdAt = new \DateTime();
        $this->participantRoles = new ArrayCollection();
        $this->evaluations = new ArrayCollection();
        $this->attachments = new ArrayCollection();
        $this->helpNeeds = new ArrayCollection();
        $this->webinarConvocation = new ArrayCollection();
        $this->speakers = new ArrayCollection();
        $this->replays = new ArrayCollection();
        $this->smsBounces = new ArrayCollection();
    }

    abstract public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool;

    abstract public function getProviderDistantID(): ?string;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBusinessNumber(): ?string
    {
        return $this->businessNumber;
    }

    public function setBusinessNumber(?string $businessNumber): self
    {
        $this->businessNumber = $businessNumber;

        return $this;
    }

    public function getCategory(): CategorySessionType
    {
        return $this->category;
    }

    public function setCategory(CategorySessionType $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): self
    {
        $this->duration = $duration;
        if (!is_null($this->getDateStart())) {
            $myDate = clone $this->getDateStart();
            $this->dateEnd = $myDate->modify('+ '.$duration.' min');
        }

        return $this;
    }

    public function getInformation(): ?string
    {
        return $this->information;
    }

    public function setInformation(?string $information): self
    {
        $this->information = $information;

        return $this;
    }

    public function isAutomaticSendingTrainingCertificates(): bool
    {
        return $this->automaticSendingTrainingCertificates;
    }

    public function setAutomaticSendingTrainingCertificates(bool $automaticSendingTrainingCertificates): self
    {
        $this->automaticSendingTrainingCertificates = $automaticSendingTrainingCertificates;

        return $this;
    }

    public function getSentTrainingCertificates(): ?DateTimeImmutable
    {
        return $this->sentTrainingCertificates;
    }

    public function setSentTrainingCertificates(?DateTimeImmutable $sentTrainingCertificates): self
    {
        $this->sentTrainingCertificates = $sentTrainingCertificates;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    public function setOrigin(?string $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    public function getTags(): ?array
    {
        return $this->tags;
    }

    public function setTags(?array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function getConnexionTypeSession(): ?string
    {
        return $this->connexionTypeSession;
    }

    public function setConnexionTypeSession(?string $connexionTypeSession): self
    {
        $this->connexionTypeSession = $connexionTypeSession;

        return $this;
    }

    public static function getProvider(): string
    {
        return static::$provider;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function isSessionCanceledLessOneDay(): bool
    {
        return $this->sessionCanceledLessOneDay;
    }

    public function setSessionCanceledLessOneDay(bool $sessionCanceledLessOneDay): self
    {
        $this->sessionCanceledLessOneDay = $sessionCanceledLessOneDay;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDateStart(): ?\DateTime
    {
        return $this->dateStart;
    }

    public function setDateStart(?\DateTime $dateStart): self
    {
        $this->dateStart = $dateStart;
        $this->dateStart?->setTime(
            $this->dateStart->format('H'),
            $this->dateStart->format('i'),
            00
        );

        return $this;
    }

    public function getDateEnd(): ?\DateTime
    {
        return $this->dateEnd;
    }

    public function setDateEnd(?\DateTime $dateEnd): self
    {
        $this->dateEnd = $dateEnd;
        $this->dateEnd?->setTime(
            $this->dateEnd->format('H'),
            $this->dateEnd->format('i'),
            00
        );
        $interval = $this->getDateStart()->diff($dateEnd);
        $total = $interval->d * 24 + $interval->h * 60 + $interval->i;
        $this->duration = $total;

        return $this;
    }

    public function getRef(): string
    {
        return $this->ref;
    }

    #[Groups(ProviderSessionApiController::GROUP_VIEW)]
    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    #[Groups('read_session')]
    public function getParticipantRoles(): ArrayCollection|Collection
    {
        return $this->participantRoles;
    }

    public function setParticipantRoles(ArrayCollection|Collection $participantRoles): self
    {
        $this->participantRoles = $participantRoles;

        return $this;
    }

    public function addParticipantRole(ProviderParticipantSessionRole $role): self
    {
        $this->participantRoles->add($role);

        return $this;
    }

    public function removeParticipantRole(ProviderParticipantSessionRole $role): self
    {
        if ($this->participantRoles->contains($role)) {
            $this->participantRoles->removeElement($role);
        }

        return $this;
    }

    #[Groups(['help_need:read', 'ui:module:read', 'read_session', 'session:evaluation:synthesis'])]
    public function getTraineesOnly(): array
    {
        return array_values(array_filter($this->participantRoles->toArray(), fn (ProviderParticipantSessionRole $participantRole
        ) => $participantRole->getRole() === ProviderParticipantSessionRole::ROLE_TRAINEE));
    }

    #[Groups(['help_need:read'])]
    public function getAnimatorsOnly(): array
    {
        return array_values(array_filter($this->participantRoles->toArray(), fn (ProviderParticipantSessionRole $participantRole
        ) => $participantRole->getRole() === ProviderParticipantSessionRole::ROLE_ANIMATOR));
    }

    public function getConvocation(): ?Convocation
    {
        return $this->convocation;
    }

    public function setConvocation(?Convocation $convocation): self
    {
        $this->convocation = $convocation;

        return $this;
    }

    public function getNotificationDate(): ?DateTimeImmutable
    {
        return $this->notificationDate;
    }

    public function setNotificationDate(?DateTimeImmutable $notificationDate): ProviderSession
    {
        $this->notificationDate = $notificationDate;

        return $this;
    }

    public function getDateOfSendingReminders(): array
    {
        sort($this->dateOfSendingReminders);

        return $this->dateOfSendingReminders;
    }

    public function setDateOfSendingReminders(array $dateOfSendingReminders): ProviderSession
    {
        foreach ($dateOfSendingReminders as $dateOfSendingReminder) {
            if (!($dateOfSendingReminder instanceof DateTimeImmutable)) {
                throw new \LogicException('Invalid date. Reminders dates of provider session must be an instance of DateTimeImmutable class');
            }
        }

        $this->dateOfSendingReminders = $dateOfSendingReminders;
        sort($this->dateOfSendingReminders);

        return $this;
    }

    public function getAttachments(): ArrayCollection|Collection
    {
        return $this->attachments;
    }

    public function setAttachments(ArrayCollection|Collection $attachments): self
    {
        $this->attachments = $attachments;
        /** @var SessionConvocationAttachment $attachment */
        foreach ($this->attachments as $attachment) {
            $attachment->setSession($this);
        }

        return $this;
    }

    public function isSendInvitationEmails(): bool
    {
        return $this->sendInvitationEmails;
    }

    public function setSendInvitationEmails(bool $sendInvitationEmails): ProviderSession
    {
        $this->sendInvitationEmails = $sendInvitationEmails;

        return $this;
    }

    #[Groups(['read_session', 'read_session_list', 'ui:registration_page_template:read'])]
    public function isSessionTest(): bool
    {
        return $this->sessionTest;
    }

    public function setSessionTest(bool $sessionTest): ProviderSession
    {
        $this->sessionTest = $sessionTest;

        return $this;
    }

    public function isEnableSendRemindersToAnimators(): bool
    {
        return $this->enableSendRemindersToAnimators;
    }

    public function setEnableSendRemindersToAnimators(bool $enableSendRemindersToAnimators): ProviderSession
    {
        $this->enableSendRemindersToAnimators = $enableSendRemindersToAnimators;

        return $this;
    }

    public function getPersonalizedContent(): ?string
    {
        return $this->personalizedContent;
    }

    public function setPersonalizedContent(?string $personalizedContent): ProviderSession
    {
        $this->personalizedContent = $personalizedContent;

        return $this;
    }

    public function getAbstractProviderConfigurationHolder(): AbstractProviderConfigurationHolder
    {
        return $this->abstractProviderConfigurationHolder;
    }

    public function setAbstractProviderConfigurationHolder(
        AbstractProviderConfigurationHolder $abstractProviderConfigurationHolder
    ): self {
        if (!$this->supportConfigurationHolder($abstractProviderConfigurationHolder)) {
            throw new InvalidArgumentException('The given configuration holder is not compatible with this session');
        }

        $this->abstractProviderConfigurationHolder = $abstractProviderConfigurationHolder;

        return $this;
    }

    public function getLastConvocationSent(): ?DateTimeImmutable
    {
        return $this->lastConvocationSent;
    }

    public function setLastConvocationSent(?DateTimeImmutable $lastConvocationSent): self
    {
        $this->lastConvocationSent = $lastConvocationSent;

        return $this;
    }

    public function getEvaluationPerformanceSent(): ?DateTimeImmutable
    {
        return $this->evaluationPerformanceSent;
    }

    public function setEvaluationPerformanceSent(?DateTimeImmutable $evaluationPerformanceSent): self
    {
        $this->evaluationPerformanceSent = $evaluationPerformanceSent;

        return $this;
    }

    public function getLastReminderSent(): ?DateTimeImmutable
    {
        return $this->lastReminderSent;
    }

    public function setLastReminderSent(?DateTimeImmutable $lastReminderSent): self
    {
        $this->lastReminderSent = $lastReminderSent;

        return $this;
    }

    #[Groups(['read_session', 'read_session_list', 'help_need:read', 'read_session_calendar'])]
    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    #[Groups(['read_session', 'read_session_list', 'help_need:read', 'ui:module:read', 'read_session_calendar'])]
    public function getDatesDiffString(): string
    {
        $diff = $this->getDateEnd()->diff($this->getDateStart());

        if ($this->getLanguage() === 'fr') {
            $union = 'h';
        } else {
            $union = ':';
        }

        return $diff ? $diff->h.$union.($diff->i < 10 ? '0'.$diff->i : $diff->i) : '';
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        if (!in_array($status, [self::STATUS_DRAFT, self::STATUS_SCHEDULED, self::STATUS_CANCELED])) {
            throw new \LogicException();
        }

        $this->status = $status;

        return $this;
    }

    public function getBounces(): ?Collection
    {
        return $this->bounces;
    }

    public function setBounces(?Collection $bounces): ProviderSession
    {
        $this->bounces = $bounces;

        return $this;
    }

    public function addBounce(Bounce $bounce): ProviderSession
    {
        $this->bounces->add($bounce);

        return $this;
    }

    public function removeBounce(Bounce $bounce): ProviderSession
    {
        $this->bounces->removeElement($bounce);

        return $this;
    }

    #[Groups(['read_session', 'read_session_list'])]
    public function isADraft(): bool
    {
        return $this->status === self::STATUS_DRAFT;
    }

    #[Groups(['read_session', 'read_session_list'])]
    public function isCancelled(): bool
    {
        return $this->status === self::STATUS_CANCELED;
    }

    #[Groups(['read_session', 'read_session_list', 'ui:subjects:hub', 'help_need:read', 'ui:module:read'])]
    public function isScheduled(): bool
    {
        return $this->status === self::STATUS_SCHEDULED && new \DateTime() < $this->dateStart;
    }

    #[Groups(['read_session', 'read_session_list', 'read_lobby_signature', 'ui:subjects:hub', 'help_need:read', 'ui:module:read', 'ui:registration_page_template:read'])]
    public function isRunning(): bool
    {
        return $this->status === self::STATUS_SCHEDULED && new \DateTime() >= $this->dateStart && new \DateTime() <= $this->getDateEnd();
    }

    #[Groups(['read_session', 'read_session_list', 'read_lobby_signature', 'ui:registration_page_template:read'])]
    public function isFinished(): bool
    {
        return $this->status === self::STATUS_SCHEDULED && new \DateTime() > $this->getDateEnd();
    }

    #[Groups(['read_session', 'read_session_2', 'read_session_list', 'help_need:read', 'ui:module:read', 'ui:subjects:hub', 'read_session_calendar'])]
    public function getReadableState(): string
    {
        if ($this->isADraft()) {
            return self::FINAL_STATUS_DRAFT;
        } elseif ($this->isCancelled()) {
            return self::FINAL_STATUS_CANCELED;
        } elseif ($this->isScheduled()) {
            if (($this->isConvocationSent() && $this->getCategory()->label() != self::FINAL_CATEGORY_WEBINAR) || ($this->isCommunicationSent() && $this->getCategory()->label() == self::FINAL_CATEGORY_WEBINAR)) {
                return self::FINAL_STATUS_SENT;
            } else {
                return self::FINAL_STATUS_SCHEDULED;
            }
        } elseif ($this->isRunning()) {
            return self::FINAL_STATUS_RUNNING;
        } else {
            return self::FINAL_STATUS_FINISHED;
        }
    }

    public function getNextReminder(): ?DateTimeImmutable
    {
        if (empty($this->lastReminderSent) && !empty($this->dateOfSendingReminders)) {
            return reset($this->dateOfSendingReminders);
        }

        foreach ($this->dateOfSendingReminders as $dateOfSendingReminder) {
            if ($dateOfSendingReminder > $this->lastReminderSent) {
                return $dateOfSendingReminder;
            }
        }

        return null;
    }

    #[Groups(['read_session'])]
    public function isConvocationSent(): bool
    {
        return !empty($this->notificationDate)
               && !empty($this->getLastConvocationSent());
    }

    #[Groups(['read_session'])]
    public function isCommunicationSent(): bool
    {
        foreach ($this->getWebinarConvocation() as $communication) {
            if ($communication->getConvocationType() == WebinarConvocation::TYPE_CONFIRMATION) {
                if ($communication->isSent()) {
                    return true;
                }
            }
        }

        return false;
    }

    #[Groups(['read_session'])]
    public function hasACommunicationConfirmation(): bool
    {
        foreach ($this->getWebinarConvocation() as $communication) {
            if ($communication->getConvocationType() == WebinarConvocation::TYPE_CONFIRMATION) {
                return true;
            }
        }

        return false;
    }

    #[Groups(['read_session'])]
    public function hasACommunicationThankYouType(): bool
    {
        if ($this->getCategory()->label() == self::FINAL_CATEGORY_WEBINAR) {
            foreach ($this->getWebinarConvocation() as $communication) {
                if ($communication->getConvocationType() == WebinarConvocation::TYPE_THANK_YOU) {
                    return true;
                }
            }
        }

        return false;
    }

    #[Groups(['read_lobby_signature'])]
    public function isEvaluationAvailable(): bool
    {
        /* TODO : completer la fonction pour tous les cas d'usage et les types d'évaluations - getAndValidFromTokenForEvaluationProviderSession - & ProviderSession -  */
        //return in_array($this->getReadableState(), [self::FINAL_STATUS_FINISHED, self::FINAL_STATUS_RUNNING]);
        return true;
    }

    #[Groups(['read_session'])]
    public function isAttendanceReportAvailable(): bool
    {
        $availableAt = (clone $this->dateStart)->add(\DateInterval::createFromDateString('1 day'));
        $now = new \DateTime();

        return $now > $availableAt && $this->isFinished() && !$this->isADraft();
    }

    #[Callback(groups: ['create', 'update'])]
    public function validateDates(ExecutionContextInterface $context, $payload): void
    {
        if ($this->getDateStart() >= $this->getDateEnd()) {
            $context->buildViolation('Start date must be anterior to end date.')
                ->atPath('dateStart')
                ->addViolation();
        }
    }

    #[ORM\PreUpdate]
    public function changeUpdateDate(): void
    {
        $this->updatedAt = new \DateTime();
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?UserInterface $user): ProviderSession
    {
        $this->createdBy = $user;

        return $this;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?UserInterface $updatedBy): ProviderSession
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getProviderAudio(): ?AbstractProviderAudio
    {
        return $this->providerAudio;
    }

    public function setProviderAudio(?AbstractProviderAudio $providerAudio): ProviderSession
    {
        $this->providerAudio = $providerAudio;

        return $this;
    }

    public function getEvaluations(): Collection
    {
        return $this->evaluations;
    }

    public function setEvaluations(Collection $evaluations): self
    {
        if (null === $this->oldEvaluations) {
            $this->oldEvaluations = $this->evaluations;
        }
        $this->evaluations = $evaluations;

        return $this;
    }

    public function getOldEvaluations(): ?Collection
    {
        return $this->oldEvaluations;
    }

    public function getNature(): ?TrainingActionNatureEnum
    {
        return $this->nature;
    }

    public function setNature(?TrainingActionNatureEnum $nature): self
    {
        $this->nature = $nature;

        return $this;
    }

    public function getObjectives(): ?array
    {
        return $this->objectives;
    }

    public function setObjectives(?array $objectives): self
    {
        $this->objectives = $objectives;

        return $this;
    }

    public function dumpSessionBeforeDelete(): array
    {
        $dump = [
            'id' => $this->getId(),
            'ref' => $this->getRef(),
            'name' => $this->getName(),
            'dateStart' => $this->getDateStart()->format('c'),
            'dateEnd' => $this->getDateEnd()->format('c'),
            'duration' => $this->getDuration(),
            'category' => $this->getCategory(),
            'client' => $this->getAbstractProviderConfigurationHolder()->getClient()->getName(),
            'numberParticipants' => count($this->getParticipantRoles()),
            'participants' => [],
            'animators' => [],
        ];

        foreach ($this->getParticipantRoles() as $role) {
            if ($role->getRole() === ProviderParticipantSessionRole::ROLE_TRAINEE) {
                $dump['participants'][] = ['name' => $role->getParticipant()->getFullName(), 'email' => $role->getParticipant()->getEmail(), 'id' => $role->getParticipant()->getUserId()];
            } elseif ($role->getRole() === ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                $dump['animators'][] = ['name' => $role->getParticipant()->getFullName(), 'email' => $role->getParticipant()->getEmail(), 'id' => $role->getParticipant()->getUserId()];
            }
        }

        return $dump;
    }

    #[Groups(['read_session', 'read_lobby_signature'])]
    public function isFullDaySession(): bool
    {
        return
            strtotime($this->getDateStart()->format('H:i:s')) < strtotime(date('12:30:00')) &&
            strtotime($this->getDateEnd()->format('H:i:s')) > strtotime(date('12:30:00'))
        ;
    }

    public function getAssistanceType(): ?int
    {
        return $this->assistanceType;
    }

    public function setAssistanceType(?int $assistanceType): ProviderSession
    {
        $this->assistanceType = $assistanceType;

        return $this;
    }

    public function getSupportType(): ?int
    {
        return $this->supportType;
    }

    public function setSupportType(?int $supportType): ProviderSession
    {
        $this->supportType = $supportType;

        return $this;
    }

    public function isStandardHours(): bool
    {
        return $this->standardHours;
    }

    public function setStandardHours(bool $standardHours): ProviderSession
    {
        $this->standardHours = $standardHours;

        return $this;
    }

    public function isOutsideStandardHours(): bool
    {
        return $this->outsideStandardHours;
    }

    public function setOutsideStandardHours(bool $outsideStandardHours): ProviderSession
    {
        $this->outsideStandardHours = $outsideStandardHours;

        return $this;
    }

    public function getRestlunch(): ?int
    {
        return $this->restlunch;
    }

    public function setRestlunch(?int $restlunch): ProviderSession
    {
        $this->restlunch = $restlunch;

        return $this;
    }

    public function getNbrdays(): ?int
    {
        return $this->nbrdays;
    }

    public function setNbrdays(?int $nbrdays): ProviderSession
    {
        $this->nbrdays = $nbrdays;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): ProviderSession
    {
        $this->location = $location;

        return $this;
    }

    #[Groups(['read_session', 'read_session_list'])]
    public function isSessionPresential(): bool
    {
        return $this->category === CategorySessionType::CATEGORY_SESSION_PRESENTIAL;
    }

    #[Groups(['read_session', 'read_session_list'])]
    public function isSessionMixte(): bool
    {
        return $this->category === CategorySessionType::CATEGORY_SESSION_MIXTE;
    }

    #[Groups(['read_session', 'read_session_list'])]
    public function isSessionPresentialOrMixte(): bool
    {
        return $this->category === CategorySessionType::CATEGORY_SESSION_PRESENTIAL || $this->category === CategorySessionType::CATEGORY_SESSION_MIXTE;
    }

    public function __toString(): string
    {
        return "{$this->getId()} - {$this->getRef()} - {$this->getName()}";
    }

    #[Groups(['read_session_list', 'read_lobby_signature', 'read_session_calendar'])]
    public function getAnimatorsCount(): int
    {
        return count($this->getAnimatorsOnly());
    }

    #[Groups(['read_session_list', 'read_lobby_signature', 'ui:subjects:hub', 'read_session_calendar'])]
    public function getTraineesCount(): int
    {
        return count($this->getTraineesOnly());
    }

    #[Groups(['read_session_list', 'read_lobby_signature', 'ui:subjects:hub', 'read_session_calendar'])]
    public function getAvailableSeats(): int
    {
        return $this->subscriptionMax - count($this->getTraineesOnly());
    }

    #[Groups(['read_session'])]
    public function getIsSendTrainingCertificatesClient(): bool
    {
        return $this->getAbstractProviderConfigurationHolder()->getClient()->getEmailOptions()->isSendAnAttestationAutomatically();
    }

    public function getHelpNeeds(): ArrayCollection|Collection
    {
        return $this->helpNeeds;
    }

    public function setHelpNeeds(ArrayCollection|Collection $helpNeeds): self
    {
        $this->helpNeeds = $helpNeeds;

        return $this;
    }

    public function addHelpNeed(HelpNeed $helpNeed): self
    {
        $this->helpNeeds->add($helpNeed);

        return $this;
    }

    public function removeHelpNeed(HelpNeed $helpNeed): self
    {
        if (!$this->helpNeeds->contains($helpNeed)) {
            return $this;
        }

        $this->helpNeeds->removeElement($helpNeed);
        $helpNeed->setSession(null);

        return $this;
    }

    public function getHelpNeedsByType(int $type): array
    {
        return array_values($this->helpNeeds->filter(fn (HelpNeed $helpNeed) => $helpNeed->getType() === $type)->toArray());
    }

    #[Groups(['read_session', 'read_session_list'])]
    public function getSupportStatus(): ?int
    {
        $dateNow = new \DateTime();
        $supportHelpNeeds = $this->getHelpNeedsByType(HelpNeed::TYPE_SUPPORT);
        /** @var HelpNeed $supportHelpNeed */
        foreach ($supportHelpNeeds as $supportHelpNeed) {
            if ($dateNow > $supportHelpNeed->getEnd()) {
                return self::HELP_TASK_CLOSED;
            }

            /** @var HelpTask $supportHelpTask */
            foreach ($supportHelpNeed->getHelpTasks() as $supportHelpTask) {
                if ($dateNow >= $supportHelpTask->getStart() &&
                    $dateNow < $supportHelpTask->getEnd()) {
                    return self::HELP_TASK_OPEN;
                }
            }
        }

        return self::HELP_TASK_PROGRAMMED;
    }

    #[Groups(['read_session', 'read_session_list'])]
    public function getAssistanceStatus(): ?int
    {
        $dateNow = new DateTimeImmutable();
        $assistanceHelpNeeds = $this->getHelpNeedsByType(HelpNeed::TYPE_ASSISTANCE);
        $status = self::HELP_TASK_PROGRAMMED;
        /** @var HelpNeed $assistanceHelpNeed */
        foreach ($assistanceHelpNeeds as $assistanceHelpNeed) {
            if ($dateNow > $assistanceHelpNeed->getEnd()) {
                $status = self::HELP_TASK_CLOSED;
            }

            /** @var HelpTask $assistanceHelpTask */
            foreach ($assistanceHelpNeed->getHelpTasks() as $assistanceHelpTask) {
                if ($dateNow >= $assistanceHelpTask->getStart() &&
                    $dateNow < $assistanceHelpTask->getEnd()) {
                    $status = self::HELP_TASK_OPEN;

                    if ($assistanceHelpTask->getDateTakingAssistance()) {
                        return self::HELP_TASK_TAKEN;
                    }
                }
            }
        }

        return $status;
    }

    #[Groups(['read_session'])]
    public function getAssistanceLogBook(): array
    {
        $answers = [];
        $assistanceHelpNeeds = $this->getHelpNeedsByType(HelpNeed::TYPE_ASSISTANCE);
        /** @var HelpNeed $assistanceHelpNeed */
        foreach ($assistanceHelpNeeds as $assistanceHelpNeed) {
            $helpTasks = $assistanceHelpNeed->getHelpTasks();
            /** @var HelpTask $helpTak */
            foreach ($helpTasks as $helpTak) {
                $currentAnswer = $helpTak->getAnswer();
                if ($currentAnswer !== []) {
                    $currentAnswer['plainData']['animator']['name'] = $helpTak->getUser()->getFullName();
                    if (isset($currentAnswer['replyDate'])) {
                        $currentAnswer['plainData']['replyDateTime'] = $currentAnswer['replyDate'];
                    }
                    $answers[] = $currentAnswer['plainData'];
                }
            }
        }

        return $answers;
    }

    #[Groups(['read_session', 'read_session_list'])]
    public function getSubject(): ?Subject
    {
        return $this->subject;
    }

    public function setSubject(?Subject $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getSubscriptionMax(): int
    {
        return $this->subscriptionMax;
    }

    public function setSubscriptionMax(int $subscriptionMax): self
    {
        $this->subscriptionMax = $subscriptionMax;

        return $this;
    }

    public function getAlertLimit(): int
    {
        return $this->alertLimit;
    }

    public function setAlertLimit(int $alertLimit): self
    {
        $this->alertLimit = $alertLimit;

        return $this;
    }

    public function isSentPresenceReport(): bool
    {
        return $this->sentPresenceReport;
    }

    public function setSentPresenceReport(bool $sentPresenceReport): self
    {
        $this->sentPresenceReport = $sentPresenceReport;

        return $this;
    }

    #[Groups(['read_session', 'read_session_list', 'help_need:read'])]
    public function getTitleAttachementModuleThemeSubject(): ?string
    {
        if ($this->getSubject()) {
            return $this->subject->getTheme()->getModule()->getName().' / '.$this->subject->getTheme()->getName().' / '.$this->subject->getName();
        }

        return '';
    }

    public function getLastPresenceStatusSynchronization(): ?DateTimeImmutable
    {
        return $this->lastPresenceStatusSynchronization;
    }

    public function setLastPresenceStatusSynchronization(?DateTimeImmutable $lastPresenceStatusSynchronization): self
    {
        $this->lastPresenceStatusSynchronization = $lastPresenceStatusSynchronization;

        return $this;
    }

    #[Groups(['read_session', 'read_session_list'])]
    public function getTitleAttachementSubject(): ?string
    {
        if ($this->getSubject()) {
            return $this->subject->getName();
        }

        return '';
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription2(): ?string
    {
        return $this->description2;
    }

    public function setDescription2(?string $description2): self
    {
        $this->description2 = $description2;

        return $this;
    }

    #[Groups(['read_session'])]
    public function getWebinarConvocation(): Collection
    {
        return $this->webinarConvocation;
    }

    public function setWebinarConvocation(Collection $webinarConvocation): self
    {
        $this->webinarConvocation = $webinarConvocation;

        return $this;
    }

    public function updateWebinarConvocation(array $webinarConvocation): self
    {
        $this->webinarConvocation = new ArrayCollection();

        foreach ($webinarConvocation as $communication) {
            $this->webinarConvocation->add($communication);
        }

        return $this;
    }

    public function getReplayVideoPlatform(): ?string
    {
        return $this->replayVideoPlatform;
    }

    public function setReplayVideoPlatform(?string $replayVideoPlatform): self
    {
        $this->replayVideoPlatform = $replayVideoPlatform;

        return $this;
    }

    public function getReplayUrlKey(): ?string
    {
        return $this->replayUrlKey;
    }

    public function setReplayUrlKey(?string $replayUrlKey): self
    {
        $this->replayUrlKey = $replayUrlKey;

        return $this;
    }

    public function isPublishedReplay(): ?bool
    {
        return $this->publishedReplay;
    }

    public function setPublishedReplay(?bool $publishedReplay): void
    {
        $this->publishedReplay = $publishedReplay;
    }

    public function getRegistrationPageTemplate(): ?RegistrationPageTemplate
    {
        return $this->registrationPageTemplate;
    }

    public function setRegistrationPageTemplate(?RegistrationPageTemplate $registrationPageTemplate): self
    {
        $this->registrationPageTemplate = $registrationPageTemplate;

        return $this;
    }

    public function getProviderSessionRegistrationPage(): ?ProviderSessionRegistrationPage
    {
        return $this->providerSessionRegistrationPage;
    }

    public function getSpeakers(): Collection
    {
        return $this->speakers;
    }

    public function setSpeakers(Collection $speakers): self
    {
        $this->speakers = $speakers;

        return $this;
    }

    public function addSpeaker(ProviderSessionSpeaker $speaker): self
    {
        if (!$this->speakers->contains($speaker)) {
            $this->speakers->add($speaker);
        }

        return $this;
    }

    public function removeSpeaker(ProviderSessionSpeaker $speaker): self
    {
        if ($this->speakers->contains($speaker)) {
            $this->speakers->removeElement($speaker);
        }

        return $this;
    }

    public function setProviderSessionRegistrationPage(?ProviderSessionRegistrationPage $providerSessionRegistrationPage): self
    {
        $this->providerSessionRegistrationPage = $providerSessionRegistrationPage;

        return $this;
    }

    #[Groups(['read_session', 'read_client', 'ui:subjects:hub', 'ui:module:read'])]
    public function getCloseSubscriptionsAfterDate(): bool
    {
        if (!empty($this->getSubject())) {
            if ($this->getSubject()->getTheme()->getModule()->isCloseSubscriptions()) {
                $today = new \DateTime();
                $sessionDateStart = $this->getDateStart();
                $minus = $this->getSubject()->getTheme()->getModule()->getCloseSubscriptionsBeforeNbrDays();

                if ($minus != 0) {
                    $sessionDateStartModify = (clone $sessionDateStart)->modify('-'.$minus.' days');
                    if ($today > $sessionDateStartModify) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getLegendAssistanceType(): ?string
    {
        return match ($this->assistanceType) {
            self::ENUM_NO => 'No assistance',
            self::ENUM_LAUNCH => 'Launch',
            self::ENUM_TOTAL => 'Total',
            self::ENUM_PEDA => 'Educational',
            self::ENUM_COANIM => 'Co-animation',
        };
    }

    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:hub'])]
    public function getRegistrationPagePlaceAvailable(): ?int
    {
        if ($this->providerSessionRegistrationPage && $this->providerSessionRegistrationPage->getPlaces() !== 0 && $this->providerSessionRegistrationPage->getPlaces() !== null) {
            return max($this->providerSessionRegistrationPage->getPlaces() - $this->getTraineesCount(), 0);
        }

        return 0;
    }

    public function canShowRegistrationPage(): bool
    {
        return in_array($this->getReadableState(),
            [
                self::FINAL_STATUS_SENT,
                self::FINAL_STATUS_FINISHED,
                self::FINAL_STATUS_RUNNING,
            ]);
    }

    public function getReplays(): Collection
    {
        return $this->replays;
    }

    public function setReplays(Collection $replays): self
    {
        $this->replays = $replays;

        return $this;
    }

    public function getPhotoReplay(): ?string
    {
        return $this->photoReplay;
    }

    public function setPhotoReplay(?string $photoReplay): void
    {
        $this->photoReplay = $photoReplay;
    }

    public function getReplayLayoutPlayButton(): ?bool
    {
        return $this->replayLayoutPlayButton;
    }

    public function setReplayLayoutPlayButton(?bool $replayLayoutPlayButton): void
    {
        $this->replayLayoutPlayButton = $replayLayoutPlayButton;
    }

    public function getAdditionalTimezones(): array
    {
        sort($this->additionalTimezones);

        return $this->additionalTimezones;
    }

    public function setAdditionalTimezones(array $additionalTimezones): ProviderSession
    {
        $this->additionalTimezones = $additionalTimezones;
        sort($this->dateOfSendingReminders);

        return $this;
    }

    public function getRegistrationPageTemplateBanner(): ?string
    {
        return $this->registrationPageTemplateBanner;
    }

    public function setRegistrationPageTemplateBanner(?string $registrationPageTemplateBanner): void
    {
        $this->registrationPageTemplateBanner = $registrationPageTemplateBanner;
    }

    public function getRegistrationPageTemplateIllustration(): ?string
    {
        return $this->registrationPageTemplateIllustration;
    }

    public function setRegistrationPageTemplateIllustration(?string $registrationPageTemplateIllustration): void
    {
        $this->registrationPageTemplateIllustration = $registrationPageTemplateIllustration;
    }

    public function getAccessParticipantReplay(): ?bool
    {
        return $this->accessParticipantReplay;
    }

    public function setAccessParticipantReplay(?bool $accessParticipantReplay): self
    {
        $this->accessParticipantReplay = $accessParticipantReplay;

        return $this;
    }

    public function getGroupsParticipants(): array
    {
        return $this->groupsParticipants;
    }

    public function setGroupsParticipants(array $groupsParticipants): self
    {
        $this->groupsParticipants = $groupsParticipants;

        return $this;
    }

    public function getEvaluationsProviderSession(): Collection
    {
        return $this->evaluationsProviderSession;
    }

    public function setEvaluationsProviderSession(Collection $evaluationsProviderSession): ProviderSession
    {
        $this->evaluationsProviderSession = $evaluationsProviderSession;

        return $this;
    }

    public function updateEvaluationsProviderSession(array $evaluationsProviderSession): self
    {
        $this->evaluationsProviderSession = new ArrayCollection();

        foreach ($evaluationsProviderSession as $evaluationProviderSession) {
            $this->evaluationsProviderSession->add($evaluationProviderSession);
        }

        return $this;
    }

    public function getPercentageAttendance(): int
    {
        return $this->percentageAttendance;
    }

    public function setPercentageAttendance(int $percentageAttendance): ProviderSession
    {
        $this->percentageAttendance = $percentageAttendance;

        return $this;
    }

    #[Groups(['read_session'])]
    public function getCanUpdatePercentageAttendance(): array
    {
        if ($this->getClient()->getName() === 'Renault') {
            $dateStartSession = (clone $this->getDateStart());
            $dateStartSessionAfter = $dateStartSession->modify('+2 day');
            if (new \DateTimeImmutable() <= $dateStartSessionAfter) {
                return ['status' => true];
            }

            return ['status' => false, 'message' => 'The modification period for Renault has expired'];
        }

        if ($this->getLastPresenceStatusSynchronization() === null) {
            return ['status' => true];
        }

        return ['status' => false, 'message' => 'Last presence status synchronization is not null'];
    }

    #[Groups(['read_session'])]
    public function getTitle(): string
    {
        if ($this->getSubject()) {
            return $this->getSubject()->getName();
        }

        return $this->getName();
    }
}
