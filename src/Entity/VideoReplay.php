<?php

namespace App\Entity;

use App\SelfSubscription\Entity\Module;
use App\SelfSubscription\Entity\Subject;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class VideoReplay
{
    public const REPLAY_PLATFORM_VIMEO = 'vimeo';
    public const REPLAY_PLATFORM_OTHER = 'other';
    public const REPLAY_PLATFORM_YOUTUBE = 'youtube';
    public const REPLAY_PLATFORM_DAILYMOTION = 'dailymotion';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub', 'ui:edit:subject', 'ui:video:replay:update', 'ui:subjects:hub', 'ui:subject:update'])]
    private int $id;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Assert\Choice([
        self::REPLAY_PLATFORM_VIMEO,
        self::REPLAY_PLATFORM_OTHER,
        self::REPLAY_PLATFORM_YOUTUBE,
        self::REPLAY_PLATFORM_DAILYMOTION,
    ])]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub', 'ui:edit:subject', 'ui:video:replay:update', 'ui:subjects:hub', 'ui:subject:update'])]
    protected ?string $platform = self::REPLAY_PLATFORM_VIMEO;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub', 'ui:edit:subject', 'ui:video:replay:update', 'ui:subjects:hub', 'ui:subject:update'])]
    protected ?string $urlKey = null;

    #[ORM\Column(type: 'boolean', nullable: true, options: ['default' => true])]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub', 'ui:edit:subject', 'ui:video:replay:update', 'read_client', 'read_session', 'ui:subjects:hub', 'ui:subject:update'])]
    protected ?bool $published = false;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub', 'ui:edit:subject', 'ui:video:replay:update', 'ui:subjects:hub', 'ui:subject:update'])]
    private ?string $photoReplay = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub', 'ui:edit:subject', 'ui:video:replay:update', 'ui:subjects:hub', 'ui:subject:update'])]
    private ?string $description = null;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => 'The trainings'])]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub', 'ui:edit:subject', 'ui:video:replay:update', 'ui:subjects:hub', 'ui:subject:update'])]
    private ?string $title = 'Titre of the video';

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub', 'ui:edit:subject', 'ui:video:replay:update', 'ui:subjects:hub', 'ui:subject:update'])]
    protected ?\DateTimeImmutable $publishedDate;

    #[ORM\Column(type: 'json')]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub', 'ui:edit:subject', 'ui:video:replay:update', 'ui:subjects:hub', 'ui:subject:update'])]
    private array $catalogsReplay = [];

    #[ORM\ManyToOne(targetEntity: Module::class, inversedBy: 'videosReplay')]
    #[Groups(['ui:subject:update'])]
    private Module $module;

    #[ORM\OneToMany(mappedBy: 'videoReplay', targetEntity: VideoReplayViewer::class, cascade: ['persist'])]
    #[Groups(['ui:subject:update'])]
    private Collection $videosReplaysViewers;

    #[ORM\OneToOne(inversedBy: 'videoReplay', targetEntity: Subject::class)]
    #[Groups(['ui:subject:update'])]
    private ?Subject $subject;

    public function __construct()
    {
        $this->publishedDate = new \DateTimeImmutable();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    public function setPlatform(?string $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    public function getUrlKey(): ?string
    {
        return $this->urlKey;
    }

    public function setUrlKey(?string $urlKey): self
    {
        $this->urlKey = $urlKey;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(?bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getPhotoReplay(): ?string
    {
        return $this->photoReplay;
    }

    public function setPhotoReplay(?string $photoReplay): self
    {
        $this->photoReplay = $photoReplay;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPublishedDate(): ?\DateTimeImmutable
    {
        return $this->publishedDate;
    }

    public function setPublishedDate(?\DateTimeImmutable $publishedDate): self
    {
        $this->publishedDate = $publishedDate;

        return $this;
    }

    public function getModule(): Module
    {
        return $this->module;
    }

    public function setModule(Module $module): self
    {
        $this->module = $module;

        return $this;
    }

    #[Groups(['ui:module:read', 'ui:edit:subject', 'ui:video:replay:update', 'ui:subject:update'])]
    public function getModuleId(): int
    {
        return $this->module->getId();
    }

    public function getCatalogsReplay(): array
    {
        return $this->catalogsReplay;
    }

    public function setCatalogsReplay(array $catalogsReplay): self
    {
        $this->catalogsReplay = $catalogsReplay;

        return $this;
    }

    public function addCatalogReplay(string $catalogReplay): self
    {
        $this->catalogsReplay[] = $catalogReplay;

        return $this;
    }

    public function getVideosReplaysViewers(): Collection
    {
        return $this->videosReplaysViewers;
    }

    public function setVideosReplaysViewers(Collection $videosReplaysViewers): self
    {
        $this->videosReplaysViewers = $videosReplaysViewers;

        return $this;
    }

    public function getSubject(): ?Subject
    {
        return $this->subject;
    }

    public function setSubject(?Subject $subject): void
    {
        $this->subject = $subject;
    }

    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub', 'ui:edit:subject', 'ui:video:replay:update', 'ui:subject:update'])]
    public function getSubjectId(): ?int
    {
        return $this->subject?->getId();
    }
}
