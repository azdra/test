<?php

namespace App\Entity;

use App\Controller\Api\ProviderSessionApiController;
use App\Controller\Api\UserApiController;
use App\CustomerService\Controller\HelperController;
use App\CustomerService\Entity\AvailabilityUser;
use App\CustomerService\Entity\HelpTask;
use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Cisco\WebexParticipant;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use LogicException;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: 'App\Repository\UserRepository')]
#[UniqueEntity(fields: ['email', 'client'], message: 'This address email already exist with this client', errorPath: 'email')]
#[ORM\UniqueConstraint(name: 'users_idx', columns: ['email', 'client_id'])]
#[ORM\UniqueConstraint(columns: ['token'])]
#[ORM\HasLifecycleCallbacks]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    public const ALIAS = 'user';

    public const MAX_RESULT_LIST = 50;

    public const ID_USER_GUEST = 99999999999999;

    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_MANAGER = 'ROLE_MANAGER';
    public const ROLE_ADMIN_MANAGER = 'ROLE_ADMIN_MANAGER';
    public const ROLE_TRAINEE = 'ROLE_TRAINEE';
    public const ROLE_HOTLINE_CUSTOMER_SERVICE = 'ROLE_HOTLINE_CUSTOMER_SERVICE';
    public const ROLE_CUSTOMER_SERVICE = 'ROLE_CUSTOMER_SERVICE';
    public const ROLE_API = 'ROLE_API';
    public const TYPES_TO_EXCLUDE = [
        'firstName',
        'lastName',
        'phone_system',
        'company',
        'password',
        'email_system',
    ];

    public const ROLES = [
        'Participant' => self::ROLE_TRAINEE,
        'Super Admin' => self::ROLE_ADMIN,
        'Account admin' => self::ROLE_ADMIN_MANAGER,
        'Account manager' => self::ROLE_MANAGER,
        'Account hotline customer service' => self::ROLE_HOTLINE_CUSTOMER_SERVICE,
        'Account customer service' => self::ROLE_CUSTOMER_SERVICE,
        'Account API user' => self::ROLE_API,
    ];

    public const OPTION_SKILL_AC = 'OPTION_SKILL_AC';
    public const OPTION_SKILL_WT = 'OPTION_SKILL_WT';
    public const OPTION_SKILL_WM = 'OPTION_SKILL_WM';
    public const OPTION_SKILL_RETAIL = 'OPTION_SKILL_RETAIL';

    public const STATUS_ARCHIVE = -1;
    public const STATUS_INACTIVE = 0;
    public const STATUS_ACTIVE = 1;

    public const EMAIL_ONLY_FOR_MY_SESSIONS = 'EmailOnlyForMySessions';
    public const EMAIL_SESSION_WITHOUT_ANIMATOR = 'EmailSessionWithoutAnimator';
    public const EMAIL_SESSION_WITHOUT_TRAINEE = 'EmailSessionWithoutTrainee';
    public const EMAIL_SESSION_CANCELED = 'EmailSessionCanceled';
    public const EMAIL_SESSION_UNCANCELED = 'EmailSessionUncanceled';
    public const EMAIL_ANIMATOR_ON_SEVERAL_SESSIONS = 'EmailAnimatorOnSeveralSessions';
    public const EMAIL_TRAINEE_ON_SEVERAL_SESSIONS = 'EmailTraineeOnSeveralSessions';
    public const EMAIL_NO_ANIMATOR_WITH_HOST_ON_SESSION = 'EmailNoAnimatorWithHostOnSession';
    public const EMAIL_SEVERAL_ANIMATORS_ON_SAME_SESSION = 'EmailSeveralAnimatorsOnSameSession';
    public const EMAIL_INVALID_NOTIFICATION = 'EmailInvalidNotification';
    public const EMAIL_NO_MORE_CREDIT = 'EmailNoMoreCredit';
    public const EMAILS_OPTIONS_ALL = [
        self::EMAIL_ONLY_FOR_MY_SESSIONS => false,
        self::EMAIL_SESSION_WITHOUT_ANIMATOR => false,
        self::EMAIL_SESSION_WITHOUT_TRAINEE => false,
        self::EMAIL_SESSION_CANCELED => false,
        self::EMAIL_SESSION_UNCANCELED => false,
        self::EMAIL_ANIMATOR_ON_SEVERAL_SESSIONS => false,
        self::EMAIL_TRAINEE_ON_SEVERAL_SESSIONS => false,
        self::EMAIL_NO_ANIMATOR_WITH_HOST_ON_SESSION => false,
        self::EMAIL_SEVERAL_ANIMATORS_ON_SAME_SESSION => false,
        self::EMAIL_INVALID_NOTIFICATION => false,
        self::EMAIL_NO_MORE_CREDIT => false,
    ];

    public const REDIS_USER_ACCOUNTS_AVAILABLE_KEY = 'users_accounts_available_';

    public const LANGUAGES = [
        'fr' => 'French',
        'en' => 'English',
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_user', 'read_bounce', 'read_participant', 'collection:vuejs', 'read_webex_user', 'read_webex_rest_user',
        'read_microsoft_teams_user', 'read_microsoft_teams_event_user', 'read_user_session_list', 'switch_user',
        UserApiController::GROUP_VIEW, 'read_helpers', 'filter:view',
        HelperController::GROUP_VIEW, 'help_need:read', ProviderSessionApiController::GROUP_CREATE,
        'ui:user:hub', 'ui:module:read', 'read_adobe_user', 'ui:registration_page_template:read', 'read_users',
    ])]
    private int $id;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups([
        'read_user', 'read_user_module', 'read_session', 'collection:vuejs', 'read_user_session_list', 'read_helpers', 'read_lobby_signature', 'read_grouped_activities',
        UserApiController::GROUP_VIEW, UserApiController::GROUP_CREATE, 'ui:user:hub', ]
    )]
    #[Assert\NotBlank(allowNull: true)]
    private ?string $firstName = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_user', 'read_user_module', 'read_session', 'collection:vuejs', 'read_user_session_list', 'read_helpers', 'read_lobby_signature',
        UserApiController::GROUP_VIEW, UserApiController::GROUP_CREATE,
        'ui:user:hub', ])]
    #[Assert\NotBlank(allowNull: true)]
    private ?string $lastName = null;

    #[ORM\Column(type: 'string', length: 180)]
    #[Groups([
        'read_user', 'read_session', 'collection:vuejs', 'read_webex_user', 'read_webex_rest_user', 'read_helpers', 'read_microsoft_teams_user', 'read_microsoft_teams_event_user', 'read_white_connector_user',
        'read_grouped_activities', 'read_user_session_list', 'filter:view', 'help_need:read',
        HelperController::GROUP_VIEW, UserApiController::GROUP_VIEW, UserApiController::GROUP_CREATE,
        'ui:user:hub', 'read_adobe_user', 'read_users', 'read_user_module',
    ])]
    #[Assert\Email(groups: ['create', 'update', UserApiController::GROUP_CREATE, UserApiController::GROUP_UPDATE])]
    #[Assert\NotBlank(groups: ['create', 'update', UserApiController::GROUP_CREATE, UserApiController::GROUP_UPDATE])]
    private string $email;

    #[ORM\Column(type: 'json')]
    #[Groups(['read_user', 'read_user_session_list'])]
    private array $roles = [self::ROLE_TRAINEE];

    #[ORM\Column(type: 'smallint')]
    #[Groups(['read_helpers'])]
    private int $status = self::STATUS_ACTIVE;

    #[ORM\Column(type: 'string')]
    private string $password;

    private ?string $clearPassword = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_user', 'read_helpers', 'read_user_module', 'read_users', HelperController::GROUP_VIEW, UserApiController::GROUP_VIEW, UserApiController::GROUP_CREATE])]
    private ?string $reference;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups([
        'collection:vuejs', UserApiController::GROUP_VIEW, UserApiController::GROUP_CREATE,
        'ui:user:hub',
    ])]
    #[Assert\NotBlank(allowNull: true)]
    private ?string $company = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_user', 'collection:vuejs', UserApiController::GROUP_VIEW, UserApiController::GROUP_CREATE])]
    #[Assert\NotBlank(allowNull: true)]
    private ?string $phone = null;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_user', 'read_session', 'read_grouped_activities', 'collection:vuejs', 'read_user_session_list'])]
    #[Assert\NotBlank]
    private string $preferredLang;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Assert\Email(groups: ['create', 'update', UserApiController::GROUP_CREATE, UserApiController::GROUP_UPDATE])]
    private ?string $emailReferrer = null;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_user', 'read_users'])]
    private DateTimeInterface $createdAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?DateTimeInterface $updatedAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?DateTimeInterface $lastLogin;

    #[ORM\Column(type: 'json')]
    private array $options = [];

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $passwordResetToken = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $lastPasswordReset = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:user:hub'])]
    private ?string $profilePictureFilename = null;

    #[ORM\Column(type: 'string', options: ['default' => '_home'])]
    private string $preferredInitialRoute = '_home';

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups([UserApiController::GROUP_VIEW, UserApiController::GROUP_CREATE])]
    private ?string $information;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'users')]
    #[ORM\JoinColumn(name: 'client_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(['read_user', 'collection:vuejs', 'switch_user', 'read_session'])]
    private Client $client;

    #[ORM\OneToMany(mappedBy: 'userOrigin', targetEntity: AsyncTask::class, cascade: ['all'])]
    private Collection $asyncTasks;

    #[ORM\OneToMany(mappedBy: 'userOrigin', targetEntity: DeferredActionsTask::class, cascade: ['all'])]
    private Collection $deferredActionsTasks;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ActivityLog::class, cascade: ['all'])]
    private Collection $activityLogs;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['collection:vuejs'])]
    private ?string $referenceClient;

    #[ORM\Column(type: 'boolean', nullable: false)]
    #[Groups(['read_helpers'])]
    private bool $intern = false;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: ProviderParticipant::class, cascade: ['all'])]
    protected Collection $participants;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: Bounce::class, cascade: ['all'])]
    private ?Collection $bounces = null;

    #[ORM\Column(type: 'json')]
    #[Groups(['read_user'])]
    private array $preferences = self::EMAILS_OPTIONS_ALL;

    #[ORM\Column(type: 'string')]
    private string $token;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: HelpTask::class, cascade: ['all'])]
    private Collection $helpTasks;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: AvailabilityUser::class, cascade: ['all'])]
    private Collection $availabilityUsers;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: WidgetNews::class, cascade: ['all'])]
    private Collection $widgetNews;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: VideoReplayViewer::class, cascade: ['persist'])]
    private Collection $videosReplaysViewers;

    #[ORM\ManyToMany(targetEntity: GroupParticipant::class, inversedBy: 'users')]
    #[ORM\JoinTable(name: 'user_group')]
    private Collection $groups;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: UserModule::class, cascade: ['persist'])]
    #[Groups(['read_session', 'read_lobby_signature', 'session:evaluation:synthesis'])]
    protected Collection $userModules;

    #[ORM\Column(type: 'json')]
    #[Groups(['read_session'])]
    private ?array $registrationFormCatalogResponses = [];

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: SmsBounce::class, cascade: ['all'])]
    protected ?Collection $smsBounces = null;

    public function __construct()
    {
        $this->preferredLang = 'fr';
        $this->createdAt = new DateTime();
        $this->asyncTasks = new ArrayCollection();
        $this->deferredActionsTasks = new ArrayCollection();
        $this->activityLogs = new ArrayCollection();
        $this->participants = new ArrayCollection();
        $this->helpTasks = new ArrayCollection();
        $this->availabilityUsers = new ArrayCollection();
        $this->widgetNews = new ArrayCollection();
        $this->videosReplaysViewers = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->smsBounces = new ArrayCollection();
        $this->generateToken();
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEmailReferrer(): ?string
    {
        return $this->emailReferrer;
    }

    public function setEmailReferrer(?string $emailReferrer): void
    {
        $this->emailReferrer = $emailReferrer;
    }

    public function getUsername(): string
    {
        return $this->email;
    }

    public function getUserIdentifier(): string
    {
        return $this->getEmail();
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->getStatus() === self::STATUS_ACTIVE;
    }

    public function isIntern(): bool
    {
        return $this->intern;
    }

    public function setIntern(bool $intern): self
    {
        $this->intern = $intern;

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getClearPassword(): ?string
    {
        return $this->clearPassword;
    }

    public function setClearPassword(?string $clearPassword): self
    {
        $this->clearPassword = $clearPassword;

        return $this;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials(): void
    {
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPreferredLang(): string
    {
        return $this->preferredLang;
    }

    public function setPreferredLang(string $preferredLang): self
    {
        $this->preferredLang = $preferredLang;

        return $this;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    #[ORM\PrePersist]
    public function setCreatedAt(): void
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    #[ORM\PreUpdate]
    public function setUpdatedAt(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getLastLogin(): ?DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    //<editor-folder> Options

    public function getOptions(): ?array
    {
        return $this->options;
    }

    public function setOptions(array $options): self
    {
        $this->options = $options;

        return $this;
    }

    public function addOption(string $name, mixed $data = null): self
    {
        $this->validateOptionName($name);

        $this->options[$name] = $data;

        return $this;
    }

    public function removeOption(string $name): self
    {
        $this->validateOptionName($name);

        unset($this->options[$name]);

        return $this;
    }

    public function getOption(string $name): mixed
    {
        $this->validateOptionName($name);

        return $this->options[$name] ?? null;
    }

    public function getAvailableOptions(): array
    {
        return [
            self::OPTION_SKILL_AC,
            self::OPTION_SKILL_WT,
            self::OPTION_SKILL_WM,
            self::OPTION_SKILL_RETAIL,
        ];
    }

    private function validateOptionName(string $name): void
    {
        if (!in_array($name, $this->getAvailableOptions())) {
            throw new LogicException(sprintf('User option %s does not exist, or has not yet been implemented.', $name));
        }
    }

    public function hasOption($name): bool
    {
        $this->validateOptionName($name);

        return array_key_exists($name, $this->options);
    }

    //</editor-folder>

    public function hasRole($role): bool
    {
        return \in_array($role, $this->roles, true);
    }

    public function getPasswordResetToken(): ?string
    {
        return $this->passwordResetToken;
    }

    public function setPasswordResetToken(?string $passwordResetToken): User
    {
        $this->passwordResetToken = $passwordResetToken;

        return $this;
    }

    public function getLastPasswordReset(): ?\DateTimeImmutable
    {
        return $this->lastPasswordReset;
    }

    public function setLastPasswordReset(?\DateTimeImmutable $lastPasswordReset): User
    {
        $this->lastPasswordReset = $lastPasswordReset;

        return $this;
    }

    public function getProfilePictureFilename(): ?string
    {
        return $this->profilePictureFilename;
    }

    public function setProfilePictureFilename(?string $profilePictureFilename): User
    {
        $this->profilePictureFilename = $profilePictureFilename;

        return $this;
    }

    public function getPreferredInitialRoute(): string
    {
        return $this->preferredInitialRoute;
    }

    public function setPreferredInitialRoute(string $preferredInitialRoute): User
    {
        $this->preferredInitialRoute = $preferredInitialRoute;

        return $this;
    }

    public function getInformation(): ?string
    {
        return $this->information;
    }

    public function setInformation(?string $information): self
    {
        $this->information = $information;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): User
    {
        $this->client = $client;

        return $this;
    }

    public function getAsyncTasks(): Collection
    {
        return $this->asyncTasks;
    }

    public function getDeferredActionsTasks(): Collection
    {
        return $this->deferredActionsTasks;
    }

    public function getActivityLogs(): Collection
    {
        return $this->activityLogs;
    }

    public function getReferenceClient(): ?string
    {
        return $this->referenceClient;
    }

    public function setReferenceClient($referenceClient): User
    {
        $this->referenceClient = $referenceClient;

        return $this;
    }

    public function getPreferences(): array
    {
        return $this->preferences;
    }

    public function setPreferences(array $preferences): User
    {
        $this->preferences = $preferences;

        return $this;
    }

    public function getBounces(): ?Collection
    {
        return $this->bounces;
    }

    public function setBounces(?Collection $bounces): User
    {
        $this->bounces = $bounces;

        return $this;
    }

    public function addBounce(Bounce $bounce): User
    {
        $this->bounces->add($bounce);

        return $this;
    }

    public function removeBounce(Bounce $bounce): User
    {
        $this->bounces->removeElement($bounce);

        return $this;
    }

    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function setParticipants(Collection $participants): User
    {
        $this->participants = $participants;

        return $this;
    }

    public function addParticipant(ProviderParticipant $participant): User
    {
        $this->participants->add($participant);

        return $this;
    }

    public function removeParticipant(ProviderParticipant $participant): User
    {
        if ($this->participants->contains($participant)) {
            $this->participants->removeElement($participant);
        }

        return $this;
    }

    #[Groups(['read_user', 'read_session', 'read_grouped_activities', 'collection:vuejs', 'read_user_session_list'])]
    public function isAnAdmin(): bool
    {
        return in_array(self::ROLE_ADMIN, $this->roles);
    }

    #[Groups(['read_user', 'read_session', 'read_grouped_activities', 'collection:vuejs', 'read_user_session_list'])]
    public function isAManager(): bool
    {
        return in_array(self::ROLE_MANAGER, $this->roles);
    }

    #[Groups(['read_user', 'read_user_session_list'])]
    public function isACustomerService(): bool
    {
        $rolesCustomerService = [self::ROLE_CUSTOMER_SERVICE, self::ROLE_HOTLINE_CUSTOMER_SERVICE];

        return !empty(array_intersect($rolesCustomerService, $this->roles));
    }

    #[Groups([
        'read_grouped_activities', 'read_user_session_list', 'read_session', 'filter:view', HelperController::GROUP_VIEW,
        'help_need:read', 'ui:module:read', 'read_adobe_user', 'read_widget_news', 'ui:registration_page_template:read', 'read_users',
    ])]
    public function getFullName(): string
    {
        return $this->firstName.' '.$this->lastName;
    }

    public function getAdobeParticipants(): ?array
    {
        return $this->participants->filter(function ($participant) {
            return $participant instanceof AdobeConnectPrincipal;
        })->getValues();
    }

    public function getWebExParticipants(): ?array
    {
        return $this->participants->filter(function ($participant) {
            return $participant instanceof WebexParticipant;
        })->getValues();
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function generateToken(): self
    {
        $this->token = \md5(\random_bytes(32));

        return $this;
    }

    public function __toString(): string
    {
        return $this->getFullName();
    }

    #[Groups(['read_webex_user', 'read_user'])]
    public function isAWebexMeetingLicence(): bool
    {
        $webexConfigurationHolders = $this->client->getWebexConfigurationHolders();

        foreach ($webexConfigurationHolders as $webexConfigurationHolder) {
            if (in_array($this->email, $webexConfigurationHolder->getLicences())) {
                return true;
            }
        }

        return false;
    }

    #[Groups(['read_webex_rest_user', 'read_user'])]
    public function isAWebexRestMeetingLicence(): bool
    {
        $webexRestConfigurationHolders = $this->client->getWebexRestConfigurationHolders();

        foreach ($webexRestConfigurationHolders as $webexRestConfigurationHolder) {
            if (in_array($this->email, $webexRestConfigurationHolder->getLicences())) {
                return true;
            }
        }

        return false;
    }

    #[Groups(['read_microsoft_teams_user', 'read_user'])]
    public function isAMicrosoftTeamsLicence(): bool
    {
        $microsoftTeamsConfigurationHolders = $this->client->getMicrosoftTeamsConfigurationHolders();

        foreach ($microsoftTeamsConfigurationHolders as $microsoftTeamsConfigurationHolder) {
            if (array_key_exists($this->email, $microsoftTeamsConfigurationHolder->getLicences())) {
                return true;
            }
        }

        return false;
    }

    #[Groups(['read_microsoft_teams_event_user', 'read_user'])]
    public function isAMicrosoftTeamsEventLicence(): bool
    {
        $microsoftTeamsEventConfigurationHolders = $this->client->getMicrosoftTeamsEventConfigurationHolders();

        foreach ($microsoftTeamsEventConfigurationHolders as $microsoftTeamsEventConfigurationHolder) {
            if (array_key_exists($this->email, $microsoftTeamsEventConfigurationHolder->getLicences())) {
                return true;
            }
        }

        return false;
    }

    #[Groups(['read_user', 'read_adobe_user'])]
    public function isAnAdobeConnectLicence(): bool
    {
        $adobeConfigurationHolders = $this->client->getAdobeConfigurationHolders();

        /** @var AdobeConnectConfigurationHolder $adobeConfigurationHolder */
        foreach ($adobeConfigurationHolders as $adobeConfigurationHolder) {
            if (array_key_exists($this->email, $adobeConfigurationHolder->getLicences() ?? [])) {
                return true;
            }
        }

        return false;
    }

    #[Groups(['read_microsoft_teams_user', 'read_user'])]
    public function isAMicrosoftTeamsLicenceShared(): bool
    {
        $microsoftTeamsConfigurationHolders = $this->client->getMicrosoftTeamsConfigurationHolders();

        foreach ($microsoftTeamsConfigurationHolders as $microsoftTeamsConfigurationHolder) {
            if (array_key_exists($this->email, $microsoftTeamsConfigurationHolder->getLicences())) {
                if ($microsoftTeamsConfigurationHolder->getLicences()[$this->email]['shared']) {
                    return true;
                }
            }
        }

        return false;
    }

    #[Groups(['read_microsoft_teams_event_user', 'read_user'])]
    public function isAMicrosoftTeamsEventLicenceShared(): bool
    {
        $microsoftTeamsEventConfigurationHolders = $this->client->getMicrosoftTeamsEventConfigurationHolders();

        foreach ($microsoftTeamsEventConfigurationHolders as $microsoftTeamsEventConfigurationHolder) {
            if (array_key_exists($this->email, $microsoftTeamsEventConfigurationHolder->getLicences())) {
                if ($microsoftTeamsEventConfigurationHolder->getLicences()[$this->email]['shared']) {
                    return true;
                }
            }
        }

        return false;
    }

    #[Groups(['read_microsoft_teams_event_user', 'read_user'])]
    public function isAMicrosoftTeamsEventLicenceAttributed(): bool
    {
        $microsoftTeamsEventConfigurationHolders = $this->client->getMicrosoftTeamsEventConfigurationHolders();

        foreach ($microsoftTeamsEventConfigurationHolders as $microsoftTeamsEventConfigurationHolder) {
            $licences = $microsoftTeamsEventConfigurationHolder->getLicences();
            if (array_key_exists($this->email, $microsoftTeamsEventConfigurationHolder->getLicences())) {
                if (array_key_exists($this->email, $licences)) {
                    $licence = $licences[$this->email];
                    if (isset($licence['attributed']) && $licence['attributed']) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    #[Groups(['read_adobe_user', 'read_user'])]
    public function isAnAdobeConnectLicenceShared(): bool
    {
        $adobeConnectConfigurationHolders = $this->client->getAdobeConfigurationHolders();

        foreach ($adobeConnectConfigurationHolders as $adobeConnectConfigurationHolder) {
            if (array_key_exists($this->email, $adobeConnectConfigurationHolder->getLicences())) {
                if ($adobeConnectConfigurationHolder->getLicences()[$this->email]['shared']) {
                    return true;
                }
            }
        }

        return false;
    }

    // fqfqsfqfqsqsfsqf

    public function hasWebexMeetingLicence(): ?string
    {
        $webexConfigurationHolders = $this->client->getWebexConfigurationHolders();

        foreach ($webexConfigurationHolders as $webexConfigurationHolder) {
            if (in_array($this->email, $webexConfigurationHolder->getLicences())) {
                return $webexConfigurationHolder->getName();
            }
        }

        return null;
    }

    public function hasWebexRestMeetingLicence(): ?string
    {
        $webexRestConfigurationHolders = $this->client->getWebexRestConfigurationHolders();

        foreach ($webexRestConfigurationHolders as $webexRestConfigurationHolder) {
            if (in_array($this->email, $webexRestConfigurationHolder->getLicences())) {
                return $webexRestConfigurationHolder->getName();
            }
        }

        return null;
    }

    public function hasMicrosoftTeamsLicence(): ?string
    {
        $microsoftTeamsConfigurationHolders = $this->client->getMicrosoftTeamsConfigurationHolders();

        foreach ($microsoftTeamsConfigurationHolders as $microsoftTeamsConfigurationHolder) {
            if (array_key_exists($this->email, $microsoftTeamsConfigurationHolder->getLicences())) {
                return $microsoftTeamsConfigurationHolder->getName();
            }
        }

        return null;
    }

    public function hasMicrosoftTeamsEventLicence(): ?string
    {
        $microsoftTeamsEventConfigurationHolders = $this->client->getMicrosoftTeamsEventConfigurationHolders();

        foreach ($microsoftTeamsEventConfigurationHolders as $microsoftTeamsEventConfigurationHolder) {
            if (array_key_exists($this->email, $microsoftTeamsEventConfigurationHolder->getLicences())) {
                return $microsoftTeamsEventConfigurationHolder->getName();
            }
        }

        return null;
    }

    public function hasAdobeConnectLicence(): ?string
    {
        $adobeConfigurationHolders = $this->client->getAdobeConfigurationHolders();

        /** @var AdobeConnectConfigurationHolder $adobeConfigurationHolder */
        foreach ($adobeConfigurationHolders as $adobeConfigurationHolder) {
            if (array_key_exists($this->email, $adobeConfigurationHolder->getLicences() ?? [])) {
                return $adobeConfigurationHolder->getName();
            }
        }

        return null;
    }

    public function hasMicrosoftTeamsLicenceShared(): ?string
    {
        $microsoftTeamsConfigurationHolders = $this->client->getMicrosoftTeamsConfigurationHolders();

        /** @var MicrosoftTeamsConfigurationHolder $microsoftTeamsConfigurationHolder */
        foreach ($microsoftTeamsConfigurationHolders as $microsoftTeamsConfigurationHolder) {
            if (array_key_exists($this->email, $microsoftTeamsConfigurationHolder->getLicences())) {
                if ($microsoftTeamsConfigurationHolder->getLicences()[$this->email]['shared']) {
                    return $microsoftTeamsConfigurationHolder->getName();
                }
            }
        }

        return null;
    }

    public function hasMicrosoftTeamsEventLicenceShared(): ?string
    {
        $microsoftTeamsEventConfigurationHolders = $this->client->getMicrosoftTeamsEventConfigurationHolders();

        foreach ($microsoftTeamsEventConfigurationHolders as $microsoftTeamsEventConfigurationHolder) {
            if (array_key_exists($this->email, $microsoftTeamsEventConfigurationHolder->getLicences())) {
                if ($microsoftTeamsEventConfigurationHolder->getLicences()[$this->email]['shared']) {
                    return $microsoftTeamsEventConfigurationHolder->getName();
                }
            }
        }

        return null;
    }

    public function hasMicrosoftTeamsEventLicenceAttributed(): bool
    {
        $microsoftTeamsEventConfigurationHolders = $this->client->getMicrosoftTeamsEventConfigurationHolders();

        foreach ($microsoftTeamsEventConfigurationHolders as $microsoftTeamsEventConfigurationHolder) {
            $licences = $microsoftTeamsEventConfigurationHolder->getLicences();
            if (array_key_exists($this->email, $microsoftTeamsEventConfigurationHolder->getLicences())) {
                if (array_key_exists($this->email, $licences)) {
                    $licence = $licences[$this->email];
                    if (isset($licence['attributed']) && $licence['attributed']) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function hasAdobeConnectLicenceShared(): ?string
    {
        $adobeConnectConfigurationHolders = $this->client->getAdobeConfigurationHolders();

        foreach ($adobeConnectConfigurationHolders as $adobeConnectConfigurationHolder) {
            if (array_key_exists($this->email, $adobeConnectConfigurationHolder->getLicences())) {
                if ($adobeConnectConfigurationHolder->getLicences()[$this->email]['shared']) {
                    return $adobeConnectConfigurationHolder->getName();
                }
            }
        }

        return null;
    }

    #[Groups(['read_user'])]
    public function hasLicenceShared(): array
    {
        return array_filter(array_merge(
            (array) $this->hasMicrosoftTeamsLicenceShared(),
            (array) $this->hasMicrosoftTeamsEventLicenceShared(),
            (array) $this->hasAdobeConnectLicenceShared()
        ));
    }

    #[Groups(['read_user'])]
    public function hasLicenceAttributed(): array
    {
        return array_filter(array_merge(
            (array) $this->hasMicrosoftTeamsEventLicenceAttributed()
        ));
    }

    #[Groups(['read_user'])]
    public function hasLicence(): array
    {
        return array_filter(array_merge(
            (array) $this->hasWebexMeetingLicence(),
            (array) $this->hasWebexRestMeetingLicence(),
            !$this->hasMicrosoftTeamsLicenceShared() ? (array) $this->hasMicrosoftTeamsLicence() : [],
            !$this->hasMicrosoftTeamsEventLicenceShared() ? (array) $this->hasMicrosoftTeamsEventLicence() : [],
            !$this->hasAdobeConnectLicenceShared() ? (array) $this->hasAdobeConnectLicence() : []
        ));
    }

    /**
     * Return default true.
     */
    public function isSubscribeToAlert(string $typeAlert): bool
    {
        return !array_key_exists($typeAlert, $this->preferences) || $this->preferences[$typeAlert];
    }

    #[Groups([UserApiController::GROUP_VIEW_SUBSCRIPTIONS])]
    #[OA\Property(
        property: 'getSessions',
        type: 'array',
        items: new OA\Items(
            ref: new Model(type: ProviderSession::class)
        )
    )]
    public function getSessions(): array
    {
        return array_merge(...array_map(static function (ProviderParticipant $participant) {
            return $participant->getSessionRoles()->toArray();
        }, $this->participants->toArray()
        ));
    }

    public function getHelpTasks(): ArrayCollection|Collection
    {
        return $this->helpTasks;
    }

    public function setHelpTasks(ArrayCollection|Collection $helpTasks): self
    {
        $this->helpTasks = $helpTasks;

        return $this;
    }

    public function getAvailabilityUsers(): ArrayCollection|Collection
    {
        return $this->availabilityUsers;
    }

    public function setAvailabilityUsers(ArrayCollection|Collection $availabilityUsers): self
    {
        $this->availabilityUsers = $availabilityUsers;

        return $this;
    }

    public function getWidgetNews(): Collection
    {
        return $this->widgetNews;
    }

    public function setWidgetNews(Collection $widgetNews): self
    {
        $this->widgetNews = $widgetNews;

        return $this;
    }

    #[Groups(['ui:user:hub'])]
    public function getIsGuestUser(): bool
    {
        return $this->id == self::ID_USER_GUEST;
    }

    #[Groups(HelperController::GROUP_VIEW)]
    public function getAcronym(): string
    {
        // Use mb_substr() with UTF-8 encoding to get the first characters.
        return strtoupper(mb_substr($this->getFirstName(), 0, 1, 'UTF-8').mb_substr($this->getLastName(), 0, 2, 'UTF-8'));
    }

    public function getVideosReplaysViewers(): Collection
    {
        return $this->videosReplaysViewers;
    }

    public function setVideosReplaysViewers(Collection $videosReplaysViewers): User
    {
        $this->videosReplaysViewers = $videosReplaysViewers;

        return $this;
    }

    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function setGroups(Collection $groups): User
    {
        $this->groups = $groups;

        return $this;
    }

    public function addGroup(GroupParticipant $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
        }

        return $this;
    }

    public function removeGroup(GroupParticipant $group): self
    {
        if ($this->groups->removeElement($group)) {
            $group->removeUser($this); // S'assure que l'utilisateur est également retiré du groupe
        }

        return $this;
    }

    public function getRegistrationFormCatalogResponses(): ?array
    {
        return $this->registrationFormCatalogResponses;
    }

    public function setRegistrationFormCatalogResponses(?array $registrationFormCatalogResponses): User
    {
        $this->registrationFormCatalogResponses = $registrationFormCatalogResponses;

        return $this;
    }

    public function getUserModules(): Collection
    {
        return $this->userModules;
    }

    public function setUserModules(Collection $userModules): User
    {
        $this->userModules = $userModules;

        return $this;
    }
}
