<?php

namespace App\Entity\Microsoft;

use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Model\GeneratorTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class MicrosoftTeamsTelephonyProfile extends AbstractProviderAudio
{
    use GeneratorTrait;

    // FIXME: Remove this. It's redundant with the declaration on AbstractProviderAudio
    #[ORM\ManyToOne(targetEntity: MicrosoftTeamsSession::class, inversedBy: 'telephonyProfiles')]
    private ?MicrosoftTeamsSession $linkedPrincipal;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_audio'])]
    private ?string $microsoftTeamsDefaultPhoneNumber = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_audio'])]
    private ?string $codeAnimator = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_audio'])]
    private ?string $codeParticipant = null;

    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(['read_session', 'read_audio'])]
    private array $audioConferencing = [];

    public function __construct()
    {
        parent::__construct();

        // TODO: Refactor some stuff to match with AbstractProviderAudio
        $this->name = $this->randomString();
    }

    public function getLinkedPrincipal(): ?MicrosoftTeamsSession
    {
        return $this->linkedPrincipal;
    }

    public function setLinkedPrincipal(?MicrosoftTeamsSession $linkedPrincipal): MicrosoftTeamsTelephonyProfile
    {
        $this->linkedPrincipal = $linkedPrincipal;

        return $this;
    }

    public static function getAvailableConnectionType(): array
    {
        return [
            self::AUDIO_CALLIN,
            self::AUDIO_VOIP,
        ];
    }

    public function getMicrosoftTeamsDefaultPhoneNumber(): ?string
    {
        return $this->microsoftTeamsDefaultPhoneNumber;
    }

    public function setMicrosoftTeamsDefaultPhoneNumber(?string $microsoftTeamsDefaultPhoneNumber): MicrosoftTeamsTelephonyProfile
    {
        $this->microsoftTeamsDefaultPhoneNumber = $microsoftTeamsDefaultPhoneNumber;

        return $this;
    }

    public function getCodeAnimator(): ?string
    {
        return $this->codeAnimator;
    }

    public function setCodeAnimator(?string $codeAnimator): MicrosoftTeamsTelephonyProfile
    {
        $this->codeAnimator = $codeAnimator;

        return $this;
    }

    public function getCodeParticipant(): ?string
    {
        return $this->codeParticipant;
    }

    public function setCodeParticipant(?string $codeParticipant): MicrosoftTeamsTelephonyProfile
    {
        $this->codeParticipant = $codeParticipant;

        return $this;
    }

    public function getAudioConferencing(): array
    {
        return $this->audioConferencing;
    }

    public function setAudioConferencing(array $audioConferencing): self
    {
        $this->audioConferencing = $audioConferencing;

        return $this;
    }
}
