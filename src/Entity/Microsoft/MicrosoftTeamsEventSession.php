<?php

namespace App\Entity\Microsoft;

use App\Controller\Api\ProviderSessionApiController;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Service\MiddlewareService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity]
class MicrosoftTeamsEventSession extends ProviderSession
{
    protected static string $provider = MiddlewareService::SERVICE_TYPE_MICROSOFT_TEAMS_EVENT;

    #[ORM\Column(type: 'text')]
    private string $joinUrl;

    #[ORM\Column(type: 'text')]
    #[NotBlank(groups: ['update'])]
    private string $eventIdentifier;

    #[ORM\Column(type: 'text')]
    #[NotBlank(groups: ['update'])]
    private string $onlineMeetingIdentifier;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session'])]
    #[NotNull(groups: ['create', 'update', ProviderSessionApiController::GROUP_CREATE])]
    protected ?string $licence = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session'])]
    #[NotNull(groups: [ProviderSessionApiController::GROUP_CREATE])]
    protected ?string $userLicence = null;

    #[ORM\ManyToMany(targetEntity: MicrosoftTeamsParticipant::class, mappedBy: 'meetings')]
    protected Collection $participants;

    #[ORM\OneToMany(mappedBy: 'linkedPrincipal', targetEntity: MicrosoftTeamsEventTelephonyProfile::class)]
    #[Groups(['read_session'])]
    protected Collection $telephonyProfiles;

    protected array $candidates = [];

    public function __construct(AbstractProviderConfigurationHolder $configurationHolder)
    {
        parent::__construct($configurationHolder);
        $this->telephonyProfiles = new ArrayCollection();
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof MicrosoftTeamsEventConfigurationHolder;
    }

    #[Groups(['read_session'])]
    public function getJoinUrl(): string
    {
        return $this->joinUrl;
    }

    public function setJoinUrl(string $joinUrl): MicrosoftTeamsEventSession
    {
        $this->joinUrl = $joinUrl;

        return $this;
    }

    public function getEventIdentifier(): string
    {
        return $this->eventIdentifier;
    }

    public function setEventIdentifier(string $eventIdentifier): MicrosoftTeamsEventSession
    {
        $this->eventIdentifier = $eventIdentifier;

        return $this;
    }

    public function getOnlineMeetingIdentifier(): string
    {
        return $this->onlineMeetingIdentifier;
    }

    public function setOnlineMeetingIdentifier(string $onlineMeetingIdentifier): MicrosoftTeamsEventSession
    {
        $this->onlineMeetingIdentifier = $onlineMeetingIdentifier;

        return $this;
    }

    public function getLicence(): ?string
    {
        return $this->licence;
    }

    public function setLicence(?string $licence): self
    {
        $this->licence = $licence;

        return $this;
    }

    public function getUserLicence(): ?string
    {
        return $this->userLicence;
    }

    public function setUserLicence(?string $userLicence): self
    {
        $this->userLicence = $userLicence;

        return $this;
    }

    public function getLicenceUser(): ?User
    {
        return $this->getLicenceParticipantRole()?->getParticipant()->getUser();
    }

    public function getAnimatorChoiceLicence(): ?string
    {
        $licenceUsed = $this->getLicence();
        $userLicence = $this->getUserLicence();

        /** @var MicrosoftTeamsEventConfigurationHolder $configurationHolder */
        $configurationHolder = $this->getAbstractProviderConfigurationHolder();

        foreach ($configurationHolder->getLicences() as $keyLicence => $arrayValues) {
            if (!empty($userLicence)) {
                if ($licenceUsed === $userLicence) {
                    return $licenceUsed;
                } else {
                    return $userLicence;
                }
            } else {
                return $licenceUsed;
            }
        }

        return null;
    }

    public function getLicenceParticipantRole(): ?ProviderParticipantSessionRole
    {
        /** @var ProviderParticipantSessionRole $animatorParticipantRole */
        foreach ($this->getAnimatorsOnly() as $animatorParticipantRole) {
            if ($animatorParticipantRole->getParticipant()->getUser()->isAMicrosoftTeamsLicence() && $animatorParticipantRole->getParticipant()->getUser()->getEmail() == $this->getLicence()) {
                return $animatorParticipantRole;
            }
        }

        return null;
    }

    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function setParticipants(Collection $participants): MicrosoftTeamsEventSession
    {
        $this->participants = $participants;

        return $this;
    }

    public function getProviderDistantID(): ?string
    {
        return $this->eventIdentifier;
    }

    public function getCandidates(): array
    {
        return $this->candidates;
    }

    public function setCandidates(array $candidates): MicrosoftTeamsEventSession
    {
        $this->candidates = $candidates;

        return $this;
    }

    public function getTelephonyProfiles(): ArrayCollection|Collection
    {
        return $this->telephonyProfiles;
    }

    public function setTelephonyProfiles(ArrayCollection|Collection $telephonyProfiles): MicrosoftTeamsEventSession
    {
        $this->telephonyProfiles = $telephonyProfiles;

        return $this;
    }

    public function addTelephonyProfile(MicrosoftTeamsEventTelephonyProfile $microsoftTeamsEventTelephonyProfile): MicrosoftTeamsEventSession
    {
        if (!$this->telephonyProfiles->contains($microsoftTeamsEventTelephonyProfile)) {
            $this->telephonyProfiles->add($microsoftTeamsEventTelephonyProfile);
            $microsoftTeamsEventTelephonyProfile->setLinkedPrincipal($this);
        }

        return $this;
    }

    public function removeTelephonyProfile(MicrosoftTeamsEventTelephonyProfile $microsoftTeamsEventTelephonyProfile): MicrosoftTeamsEventSession
    {
        if ($this->telephonyProfiles->contains($microsoftTeamsEventTelephonyProfile)) {
            $this->telephonyProfiles->removeElement($microsoftTeamsEventTelephonyProfile);
            $microsoftTeamsEventTelephonyProfile->setLinkedPrincipal(null);
        }

        return $this;
    }
}
