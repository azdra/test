<?php

namespace App\Entity\Microsoft;

use App\Controller\Api\ProviderSessionApiController;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Service\MiddlewareService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

#[ORM\Entity]
class MicrosoftTeamsSession extends ProviderSession
{
    protected static string $provider = MiddlewareService::SERVICE_TYPE_MICROSOFT_TEAMS;

    #[ORM\Column(type: 'text')]
    private string $joinWebUrl;

    #[ORM\Column(type: 'text')]
    #[NotBlank(groups: ['update'])]
    private string $meetingIdentifier;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session'])]
    #[NotNull(groups: ['create', 'update', ProviderSessionApiController::GROUP_CREATE])]
    protected ?string $licence = null;

    #[ORM\ManyToMany(targetEntity: MicrosoftTeamsParticipant::class, mappedBy: 'meetings')]
    protected Collection $participants;

    #[ORM\OneToMany(mappedBy: 'linkedPrincipal', targetEntity: MicrosoftTeamsTelephonyProfile::class)]
    #[Groups(['read_session'])]
    protected Collection $telephonyProfiles;

    protected array $candidates = [];

    public function __construct(AbstractProviderConfigurationHolder $configurationHolder)
    {
        parent::__construct($configurationHolder);
        $this->telephonyProfiles = new ArrayCollection();
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof MicrosoftTeamsConfigurationHolder;
    }

    #[Groups(['read_session'])]
    public function getJoinWebUrl(): string
    {
        return $this->joinWebUrl;
    }

    public function setJoinWebUrl(string $joinWebUrl): MicrosoftTeamsSession
    {
        $this->joinWebUrl = $joinWebUrl;

        return $this;
    }

    public function getMeetingIdentifier(): string
    {
        return $this->meetingIdentifier;
    }

    public function setMeetingIdentifier(string $meetingIdentifier): MicrosoftTeamsSession
    {
        $this->meetingIdentifier = $meetingIdentifier;

        return $this;
    }

    public function getLicence(): ?string
    {
        return $this->licence;
    }

    public function setLicence(?string $licence): self
    {
        $this->licence = $licence;

        return $this;
    }

    public function getLicenceUser(): ?User
    {
        return $this->getLicenceParticipantRole()?->getParticipant()->getUser();
    }

    public function getLicenceParticipantRole(): ?ProviderParticipantSessionRole
    {
        /** @var ProviderParticipantSessionRole $animatorParticipantRole */
        foreach ($this->getAnimatorsOnly() as $animatorParticipantRole) {
            if ($animatorParticipantRole->getParticipant()->getUser()->isAMicrosoftTeamsLicence() && $animatorParticipantRole->getParticipant()->getUser()->getEmail() == $this->getLicence()) {
                return $animatorParticipantRole;
            }
        }

        return null;
    }

    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function setParticipants(Collection $participants): MicrosoftTeamsSession
    {
        $this->participants = $participants;

        return $this;
    }

    public function getProviderDistantID(): ?string
    {
        return $this->meetingIdentifier;
    }

    public function getCandidates(): array
    {
        return $this->candidates;
    }

    public function setCandidates(array $candidates): MicrosoftTeamsSession
    {
        $this->candidates = $candidates;

        return $this;
    }

    public function getTelephonyProfiles(): ArrayCollection|Collection
    {
        return $this->telephonyProfiles;
    }

    public function setTelephonyProfiles(ArrayCollection|Collection $telephonyProfiles): MicrosoftTeamsSession
    {
        $this->telephonyProfiles = $telephonyProfiles;

        return $this;
    }

    public function addTelephonyProfile(MicrosoftTeamsTelephonyProfile $microsoftTeamsTelephonyProfile): MicrosoftTeamsSession
    {
        if (!$this->telephonyProfiles->contains($microsoftTeamsTelephonyProfile)) {
            $this->telephonyProfiles->add($microsoftTeamsTelephonyProfile);
            $microsoftTeamsTelephonyProfile->setLinkedPrincipal($this);
        }

        return $this;
    }

    public function removeTelephonyProfile(MicrosoftTeamsTelephonyProfile $microsoftTeamsTelephonyProfile): MicrosoftTeamsSession
    {
        if ($this->telephonyProfiles->contains($microsoftTeamsTelephonyProfile)) {
            $this->telephonyProfiles->removeElement($microsoftTeamsTelephonyProfile);
            $microsoftTeamsTelephonyProfile->setLinkedPrincipal(null);
        }

        return $this;
    }
}
