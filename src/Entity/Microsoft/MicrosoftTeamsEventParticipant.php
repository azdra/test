<?php

namespace App\Entity\Microsoft;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Service\MiddlewareService;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;

#[ORM\Entity]
class MicrosoftTeamsEventParticipant extends ProviderParticipant
{
    protected static string $provider = MiddlewareService::SERVICE_TYPE_MICROSOFT_TEAMS_EVENT;

    public const ROLE_ATTENDEE = 'attendee';

    public const ROLE_PRESENTER = 'presenter';

    public const ROLE_PRODUCER = 'producer';

    public const ROLE_UNKNOWN_FUTURE_VALUE = 'unknownFutureValue';

    #[ORM\Column(type: 'integer', nullable: true)]
    protected ?int $contactIdentifier = null;

    #[ORM\ManyToMany(targetEntity: MicrosoftTeamsEventSession::class, inversedBy: 'participants')]
    #[Ignore]
    protected Collection $meetings;

    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    #[Groups(['read_microsoft_teams_event_user'])]
    protected bool $active = true;

    /**
     * Unmapped. Used to create a participant. We don't store user password.
     */
    protected string $password = '';

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof MicrosoftTeamsEventConfigurationHolder;
    }

    #[Groups(['read_session'])]
    public function getName(): string
    {
        return $this->getUser()->getFirstName().' '.$this->getUser()->getLastName();
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): MicrosoftTeamsEventParticipant
    {
        $this->password = $password;

        return $this;
    }

    public function getContactIdentifier(): ?int
    {
        return $this->contactIdentifier;
    }

    public function setContactIdentifier(?int $contactIdentifier): MicrosoftTeamsEventParticipant
    {
        $this->contactIdentifier = $contactIdentifier;

        return $this;
    }

    public function getMeetings(): Collection
    {
        return $this->meetings;
    }

    public function setMeetings(Collection $meetings): MicrosoftTeamsEventParticipant
    {
        $this->meetings = $meetings;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): MicrosoftTeamsEventParticipant
    {
        $this->active = $active;

        return $this;
    }

    public function getRole(): string
    {
        return is_null($this->contactIdentifier)
            ? self::ROLE_ATTENDEE
            : self::ROLE_PRESENTER;
    }
}
