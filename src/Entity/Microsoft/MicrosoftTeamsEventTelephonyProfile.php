<?php

namespace App\Entity\Microsoft;

use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Model\GeneratorTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class MicrosoftTeamsEventTelephonyProfile extends AbstractProviderAudio
{
    use GeneratorTrait;

    // FIXME: Remove this. It's redundant with the declaration on AbstractProviderAudio
    #[ORM\ManyToOne(targetEntity: MicrosoftTeamsEventSession::class, inversedBy: 'telephonyProfiles')]
    private ?MicrosoftTeamsEventSession $linkedPrincipal;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_audio'])]
    private ?string $microsoftTeamsEventDefaultPhoneNumber = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_audio'])]
    private ?string $codeAnimator = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_audio'])]
    private ?string $codeParticipant = null;

    #[ORM\Column(type: 'json', nullable: true)]
    #[Groups(['read_session', 'read_audio'])]
    private array $audioConferencing = [];

    public function __construct()
    {
        parent::__construct();

        // TODO: Refactor some stuff to match with AbstractProviderAudio
        $this->name = $this->randomString();
    }

    public function getLinkedPrincipal(): ?MicrosoftTeamsEventSession
    {
        return $this->linkedPrincipal;
    }

    public function setLinkedPrincipal(?MicrosoftTeamsEventSession $linkedPrincipal): MicrosoftTeamsEventTelephonyProfile
    {
        $this->linkedPrincipal = $linkedPrincipal;

        return $this;
    }

    public static function getAvailableConnectionType(): array
    {
        return [
            self::AUDIO_CALLIN,
            self::AUDIO_VOIP,
        ];
    }

    public function getMicrosoftTeamsEventDefaultPhoneNumber(): ?string
    {
        return $this->microsoftTeamsEventDefaultPhoneNumber;
    }

    public function setMicrosoftTeamsEventDefaultPhoneNumber(?string $microsoftTeamsEventDefaultPhoneNumber): MicrosoftTeamsEventTelephonyProfile
    {
        $this->microsoftTeamsEventDefaultPhoneNumber = $microsoftTeamsEventDefaultPhoneNumber;

        return $this;
    }

    public function getCodeAnimator(): ?string
    {
        return $this->codeAnimator;
    }

    public function setCodeAnimator(?string $codeAnimator): MicrosoftTeamsEventTelephonyProfile
    {
        $this->codeAnimator = $codeAnimator;

        return $this;
    }

    public function getCodeParticipant(): ?string
    {
        return $this->codeParticipant;
    }

    public function setCodeParticipant(?string $codeParticipant): MicrosoftTeamsEventTelephonyProfile
    {
        $this->codeParticipant = $codeParticipant;

        return $this;
    }

    public function getAudioConferencing(): array
    {
        return $this->audioConferencing;
    }

    public function setAudioConferencing(array $audioConferencing): self
    {
        $this->audioConferencing = $audioConferencing;

        return $this;
    }
}
