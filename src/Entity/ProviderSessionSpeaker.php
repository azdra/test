<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity()]
#[UniqueEntity(fields: ['name', 'provider_session'], message: 'This speaker already exists with this session', errorPath: 'name')]
class ProviderSessionSpeaker
{
    public const AUTHORIZED_EXTENSIONS = ['jpg', 'jpeg', 'png'];
    public const MAX_FILE_SIZE = 20000000;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'ui:registration_page_template:read'])]
    protected int $id;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Groups(['read_session', 'ui:registration_page_template:read'])]
    private string $name;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['read_session', 'ui:registration_page_template:read'])]
    private ?string $information = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'ui:registration_page_template:read'])]
    private ?string $photo = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['read_session', 'ui:registration_page_template:read'])]
    private ?string $title = null;

    #[ORM\ManyToOne(targetEntity: ProviderSession::class, inversedBy: 'speakers')]
    #[ORM\JoinColumn(name: 'provider_session_id', referencedColumnName: 'id')]
    private ProviderSession $providerSession;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): ProviderSessionSpeaker
    {
        $this->id = $id;

        return $this;
    }

    public function setName(string $name): ProviderSessionSpeaker
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setInformation(?string $information): ProviderSessionSpeaker
    {
        $this->information = $information;

        return $this;
    }

    public function getInformation(): ?string
    {
        return $this->information;
    }

    public function setPhoto(?string $photo): ProviderSessionSpeaker
    {
        $this->photo = $photo;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function getProviderSession(): ProviderSession
    {
        return $this->providerSession;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function setProviderSession(ProviderSession $providerSession): self
    {
        $this->providerSession = $providerSession;

        return $this;
    }
}
