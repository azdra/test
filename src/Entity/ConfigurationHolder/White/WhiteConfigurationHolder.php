<?php

namespace App\Entity\ConfigurationHolder\White;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Service\MiddlewareService;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class WhiteConfigurationHolder extends AbstractProviderConfigurationHolder
{
    protected static string $provider = MiddlewareService::SERVICE_TYPE_WHITE;
    protected static string $providerApiUrl = 'https://white.connect.com/v1.0';

    #[ORM\Column]
    #[Groups(['read_configuration_holder'])]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private int $safetyTime = 0;

    public function isValid(): bool
    {
        return true;
    }

    public function getSafetyTime(): int
    {
        return $this->safetyTime;
    }

    public function setSafetyTime(int $safetyTime): self
    {
        $this->safetyTime = $safetyTime;

        return $this;
    }
}
