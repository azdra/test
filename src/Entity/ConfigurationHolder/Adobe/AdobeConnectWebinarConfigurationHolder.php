<?php

namespace App\Entity\ConfigurationHolder\Adobe;

use App\Controller\Api\ClientApiController;
use App\Controller\Api\ProviderSessionApiController;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Service\MiddlewareService;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

#[ORM\Entity]
class AdobeConnectWebinarConfigurationHolder extends AbstractProviderConfigurationHolder
{
    public const LICENCES_CLIENT_SIMULTANEOUS = 1;
    public const LICENCES_CLIENT_NAMED = 2;

    public const API_DOMAIN_SUFFIX = '.adobeconnect.com';

    protected static string $provider = MiddlewareService::SERVICE_TYPE_ADOBE_CONNECT_WEBINAR;
    protected static string $providerApiUrl = 'https://'.self::PLACEHOLDER_DOMAIN.'/api/xml';

    #[ORM\Column(type: 'string')]
    #[NotBlank]
    #[Regex(pattern: '/^([a-z0-9]+)([_-]*)([a-z0-9]+)$/')]
    #[Groups(['read_configuration_holder', ClientApiController::GROUP_VIEW, 'read_adobe_user'])]
    private string $subdomain = '';

    #[ORM\Column]
    #[Choice(choices: [
        AdobeConnectWebinarSCO::PERMISSION_VERB_DENIED,
        AdobeConnectWebinarSCO::PERMISSION_VERB_REMOVE,
        AdobeConnectWebinarSCO::PERMISSION_VERB_VIEW_HIDDEN,
    ])]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private string $defaultConnexionType = AdobeConnectWebinarSCO::PERMISSION_VERB_VIEW_HIDDEN;

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private \DateTime $birthday;

    #[ORM\Column]
    #[Choice(choices: [
        self::LICENCES_CLIENT_SIMULTANEOUS,
        self::LICENCES_CLIENT_NAMED,
    ])]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private int $licenceType = self::LICENCES_CLIENT_SIMULTANEOUS;

    #[ORM\Column(type: 'bigint', nullable: true)]
    #[NotBlank(allowNull: true)]
    #[Groups(['read_configuration_holder'])]
    private ?int $defaultRoom = null;

    #[ORM\Column]
    #[Groups(['read_configuration_holder'])]
    private bool $standardViewOnOpeningRoom = true;

    #[ORM\Column(nullable: true)]
    #[NotBlank(allowNull: true)]
    #[Groups(['read_adobe_user', 'read_configuration_holder', 'read_session'])]
    private ?array $licences = null;

    #[ORM\Column(nullable: true)]
    #[NotBlank(allowNull: true)]
    #[Groups(['read_configuration_holder'])]
    private ?array $adminGroupScoIds = null;

    #[ORM\Column(type: 'bigint', nullable: true)]
    #[NotBlank(allowNull: true)]
    private ?int $hostGroupScoId = null;

    #[ORM\Column(type: 'bigint', nullable: true)]
    #[NotBlank(allowNull: true)]
    #[Groups(['read_configuration_holder'])]
    private ?int $usernameScoId = null;

    #[ORM\Column(type: 'bigint')]
    #[NotBlank]
    private int $savingMeetingFolderScoId = 0;

    #[ORM\Column]
    #[Groups(['read_configuration_holder', 'read_session'])]
    private bool $automaticLicensing = false;

    #[ORM\Column(type: 'bigint')]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private int $maxParticipantsWebinar = 999;

    public function __construct()
    {
        $this->birthday = new \DateTime();
    }

    public function isValid(): bool
    {
        return !empty($this->subdomain) && !empty($this->username) && !empty($this->password);
    }

    public function getSubdomain(): string
    {
        return $this->subdomain;
    }

    public function setSubdomain(string $subdomain): self
    {
        $this->subdomain = $subdomain;
        $this->domain = $subdomain.self::API_DOMAIN_SUFFIX;

        return $this;
    }

    #[Groups([ProviderSessionApiController::GROUP_VIEW])]
    public function getBaseUrl(): string
    {
        return $this->subdomain.self::API_DOMAIN_SUFFIX;
    }

    public function getDefaultConnexionType(): string
    {
        return $this->defaultConnexionType;
    }

    public function setDefaultConnexionType(string $defaultConnexionType): self
    {
        $this->defaultConnexionType = $defaultConnexionType;

        return $this;
    }

    public function getBirthday(): \DateTime
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTime $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getLicenceType(): int
    {
        return $this->licenceType;
    }

    public function setLicenceType(int $licenceType): self
    {
        $this->licenceType = $licenceType;

        return $this;
    }

    public function getDefaultRoom(): ?int
    {
        return $this->defaultRoom;
    }

    public function setDefaultRoom(?int $defaultRoom): self
    {
        $this->defaultRoom = $defaultRoom;

        return $this;
    }

    public function isStandardViewOnOpeningRoom(): bool
    {
        return $this->standardViewOnOpeningRoom;
    }

    public function setStandardViewOnOpeningRoom(bool $standardViewOnOpeningRoom): self
    {
        $this->standardViewOnOpeningRoom = $standardViewOnOpeningRoom;

        return $this;
    }

    public function getLicences(): ?array
    {
        return $this->encryptDecryptLicencePassword($this->licences, 'decrypt');
    }

    public function getLicencesShared(): ?array
    {
        $licences = $this->encryptDecryptLicencePassword($this->licences, 'decrypt');
        foreach ($licences as $email => $data) {
            if (!$data['shared']) {
                unset($licences[$email]);
            }
        }

        return $licences;
    }

    public function setLicences(?array $licences): self
    {
        $this->licences = $this->encryptDecryptLicencePassword($licences);

        return $this;
    }

    public function addLicence(string $email, array $paramslicence): self
    {
        if ($this->licences === null) {
            $this->licences = [];
        }

        if (!array_key_exists($email, $this->licences)) {
            $this->licences[$email] = $this->encryptDecryptSingleLicencePassword($paramslicence);
        }

        return $this;
    }

    public function removeLicence(string $email): self
    {
        if (array_key_exists($email, $this->licences)) {
            unset($this->licences[$email]);
        }

        return $this;
    }

    public function getAdminGroupScoIds(): ?array
    {
        return $this->adminGroupScoIds;
    }

    public function setAdminGroupScoIds(?array $adminGroupScoIds): self
    {
        $this->adminGroupScoIds = $adminGroupScoIds;

        return $this;
    }

    public function addAdminGroupScoId(string $scoid): self
    {
        if ($this->adminGroupScoIds === null) {
            $this->adminGroupScoIds = [];
        }

        if (!array_key_exists($scoid, $this->adminGroupScoIds)) {
            $this->adminGroupScoIds[] = $scoid;
        }

        return $this;
    }

    public function removeAdminGroupScoId(string $scoid): self
    {
        if (array_key_exists($scoid, $this->adminGroupScoIds)) {
            unset($this->adminGroupScoIds[$scoid]);
        }

        return $this;
    }

    public function getHostGroupScoId(): ?int
    {
        return $this->hostGroupScoId;
    }

    public function setHostGroupScoId(?int $hostGroupScoId): self
    {
        $this->hostGroupScoId = $hostGroupScoId;

        return $this;
    }

    public function getUsernameScoId(): ?int
    {
        return $this->usernameScoId;
    }

    public function setUsernameScoId(?int $usernameScoId): self
    {
        $this->usernameScoId = $usernameScoId;

        return $this;
    }

    public function getSavingMeetingFolderScoId(): int
    {
        return $this->savingMeetingFolderScoId;
    }

    public function setSavingMeetingFolderScoId(int $savingMeetingFolderScoId): self
    {
        $this->savingMeetingFolderScoId = $savingMeetingFolderScoId;

        return $this;
    }

    public function isAutomaticLicensing(): bool
    {
        return $this->automaticLicensing;
    }

    public function setAutomaticLicensing(bool $automaticLicensing): self
    {
        $this->automaticLicensing = $automaticLicensing;

        return $this;
    }

    public function getMaxParticipantsWebinar(): int
    {
        return $this->maxParticipantsWebinar;
    }

    public function setMaxParticipantsWebinar(int $maxParticipantsWebinar): self
    {
        $this->maxParticipantsWebinar = $maxParticipantsWebinar;

        return $this;
    }

    private function encryptDecryptSingleLicencePassword(array $licence, string $method = 'encrypt'): array
    {
        $cipher = 'aes-128-gcm';
        $ivLength = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivLength);

        if ($method == 'encrypt') {
            $ciphertext = openssl_encrypt($licence['password'], $cipher, $this->id, $options = 0, $iv, $tags);
            $licence['password'] = base64_encode($ciphertext);
            $licence['tag'] = base64_encode($tags);
            $licence['iv'] = base64_encode($iv);
        } else {
            $output = openssl_decrypt(base64_decode($licence['password']), $cipher, $this->id, $options = 0, base64_decode($licence['iv']), base64_decode($licence['tag']));
            $licence['password'] = $output;
        }

        return $licence;
    }

    private function encryptDecryptLicencePassword(?array $licences, string $method = 'encrypt'): array
    {
        if ($licences === null) {
            return [];
        }

        $cipher = 'aes-128-gcm';
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);

        foreach ($licences as $key => $values) {
            if ($method == 'encrypt') {
                $ciphertext = openssl_encrypt($values['password'], $cipher, $this->getId(), $options = 0, $iv, $tags);
                $licences[$key]['password'] = base64_encode($ciphertext);
                $licences[$key]['tag'] = base64_encode($tags);
                $licences[$key]['iv'] = base64_encode($iv);
            } else {
                $output = openssl_decrypt(base64_decode($values['password']), $cipher, $this->getId(), $options = 0, base64_decode($values['iv']), base64_decode($values['tag']));
                $licences[$key]['password'] = $output;
            }
        }

        return $licences;
    }
}
