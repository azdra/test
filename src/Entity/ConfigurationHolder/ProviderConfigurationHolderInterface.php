<?php

namespace App\Entity\ConfigurationHolder;

use App\Entity\Client\Client;

interface ProviderConfigurationHolderInterface
{
    public function getApiUrl(): string;

    public function getDomain(): ?string;

    public function getUsername(): ?string;

    public function getPassword(): ?string;

    public function getClient(): Client;

    public function isValid(): bool;
}
