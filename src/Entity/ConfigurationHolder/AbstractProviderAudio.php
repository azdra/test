<?php

namespace App\Entity\ConfigurationHolder;

use App\Controller\Api\ProviderSessionApiController;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectTelephonyProfile;
use App\Entity\Adobe\AdobeConnectWebinarTelephonyProfile;
use App\Entity\Cisco\WebexMeetingTelephonyProfile;
use App\Entity\Cisco\WebexRestMeetingTelephonyProfile;
use App\Entity\Microsoft\MicrosoftTeamsEventTelephonyProfile;
use App\Entity\Microsoft\MicrosoftTeamsTelephonyProfile;
use App\Entity\ProviderSession;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Choice;

#[ORM\Entity]
#[ORM\InheritanceType('JOINED')]
#[ORM\HasLifecycleCallbacks]
#[ORM\DiscriminatorColumn(name: 'discr', type: 'string')]
#[ORM\DiscriminatorMap(['adobe_connect' => AdobeConnectTelephonyProfile::class, 'adobe_connect_webinar' => AdobeConnectWebinarTelephonyProfile::class, 'cisco_webex' => WebexMeetingTelephonyProfile::class, 'cisco_webex_rest' => WebexRestMeetingTelephonyProfile::class, 'microsoft_teams' => MicrosoftTeamsTelephonyProfile::class, 'microsoft_teams_event' => MicrosoftTeamsEventTelephonyProfile::class])]
#[UniqueEntity('profileIdentifier')]
abstract class AbstractProviderAudio
{
    public const AUDIO_INTERCALL = 'InterCall';
    public const AUDIO_PGI = 'PGi NA';
    public const AUDIO_VOIP = 'VOIP only';
    public const AUDIO_CALLIN = 'CALLIN';
    public const AUDIO_PGI_EMEA = 'PGi EMEA';
    public const AUDIO_MEETINGONE_EMEA = 'MeetingOne EMEA';
    public const AUDIO_MEETINGONE_NA = 'MeetingOne Amérique du Nord';

    public const AUDIO_CATEGORY_PHONE = 'Telephone';
    public const AUDIO_CATEGORY_COMPUTER = 'Computer';
    public const AUDIO_CATEGORY_HYBRIDE = 'Hybrid';
    public const AUDIO_CATEGORY_PRESENTIAL = 'Presential';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_audio', ProviderSessionApiController::GROUP_VIEW])]
    protected int $id;

    #[ORM\Column(type: 'bigint', nullable: true)]
    #[Groups(['read_session', 'read_audio', ProviderSessionApiController::GROUP_VIEW])]
    protected ?int $profileIdentifier = null;

    #[Choice(choices: [
            self::AUDIO_INTERCALL,
            self::AUDIO_PGI,
            self::AUDIO_VOIP,
            self::AUDIO_CALLIN,
            self::AUDIO_PGI_EMEA,
            self::AUDIO_MEETINGONE_EMEA,
            self::AUDIO_MEETINGONE_NA,
        ], groups: ['create', 'update']
    )]
    #[ORM\Column(type: 'string')]
    #[Groups(['read_session', 'read_audio'])]
    protected ?string $connectionType = self::AUDIO_INTERCALL;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_session', 'read_audio'])]
    protected string $name;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_session', 'read_audio'])]
    protected ?string $phoneNumber = null;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_audio'])]
    protected string $phoneLang = 'fr';

    // FIXME: Rename this with a ganaric name
    #[ORM\OneToMany(mappedBy: 'providerAudio', targetEntity: ProviderSession::class)]
    protected Collection $linkedSCOs;

    #[ORM\ManyToOne(targetEntity: AbstractProviderConfigurationHolder::class, inversedBy: 'audioProviders')]
    private AbstractProviderConfigurationHolder $configurationHolder;

    public function __construct()
    {
        $this->linkedSCOs = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getProfileIdentifier(): ?int
    {
        return $this->profileIdentifier;
    }

    public function setProfileIdentifier(?int $profileIdentifier): AbstractProviderAudio
    {
        $this->profileIdentifier = $profileIdentifier;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): AbstractProviderAudio
    {
        $this->name = $name;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): AbstractProviderAudio
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getPhoneLang(): string
    {
        return $this->phoneLang;
    }

    public function setPhoneLang(string $phoneLang): AbstractProviderAudio
    {
        $this->phoneLang = $phoneLang;

        return $this;
    }

    public function getConnectionType(): ?string
    {
        return $this->connectionType;
    }

    public function setConnectionType(?string $connectionType): AbstractProviderAudio
    {
        if (!in_array($connectionType, static::getAvailableConnectionType())) {
            throw new \LogicException(sprintf('SCO Connection Type %s does not exist, or has not yet been implemented.', $connectionType));
        }

        $this->connectionType = $connectionType;

        return $this;
    }

    public function getLinkedSCOs(): ArrayCollection|Collection
    {
        return $this->linkedSCOs;
    }

    public function setLinkedSCOs(ArrayCollection|Collection $linkedSCOs): AbstractProviderAudio
    {
        $this->linkedSCOs = $linkedSCOs;

        return $this;
    }

    public function addLinkedSCO(AdobeConnectSCO $adobeConnectSCO): AbstractProviderAudio
    {
        if (!$this->linkedSCOs->contains($adobeConnectSCO)) {
            $this->linkedSCOs->add($adobeConnectSCO);
        }

        return $this;
    }

    public function getConfigurationHolder(): AbstractProviderConfigurationHolder
    {
        return $this->configurationHolder;
    }

    public function setConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): AbstractProviderAudio
    {
        $this->configurationHolder = $configurationHolder;

        return $this;
    }

    abstract public static function getAvailableConnectionType(): array;
}
