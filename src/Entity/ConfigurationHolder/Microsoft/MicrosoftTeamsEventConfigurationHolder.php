<?php

namespace App\Entity\ConfigurationHolder\Microsoft;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Service\MiddlewareService;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotBlank;

#[ORM\Entity]
class MicrosoftTeamsEventConfigurationHolder extends AbstractProviderConfigurationHolder
{
    protected static string $provider = MiddlewareService::SERVICE_TYPE_MICROSOFT_TEAMS_EVENT;
    protected static string $providerApiUrl = 'https://graph.microsoft.com/v1.0';

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private string $tokenEndpoint = '';

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private string $tenantId = '';

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private string $scope = 'https://graph.microsoft.com/.default';

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private string $grantType = 'client_credentials';

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private string $organizerEmail = '';

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private string $organizerUniqueIdentifier = '';

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_configuration_holder', 'read_microsoft_teams_event_user'])]
    private string $clientId = '';

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private string $clientSecret = '';

    #[ORM\Column]
    #[Groups(['read_configuration_holder'])]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private int $safetyTime = 0;

    #[ORM\Column(type: 'json')]
    #[Groups(['read_session', 'read_microsoft_teams_event_user', 'read_client'])]
    private ?array $licences = [];

    #[ORM\Column]
    #[Groups(['read_configuration_holder', 'read_session'])]
    private bool $automaticLicensing = true;

    public function isValid(): bool
    {
        return true;
    }

    public function getTokenEndpoint(): string
    {
        return $this->tokenEndpoint;
    }

    public function setTokenEndpoint(string $tokenEndpoint): self
    {
        $this->tokenEndpoint = $tokenEndpoint;

        return $this;
    }

    public function getTenantId(): string
    {
        return $this->tenantId;
    }

    public function setTenantId(string $tenantId): self
    {
        $this->tenantId = $tenantId;

        return $this;
    }

    public function getScope(): string
    {
        return $this->scope;
    }

    public function setScope(string $scope): self
    {
        $this->scope = $scope;

        return $this;
    }

    public function getGrantType(): string
    {
        return $this->grantType;
    }

    public function setGrantType(string $grantType): self
    {
        $this->grantType = $grantType;

        return $this;
    }

    public function getOrganizerEmail(): string
    {
        return $this->organizerEmail;
    }

    public function setOrganizerEmail(string $organizerEmail): self
    {
        $this->organizerEmail = $organizerEmail;

        return $this;
    }

    public function getOrganizerUniqueIdentifier(): string
    {
        return $this->organizerUniqueIdentifier;
    }

    public function setOrganizerUniqueIdentifier(string $organizerUniqueIdentifier): self
    {
        $this->organizerUniqueIdentifier = $organizerUniqueIdentifier;

        return $this;
    }

    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function setClientId(string $clientId): self
    {
        $this->clientId = $clientId;

        return $this;
    }

    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    public function setClientSecret(string $clientSecret): self
    {
        $this->clientSecret = $clientSecret;

        return $this;
    }

    public function getSafetyTime(): int
    {
        return $this->safetyTime;
    }

    public function setSafetyTime(int $safetyTime): self
    {
        $this->safetyTime = $safetyTime;

        return $this;
    }

    public function getLicences(): ?array
    {
        return $this->encryptDecryptLicencePassword($this->licences, 'decrypt');
    }

    public function getLicencesShared(): ?array
    {
        $licences = $this->encryptDecryptLicencePassword($this->licences, 'decrypt');
        foreach ($licences as $email => $data) {
            if (!$data['shared']) {
                unset($licences[$email]);
            }
        }

        return $licences;
    }

    public function getLicencesAttributed(): ?array
    {
        $licences = $this->encryptDecryptLicencePassword($this->licences, 'decrypt');
        foreach ($licences as $email => $data) {
            if (!$data['attributed']) {
                unset($licences[$email]);
            }
        }

        return $licences;
    }

    public function setLicences(?array $licences): self
    {
        $this->licences = $this->encryptDecryptLicencePassword($licences);

        return $this;
    }

    public function addLicence(string $email, array $paramslicence): self
    {
        if (!array_key_exists($email, $this->licences)) {
            $this->licences[$email] = $paramslicence;
            $this->encryptDecryptLicencePassword($this->licences[$email]);
        }

        return $this;
    }

    public function removeLicence(string $email): self
    {
        if (array_key_exists($email, $this->licences)) {
            unset($this->licences[$email]);
        }

        return $this;
    }

    public function isAutomaticLicensing(): bool
    {
        return $this->automaticLicensing;
    }

    public function setAutomaticLicensing(bool $automaticLicensing): self
    {
        $this->automaticLicensing = $automaticLicensing;

        return $this;
    }

    private function encryptDecryptLicencePassword(?array $licences, string $method = 'encrypt'): array
    {
        $cipher = 'aes-128-gcm';
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);

        foreach ($licences as $key => $values) {
            if ($method == 'encrypt') {
                $ciphertext = openssl_encrypt($values['password'], $cipher, $this->tenantId, $options = 0, $iv, $tags);
                $ciphertextAttributed = openssl_encrypt($values['passwordAttributed'], $cipher, $this->tenantId, $options = 0, $iv, $tags);
                $licences[$key]['password'] = base64_encode($ciphertext);
                $licences[$key]['passwordAttributed'] = base64_encode($ciphertextAttributed);
                $licences[$key]['tag'] = base64_encode($tags);
                $licences[$key]['iv'] = base64_encode($iv);
            } else {
                $output = openssl_decrypt(base64_decode($values['password']), $cipher, $this->tenantId, $options = 0, base64_decode($values['iv']), base64_decode($values['tag']));
                $licences[$key]['password'] = $output;
                $outputAttributed = openssl_decrypt(base64_decode($values['passwordAttributed']), $cipher, $this->tenantId, $options = 0, base64_decode($values['iv']), base64_decode($values['tag']));
                $licences[$key]['passwordAttributed'] = $outputAttributed;
            }
        }

        return $licences;
    }
}
