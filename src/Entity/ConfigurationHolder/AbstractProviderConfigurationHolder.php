<?php

namespace App\Entity\ConfigurationHolder;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use App\Controller\Api\ClientApiController;
use App\Controller\Api\ProviderSessionApiController;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\DiscriminatorMap;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;

#[ORM\Entity]
#[ORM\InheritanceType('JOINED')]
#[ORM\DiscriminatorColumn(name: 'discr', type: 'string')]
#[ORM\DiscriminatorMap([
    'adobe_connect' => AdobeConnectConfigurationHolder::class,
    'adobe_connect_webinar' => AdobeConnectWebinarConfigurationHolder::class,
    'cisco_webex' => WebexConfigurationHolder::class,
    'cisco_webex_rest' => WebexRestConfigurationHolder::class,
    'microsoft_teams' => MicrosoftTeamsConfigurationHolder::class,
    'microsoft_teams_event' => MicrosoftTeamsEventConfigurationHolder::class,
    'white_connector' => WhiteConfigurationHolder::class,
])]
#[DiscriminatorMap(typeProperty: 'type', mapping: [
    'adobe_connect' => AdobeConnectConfigurationHolder::class,
    'adobe_connect_webinar' => AdobeConnectWebinarConfigurationHolder::class,
    'cisco_webex' => WebexConfigurationHolder::class,
    'cisco_webex_rest' => WebexRestConfigurationHolder::class,
    'microsoft_teams' => MicrosoftTeamsConfigurationHolder::class,
    'microsoft_teams_event' => MicrosoftTeamsEventConfigurationHolder::class,
    'white_connector' => WhiteConfigurationHolder::class,
])]
#[UniqueEntity('name')]
abstract class AbstractProviderConfigurationHolder implements ProviderConfigurationHolderInterface
{
    public const PLACEHOLDER_DOMAIN = '__domain__';

    protected static string $provider = 'none';
    protected static string $providerApiUrl = 'none';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups([
        'read_session', 'read_configuration_holder', 'read_session_list', 'read_webex_user', 'read_webex_rest_user', ProviderSessionApiController::GROUP_VIEW,
        'read_microsoft_teams_user', 'read_microsoft_teams_event_user', 'read_white_connector_user', ClientApiController::GROUP_VIEW, 'read_client', 'session_wizard', 'read_adobe_user',
    ])]
    protected int $id;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups([
        'read_configuration_holder', ClientApiController::GROUP_VIEW, 'read_session_list', 'read_session', 'read_user',
        'read_client', 'session_wizard', 'read_adobe_user', 'read_webex_user', 'read_webex_rest_user', 'read_microsoft_teams_user', 'read_microsoft_teams_event_user', 'read_white_connector_user',
    ])]
    private ?string $name = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[NotBlank(allowNull: true)]
    #[Groups([
        'read_configuration_holder', 'read_webex_user', 'read_webex_rest_user', 'read_microsoft_teams_user', 'read_microsoft_teams_event_user', ClientApiController::GROUP_VIEW,
        'session_wizard', 'read_adobe_user', 'read_white_connector_user',
    ])]
    protected ?string $domain = null;

    #[ORM\Column(nullable: true)]
    #[NotBlank]
    #[Groups(['read_configuration_holder', ClientApiController::GROUP_VIEW, 'session_wizard'])]
    protected ?string $username = null;

    #[ORM\Column(nullable: true)]
    #[NotBlank]
    #[Groups(['read_configuration_holder', 'session_wizard'])]
    #[Encrypted]
    protected ?string $password = null;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Groups(['read_configuration_holder', ClientApiController::GROUP_VIEW, 'read_session_list', 'session_wizard', 'read_client'])]
    private bool $archived = false;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'configurationHolders')]
    #[Groups(['read_session', 'read_webex_user', 'read_webex_rest_user', 'read_microsoft_teams_user', 'read_microsoft_teams_event_user', 'read_adobe_user', 'read_white_connector_user'])]
    protected Client $client;

    #[ORM\Column(nullable: true)]
    #[Groups(['read_configuration_holder', 'session_wizard'])]
    protected ?int $defaultConvocation = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['read_configuration_holder', 'session_wizard'])]
    protected ?int $defaultConvocationEnglish = null;

    #[ORM\OneToMany(mappedBy: 'configurationHolder', targetEntity: AbstractProviderAudio::class)]
    private Collection $audioProviders;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read_configuration_holder'])]
    private int $percentageAttendance = 70;

    #[Groups(['read_configuration_holder', 'read_session'])]
    public static function getProvider(): string
    {
        return static::$provider;
    }

    abstract public function isValid(): bool;

    public function getApiUrl(): string
    {
        return (is_null($this->domain))
            ? static::$providerApiUrl
            : str_replace(static::PLACEHOLDER_DOMAIN, $this->domain, static::$providerApiUrl);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(?string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getAudioProviders(): Collection
    {
        return $this->audioProviders;
    }

    public function setAudioProviders(Collection $audioProviders): self
    {
        $this->audioProviders = $audioProviders;

        return $this;
    }

    public function addAudioProvider(AbstractProviderAudio $providerAudio): self
    {
        if (!$this->audioProviders->contains($providerAudio)) {
            $providerAudio->setConfigurationHolder($this);
            $this->audioProviders->add($providerAudio);
        }

        return $this;
    }

    public function getDefaultConvocation(): ?int
    {
        return $this->defaultConvocation;
    }

    public function setDefaultConvocation(?int $defaultConvocation): self
    {
        $this->defaultConvocation = $defaultConvocation;

        return $this;
    }

    public function isArchived(): bool
    {
        return $this->archived;
    }

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function getDefaultConvocationEnglish(): ?int
    {
        return $this->defaultConvocationEnglish;
    }

    public function setDefaultConvocationEnglish(?int $defaultConvocationEnglish): self
    {
        $this->defaultConvocationEnglish = $defaultConvocationEnglish;

        return $this;
    }

    public function getPercentageAttendance(): int
    {
        return $this->percentageAttendance;
    }

    public function setPercentageAttendance(int $percentageAttendance): AbstractProviderConfigurationHolder
    {
        $this->percentageAttendance = $percentageAttendance;

        return $this;
    }
}
