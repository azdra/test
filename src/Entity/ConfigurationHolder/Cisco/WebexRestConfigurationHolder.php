<?php

namespace App\Entity\ConfigurationHolder\Cisco;

use App\Controller\Api\ClientApiController;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Service\MiddlewareService;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotBlank;

#[ORM\Entity]
class WebexRestConfigurationHolder extends AbstractProviderConfigurationHolder
{
    protected static string $provider = MiddlewareService::SERVICE_TYPE_CISCO_WEBEX_REST;
    protected static string $providerApiUrl = 'https://api.webex.com/WBXService/XMLService';

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_webex_rest_user', 'read_configuration_holder', ClientApiController::GROUP_VIEW])]
    private string $siteName = '';

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private string $clientId = '';

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private string $clientSecret = '';

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private string $clientRefreshToken = '';

    #[ORM\Column]
    #[NotBlank]
    #[Groups(['read_configuration_holder'])]
    private string $clientAccessToken = '';

    #[ORM\Column(type: 'json')]
    private ?array $defaultOptions = [];

    #[ORM\Column(type: 'json')]
    #[Groups(['read_session', 'read_webex_rest_user', 'read_client'])]
    private ?array $licences = [];

    #[ORM\Column]
    #[Groups(['read_configuration_holder', 'read_session'])]
    private bool $automaticLicensing = false;

    #[ORM\Column]
    #[Groups(['read_configuration_holder'])]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private int $safetyTime = 0;

    #[ORM\Column(type: 'datetime', nullable: true, options: ['default' => null])]
    #[Groups(['read_configuration_holder'])]
    private ?\DateTime $expireAtToken = null;

    public function isValid(): bool
    {
        return !empty($this->username) && !empty($this->password) && !empty($this->siteName);
    }

    public function getSiteName(): string
    {
        return $this->siteName;
    }

    public function setSiteName(string $siteName): self
    {
        $this->siteName = $siteName;

        return $this;
    }

    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function setClientId(string $clientId): self
    {
        $this->clientId = $clientId;

        return $this;
    }

    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    public function setClientSecret(string $clientSecret): self
    {
        $this->clientSecret = $clientSecret;

        return $this;
    }

    public function getClientRefreshToken(): string
    {
        return $this->clientRefreshToken;
    }

    public function setClientRefreshToken(string $clientRefreshToken): self
    {
        $this->clientRefreshToken = $clientRefreshToken;

        return $this;
    }

    public function getClientAccessToken(): string
    {
        return $this->clientAccessToken;
    }

    public function setClientAccessToken(string $clientAccessToken): self
    {
        $this->clientAccessToken = $clientAccessToken;

        return $this;
    }

    public function getDefaultOptions(): ?array
    {
        return $this->defaultOptions;
    }

    public function setDefaultOptions(array $defaultOptions = []): self
    {
        $this->defaultOptions = $defaultOptions;

        return $this;
    }

    public function getLicences(): ?array
    {
        return $this->licences;
    }

    public function setLicences(?array $licences): self
    {
        $this->licences = $licences;

        return $this;
    }

    public function addLicence(string $licence): self
    {
        if (!in_array($licence, $this->licences)) {
            array_push($this->licences, $licence);
        }

        return $this;
    }

    public function removeLicence(string $licence): self
    {
        if (in_array($licence, $this->licences)) {
            $pos = array_search($licence, $this->licences);
            unset($this->licences[$pos]);
        }

        return $this;
    }

    public function isAutomaticLicensing(): bool
    {
        return $this->automaticLicensing;
    }

    public function setAutomaticLicensing(bool $automaticLicensing): self
    {
        $this->automaticLicensing = $automaticLicensing;

        return $this;
    }

    public function getSafetyTime(): int
    {
        return $this->safetyTime;
    }

    public function setSafetyTime(int $safetyTime): self
    {
        $this->safetyTime = $safetyTime;

        return $this;
    }

    #[Groups(['read_configuration_holder'])]
    public function getOptionJoinTeleconfBeforeHost(): bool
    {
        return $this->defaultOptions['optionJoinTeleconfBeforeHost'] ?? false;
    }

    #[Groups(['read_configuration_holder'])]
    public function getOptionOpenTime(): int
    {
        return $this->defaultOptions['optionOpenTime'] ?? 30;
    }

    public function getExpireAtToken(): ?\DateTime
    {
        return $this->expireAtToken;
    }

    public function setExpireAtToken(?\DateTime $expireAtToken): void
    {
        $this->expireAtToken = $expireAtToken;
    }
}
