<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class VideoReplayViewer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['provider_session_replay'])]
    private int $id;

    #[ORM\Column(type: 'integer')]
    #[Groups(['provider_session_replay'])]
    private int $seen = 0;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['provider_session_replay'])]
    private ?\DateTimeImmutable $lastShowReplay = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'videosReplaysViewers')]
    protected ?User $user;

    #[ORM\ManyToOne(targetEntity: VideoReplay::class, inversedBy: 'videosReplaysViewers')]
    protected VideoReplay $videoReplay;

    public function __construct()
    {
        $this->seen = 1;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getSeen(): int
    {
        return $this->seen;
    }

    public function setSeen(int $seen): self
    {
        $this->seen = $seen;

        return $this;
    }

    public function incrementSeen(): void
    {
        ++$this->seen;
    }

    public function getLastShowReplay(): ?\DateTimeImmutable
    {
        return $this->lastShowReplay;
    }

    public function setLastShowReplay(?\DateTimeImmutable $lastShowReplay): self
    {
        $this->lastShowReplay = $lastShowReplay;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getVideoReplay(): VideoReplay
    {
        return $this->videoReplay;
    }

    public function setVideoReplay(VideoReplay $videoReplay): self
    {
        $this->videoReplay = $videoReplay;

        return $this;
    }
}
