<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class Bounce
{
    public const ALIAS = 'bounce';

    public const TO_CORRECT = 1; // Bounce à traiter (changement email)
    public const CORRECTED = 2;  // Bounce traité (changement email)
    public const IGNORED = 3;    // Bounce ignoré

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_bounce', 'read_bounce_session_list'])]
    private int $id;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_bounce'])]
    private \DateTimeInterface $createdAt;

    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    private bool $active = true;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_bounce'])]
    private \DateTimeInterface $dateSend;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_bounce'])]
    private \DateTimeInterface $dateStatus;

    #[ORM\Column(type: 'string', nullable: true)]
    #[
        Assert\Email(),
        Assert\NotBlank(allowNull: true),
    ]
    #[Groups(['read_bounce', 'read_participant'])]
    private ?string $email = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[
        Assert\Length(min: 0, max: 255)
    ]
    #[Groups(['read_bounce'])]
    private ?string $subject = null;

    #[ORM\Column()]
    #[
        Assert\Choice(choices: [self::TO_CORRECT, self::CORRECTED, self::IGNORED], )
    ]
    #[Groups(['read_bounce'])]
    private int $state = self::TO_CORRECT;

    #[ORM\ManyToOne(targetEntity: ProviderSession::class, cascade: ['persist'], inversedBy: 'bounces')]
    #[ORM\JoinColumn(name: 'session_id', referencedColumnName: 'id', nullable: true)]
    #[Groups(['read_bounce', 'read_participant', 'read_bounce_session_list'])]
    private ?ProviderSession $session = null;

    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['persist'], inversedBy: 'bounces')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    #[Groups(['read_bounce', 'read_participant'])]
    private User $user;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_bounce'])]
    private ?string $reason = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_bounce'])]
    private ?string $mandrillEvent = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['read_bounce'])]
    private ?string $mailOrigin = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Bounce
    {
        $this->id = $id;

        return $this;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): Bounce
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): Bounce
    {
        $this->active = $active;

        return $this;
    }

    public function getDateSend(): \DateTimeInterface
    {
        return $this->dateSend;
    }

    public function setDateSend(\DateTimeInterface $dateSend): Bounce
    {
        $this->dateSend = $dateSend;

        return $this;
    }

    public function getDateStatus(): \DateTimeInterface
    {
        return $this->dateStatus;
    }

    public function setDateStatus(\DateTimeInterface $dateStatus): Bounce
    {
        $this->dateStatus = $dateStatus;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): Bounce
    {
        $this->email = $email;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): Bounce
    {
        $this->subject = $subject;

        return $this;
    }

    public function getState(): int
    {
        return $this->state;
    }

    public function setState(int $state): Bounce
    {
        if (!in_array($state, [self::TO_CORRECT, self::CORRECTED, self::IGNORED])) {
            throw new \LogicException('State Bounce is not valid');
        }

        $this->state = $state;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): Bounce
    {
        $this->user = $user;

        return $this;
    }

    public function getSession(): ?ProviderSession
    {
        return $this->session;
    }

    public function setSession(?ProviderSession $session): Bounce
    {
        $this->session = $session;

        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setReason(?string $reason): Bounce
    {
        $this->reason = $reason;

        return $this;
    }

    public function getMandrillEvent(): ?string
    {
        return $this->mandrillEvent;
    }

    public function setMandrillEvent(?string $mandrillEvent): Bounce
    {
        $this->mandrillEvent = $mandrillEvent;

        return $this;
    }

    public function getMailOrigin(): ?string
    {
        return $this->mailOrigin;
    }

    public function setMailOrigin(?string $mailOrigin): Bounce
    {
        $this->mailOrigin = $mailOrigin;

        return $this;
    }

    #[Groups(['read_bounce'])]
    public function getReadableState(): string
    {
        return match ($this->state) {
            self::TO_CORRECT => 'To be corrected',
            self::CORRECTED => 'Treated',
            self::IGNORED => 'Ignored',
        };
    }
}
