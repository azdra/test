<?php

namespace App\Entity;

    use App\Controller\Api\ProviderSessionApiController;
    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Serializer\Annotation\Groups;

    #[ORM\Embeddable]
    class ProviderSessionAccessToken
    {
        #[ORM\Column(unique: true)]
        #[Groups(['read_lobby_signature', ProviderSessionApiController::GROUP_VIEW_PRESENCES, 'read_session'])]
        private string $token;

        #[ORM\Column]
        private string $tag;

        #[ORM\Column]
        private string $iv;

        public function getToken(): string
        {
            return $this->token;
        }

        public function setToken(string $token): self
        {
            $this->token = $token;

            return $this;
        }

        public function getTag(): string
        {
            return $this->tag;
        }

        public function setTag(string $tag): self
        {
            $this->tag = $tag;

            return $this;
        }

        public function getIv(): string
        {
            return $this->iv;
        }

        public function setIv(string $iv): self
        {
            $this->iv = $iv;

            return $this;
        }
    }
