<?php

namespace App\Serializer;

use App\Entity\User;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserNormalizer implements NormalizerInterface
{
    public function __construct(
        protected ObjectNormalizer $normalizer,
        private TranslatorInterface $translator
    ) {
    }

    public function normalize($object, string $format = null, array $context = []): mixed
    {
        /** @var User $object */
        $data = $this->normalizer->normalize($object, $format, $context);

        if (isset($context['groups']) && $context['groups'] === 'read_user') {
            $data['transform'] = [
                'roles_readable' => array_map(fn ($role) => match ($role) {
                    User::ROLE_TRAINEE => $this->translator->trans('Participant'),
                    User::ROLE_ADMIN => $this->translator->trans('Super Admin'),
                    User::ROLE_ADMIN_MANAGER => $this->translator->trans('Account admin'),
                    User::ROLE_MANAGER => $this->translator->trans('Account manager'),
                    User::ROLE_CUSTOMER_SERVICE => $this->translator->trans('Account customer service'),
                    User::ROLE_API => $this->translator->trans('Account API user'),
                    default => ''
                }, $object->getRoles()),
            ];
        }

        return $data;
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof User;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [
            User::class => true,
        ];
    }
}
