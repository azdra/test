<?php

namespace App\Serializer;

use App\Entity\Convocation;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ConvocationNormalizer implements NormalizerInterface
{
    public function __construct(protected ObjectNormalizer $normalizer)
    {
    }

    public function normalize($object, string $format = null, array $context = []): ?array
    {
        $data = $this->normalizer->normalize($object, $format, $context);

        return is_array($data) ? $data : $data->getArrayCopy();
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof Convocation;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [
            Convocation::class => true,
        ];
    }
}
