<?php

namespace App\Serializer;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ProviderSession;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ProviderSessionNormalizer implements NormalizerInterface, DenormalizerInterface
{
    public function __construct(
        protected ObjectNormalizer $normalizer,
        private readonly string $registrationPageTemplateLogoUri,
    ) {
    }

    public function normalize($object, string $format = null, array $context = []): mixed
    {
        /** @var ProviderSession $object */
        $data = $this->normalizer->normalize($object, $format, $context);

        if (array_key_exists('speakers', $data)) {
            foreach ($data['speakers'] as $key => $speaker) {
                $data['speakers'][$key]['photoUri'] = $speaker['photo'] ? $this->registrationPageTemplateLogoUri.$speaker['photo'] : null;
            }
        }

        ksort($data);

        return $data;
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof ProviderSession;
    }

    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): mixed
    {
        $context[AbstractNormalizer::DEFAULT_CONSTRUCTOR_ARGUMENTS] = [
            AdobeConnectSCO::class => ['configurationHolder' => new AdobeConnectConfigurationHolder()],
            WebexMeeting::class => ['configurationHolder' => new WebexConfigurationHolder()],
        ];

        return $this->normalizer->denormalize($data, $type, $format, $context);
    }

    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return in_array($type, [AdobeConnectSCO::class, WebexMeeting::class, ProviderSession::class]);
    }

    private function hasGroup(string $targetGroup, array $context): bool
    {
        if (isset($context['groups'])) {
            if (is_array($context['groups'])) {
                return in_array($targetGroup, $context['groups']);
            } elseif (is_string($context['groups'])) {
                return $context['groups'] === $targetGroup;
            }
        }

        return false;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [
            ProviderSession::class => true,
            AdobeConnectSCO::class => true,
            WebexMeeting::class => true,
        ];
    }
}
