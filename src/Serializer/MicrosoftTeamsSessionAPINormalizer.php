<?php

namespace App\Serializer;

use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\White\WhiteParticipant;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class MicrosoftTeamsSessionAPINormalizer implements NormalizerInterface
{
    public function __construct(
        protected ObjectNormalizer $normalizer
    ) {
    }

    public function normalize($object, string $format = null, array $context = []): mixed
    {
        if (!empty($context['groups'])) {
            if ($context['groups'] === 'microsoft_teams_session') {
                $data = [
                    'subject' => $object->getName(),
                    'startDateTime' => $object->getDateStart()->format('c'),
                    'endDateTime' => $object->getDateEnd()->format('c'),
                    'lobbyBypassSettings' => [
                        'scope' => 'everyone',
                        'isDialInBypassEnabled' => true,
                    ],
                ];
            } else {
                /** @var ProviderSession $object */
                $data = $this->normalizer->normalize($object, $format, $context);
            }
        } else {
            /** @var ProviderSession $object */
            $data = $this->normalizer->normalize($object, $format, $context);
        }

        if (\array_key_exists('include_participants', $context) && $context['include_participants']) {
            $data['allowedPresenters'] = 'roleIsPresenter';
            $providerParticipantsSessionRole = $object->getParticipantRoles();
        }

        /* @var ProviderParticipantSessionRole $providerParticipantSessionRole */
        if (!empty($providerParticipantsSessionRole)) {
            foreach ($providerParticipantsSessionRole as $ppsr) {
                if ($ppsr->getRole() !== ProviderParticipantSessionRole::ROLE_REMOVE) {
                    $participant = $ppsr->getParticipant();
                    $data['participants']['attendees'][] = [
                        'upn' => $participant->getEmail(),
                        'role' => WhiteParticipant::ROLE_ATTENDEE,
                        'identity' => [
                            'user' => [
                                'displayName' => $participant->getFirstName().' '.$participant->getLastName(),
                                'id' => '',
                            ],
                        ],
                    ];
                }
            }
        } else {
            $data['participants']['attendees'] = [];
        }

        return $data;
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof MicrosoftTeamsSession;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [
            MicrosoftTeamsSession::class => true,
        ];
    }
}
