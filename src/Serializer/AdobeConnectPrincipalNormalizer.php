<?php

namespace App\Serializer;

use App\Entity\Adobe\AdobeConnectPrincipal;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class AdobeConnectPrincipalNormalizer implements NormalizerInterface
{
    private ObjectNormalizer $normalizer;

    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function normalize($object, string $format = null, array $context = []): ?array
    {
        $data = $this->normalizer->normalize($object, $format, $context);

        return is_array($data) ? $data : $data->getArrayCopy();
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof AdobeConnectPrincipal;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [
            AdobeConnectPrincipal::class => true,
        ];
    }
}
