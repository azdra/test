<?php

namespace App\Serializer;

use App\Entity\Client\Client;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ClientNormalizer implements NormalizerInterface
{
    public function __construct(protected ObjectNormalizer $normalizer)
    {
    }

    public function normalize($object, string $format = null, array $context = []): ?array
    {
        $data = $this->normalizer->normalize($object, $format, $context);

        return is_array($data) ? $data : $data->getArrayCopy();
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof Client;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [
            Client::class => true,
        ];
    }
}
