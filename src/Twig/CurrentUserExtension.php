<?php

namespace App\Twig;

use App\Entity\User;
use App\Repository\UserRepository;
use Redis;
use Symfony\Component\Serializer\SerializerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CurrentUserExtension extends AbstractExtension
{
    public const REDIS_TIMEOUT_IN_SECONDS = 3600;

    public function __construct(
        private Redis $redisClient,
        private SerializerInterface $serializer,
        private UserRepository $userRepository
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('userDisplayName', [$this, 'userDisplayName']),
            new TwigFunction('userAvailableAccounts', [$this, 'userAvailableAccounts']),
        ];
    }

    public function userDisplayName(User $user): string
    {
        return $user->getFirstName() === null && $user->getLastName() === null ?
            $user->getEmail() :
            $user->getFirstName().' '.$user->getLastName();
    }

    public function userAvailableAccounts(User $user): Redis|false|string
    {
        $redisKey = User::REDIS_USER_ACCOUNTS_AVAILABLE_KEY.$user->getEmail();

        if ($this->redisClient->exists($redisKey)) {
            $response = $this->redisClient->get($redisKey);
        } else {
            $response = $this->serializer->serialize(
                $this->userRepository->findAllUsersManagerAccounts($user),
                'json',
                ['groups' => ['switch_user']]
            );

            $this->redisClient->setex(
                $redisKey,
                self::REDIS_TIMEOUT_IN_SECONDS,
                $response
            );
        }

        return $response;
    }
}
