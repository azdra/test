<?php

namespace App\Twig;

use App\Entity\CategorySessionType;
use App\Entity\Client\TrainingActionNatureEnum;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class EnumExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('getTrainingActionNatureEnumValues', [$this, 'getTrainingActionNatureEnumValues']),
            new TwigFunction('getCategorySessionValues', [$this, 'getCategorySessionValues']),
        ];
    }

    public function getTrainingActionNatureEnumValues(): array
    {
        $values = [];
        foreach (TrainingActionNatureEnum::cases() as $enum) {
            $values[$enum->value] = $enum->label();
        }

        return $values;
    }

    public function getCategorySessionValues(): array
    {
        $values = [];
        foreach (CategorySessionType::cases() as $enum) {
            $values[$enum->value] = $enum->label();
        }

        return $values;
    }
}
