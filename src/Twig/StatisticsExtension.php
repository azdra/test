<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class StatisticsExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('calculateTendency', [$this, 'calculateTendency']),
        ];
    }

    public function calculateTendency(int $newValue, int $oldValue): string
    {
        if ($oldValue === 0) {
            return '0%';
        }

        return ($newValue > $oldValue ? '+' : '-').round((100 * abs($newValue - $oldValue)) / $oldValue).'%';
    }
}
