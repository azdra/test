<?php

namespace App\Twig;

use App\Service\Utils\DateUtils;
use Symfony\Component\Intl\Languages;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ConvocationExtension extends AbstractExtension
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('applyTimezone', [$this, 'applyTimezone']),
            new TwigFilter('localizeDate', [$this, 'localizeDate']),
            new TwigFilter('dateOnly', [$this, 'dateOnly']),
            new TwigFilter('dateYearOnly', [$this, 'dateYearOnly']),
            new TwigFilter('dateMonthOnly', [$this, 'dateMonthOnly']),
            new TwigFilter('dateDayOnly', [$this, 'dateDayOnly']),
            new TwigFilter('dateDaySuffixedOnly', [$this, 'dateDaySuffixedOnly']),
            new TwigFilter('hourOnly', [$this, 'hourOnly']),
            new TwigFilter('hourH12Only', [$this, 'hourH12Only']),
            new TwigFilter('humanizeMonth', [$this, 'humanizeMonth']),
            new TwigFilter('humanizeWeekDay', [$this, 'humanizeWeekDay']),
            new TwigFilter('humanizeLang', [$this, 'humanizeLang']),
            new TwigFilter('humanizeDuration', [$this, 'humanizeDuration']),
            new TwigFilter('humanizeDurationWithoutM', [$this, 'humanizeDurationWithoutM']),
        ];
    }

    public function applyTimezone(\DateTimeInterface $dateTime, string $timezone): \DateTimeInterface
    {
        return \DateTimeImmutable::createFromInterface($dateTime)->setTimezone(new \DateTimeZone($timezone));
    }

    public function localizeDate(\DateTimeInterface $dateTime, string $locale = 'fr'): string
    {
        return $dateTime->format($locale === 'en' ? 'Y/m/d h:i a' : 'd/m/Y H:i');
    }

    public function dateOnly(\DateTimeInterface $dateTime, string $locale = 'fr'): string
    {
        return $dateTime->format($locale === 'en' ? 'Y/m/d' : 'd/m/Y');
    }

    public function dateYearOnly(\DateTimeInterface $dateTime): string
    {
        return $dateTime->format('Y');
    }

    public function dateMonthOnly(\DateTimeInterface $dateTime): string
    {
        return $dateTime->format('m');
    }

    public function dateDayOnly(\DateTimeInterface $dateTime): string
    {
        return $dateTime->format('d');
    }

    public function dateDaySuffixedOnly(\DateTimeInterface $dateTime, string $locale = 'fr'): string
    {
        $translatedText = $this->translator->trans($dateTime->format('S'), locale: $locale);

        return $dateTime->format('d').$translatedText;
    }

    public function hourOnly(\DateTimeInterface $dateTime, string $locale = 'fr'): string
    {
        return $dateTime->format($locale === 'en' ? 'h:i a' : 'H:i');
    }

    public function hourH12Only(\DateTimeInterface $dateTime, string $locale = 'fr'): string
    {
        $translatedText = $this->translator->trans($dateTime->format('a'), locale: $locale);

        return $dateTime->format('h:i ').$translatedText;
    }

    public function humanizeMonth(\DateTimeInterface $dateTime, string $locale = 'fr'): string
    {
        return $this->translator->trans($dateTime->format('F'), locale: $locale);
    }

    public function humanizeWeekDay(\DateTimeInterface $dateTime, string $locale = 'fr'): string
    {
        return $this->translator->trans($dateTime->format('l'), locale: $locale);
    }

    public function humanizeLang(string $lang, string $locale = 'fr'): string
    {
        return Languages::getName($lang, displayLocale: $locale);
    }

    public function humanizeDuration(int $durationInMinutes, string $locale = 'fr'): string
    {
        return DateUtils::humanizeDuration($durationInMinutes, $locale);
    }

    public function humanizeDurationWithoutM(int $durationInMinutes, string $locale = 'fr'): string
    {
        return DateUtils::humanizeDurationWithoutM($durationInMinutes, $locale);
    }
}
