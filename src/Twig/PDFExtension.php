<?php

namespace App\Twig;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class PDFExtension extends AbstractExtension
{
    public function __construct(private ParameterBagInterface $parameters)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('pdf_upload_url', [$this, 'generateUploadUrl']),
        ];
    }

    public function generateUploadUrl(string $path): string
    {
        return $this->parameters->get('kernel.project_dir').'/public'.$path;
    }
}
