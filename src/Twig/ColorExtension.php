<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ColorExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('hexToRgb', [$this, 'hexToRgb']),
        ];
    }

    public function hexToRgb(string $hex): string
    {
        return join(',', sscanf($hex, '#%02x%02x%02x'));
    }
}
