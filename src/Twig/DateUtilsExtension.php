<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class DateUtilsExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('convertMinuteToReadable', [$this, 'convertMinuteToReadable']),
        ];
    }

    public function convertMinuteToReadable(int $minutes, string $lang): string
    {
        $hours = floor($minutes / 60);
        $hourSeparator = ($lang === 'fr' ? 'H' : ':');
        $leftMinutes = $minutes % 60;

        return ($hours > 0 ? $hours.$hourSeparator : '').($leftMinutes < 10 ? '0' : '').$leftMinutes;
    }
}
