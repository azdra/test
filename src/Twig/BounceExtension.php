<?php

namespace App\Twig;

use App\Repository\BounceRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class BounceExtension extends AbstractExtension
{
    public function __construct(
        private BounceRepository $bounceRepository
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('countBounceToCorrect', [$this, 'countBounceToCorrect']),
        ];
    }

    public function countBounceToCorrect(): int
    {
        return $this->bounceRepository->getNumberOfBounceToCorrect();
    }
}
