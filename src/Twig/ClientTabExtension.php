<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ClientTabExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
          new TwigFunction('tabName', [$this, 'getTabName']),
        ];
    }

    public function getTabName(string $formName): string
    {
        $className = "App\Form\Client\Tab\\$formName";

        return $className::getName();
    }
}
