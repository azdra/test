<?php

namespace App\Twig;

use App\Entity\Client\Client;
use App\Repository\ActivityLogRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class NotificationsExtension extends AbstractExtension
{
    public function __construct(
        private ActivityLogRepository $activityLogRepository
    ) {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('countNotifications', [$this, 'countNotifications']),
            new TwigFunction('getLastNotifications', [$this, 'getLastNotifications']),
        ];
    }

    public function countNotifications(Client $client): int
    {
        return count($this->getNotifications($client));
    }

    public function getLastNotifications(Client $client): array
    {
        return $this->getNotifications(($client));
    }

    private function getNotifications(Client $client): array
    {
        $listNotifications = $this->activityLogRepository->findAlertNotification($client);

        return $listNotifications;
    }
}
