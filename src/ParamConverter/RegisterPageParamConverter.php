<?php

namespace App\ParamConverter;

use App\Entity\Client\Client;
use App\Entity\ProviderSession;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

#[AutoconfigureTag('request.param_converter', ['converter' => 'register_page_param_converter'])]
class RegisterPageParamConverter implements ParamConverterInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $client = $request->attributes->get('client');
        $session = $request->attributes->get('session');

        /** @var Client $client */
        $client = $this->entityManager->getRepository(Client::class)->findOneBy(['slug' => $client]);
        if (!$client) {
            throw new NotFoundHttpException('Client not found');
        }

        /** @var ProviderSession $session */
        $session = $this->entityManager->getRepository(ProviderSession::class)->findOneBy(['id' => $session, 'client' => $client]);
        if (!$session) {
            throw new NotFoundHttpException('Session not found');
        }

        if (!$session->getRegistrationPageTemplate()) {
            throw new NotFoundHttpException('Session template not found');
        }

        $request->attributes->set($configuration->getName(), $session);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getConverter() === 'register_page_param_converter' && $configuration->getClass() === ProviderSession::class;
    }
}
