<?php

namespace App\ParamConverter;

use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\ProviderSession;
use App\Exception\MiddlewareFailureException;
use App\Service\Provider\Common\ProviderSessionFactory;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Throwable;

#[AutoconfigureTag('request.param_converter', ['converter' => 'provider_session_api_converter'])]
class ProviderSessionApiConverter implements ParamConverterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @throws Exception
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $format = $request->getContentType();
        if (null === $format) {
            return $this->throwException(new UnsupportedMediaTypeHttpException(), $configuration);
        }

        try {
            $session = $this->createProviderSessionObject($request);

            if ($session instanceof WebexMeeting) {
                /** @var WebexConfigurationHolder $configurationHolder */
                $configurationHolder = $session->getAbstractProviderConfigurationHolder();
                if ($configurationHolder->isAutomaticLicensing()) {
                    $session->setLicence(null);
                }
            }

            if ($session instanceof WebexRestMeeting) {
                /** @var WebexRestConfigurationHolder $configurationHolder */
                $configurationHolder = $session->getAbstractProviderConfigurationHolder();
                if ($configurationHolder->isAutomaticLicensing()) {
                    $session->setLicence(null);
                }
            }

            if ($session instanceof MicrosoftTeamsSession) {
                /** @var MicrosoftTeamsConfigurationHolder $configurationHolder */
                $configurationHolder = $session->getAbstractProviderConfigurationHolder();
                if ($configurationHolder->isAutomaticLicensing()) {
                    $session->setLicence(null);
                }
            }

            if ($session instanceof MicrosoftTeamsEventSession) {
                /** @var MicrosoftTeamsEventConfigurationHolder $configurationHolder */
                $configurationHolder = $session->getAbstractProviderConfigurationHolder();
                if ($configurationHolder->isAutomaticLicensing()) {
                    $session->setLicence(null);
                }
            }

            $request->attributes->set($configuration->getName(), $session);
        } catch (Throwable $e) {
            $this->logger->error($e);

            if ($e instanceof NotFoundHttpException) {
                throw $e;
            }

            return $this->throwException(new BadRequestHttpException($e->getMessage(), $e), $configuration);
        }

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getConverter() === 'provider_session_api_converter' && $configuration->getClass() === ProviderSession::class;
    }

    /**
     * @throws Exception
     */
    private function throwException(Exception $exception, ParamConverter $configuration): bool
    {
        $className = self::class;
        $this->logger->error($exception, [
            'options' => $configuration->getOptions(),
            'name' => $configuration->getName(),
        ]);
        throw $exception;
    }

    private function getEntity(string $class, string $fieldName, array $collection = []): mixed
    {
        $path = explode('.', $fieldName);
        $currentEntry = $collection;
        $lastEntryName = end($path);

        foreach ($path as $item) {
            if (!array_key_exists($item, $currentEntry)) {
                throw new BadRequestHttpException("Invalid $item field");
            }

            if ($item !== $lastEntryName) {
                $currentEntry = $currentEntry[$item];
            }
        }

        $entityId = $currentEntry[$lastEntryName];

        return $this->entityManager
            ->getRepository($class)
            ->find($entityId);
    }

    /**
     * @throws MiddlewareFailureException
     */
    private function createProviderSessionObject(Request $request): ProviderSession
    {
        /** @var AbstractProviderConfigurationHolder $configurationHolder */
        $configurationHolder = $this->getEntity(AbstractProviderConfigurationHolder::class, 'abstractProviderConfigurationHolder.id', $request->request->all());

        if (empty($configurationHolder)) {
            throw new BadRequestException('The given configuration holder was not found');
        }

        $session = ProviderSessionFactory::createProviderSession($request->request->get('type'), $configurationHolder);
        $session->setStatus(ProviderSession::STATUS_SCHEDULED);

        return $session;
    }
}
