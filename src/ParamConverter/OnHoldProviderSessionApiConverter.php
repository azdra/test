<?php

namespace App\ParamConverter;

use App\Entity\OnHoldProviderSession;
use App\Exception\MiddlewareFailureException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Throwable;

#[AutoconfigureTag('request.param_converter', ['converter' => 'on_hold_provider_session_api_converter'])]
class OnHoldProviderSessionApiConverter implements ParamConverterInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger
    ) {
    }

    /**
     * @throws Exception
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $format = $request->getContentType();
        if (null === $format) {
            return $this->throwException(new UnsupportedMediaTypeHttpException(), $configuration);
        }

        try {
            $session = $this->createProviderSessionObject();
            $request->attributes->set($configuration->getName(), $session);

            return true;
        } catch (Throwable $e) {
            $this->logger->error($e);

            if ($e instanceof NotFoundHttpException) {
                throw $e;
            }

            return $this->throwException(new BadRequestHttpException($e->getMessage(), $e), $configuration);
        }

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getConverter() === 'on_hold_provider_session_api_converter' && $configuration->getClass() === OnHoldProviderSession::class;
    }

    /**
     * @throws Exception
     */
    private function throwException(Exception $exception, ParamConverter $configuration): bool
    {
        $this->logger->error($exception, [
            'options' => $configuration->getOptions(),
            'name' => $configuration->getName(),
        ]);
        throw $exception;
    }

    /**
     * @throws MiddlewareFailureException
     */
    private function createProviderSessionObject(): OnHoldProviderSession
    {
        $session = new OnHoldProviderSession();
        $session->setStatusValidation(OnHoldProviderSession::STATUS_WAIT_FOR_VALIDATION);

        return $session;
    }
}
