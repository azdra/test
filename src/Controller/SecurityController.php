<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\LoginAuthenticatorFormType;
use App\Form\ResetUserPasswordFormType;
use App\Form\ResetUserPasswordRequestFormType;
use App\Model\ResetUserPasswordModel;
use App\Model\ResetUserPasswordRequestModel;
use App\Repository\UserRepository;
use App\Security\LoginFormAuthenticator;
use App\Security\UserSecurityService;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

class SecurityController extends AbstractController
{
    public function __construct(
        private TranslatorInterface $translator,
        private UserSecurityService $userSecurityService,
        private EntityManagerInterface $entityManager,
        private UserRepository $userRepository
    ) {
    }

    #[Route('/login', name: '_app_login', options: ['expose' => true])]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute(LoginFormAuthenticator::DEFAULT_ROUTE);
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $form = $this->createForm(LoginAuthenticatorFormType::class, [
            'username' => $lastUsername,
        ]);

        if ($error) {
            $message = (!empty($error->getMessageKey()))
                ? $this->translator->trans('Invalid login credentials')
                : $this->translator->trans('Unknown error');
            $form->addError(new FormError($message));
        }

        return $this->render('security/login.html.twig', [
            'title' => 'Login',
            'form' => $form->createView(),
            'lastUsername' => $lastUsername,
        ]);
    }

    #[Route('/logout', name: '_app_logout')]
    public function logout(): void
    {
        throw new LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    #[Route('/password/reset-request', name: '_password_reset_request')]
    public function resetPasswordRequest(Request $request): Response
    {
        $model = new ResetUserPasswordRequestModel();
        $form = $this->createForm(ResetUserPasswordRequestFormType::class, $model);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $user = $this->userRepository->findOneBy([
                    'email' => $model->getEmail(),
                    'status' => User::STATUS_ACTIVE,
                ]);

                if ($user) {
                    $this->userSecurityService->requestPasswordReset($user);
                    $this->entityManager->flush();
                }

                $this->addFlash('info', 'If the email address entered is linked to a user, a password reset link will be sent to it. This action is only available once every 3 minutes.');
                $form = $this->createForm(ResetUserPasswordRequestFormType::class);
            }
        }

        $response = $this->render('security/reset_password_request.html.twig', [
            'title' => 'Password reset request',
            'form' => $form->createView(),
        ]);

        return $response;
    }

    #[Route('/password/reset/{token}', name: '_password_reset_action')]
    public function resetPasswordAction(Request $request, string $token): Response
    {
        /** @var User $user */
        if (null === $user = $this->userRepository->findOneBy(['passwordResetToken' => $token])) {
            throw new AccessDeniedException();
        }

        $model = new ResetUserPasswordModel();
        $form = $this->createForm(ResetUserPasswordFormType::class, $model);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->userSecurityService->updatePasswordManager($user, $model->getPassword());
                $this->entityManager->flush();

                return $this->redirectToRoute('_home');
            }
        }

        return $this->render('security/reset_password_action.html.twig', [
            'title' => 'Password reset',
            'form' => $form->createView(),
        ]);
    }

    #[Route('/change-locale/{_locale}', name: '_change_locale', requirements: ['_locale' => 'en|fr'])]
    public function changeLocale(): Response
    {
        return $this->redirectToRoute('_app_login');
    }
}
