<?php

namespace App\Controller\Api;

use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderSession;
use App\Exception\MiddlewareFailureException;
use App\Exception\ProviderServiceValidationException;
use App\Form\Api\AdobeConnectScoApiType;
use App\Form\Api\MicrosoftTeamsApiType;
use App\Form\Api\WebexMeetingApiType;
use App\Form\Api\WebexRestMeetingApiType;
use App\Repository\ProviderSessionRepository;
use App\Security\ProviderSessionVoter;
use App\Serializer\ProviderSessionNormalizer;
use App\Service\AdobeConnectService;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\Provider\Model\LicenceChecker;
use App\Service\ProviderSessionService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[OA\Tag(name: 'Sessions')]
#[Route('sessions')]
#[IsGranted('ROLE_MANAGER')]
class ProviderSessionApiController extends AbstractApiController
{
    public const GROUP_VIEW = 'api:session:view';
    public const GROUP_VIEW_PRESENCES = 'api:session:view:presences';
    public const GROUP_CREATE = 'api:session:create';
    public const GROUP_UPDATE = 'api:session:update';

    public function __construct(
        LoggerInterface $logger,
        private EntityManagerInterface $entityManager,
        private ProviderSessionService $providerSessionService,
        private ProviderSessionRepository $providerSessionRepository,
        private ValidatorInterface $validator,
        private ParticipantSessionSubscriber $participantSessionSubscriber,
        private ProviderSessionNormalizer $providerSessionNormalizer,
        private LicenceChecker $licenceChecker,
        private AdobeConnectService $adobeConnectService,
    ) {
        parent::__construct($logger);
    }

    #[View(serializerGroups: [self::GROUP_VIEW])]
    #[Get('/', name: '_api_session_get_all')]
    #[OA\Response(
        response: 200,
        description: 'List all sessions',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(
                ref: new Model(
                    type: ProviderSession::class,
                    groups: ['default', self::GROUP_VIEW]
                )
            )
        )
    )]
    public function getAll(Request $request): array
    {
        return $this->providerSessionRepository->globalSearch(
            null,
            true,
            from: $request->request->get('from'),
            to: $request->request->get('to')
        );
    }

    #[View(serializerGroups: [self::GROUP_VIEW, self::GROUP_VIEW_PRESENCES])]
    #[Get('/{id}', name: '_api_sessions_get', requirements: ['id' => '\d+'])]
    #[IsGranted(ProviderSessionVoter::ACCESS, subject: 'session')]
    #[OA\Response(
        response: 200,
        description: 'Get a specified session',
        content: new OA\JsonContent(
            ref: new Model(
                type: ProviderSession::class,
                groups: ['default', self::GROUP_VIEW, self::GROUP_VIEW_PRESENCES]
            ),
            type: 'object'
        )
    )]
    public function getProviderSession(ProviderSession $session): ProviderSession
    {
        return $session;
    }

    /**
     * @throws \Throwable
     */
    #[View(serializerGroups: [self::GROUP_VIEW])]
    #[ParamConverter('session', converter: 'provider_session_api_converter')]
    #[Post('/', name: '_api_sessions_post')]
    #[OA\RequestBody(
        description: 'Create a new meeting',
        required: true,
        content: new OA\JsonContent(
            properties: [
                new OA\Property(property: 'licence', description: 'Licence require for a webex session', type: 'string'),
                new OA\Property(property: 'name', description: 'Session title', type: 'string'),
                new OA\Property(property: 'ref', description: 'Session reference', type: 'string'),
                new OA\Property(property: 'dateStart', type: 'datetime'),
                new OA\Property(property: 'dateEnd', type: 'datetime'),
                new OA\Property(property: 'duration', type: 'int'),
                new OA\Property(property: 'type', description: 'Adobe: adobe_connect_sco <br /> Webex: cisco_webex_meeting <br /> Teams: microsoft_teams', type: 'string'),
                new OA\Property(
                    property: 'abstractProviderConfigurationHolder',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(property: 'id', description: 'Connector ID on MyLivesessionManager', type: 'int'),
                            new OA\Property(property: 'type', description: 'Connector type on MyLivesessionManager', type: 'string'),
                        ]
                    )
                ),
                new OA\Property(property: 'convocation', description: 'Convocation ID on MyLivesessionManager', type: 'int'),
                new OA\Property(
                    property: 'participants',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(property: 'userId', type: 'int'),
                            new OA\Property(property: 'role', type: 'string'),
                        ]
                    )
            ), ]
        )
    )]
    #[OA\Response(
        response: 200,
        description: 'Get the new session created',
        content: new OA\JsonContent(
            ref: new Model(
                type: ProviderSession::class,
                groups: ['default', self::GROUP_VIEW]
            ),
            type: 'object'
        )
    )]
    public function postProviderSession(Request $request, ProviderSession $session): ProviderSession|\FOS\RestBundle\View\View
    {
        try {
            $session = $this->handle($request, $session, self::GROUP_CREATE);

            $this->providerSessionService->createSession($session);

            $this->entityManager->flush();

            if ($request->request->has('participants')) {
                $participants = $request->request->all('participants');
                $this->participantSessionSubscriber->massSubscribe($session, $participants, 'userId');
                $this->participantSessionSubscriber->massUnsubscribe($session, $participants, 'userId');
            }

            $this->licenceChecker->checkLicence($session);

            $this->entityManager->flush();

            return $session;
        } catch (\Throwable $e) {
            return $this->exceptionViewHandler($e);
        }
    }

    /**
     * @throws \Throwable
     */
    #[View(serializerGroups: [self::GROUP_VIEW])]
    #[Patch('/{id}', name: '_api_sessions_patch', requirements: ['id' => '\d+'])]
    #[Put('/{id}', name: '_api_sessions_put', requirements: ['id' => '\d+'])]
    #[IsGranted(ProviderSessionVoter::EDIT, subject: 'session')]
    #[OA\RequestBody(
        description: 'Update an existing meeting',
        required: true,
        content: new OA\JsonContent(
            properties: [
                new OA\Property(property: 'licence', description: 'Licence require if type = cisco_webex_meeting ', type: 'string'),
                new OA\Property(property: 'name', description: 'Session title', type: 'string'),
                new OA\Property(property: 'ref', description: 'Session reference', type: 'string'),
                new OA\Property(property: 'dateStart', type: 'datetime'),
                new OA\Property(property: 'dateEnd', type: 'datetime'),
                new OA\Property(property: 'duration', type: 'int'),
                new OA\Property(property: 'type', description: 'Adobe: adobe_connect_sco <br /> Webex: cisco_webex_meeting <br /> Teams: microsoft_teams', type: 'string'),
                new OA\Property(property: 'abstractProviderConfigurationHolder', description: 'Connector ID on MyLivesessionManager', type: 'int'),
                new OA\Property(property: 'convocation', description: 'Convocation ID on MyLivesessionManager', type: 'int'),
                new OA\Property(
                    property: 'participants',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(property: 'userId', type: 'int'),
                            new OA\Property(property: 'role', type: 'string'),
                        ]
                    )
                ), ]
        )
    )]
    #[OA\Response(
        response: 200,
        description: 'Get the updated session',
        content: new OA\JsonContent(
            ref: new Model(
                type: ProviderSession::class,
                groups: ['default', self::GROUP_VIEW]
            ),
            type: 'object'
        )
    )]
    public function patchProviderSession(Request $request, ProviderSession $session): ProviderSession|\FOS\RestBundle\View\View
    {
        try {
            $session = $this->handle($request, $session, self::GROUP_UPDATE);

            $this->providerSessionService->updateSession($session);

            if ($request->request->has('participants')) {
                $participants = $request->request->all('participants');
                $this->participantSessionSubscriber->massSubscribe($session, $participants, 'userId');
                $this->participantSessionSubscriber->massUnsubscribe($session, $participants, 'userId');
            }

            $this->licenceChecker->checkLicence($session);

            $this->entityManager->flush();

            return $session;
        } catch (\Throwable $e) {
            return $this->exceptionViewHandler($e);
        }
    }

    #[View(statusCode: Response::HTTP_NO_CONTENT)]
    #[Delete('/{id}', name: '_api_sessions_delete', requirements: ['id' => '\d+'])]
    #[IsGranted(ProviderSessionVoter::DELETE, subject: 'session')]
    public function delete(ProviderSession $session): void
    {
        $this->providerSessionService->deleteSession($session);
        $this->entityManager->flush();
    }

    /**
     * @throws MiddlewareFailureException
     */
    private function providerSessionFromHandler(string $type): string
    {
        return match ($type) {
            'cisco_webex_meeting' => WebexMeetingApiType::class,
            'cisco_webex_rest_meeting' => WebexRestMeetingApiType::class,
            'adobe_connect_sco' => AdobeConnectScoApiType::class,
            'microsoft_teams_session' => MicrosoftTeamsApiType::class,
            default => throw new MiddlewareFailureException(ProviderServiceValidationException::MESSAGE, ['type' => 'This session type in not recognized'])
        };
    }

    /**
     * @throws MiddlewareFailureException
     * @throws \Exception
     */
    private function handle(Request $request, ProviderSession $session, string $group): ProviderSession
    {
        $data = $request->request->all();
        if (!array_key_exists('type', $data)) {
            throw new MiddlewareFailureException(ProviderServiceValidationException::MESSAGE, ['type' => 'This field is missing']);
        }

        if (array_key_exists('abstractProviderConfigurationHolder', $data) && array_key_exists('convocation', $data)) {
            if (!$this->providerSessionService->isValidConvocation(configurationHolderId: $data['abstractProviderConfigurationHolder']['id'], convocationId: $data['convocation'])) {
                throw new \Exception('the convocation does not exist in this connector');
            }
        }

        $normalizedSession = $this->providerSessionNormalizer->normalize($session, context: ['groups' => [
            $group, ProviderSessionApiController::GROUP_VIEW,
        ]]);

        $requestData = array_merge($normalizedSession, $request->request->all());

        $form = $this->createForm($this->providerSessionFromHandler($data['type']), $session, ['group' => $group]);

        $form->submit($requestData, $request->isMethod(Request::METHOD_PUT));

        $configurationHolder = $session->getAbstractProviderConfigurationHolder();
        if ($configurationHolder instanceof AdobeConnectConfigurationHolder && empty($session->getProviderAudio()) && $session->getConvocation()->getCategoryTypeAudio() != AbstractProviderAudio::AUDIO_CATEGORY_COMPUTER) {
            $resultAudio = $this->adobeConnectService->getAvailableAudios($session->getDateStart(), $session->getDateEnd(), $session->getConvocation()->getTypeAudio(), $session->getConvocation()->getLanguages(), $configurationHolder);
            if (empty($resultAudio)) {
                throw new \Exception('No audio is available for session in this timeslot');
            }
        }

        $constraintsViolations = $this->validator->validate($session, groups: $group);

        if ($form->isSubmitted() && $constraintsViolations->count() > 0) {
            throw new MiddlewareFailureException(ProviderServiceValidationException::MESSAGE, $constraintsViolations);
        }

        return $session;
    }
}
