<?php

namespace App\Controller\Api;

use App\Exception\MiddlewareFailureException;
use FOS\RestBundle\Controller\ControllerTrait;
use FOS\RestBundle\View\View;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

abstract class AbstractApiController extends AbstractController
{
    use ControllerTrait;

    public function __construct(
        protected LoggerInterface $logger
    ) {
    }

    /**
     * @throws Throwable
     */
    protected function exceptionViewHandler(Throwable $exception): View
    {
        switch (get_class($exception)) {
            case MiddlewareFailureException::class:
                if ($exception->getData() instanceof ConstraintViolationListInterface) {
                    $errors = [];

                    /** @var ConstraintViolation $constraintViolation */
                    foreach ($exception->getData() as $constraintViolation) {
                        $errors[$constraintViolation->getPropertyPath()] = $constraintViolation->getMessage();
                    }
                } else {
                    $errors = $exception->getData();
                }

                $response = [
                    'code' => Response::HTTP_BAD_REQUEST,
                    'message' => $exception->getMessage(),
                    'errors' => $errors,
                ];

                $this->logger->error($exception, [
                    'errors' => $errors,
                ]);

                return $this->view($response, $response['code']);

            default:
                $this->logger->error($exception);
                throw $exception;
        }
    }
}
