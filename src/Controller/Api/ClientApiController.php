<?php

namespace App\Controller\Api;

use App\Entity\Client\Client;
use App\Repository\ClientRepository;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;

#[OA\Tag(
    name: 'Clients',
)]
#[Route('/clients')]
#[IsGranted('ROLE_MANAGER')]
class ClientApiController extends AbstractApiController
{
    public const GROUP_VIEW = 'api:client:view';

    public function __construct(
        LoggerInterface $logger,
        private ClientRepository $clientRepository
    ) {
        parent::__construct($logger);
    }

    #[View(serializerGroups: [ClientApiController::GROUP_VIEW])]
    #[Get('/', name: '_api_client_list')]
    #[OA\Response(
        response: 200,
        description: 'All available client',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: Client::class, groups: [self::GROUP_VIEW]))
        )
    )]
    public function getAll(): array
    {
        if ($this->isGranted('ROLE_ADMIN')) {
            return $this->clientRepository->findAll();
        } else {
            return [$this->getUser()->getClient()];
        }
    }
}
