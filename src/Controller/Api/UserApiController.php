<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Exception\MiddlewareFailureException;
use App\Form\Api\UserApiType;
use App\Repository\UserRepository;
use App\Security\UserVoter;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\Alice\Throwable\Exception\Generator\Hydrator\InvalidArgumentException;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[OA\Tag(name: 'Users')]
#[Route('/users')]
#[IsGranted('ROLE_MANAGER')]
class UserApiController extends AbstractApiController
{
    public const GROUP_VIEW = 'api:user:view';
    public const GROUP_VIEW_SUBSCRIPTIONS = 'api:user:view:subscriptions';
    public const GROUP_CREATE = 'api:user:create';
    public const GROUP_UPDATE = 'api:user:update';

    public function __construct(
        LoggerInterface $logger,
        private UserRepository $userRepository,
        private EntityManagerInterface $entityManager,
        private UserService $userService,
        private ValidatorInterface $validator
    ) {
        parent::__construct($logger);
    }

    #[View(serializerGroups: [UserApiController::GROUP_VIEW])]
    #[Get('/', name: '_api_users_list')]
    #[OA\Response(
        response: 200,
        description: 'List all trainee user available',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: User::class, groups: [self::GROUP_VIEW]))
        )
    )]
    public function getAll(): array
    {
        return $this->userRepository->findListByRoleTarget(User::ROLE_TRAINEE);
    }

    #[View(serializerGroups: [UserApiController::GROUP_VIEW, self::GROUP_VIEW_SUBSCRIPTIONS])]
    #[Get('/me', name: '_api_users_get_himself')]
    #[OA\Response(
        response: 200,
        description: 'Return your my live session manager user',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: User::class, groups: [self::GROUP_VIEW]))
        )
    )]
    public function getHimself(): ?User
    {
        return $this->getUser();
    }

    #[View(serializerGroups: [UserApiController::GROUP_VIEW, self::GROUP_VIEW_SUBSCRIPTIONS])]
    #[Get('/{id}', name: '_api_users_get', requirements: ['id' => '\d+'])]
    #[OA\Response(
        response: 200,
        description: 'Get user',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: User::class, groups: [self::GROUP_VIEW]))
        )
    )]
    #[IsGranted(UserVoter::EDIT, subject: 'user')]
    public function getItem(User $user): User
    {
        return $user;
    }

    /**
     * @throws \Throwable
     */
    #[View(serializerGroups: [UserApiController::GROUP_VIEW])]
    #[Post('/', name: '_api_users_post')]
    #[OA\RequestBody(
        description: 'Add user',
        required: true,
        content: new OA\JsonContent(
            ref: new Model(type: User::class, groups: [self::GROUP_CREATE])
        )
    )]
    #[OA\Response(
        response: 200,
        description: 'Create a new user',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: User::class, groups: [self::GROUP_VIEW]))
        )
    )]
    public function create(Request $request): User|\FOS\RestBundle\View\View
    {
        try {
            $user = new User();
            $this->handleUpdate($request, $user, false, [self::GROUP_CREATE]);
            if (!$this->userService->isValidMail($user)) {
                throw new InvalidArgumentException('The email is not in the right format');
            }
            $this->userService->initUser($user);

            /** @var User $userInDb */
            $userInDb = $this->userRepository->findOneBy(['email' => $user->getEmail(), 'client' => $user->getClient()]);
            if (!is_null($userInDb)) {
                throw new MiddlewareFailureException('This user is already exist', ['email' => $userInDb->getEmail(), 'clientName' => $userInDb->getClient()->getName(), 'clientId' => $userInDb->getClient()->getId()]);
            }

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            return $user;
        } catch (\Throwable $e) {
            return $this->exceptionViewHandler($e);
        }
    }

    /**
     * @throws \Throwable
     */
    #[View(serializerGroups: [UserApiController::GROUP_VIEW])]
    #[Put('/{id}', name: '_api_users_put', requirements: ['id' => '\d+'])]
    #[OA\RequestBody(
        description: 'Add user',
        required: true,
        content: new OA\JsonContent(
            ref: new Model(type: User::class, groups: [self::GROUP_CREATE])
        )
    )]
    #[OA\Response(
        response: 200,
        description: 'Replacing an existing user',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: User::class, groups: [self::GROUP_VIEW]))
        )
    )]
    #[IsGranted(UserVoter::EDIT, subject: 'user')]
    public function update(Request $request, User $user): User|\FOS\RestBundle\View\View
    {
        try {
            $this->handleUpdate($request, $user, false, [self::GROUP_UPDATE]);
            if (!$this->userService->isValidMail($user)) {
                throw new InvalidArgumentException('The email is not in the right format');
            }
            $this->entityManager->flush();

            return $user;
        } catch (\Throwable $e) {
            return $this->exceptionViewHandler($e);
        }
    }

    /**
     * @throws \Throwable
     */
    #[View(serializerGroups: [UserApiController::GROUP_VIEW])]
    #[Patch('/{id}', name: '_api_users_patch', requirements: ['id' => '\d+'])]
    #[OA\RequestBody(
        description: 'Add user',
        required: true,
        content: new OA\JsonContent(
            ref: new Model(type: User::class, groups: [self::GROUP_CREATE])
        )
    )]
    #[OA\Response(
        response: 200,
        description: 'Updating user',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(ref: new Model(type: User::class, groups: [self::GROUP_VIEW]))
        )
    )]
    #[IsGranted(UserVoter::EDIT, subject: 'user')]
    public function partialUpdate(Request $request, User $user): User|\FOS\RestBundle\View\View
    {
        try {
            $this->handleUpdate($request, $user, true, [self::GROUP_UPDATE]);
            $this->entityManager->flush();

            return $user;
        } catch (\Throwable $e) {
            return $this->exceptionViewHandler($e);
        }
    }

    #[View(statusCode: Response::HTTP_NO_CONTENT)]
    #[Delete('/{id}', name: '_api_users_delete', requirements: ['id' => '\d+'])]
    #[OA\Response(
        response: 200,
        description: 'Delete a user',
    )]
    #[IsGranted(UserVoter::EDIT, subject: 'user')]
    public function delete(User $user): void
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

    /**
     * @throws MiddlewareFailureException
     */
    private function handleUpdate(Request $request, User $user, bool $partial, array $groups): void
    {
        $form = $this->createForm(UserApiType::class, $user);
        $form->submit($request->request->all(), !$partial);
        $violations = $this->validator->validate($user, groups: $groups);

        if ($form->isSubmitted() && $violations->count() > 0) {
            throw new MiddlewareFailureException('One or more field are invalid on this user', $violations);
        }
    }
}
