<?php

namespace App\Controller\Api;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\ControllerTrait;
use OpenApi\Attributes as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[OA\Tag(name: 'Test')]
#[IsGranted('ROLE_MANAGER')]
class DefaultApiController extends AbstractController
{
    use ControllerTrait;

    #[Get('/ping', name: '_api_ping')]
    #[OA\Response(
        response: 200,
        description: 'Return "pong" if service is available'
    )]
    public function ping(): string
    {
        return 'pong';
    }
}
