<?php

namespace App\Controller\Api;

use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\OnHoldProviderSession;
use App\Entity\ProviderSession;
use App\Exception\MiddlewareFailureException;
use App\Exception\ProviderServiceValidationException;
use App\Form\Api\OnHoldProviderSessionApiType;
use App\Model\ApiResponseTrait;
use App\Repository\ClientRepository;
use App\Repository\OnHoldProviderSessionRepository;
use App\Repository\ProviderSessionRepository;
use App\Security\OnHoldProviderSessionVoter;
use App\Service\OnHoldProviderSessionService;
use DateInterval;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use http\Message;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

//https://docs.google.com/document/d/1TPHvTrz1THhbFKYqGmUByoviie7Y5pVmU1FdwmyGQkU/edit?usp=sharing
#[OA\Tag(name: 'OnHoldSessions')]
#[Route('onhold-sessions')]
#[IsGranted('ROLE_MANAGER')]
class OnHoldProviderSessionApiController extends AbstractApiController
{
    use ApiResponseTrait;

    public const GROUP_VIEW = 'api:onhold-session:view';
    public const GROUP_VIEW_PRESENCES = 'api:onhold-session:view:presences';
    public const GROUP_CREATE = 'api:onhold-session:create';
    public const GROUP_UPDATE = 'api:onhold-session:update';

    public function __construct(
        LoggerInterface $logger,
        private EntityManagerInterface $entityManager,
        private OnHoldProviderSessionService $onHoldProviderSessionService,
        private OnHoldProviderSessionRepository $onHoldProviderSessionRepository,
        private ProviderSessionRepository $providerSessionRepository,
        private ClientRepository $clientRepository,
        private ValidatorInterface $validator,
        //private ObjectNormalizer $normalizer,
        private NormalizerInterface $normalizer,
        private Security $security,
    ) {
        parent::__construct($logger);
    }

    #[View(serializerGroups: [self::GROUP_VIEW])]
    #[ParamConverter('session', converter: 'on_hold_provider_session_api_converter')]
    #[Post('/', name: '_api_on_hold_session_create')]
    #[OA\RequestBody(
        description: 'Create a new on hold session',
        required: true,
        content: new OA\JsonContent(
        /*ref: new Model(
            type: OnHoldProviderSession::class,
            groups: ['default', self::GROUP_CREATE]
        ),
        type: 'object',*/
            properties: [
                new OA\Property(property: 'megaUUID', description: 'MegaUUID of the session', type: 'string'),
                new OA\Property(property: 'title', description: 'Session title', type: 'string'),
                new OA\Property(property: 'connectorType', description: 'Connector type', type: 'string'),
                new OA\Property(property: 'participants', description: 'Participants', type: 'array'),
                new OA\Property(property: 'slotsDates',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(property: 'megaId', type: 'string'),
                            new OA\Property(property: 'dateStart', type: 'string'),
                            new OA\Property(property: 'dateEnd', type: 'string'),
                            new OA\Property(property: 'statusValidation', type: 'int'),
                        ]
                    )),
                new OA\Property(property: 'nbrMinParticipants', description: 'Minimum number of participants', type: 'integer'),
                new OA\Property(property: 'nbrMaxParticipants', description: 'Maximum number of participants', type: 'integer'),
            ]
        )
    )]
    #[OA\Response(
        response: 200,
        description: 'Get the new session created',
        content: new OA\JsonContent(
            ref: new Model(
                type: OnHoldProviderSession::class,
                groups: [self::GROUP_VIEW]
            ),
            type: 'object'
        )
    )]
    public function create(Request $request, OnHoldProviderSession $session): Response
    {
        try {
            $data = $request->request->all();

            $client = $this->getUser()->getClient();
            if (empty($this->getUser())) {
                return $this->apiErrorResponse('A problem with your connection !', extraData: 'Not user find with the x-auth-token.');
            }

            if (empty($client)) {
                return $this->apiErrorResponse('Your connection account is not present for this account!', extraData: 'You use '.$this->getUser()->getEmail().' ('.implode(', ', $this->getUser()->getRoles()).') for this connection.');
            }

            if (empty($data['megaUUID']) || $data['megaUUID'] == '') {
                return $this->apiErrorResponse('A required value is empty!', extraData: 'The field "megaUUID" is empty.');
            }

            $onHoldProviderSession = new OnHoldProviderSession();
            $onHoldProviderSession->setClient($client);
            $onHoldProviderSession->setCreatedBy($this->getUser());
            $onHoldProviderSession->setUpdatedBy($this->getUser());
            $onHoldProviderSession->setUpdatedAt(new \DateTime());
            $onHoldProviderSession->setMegaUUID($data['megaUUID']);
            $onHoldProviderSession->setTitle($data['title']);
            $onHoldProviderSession->setConnectorType($data['connectorType']);
            $onHoldProviderSession->setParticipants($data['participants']);
            $this->onHoldProviderSessionService->setParticipantsDiff($onHoldProviderSession);
            //dump($onHoldProviderSession->getParticipants()); die();
            //Converti en UTC et tri par dateStart
            $dateSlotsdatesToUTC = $this->onHoldProviderSessionService->convertSlotsDateToUTC($data['slotsDates']);
            $sortSlotsDates = $this->onHoldProviderSessionService->compareDatesSort($dateSlotsdatesToUTC, 'Asc');
            $onHoldProviderSession->setSlotsDates($sortSlotsDates);

            $onHoldProviderSession->setNbrMinParticipants($data['nbrMinParticipants']);
            $onHoldProviderSession->setNbrMaxParticipants($data['nbrMaxParticipants']);
            $onHoldProviderSession->setStatusValidation(OnHoldProviderSession::STATUS_WAIT_FOR_VALIDATION);
            $this->entityManager->persist($onHoldProviderSession);

            //On ajoute le diff entre les dates de début et de fin
            $this->onHoldProviderSessionService->setSlotsDatesDiff($onHoldProviderSession);
            $this->entityManager->persist($onHoldProviderSession);
            $this->entityManager->flush();

            //Converti au timezone du client pour la réponse du POST
            $dateSlotsdatesToTimezone = $this->onHoldProviderSessionService->convertSlotsDateToTimezone($onHoldProviderSession->getSlotsDates(), $client->getTimezone());
            $onHoldProviderSession->setSlotsDates($dateSlotsdatesToTimezone);
            $dataOnHoldProviderSession = $this->normalizer->normalize($onHoldProviderSession, null, ['groups' => [self::GROUP_VIEW]]);

            return $this->apiResponse(['data' => $dataOnHoldProviderSession, 'status' => 200]);
        } catch (\Throwable $e) {
            return $this->apiErrorResponse('An unknown error has occurred', extraData: $e->getMessage());
            //return $this->exceptionViewHandler($e);
        }
    }

    /**
     * @throws \Throwable
     */
    #[View(serializerGroups: [self::GROUP_VIEW])]
    #[Patch('/{id}', name: '_api_on_hold_provider_session_patch', requirements: ['id' => '\d+'])]
    #[Put('/{id}', name: '_api_on_hold_provider_session_put', requirements: ['id' => '\d+'])]
    //#[IsGranted(OnHoldProviderSessionVoter::EDIT, subject: 'session')]
    #[OA\RequestBody(
        description: 'Update an existing onHoldProviderSession',
        required: true,
        content: new OA\JsonContent(
            properties: [
                //new OA\Property(property: 'licence', description: 'Licence require if type = cisco_webex_meeting ', type: 'string'),
                new OA\Property(property: 'name', description: 'Session title', type: 'string'),
                new OA\Property(
                    property: 'slotsDates',
                    type: 'array',
                    items: new OA\Items(
                        properties: [
                            new OA\Property(property: 'megaId', type: 'string'),
                            new OA\Property(property: 'dateStart', type: 'string'),
                            new OA\Property(property: 'dateEnd', type: 'string'),
                            new OA\Property(property: 'statusValidation', type: 'int'),
                        ]
                    )
                ), ]
        )
    )]
    #[OA\Response(
        response: 200,
        description: 'Get the updated on hold provider session',
        content: new OA\JsonContent(
            ref: new Model(
                type: ProviderSession::class,
                groups: ['default', self::GROUP_VIEW]
            ),
            type: 'object'
        )
    )]
    public function patchOnHoldProviderSession(Request $request, OnHoldProviderSession $onHoldProviderSession): Response
    {
        try {
            $data = $request->request->all();

            $client = $this->getUser()->getClient();
            if (empty($this->getUser())) {
                return $this->apiErrorResponse('A problem with your connection !', extraData: 'Not user find with the x-auth-token.');
            }

            if (empty($client)) {
                return $this->apiErrorResponse('Your connection account is not present for this account!', extraData: 'You use '.$this->getUser()->getEmail().' ('.implode(', ', $this->getUser()->getRoles()).') for this connection.');
            }

            if (empty($data['megaUUID']) || $data['megaUUID'] == '') {
                return $this->apiErrorResponse('A required value is empty!', extraData: 'The field "megaUUID" is empty.');
            }

            $deltaAnimators = $this->compareArrays($onHoldProviderSession->getParticipants()['animators'], $data['participants']['animators']);
            $addAndUnchangedNewDataAnimators = array_merge($deltaAnimators['add'], $deltaAnimators['unchangedNew']);
            $deltaTrainees = $this->compareArrays($onHoldProviderSession->getParticipants()['trainees'], $data['participants']['trainees']);
            $addAndUnchangedNewDataTrainees = array_merge($deltaTrainees['add'], $deltaTrainees['unchangedNew']);
            $newDataParticipants = [
                'animators' => $addAndUnchangedNewDataAnimators,
                'trainees' => $addAndUnchangedNewDataTrainees,
            ];
            $onHoldProviderSession->setParticipants($newDataParticipants);
            $this->onHoldProviderSessionService->setParticipantsDiff($onHoldProviderSession);

            //Converti en UTC
            $dateSlotsdatesToUTC = $this->onHoldProviderSessionService->convertSlotsDateToUTC($data['slotsDates']);
            $deltaSlotsDates = $this->compareArrays($onHoldProviderSession->getSlotsDates(), $dateSlotsdatesToUTC);
            $newDataSlotsDates = array_merge($deltaSlotsDates['add'], $deltaSlotsDates['unchangedNew']);
            $this->onHoldProviderSessionService->updateSlotsDateToSessions($onHoldProviderSession, $newDataSlotsDates, $newDataParticipants);
            //Tri les slotsDates en Asc
            $sortSlotsDates = $this->onHoldProviderSessionService->compareDatesSort($newDataSlotsDates, 'Asc');
            $onHoldProviderSession->setSlotsDates($sortSlotsDates);
            $this->onHoldProviderSessionService->setSlotsDatesDiff($onHoldProviderSession);
            if ($this->onHoldProviderSessionService->checkSlotsDatesAllValidate($onHoldProviderSession)) {
                $onHoldProviderSession->setStatusValidation(OnHoldProviderSession::STATUS_VALIDATED);
            } else {
                $onHoldProviderSession->setStatusValidation(OnHoldProviderSession::STATUS_WAIT_FOR_VALIDATION);
            }

            $onHoldProviderSession->setUpdatedBy($this->getUser());
            $onHoldProviderSession->setUpdatedAt(new \DateTime());
            $onHoldProviderSession->setMegaUUID($data['megaUUID']);
            $onHoldProviderSession->setTitle($data['title']);
            $onHoldProviderSession->setConnectorType($data['connectorType']);
            $onHoldProviderSession->setNbrMinParticipants($data['nbrMinParticipants']);
            $onHoldProviderSession->setNbrMaxParticipants($data['nbrMaxParticipants']);

            $this->entityManager->persist($onHoldProviderSession);
            $this->entityManager->flush();

            //Converti au timezone du client pour la réponse du POST
            $dateSlotsdatesToTimezone = $this->onHoldProviderSessionService->convertSlotsDateToTimezone($onHoldProviderSession->getSlotsDates(), $client->getTimezone());
            $onHoldProviderSession->setSlotsDates($dateSlotsdatesToTimezone);
            $dataOnHoldProviderSession = $this->normalizer->normalize($onHoldProviderSession, null, ['groups' => [self::GROUP_VIEW]]);

            return $this->apiResponse(['data' => $dataOnHoldProviderSession, 'status' => 200]);
        } catch (\Throwable $e) {
            return $this->apiErrorResponse('An unknown error has occurred', extraData: $e->getMessage());
            //return $this->exceptionViewHandler($e);
        }
    }

    private function compareArrays($arrayOld, $arrayNew): array
    {
        // Compare les deux tableaux en utilisant la fonction de rappel personnalisée pour identifier les éléments uniques
        $entriesToRemove = array_udiff($arrayOld, $arrayNew, function ($a, $b) {
            return $a['megaId'] <=> $b['megaId'];
        });

        $entriesToAdd = array_udiff($arrayNew, $arrayOld, function ($a, $b) {
            return $a['megaId'] <=> $b['megaId'];
        });

        // Compare les deux tableaux en conservant les valeurs des tableaux de départ
        $unchangedEntriesOld = array_udiff_assoc($arrayOld, $entriesToRemove, function ($a, $b) {
            return $a['megaId'] <=> $b['megaId'];
        });

        $unchangedEntriesNew = array_udiff_assoc($arrayNew, $entriesToAdd, function ($a, $b) {
            return $a['megaId'] <=> $b['megaId'];
        });

        return ['remove' => $entriesToRemove, 'add' => $entriesToAdd, 'unchangedNew' => $unchangedEntriesOld];
    }

    #[View(serializerGroups: [self::GROUP_VIEW])]
    #[Get('/', name: '_api_on_hold_session_get_all')]
    #[OA\Response(
        response: 200,
        description: 'List all on hold sessions',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(
                ref: new Model(
                    type: OnHoldProviderSession::class,
                    groups: [self::GROUP_VIEW]
                )
            )
        )
    )]
    public function getAll(Request $request): Response
    {
        $client = $this->getUser()->getClient();
        $from = (!empty($request->query->get('from'))) ? $request->query->get('from') : null;
        $to = (!empty($request->query->get('to'))) ? $request->query->get('to') : null;

        $oHPSsConvertedDates = [];
        $allOHPSs = $this->onHoldProviderSessionRepository->searchAll($client, $from, $to);
        foreach ($allOHPSs as $onHoldProviderSession) {
            //Converti au timezone du client pour la réponse du POST
            $dateSlotsdatesToTimezone = $this->onHoldProviderSessionService->convertSlotsDateToTimezone($onHoldProviderSession->getSlotsDates(), $client->getTimezone());
            $onHoldProviderSession->setSlotsDates($dateSlotsdatesToTimezone);
            $oHPSsConvertedDates[] = $onHoldProviderSession;
        }

        $data = $this->normalizer->normalize($oHPSsConvertedDates, null, ['groups' => ['read_on_hold_session_list']]);

        return $this->apiResponse(['data' => $data, 'status' => 200]);
    }

    #[View(serializerGroups: [self::GROUP_VIEW, self::GROUP_VIEW_PRESENCES])]
    #[Get('/{id}', name: '_api_on_hold_session_get', requirements: ['id' => '\d+'])]
    //#[IsGranted(OnHoldProviderSessionVoter::ACCESS, subject: 'on_hold_session')]
    #[OA\Response(
        response: 200,
        description: 'Get a specified on hold providersession',
        content: new OA\JsonContent(
            ref: new Model(
                type: OnHoldProviderSession::class,
                groups: ['default', self::GROUP_VIEW, self::GROUP_VIEW_PRESENCES]
            ),
            type: 'object'
        )
    )]
    public function getOnHoldProviderSession(OnHoldProviderSession $onHoldProviderSession): Response
    {
        //Converti au timezone du client pour la réponse du POST
        $dateSlotsdatesToTimezone = $this->onHoldProviderSessionService->convertSlotsDateToTimezone($onHoldProviderSession->getSlotsDates(), $onHoldProviderSession->getClient()->getTimezone());
        $onHoldProviderSession->setSlotsDates($dateSlotsdatesToTimezone);
        $data = $this->normalizer->normalize($onHoldProviderSession, null, ['groups' => ['read_on_hold_session_list']]);

        return $this->apiResponse(['data' => $data, 'status' => 200]);
    }

    #[View(statusCode: Response::HTTP_NO_CONTENT)]
    #[Delete('/{id}', name: '_api_on_hold_session_delete', requirements: ['id' => '\d+'])]
    //#[IsGranted(OnHoldProviderSessionVoter::DELETE, subject: 'on_hold_session')]
    public function deleteOnHoldProviderSession(OnHoldProviderSession $onHoldProviderSession): Response
    {
        $this->onHoldProviderSessionService->deleteSession($onHoldProviderSession);
        $this->entityManager->flush();

        return $this->apiResponse(['data' => [], 'status' => 200]);
    }

    /*private function handle(Request $request, OnHoldProviderSession $session, string $group): OnHoldProviderSession
    {
        $normalize = $this->normalizer->normalize($session, null, ['groups' => [$group]]);
        $requestData = array_merge($normalize, $request->request->all());
        dd($requestData);

        $form = $this->createForm(OnHoldProviderSessionApiType::class, $session, ['group' => $group]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            dd('s');
        }

        $constraintsViolations = $this->validator->validate($session, groups: $group);

        if ($form->isSubmitted() && $constraintsViolations->count() > 0) {
            throw new MiddlewareFailureException(ProviderServiceValidationException::MESSAGE, $constraintsViolations);
        }

        return $session;
    }*/

    #[View(serializerGroups: [self::GROUP_VIEW])]
    #[Get('/presences/', name: '_api_on_hold_session_get_presences_all')]
    #[OA\Response(
        response: 200,
        description: 'List all presences sessions',
        content: new OA\JsonContent(
            type: 'array',
            items: new OA\Items(
                ref: new Model(
                    type: ProviderSession::class,
                    groups: [self::GROUP_VIEW]
                )
            )
        )
    )]
    public function getPresencesAll(Request $request): Response
    {
        $client = $this->getUser()->getClient();
        $from = (!empty($request->query->get('from'))) ? $request->query->get('from') : '2024-01-03T00:00:00';
        $to = (!empty($request->query->get('to'))) ? $request->query->get('to') : null;

        $dtiFrom = DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s', $from);
        if (!is_null($to)) {
            $dtiTo = DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s', $to);
        } else {
            $dateTo = new DateTimeImmutable();
            $dateTo = $dateTo->sub(new DateInterval('P1D'));
            $dtiTo = $dateTo->setTime(23, 59, 59);
        }

        $sessions = $this->providerSessionRepository->findAllBetweenDatesWithOrderByClientAndSessionsStart($dtiFrom, $dtiTo);
        $dataPresences = $this->getArrPresences($sessions);

        return $this->apiResponse(['data' => $dataPresences, 'status' => 200]);
    }

    #[View(serializerGroups: [self::GROUP_VIEW, self::GROUP_VIEW_PRESENCES])]
    #[Get('/presences/{id}', name: '_api_on_hold_session_get_presences_from_a_session', requirements: ['id' => '\d+'])]
    //#[IsGranted(OnHoldProviderSessionVoter::ACCESS, subject: 'on_hold_session')]
    #[OA\Response(
        response: 200,
        description: 'Get a specified presences from a session',
        content: new OA\JsonContent(
            ref: new Model(
                type: ProviderSession::class,
                groups: ['default', self::GROUP_VIEW, self::GROUP_VIEW_PRESENCES]
            ),
            type: 'object'
        )
    )]
    public function getPresencesFromASession(ProviderSession $providerSession): Response
    {
        $dataPresences = $this->getArrPresences([$providerSession])[$providerSession->getId()];

        return $this->apiResponse(['data' => $dataPresences, 'status' => 200]);
    }

    private function getArrPresences(array $sessions): array
    {
        $dataPresences = [];
        /*[
            '123' => [
                'sessionId' => '123',
                'sessionName' => 'My session name',
                'sessionReference' => 'CA123654789',
                'sessionDateStart' => '02/04/2024 09:00:00',
                'sessionDateEnd' => '02/04/2024 10:00:00',
                'sessionDuration' => '60',
                'sessionConnector' => 'adobe_connect_sco',
                'presences' => [
                    '456' => [
                        'ppsrId' => '456',
                        'participantRole' => 'animator',
                        'participantReference' => '123654',
                        'participantFirstName' => 'Alain',
                        'participantLastName' => 'Dissoir',
                        'participantEmail' => 'alain.dissoir@domainfake.com',
                        'participantPresenceDateStart' => '02/04/2024 08:55:20',
                        'participantPresenceDateEnd' => '02/04/2024 09:05:20',
                        'participantPresenceSlotPresenceDuration' => 70,
                        'participantPresenceStatus' => 1,
                        'participantPresenceManualPresenceStatus' => 0,
                        'participantPresenceSignatures' => [
                            '789' => [
                                'signatureForTheSessionOf' => '02/04/2024 09:00:00',
                                'dayPartIndicator' => 'AM',
                                'signatureBlob' => 'sdfsdfsdf14dsf2sdf25sdfs2d'
                            ]
                        ]
                    ]
                ]
            ]
        ]*/

        foreach ($sessions as $session) {
            if ($session instanceof ProviderSession) {
                foreach ($session->getParticipantRoles() as $participantRole) {
                    if (!array_key_exists($session->getId(), $dataPresences)) {
                        if ($session->getAbstractProviderConfigurationHolder() instanceof AdobeConnectConfigurationHolder) {
                            $sessionConnector = 'adobe_connect_sco';
                        } elseif ($session->getAbstractProviderConfigurationHolder() instanceof MicrosoftTeamsEventConfigurationHolder) {
                            $sessionConnector = 'microsoft_teams_event';
                        } else {
                            $sessionConnector = null;
                        }
                        $dataPresences[$session->getId()] = [
                            'sessionId' => $session->getId(),
                            'sessionName' => $session->getName(),
                            'sessionReference' => $session->getRef(),
                            'sessionDateStart' => $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i:s'),
                            'sessionDateEnd' => $session->getDateEnd()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i:s'),
                            'sessionDuration' => $session->getDuration(),
                            'sessionConnector' => $sessionConnector,
                            'sessionInfoMega' => $session->getDescription(),
                            'presences' => [],
                        ];
                    }

                    if (!array_key_exists($participantRole->getId(), $dataPresences[$session->getId()]['presences'])) {
                        $presenceDateStart = null;
                        $presenceDateEnd = null;
                        $slotPresenceDuration = 0;
                        if (!empty($participantRole->getPresenceDateStart())) {
                            $presenceDateStart = ($participantRole->getPresenceDateStart())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i:s');
                        }
                        if (!empty($participantRole->getPresenceDateEnd())) {
                            $presenceDateEnd = ($participantRole->getPresenceDateEnd())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i:s');
                        }
                        $dataPresences[$session->getId()]['presences'][$participantRole->getId()] = [
                            'ppsrId' => $participantRole->getId(),
                            'participantRole' => $participantRole->getRole(),
                            'participantReference' => $participantRole->getParticipant()->getUser()->getReference(),
                            'participantFirstName' => $participantRole->getParticipant()->getUser()->getFirstName(),
                            'participantLastName' => $participantRole->getParticipant()->getUser()->getLastName(),
                            'participantEmail' => $participantRole->getParticipant()->getUser()->getEmail(),
                            'participantPresenceDateStart' => $presenceDateStart,
                            'participantPresenceDateEnd' => $presenceDateEnd,
                            'participantPresenceSlotPresenceDuration' => $participantRole->getSlotPresenceDuration(),
                            'participantPresenceStatus' => $participantRole->getPresenceStatus(),
                            'participantPresenceManualPresenceStatus' => $participantRole->isManualPresenceStatus(),
                            'participantPresenceSignatures' => [],
                        ];
                        foreach ($participantRole->getSignatures() as $signature) {
                            $dataPresences[$session->getId()]['presences'][$participantRole->getId()]['participantPresenceSignatures'][$signature->getId()] = [
                                'signatureForTheSessionOf' => ($signature->getSignatureForTheSessionOf())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i:s'),
                                'dayPartIndicator' => $signature->getDayPartIndicator(),
                                'signatureBlob' => $signature->getSignature(),
                            ];
                        }
                    }
                }
            }
        }

        return $dataPresences;
    }
}
