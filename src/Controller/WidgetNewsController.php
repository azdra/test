<?php

namespace App\Controller;

use App\Entity\WidgetNews;
use App\Model\ApiResponseTrait;
use App\Repository\WidgetNewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/news')]
#[IsGranted('ROLE_MANAGER')]
class WidgetNewsController extends AbstractController
{
    use ApiResponseTrait;

    private WidgetNewsRepository $newsRepository;
    private EntityManagerInterface $entityManager;
    private TranslatorInterface $translator;
    private LoggerInterface $logger;

    public function __construct(WidgetNewsRepository $newsRepository, EntityManagerInterface $entityManager, TranslatorInterface $translator, LoggerInterface $logger)
    {
        $this->newsRepository = $newsRepository;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
        $this->logger = $logger;
    }

    #[Route('/', name: '_widget_news_index', options: ['expose' => true])]
    public function listNews(): Response
    {
        return $this->render('widgetNews/index.html.twig', [
            'title' => 'News',
            'news' => $this->newsRepository->findByLangs($this->getUser()->hasRole('ROLE_ADMIN') ? WidgetNews::LANGUAGES_LIST : [$this->getUser()->getPreferredLang()]),
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/create', name: '_widget_news_create', options: ['expose' => true], methods: ['GET'])]
    public function create(): Response
    {
        return $this->render('widgetNews/create_or_edit.html.twig', [
            'title' => 'Add a news',
            'type' => 'create',
            'widgetNews' => null,
        ]);
    }

    #[IsGranted('ROLE_ADMIN')]
    #[Route('/edit/{id}', name: '_widget_news_edit', options: ['expose' => true], methods: ['GET'])]
    public function edit(Request $request, WidgetNews $widgetNews): Response
    {
        return $this->render('widgetNews/create_or_edit.html.twig', [
            'title' => 'Edit a news',
            'type' => 'edit',
            'widgetNews' => $widgetNews,
        ]);
    }

    #[Route('/api/create_or_update/{id}', name: '_widget_news_api_create_or_edit', options: ['expose' => true], defaults: ['id' => null], methods: ['POST', 'PUT', 'PATCH'])]
    public function apiCreateOrUpdate(Request $request, WidgetNews $widgetNews = null): JsonResponse
    {
        $isCreated = $widgetNews === null;
        if (!$widgetNews) {
            $widgetNews = new WidgetNews($this->getUser());
        }

        try {
            $data = json_decode($request->getContent(), true);
            if (0 == count(array_diff(['title', 'color', 'languages', 'content'], array_keys($data)))) {
                $widgetNews
                    ->setTitle($data['title'])
                    ->setColor($data['color'])
                    ->setLanguages($data['languages'])
                    ->setContent($data['content']);
                if ($isCreated) {
                    $widgetNews->setDate(new \DateTimeImmutable());
                }
            } else {
                return $this->apiErrorResponse($this->translator->trans('An error has occurred in the action, please complete the fields.'));
            }

            $this->entityManager->persist($widgetNews);
            $this->entityManager->flush();

            $this->addFlash('success', 'A news has been added');

            return $this->apiResponse(['success' => true]);
        } catch (Throwable $t) {
            $this->logger->error($t, [
                'loggedUserId' => $this->getUser()->getId(),
                'message' => 'An unknown error occurred while added news in the widget.',
            ]);

            return $this->apiErrorResponse($this->translator->trans('An error has occurred in the action'));
        }
    }

    #[Route('/api/delete/{id}', name: '_widget_news_delete', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function deleteNewsWidget(WidgetNews $widgetNews): Response
    {
        $this->entityManager->remove($widgetNews);
        $this->entityManager->flush();

        $this->addFlash('success', 'A news widget has been deleted');

        return $this->apiResponse(['success' => true]);
    }
}
