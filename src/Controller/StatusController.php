<?php

namespace App\Controller;

use App\Service\StatusService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StatusController extends AbstractController
{
    public function __construct(
        private readonly StatusService $statusService,
        private readonly string $appVersion
    ) {
    }

    #[Route('/status_page', name: '_app_status')]
    public function status(Request $request): Response
    {
        return new JsonResponse([
            'app_version' => $this->appVersion,
            'bdd_status' => $this->statusService->bddStatus(),
            'redis_status' => $this->statusService->redisStatus(),
        ]);
    }
}
