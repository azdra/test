<?php

namespace App\Controller;

use App\Service\Exporter\ExporterService;
use App\Service\RessourceUploader\RessourceUploader;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/file')]
class FileController extends AbstractController
{
    public function __construct(
        private RessourceUploader $ressourceUploader,
        private TranslatorInterface $translator,
        private LoggerInterface $logger,
    ) {
    }

    #[Route('/file-upload', name: '_file_upload', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function fileUpload(Request $request): Response
    {
        $location = [];
        try {
            $uploadedFile = $request->files->get('file');
            $newFile = $this->ressourceUploader->uploadImage($uploadedFile);
            $path = $this->getParameter('client_convocations_images_pathweb').$newFile->getBasename();
            $location = ['location' => $path];
        } catch (\Exception $e) {
            $this->logger->error($e);
        }

        return new JsonResponse($location);
    }

    #[Route('/list-images', name: '_list_images', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function loadListImages(): Response
    {
        $list = [];

        try {
            $list = $this->ressourceUploader->listImagesConvocation();
        } catch (\Exception $e) {
            $this->logger->error($e);
        }

        return new JsonResponse($list);
    }

    #[Route('/download-archive/{token}', name: '_download_exports', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_TRAINEE')]
    public function downloadExport(string $token, ExporterService $exporterService): Response
    {
        $file = $exporterService->validateDownloadingToken($token);
        $filename = basename($file);

        $response = new BinaryFileResponse($file);
        $response->headers->set('Cache-Control', 'max-age=0');
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        $exporterService->generateActivityLogExport(user: $this->getUser(), filename: $filename);

        return $response;
    }

    #[Route('/download-archive-direct/{file}', name: '_download_exports_direct', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_MANAGER')]
    public function downloadExportDirect(string $file, ExporterService $exporterService): Response
    {
        //$file = $exporterService->validateDownloadingToken($token);
        $filename = basename($file);

        $response = new BinaryFileResponse('/srv/data/exports_savefiles/'.$file);
        $response->headers->set('Cache-Control', 'max-age=0');
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );
        //$exporterService->generateActivityLogExport(user: $this->getUser(), filename: $filename);

        return $response;
    }

    #[Route('/download-file-zoho-analytics', name: '_download_file_zoho_analytics', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_TRAINEE')]
    public function downloadFileZohoAnalytics(ExporterService $exporterService): Response
    {
        //TODO Une fois pousser en staging et en prod déclencher l'ouverture du workspace dans Zoho Analytics car il y a une vérification automatique qui retournera une 404 avant cela !
        /*
         * Type de fichier : Excel
         * URL : https://managerpp300.mylivesession.com/file/download-file-zoho-analytics | https://app.mylivesession.com/file/download-file-zoho-analytics
         */
        $filename = basename('export-session-zoho-data.xlsx');

        $response = new BinaryFileResponse('/srv/data/exports_savefiles/export-session-zoho-data.xlsx');
        $response->headers->set('Cache-Control', 'max-age=0');
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        return $response;
    }
}
