<?php

namespace App\Controller;

use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Exception\ProviderGenericException;
use App\Model\ApiResponseTrait;
use App\Security\ConfigurationHolderVoter;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/configuration-holder/webex-rest')]
#[IsGranted('ROLE_MANAGER')]
class WebexRestConfigurationHolderController extends AbstractController
{
    use ApiResponseTrait;

    #[Route('/{id}/licences', name: '_webex_rest_licences', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ConfigurationHolderVoter::EDIT, subject: 'configurationHolder')]
    public function getWebexRestLicences(
        WebexRestConfigurationHolder $configurationHolder,
        LoggerInterface $logger,
    ): Response {
        try {
            return $this->apiResponse($configurationHolder->getLicences());
        } catch (ProviderGenericException $exception) {
            $logger->error($exception, [
                'id' => $configurationHolder->getId(),
                'type' => 'WebexRestConfigurationHolder',
            ]);

            return $this->apiErrorResponse('Unable to get the meeting model list', extraData: $exception->getMessage());
        }
    }
}
