<?php

namespace App\Controller;

use App\Entity\ActivityLog;
use App\Entity\Bounce;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Event\UserPasswordEvent;
use App\Exception\InvalidTemplateUploadedForImportException;
use App\Exception\MissingClientParameter;
use App\Exception\UploadFile\UploadFileException;
use App\Form\UpdateUserPasswordFormType;
use App\Form\UserAdminFormType;
use App\Form\UserParticipantFormType;
use App\Form\UserProfileFormType;
use App\Model\ActivityLog\ActivityLogTrait;
use App\Model\UpdateUserPasswordModel;
use App\Repository\BounceRepository;
use App\Repository\ClientRepository;
use App\Repository\ProviderConfigurationHolderRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Security\UserSecurityService;
use App\Security\UserVoter;
use App\Service\ActivityLogger;
use App\Service\Exporter\ExporterService;
use App\Service\ExportUserService;
use App\Service\ImportUserService;
use App\Service\ProviderSessionService;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/user')]
#[IsGranted('ROLE_TRAINEE')]
class UserController extends AbstractController
{
    use ActivityLogTrait;

    public function __construct(
        private UserSecurityService $userSecurityService,
        private UserService $userService,
        private EntityManagerInterface $entityManager,
        private SerializerInterface $serializer,
        private ActivityLogger $activityLogger,
        private ImportUserService $importUserService,
        private EventDispatcherInterface $dispatcher,
        private TranslatorInterface $translator,
        private UserRepository $userRepository
    ) {
    }

    #[Route('/password/update', name: '_password_update')]
    #[IsGranted('ROLE_TRAINEE')]
    public function updatePassword(Request $request): Response
    {
        $model = new UpdateUserPasswordModel();
        $form = $this->createForm(UpdateUserPasswordFormType::class, $model);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            /** @var User $user */
            $user = $this->getUser();

            if ($form->isValid()) {
                $this->userSecurityService->updatePassword($user, $model->getPassword());
                $this->entityManager->flush();

                $this->addFlash('success', 'Your password has been updated.');
            }
        }

        return $this->render('users/update_password_action.html.twig', [
            'title' => 'Reset your password',
            'form' => $form->createView(),
        ]);
    }

    #[Route('/list', name: '_user_list')]
    #[IsGranted('ROLE_ADMIN')]
    public function listUsers(UserRepository $userRepository): Response
    {
        return $this->render('users/list.html.twig', [
            'title' => 'Administrators',
            'users' => $userRepository->findListByRolesAdministrators(),
        ]);
    }

    #[Route('/create', name: '_user_create', methods: ['POST'])]
    #[Route('/create/participant', name: '_participant_create', methods: ['POST'])]
    #[Route('/create/admin', name: '_admin_create', methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function create(Request $request, UserService $userService, LoggerInterface $logger): Response
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['email' => $request->request->all('user_participant_form')['email'] ?? '', 'client' => $this->getUser()->getClient(), 'status' => User::STATUS_INACTIVE]);
        if (!empty($user)) {
            $user->setStatus(User::STATUS_ACTIVE);

            return $this->forward(UserController::class.'::edit', ['user' => $user]);
        }
        $user = $userService->createUser();

        $form = $this->createForm(UserParticipantFormType::class, $user, [
            'label' => false,
            'method' => 'POST',
            'validation_groups' => 'create',
        ])->handleRequest($request);

        $this->activityLogger->initActivityLogContext($user->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                try {
                    $this->entityManager->persist($user);
                    $this->entityManager->flush();
                    $this->activityLogger->flushActivityLogs();

                    $this->addFlash('success', 'User has been successfully created');
                } catch (\Exception $e) {
                    $logger->error($e);
                    if ($e->getCode() === 1062) {
                        // Duplicate entry : check if it's a duplicate email
                        $this->addFlash('error', 'An error has occurred, the user already exists');
                    } else {
                        $this->addFlash('error', 'An error has occurred, the user has not been  not created');
                    }
                } finally {
                    return $this->redirectToRoute('_participant_list');
                }
            } else {
                $this->addFlash('error', 'Some fields are not valid');

                return $this->render('users/update_user.html.twig', [
                    'form' => $form->createView(),
                    'creationMode' => true,
                ]);
            }
        }

        return $this->render('participants/form_user_participant.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/create/new-admin', name: '_admin_create_new', methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function createNewAdmin(Request $request, UserService $userService, LoggerInterface $logger): Response
    {
        $user = $userService->createUser();

        $form = $this->createForm(UserAdminFormType::class, $user, [
            'label' => false,
        ]);
        $form->handleRequest($request);

        $this->activityLogger->initActivityLogContext($user->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid() && $userService->isValidMail($user)) {
                $entityManager = $this->entityManager;
                try {
                    $user->setRoles(['ROLE_ADMIN']);
                    $isCloned = $this->userSecurityService->createOrClonePassword($user);
                    $entityManager->persist($user);
                    $entityManager->flush();

                    if (!$isCloned) {
                        $eventName = UserPasswordEvent::NEW;
                    } else {
                        $eventName = UserPasswordEvent::CLONE;
                    }

                    $this->dispatcher->dispatch(new UserPasswordEvent($user), $eventName);
                    $this->activityLogger->flushActivityLogs();

                    $this->addFlash('success', 'Super Admin has been successfully created');
                } catch (\Exception $e) {
                    $logger->error($e);
                    $this->addFlash('error', 'An error has occurred, the user has not been  not created');
                } finally {
                    return $this->redirectToRoute('_user_list');
                }
            } else {
                $logger->error(new \Exception('An error has occurred, the user has not been  not created'));
                $this->addFlash('error', 'An error has occurred, the user has not been  not created');

                return $this->redirectToRoute('_user_list');
            }
        }

        return $this->render('users/form_user_account_admin.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/delete/{id}', name: '_user_delete', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted(UserVoter::EDIT, subject: 'user')]
    public function delete(Request $request, User $user, ProviderSessionService $providerSessionService, LoggerInterface $logger): Response
    {
        $referer = $request->headers->get('referer');

        $userUsedLikeLicence = $providerSessionService->findLicenceUsedInSessions($user->getEmail());

        if ($userUsedLikeLicence > 0) {
            $this->addFlash('error', $this->translator->trans('The user cannot be deleted because it is used as a license in %nbrSessions% session.s', ['%nbrSessions%' => $userUsedLikeLicence]));

            return $this->redirect(
                $referer ?? $this->generateUrl('_user_list')
            );
        }

        if ($user === $this->getUser()) {
            $this->addFlash('error', 'An error has occurred, you are trying to delete your own account');

            return $this->redirect(
                $referer ?? $this->generateUrl('_user_list')
            );
        }

        $this->activityLogger->initActivityLogContext($user->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
        try {
            $this->entityManager->remove($user);
            $this->entityManager->flush();

            $this->activityLogger->flushActivityLogs();

            $this->addFlash('success', 'User has been successfully deleted');
        } catch (\Exception $e) {
            $logger->error($e);
            $this->addFlash('error', 'An error has occurred, the user has not been deleted');
        }

        if (null !== $referer && strpos($referer, 'client/edit') && !strpos($referer, '?tab-name=')) {
            $referer .= '?tab-name=clientAccountManagerListTab';
        }

        return $this->redirect(
            $referer ?? $this->generateUrl('_user_list')
        );
    }

    #[Route('/edit/{id}', name: '_user_edit', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[Route('/edit/participant/{id}', name: '_participant_edit', options: ['expose' => true])]
    #[Route('/edit/admin/{id}', name: '_admin_edit', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[Route('/edit/user/{user}/bounce/{bounce}', name: '_user_bounce_edit', options: ['expose' => true])]
    #[IsGranted(UserVoter::EDIT, subject: 'user')]
    public function edit(
        Request $request,
        ProviderSessionRepository $sessionRepository,
        BounceRepository $bounceRepository,
        ProviderParticipantSessionRoleRepository $roleRepository,
        LoggerInterface $logger,
        User $user,
        UserService $userService,
        ?Bounce $bounce = null
    ): Response {
        $form = $this->createForm(UserParticipantFormType::class, $user, [
            'label' => false,
            'update_role' => in_array($request->attributes->get('_route'), ['_admin_edit', '_user_edit']),
        ]);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid() && $userService->isValidMail($user)) {
                $isUpdatedEmail = $this->userService->isUpdatedEmail($user);
                if ($isUpdatedEmail) {
                    $this->userService->updateSessionsOldEmail($roleRepository->getDataForUser($user), $user);
                }
                $userParticipant = $form->getData();
                $this->activityLogger->initActivityLogContext($user->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
                try {
                    $this->entityManager->persist($userParticipant);

                    if (!empty($bounce)) {
                        $bounce->setState(Bounce::CORRECTED);
                        $this->entityManager->persist($bounce);
                        $bounceRepository->markAsProcessedForUser($user);
                    }

                    $this->entityManager->flush();

                    $this->activityLogger->flushActivityLogs();
                    if ($isUpdatedEmail) {
                        $this->addFlash('success', $this->translator->trans('The email address of the participant %participant_name% has been changed. If he is registered for a session in "Sent" status, the invitation emails will automatically go out', ['%participant_name%' => $user->getFullName()]));
                    } else {
                        $this->addFlash('success', $this->translator->trans('The %participant_name% profile has been modified.', ['%participant_name%' => $user->getFullName()]));
                    }
                } catch (\Exception|\Throwable $e) {
                    $logger->error($e);
                    $this->addFlash('error', 'An error has occurred, the user has not been updated');
                } finally {
                    return $this->redirect($request->headers->get('referer'));
                }
            } else {
                $logger->error(new \Exception('An error has occurred, the user has not been updated'));
                $this->addFlash('error', 'An error has occurred, the user has not been updated');
            }
        }

        $microsoftTeamsEventConfigurationHoldersActive = [];
        foreach ($user->getClient()->getMicrosoftTeamsEventConfigurationHolders()->getValues() as $chmte) {
            if (!$chmte->isArchived()) {
                $microsoftTeamsEventConfigurationHoldersActive[] = $chmte;
            }
        }

        return $this->render('users/update_user.html.twig', [
            'adobeConfigurationHolders' => $user->getClient()->getAdobeConfigurationHolders()->getValues(),
            'webexConfigurationHolders' => $user->getClient()->getWebexConfigurationHolders()->getValues(),
            'webexRestConfigurationHolders' => $user->getClient()->getWebexRestConfigurationHolders()->getValues(),
            'microsoftTeamsConfigurationHolders' => $user->getClient()->getMicrosoftTeamsConfigurationHolders()->getValues(),
            'microsoftTeamsEventConfigurationHolders' => $microsoftTeamsEventConfigurationHoldersActive,
            'whiteConfigurationHolders' => $user->getClient()->getWhiteConfigurationHolders()->getValues(),
            'form' => $form->createView(),
            'clients' => [$user->getClient()],
            'sessionRoleMapping' => $roleRepository->getDataForUser($user),
            'bouncedMails' => $bounceRepository->findBy(['state' => Bounce::TO_CORRECT, 'user' => $user]),
        ]);
    }

    #[Route('/{id}/session-search', name: '_user_session_search', methods: ['GET'], format: 'json')]
    #[IsGranted(UserVoter::EDIT, subject: 'user')]
    public function sessionSearch(Request $request, User $user, ProviderSessionRepository $providerSessionRepository): Response
    {
        $data['sessions'] = $providerSessionRepository->globalSearch(
            query: (string) $request->query->get('q'),
            allowOutdated: $request->query->getBoolean('allow-outdated', false),
            user: $user,
            configurationHolderId: $request->query->getInt('configuration-holder'),
            firstResult: $request->query->getInt('first-result', 0),
            countResult: ProviderSession::MAX_RESULT_LIST,
            filtersSearch: json_decode($request->query->get('filters-search'))
        );

        if ($request->query->getInt('need-count', 0)) {
            $data['count'] = $providerSessionRepository->countGlobalSearchResult(
                query: (string) $request->query->get('q'),
                allowOutdated: $request->query->getBoolean('allow-outdated', false),
                user: $user,
                configurationHolderId: $request->query->getInt('configuration-holder'),
                filtersSearch: json_decode($request->query->get('filters-search'))
            );
        }

        return new Response(
            $this->serializer->serialize(
                $data,
                'json',
                ['groups' => 'read_session_list'],
            )
        );
    }

    #[Route('/global-search', name: '_user_global_search', methods: ['GET'], format: 'json')]
    #[IsGranted('ROLE_MANAGER')]
    public function globalSearch(Request $request, UserRepository $userRepository, ClientRepository $clientRepository): Response
    {
        $context = $request->query->get('context') ?? 'default';

        $client = null;
        if ($request->query->has('client')) {
            $client = $clientRepository->find(
                $request->query->get('client')
            );
        }

        $users = match ($context) {
//            'participant' => $userRepository->globalSearchByRoleTarget((string) $request->query->get('q'), User::ROLE_TRAINEE),
            'administrator' => $userRepository->globalSearch((string) $request->query->get('q'), true),
            'manager' => $userRepository->globalSearchByRoleTarget((string) $request->query->get('q'), User::ROLE_MANAGER, $client),
            default => $userRepository->globalSearch((string) $request->query->get('q'))
        };

        return new Response(
            $this->serializer->serialize(
                $users,
                'json',
                ['groups' => 'read_user'],
            )
        );
    }

    #[Route('/profile', name: '_user_profile')]
    #[IsGranted('ROLE_TRAINEE')]
    public function profileUpdate(Request $request, SluggerInterface $slugger, UserRepository $userRepository, LoggerInterface $logger): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $activeUser = $this->userService->getUserProfil($userRepository->getActiveUserByEmail($user->getEmail()));
        $preferredInitialRoute = [
            'Dashboard' => '_dashboard_index',
            'Sessions' => '_session_viewer',
            'Participant' => '_participant_list',
            'Activity log' => '_activity_list',
            'Invalid emails' => '_bounce_list',
            'Statistics - usage' => '_statistics',
            'Statistics - evaluation' => '_sessions_evaluations_home',
        ];
        if ($this->isGranted('ROLE_ADMIN')) {
            $preferredInitialRoute['Admin'] = '_user_list';
        }
        if ($user->getClient()->getLicensesAvailable()->isEnabled()) {
            $preferredInitialRoute['Licences'] = '_licenses_available';
        }
        $form = $this->createForm(UserProfileFormType::class, $user, ['activeClient' => $activeUser?->getClient(), 'preferredInitialRoute' => $preferredInitialRoute]);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $profilePictureFile = $form->get('profilePicture')->getData();
                $defaultClient = $form->get('defaultClient')->getData();

                if ($defaultClient->getId() !== $user->getClient()->getId()) {
                    $this->userService->changeDefaultClient($user, $defaultClient);
                    $this->addFlash('success', 'The default account has changed, please login to your account.');
                }

                if ($profilePictureFile) {
                    $originalFilename = pathinfo($profilePictureFile->getClientOriginalName(), PATHINFO_FILENAME);
                    $safeFilename = $slugger->slug($originalFilename);
                    $newFilename = $safeFilename.'-'.uniqid().'.'.$profilePictureFile->guessExtension();

                    try {
                        $profilePictureFile->move(
                            $this->getParameter('profile_pictures_directory'),
                            $newFilename
                        );

                        $user->setProfilePictureFilename($newFilename);
                        $profilePictureFile = null;
                    } catch (FileException $exception) {
                        $logger->error($exception, [
                            'User Email' => $user->getEmail(),
                        ]);
                        $this->addFlash('error', 'An unknown error occurred while saving the new profile picture.');
                    }
                }

                if ($profilePictureFile === null) {
                    $this->addFlash('success', 'The changes have been well implemented.');
                    $this->entityManager->flush();

                    // In case user changed its language preferences, update the session.
                    // Then, redirect to route instead of just rendering the template => translator service has already
                    // set its locale when resolving the controller's route.
                    $request->getSession()->set('_locale', $user->getPreferredLang());

                    return $this->redirectToRoute('_user_profile');
                }
            }
        }

        return $this->render('users/profile.html.twig', [
            'title' => 'My profile',
            'form' => $form->createView(),
            'preferredInitialRoute' => $preferredInitialRoute,
        ]);
    }

    #[Route('/preferences-profile', name: '_user_preferences_profile', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_TRAINEE')]
    public function preferencesProfile(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!empty($request->request->get('preferencesProfil'))) {
            $choices = json_decode($request->request->get('preferencesProfil'), true);
            $user->setPreferences($choices);
            $this->entityManager->persist($user);
            $this->addFlash('success', 'The changes have been well implemented.');
            $this->entityManager->flush();
        }

        return $this->render('users/preferences_profile.html.twig', [
            'title' => 'Notifications emails',
            'user' => $user,
        ]);
    }

    #[Route('/remove-profile-picture', name: '_user_profile_remove_picture')]
    #[IsGranted('ROLE_TRAINEE')]
    public function removeProfilePicture(): Response
    {
        $this->getUser()->setProfilePictureFilename(null);

        $this->entityManager->flush();
        $this->addFlash('success', 'The changes have been well implemented.');

        return $this->redirectToRoute('_user_profile');
    }

    #[Route('/import-file-users', name: '_users_import_file', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function importFileUsers(Request $request, LoggerInterface $logger): Response
    {
        try {
            $this->importUserService->createImportAsyncTasksFromRequest($request);
            $this->entityManager->flush();

            $response = $this->translator->trans('The file has been received. It may take a few seconds or several minutes to process, depending on the number of lines it contains. As soon as it is finished, you will receive the import report by email.');
            $this->addFlash('success', $response);
            $code = Response::HTTP_OK;
        } catch (UploadFileException|MissingClientParameter|InvalidTemplateUploadedForImportException $e) {
            $logger->error($e);
            $response = $e->getMessage();
            $code = Response::HTTP_BAD_REQUEST;
        } catch (Throwable $e) {
            $logger->error($e);
            $response = $this->translator->trans('The file can not be imported for an unknown error');
            $code = Response::HTTP_BAD_REQUEST;
        }

        return new Response($response, $code);
    }

    #[Route('/set-is-licence-webex', name: '_user_webex_is_licence', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function setIsLicenceWebex(Request $request, ProviderConfigurationHolderRepository $providerConfigurationHolderRepository, EntityManagerInterface $entityManager): Response
    {
        $webexLicences = json_decode(
            $request->request->get('webexLicences')
        );

        $email = $request->request->get('email');

        foreach ($webexLicences as $webexLicence) {
            $configHolder = $providerConfigurationHolderRepository->find(
                $webexLicence->webexConfigHolder
            );

            if ($configHolder instanceof WebexConfigurationHolder) {
                /* @var WebexConfigurationHolder $configHolder */
                if (true === $webexLicence->checkedAsLicence) {
                    $configHolder->addLicence($email);
                } else {
                    $configHolder->removeLicence($email);
                }
            }
        }

        $this->addFlash('success', 'Updated Webex licenses');

        $entityManager->flush();

        return new Response();
    }

    #[Route('/set-is-licence-webex-rest', name: '_user_webex_rest_is_licence', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function setIsLicenceWebexRest(Request $request, ProviderConfigurationHolderRepository $providerConfigurationHolderRepository, EntityManagerInterface $entityManager): Response
    {
        $webexRestLicences = json_decode(
            $request->request->get('webexRestLicences')
        );

        $email = $request->request->get('email');

        foreach ($webexRestLicences as $webexRestLicence) {
            $configHolder = $providerConfigurationHolderRepository->find(
                $webexRestLicence->webexRestConfigHolder
            );

            if ($configHolder instanceof WebexRestConfigurationHolder) {
                /* @var WebexRestConfigurationHolder $configHolder */
                if (true === $webexRestLicence->checkedAsLicence) {
                    $configHolder->addLicence($email);
                } else {
                    $configHolder->removeLicence($email);
                }
            }
        }

        $this->addFlash('success', 'Updated Webex licenses');

        $entityManager->flush();

        return new Response();
    }

    #[Route('/set-is-licence-microsoft-teams', name: '_user_microsoft_teams_is_licence', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function setIsLicenceMicrosoftTeams(Request $request, ProviderConfigurationHolderRepository $providerConfigurationHolderRepository, EntityManagerInterface $entityManager): Response
    {
        $microsoftTeamsLicences = json_decode(
            $request->request->get('microsoftTeamsLicences'), true
        );

        foreach ($microsoftTeamsLicences as $microsoftTeamsLicence) {
            $configHolder = $providerConfigurationHolderRepository->find(
                $microsoftTeamsLicence['id']
            );
            if ($configHolder instanceof MicrosoftTeamsConfigurationHolder) {
                $configHolder->setLicences($microsoftTeamsLicence['licences']);
            }
        }

        $this->addFlash('success', 'Updating Microsoft Teams licenses');

        $entityManager->flush();

        return new Response();
    }

    #[Route('/set-is-licence-microsoft-teams-event', name: '_user_microsoft_teams_event_is_licence', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function setIsLicenceMicrosoftTeamsEvent(Request $request, ProviderConfigurationHolderRepository $providerConfigurationHolderRepository, EntityManagerInterface $entityManager): Response
    {
        $microsoftTeamsEventLicences = json_decode(
            $request->request->get('microsoftTeamsEventLicences'), true
        );

        foreach ($microsoftTeamsEventLicences as $microsoftTeamsEventLicence) {
            $configHolder = $providerConfigurationHolderRepository->find(
                $microsoftTeamsEventLicence['id']
            );
            if ($configHolder instanceof MicrosoftTeamsEventConfigurationHolder) {
                $configHolder->setLicences($microsoftTeamsEventLicence['licences']);
            }
        }

        $this->addFlash('success', 'Updating Microsoft Teams Event licenses');

        $entityManager->flush();

        return new Response();
    }

    #[Route('/set-is-licence-adobe', name: '_user_adobe_is_licence', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function setIsLicenceAdobe(Request $request, ProviderConfigurationHolderRepository $providerConfigurationHolderRepository, EntityManagerInterface $entityManager): Response
    {
        $adobeLicences = json_decode(
            $request->request->get('adobeLicences'), true
        );

        foreach ($adobeLicences as $adobeLicence) {
            $configHolder = $providerConfigurationHolderRepository->find($adobeLicence['id']);
            if ($configHolder instanceof AdobeConnectConfigurationHolder) {
                $configHolder->setLicences($adobeLicence['licences']);
            }
        }

        $this->addFlash('success', 'Adobe Connect License Update');

        $entityManager->flush();

        return new Response();
    }

    #[Route('/export/rgpd-datas/{id}', name: '_user_rdpg_data_export', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(UserVoter::EDIT, subject: 'user')]
    public function exportRGPDDatas(Request $request, ExporterService $exporterService, User $user, ExportUserService $exportUserService, UserRepository $userRepository): Response
    {
        $exportUserService->createAsyncTasks($user, $this->getUser());
        $this->entityManager->flush();

        $this->addFlash('success', 'The request to export the user\'s personal data has been processed. This process may take a few seconds or several minutes depending on the number of lines it contains. As soon as it is finished, you will receive an email allowing you to download the file.');

        return $this->redirectToRoute('_participant_list');
    }
}
