<?php

namespace App\Controller;

use App\Entity\ActivityLog;
use App\Entity\Client\Client;
use App\Entity\Client\ClientServices;
use App\Entity\Client\CreditAllocation;
use App\Entity\Client\CreditAllocationParticipants;
use App\Entity\Client\CreditAllocationSms;
use App\Entity\Client\EvaluationModalMail;
use App\Entity\SessionConvocationAttachment;
use App\Entity\User;
use App\Event\UserPasswordEvent;
use App\Exception\ConfigurationHolderViolationsException;
use App\Form\Client\ClientFormType;
use App\Form\Client\Tab\ClientCustomizationTabFormType;
use App\Form\Client\Tab\ClientEmailTabFormType;
use App\Form\Client\Tab\ClientGeneralTabFormType;
use App\Form\Client\Tab\ClientOptionsTabFormType;
use App\Form\Client\Tab\ClientOrganizationTabFormType;
use App\Form\UserAccountManagerFormType;
use App\Model\ActivityLog\ActivityLogTrait;
use App\Model\ApiResponseTrait;
use App\Model\ValidatorTrait;
use App\Repository\ClientRepository;
use App\Repository\EvaluationRepository;
use App\Repository\UserRepository;
use App\Security\ClientVoter;
use App\Security\UserSecurityService;
use App\Service\ActivityLogger;
use App\Service\ClientService;
use App\Service\ConfigurationHolderFormHandler\ConfigurationHolderRequestHandler;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use App\Service\UserService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/client')]
class ClientController extends AbstractController
{
    use ApiResponseTrait;
    use ActivityLogTrait;
    use ValidatorTrait;

    public function __construct(
        private UserSecurityService $userSecurityService,
        private EntityManagerInterface $entityManager,
        private SerializerInterface $serializer,
        private AdobeConnectConnector $adobeConnectConnector,
        private ClientService $clientService,
        private UserService $userService,
        private EventDispatcherInterface $dispatcher,
        private ActivityLogger $activityLogger,
        private ValidatorInterface $validator,
        private LoggerInterface $logger,
        private TranslatorInterface $translator
    ) {
    }

    #[Route('/', name: '_client_list')]
    #[IsGranted('ROLE_HOTLINE_CUSTOMER_SERVICE')]
    public function listClients(ClientRepository $clientRepository): Response
    {
        return $this->render('clients/list.html.twig', [
            'title' => 'Accounts',
            'clients' => $clientRepository->findBy(['active' => 1]),
        ]);
    }

    #[Route('/create', name: '_client_create')]
    #[IsGranted('ROLE_ADMIN')]
    public function create(
        Request $request,
    ): Response {
        $client = new Client();
        $form = $this->createForm(ClientFormType::class, options: ['client' => $client]);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->entityManager->persist($client);
                $this->clientService->createEvaluationModal($client);
                $this->entityManager->flush();

                $this->generateActivityLog($client, $this->getUser(), ActivityLog::SEVERITY_INFORMATION, ActivityLog::ACTION_CLIENT_CREATION, ['message' => ['key' => 'Account created', 'params' => []]]);

                $client->cleanUploadedFile();

                $this->addFlash('success', 'Account created');

                return $this->redirectToRoute('_client_edit', ['id' => $client->getId()]);
            }
        }

        return $this->render('clients/form.html.twig', [
            'title' => 'Create an account',
            'form' => $form->createView(),
        ]);
    }

    #[Route('/delete/{id}', name: '_client_delete', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ClientVoter::EDIT, 'client')]
    public function delete(Client $client): Response
    {
        // TODO: Do something with this.
        return $this->redirectToRoute('_client_list');
    }

    #[Route('/archive/{id}', name: '_client_archive', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ClientVoter::EDIT, 'client')]
    public function archive(Client $client): Response
    {
        $client->setActive(false);
        $this->entityManager->flush();
        $this->addFlash('success', 'Account has been archived');

        return $this->redirectToRoute('_client_list');
    }

    #[Route('/activate/{id}', name: '_client_activate', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ClientVoter::EDIT, 'client')]
    public function activate(Client $client): Response
    {
        $client->setActive(true);
        $this->entityManager->flush();
        $this->addFlash('success', 'Account has been activated');

        return $this->redirectToRoute('_client_edit', ['id' => $client->getId()]);
    }

    #[Route('/edit/{id}', name: '_client_edit', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted(ClientVoter::ACCESS, 'client')]
    public function edit(
        Request $request,
        Client $client,
        UserRepository $userRepository,
        EvaluationRepository $evaluationRepository
    ): Response {
        $readonly = !$this->isGranted(User::ROLE_ADMIN);

        $activeTabName = $request->query->get('tab-name', ClientGeneralTabFormType::getName());

        $listTab = [
            ClientGeneralTabFormType::getName(),
            ClientOrganizationTabFormType::getName(),
            'clientServiceTab',
            'clientConnectorTab',
            'clientAccountManagerListTab',
            ClientOptionsTabFormType::getName(),
            ClientCustomizationTabFormType::getName(),
            ClientEmailTabFormType::getName(),
            'clientEvaluationsTab',
            'clientBillingTab',
        ];

        $formTabs = [
            ClientGeneralTabFormType::getName() => $this->createForm(ClientGeneralTabFormType::class, $client, ['disabled' => $readonly]),
            ClientOrganizationTabFormType::getName() => $this->createForm(ClientOrganizationTabFormType::class, $client, ['disabled' => $readonly]),
            //ClientServicesTabFormType::getName() => $this->createForm(ClientServicesTabFormType::class, ($client->getServices() ?? new ClientServices()), ['disabled' => $readonly]),
            ClientOptionsTabFormType::getName() => $this->createForm(ClientOptionsTabFormType::class, $client, ['disabled' => $readonly]),
            ClientCustomizationTabFormType::getName() => $this->createForm(ClientCustomizationTabFormType::class, $client, ['disabled' => $readonly]),
            ClientEmailTabFormType::getName() => $this->createForm(ClientEmailTabFormType::class, $client, ['disabled' => $readonly]),
        ];

        if ($client->getConfigurationHolders()->isEmpty()) {
            $this->addFlash('warning', 'No connector is configured.');
        }

        if (!in_array($activeTabName, $listTab)) {
            throw new \LogicException('Unrecognized tab name');
        }

        if (array_key_exists($activeTabName, $formTabs)) {
            /** @var FormInterface $form */
            $form = $formTabs[$activeTabName];
        }

        if (!empty($form) && $request->isMethod('POST') && !$readonly) {
            $form->handleRequest($request);

            if ($form->isValid() && $form->isSubmitted()) {
                $this->entityManager->flush();

                $this->generateActivityLog($client, $this->getUser(), ActivityLog::SEVERITY_INFORMATION, ActivityLog::ACTION_CLIENT_UPDATED, ['message' => ['key' => 'Account updated', 'params' => []]]);

                $this->addFlash('success', 'The changes have been saved.');
            }

            if ($activeTabName === ClientCustomizationTabFormType::getName()) {
                $client->cleanUploadedFile();
            }
        }

        return $this->render('clients/form_tab.html.twig', [
            'title' => 'Account',
            'forms' => array_map(fn ($form) => $form->createView(), $formTabs),
            'activeTab' => $activeTabName,
            'client' => $client,
            'convocations' => $client->getConvocations(),
            'evaluationsModalMail' => $client->getEvaluationsModalMail(),
            'clientServices' => $client->getServices(),
            'users' => $userRepository->findManagerListByClient($client),
            'readonly' => $readonly,
            'surveys' => $evaluationRepository->findBy(['client' => $client]),
        ]);
    }

    #[Route('/edit/{id}/create-account-manager', name: '_client_edit_account_manager_create')]
    #[IsGranted(ClientVoter::ACCESS, 'client')]
    public function createAccountManager(Request $request, Client $client): Response
    {
        $user = $this->userService->createUser([User::ROLE_MANAGER]);

        $form = $this->createForm(UserAccountManagerFormType::class, $user, [
            'client' => $client,
            'label' => false,
        ]);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->entityManager;

                $formSelectedClient = $form->get('client')->getData();
                $user->setClient($formSelectedClient);

                try {
                    $isCloned = $this->userSecurityService->createOrClonePassword($user);

                    $entityManager->persist($user);
                    $entityManager->flush();

                    if (!$isCloned) {
                        $eventName = UserPasswordEvent::NEW;
                    } else {
                        $eventName = UserPasswordEvent::CLONE;
                    }

                    $this->dispatcher->dispatch(new UserPasswordEvent($user), $eventName);

                    $this->addFlash('success', 'User has been successfully created');
                } catch (\Exception $e) {
                    $this->logger->error($e, [
                        'client' => ['id' => $client->getId(), 'name' => $client->getName()],
                    ]);
                    $this->addFlash('error', 'An error has occurred, the user has not been  not created');
                } finally {
                    return $this->redirectToRoute('_client_edit', ['id' => $client->getId(), 'tab-name' => 'clientAccountManagerListTab']);
                }
            }

            return $this->render('users/update_user.html.twig', [
                'title' => 'User profile',
                'form' => $form->createView(),
                'creationMode' => true,
            ]);
        }

        return $this->render('users/form_user_account_manager.html.twig', [
            'form' => $form->createView(),
            'client' => $client,
        ]);
    }

    #[Route('/global-search', name: '_client_global_search', methods: ['GET'], format: 'json')]
    #[IsGranted('ROLE_ADMIN')]
    public function globalSearch(Request $request, ClientRepository $clientRepository): Response
    {
        return new Response(
            $this->serializer->serialize(
                $clientRepository->globalSearch((string) $request->query->get('q')),
                'json',
                ['groups' => 'read_client'],
            )
        );
    }

    /**
     * This function Create, Update And Delete configuration holder of a client from a request.
     */
    #[Route('/{id}/update-client-provider-configuration-holders', name: '_client_update_configuration_holders', options: ['expose' => true], methods: ['POST'], format: 'json')]
    #[IsGranted(ClientVoter::EDIT, 'client')]
    public function updateClientProviderConfigurationHolders(
        Request $request,
        Client $client,
        ConfigurationHolderRequestHandler $requestHandler
    ): Response {
        try {
            $requestHandler->handleRequest($client, $request);
        } catch (ConfigurationHolderViolationsException $exception) {
            $this->logger->error($exception, [
                'client' => ['id' => $client->getId(), 'name' => $client->getName()],
            ]);

            return $this->apiErrorResponse('Invalid provider configuration', extraData: $exception->violations);
        } catch (Throwable $exception) {
            $this->logger->error($exception, [
                'client' => ['id' => $client->getId(), 'name' => $client->getName()],
            ]);

            return $this->apiErrorResponse('Invalid provider configuration', extraData: $exception->getMessage());
        }

        return $this->apiResponse($client->getConfigurationHolders()->getValues());
    }

    public function extractUploadedFileFromClientForm(FormInterface $clientForm): array
    {
        $fields = $clientForm->all();
        $files = [];

        foreach ($fields as $name => $field) {
            if ($field->getData() instanceof UploadedFile) {
                $files[$name] = $field->getData();
            }
        }

        return $files;
    }

    #[Route('/preferences', name: '_client_preferences', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function preferencesClient(Request $request, ClientRepository $clientRepository): Response
    {
        $client = $clientRepository->find($request->request->get('clientId'));

        if (!empty($request->request->get('preferencesClient')) && !empty($client)) {
            $choices = json_decode($request->request->get('preferencesClient'), true);
            $client->setPreferences($choices);
            $this->entityManager->persist($client);
            $this->addFlash('success', 'The changes have been well implemented.');
            $this->entityManager->flush();
        }

        return new Response();
    }

    #[Route('/update-services', name: '_client_update_services', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function updateServicesClient(Request $request, ClientRepository $clientRepository, Security $security): Response
    {
        try {
            $client = $clientRepository->find($request->request->get('clientId'));

            if (!$security->isGranted(ClientVoter::EDIT, subject: $client)) {
                $this->addFlash('error', 'Your are not allowed to do this action');

                return $this->apiErrorResponse('Your are not allowed to do this action');
            }

            if (!empty($request->request->get('clientServices')) && !empty($client)) {
                $services = json_decode($request->request->get('clientServices'), true);
                /** @var ClientServices $clientService */
                $clientService = $client->getServices();
                $clientService->setLumpSumContract($services['lumpSumContract']);
                $clientService->setAssistanceType($services['assistanceType']);
                $clientService->setAssistanceDuration($services['assistanceDuration']);
                $clientService->setAssistanceStart($services['assistanceStart']);
                //$clientService->setCommitment($services['commitment']);
                $clientService->setEmailToNotifyForAssistance($services['emailToNotifyForAssistance']);
                $clientService->setSupportType($services['supportType']);
                $clientServiceStandardHours = $clientService->getStandardHours();
                $clientServiceStandardHours->setActive($services['standardHours']['active']);
                $clientServiceStandardHours->setStartTimeAM(new \DateTime($services['standardHours']['startTimeAM']));
                $clientServiceStandardHours->setEndTimeAM(new \DateTime($services['standardHours']['endTimeAM']));
                $clientServiceStandardHours->setStartTimeLH(new \DateTime($services['standardHours']['endTimeAM']));
                $clientServiceStandardHours->setEndTimeLH(new \DateTime($services['standardHours']['startTimePM']));
                $clientServiceStandardHours->setStartTimePM(new \DateTime($services['standardHours']['startTimePM']));
                $clientServiceStandardHours->setEndTimePM(new \DateTime($services['standardHours']['endTimePM']));
                $clientService->setOutsideSupportType($services['outsideSupportType']);
                $clientServiceOutsideStandardHours = $clientService->getOutsideStandardHours();
                $clientServiceOutsideStandardHours->setActive($services['outsideStandardHours']['active']);
                $clientServiceOutsideStandardHours->setStartTimeAM(new \DateTime($services['outsideStandardHours']['startTimeAM']));
                $clientServiceOutsideStandardHours->setEndTimeAM(new \DateTime($services['outsideStandardHours']['endTimeAM']));
                $clientServiceOutsideStandardHours->setStartTimeLH(new \DateTime($services['outsideStandardHours']['startTimeLH']));
                $clientServiceOutsideStandardHours->setEndTimeLH(new \DateTime($services['outsideStandardHours']['endTimeLH']));
                $clientServiceOutsideStandardHours->setStartTimePM(new \DateTime($services['outsideStandardHours']['startTimePM']));
                $clientServiceOutsideStandardHours->setEndTimePM(new \DateTime($services['outsideStandardHours']['endTimePM']));
                $clientService->setTimeBeforeSession($services['timeBeforeSession']);
                $clientService->setTimeSupportDurationSession($services['timeSupportDurationSession']);
                $clientService->setDedicatedSupport($services['dedicatedSupport']);
                $clientService->setMailSupport($services['mailSupport']);
                $clientService->setPhoneSupport($services['phoneSupport']);
                $clientService->setMailManager($services['mailManager']);
                $clientService->setPhoneManager($services['phoneManager']);
                /*$clientService->setEmailToNotifyForSupport($services['emailToNotifyForSupport']);
                $clientService->setDedicatedUserGuide($services['dedicatedUserGuide']);
                $clientService->setPersonnalizedInfoMailTransactionnal($services['personnalizedInfoMailTransactionnal']);*/

                $client->setServices($clientService);
                $this->entityManager->persist($client);
                $this->addFlash('success', 'The changes have been well implemented.');
                $this->entityManager->flush();
            }
        } catch (\Exception $exception) {
            $this->addFlash('error', 'Account services could not be saved');
            $this->logger->critical($exception, [
                'Id Client' => $request->request->get('clientId'),
            ]);

            return $this->apiErrorResponse('Account services could not be saved', extraData: $exception->getMessage());
        }

        return new Response();
    }

    #[Route('/{id}/billing-setting/save', name: '_client_save_billing_settings', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted(ClientVoter::EDIT, subject: 'client')]
    public function saveBillingSettings(Request $request, Client $client): Response
    {
        try {
            $data = json_decode($request->getContent(), true);
            $client->setBillingMode($data['billingMode']);

            $violations = $this->validator->validate($client);

            if ($violations->count() > 0) {
                $errors = [];
                /** @var ConstraintViolation $violation */
                foreach ($violations as $violation) {
                    if ($violation->getPropertyPath() !== 'billingMode') {
                        $errors['client'] = $this->translator->trans('Account configuration is incorrect');
                    }
                    $errors[$violation->getPropertyPath()] = $violation->getMessage();
                }

                return $this->apiErrorResponse('Billing settings sent are not valid', extraData: $errors);
            }

            $this->entityManager->flush();

            return $this->apiResponse([]);
        } catch (\Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse('Billing settings could not be saved');
        }
    }

    #[Route('/{id}/credit/add', name: '_client_credit_add', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ClientVoter::EDIT, subject: 'client')]
    public function addCredit(Request $request, Client $client): Response
    {
        try {
            $creditAllocation = match ($client->getBillingMode()) {
                Client::BILLING_MODE_CREDIT => (new CreditAllocation($client))->setAllowedCredit($request->query->get('credit')),
                Client::BILLING_MODE_PARTICIPANT => (new CreditAllocationParticipants($client))->setAllowedCredit($request->query->get('credit')),
                default => throw new \Exception('Invalid billing mode, cannot add credit'),
            };
            $client->setExpirationDateCredits((new \DateTime())->modify('+1 years'));

            $this->entityManager->persist($creditAllocation);
            $this->entityManager->flush();

            $response = match ($client->getBillingMode()) {
                Client::BILLING_MODE_CREDIT => [
                    'allowedCredit' => $client->getAllowedCredit(),
                    'remainingCredit' => $client->getRemainingCredit(),
                    'expirationDateCredits' => ($client->getExpirationDateCredits())->setTimezone(new \DateTimeZone($client->getTimezone()))->format('Y-m-d\TH:i:s'),
                    'creditAllocation' => $this->serializer->serialize($creditAllocation, 'json', ['groups' => 'read_client']),
                ],
                Client::BILLING_MODE_PARTICIPANT => [
                    'allowedCredit' => $client->getAllowedCreditParticipants(),
                    'remainingCredit' => $client->getRemainingCreditParticipants(),
                    'expirationDateCredits' => ($client->getExpirationDateCredits())->setTimezone(new \DateTimeZone($client->getTimezone()))->format('Y-m-d\TH:i:s'),
                    'creditAllocation' => $this->serializer->serialize($creditAllocation, 'json', ['groups' => 'read_client']),
                ],
                default => throw new \Exception('Invalid billing mode, cannot add credit'),
            };

            return $this->apiResponse($response);
        } catch (\Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse('Credit could not be added');
        }
    }

    #[Route('/{id}/credit/sms/add', name: '_client_credit_sms_add', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted(ClientVoter::EDIT, subject: 'client')]
    public function addCreditSms(Request $request, Client $client): Response
    {
        try {
            $data = json_decode($request->getContent(), true);
            $creditAllocation = (new CreditAllocationSms())->setClient($client)->setAllowedCredit($data['credit']);

            $this->entityManager->persist($creditAllocation);
            $this->entityManager->flush();

            return $this->apiResponse([
                'allowedCredit' => $client->getCreditSms(),
                'creditAllocation' => $this->serializer->serialize($creditAllocation, 'json', ['groups' => 'read_client']),
            ]);
        } catch (\Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse('Credit could not be added');
        }
    }

    #[Route('/evaluation-mail-update/{id}/', name: '_session_evaluation_mail_update', options: ['expose' => true], methods: ['POST'], format: 'json')]
    #[IsGranted('ROLE_MANAGER')]
    public function updateEvaluationMailModal(
        Request $request,
        EvaluationModalMail $evaluationModalMail
    ): Response {
        try {
            $data = json_decode($request->getContent(), true);
            $evaluationModalMail
                ->setSubjectMail($data['subjectMail'])
                ->setContent($data['content']);
            if (isset($data['attachments'])) {
                $attachmentsCollection = new ArrayCollection();
                foreach ($data['attachments'] as $attachmentData) {
                    $attachment = $this->entityManager->getRepository(SessionConvocationAttachment::class)->find($attachmentData['id']);
                    if ($attachment instanceof SessionConvocationAttachment) {
                        $attachment->setEvaluationModalMail($evaluationModalMail);
                        /** @psalm-suppress InvalidArgument */
                        $attachmentsCollection->add($attachment);
                    }
                }
                $evaluationModalMail->setAttachments($attachmentsCollection);
            }
            $this->entityManager->flush();

            return $this->apiResponse('Evaluation mail modal updated');
        } catch (\Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse('Evaluation mail modal could not be updated');
        }
    }
}
