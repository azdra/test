<?php

namespace App\Controller;

use App\Entity\Evaluation;
use App\Entity\EvaluationProviderSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Model\ApiResponseTrait;
use App\Repository\ClientRepository;
use App\Repository\EvaluationRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Security\ProviderSessionVoter;
use App\Service\ActivityLogger;
use App\Service\EvaluationProviderSessionService;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/evaluation')]
class EvaluationController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private LoggerInterface $logger,
        private TranslatorInterface $translator,
        private EvaluationRepository $evaluationRepository,
        private EvaluationProviderSessionService $evaluationProviderSessionService,
        private EntityManagerInterface $entityManager,
        private ActivityLogger $activityLogger,
    ) {
    }

    #[Route('/save', name: '_evaluation_save', options: ['expose' => true])]
    #[IsGranted('ROLE_ADMIN')]
    public function addNewEvaluationForm(
        Request $request,
        ClientRepository $clientRepository,
        TranslatorInterface $translator
    ): Response {
        $client = $clientRepository->find(
            $request->query->get('client')
        );

        $survey = $request->query->all('survey');

        if (array_key_exists('id', $survey)) {
            $evaluation = $this->evaluationRepository->find($survey['id']);
            if ($evaluation === null) {
                return $this->apiErrorResponse($translator->trans('Unable to find the evaluation form to edit'));
            }
        } else {
            $evaluation = new Evaluation();
        }

        $evaluation
            ->setTitle($survey['title'])
            ->setSurveyId($survey['surveyId'])
            ->setClient($client);

        if (!empty($survey['organizationId'])) {
            $evaluation->setOrganizationId(
                $survey['organizationId']
            );
        }

        try {
            $this->entityManager->persist($evaluation);
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'Id Client' => $client->getId(),
            ]);

            return $this->apiErrorResponse($translator->trans('Unable to create the evaluation form'), extraData: $exception->getMessage());
        }

        return $this->apiResponse([$translator->trans('The evaluation form has been successfully added')]);
    }

    #[Route('/{id}', name: '_evaluation_delete', options: ['expose' => true], methods: ['DELETE'])]
    #[IsGranted('ROLE_ADMIN')]
    public function deleteEvaluationForm(
        Evaluation $evaluation,
        TranslatorInterface $translator
    ): Response {
        try {
            if ($evaluation->getSessions()->count() > 0) {
                return $this->apiErrorResponse($translator->trans('This evaluation form is used, you can not delete it.'));
            }

            $this->entityManager->remove($evaluation);
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'Id evaluation' => $evaluation->getId(),
            ]);

            return $this->apiErrorResponse($translator->trans('Unable to delete the evaluation form'), extraData: $exception->getMessage());
        }

        return $this->apiResponse([$translator->trans('The evalution form has been successfully deleted')]);
    }

    #[Route('/send-email-evaluation/{id}', name: '_send_email_evaluation', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted(ProviderSessionVoter::ACCESS, subject: 'session')]
    public function sendEvaluationMailToParticipant(ProviderSession $session, Request $request, TranslatorInterface $translator, MailerService $mailerService, ProviderParticipantSessionRoleRepository $participantSessionRoleRepository): Response
    {
        /** @var array $participants */
        $participants = explode(',', $request->query->get('participant'));

        if (empty($participants)) {
            return $this->apiErrorResponse('No participants are selected');
        }

        if (empty($session->getEvaluations())) {
            return $this->apiErrorResponse('No evaluation form is associated with the session');
        }

        foreach ($participants as $participantSessionRoleId) {
            /** @var ProviderParticipantSessionRole $participantSessionRole */
            $participantSessionRole = $participantSessionRoleRepository->find($participantSessionRoleId);
            $user = $participantSessionRole->getParticipant()->getUser();
            $subject = $translator->trans('Session evaluation %sessionName% - %date% from %hoursStart% to %hoursEnd%', [
                '%sessionName%' => $session->getName(),
                '%date%' => $session->getDateStart()->format('d/m/Y'),
                '%hoursStart%' => $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('H:i'),
                '%hoursEnd%' => $session->getDateEnd()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('H:i'),
            ]);
            try {
                $mailerService->sendTemplatedMail(
                    $user->getEmail(),
                    $subject,
                    'emails/participant/evaluation_link.html.twig',
                    context: [
                        'user' => $user,
                        'client' => $user->getClient(),
                        'participant' => $participantSessionRole->getParticipant(),
                        'session' => $participantSessionRole->getSession(),
                    ],
                    metadatas: [
                        'origin' => MailerService::ORIGIN_EMAIL_SEND_EVALUATION_LINK,
                        'user' => $user->getId(),
                        'client' => $user->getClient()->getId(),
                    ]
                );
            } catch (\Exception $exception) {
                $this->logger->error($exception, [
                    'Id Participant' => $participantSessionRoleId,
                ]);

                return $this->apiErrorResponse($exception->getMessage());
            }
        }

        $this->addFlash('success', 'Email sent to each selected participant');

        return $this->apiResponse('success');
    }

    #[Route('/send-email-evaluation-provider-session/{id}', name: '_send_email_evaluation_provider_session', options: ['expose' => true], methods: ['POST'])]
    public function sendSelectedEvaluationMailToParticipant(EvaluationProviderSession $evaluationProviderSession, Request $request): Response
    {
        $this->denyAccessUnlessGranted(ProviderSessionVoter::ACCESS, $evaluationProviderSession->getProviderSession());
        /** @var array $participants */
        $participants = explode(',', $request->query->get('participant'));

        if (empty($participants)) {
            return $this->apiErrorResponse('No participants are selected');
        }
        try {
            $this->evaluationProviderSessionService->sendEvaluationMailToSelectedParticipant(evaluationProviderSession: $evaluationProviderSession, participants: $participants);
            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'Id Evaluation Provider Session' => $evaluationProviderSession->getId(),
            ]);

            return $this->apiErrorResponse($exception->getMessage());
        }

        $this->addFlash('success', 'Email sent to each selected participant');

        return $this->apiResponse('success');
    }

    #[Route('/schedule-shipment-email-evaluation-provider-session/{id}', name: '_schedule_shipment_email_evaluation_provider_session', options: ['expose' => true], methods: ['POST'])]
    public function scheduleShipmentEvaluationMail(EvaluationProviderSession $evaluationProviderSession, Request $request): Response
    {
        $this->denyAccessUnlessGranted(ProviderSessionVoter::ACCESS, $evaluationProviderSession->getProviderSession());
        $sendDate = $request->query->get('sendDate');
        if (empty($sendDate)) {
            return $this->apiErrorResponse('No date is selected for sending the email');
        }
        try {
            $newDateSend = (new \DateTime($sendDate))->setTimezone(new \DateTimeZone($evaluationProviderSession->getProviderSession()->getClient()->getTimezone()));
            $evaluationProviderSession->setSendAt($newDateSend);
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'Id Evaluation Provider Session' => $evaluationProviderSession->getId(),
            ]);

            return $this->apiErrorResponse($exception->getMessage());
        }
        $this->addFlash('success', 'Email scheduled for shipment');

        return $this->apiResponse('success');
    }

    #[Route('/cancel-sending-email-evaluation-provider-session/{id}', name: '_cancel_sending_email_evaluation_provider_session', options: ['expose' => true], methods: ['POST'])]
    public function cancelSendingEvaluationMail(EvaluationProviderSession $evaluationProviderSession): Response
    {
        $this->denyAccessUnlessGranted(ProviderSessionVoter::ACCESS, $evaluationProviderSession->getProviderSession());
        try {
            $evaluationProviderSession->setSendAt(null);
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'Id Evaluation Provider Session' => $evaluationProviderSession->getId(),
            ]);

            return $this->apiErrorResponse($exception->getMessage());
        }
        $this->addFlash('success', 'Cancelled email programming');

        return $this->apiResponse('success');
    }

    #[Route('/{id}/delete-evaluation-session', name: '_delete_evaluation_session', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function deleteEvaluationSession(
        EvaluationProviderSession $evaluationProviderSession
    ): Response {
        try {
            $idEvaluationProviderSessionForLogger = $evaluationProviderSession->getId();
            $this->entityManager->remove($evaluationProviderSession);
            $this->entityManager->flush();

            $this->addFlash('success', 'The evaluation has been deleted successfully.');

            return $this->apiResponse(['success' => true]);
        } catch (Throwable $throwable) {
            $this->logger->error($throwable, [
                'evaluationId' => $idEvaluationProviderSessionForLogger,
                'loggedUserId' => $this->getUser()->getId(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An unknown error occurred while deleted webinar communication in the session.'));
        }
    }
}
