<?php

namespace App\Controller;

use App\CustomerService\Repository\HelpTaskRepository;
use App\Entity\User;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\WebCalService;
use Eluceo\iCal\Presentation\Component;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

#[Route('/webcal')]
class WebCalController extends AbstractController
{
    #[Route('/upcoming-sessions/{token}.ics', name: '_webcal_upcoming_sessions', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['GET'])]
    public function webCalFile(
        string $token,
        WebCalService $webCalService,
        UserRepository $userRepository,
        RoleHierarchyInterface $roleHierarchy,
        ProviderSessionRepository $providerSessionRepository
    ): Response {
        /** @var ?User $user */
        if (null === ($user = $userRepository->findOneBy(['token' => $token]))) {
            throw new NotFoundHttpException('Unable to found ressource for this token');
        }

        $userRoles = $roleHierarchy->getReachableRoleNames($user->getRoles());
        if (array_search('ROLE_MANAGER', $userRoles) === false) {
            throw new AccessDeniedHttpException('Your are not allowed to access ressource this ressource');
        }

        $dateStart = new \DateTime('now');
        $dateStart->modify('-'.WebCalService::WEEKS_BEFORE.' weeks');
        $dateEnd = new \DateTime('now');
        $dateEnd->modify('+'.WebCalService::WEEKS_AFTER.' weeks');

        if (array_search('ROLE_ADMIN', $userRoles) === false) {
            $sessions = $providerSessionRepository->findUserAccessibleSessionsForPeriod($dateStart, $dateEnd, $user);
        } else {
            $sessions = $providerSessionRepository->findAllSessionForPeriod($dateStart, $dateEnd);
        }

        /** @var Component $icsFile */
        $icsFile = $webCalService->generateWebCalFile($sessions);

        $content = '';
        foreach ($icsFile as $line) {
            $content .= $line;
        }

        return new Response(
            $content,
            200,
            [
                'Content-Type' => 'text/calendar; charset=utf-8',
            ]
        );
    }

    #[Route('/my-service/{token}.ics', name: '_webcal_my_service', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['GET'])]
    public function myServiceWebCalFile(
        string $token,
        WebCalService $webCalService,
        UserRepository $userRepository,
        RoleHierarchyInterface $roleHierarchy,
        ProviderSessionRepository $providerSessionRepository,
        HelpTaskRepository $helpTaskRepository
    ): Response {
        /** @var ?User $user */
        if (null === ($user = $userRepository->findOneBy(['token' => $token]))) {
            throw new NotFoundHttpException('Unable to found ressource for this token');
        }

        $userRoles = $roleHierarchy->getReachableRoleNames($user->getRoles());
        if (array_search('ROLE_CUSTOMER_SERVICE', $userRoles) === false) {
            throw new AccessDeniedHttpException('Your are not allowed to access ressource this ressource');
        }

        $dateStart = new \DateTime('now');
        $dateStart->modify('-'.WebCalService::WEEKS_BEFORE.' weeks');
        $dateEnd = new \DateTime('now');
        $dateEnd->modify('+'.WebCalService::WEEKS_AFTER.' weeks');

        $helpTask = $helpTaskRepository->findBetweenDates(['start' => $dateStart->format('Y-m-d\TH:i:sP'), 'end' => $dateEnd->format('Y-m-d\TH:i:sP'), 'helperUser' => $user]);

        /** @var Component $icsFile */
        $icsFile = $webCalService->generateWebCalFile($helpTask);

        $content = '';
        foreach ($icsFile ?? [] as $line) {
            $content .= $line;
        }

        return new Response(
            $content,
            200,
            [
                'Content-Type' => 'text/calendar; charset=utf-8',
            ]
        );
    }
}
