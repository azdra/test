<?php

namespace App\Controller;

use App\Handler\BrevoSmsWebhookHandlerInterface;
use App\Handler\BrevoWebhookHandlerInterface;
use App\Security\WebhookAuthenticationInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/webhook-brevo')]
class WebhookBrevoController extends AbstractController
{
    public function __construct(
        private BrevoWebhookHandlerInterface $webhookHandler,
        private BrevoSmsWebhookHandlerInterface $brevoSmsWebhookHandler,
        private ?WebhookAuthenticationInterface $webhookAuthentication = null
    ) {
    }

    #[Route('/handle', name: '_webhook_brevo_handle', methods: ['POST', 'GET', 'HEAD'])]
    public function handle(Request $request): Response
    {
        // Mandrill will do a quick check that the URL exists by using a HEAD request (not POST).
        // Note that Symfony silently transforms HEAD requests to GET.
        // Enlever les commentaires si l'appel ne se fait pas du navigateur. On peut peut être tester avec postman !
        /*if (in_array($request->getMethod(), ['GET', 'POST', 'HEAD'])) {
            return new Response();
        }*/

        if (!$this->isAuthenticated($request)) {
            throw new AccessDeniedHttpException();
        }

        $this->webhookHandler->handleRequest($request);

        return new Response();
    }

    #[Route('/sms/handle', name: '_webhook_brevo_sms_handle', methods: ['POST', 'GET', 'HEAD'])]
    public function handleSms(Request $request): Response
    {
        // Mandrill will do a quick check that the URL exists by using a HEAD request (not POST).
        // Note that Symfony silently transforms HEAD requests to GET.
        // Enlever les commentaires si l'appel ne se fait pas du navigateur. On peut peut être tester avec postman !
        /*if (in_array($request->getMethod(), ['GET', 'POST', 'HEAD'])) {
            return new Response();
        }*/

        if (!$this->isAuthenticated($request)) {
            throw new AccessDeniedHttpException();
        }

        $this->brevoSmsWebhookHandler->handleRequest($request);

        return new Response();
    }

    private function isAuthenticated(Request $request): bool
    {
        if (!$this->webhookAuthentication) {
            return true;
        }

        return $this->webhookAuthentication->isAuthenticated($request);
    }
}
