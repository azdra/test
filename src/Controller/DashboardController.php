<?php

namespace App\Controller;

use App\Model\ApiResponseTrait;
use App\Repository\BounceRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\WidgetNewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/dashboard')]
#[IsGranted('ROLE_MANAGER')]
class DashboardController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private LoggerInterface $logger,
        private EntityManagerInterface $entityManager,
        private TranslatorInterface $translator
    ) {
    }

    #[Route('/current-session', name: '_dashboard_index', options: ['expose' => true])]
    public function listIncoming(ProviderSessionRepository $sessionRepository, BounceRepository $bounceRepository, WidgetNewsRepository $widgetNewsRepository): Response
    {
        $client = $this->getUser()->getClient();
        $now = (new \DateTimeImmutable())->setTime(0, 0);
        $aMonthInterval = \DateInterval::createFromDateString('30 day');

        return $this->render('dashboard/index.html.twig', [
            'title' => 'Running and incoming sessions',
            'currentSession' => $sessionRepository->getCurrentSession($client, $now),
            'statistics' => $sessionRepository->getPeriodStatistics($client, $now),
            'currentStatistics' => $sessionRepository->getPeriodStatistics($client, $now, $now->add($aMonthInterval)),
            'pastStatistics' => $sessionRepository->getPeriodStatistics($client, $now->sub($aMonthInterval), $now),
            'countMailInError' => $bounceRepository->getMailInErrorForSessionInPeriod($client, $now, $now->add($aMonthInterval)),
            'widgetNews' => $widgetNewsRepository->findByLangs([$this->getUser()->getPreferredLang()], 5),
        ]);
    }
}
