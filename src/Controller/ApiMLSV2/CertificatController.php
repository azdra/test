<?php

namespace App\Controller\ApiMLSV2;

use App\Controller\Api\ProviderSessionApiController;
use App\Entity\ProviderSession;
use App\Repository\ProviderSessionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/certificate/meetingparticipant')]
class CertificatController extends AbstractController
{
    public function __construct(
        private ProviderSessionRepository $sessionRepository
    ) {
    }

    #[Route('/{id}', methods: ['GET'])]
    public function meetingParticipantCertificat(Request $request, int $id): Response
    {
        $request->setRequestFormat('json');
        $request->headers->set('Content-Type', 'application/json');

        $session = $this->sessionRepository->find($id);
        $response = $this->forward(ProviderSessionApiController::class.'::getProviderSession', ['session' => $session]);

        $response->setContent(json_encode($this->translateParticipants(
            json_decode($response->getContent(), true),
            $session
        )));

        return $response;
    }

    private function translateParticipants(array $meeting, ProviderSession $session): array
    {
        if (!array_key_exists('participantRoles', $meeting)) {
            return [];
        }

        $translation = [];
        foreach ($meeting['participantRoles'] as $participantRole) {
            $translation[] = $this->translateParticipant($participantRole, $session);
        }

        return $translation;
    }

    private function translateParticipant(array $participant, ProviderSession $session): array
    {
        return [
            'id' => $participant['id'],
            'meeting_id' => $session->getId(),
            'user_id' => $participant['userId'],
            'markedPresent' => $participant['manualPresenceStatus'],
            'typeUser' => $participant['role'] === 'animator' ? 'Animateur' : 'Participant',
            'emailUser' => $participant['email'],
            'firstnameUser' => $participant['firstName'],
            'nameUser' => $participant['lastName'],
            'durationPres' => $participant['slotPresenceDuration'],
            'dateStartPres' => (new \DateTime($participant['presenceDateStart']))->format('d/m/Y h:i'),
            'dateEndPres' => (new \DateTime($participant['presenceDateEnd']))->format('d/m/Y h:i'),
            'statusPres' => $participant['present'] ? 'Present' : 'Absent',
            'referenceMeeting' => $session->getProviderDistantID(),
        ];
    }
}
