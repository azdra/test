<?php

namespace App\Controller\ApiMLSV2;

use App\Controller\Api\ProviderSessionApiController;
use App\Entity\ProviderSession;
use App\Repository\ProviderSessionRepository;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/meeting')]
class MeetingController extends AbstractController
{
    public function __construct(private ProviderSessionRepository $sessionRepository)
    {
    }

    #[Route('', name: '_api_mls_sessions_list', methods: ['GET'])]
    public function listSession(Request &$request): Response
    {
        $request->setRequestFormat('json');
        $request->headers->set('Content-Type', 'application/json');
        $response = $this->forward(ProviderSessionApiController::class.'::getAll');

        $response->setContent(json_encode(
            $this->translateMeetings(json_decode($response->getContent(), true))
        ));

        return $response;
    }

    #[Route('/dates/{from}/{to}', name: '_api_mls_sessions_list_by_dates', methods: ['GET'])]
    public function listSessionByDates(Request &$request, string $from, string $to): Response
    {
        $request->setRequestFormat('json');
        $request->headers->set('Content-Type', 'application/json');

        $request->request->add(['from' => $from, 'to' => $to]);
        $response = $this->forward(ProviderSessionApiController::class.'::getAll');

        $response->setContent(json_encode(
            $this->translateMeetings(json_decode($response->getContent(), true))
        ));

        return $response;
    }

    #[Route('/reference/{ref}', name: '_api_mls_session_by_ref', methods: ['GET'])]
    public function listSessionByRef(Request &$request, string $ref): Response
    {
        $request->setRequestFormat('json');
        $request->headers->set('Content-Type', 'application/json');

        /** @var ProviderSession $session */
        $session = $this->sessionRepository->findOneBy(['ref' => $ref]);
        $response = $this->forward(ProviderSessionApiController::class.'::getProviderSession', ['session' => $session]);

        $response->setContent(json_encode(
            $this->translateMeeting(json_decode($response->getContent(), true))
        ));

        return $response;
    }

    #[Route('/{id}', name: '_api_mls_session_by_id', methods: ['GET'])]
    public function listSessionById(Request &$request, int $id): Response
    {
        $request->setRequestFormat('json');
        $request->headers->set('Content-Type', 'application/json');

        $session = $this->sessionRepository->find($id);
        $response = $this->forward(ProviderSessionApiController::class.'::getProviderSession', ['session' => $session]);

        $response->setContent(json_encode(
            $this->translateMeeting(json_decode($response->getContent(), true))
        ));

        return $response;
    }

    private function translateMeetings(array $meetings): array
    {
        return array_map(
            function (array $meeting) {
                return $this->translateMeeting($meeting);
            },
            $meetings
        );
    }

    private function translateMeeting(array $meeting): array
    {
        $translation = [
            'id' => $meeting['id'],
            'reference' => $meeting['ref'],
            'categorymeeting' => $meeting['category'],
            'name' => $meeting['name'],
            'date' => $meeting['dateStart'],
            'duration' => $meeting['duration'],
            'convocation_model' => $meeting['convocation']['name'],
            'language' => $meeting['language'],
            'send_email' => $meeting['sendInvitationEmails'],
            'access_type' => '',
            'base_url' => '',
            'url' => '',
            'conference_phone' => '',
            'conference_phone_code_animator' => '',
            'conference_phone_code_participant' => '',
        ];

        if ($meeting['type'] === 'adobe_connect_sco') {
            $translation['access_type'] = match ($meeting['admittedSession']) {
                AdobeConnectConnector::GRANT_ACCESS_VISITOR => 'PVA',
                AdobeConnectConnector::GRANT_ACCESS_USER_ONLY => 'PA',
                default => 'TLM'
            };  // verifier correspondance ok
            $translation['base_url'] = $meeting['abstractProviderConfigurationHolder']['baseUrl'];
            $translation['url'] = $meeting['urlPath'];

            if (array_key_exists('providerAudio', $meeting)) {
                $translation['conference_phone'] = $meeting['providerAudio']['adobeDefaultPhoneNumber'] ?? null; // verifier
                $translation['conference_phone_code_animator'] = $meeting['providerAudio']['codeAnimator'] ?? null; // verifier
                $translation['conference_phone_code_participant'] = $meeting['providerAudio']['codeParticipant'] ?? null; // verifier
            }
        }

        return $translation;
    }
}
