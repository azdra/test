<?php

namespace App\Controller\ApiMLSV2;

use App\Controller\Api\ProviderSessionApiController;
use App\Controller\Api\UserApiController;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

#[Route('/meeting/{id}/participant')]
class TraineeController extends AbstractController
{
    public function __construct(
        private ProviderSessionRepository $sessionRepository,
        private UserRepository $userRepository,
        private string $endPointDomain,
        private RouterInterface $router
    ) {
    }

    #[Route('', name: '_api_mls_session_participant_list', methods: ['GET'])]
    public function listSessionParticipant(Request $request, int $id): Response
    {
        $request->setRequestFormat('json');
        $request->headers->set('Content-Type', 'application/json');

        $session = $this->sessionRepository->find($id);
        $response = $this->forward(ProviderSessionApiController::class.'::getProviderSession', ['session' => $session]);

        $response->setContent(json_encode(
            $this->translateParticipants(json_decode($response->getContent(), true))
        ));

        return $response;
    }

    #[Route('', name: '_api_mls_session_participant_create', methods: ['POST'])]
    public function createSessionParticipant(Request &$request, int $id): Response
    {
        $request->setRequestFormat('json');
        $request->headers->set('Content-Type', 'application/json');

        $originalRequestContent = $request->request->all();
        $session = $this->sessionRepository->find($id);

        $user = $this->getAndUpdateOrCreateUser($request, $originalRequestContent);

        return $this->subscribeNewParticipant($request, $session, $user, $originalRequestContent['type'] === 'A' ? ProviderParticipantSessionRole::ROLE_ANIMATOR : ProviderParticipantSessionRole::ROLE_TRAINEE);
    }

    #[Route('/{trainee_id}', name: '_api_mls_session_participant_edit', methods: ['PATCH'])]
    #[ParamConverter('user', options: ['mapping' => ['trainee_id' => 'id']])]
    public function updateSessionParticipant(Request $request, ProviderSession $session, User $user): Response
    {
        $request->setRequestFormat('json');
        $request->headers->set('Content-Type', 'application/json');

        $originalRequestContent = $request->request->all();

        if ($user->getEmail() !== $originalRequestContent['email']) {
            $user = $this->userRepository->findOneBy(['email' => $originalRequestContent['email'], 'client' => $this->getUser()->getClient()]);
            if (!empty($user)) {
                return new Response(
                    content: json_encode(['code' => 'ERROR_EMAIL_ALREADY_USED', 'message' => 'Another user already uses this email']),
                    status: Response::HTTP_INTERNAL_SERVER_ERROR
                );
            }
        }

        $request->request->replace([
            'firstName' => $originalRequestContent['firstname'],
            'lastName' => $originalRequestContent['lastname'],
            'email' => $originalRequestContent['email'],
        ]);
        $response = $this->forward(UserApiController::class.'::partialUpdate', ['request' => $request, 'user' => $user]);

        return $this->subscribeNewParticipant($request, $session, $user, $originalRequestContent['type'] === 'A' ? ProviderParticipantSessionRole::ROLE_ANIMATOR : ProviderParticipantSessionRole::ROLE_TRAINEE);
    }

    #[Route('/{trainee_id}', name: '_api_mls_session_participant_delete', methods: ['DELETE'])]
    #[ParamConverter('user', options: ['mapping' => ['trainee_id' => 'id']])]
    public function deleteSessionParticipant(Request $request, ProviderSession $session, User $user): Response
    {
        $request->setRequestFormat('json');
        $request->headers->set('Content-Type', 'application/json');

        $response = $this->removeParticipant($request, $session, $user);
        $response->setContent(null);

        return $response;
    }

    private function getAndUpdateOrCreateUser(Request &$request, array $content): ?User
    {
        $user = $this->userRepository->findOneBy(['email' => $content['email'], 'client' => $this->getUser()->getClient()]);

        $request->request->replace([
            'firstName' => $content['firstname'],
            'lastName' => $content['lastname'],
            'email' => $content['email'],
        ]);

        if (empty($user)) {
            $response = $this->forward(UserApiController::class.'::create', ['request' => $request]);
            $user = $this->userRepository->findOneBy(['email' => $content['email'], 'client' => $this->getUser()->getClient()]);
        } else {
            $response = $this->forward(UserApiController::class.'::partialUpdate', ['request' => $request, 'user' => $user]);
        }

        return $user;
    }

    private function removeParticipant(Request $request, ProviderSession $session, User $user): Response
    {
        $participants = array_map(function (array $participant) {
            return [
                    'userId' => $participant['userId'],
                    'role' => $participant['role'],
                ];
        }, $this->getCurrentSessionParticipant($session)
        );

        $userId = $user->getId();
        $participantIndexesToRemove = array_keys(array_filter($participants, function (array $participant) use ($userId) {
            return $participant['userId'] === $userId;
        }));

        foreach ($participantIndexesToRemove as $index) {
            array_splice($participants, $index, 1);
        }

        $request->setMethod(Request::METHOD_PATCH);
        $request->request->replace([
            'type' => match (get_class($session)) {
                AdobeConnectSCO::class => 'adobe_connect_sco',
                WebexMeeting::class => 'cisco_webex_meeting',
                WebexRestMeeting::class => 'cisco_webex_rest_meeting',
                MicrosoftTeamsSession::class => 'microsoft_teams_session'
            }, 'participants' => $participants,
        ]);

        return $this->forward(ProviderSessionApiController::class.'::patchProviderSession', ['request' => $request, 'session' => $session]);
    }

    private function subscribeNewParticipant(Request $request, ProviderSession $session, User $user, string $role): Response
    {
        // prepare participants list from existing participant and the new one
        $participants = array_map(function (array $participant) {
            return [
                    'userId' => $participant['userId'],
                    'role' => $participant['role'],
                ];
        }, $this->getCurrentSessionParticipant($session)
        );
        $participants[] = ['userId' => $user->getId(), 'role' => $role];

        // prepare the request
        $request->setMethod(Request::METHOD_PATCH);
        $request->request->replace([
            'type' => match (get_class($session)) {
                AdobeConnectSCO::class => 'adobe_connect_sco',
                WebexMeeting::class => 'cisco_webex_meeting',
                WebexRestMeeting::class => 'cisco_webex_rest_meeting',
                MicrosoftTeamsSession::class => 'microsoft_teams_session'
            }, 'participants' => $participants,
        ]);

        // subscribe with the new api
        $response = $this->forward(ProviderSessionApiController::class.'::patchProviderSession', ['request' => $request, 'session' => $session]);

        // translate the response
        $participants = $this->getCurrentSessionParticipant($session);
        $userId = $user->getId();
        $participant = array_values(array_filter($participants, function (array $participant) use ($userId) {
            return $participant['userId'] === $userId;
        }));
        $response->setContent(json_encode($this->translateParticipant($participant[0])));

        return $response;
    }

    private function getCurrentSessionParticipant(ProviderSession $session): array
    {
        $response = $this->forward(ProviderSessionApiController::class.'::getProviderSession', ['session' => $session]);
        $meeting = json_decode($response->getContent(), true);

        return $meeting['participantRoles'] ?? [];
    }

    private function translateParticipants(array $meeting): array
    {
        if (!array_key_exists('participantRoles', $meeting)) {
            return [];
        }

        $translation = [];
        foreach ($meeting['participantRoles'] as $participantRole) {
            $translation[] = $this->translateParticipant($participantRole);
        }

        return $translation;
    }

    private function translateParticipant(array $participant): array
    {
        $connexionUrl = $this->endPointDomain
            .$this->router->generate('_session_access_plateforme', ['token' => $participant['providerSessionAccessToken']['token']]);

        return [
            'id' => $participant['userId'], // verfier
            'firstname' => $participant['firstName'],
            'lastname' => $participant['lastName'],
            'email' => $participant['email'],
            'type' => $participant['role'] === 'animator' ? 'A' : 'P',
            'retail' => false,
            'mite' => $participant['situationMixte'],
            'connection_url' => $connexionUrl,
        ];
    }
}
