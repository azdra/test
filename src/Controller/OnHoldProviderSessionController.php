<?php

namespace App\Controller;

use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\OnHoldProviderSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Model\ApiResponseTrait;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Security\LoginFormAuthenticator;
use App\Security\UserSecurityService;
use App\Service\ActivityLogger;
use App\Service\AdobeConnectService;
use App\Service\OnHoldProviderSessionService;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/on-hold-session')]

class OnHoldProviderSessionController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private ActivityLogger $activityLogger,
        protected EntityManagerInterface $entityManager,
        protected NormalizerInterface $normalizer,
        protected AdobeConnectService $adobeConnectService,
        protected OnHoldProviderSessionService $onHoldProviderSessionService,
        protected ParticipantSessionSubscriber $participantSessionSubscriber,
        protected ProviderSessionRepository $providerSessionRepository,
        protected UserRepository $userRepository,
        protected UserSecurityService $userSecurityService,
        protected UserAuthenticatorInterface $userAuthenticator,
        private LoginFormAuthenticator $loginFormAuthenticator
    ) {
    }

    #[Route('/viewer/{id}', name: '_on_hold_provider_session_viewer', requirements: ['id' => '\d+'], options: ['expose' => true])]
    //#[IsGranted(OnHoldProviderSessionVoter::ACCESS, subject: 'onHoldProviderSession')]
    public function getOnHoldProviderSessionViewer(Request $request, OnHoldProviderSession $onHoldProviderSession): Response
    {
        if (empty($this->getUser())) {
            try {
                $token = $request->query->get('token');
                //dump($token); //http://mlsm300.ccu/on-hold-session/viewer/11?token=0a6afac75bdba68fc4301501e02bd132
                $user = $this->entityManager->getRepository(User::class)->findOneBy(['token' => $token]);

                $rememberMe = new RememberMeBadge();
                $rememberMe->enable();
                $this->userAuthenticator->authenticateUser($user, $this->loginFormAuthenticator, $request, [$rememberMe]);
            } catch (\Throwable $exception) {
                return $this->redirectToRoute('_app_login');
            }
        }

        $modelsAdobeConnect = [];
        foreach ($onHoldProviderSession->getClient()->getConfigurationHolders() as $configurationHolder) {
            if ($configurationHolder instanceof AdobeConnectConfigurationHolder) {
                $modelsAdobeConnect = $this->adobeConnectService->getSharedMeetingsTemplate($configurationHolder, 'meeting');
                break;
            }
        }

        return $this->render('onHoldProviderSession/viewer.html.twig', [
            'title' => 'OnHoldSessions',
            'onHoldProviderSession' => $onHoldProviderSession,
            'modelsAdobeConnect' => (!empty($modelsAdobeConnect->getData())) ? json_encode($modelsAdobeConnect->getData()) : json_encode([]),
        ]);
    }

    #[Route('/select-animator-teams/{id}', name: '_on_hold_provider_session_select_animator_teams', requirements: ['id' => '\d+'], options: ['expose' => true])]
    //#[IsGranted(OnHoldProviderSessionVoter::ACCESS, subject: 'onHoldProviderSession')]
    public function selectAnimator(OnHoldProviderSession $onHoldProviderSession, Request $request): Response
    {
        $demandStatusValidation = 2;
        $slotDateMegaId = $request->query->get('slodDateMegaId');
        $animatorEmail = $request->query->get('animatorEmail');
        $result = $this->onHoldProviderSessionService->selectAnimatorOnDemand($onHoldProviderSession, $animatorEmail, $slotDateMegaId);
        $this->entityManager->persist($result);
        $this->entityManager->flush();

        $sessionNewCreated = null;  //Session nouvellement créée depuis le slotDate
        $newCurrentSlotDate = [];
        foreach ($result->getSlotsDates() as $slotDate) {
            if ($slotDate['statusValidation'] == 1) {
                $demandStatusValidation = 1; //Au moins un n'est pas finalisé
            }
            if ($slotDate['megaId'] == $slotDateMegaId) {
                $newCurrentSlotDate = $slotDate;
                $sessionNewCreated = $this->providerSessionRepository->findOneBy(['ref' => $slotDate['sessionReference']]);
            }
        }

        $onHoldProviderSession->setStatusValidation($demandStatusValidation);
        $this->entityManager->persist($onHoldProviderSession);
        $this->entityManager->flush();

        foreach ($result->getParticipants()['animators'] as $animator) {
            //dump($animator['email']);
            $userClient = $this->userRepository->findOneBy(['email' => $animator['email'], 'client' => $onHoldProviderSession->getClient()]);
            //$findAnimator = false;
            /*$animator = null;
            foreach ($sessionNewCreated->getParticipantRoles() as $participant) {
                if ($participant->getRole() == ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                    //L'animateur fait partie de la session ! A ce stade normalement non !
                }
            }*/
            if (empty($userClient)) {
                $animatorUser = new User();
                $animatorUser->setClient($onHoldProviderSession->getClient())
                    ->setFirstName($animator['forname'])
                    ->setLastName($animator['name'])
                    ->setClearPassword($this->userSecurityService->randomPassword())
                    ->setEmail($animator['email'])
                    ->setRoles(['ROLE_TRAINEE'])
                    ->setPreferredLang('fr');
                $this->entityManager->persist($animatorUser);
                $this->entityManager->flush();
            } else {
                //dump($userClient->getEmail());
                $animatorUser = $userClient;
            }
            /** @var MicrosoftTeamsEventSession $sessionNewCreated */
            if ($animatorUser->getEmail() != $sessionNewCreated->getUserLicence()) {
                $ppsrAnimator = $this->participantSessionSubscriber->subscribe($sessionNewCreated, $animatorUser, ProviderParticipantSessionRole::ROLE_ANIMATOR);
                //dump($ppsrAnimator->getRole());
                //$this->entityManager->persist($result);
                $this->entityManager->flush();
            }
        }

        foreach ($result->getParticipants()['trainees'] as $trainee) {
            //dump($animator['email']);
            $userClient = $this->userRepository->findOneBy(['email' => $trainee['email'], 'client' => $onHoldProviderSession->getClient()]);
            //$findAnimator = false;
            /*$animator = null;
            foreach ($sessionNewCreated->getParticipantRoles() as $participant) {
                if ($participant->getRole() == ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                    //L'animateur fait partie de la session ! A ce stade normalement non !
                }
            }*/
            if (empty($userClient)) {
                $traineeUser = new User();
                $traineeUser->setClient($onHoldProviderSession->getClient())
                    ->setFirstName($trainee['forname'])
                    ->setLastName($trainee['name'])
                    ->setClearPassword($this->userSecurityService->randomPassword())
                    ->setEmail($trainee['email'])
                    ->setRoles(['ROLE_TRAINEE'])
                    ->setPreferredLang('fr');
                $this->entityManager->persist($traineeUser);
                $this->entityManager->flush();
            } else {
                //dump($userClient->getEmail());
                $traineeUser = $userClient;
            }
            $ppsrTrainee = $this->participantSessionSubscriber->subscribe($sessionNewCreated, $traineeUser, ProviderParticipantSessionRole::ROLE_TRAINEE);
            //dump($ppsrAnimator->getRole());
            //$this->entityManager->persist($result);
            $this->entityManager->flush();
        }

        /*foreach ($result->getParticipants()['animators'] as $animator) {
            //dump($animator['email']);
            $userClient = $this->userRepository->findOneBy(['email' => $animator['email'], 'client' => $onHoldProviderSession->getClient()]);
            $animatorUser = $userClient;
            $ppsrAnimator = $this->participantSessionSubscriber->subscribe($sessionNewCreated, $animatorUser, ProviderParticipantSessionRole::ROLE_ANIMATOR);
            $this->entityManager->flush();
        }*/

        return $this->apiResponse([
            'success' => true,
            'onHoldProviderSession' => $this->normalizer->normalize($result, context: ['groups' => ['read_on_hold_session']]),
            'slotDate' => $this->normalizer->normalize($newCurrentSlotDate, context: ['groups' => ['read_on_hold_session']]),
        ]);
    }

    #[Route('/select-modelac/{id}', name: '_on_hold_provider_session_select_modelac', requirements: ['id' => '\d+'], options: ['expose' => true])]
    //#[IsGranted(OnHoldProviderSessionVoter::ACCESS, subject: 'onHoldProviderSession')]
    public function selectModelAC(OnHoldProviderSession $onHoldProviderSession, Request $request): Response
    {
        $demandStatusValidation = 2;
        $slotDateMegaId = $request->query->get('slodDateMegaId');
        $modelACId = $request->query->get('modelACId');
        $result = $this->onHoldProviderSessionService->selectModelACOnDemand($onHoldProviderSession, $modelACId, $slotDateMegaId);
        $this->entityManager->persist($result);
        $this->entityManager->flush();

        $sessionNewCreated = null;  //Session nouvellement créée depuis le slotDate
        $newCurrentSlotDate = [];
        foreach ($result->getSlotsDates() as $slotDate) {
            if ($slotDate['statusValidation'] == 1) {
                $demandStatusValidation = 1; //Au moins un n'est pas finalisé
            }
            if ($slotDate['megaId'] == $slotDateMegaId) {
                $newCurrentSlotDate = $slotDate;
                $sessionNewCreated = $this->providerSessionRepository->findOneBy(['ref' => $slotDate['sessionReference']]);
            }
        }

        $onHoldProviderSession->setStatusValidation($demandStatusValidation);
        $this->entityManager->persist($onHoldProviderSession);
        $this->entityManager->flush();

        foreach ($result->getParticipants()['animators'] as $animator) {
            //dump($animator['email']);
            $userClient = $this->userRepository->findOneBy(['email' => $animator['email'], 'client' => $onHoldProviderSession->getClient()]);
            //$findAnimator = false;
            /*$animator = null;
            foreach ($sessionNewCreated->getParticipantRoles() as $participant) {
                if ($participant->getRole() == ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                    //L'animateur fait partie de la session ! A ce stade normalement non !
                }
            }*/
            if (empty($userClient)) {
                $animatorUser = new User();
                $animatorUser->setClient($onHoldProviderSession->getClient())
                    ->setFirstName($animator['forname'])
                    ->setLastName($animator['name'])
                    ->setClearPassword($this->userSecurityService->randomPassword())
                    ->setEmail($animator['email'])
                    ->setRoles(['ROLE_TRAINEE'])
                    ->setPreferredLang('fr');
                $this->entityManager->persist($animatorUser);
                $this->entityManager->flush();
            } else {
                //dump($userClient->getEmail());
                $animatorUser = $userClient;
            }

            $ppsrAnimator = $this->participantSessionSubscriber->subscribe($sessionNewCreated, $animatorUser, ProviderParticipantSessionRole::ROLE_ANIMATOR);
            //dump($ppsrAnimator->getRole());
            //$this->entityManager->persist($result);
            $this->entityManager->flush();
        }

        foreach ($result->getParticipants()['trainees'] as $trainee) {
            //dump($animator['email']);
            $userClient = $this->userRepository->findOneBy(['email' => $trainee['email'], 'client' => $onHoldProviderSession->getClient()]);
            //$findAnimator = false;
            /*$animator = null;
            foreach ($sessionNewCreated->getParticipantRoles() as $participant) {
                if ($participant->getRole() == ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                    //L'animateur fait partie de la session ! A ce stade normalement non !
                }
            }*/
            if (empty($userClient)) {
                $traineeUser = new User();
                $traineeUser->setClient($onHoldProviderSession->getClient())
                    ->setFirstName($trainee['forname'])
                    ->setLastName($trainee['name'])
                    ->setClearPassword($this->userSecurityService->randomPassword())
                    ->setEmail($trainee['email'])
                    ->setRoles(['ROLE_TRAINEE'])
                    ->setPreferredLang('fr');
                $this->entityManager->persist($traineeUser);
                $this->entityManager->flush();
            } else {
                //dump($userClient->getEmail());
                $traineeUser = $userClient;
            }
            $ppsrTrainee = $this->participantSessionSubscriber->subscribe($sessionNewCreated, $traineeUser, ProviderParticipantSessionRole::ROLE_TRAINEE);
            //dump($ppsrAnimator->getRole());
            //$this->entityManager->persist($result);
            $this->entityManager->flush();
        }

        return $this->apiResponse([
                'success' => true,
                'onHoldProviderSession' => $this->normalizer->normalize($result, context: ['groups' => ['read_on_hold_session']]),
                'slotDate' => $this->normalizer->normalize($newCurrentSlotDate, context: ['groups' => ['read_on_hold_session']]),
            ]);
        /*} else {
            return $this->apiResponse([
                'success' => false,
                'errorDetails' => 'Une erreur ...',
            ]);
        }*/
    }

    #[Route('/select-connector-type/{id}', name: '_on_hold_provider_session_select_connector_type', requirements: ['id' => '\d+'], options: ['expose' => true])]
    //#[IsGranted(OnHoldProviderSessionVoter::ACCESS, subject: 'onHoldProviderSession')]
    public function selectConnectorType(OnHoldProviderSession $onHoldProviderSession, Request $request): Response
    {
        $connectorType = $request->query->get('connectorType');
        $result = $this->onHoldProviderSessionService->selectConnectorType($onHoldProviderSession, $connectorType);
        $this->entityManager->persist($result);
        $this->entityManager->flush();
        //dump($this->normalizer->normalize($result, context: ['groups' => ['read_on_hold_session']]));
        //if ($result instanceof OnHoldProviderSession) {
        return $this->apiResponse([
                'success' => true,
                'onHoldProviderSession' => $this->normalizer->normalize($result, context: ['groups' => ['read_on_hold_session']]),
            ]);
        /*} else {
            return $this->apiResponse([
                'success' => false,
                'errorDetails' => 'Une erreur ...',
            ]);
        }*/
    }

    /*#[Route('/check-status-validation-validate-slots/{id}', name: '_on_hold_provider_session_check_status_validation_validate_slots', requirements: ['id' => '\d+'], options: ['expose' => true])]
    //#[IsGranted(OnHoldProviderSessionVoter::ACCESS, subject: 'onHoldProviderSession')]
    public function checkStatusValidationValidatedSlots(OnHoldProviderSession $onHoldProviderSession, Request $request): Response
    {
        $connectorType = $request->query->get('connectorType');
        $result = $this->onHoldProviderSessionService->checkStatusValidationValidatedSlots($onHoldProviderSession);

        return $this->apiResponse([
            'success' => true,
            'nbrSlotsDatesValidated' => $result,
        ]);
    }*/
}
