<?php

namespace App\Controller;

use App\Entity\Bounce;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Repository\BounceRepository;
use App\Repository\ClientRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\UserRepository;
use App\Security\ImportVoter;
use App\Security\ProviderParticipantVoter;
use App\Service\ImportUserService;
use App\Service\MiddlewareService;
use App\Service\RessourceUploader\RessourceUploader;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/participant')]
#[IsGranted('ROLE_TRAINEE')]
class ParticipantController extends AbstractController
{
    public function __construct(
        protected MiddlewareService $middlewareService,
        protected EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/get/{id}', name: '_participant_get')]
    #[IsGranted(ProviderParticipantVoter::EDIT, subject: 'providerParticipant')]
    public function getParticipant(ProviderParticipant $providerParticipant): Response
    {
        return $this->render('participants/create.html.twig', [
            'title' => 'Participant',
            'provider' => $providerParticipant->getProvider(),
        ]);
    }

    #[Route('/list', name: '_participant_list', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function listParticipants(Request $request, ClientRepository $clientRepository, BounceRepository $bounceRepository): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($this->isGranted(ImportVoter::CAN_SELECT_CLIENT, $user)) {
            $clients = $clientRepository->findAll();
        } else {
            $clients = [$user->getClient()];
        }

        $importRestrictions = [
            'max_file_size' => RessourceUploader::humanReadableFileSize(ImportUserService::MAX_FILE_SIZE),
            'allowed_extension' => implode(', ', ImportUserService::AUTHORIZED_EXTENSION),
            'allowed_max_row' => ImportUserService::ALLOWED_MAX_ROW,
        ];

        if ($request->query->has('pre-search')) {
            $preSearch = (string) $request->query->get('pre-search');
        }

        return $this->render('participants/list.html.twig', [
            'title' => 'Participants',
            'importRestrictions' => $importRestrictions,
            'clients' => $clients,
            'bouncedMails' => $bounceRepository->findBy(['state' => Bounce::TO_CORRECT]),
            'preSearch' => !empty($preSearch) ? $preSearch : null,
        ]);
    }

    #[Route('/fetch_list', name: '_fetch_participant_list', options: ['expose' => true])]
    #[IsGranted('ROLE_TRAINEE')]
    public function fetchParticipants(Request $request, UserRepository $userRepository, SerializerInterface $serializer): Response
    {
        $search = $request->query->get('search', '');
        $firstResult = (int) $request->query->get('firstResult', '1');
        $needCount = (int) $request->query->get('needCount', '0');

        $data['participants'] = $userRepository->findList($search, $firstResult);
        if ($needCount) {
            $data['count'] = $userRepository->countListResult($search);
        }

        $participantsSerialized = $serializer->serialize($data, 'json', ['groups' => 'read_user']);

        return new Response($participantsSerialized);
    }

    #[Route('/fetch_list_session_participant/{id}', name: '_fetch_participant_session_list', options: ['expose' => true])]
    #[IsGranted('ROLE_TRAINEE')]
    public function fetchParticipantsSession(Request $request, ProviderSession $session, ProviderParticipantSessionRoleRepository $participantSessionRoleRepository, SerializerInterface $serializer): Response
    {
        $search = $request->query->get('search', '');
        $firstResult = (int) $request->query->get('firstResult', '1');
        $needCount = (int) $request->query->get('needCount', '0');

        $data['participants'] = $participantSessionRoleRepository->findList(search: $search, providerSession: $session, firstResult: $firstResult);

        if ($needCount) {
            $data['count'] = $participantSessionRoleRepository->countTrainees(providerSession: $session, search: $search);
        }

        $participantsSerialized = $serializer->serialize($data, 'json', ['groups' => 'read_session']);

        return new Response($participantsSerialized);
    }

    #[Route('/fetch_list_session_animator/{id}', name: '_fetch_animator_session_list', options: ['expose' => true])]
    #[IsGranted('ROLE_TRAINEE')]
    public function fetchAnimatorsSession(Request $request, ProviderSession $session, ProviderParticipantSessionRoleRepository $participantSessionRoleRepository, SerializerInterface $serializer): Response
    {
        $search = $request->query->get('search', '');

        $data['participants'] = $participantSessionRoleRepository->findListAnimators(search: $search, providerSession: $session);
        $data['count'] = $participantSessionRoleRepository->countAnimatorsWithSearch(providerSession: $session, search: $search);

        $animatorSerialized = $serializer->serialize($data, 'json', ['groups' => 'read_session']);

        return new Response($animatorSerialized);
    }

    #[Route('/anonymize/{id}', name: '_participant_anonymize', options: ['expose' => true])]
    #[Isgranted('ROLE_ADMIN')]
    public function anonymizeParticipant(User $user, UserService $userService): Response
    {
        $userService->anonymize($user);

        $this->entityManager->flush();

        return $this->redirectToRoute('_participant_list');
    }
}
