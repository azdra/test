<?php

namespace App\Controller;

use App\Entity\ActivityLog;
use App\Entity\Client\Client;
use App\Entity\Convocation;
use App\Form\ConvocationFormType;
use App\Model\ActivityLog\ActivityLogModel;
use App\Model\ApiResponseTrait;
use App\Repository\ProviderSessionRepository;
use App\Service\ActivityLogger;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/convocation')]
#[IsGranted('ROLE_ADMIN')]
class ConvocationController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected SerializerInterface $serializer,
        protected TranslatorInterface $translator,
        private ActivityLogger $activityLogger,
        protected ProviderSessionRepository $providerSessionRepository,
    ) {
    }

    #[Route('/create/{id}', name: '_convocation_create')]
    public function create(Request $request, Client $client, LoggerInterface $logger): Response
    {
        $convocation = new Convocation();
        $convocation->setClient($client);

        $form = $this->createForm(ConvocationFormType::class, $convocation);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->entityManager;
                try {
                    $entityManager->persist($convocation);

                    $this->activityLogger->initActivityLogContext($convocation->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
                    $infos = [
                        'message' => ['key' => 'Creation of convocation template for the account %accountname%.', 'params' => ['%accountName%' => $convocation->getClient()->getName()]],
                    ];
                    $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_UPDATE_CONVOCATION, ActivityLog::SEVERITY_INFORMATION, $infos), true);
                    $this->activityLogger->flushActivityLogs();
                    $entityManager->flush();

                    $this->addFlash('success', 'The convocation was successfully created.');

                    return $this->redirectToRoute('_client_edit', [
                        'id' => $client->getId(),
                        'tab-name' => 'clientEmailsForm',
                    ]);
                } catch (\Exception $e) {
                    $logger->error($e);
                    $this->addFlash('error', 'An error occurred, the convocation was not created.');
                }
            }
        }

        return $this->render('convocations/create.html.twig', [
            'title' => 'Add a convocation template',
            'form' => $form->createView(),
            'clientId' => $client->getId(),
            'formActionUrl' => $this->generateUrl('_convocation_create', ['id' => $client->getId()]),
            'convocation' => $convocation,
        ]);
    }

    #[Route('/update/{id}', name: '_convocation_update', options: ['expose' => true], methods: ['GET', 'POST'])]
    public function updateConvocation(Request $request, Convocation $convocation, LoggerInterface $logger): Response
    {
        $sessionsWithThisConvocation = $this->providerSessionRepository->getProviderSessionsConvocation($convocation);

        $form = $this->createForm(ConvocationFormType::class, $convocation);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->entityManager;
                try {
                    $this->activityLogger->initActivityLogContext($convocation->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
                    $infos = [
                        'message' => ['key' => 'Modification of the template "%convocationName%" of the account %accountName%.', 'params' => ['%convocationName%' => $convocation->getName(), '%accountName%' => $convocation->getClient()->getName()]],
                    ];
                    $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_UPDATE_CONVOCATION, ActivityLog::SEVERITY_INFORMATION, $infos), true);
                    $this->activityLogger->flushActivityLogs();

                    $entityManager->flush();

                    $this->addFlash('success', 'The convocation was successfully created.');

                    return $this->redirectToRoute('_client_edit', [
                        'id' => $convocation->getClient()->getId(),
                        'tab-name' => 'clientEmailsForm',
                    ]);
                } catch (\Exception $e) {
                    $logger->error($e);
                    $this->addFlash('error', 'An error occurred, the convocation was not created.');
                }
            }
        }

        return $this->render('convocations/create.html.twig', [
            'title' => 'Updating a convocation template',
            'clientId' => $convocation->getClient()->getId(),
            'formActionUrl' => $this->generateUrl('_convocation_update', ['id' => $convocation->getId()]),
            'form' => $form->createView(),
            'countSessionsWithThisConvocation' => count($sessionsWithThisConvocation),
        ]);
    }

    #[Route('/disable/{id}', name: '_convocation_disable', options: ['expose' => true], methods: ['GET'])]
    public function disableConvocation(Convocation $convocation, LoggerInterface $logger): Response
    {
        try {
            $convocation->setStatus(Convocation::STATUS_DISABLED);
            $entityManager = $this->entityManager;

            $this->activityLogger->initActivityLogContext($convocation->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
            $infos = [
                'message' => ['key' => 'Deactivated of the "%convocationName%" template from the %accountName% account.', 'params' => ['%convocationName%' => $convocation->getName(), '%accountName%' => $convocation->getClient()->getName()]],
            ];
            $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_UPDATE_CONVOCATION, ActivityLog::SEVERITY_INFORMATION, $infos), true);
            $this->activityLogger->flushActivityLogs();

            $entityManager->flush();

            $this->addFlash('success', 'The convocation has been successfully deactivated.');
        } catch (\Exception $e) {
            $logger->error($e);
            $this->addFlash('error', 'An error has occurred, the convocation cannot be deactivated.');
        }

        return $this->redirectToRoute('_client_edit', [
            'id' => $convocation->getClient()->getId(),
            'tab-name' => 'clientEmailsForm',
        ]);
    }
}
