<?php

namespace App\Controller;

use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Exception\ProviderGenericException;
use App\Model\ApiResponseTrait;
use App\Security\ConfigurationHolderVoter;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/configuration-holder/microsoft-teams-event')]
#[IsGranted('ROLE_MANAGER')]
class MicrosoftTeamsEventConfigurationHolderController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private LoggerInterface $logger,
    ) {
    }

    #[Route('/{id}/licences', name: '_microsoft_teams_event_licences', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ConfigurationHolderVoter::EDIT, subject: 'configurationHolder')]
    public function getMicrosoftTeamsEventLicences(
        MicrosoftTeamsEventConfigurationHolder $configurationHolder,
    ): Response {
        try {
            return $this->apiResponse($configurationHolder->getLicencesShared());
        } catch (ProviderGenericException $exception) {
            $this->logger->error($exception, [
                'id' => $configurationHolder->getId(),
                'type' => 'MicrosoftTeamsEventConfigurationHolder',
            ]);

            return $this->apiErrorResponse('Unable to get the meeting model list', extraData: $exception->getMessage());
        }
    }

    #[Route('/{id}/licences-attributed', name: '_microsoft_teams_event_licences_attributed', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ConfigurationHolderVoter::EDIT, subject: 'configurationHolder')]
    public function getMicrosoftTeamsEventLicencesAttributed(
        MicrosoftTeamsEventConfigurationHolder $configurationHolder,
    ): Response {
        try {
            return $this->apiResponse($configurationHolder->getLicencesAttributed());
        } catch (ProviderGenericException $exception) {
            $this->logger->error($exception, [
                'id' => $configurationHolder->getId(),
                'type' => 'MicrosoftTeamsEventConfigurationHolder',
            ]);

            return $this->apiErrorResponse('Unable to get the meeting model list', extraData: $exception->getMessage());
        }
    }
}
