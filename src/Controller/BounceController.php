<?php

namespace App\Controller;

use App\Entity\Bounce;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Repository\BounceRepository;
use App\Security\BounceVoter;
use App\Security\ProviderSessionVoter;
use App\Security\UserVoter;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/npai')]
#[IsGranted('ROLE_MANAGER')]
class BounceController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private SerializerInterface $serializer
    ) {
    }

    #[Route('/', name: '_bounce_list')]
    public function listBounces(BounceRepository $bounceRepository): Response
    {
        return $this->render('npai/list.html.twig', [
            'title' => 'Invalid emails',
            'bounces' => $bounceRepository->findAllBounces(),
        ]);
    }

    #[Route('/user/{id}', name: '_bounce_list_user_filtered', options: ['expose' => true])]
    #[IsGranted(UserVoter::EDIT, subject: 'user')]
    public function prefiltredBounceListByUser(User $user, BounceRepository $bounceRepository): Response
    {
        return $this->render('npai/list.html.twig', [
            'title' => 'Invalid emails',
            'preSearch' => $user->getEmail(),
            'bounces' => $bounceRepository->findBy([
                'email' => $user->getEmail(),
            ]),
        ]);
    }

    #[Route('/session/{id}', name: '_bounce_list_session_filtered', options: ['expose' => true])]
    #[IsGranted(ProviderSessionVoter::EDIT, subject: 'session')]
    public function prefiltredBounceListBySession(ProviderSession $session, BounceRepository $bounceRepository): Response
    {
        return $this->render('npai/list.html.twig', [
            'title' => 'Invalid emails',
            'bounces' => $bounceRepository->findBy([
                'session' => $session,
            ]),
        ]);
    }

    #[Route('/ignore/{id}', name: '_bounce_ignore', options: ['expose' => true])]
    #[IsGranted(BounceVoter::EDIT, subject: 'bounce')]
    public function ignoreBounce(Request $request, Bounce $bounce): Response
    {
        try {
            $bounce->setState(Bounce::IGNORED);
            $this->entityManager->persist($bounce);
            $this->entityManager->flush();

            $this->addFlash('success', 'The There is no trainer for this session');
        } catch (\Exception $e) {
            $this->addFlash('error', 'An error occurred, the invalid email line was not ignored');
        }

        return $this->redirectToRoute('_bounce_list');
    }

    #[Route('/global-search', name: '_bounce_global_search', options: ['expose' => true], methods: ['GET'], format: 'json')]
    public function globalSearch(Request $request, BounceRepository $bounceRepository): Response
    {
        return new Response(
            $this->serializer->serialize(
                $bounceRepository->globalSearch((string) $request->query->get('q')),
                'json',
                ['groups' => 'read_bounce'],
            )
        );
    }
}
