<?php

namespace App\Controller;

use App\Entity\AsyncTask;
use App\Entity\ProviderSession;
use App\Repository\ActivityLogRepository;
use App\Repository\ClientRepository;
use App\Security\ProviderSessionVoter;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/activities')]
class ActivityController extends AbstractController
{
    private ClientRepository $clientRepository;

    public function __construct(
        private LoggerInterface $logger,
        ClientRepository $clientRepository
    ) {
        $this->clientRepository = $clientRepository;
    }

    #[Route('/', name: '_activity_list', methods: ['GET'])]
    #[IsGranted('ROLE_MANAGER')]
    public function listAll(): Response
    {
        return $this->render('activity/list.html.twig', [
            'title' => 'Activities',
            'clients' => $this->clientRepository->findBy([], ['name' => 'ASC']),
            'client' => $this->getUser()->getClient(),
        ]);
    }

    #[Route('/next', name: '_activity_list_next', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_MANAGER')]
    public function getNextActivities(
        Request $request,
        ActivityLogRepository $activityLogRepository,
        NormalizerInterface $normalizer
    ): Response {
        $last = $request->query->get('last');

        $client = $this->getUser()->getClient();
        if (!empty($request->query->get('account')) && $request->query->get('account') !== 'undefined') {
            $client = $this->clientRepository->find($request->query->get('account'));
        }

        $activities = $activityLogRepository->findAllBefore($client, $last);

        return new Response(json_encode([
            'results' => $normalizer->normalize($activities['results'], context: ['groups' => ['read_grouped_activities']]),
            'count' => $activities['count'],
        ]));
    }

    #[Route('/session/{id}/next', name: '_activity_session_list_next', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ProviderSessionVoter::ACCESS, 'session')]
    public function getNextSessionActivities(
        Request $request,
        ProviderSession $session,
        ActivityLogRepository $activityLogRepository,
        NormalizerInterface $normalizer
    ): Response {
        $last = $request->query->get('last');
        $activities = $activityLogRepository->findAllBeforeForSessionOnly(
            $session->getClient(),
            $session->getCreatedAt()->modify('-1 day')->format('Y-m-d\TH:i:sP'),
            $last,
            ['session.id' => $session->getId()]
        );

        return new Response(json_encode([
            'results' => $normalizer->normalize($activities['results'], context: ['groups' => ['read_grouped_activities']]),
            'count' => $activities['count'],
        ]));
    }

    #[Route('/notifications/next', name: '_activity_notifications_list_next', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_MANAGER')]
    public function getNextNotificationsActivities(
        Request $request,
        ActivityLogRepository $activityLogRepository,
        NormalizerInterface $normalizer
    ): Response {
        $last = $request->query->get('last');
        $activities = $activityLogRepository->findAllBefore($this->getUser()->getClient(), $last);

        return new Response(json_encode([
            'results' => $normalizer->normalize($activities['results'], context: ['groups' => ['read_grouped_activities']]),
            'count' => $activities['count'],
        ]));
    }

    #[Route('/download-import-file/{id}', name: 'download_import_file', options: ['expose' => true], methods: ['GET'])]
    #[Security('is_granted(\'access\', task.getClientOrigin())')]
    public function downloadImportFile(
        AsyncTask $task,
                  $importsSavefilesDirectory
    ): Response {
        $url = $importsSavefilesDirectory.'/'.$task->getFileOrigin();
        if (!file_exists($url)) {
            $exception = new NotFoundHttpException('Unable to found file');
            $this->logger->error($exception, ['url' => $url]);
            throw new NotFoundHttpException('Unable to found file');
        }

        $filename = basename($url);
        $response = new BinaryFileResponse($url);
        $response->headers->set('Cache-Control', 'max-age=0');
        $response->headers->set('Content-Disposition', "attachment; filename=$filename");

        return $response;
    }

    #[Route('/download-export-file/{filename}', name: 'download_export_file', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_MANAGER')]
    public function downloadExportFile(
        string $filename,
               $exportsSavefilesDirectory
    ): Response {
        $url = $exportsSavefilesDirectory.'/'.$filename;
        if (!file_exists($url)) {
            $exception = new NotFoundHttpException('Unable to found file');
            $this->logger->error($exception, ['url' => $url]);
            throw new NotFoundHttpException('Unable to found file');
        }

        $filename = basename($url);
        $response = new BinaryFileResponse($url);
        $response->headers->set('Cache-Control', 'max-age=0');
        $response->headers->set('Content-Disposition', "attachment; filename=$filename");

        return $response;
    }
}
