<?php

namespace App\Controller;

use App\Model\ApiResponseTrait;
use App\Service\licensesAvailableService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/licenses')]
#[IsGranted('ROLE_MANAGER')]
class LicencesAvailableController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private readonly NormalizerInterface $normalizer,
        private readonly licensesAvailableService $licensesAvailableService,
    ) {
    }

    #[Route('/available', name: '_licenses_available', methods: ['GET', 'POST'])]
    public function licencesAvailable(): Response
    {
        return $this->render('licenses/licenses_available.html.twig', [
            'title' => 'Licenses',
            'configurationsHolder' => $this->licensesAvailableService->getWebexConfigurationHolder($this->getUser()),
        ]);
    }

    #[Route('/time-slot-available', name: '_time_slot_available', options: ['expose' => true], methods: ['GET'])]
    public function timeSlotDataAvailableLicenses(Request $request): Response
    {
        return $this->apiResponse($this->licensesAvailableService->getTimeSlotDataAvailableLicenses($request->query->all()));
    }
}
