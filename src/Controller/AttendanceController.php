<?php

namespace App\Controller;

use App\Entity\ParticipantSessionSignature;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Exception\ProviderSessionAccessToken\InvalidProviderSessionAccessTokenException;
use App\Exception\ProviderSessionAccessToken\UnauthorizedProviderAccessTokenException;
use App\Model\ApiResponseTrait;
use App\Repository\ParticipantSessionSignatureRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Security\ProviderSessionVoter;
use App\Service\MailerService;
use App\Service\ProviderParticipantSessionRoleService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/attendance')]
class AttendanceController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private LoggerInterface $logger,
        private EntityManagerInterface $entityManager,
        private ProviderParticipantSessionRoleService $providerParticipantSessionRoleService,
        private ParticipantSessionSignatureRepository $participantSessionSignatureRepository
    ) {
    }

    #[Route('/signature/lobby/{id}', name: '_session_signature_lobby', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['GET'])]
    public function lobbySignatureSession(ProviderSession $providerSession): Response
    {
        //dd($providerSession->getTraineesOnly());
        $trainees = [];
        if (!$providerSession->getClient()->isFillEmail()) {
            $trainees = $providerSession->getTraineesOnly();
        }

        return $this->render('sessions/signature/lobby.html.twig', [
            'title' => 'End-of-session attendance form',
            'client' => $providerSession->getClient(),
            'providerSession' => $providerSession,
            'participantRoles' => $trainees,
            'fillEmail' => $providerSession->getClient()->isFillEmail(),
        ]);
    }

    #[Route('/signature/reset-lobby/{id}', name: '_session_signature_reset_lobby', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['GET'])]
    public function resetLobbySignatureSession(ProviderParticipantSessionRole $providerParticipantSessionRole): Response
    {
        try {
            $signaturesParticipant = $this->participantSessionSignatureRepository->findBy(['providerParticipantSessionRole' => $providerParticipantSessionRole]);

            foreach ($signaturesParticipant as $sign) {
                $this->entityManager->remove($sign);
            }
            $providerParticipantSessionRole->setManualPresenceStatus(false);
            $this->entityManager->persist($providerParticipantSessionRole);
            $this->entityManager->flush();

            return $this->apiResponse([]);
        } catch (\Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse('Credit could not be added');
        }
    }

    #[Route('/signature/save/{token}', name: '_session_signature_ppsr_save', options: ['expose' => true], methods: ['POST'])]
    public function saveParticipantSignature(
        string $token,
        Request $request
    ): Response {
        try {
            $participantSessionRole = $this->providerParticipantSessionRoleService->getAndValidFromToken($token);

            $data = json_decode($request->getContent(), true);

            $participantSessionSignature = new ParticipantSessionSignature();
            $participantSessionSignature->setSignatureForTheSessionOf(new \DateTime($data['signatureForTheSessionOf']))
                ->setDayPartIndicator($data['dayPartIndicator'])
                ->setSignature($data['signature'])
                ->setProviderParticipantSessionRole($participantSessionRole);
            $this->entityManager->persist($participantSessionSignature);

            if (!$participantSessionRole->isPresent()) {
                $participantSessionRole->setManualPresenceStatus(true);
            }

            $this->entityManager->flush();
        } catch (InvalidProviderSessionAccessTokenException $exception) {
            $this->logger->error($exception, ['token' => $token]);

            return $this->apiErrorResponse($exception->getMessage());
        } catch (UnauthorizedProviderAccessTokenException $exception) {
            $this->logger->error($exception, ['token' => $token]);

            return $this->apiErrorResponse('You are not allowed to sign for this session');
        } catch (\Exception $e) {
            if (isset($participantSessionRole)) {
                $context = [
                    'token' => $token,
                    'participantSessionRoleId' => $participantSessionRole->getId(),
                    'participant' => ['id' => $participantSessionRole->getParticipant()->getId(), 'email' => $participantSessionRole->getParticipant()->getEmail()],
                    'session' => ['id' => $participantSessionRole->getSession()->getId(), 'name' => $participantSessionRole->getSession()->getId()],
                ];
            } else {
                $context = ['token' => $token];
            }
            $this->logger->error($e, $context);

            return $this->apiErrorResponse($e->getMessage());
        }

        return new Response();
    }

    #[Route('/send-email-signature/{id}', name: '_send_email_signature', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted(ProviderSessionVoter::ACCESS, subject: 'session')]
    public function sendAttendanceSignatureToParticipant(ProviderSession $session, Request $request, TranslatorInterface $translator, MailerService $mailerService, ProviderParticipantSessionRoleRepository $participantSessionRoleRepository): Response
    {
        /** @var array $participants */
        $participants = explode(',', $request->query->get('participant'));

        if (empty($participants)) {
            return $this->apiErrorResponse('No participants are selected');
        }

        foreach ($participants as $participantSessionRoleId) {
            /** @var ProviderParticipantSessionRole $participantSessionRole */
            $participantSessionRole = $participantSessionRoleRepository->find($participantSessionRoleId);
            $user = $participantSessionRole->getParticipant()->getUser();

            try {
                $mailerService->sendTemplatedMail(
                    $user->getEmail(),
                    $translator->trans('Session attendance signature', locale: $user->getPreferredLang()),
                    'emails/participant/attendance_link.html.twig',
                    context: [
                        'user' => $user,
                        'participant' => $participantSessionRole->getParticipant(),
                        'client' => $user->getClient(),
                        'session' => $participantSessionRole->getSession(),
                    ],
                    metadatas: [
                        'origin' => MailerService::ORIGIN_EMAIL_SEND_SIGNATURE_LINK,
                        'user' => $user->getId(),
                        'client' => $user->getClient()->getId(),
                    ]
                );
            } catch (\Exception $exception) {
                $this->logger->error($exception, [
                    'id session' => $session->getId(),
                    'id configurationHolder' => $session->getAbstractProviderConfigurationHolder()->getId(),
                ]);

                return $this->apiErrorResponse($exception->getMessage());
            }
        }

        $this->addFlash('success', 'Email sent to each selected participant');

        return $this->apiResponse('sucess');
    }

    #[Route('/signature/qrcode/{id}', name: '_session_signature_qrcode', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['GET'])]
    public function lobbySignatureQRCodeSession(ProviderSession $providerSession): Response
    {
        return $this->render('sessions/signature/qr_code.html.twig', [
            'title' => 'End-of-session attendance QR Code',
            'client' => $providerSession->getClient(),
            'providerSession' => $providerSession,
        ]);
    }
}
