<?php

namespace App\Controller;

use App\Entity\OpenAI;
use App\Entity\ProviderSession;
use App\Model\ApiResponseTrait;
use App\Repository\OpenAIRepository;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\ChatGPTService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/gptapi')]
class ChatGPTController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(private SerializerInterface $serializer, private ChatGPTService $chatGPTService, private SubjectRepository $subjectRepository)
    {
    }

    #[Route('/correct', name: '_gptapi_correct', options: ['expose' => true], methods: ['POST'])]
    public function correctTitle(Request $request): Response
    {
        $request = json_decode($request->getContent(), true);

        if (!$request || array_key_exists('title', $request) === false) {
            return $this->apiErrorResponse('Missing title');
        }

        return $this->apiResponse($this->chatGPTService->correctTitle($request['title'], $request['lang']));
    }

    #[Route('/shorten', name: '_gptapi_shoten', options: ['expose' => true], methods: ['POST'])]
    public function shortenTitle(Request $request): Response
    {
        $request = json_decode($request->getContent(), true);

        if (!$request || array_key_exists('title', $request) === false) {
            return $this->apiErrorResponse('Missing title');
        }

        return $this->apiResponse($this->chatGPTService->shortenTitle($request['title'], $request['lang']));
    }

    #[Route('/generate', name: '_gptapi_generate', options: ['expose' => true], methods: ['POST'])]
    public function generateTitle(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        return $this->apiResponse($this->chatGPTService->generateTitle($data['additionalComment'], $data['category'], $data['emoji'], $data['tone'], $data['lang']));
    }

    #[Route('/generate/session/{session}/registration/program', name: '_gptapi_generate_session_registration_program', options: ['expose' => true], methods: ['POST'])]
    public function generateSessionRegistrationProgram(Request $request, ProviderSession $session): Response
    {
        $data = json_decode($request->getContent(), true);

        return $this->apiResponse($this->chatGPTService->generateSessionRegistrationProgram($data, $session));
    }

    #[Route('/generate/catalog/subject/description/{subject}', name: '_gptapi_generate_catalog_subject_description', options: ['expose' => true], methods: ['POST'])]
    public function generateCatalogDescription(Request $request, string $subject): Response
    {
        $subject = $this->subjectRepository->find($subject);
        $data = json_decode($request->getContent(), true);

        return $this->apiResponse($this->chatGPTService->generateCatalogSubjectDescription($data, $subject));
    }

    #[Route('/generate/catalog/subject/{subject}/program', name: '_gptapi_generate_catalog_subject_program', options: ['expose' => true], methods: ['POST'])]
    public function generateCatalogSubjectProgram(Request $request, string $subject): Response
    {
        $subject = $this->subjectRepository->find($subject);
        $data = json_decode($request->getContent(), true);

        return $this->apiResponse($this->chatGPTService->generateCatalogSubjectProgram($data, $subject));
    }

    #[Route('/openai', name: '_chatgpt_openai_home_page', options: ['expose' => true], methods: ['GET'])]
    public function openaiHomePage(OpenAIRepository $openAIRepository): Response
    {
        $request = $openAIRepository->findAll();
        $request = $request[0] ?? (new OpenAI())->setError('Missing OpenAI session key');

        return $this->render('chat-gpt/openai_home_page.html.twig', [
            'credit' => $request,
            'title' => 'OpenAI Credit Management',
        ]);
    }

    #[Route('/openai/update', name: '_chatgpt_openai_update_session', options: ['expose' => true], methods: ['POST'])]
    public function updateOpenAISession(Request $request, NormalizerInterface $normalizer): Response
    {
        $request = json_decode($request->getContent(), true);
        $openAiData = $this->chatGPTService->getCreditGrants($request['sessionId'], $request['creditThreshold']);

        return $this->apiResponse($normalizer->normalize($openAiData));
    }
}
