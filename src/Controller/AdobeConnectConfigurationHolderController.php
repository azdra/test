<?php

namespace App\Controller;

use App\Entity\Adobe\AdobeConnectTelephonyProfile;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Exception\ProviderGenericException;
use App\Model\ApiResponseTrait;
use App\Security\ConfigurationHolderVoter;
use App\Service\AdobeConnectService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/configuration-holder/adobe-connect')]
#[IsGranted('ROLE_MANAGER')]
class AdobeConnectConfigurationHolderController extends AbstractController
{
    use ApiResponseTrait;

    private AdobeConnectService $adobeConnectService;
    private LoggerInterface  $logger;

    public function __construct(AdobeConnectService $adobeConnectService, LoggerInterface $logger)
    {
        $this->adobeConnectService = $adobeConnectService;
        $this->logger = $logger;
    }

    #[Route('/{id}/shared-meeting-templates', name: '_adobe_connect_shared_meeting_templates', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ConfigurationHolderVoter::EDIT, subject: 'configurationHolder')]
    public function getAdobeConnectSharedMeetingTempaltate(AdobeConnectConfigurationHolder $configurationHolder, ?string $filter = 'meeting'): Response
    {
        try {
            if (empty($configurationHolder->getHostGroupScoId())) {
                return $this->apiResponse([]);
            }
            $response = $this->adobeConnectService
                    ->getSharedMeetingsTemplate($configurationHolder, $filter);
            $response = $this->adobeConnectService
                ->findAllModelsRoom($configurationHolder, $filter);

            return ($response->isSuccess())
                ? $this->apiResponse($response->getData())
                : $this->apiErrorResponse('Unable to get the meeting list',
                    extraData: $response->getThrown()->getMessage());
        } catch (ProviderGenericException $exception) {
            $this->logger->error($exception, [
                'id' => $configurationHolder->getId(),
                'Type' => 'AdobeConnectConfigurationHolder',
            ]);

            return $this->apiErrorResponse('Unable to get the meeting model list', extraData: $exception->getMessage());
        }
    }

    #[Route('/{id}/licences', name: '_adobe_connect_licences', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ConfigurationHolderVoter::EDIT, subject: 'configurationHolder')]
    public function getAdobeConnectLicences(
        AdobeConnectConfigurationHolder $configurationHolder,
        NormalizerInterface $normalizer
    ): Response {
        try {
            if (empty($configurationHolder->getHostGroupScoId())) {
                return $this->apiResponse([]);
            }

            $response = $this->adobeConnectService
                ->getPrincipalInHostGroup($configurationHolder);

            return ($response->isSuccess())
                ? $this->apiResponse($normalizer->normalize($response->getData(),
                    context: ['groups' => ['read_principal']]))
                : $this->apiErrorResponse('Unable to get the meeting list',
                    extraData: $response->getThrown()->getMessage());
        } catch (ProviderGenericException $exception) {
            $this->logger->error($exception, [
                'id' => $configurationHolder->getId(),
                'Type' => 'AdobeConnectConfigurationHolder',
            ]);

            return $this->apiErrorResponse('Unable to get the meeting model list', extraData: $exception->getMessage());
        }
    }

    #[Route('/{id}/audios', name: '_adobe_connect_audios', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ConfigurationHolderVoter::EDIT, subject: 'configurationHolder')]
    public function getAdobeConnectAudios(
        AdobeConnectConfigurationHolder $configurationHolder,
        NormalizerInterface $normalizer,
        EntityManagerInterface $entityManager
    ): Response {
        try {
            if (empty($configurationHolder->getHostGroupScoId())) {
                return $this->apiResponse($normalizer->normalize([], context: ['groups' => ['read_audio']]));
            }
            $response = $this->adobeConnectService
                ->getAllAudiosToCreate($configurationHolder);

            /** @var AdobeConnectTelephonyProfile $audio */
            $audiosToCreate = $response->getData();
            foreach ($audiosToCreate as $audio) {
                $entityManager->persist($audio);
                $configurationHolder->addAudioProvider($audio);
            }

            $entityManager->flush();

            $audios = $configurationHolder->getAudioProviders();

            return ($response->isSuccess())
                ? $this->apiResponse($normalizer->normalize($audios, context: ['groups' => ['read_audio']]))
                : $this->apiErrorResponse('Unable to get the audio list',
                    extraData: $response->getThrown()->getMessage());
        } catch (ProviderGenericException $exception) {
            $this->logger->error($exception, [
                'id' => $configurationHolder->getId(),
                'Type' => 'AdobeConnectConfigurationHolder',
            ]);

            return $this->apiErrorResponse('Unable to get the audio list', extraData: $exception->getMessage());
        }
    }

    #[Route('/update/number/audios/{id}', name: '_adobe_connect_audios_update_number', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function updateAdobeConnectAudioNumber(Request $request, EntityManagerInterface $entityManager, NormalizerInterface $normalizer, AdobeConnectTelephonyProfile $adobeConnectTelephonyProfile): Response
    {
        try {
            $adobeConnectTelephonyProfile->setPhoneNumber(
                $request->query->get('number')
            );
            $entityManager->flush();

            return $this->apiResponse($normalizer->normalize($adobeConnectTelephonyProfile, context: ['groups' => ['read_audio']]));
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'id' => $adobeConnectTelephonyProfile->getId(),
                'Type' => 'adobeConnectTelephonyProfile',
            ]);

            return $this->apiErrorResponse('Unable to update the number of this audio profil', extraData: $exception->getMessage());
        }
    }

    #[Route('/update/language/audios/{id}', name: '_adobe_connect_audios_update_language', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function updateAdobeConnectAudioLanguage(Request $request, EntityManagerInterface $entityManager, NormalizerInterface $normalizer, AdobeConnectTelephonyProfile $adobeConnectTelephonyProfile): Response
    {
        try {
            $adobeConnectTelephonyProfile->setPhoneLang(
                $request->query->get('language')
            );
            $entityManager->flush();

            return $this->apiResponse($normalizer->normalize($adobeConnectTelephonyProfile, context: ['groups' => ['read_audio']]));
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'id' => $adobeConnectTelephonyProfile->getId(),
                'Type' => 'AdobeConnectTelephonyProfile',
            ]);

            return $this->apiErrorResponse('Unable to update the language of this audio profile', extraData: $exception->getMessage());
        }
    }
}
