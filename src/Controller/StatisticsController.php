<?php

namespace App\Controller;

use App\Entity\ProviderSession;
use App\Entity\User;
use App\Entity\VideoReplay;
use App\Entity\VideoReplayViewer;
use App\Model\ApiResponseTrait;
use App\Repository\ProviderSessionRepository;
use App\Repository\VideoReplayRepository;
use App\Service\StatisticsService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/statistics')]
class StatisticsController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private StatisticsService $statisticsService,
        private ProviderSessionRepository $providerSessionRepository,
        private VideoReplayRepository $videoReplayRepository,
        private EntityManagerInterface $entityManager
    ) {
    }

    #[Route('/', name: '_statistics', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function statistics(Request $request): Response
    {
        $data = $request->query;
        if ($data->get('from') == null || $data->get('to') == null) {
            $dateStart = (new \DateTime())->modify('-1 month')->setTime(00, 00, 00);
            $dateEnd = (new \DateTime())->setTime(23, 59, 59);
        } else {
            $dateStart = (new \DateTime($data->get('from')))->setTime(00, 00, 00);
            $dateEnd = (new \DateTime($data->get('to')))->setTime(23, 59, 59);
        }

        /** @var ProviderSession[] $sessions */
        $sessions = $this->providerSessionRepository->findSessionForPeriod(dateStart: $dateStart, dateEnd: $dateEnd);
        $videoReplays = $this->videoReplayRepository->findReplayForPeriod(dateStart: $dateStart, dateEnd: $dateEnd);

        $resultStatSessions = $this->statisticsService->getStatisticsSessionsForPeriod($dateStart, $dateEnd, $sessions);
        $resultStatReplaysVideos = $this->statisticsService->getStatisticsReplayForPeriod($sessions, $videoReplays);

        return $this->render('statistics/statistics.html.twig', [
            'title' => 'Statistics',
            'statisticsSessions' => $resultStatSessions,
            'statisticsReplaysVideos' => $resultStatReplaysVideos,
            'dateStart' => $dateStart->format("Y-m-d\Th:i:s.000\Z"),
            'dateEnd' => $dateEnd->format("Y-m-d\Th:i:s.000\Z"),
        ]);
    }

    #[Route('/evaluations', name: '_sessions_evaluations_home', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function home(Request $request, ProviderSessionRepository $providerSessionRepository): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $client = $user->getClient();
        $availableSurveys = $client->getEvaluations();

        $data = $request->query;
        if ($data->get('from') == null || $data->get('to') == null) {
            $dateStart = (new \DateTimeImmutable())->modify('-1 month')->setTime(00, 00, 00);
            $dateEnd = (new \DateTimeImmutable())->setTime(23, 59, 59);
        } else {
            $dateStart = (new \DateTimeImmutable($data->get('from')))->setTime(00, 00, 00);
            $dateEnd = (new \DateTimeImmutable($data->get('to')))->setTime(23, 59, 59);
        }

        $surveyId = '';
        if ($data->has('surveyId') && $data->get('surveyId') !== null) {
            $surveyId = $data->get('surveyId');
        } elseif (count($availableSurveys) > 0) {
            $surveyId = $availableSurveys->first()->getSurveyId();
        } else {
            $this->addFlash('warning', 'There is no evaluation form attached to this account');
        }

        $showTabulator = false;
        if ($data->has('showTabulator') && $data->get('showTabulator') !== null) {
            $showTabulator = $data->getBoolean('showTabulator');
        }

        $evaluationsAnswer = $providerSessionRepository->getEvaluationsAnswerByPeriod($user->getClient(), $dateStart, $dateEnd, $surveyId);

        return $this->render('statistics/evaluations.html.twig', [
            'title' => 'Evaluations',
            'evaluationsAnswer' => json_encode($evaluationsAnswer),
            'dateStart' => $dateStart->format("Y-m-d\Th:i:s.000\Z"),
            'dateEnd' => $dateEnd->format("Y-m-d\Th:i:s.000\Z"),
            'surveySelected' => $surveyId,
            'surveysAvailable' => $availableSurveys,
            'showTabulator' => $showTabulator,
        ]);
    }

    #[Route('/video/{id}/new-viewer', name: '_new_viewer_replay', options: ['expose' => true], methods: ['POST'])]
    public function addViewerToVideoReplay(VideoReplay $videoReplay): Response
    {
        // Retrieve the ViewerReplay for the current user and this video replay
        $viewerReplay = $this->entityManager->getRepository(VideoReplayViewer::class)->findOneBy(['user' => $this->getUser(), 'videoReplay' => $videoReplay]);

        // Retrieve the timezone from the client associated with the video
        $timeZone = new \DateTimeZone($videoReplay->getModule()->getClient()->getTimezone());

        // If the ViewerReplay does not exist, create it
        if ($viewerReplay === null) {
            $viewerReplay = new VideoReplayViewer();
            $viewerReplay->setUser($this->getUser())
                         ->setVideoReplay($videoReplay)
                         ->setLastShowReplay((new \DateTimeImmutable())->setTimezone($timeZone));

            $this->entityManager->persist($viewerReplay);
            $this->entityManager->flush();
        } else {
            // Check if the user has already viewed the video in the last minute
            if ($this->statisticsService->isRecentView($viewerReplay)) {
                // Increment the number of views and update the timestamp of the last view
                $viewerReplay->incrementSeen();
                $viewerReplay->setLastShowReplay((new \DateTimeImmutable())->setTimezone($timeZone));
                $this->entityManager->flush();
            }
        }

        // Respond with an empty API Response
        return $this->apiResponse([]);
    }
}
