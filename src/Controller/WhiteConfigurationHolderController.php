<?php

namespace App\Controller;

use App\Model\ApiResponseTrait;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/configuration-holder/white-connector')]
#[IsGranted('ROLE_MANAGER')]
class WhiteConfigurationHolderController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private LoggerInterface $logger,
    ) {
    }
}
