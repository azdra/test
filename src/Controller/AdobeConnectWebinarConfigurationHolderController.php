<?php

namespace App\Controller;

use App\Entity\Adobe\AdobeConnectWebinarTelephonyProfile;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Exception\ProviderGenericException;
use App\Model\ApiResponseTrait;
use App\Security\ConfigurationHolderVoter;
use App\Service\AdobeConnectWebinarService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[Route('/configuration-holder/adobe-connect-webinar')]
#[IsGranted('ROLE_MANAGER')]
class AdobeConnectWebinarConfigurationHolderController extends AbstractController
{
    use ApiResponseTrait;

    private AdobeConnectWebinarService $adobeConnectWebinarService;
    private LoggerInterface  $logger;

    public function __construct(AdobeConnectWebinarService $adobeConnectWebinarService, LoggerInterface $logger)
    {
        $this->adobeConnectWebinarService = $adobeConnectWebinarService;
        $this->logger = $logger;
    }

    #[Route('/{id}/shared-webinar-templates', name: '_adobe_connect_shared_webinar_templates', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ConfigurationHolderVoter::EDIT, subject: 'configurationHolder')]
    public function getAdobeConnectSharedWebinarTempaltate(AdobeConnectWebinarConfigurationHolder $configurationHolder): Response
    {
        try {
            if (empty($configurationHolder->getHostGroupScoId())) {
                return $this->apiResponse([]);
            }
            $response = $this->adobeConnectWebinarService
                    ->getSharedMeetingsTemplate($configurationHolder);

            return ($response->isSuccess())
                ? $this->apiResponse($response->getData())
                : $this->apiErrorResponse('Unable to get the meeting list',
                    extraData: $response->getThrown()->getMessage());
        } catch (ProviderGenericException $exception) {
            $this->logger->error($exception, [
                'id' => $configurationHolder->getId(),
                'Type' => 'AdobeConnectWebinarConfigurationHolder',
            ]);

            return $this->apiErrorResponse('Unable to get the meeting model list', extraData: $exception->getMessage());
        }
    }

    #[Route('/{id}/webinar-licences', name: '_adobe_connect_webinar_licences', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ConfigurationHolderVoter::EDIT, subject: 'configurationHolder')]
    public function getAdobeConnectWebinarLicences(
        AdobeConnectWebinarConfigurationHolder $configurationHolder,
        NormalizerInterface $normalizer
    ): Response {
        try {
            if (empty($configurationHolder->getHostGroupScoId())) {
                return $this->apiResponse([]);
            }

            $response = $this->adobeConnectWebinarService
                ->getPrincipalInHostGroup($configurationHolder);

            return ($response->isSuccess())
                ? $this->apiResponse($normalizer->normalize($response->getData(),
                    context: ['groups' => ['read_principal']]))
                : $this->apiErrorResponse('Unable to get the meeting list',
                    extraData: $response->getThrown()->getMessage());
        } catch (ProviderGenericException $exception) {
            $this->logger->error($exception, [
                'id' => $configurationHolder->getId(),
                'Type' => 'AdobeConnectWebinarConfigurationHolder',
            ]);

            return $this->apiErrorResponse('Unable to get the meeting model list', extraData: $exception->getMessage());
        }
    }

    #[Route('/{id}/webinar_audios', name: '_adobe_connect_webinar_audios', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ConfigurationHolderVoter::EDIT, subject: 'configurationHolder')]
    public function getAdobeConnectWebinarAudios(
        AdobeConnectWebinarConfigurationHolder $configurationHolder,
        NormalizerInterface $normalizer,
        EntityManagerInterface $entityManager
    ): Response {
        try {
            if (empty($configurationHolder->getHostGroupScoId())) {
                return $this->apiResponse($normalizer->normalize([], context: ['groups' => ['read_audio']]));
            }
            $response = $this->adobeConnectWebinarService
                ->getAllAudiosToCreate($configurationHolder);

            /** @var AdobeConnectWebinarTelephonyProfile $audio */
            $audiosToCreate = $response->getData();
            foreach ($audiosToCreate as $audio) {
                $entityManager->persist($audio);
                $configurationHolder->addAudioProvider($audio);
            }

            $entityManager->flush();

            $audios = $configurationHolder->getAudioProviders();

            return ($response->isSuccess())
                ? $this->apiResponse($normalizer->normalize($audios, context: ['groups' => ['read_audio']]))
                : $this->apiErrorResponse('Unable to get the audio list',
                    extraData: $response->getThrown()->getMessage());
        } catch (ProviderGenericException $exception) {
            $this->logger->error($exception, [
                'id' => $configurationHolder->getId(),
                'Type' => 'AdobeConnectWebinarConfigurationHolder',
            ]);

            return $this->apiErrorResponse('Unable to get the audio list', extraData: $exception->getMessage());
        }
    }

    #[Route('/update/number/audios/{id}', name: '_adobe_connect_webinar_audios_update_number', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function updateAdobeConnectWebinarAudioNumber(Request $request, EntityManagerInterface $entityManager, NormalizerInterface $normalizer, AdobeConnectWebinarTelephonyProfile $adobeConnectWebinarTelephonyProfile): Response
    {
        try {
            $adobeConnectWebinarTelephonyProfile->setPhoneNumber(
                $request->query->get('number')
            );
            $entityManager->flush();

            return $this->apiResponse($normalizer->normalize($adobeConnectWebinarTelephonyProfile, context: ['groups' => ['read_audio']]));
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'id' => $adobeConnectWebinarTelephonyProfile->getId(),
                'Type' => 'adobeConnectWebinarTelephonyProfile',
            ]);

            return $this->apiErrorResponse('Unable to update the number of this audio profil', extraData: $exception->getMessage());
        }
    }

    #[Route('/update/language/webinar-audios/{id}', name: '_adobe_connect_webinar_audios_update_language', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function updateAdobeConnectWebinarAudioLanguage(Request $request, EntityManagerInterface $entityManager, NormalizerInterface $normalizer, AdobeConnectWebinarTelephonyProfile $adobeConnectWebinarTelephonyProfile): Response
    {
        try {
            $adobeConnectWebinarTelephonyProfile->setPhoneLang(
                $request->query->get('language')
            );
            $entityManager->flush();

            return $this->apiResponse($normalizer->normalize($adobeConnectWebinarTelephonyProfile, context: ['groups' => ['read_audio']]));
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'id' => $adobeConnectWebinarTelephonyProfile->getId(),
                'Type' => 'AdobeConnectWebinarTelephonyProfile',
            ]);

            return $this->apiErrorResponse('Unable to update the language of this audio profile', extraData: $exception->getMessage());
        }
    }
}
