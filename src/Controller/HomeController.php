<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class HomeController extends AbstractController
{
    public function __construct(
        protected SerializerInterface $serializer,
        protected NormalizerInterface $normalizer
    ) {
    }

    #[Route('/', name: '_home')]
    #[IsGranted('ROLE_TRAINEE')]
    public function home(Request $request): Response
    {
        $request->getSession()->set('_locale', $this->getUser()->getPreferredLang());

        return $this->redirectToRoute('_session_viewer');
    }

    #[Route('/webex-rest/result', name: '_webex_rest_result')]
    public function getWebexRestResult(Request $request): Response
    {
        $authorizationCode = $request->query->get('code');

        // Votre logique pour échanger l'autorisation contre un token

        // Exemple d'appel HTTPS pour échanger le code d'autorisation
        $httpClient = HttpClient::create();
        $response = $httpClient->request('POST', 'https://webex.com/oauth/token', [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'body' => [
                'grant_type' => 'authorization_code',
                'client_id' => 'C221e652b9a4a942f5481334dcc533f78b0eef1109fb49f0db52b9eaa7f3a8f6b',
                'client_secret' => '5002d587ac9ce5791df9412f2b350f63fbd4bdd56f8a64ba14300cb7551583a7',
                'code' => $authorizationCode,
                'redirect_uri' => $this->generateUrl('_webex_rest_result', []), // 'http://mlsm300.ccu/webex-rest/result', //$this->generateUrl('_webex_rest_result', [], UrlGeneratorInterface::ABSOLUTE_URL),
            ],
        ]);

        // Traitez la réponse et renvoyez-la si nécessaire
        $responseData = $response->toArray();

        return new JsonResponse($responseData);
    }

    #[Route('/main-global-search', name: '_main_global_search', options: ['expose' => true], methods: ['GET'], format: 'json')]
    #[IsGranted('ROLE_MANAGER')]
    public function mainGlobalSearch(Request $request, ProviderSessionRepository $providerSessionRepository, UserRepository $userRepository): Response
    {
        $responseSessions = $this->normalizer->normalize(
            $providerSessionRepository->globalSearch(
                (string) $request->query->get('q'),
                $request->query->getBoolean('allow-outdated', false)
            ),
            context: ['groups' => ['read_session']]
        );
        $responseUsers = $this->normalizer->normalize(
            $userRepository->globalSearchByRoleTarget(
                (string) $request->query->get('q'),
                User::ROLE_TRAINEE,
                $this->isGranted(User::ROLE_ADMIN) ? null : $this->getUser()->getClient()
            ),
            context: ['groups' => ['read_user']]
        );

        $response = $this->serializer->serialize(
            [
                $responseSessions,
                $responseUsers,
            ],
            'json'
        );

        return new Response($response);
    }
}
