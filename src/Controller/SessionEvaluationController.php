<?php

namespace App\Controller;

use App\Entity\CategorySessionType;
use App\Entity\Evaluation;
use App\Entity\EvaluationParticipantAnswer;
use App\Entity\EvaluationProviderSession;
use App\Entity\EvaluationSessionParticipantAnswer;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Model\ApiResponseTrait;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Service\EvaluationParticipantAnswerService;
use App\Service\ProviderParticipantSessionRoleService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/session/evaluation')]
class SessionEvaluationController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        private EvaluationParticipantAnswerService $evaluationParticipantAnswerService,
        private ProviderParticipantSessionRoleService $providerParticipantSessionRoleService,
        private SerializerInterface $serializer
    ) {
    }

    #[Route('/lobby/{id}', name: '_session_evaluation_lobby', options: ['expose' => true])]
    public function lobbyEvaluationSession(ProviderSession $providerSession): Response
    {
        if ($providerSession->getCategory() == CategorySessionType::CATEGORY_SESSION_WEBINAR) {
            return $this->render('sessions/evaluation/lobby_webinar.html.twig', [
                'title' => 'End-of-session evaluation form',
                'client' => $providerSession->getClient(),
                'providerSession' => $providerSession,
                'participantRoles' => [],
            ]);
        } else {
            return $this->render('sessions/evaluation/lobby.html.twig', [
                'title' => 'End-of-session evaluation form',
                'client' => $providerSession->getClient(),
                'providerSession' => $providerSession,
                'participantRoles' => $providerSession->getParticipantRoles()->filter(
                    function (ProviderParticipantSessionRole $participantSessionRole) {
                        return $participantSessionRole->isATrainee();
                    }
                ),
            ]);
        }
    }

    #[Route('/filter-search/{id}', name: '_session_evaluation_lobby_filter_search', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['GET'], format: 'json')]
    public function lobbyFilterSearch(Request $request, ProviderSession $providerSession, ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository): Response
    {
        $participantRolesFilter = $providerParticipantSessionRoleRepository->findTraineesStrictEmailQueryBuilder(providerSession: $providerSession, search: (string) $request->query->get('search'));

        return new Response(
            $this->serializer->serialize(
                $participantRolesFilter,
                'json',
                ['groups' => 'read_lobby_signature'],
            )
        );
    }

    #[Route('/answer/{id}/participant/{token}', name: '_participant_evaluation_answer', options: ['expose' => true])]
    public function accessEvaluationParticipant(
        Evaluation $evaluation,
        string $token,
    ): Response {
        $participantSessionRole = $this->providerParticipantSessionRoleService->getAndValidFromToken($token);

        $participantAnswer = $this->entityManager->getRepository(EvaluationParticipantAnswer::class)
            ->findOneBy(['evaluation' => $evaluation, 'providerParticipantSessionRole' => $participantSessionRole]);

        return $this->render('sessions/evaluation/answer.html.twig', [
            'title' => 'End-of-session evaluation form',
            'client' => $participantSessionRole->getSession()->getClient(),
            'evaluation' => $evaluation,
            'participantSessionRole' => $participantSessionRole,
            'providerSession' => $participantSessionRole->getSession(),
            'answer' => json_encode(!empty($participantAnswer) ? $participantAnswer->getAnswer() : []),
        ]);
    }

    #[Route('/save/{id}/participant/{token}', name: '_participant_evaluation_save', options: ['expose' => true])]
    public function saveEvaluationParticipant(Evaluation $evaluation, string $token, Request $request): Response
    {
        $participantSessionRole = $this->providerParticipantSessionRoleService->getAndValidFromToken($token);
        $this->evaluationParticipantAnswerService->saveAnswer($evaluation, $participantSessionRole, json_decode($request->getContent(), true));
        $this->entityManager->flush();

        return $this->apiResponse([]);
    }

    #[Route('/lobby-evaluation-session/{id}', name: '_evaluation_session_lobby', options: ['expose' => true])]
    public function lobbyEvaluationProviderSession(EvaluationProviderSession $evaluationProviderSession): Response
    {
        if ($evaluationProviderSession->getProviderSession()->getCategory() == CategorySessionType::CATEGORY_SESSION_WEBINAR) {
            return $this->render('sessions/evaluationSession/lobby_webinar.html.twig', [
                'title' => $evaluationProviderSession->getEvaluation()->getTitle(),
                'client' => $evaluationProviderSession->getProviderSession()->getClient(),
                'providerSession' => $evaluationProviderSession->getProviderSession(),
                'evaluationProviderSession' => $evaluationProviderSession,
                'participantRoles' => [],
            ]);
        } else {
            return $this->render('sessions/evaluationSession/lobby.html.twig', [
                'title' => $evaluationProviderSession->getEvaluation()->getTitle(),
                'client' => $evaluationProviderSession->getProviderSession()->getClient(),
                'providerSession' => $evaluationProviderSession->getProviderSession(),
                'evaluationProviderSession' => $evaluationProviderSession,
                'participantRoles' => $evaluationProviderSession->getProviderSession()->getParticipantRoles()->filter(
                    function (ProviderParticipantSessionRole $participantSessionRole) {
                        return $participantSessionRole->isATrainee();
                    }
                ),
            ]);
        }
    }

    #[Route('/answer-session/{id}/participant/{token}', name: '_participant_evaluation_session_answer', options: ['expose' => true])]
    public function accessEvaluationSessionParticipant(
        EvaluationProviderSession $evaluationProviderSession,
        string $token,
    ): Response {
        $participantSessionRole = $this->providerParticipantSessionRoleService->getAndValidFromTokenForEvaluationProviderSession($evaluationProviderSession, $token);
        $participantAnswer = $this->entityManager->getRepository(EvaluationSessionParticipantAnswer::class)
                                                 ->findOneBy(['evaluationProviderSession' => $evaluationProviderSession, 'providerParticipantSessionRole' => $participantSessionRole]);

        return $this->render('sessions/evaluationSession/answer.html.twig', [
            'title' => 'End-of-session evaluation form',
            'client' => $participantSessionRole->getSession()->getClient(),
            'evaluation' => $evaluationProviderSession->getEvaluation(),
            'evaluationProviderSession' => $evaluationProviderSession,
            'participantSessionRole' => $participantSessionRole,
            'providerSession' => $participantSessionRole->getSession(),
            'answer' => json_encode(!empty($participantAnswer) ? $participantAnswer->getAnswer() : []),
        ]);
    }

    #[Route('/save-answer/{id}/participant/{token}', name: '_participant_evaluation_session_save', options: ['expose' => true])]
    public function saveEvaluationSessionParticipant(EvaluationProviderSession $evaluationProviderSession, string $token, Request $request): Response
    {
        $participantSessionRole = $this->providerParticipantSessionRoleService->getAndValidFromTokenForEvaluationProviderSession($evaluationProviderSession, $token);
        $this->evaluationParticipantAnswerService->saveAnswerEvaluationSession($evaluationProviderSession, $participantSessionRole, json_decode($request->getContent(), true));
        $this->entityManager->flush();

        return $this->apiResponse([]);
    }
}
