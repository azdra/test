<?php

namespace App\Controller;

use App\Entity\ProviderSession;
use App\Entity\SessionConvocationAttachment;
use App\Exception\UploadFile\UploadFileException;
use App\Model\ApiResponseTrait;
use App\Model\GeneratorTrait;
use App\Repository\ProviderSessionRepository;
use App\Security\ProviderSessionVoter;
use App\Service\RessourceUploader\RessourceUploader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/session/attachment')]
class SessionAttachmentController extends AbstractController
{
    use ApiResponseTrait;
    use GeneratorTrait;

    public function __construct(
        private LoggerInterface $logger,
        private ProviderSessionRepository $providerSessionRepository,
        private string $convocationAttachmentSavefilesDirectory
    ) {
    }

    #[Route('/upload', name: '_session_attachment_upload', options: ['expose' => true])]
    public function uploadAttachment(
        Request $request,
        EntityManagerInterface $em,
        RessourceUploader $resourceUploader,
        string $convocationAttachmentSavefilesDirectory,
        NormalizerInterface $normalizer
    ): Response {
        try {
            $file = $request->files->get('file');

            $formatParam = $request->request->get('format', implode(',', SessionConvocationAttachment::AUTHORIZED_EXTENSIONS));
            $resourceUploader->isFileUploadedValid($file, explode(',', $formatParam), SessionConvocationAttachment::MAX_FILE_SIZE);

            /** @var UploadedFile $file */
            $attachment = new SessionConvocationAttachment();
            $attachment->setOriginalName($file->getClientOriginalName());
            $fileName = $attachment->getToken().'.'.$file->getClientOriginalExtension();
            $attachment->setFileName($fileName);

            ( new Filesystem() )->copy(
                $file->getRealPath(),
                $convocationAttachmentSavefilesDirectory.'/'.$fileName,
                true
            );

            $em->persist($attachment);
            $em->flush();

            return $this->apiResponse($normalizer->normalize($attachment));
        } catch (UploadFileException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/orphan/delete/{id}', name: '_session_attachment_delete_orphan', options: ['expose' => true])]
    public function deleteOrphanAttachment(
        SessionConvocationAttachment $attachment,
        EntityManagerInterface $em,
        TranslatorInterface $translator
    ): Response {
        if ($attachment->getSession() !== null) {
            return $this->apiErrorResponse($translator->trans('This attachment belongs to a session and can not be deleted this way'));
        }

        $em->remove($attachment);
        $em->flush();

        return $this->apiResponse([]);
    }

    #[Route('/{id}/delete/{attachmentId}', name: '_session_attachment_delete_session', options: ['expose' => true])]
    #[ParamConverter('attachment', options: ['mapping' => ['attachmentId' => 'id']])]
    #[IsGranted(ProviderSessionVoter::EDIT, subject: 'session')]
    public function deleteSessionAttachment(
        ProviderSession $session,
        SessionConvocationAttachment $attachment,
        EntityManagerInterface $em,
        TranslatorInterface $translator
    ): Response {
        if ($attachment->getSession() !== null && $attachment->getSession()->getId() !== $session->getId()) {
            return $this->apiErrorResponse($translator->trans('This attachment does not belong to this session and therefore can not be deleted.'));
        }

        $fileName = $attachment->getFileName();
        $em->remove($attachment);
        $em->flush();
        unlink($this->convocationAttachmentSavefilesDirectory.'/'.$fileName);

        return $this->apiResponse([]);
    }

    #[Route('/download/{token}', name: '_session_attachment_download', options: ['expose' => true])]
    public function downloadAttachment(
        SessionConvocationAttachment $attachment,
        string $convocationAttachmentSavefilesDirectory
    ): Response {
        $path = $convocationAttachmentSavefilesDirectory.'/'.$attachment->getFileName();

        $response = new BinaryFileResponse($path);
        $response->headers->set('Cache-Control', 'max-age=0');
        $response->headers->set('Content-Disposition', 'attachment; filename='.$attachment->getOriginalName());

        return $response;
    }
}
