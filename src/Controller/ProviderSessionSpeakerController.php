<?php

namespace App\Controller;

use App\Entity\ActivityLog;
use App\Entity\ProviderSession;
use App\Entity\ProviderSessionSpeaker;
use App\Exception\UploadFile\UploadFileException;
use App\Model\ActivityLog\ActivityLogTrait;
use App\Model\ApiResponseTrait;
use App\Model\ValidatorTrait;
use App\Repository\ProviderSessionSpeakerRepository;
use App\Service\ActivityLogger;
use App\Service\ActivityLogService;
use App\Service\RessourceUploader\RessourceUploader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/session-speaker')]
class ProviderSessionSpeakerController extends AbstractController
{
    use ApiResponseTrait;
    use ActivityLogTrait;
    use ValidatorTrait;

    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected SerializerInterface $serializer,
        protected TranslatorInterface $translator,
        protected EventDispatcherInterface $eventDispatcher,
        private LoggerInterface $logger,
        private ActivityLogService $activityLogService,
        private ActivityLogger $activityLogger,
        private Security $security,
        private RessourceUploader $ressourceUploader,
        private ProviderSessionSpeakerRepository $providerSessionSpeakerRepository,
        private NormalizerInterface $normalizer,
        private RouterInterface $router,
        private $registrationPageTemplateLogoSavefileDirectory,
    ) {
    }

    #[Route('/upload/files', name: '_provider_session_speaker_upload_files', options: ['expose' => true], methods: ['POST'])]
    public function uploadFile(Request $request): Response
    {
        try {
            /** @var UploadedFile $file */
            $file = $request->files->get('file');

            $formatParam = $request->request->get('format', implode(',', ProviderSessionSpeaker::AUTHORIZED_EXTENSIONS));
            $this->ressourceUploader->isFileUploadedValid($file, explode(',', $formatParam), ProviderSessionSpeaker::MAX_FILE_SIZE);

            $filename = \md5(\random_bytes(32)).'.'.$file->getClientOriginalExtension();

            ( new Filesystem() )->copy($file->getRealPath(), $this->registrationPageTemplateLogoSavefileDirectory.'/'.$filename, true);

            return $this->apiResponse([
                'filetarget' => $filename,
                'previewUri' => $this->registrationPageTemplateLogoSavefileDirectory.$filename,
            ]);
        } catch (UploadFileException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/remove/{id}', name: '_provider_session_speaker_remove', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['POST'])]
    //#[IsGranted(ProviderSessionVoter::VALIDATE, subject: 'providerSession')]
    public function remove(Request $request, ProviderSessionSpeaker $providerSessionSpeaker): Response
    {
        //$this->activityLogger->initActivityLogContext($providerSession->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
        try {
            $providerSession = $providerSessionSpeaker->getProviderSession();
            $providerSession->removeSpeaker($providerSessionSpeaker);
            $this->entityManager->remove($providerSessionSpeaker);
            $this->entityManager->flush();

            return $this->apiResponse($this->normalizer->normalize([$providerSession->getSpeakers()], context: ['groups' => ['read_session']]));
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'ID session' => $providerSession->getId(),
            ]);
            //$this->addFlash('error', 'An unknown error occurred during session validation.');
            return $this->apiErrorResponse($exception->getMessage());
        }

        //return $this->redirectToRoute('_session_get', ['id' => $providerSession->getId()]);
    }

    #[Route('/save/{id}', name: '_provider_session_speaker_save', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['POST'])]
    public function save(Request $request, ProviderSession $providerSession): Response
    {
        try {
            $data = json_decode($request->getContent(), true);
            $speaker = new ProviderSessionSpeaker();

            if (array_key_exists('speakerId', $data) && !is_null($data['speakerId'])) {
                /** @var ProviderSessionSpeaker $speaker */
                $speaker = $this->providerSessionSpeakerRepository->findOneBy(['id' => $data['speakerId']]);
                if (!empty($speaker->getPhoto()) && empty($data['speakerPhoto'])) {
                    unlink($this->registrationPageTemplateLogoSavefileDirectory.'/'.$speaker->getPhoto());
                }
            }
            $speaker->setProviderSession($providerSession);
            $speaker->setName($data['speakerName']);
            $speaker->setInformation($data['speakerInformation']);
            $speaker->setPhoto($data['speakerPhoto']);
            $speaker->setTitle($data['speakerTitle']);
            $providerSession->addSpeaker($speaker);
            $this->entityManager->persist($providerSession);
            $this->entityManager->flush();

            return $this->apiResponse([
                'speakerId' => $speaker->getId(),
                'speakerName' => $speaker->getName(),
                'speakerInformation' => $speaker->getInformation(),
                'speakerPhoto' => $speaker->getPhoto(),
                'speakers' => $this->normalizer->normalize([$providerSession->getSpeakers()], context: ['groups' => ['read_session']]),
            ]);
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'ID session' => $providerSession->getId(),
            ]);
            $this->addFlash('error', 'An unknown error occurred during session validation.');

            return $this->apiErrorResponse($exception->getMessage());
        }
    }
}
