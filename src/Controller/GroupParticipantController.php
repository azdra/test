<?php

namespace App\Controller;

use App\Entity\GroupParticipant;
use App\Entity\User;
use App\Model\ApiResponseTrait;
use App\Repository\ClientRepository;
use App\Repository\GroupParticipantRepository;
use App\Repository\UserRepository;
use App\Service\GroupParticipantService;
use App\Service\MiddlewareService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

#[Route('/group')]
#[IsGranted('ROLE_MANAGER')]
class GroupParticipantController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        protected MiddlewareService $middlewareService,
        protected EntityManagerInterface $entityManager,
        private GroupParticipantRepository $groupParticipantRepository,
        private GroupParticipantService $groupParticipantService,
        private SerializerInterface $serializer,
        private userRepository $userRepository,
        private ClientRepository $clientRepository
    ) {
    }

    #[Route('/list', name: '_group_list', options: ['expose' => true])]
    public function listGroups(): Response
    {
        return $this->render('groups/list.html.twig', [
            'title' => 'Groups',
            'clients' => $this->clientRepository->findAll(),
        ]);
    }

    #[Route('/fetch-list', name: '_fetch_group_list', options: ['expose' => true])]
    #[IsGranted('ROLE_TRAINEE')]
    public function fetchGroupsParticipants(Request $request): Response
    {
        $search = $request->query->get('search', '');
        $firstResult = (int) $request->query->get('firstResult', '1');
        $needCount = (int) $request->query->get('needCount', '0');

        $data['groups'] = $this->groupParticipantRepository->findList($search, $firstResult);
        if ($needCount) {
            $data['count'] = $this->groupParticipantRepository->countListResult($search);
        }

        $groupsParticipantsSerialized = $this->serializer->serialize($data, 'json', ['groups' => 'read_groups']);

        return new Response($groupsParticipantsSerialized);
    }

    #[Route('/delete/{id}', name: '_group_delete', options: ['expose' => true])]
    public function delete(GroupParticipant $groupParticipant): Response
    {
        try {
            $this->entityManager->remove($groupParticipant);
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            return $this->apiErrorResponse($exception->getMessage());
        }

        return $this->apiResponse('The group participant has been updated successfully.');
    }

    #[Route('/edit/{id}', name: '_group_edit', options: ['expose' => true])]
    public function editPage(GroupParticipant $groupParticipant): Response
    {
        $allUsers = $this->userRepository->findAllUserClient($groupParticipant->getClient(), '');

        $allGroupsClient = $this->groupParticipantRepository->findAllGroupClient();

        return $this->render('groups/edit.html.twig', [
            'allGroupsClient' => $allGroupsClient,
            'allUsers' => $allUsers,
            'title' => $groupParticipant->getName(),
            'clients' => $this->clientRepository->findAll(),
            'groupParticipant' => $groupParticipant,
        ]);
    }

    #[Route('/update/{id}', name: '_update_group_participant', options: ['expose' => true])]
    public function update(Request $request, GroupParticipant $groupParticipant): Response
    {
        try {
            $data = json_decode($request->getContent(), true);
            $this->groupParticipantService->handleDataTransformation($data, $groupParticipant);

            $this->groupParticipantService->updateGroupParticipant($groupParticipant, $data);

            $this->entityManager->flush();
        } catch (\Exception $exception) {
            return $this->apiErrorResponse($exception->getMessage());
        }

        return $this->apiResponse('The group participant has been updated successfully.');
    }

    #[Route('/new/', name: '_new_group_participant', options: ['expose' => true])]
    public function newGroupParticipant(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true);
            if ($data['groupData']['client']) {
                $client = $this->clientRepository->findOneBy(['id' => $data['groupData']['client']['id']]);
            } else {
                $client = $this->getUser()->getClient();
            }
            $groupParticipantExist = $this->groupParticipantRepository->findOneBy(['name' => $data['groupData']['name'], 'client' => $client]);
            if ($groupParticipantExist) {
                return $this->apiErrorResponse('The group participant already exists.');
            }
            $groupParticipant = (new GroupParticipant())
                ->setName($data['groupData']['name'])
                ->setClient($client)
                ->setActive(true);

            $this->entityManager->persist($groupParticipant);
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            return $this->apiErrorResponse($exception->getMessage());
        }

        return $this->apiResponse('The group participant has been updated successfully.');
    }

    #[Route('/remove-member-of-group/{id}/{idUser}', name: 'remove_member_from_group', options: ['expose' => true], methods: ['POST'])]
    public function removeMemberOfGroup(GroupParticipant $groupParticipant, User $user, LoggerInterface $logger): Response
    {
        try {
            $user->removeGroup($groupParticipant);
            $this->entityManager->persist($groupParticipant);
            $this->entityManager->flush();

            return $this->apiResponse(['message' => 'The user has been removed from the group successfully.']);
        } catch (Throwable $exception) {
            $logger->error($exception, [
                'A error when remove a user in group ID' => $groupParticipant->getId(),
            ]);

            return $this->apiErrorResponse('A error when remove a user in group.');
        }
    }

    #[Route('/update-member-group/{id}', name: '_group_update_member', requirements: ['id' => '\d+'], options: ['expose' => true])]
    public function updateMemberGroup(Request $request, GroupParticipant $groupParticipant): Response
    {
        $this->groupParticipantService->updateMemberGroupParticipant($groupParticipant, $request->request->all());
        $this->entityManager->persist($groupParticipant);
        $this->entityManager->flush();

        return $this->apiResponse(['success' => true, 'groupParticipantId' => $groupParticipant->getId()]);
    }
}
