<?php

namespace App\Controller;

use App\Entity\DeferredActionsTask;
use App\Entity\User;
use App\Model\ApiResponseTrait;
use App\Repository\ClientRepository;
use App\Security\ImportVoter;
use App\Service\Exporter\ExporterService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/deferred-actions-task')]
class DeferredActionsTaskController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected ExporterService $exporterService,
        protected ClientRepository $clientRepository,
        protected SerializerInterface $serializer,
        protected NormalizerInterface $normalizer,
        protected TranslatorInterface $translator,
        protected LoggerInterface $logger,
    ) {
    }

    #[Route('/check-status/{id}', name: '_deferred_actions_task_check_status', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function checkStatus(DeferredActionsTask $deferredActionsTask): Response
    {
        return $this->apiResponse([
            'statusTask' => $deferredActionsTask->getStatus(),
        ]);
    }

    #[Route('/export-evaluations-sessions', name: '_deferred_actions_task_export_evaluations_sessions', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function exportEvaluationsSessions(Request $request): Response
    {
        try {
            $from = (\DateTime::createFromFormat('Y-m-d\TH:i:s.u\Z', $request->request->get('from')))->setTime(00, 00, 01);
            $to = (\DateTime::createFromFormat('Y-m-d\TH:i:s.u\Z', $request->request->get('to')))->setTime(23, 59, 59);
            $format = $request->request->get('format');
            $grouping = $request->request->get('grouping');

            /** @var User $user */
            $user = $this->getUser();

            if ($this->isGranted(ImportVoter::CAN_SELECT_CLIENT, $user)) {
                $clients = $this->clientRepository->findAll();
            } else {
                $clients = [$user->getClient()];
            }
            usort($clients, function ($a, $b) {
                return strcmp($a->getName(), $b->getName());
            });

            $clientsList = [];
            foreach ($clients as $client) {
                $clientsList[$client->getId()] = $client->getName();
            }

            $data = [
                'from' => $from,
                'to' => $to,
                'grouping' => $grouping,
                'type' => $format,
                'clients' => $clientsList,
            ];

            $deferredActionsTask = new DeferredActionsTask();
            $deferredActionsTask->setClientOrigin($user->getClient())
                ->setUserOrigin($user)
                ->setClassName('Evaluation')
                ->setOrigin(DeferredActionsTask::ORIGIN_EXPORT)
                ->setAction(DeferredActionsTask::ACTION_EXPORT_EVALUATIONS)
                ->setData($data)
                ->setResult([])
                ->setStatus(DeferredActionsTask::STATUS_READY)
                ->setCreatedAt(new \DateTime())
                ->setProcessingStartTime(null)
                ->setProcessingEndTime(null)
                ->setInfos([]);
            $this->entityManager->persist($deferredActionsTask);
            $this->entityManager->flush();

            $response = $this->translator->trans('Your export has been queued. As soon as it is available you will receive an email with the download link.');
            $this->exporterService->generateActivityLogDeferredActionsTaskExportEvaluations(user: $deferredActionsTask->getUserOrigin(), status: DeferredActionsTask::STATUS_READY, deferredActionsTask: $deferredActionsTask);
            $this->addFlash('success', $response);
            $code = Response::HTTP_OK;
        } catch (Throwable $exception) {
            $response = $this->translator->trans('An unknown error appeared in the assessment export request');
            $this->addFlash('error', $response);
            $this->logger->error($exception);
            $code = Response::HTTP_BAD_REQUEST;
        }

        return new Response($response, $code);
    }
}
