<?php

namespace App\Controller;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Model\ApiResponseTrait;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/configuration-holder')]
class ConfigurationHolderController extends AbstractController
{
    use ApiResponseTrait;

    private EntityManagerInterface $entityManager;
    private LoggerInterface  $logger;
    private TranslatorInterface $translator;

    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->translator = $translator;
    }

    #[Route('/save_configuration_holder/{id}', name: '_configuration_holder_save', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function saveConfigurationHolder(Request $request, Client $client): Response
    {
        try {
            $configurationHolder = match ($request->request->get('type')) {
                'adobe-connect' => new AdobeConnectConfigurationHolder(),
                'adobe-connect-webinar' => new AdobeConnectWebinarConfigurationHolder(),
                'cisco-webex' => new WebexConfigurationHolder(),
                'cisco-webex-rest' => new WebexRestConfigurationHolder(),
                'microsoft-teams' => new MicrosoftTeamsConfigurationHolder(),
                'microsoft-teams-event' => new MicrosoftTeamsEventConfigurationHolder(),
                'white-connector' => new WhiteConfigurationHolder(),
            };

            $configurationHolder->setName($request->request->get('name'));
            $client->addConfigurationHolder($configurationHolder);

            $this->entityManager->flush();

            $response = $this->translator->trans('The new connector has been adding to account');
            $code = Response::HTTP_ACCEPTED;

            return new Response($response, $code);
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'configurationHolderType' => $request->request->get('type'),
                'configurationHolderName' => $request->request->get('name'),
            ]);

            $response = $this->translator->trans('The new connector has not been added to the account');
            $code = Response::HTTP_BAD_REQUEST;

            return new Response($response, $code);
        }
    }

    #[Route('/delete_configuration_holder/{id}', name: '_configuration_holder_delete', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function deleteConfigurationHolder(Request $request, AbstractProviderConfigurationHolder $configurationHolder): Response
    {
        try {
            $this->entityManager->remove($configurationHolder);
            $this->entityManager->flush();

            $response = $this->translator->trans('The connector has been removed from the account');
            $code = Response::HTTP_ACCEPTED;

            return new Response($response, $code);
        } catch (\Exception $exception) {
            $this->logger->error($exception, ['configurationHolderId' => $configurationHolder->getId()]);

            $response = $this->translator->trans('The connector has not been removed from the account');
            $code = Response::HTTP_BAD_REQUEST;

            return new Response($response, $code);
        }
    }

    #[Route('/save-percentage-attendance_configuration_holder/{id}', name: '_client_save_percentage_attendance', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function savePercentageAttendanceConfigurationHolder(Request $request, AbstractProviderConfigurationHolder $configurationHolder): Response
    {
        try {
            if ($request->request->has('percentageAttendance')) {
                $configurationHolder->setPercentageAttendance((int) $request->request->get('percentageAttendance'));
                $this->entityManager->flush();
            }
            $response = $this->translator->trans('The percentage of attendance has been saved');
            $code = Response::HTTP_ACCEPTED;

            return new Response($response, $code);
        } catch (\Exception $exception) {
            $this->logger->error($exception, ['configurationHolderId' => $configurationHolder->getId()]);

            $response = $this->translator->trans('The percentage of attendance has not been saved');
            $code = Response::HTTP_BAD_REQUEST;

            return new Response($response, $code);
        }
    }

    #[Route('/archive_configuration_holder/{id}', name: '_configuration_holder_archive', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function archiveConfigurationHolder(Request $request, AbstractProviderConfigurationHolder $configurationHolder): Response
    {
        try {
            $configurationHolder->setArchived(true);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('The connector has been archived from account'));
            $response = $this->translator->trans('The connector has been archived from account');
            $code = Response::HTTP_ACCEPTED;

            return new Response($response, $code);
        } catch (\Exception $exception) {
            $this->logger->error($exception, ['configurationHolderId' => $configurationHolder->getId()]);

            $response = $this->translator->trans('The connector has not been archived from the account');
            $code = Response::HTTP_BAD_REQUEST;

            return new Response($response, $code);
        }
    }

    #[Route('/unarchived_configuration_holder/{id}', name: '_configuration_holder_unarchived', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function unarchivedConfigurationHolder(Request $request, AbstractProviderConfigurationHolder $configurationHolder): Response
    {
        try {
            $configurationHolder->setArchived(false);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('The connector has been unarchived from account'));
            $response = $this->translator->trans('The connector has been unarchived from account');
            $code = Response::HTTP_ACCEPTED;

            return new Response($response, $code);
        } catch (\Exception $exception) {
            $this->logger->error($exception, ['configurationHolderId' => $configurationHolder->getId()]);

            $response = $this->translator->trans('The connector has not been unarchived from the account');
            $code = Response::HTTP_BAD_REQUEST;

            return new Response($response, $code);
        }
    }
}
