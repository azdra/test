<?php

namespace App\Controller;

use App\Entity\ActivityLog;
use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectWebinarPrincipal;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\Bounce;
use App\Entity\CategorySessionType;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Entity\Convocation;
use App\Entity\Evaluation;
use App\Entity\EvaluationProviderSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\SessionConvocationAttachment;
use App\Entity\User;
use App\Entity\WebinarConvocation;
use App\Entity\White\WhiteSession;
use App\Exception\AbstractProviderException;
use App\Exception\InvalidArgumentException;
use App\Exception\InvalidTemplateUploadedForImportException;
use App\Exception\LicenceCheckException;
use App\Exception\MiddlewareFailureException;
use App\Exception\MissingClientParameter;
use App\Exception\ProviderSessionAccessToken\ProviderSessionAccessTokenException;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Exception\UploadFile\UploadFileException;
use App\Form\ResetAdobePasswordFormType;
use App\Form\SendMailFormType;
use App\Model\ActivityLog\ActivityLogModel;
use App\Model\ActivityLog\ActivityLogTrait;
use App\Model\ApiResponseTrait;
use App\Model\ProviderResponse;
use App\Model\ResetAdobePasswordModel;
use App\Model\SendMailFormModel;
use App\Model\ValidatorTrait;
use App\RegistrationPage\Entity\RegistrationPageTemplate;
use App\RegistrationPage\Repository\RegistrationPageTemplateRepository;
use App\Repository\BounceRepository;
use App\Repository\ClientRepository;
use App\Repository\ConvocationRepository;
use App\Repository\EvaluationParticipantAnswerRepository;
use App\Repository\EvaluationProviderSessionRepository;
use App\Repository\GroupParticipantRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Security\ClientVoter;
use App\Security\ImportVoter;
use App\Security\ProviderSessionVoter;
use App\SelfSubscription\Repository\ModuleRepository;
use App\Service\ActivityLogger;
use App\Service\ActivityLogService;
use App\Service\AdobeConnectService;
use App\Service\AdobeConnectWebinarService;
use App\Service\AttendanceReportService;
use App\Service\ClientService;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use App\Service\Connector\Adobe\AdobeConnectWebinarConnector;
use App\Service\ConvocationService;
use App\Service\EvaluationProviderSessionService;
use App\Service\Exporter\ExporterService;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\Pdf\PdfAttendanceReportProviderSessionExporter;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetEvaluationReportProviderSessionExporter;
use App\Service\GroupParticipantService;
use App\Service\ImportGuestService;
use App\Service\ImportSessionService;
use App\Service\MiddlewareService;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\Provider\Model\LicenceChecker;
use App\Service\Provider\RequestHandler\SessionRequestHandler;
use App\Service\Provider\Webex\WebexRestLicensingService;
use App\Service\ProviderParticipantSessionRoleService;
use App\Service\ProviderSessionService;
use App\Service\RessourceUploader\RessourceUploader;
use App\Service\SendTrainingCertificatesService;
use App\Service\SupportService;
use App\Service\WebexMeetingService;
use App\Service\WebexRestMeetingService;
use App\Service\WebinarConvocationService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;

#[Route('/session')]
class SessionController extends AbstractController
{
    use ApiResponseTrait;
    use ActivityLogTrait;
    use ValidatorTrait;

    public function __construct(
        protected MiddlewareService $middlewareService,
        protected ClientService $clientService,
        protected EntityManagerInterface $entityManager,
        protected SerializerInterface $serializer,
        protected TranslatorInterface $translator,
        protected ImportSessionService $importSessionService,
        protected EventDispatcherInterface $eventDispatcher,
        private LoggerInterface $logger,
        private ActivityLogService $activityLogService,
        private ProviderSessionService $providerSessionService,
        private ActivityLogger $activityLogger,
        private ProviderParticipantSessionRoleService $providerParticipantSessionRoleService,
        private SupportService $supportService,
        private Security $security,
        private ConvocationService $convocationService,
        private AdobeConnectService $adobeConnectService,
        private AdobeConnectWebinarService $adobeConnectWebinarService,
        private WebexMeetingService $webexMeetingService,
        private WebexRestMeetingService $webexRestMeetingService,
        private WebexRestLicensingService $webexRestLicensingService,
        private LicenceChecker $licenceChecker,
        private RouterInterface $router,
        private RessourceUploader $ressourceUploader,
        private WebinarConvocationService $webinarConvocationService,
        private RegistrationPageTemplateRepository $pageTemplateRepository,
        private EvaluationProviderSessionRepository $evaluationProviderSessionRepository,
        private EvaluationProviderSessionService $evaluationProviderSessionService,
        private GroupParticipantRepository $groupParticipantRepository,
        private GroupParticipantService $groupParticipantService,
        private SessionRequestHandler $sessionRequestHandler,
        private $registrationPageTemplateLogoSavefileDirectory,
        private EvaluationParticipantAnswerRepository $answerRepository,
    ) {
    }

    #[Route('/{id}', name: '_session_get', requirements: ['id' => '\d+'], options: ['expose' => true])]
    #[IsGranted(ProviderSessionVoter::ACCESS, subject: 'providerSession')]
    public function getSession(Request $request, ProviderSession $providerSession, BounceRepository $bounceRepository): Response
    {
        //$readOnly = !$this->security->isGranted(ProviderSessionVoter::EDIT, $providerSession);
        $readOnly = !$this->providerSessionService->canUpdateSession($providerSession, $this->getUser());
        $sendMailForm = $this->createForm(
            SendMailFormType::class,
            new SendMailFormModel(
                recipients: $providerSession->getClient()->getEmailScheduling()->getPresenceReportRecipients() ?: [],
                bcc: ['tags' => [$this->getParameter('support_mail')]]
            ),
            [
                'action' => $this->generateUrl('_session_send_attendance_report', ['id' => $providerSession->getId()], UrlGeneratorInterface::NETWORK_PATH),
                'method' => 'POST',
            ]
        );

        $sendTestConvocationTestEmailForm = $this->createForm(
            SendMailFormType::class,
            new SendMailFormModel(
                recipients: ['tags' => [$this->getUser()->getEmail()]],
                bcc: ['tags' => [$this->getParameter('support_mail')]]
            ),
            [
                'action' => $this->generateUrl('_session_send_convocation_test_email', ['id' => $providerSession->getId()], UrlGeneratorInterface::NETWORK_PATH),
                'method' => 'POST',
            ]
        );

        $autorizedValidateDraft = true;
        if ($providerSession instanceof WhiteSession) {
            if (empty($providerSession->getJoinSessionWebUrl()) || $providerSession->getJoinSessionWebUrl() == '') {
                $autorizedValidateDraft = false;
            }
        }

        $importRestrictions = [
            'max_file_size' => RessourceUploader::humanReadableFileSize(ImportGuestService::MAX_FILE_SIZE),
            'allowed_extension' => implode(', ', ImportGuestService::AUTHORIZED_EXTENSION),
            'allowed_max_row' => ImportGuestService::ALLOWED_MAX_ROW,
        ];

        return $this->render('sessions/edit.html.twig', [
            'importRestrictions' => $importRestrictions,
            'listNewTimezones' => $this->providerSessionService->getListTimezones($this->getUser()),
            'title' => 'Sessions',
            'providerSession' => $providerSession,
            'canDeleteSession' => $providerSession->getDateStart() < (new DateTimeImmutable())->modify('+ 1 day') && !empty($providerSession->getHelpNeeds()->getValues()),
            'autorizedValidateDraft' => $autorizedValidateDraft,
            'sendMailForm' => $sendMailForm->createView(),
            'sendTestConvocationMailForm' => $sendTestConvocationTestEmailForm->createView(),
            'bouncedMails' => $bounceRepository->findBy(['state' => Bounce::TO_CORRECT, 'session' => $providerSession]),
            'readOnly' => $readOnly,
            'hasACommunicationThankYouType' => $providerSession->hasACommunicationThankYouType(),
            'userActions' => [
                'send_convocation' => $this->security->isGranted(ProviderSessionVoter::SEND_CONVOCATION, $providerSession),
                'update_participant' => $this->security->isGranted(ProviderSessionVoter::UPDATE_PARTICIPANT, $providerSession),
            ],
            'tabName' => $request->query->has('tabName') ? $request->query->get('tabName') : 'SessionParticipantsTab',
            'showUpdateSuccess' => $request->query->has('updateSuccessfully') ? $request->query->get('updateSuccessfully') : false,
        ]);
    }

    #[Route('/update-additionals-timezones/{id}', name: '_session_update_additionals_timezones', requirements: ['id' => '\d+'], options: ['expose' => true])]
    public function updateAdditionalsTimezonesSession(Request $request, ProviderSession $providerSession): Response
    {
        $providerSession->setAdditionalTimezones($request->request->all()['timezones']);
        $this->entityManager->persist($providerSession);
        $this->entityManager->flush();

        return $this->apiResponse(['success' => true, 'sessionId' => $providerSession->getId()]);
    }

    #[Route('/validate/{id}', name: '_session_validate', options: ['expose' => true])]
    #[IsGranted(ProviderSessionVoter::VALIDATE, subject: 'providerSession')]
    public function validate(ProviderSession $providerSession): Response
    {
        $this->activityLogger->initActivityLogContext($providerSession->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
        try {
            if (
                $providerSession->getAbstractProviderConfigurationHolder() instanceof AdobeConnectConfigurationHolder &&
                empty($providerSession->getProviderAudio()) &&
                $providerSession->getConvocation()->getCategoryTypeAudio() != AbstractProviderAudio::AUDIO_CATEGORY_COMPUTER
            ) {
                $response = $this->translator->trans('Error while validating session \'%sessionName%\': All audios are already used during this period', ['%sessionName%' => $providerSession->getName()]);
                $this->addFlash('warning', $response);
            } else {
                $providerSession->setStatus(ProviderSession::STATUS_SCHEDULED);
                $this->entityManager->flush();
                $this->activityLogger->flushActivityLogs();
                $response = $this->translator->trans('The session has been validated');
                $this->addFlash('success', $response);
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'ID session' => $providerSession->getId(),
            ]);
            $this->addFlash('error', 'An unknown error occurred during session validation.');
        }

        return $this->redirectToRoute('_session_get', ['id' => $providerSession->getId()]);
    }

    #[Route('/delete/{id}', name: '_session_delete', options: ['expose' => true])]
    #[IsGranted(ProviderSessionVoter::DELETE, subject: 'providerSession')]
    public function delete(ProviderSession $providerSession): Response
    {
        $this->activityLogger->initActivityLogContext($providerSession->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
        try {
            $this->providerSessionService->deleteEvaluationsSessions($providerSession);
            $this->providerSessionService->deleteSession($providerSession);

            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();
            $response = $this->translator->trans('The session has been deleted. The data is available for 30 days for consultation in the activity log.');
            $this->addFlash('success', $response);
        } catch (MiddlewareFailureException $exception) {
            $this->logger->error($exception, [
                'ID session' => $providerSession->getId(),
                'data' => $exception->getData(),
            ]);
            $this->addFlash('error', 'An unknown error occurred during session deletion.');
        }

        return $this->redirectToRoute('_session_viewer');
    }

    #[Route('/cancel/{id}', name: '_session_cancel', options: ['expose' => true])]
    #[IsGranted(ProviderSessionVoter::CANCEL, subject: 'providerSession')]
    public function cancel(ProviderSession $providerSession): Response
    {
        $this->activityLogger->initActivityLogContext($providerSession->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
        try {
            $this->providerSessionService->cancelSession($providerSession);

            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();
            $response = $this->translator->trans('The session has been cancelled.');
            $this->addFlash('success', $response);
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'ID session' => $providerSession->getId(),
            ]);
            $this->addFlash('error', 'An unknown error occurred during session cancellation.');
        }

        return $this->redirectToRoute('_session_viewer');
    }

    #[Route('/uncancel/{id}', name: '_session_uncancel', options: ['expose' => true])]
    #[IsGranted(ProviderSessionVoter::EDIT, subject: 'providerSession')]
    public function uncancel(ProviderSession $providerSession): Response
    {
        $this->activityLogger->initActivityLogContext($providerSession->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
        try {
            if ($providerSession->getClient()->getBillingMode() === Client::BILLING_MODE_PARTICIPANT && $providerSession->getClient()->getRemainingCreditParticipants() < $providerSession->getTraineesCount()) {
                $this->addFlash('error', 'You cannot cancel this session because you do not have enough participants available.');

                return $this->redirectToRoute('_session_get', ['id' => $providerSession->getId()]);
            }

            $this->providerSessionService->uncancelSession($providerSession);

            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();
            $response = $this->translator->trans('The session has been uncancelled');
            $this->addFlash('success', $response);
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'ID session' => $providerSession->getId(),
            ]);
            $this->addFlash('error', 'An unknown error occurred during session uncancellation.');
        }

        return $this->redirectToRoute('_session_get', ['id' => $providerSession->getId()]);
    }

    #[Route('/', name: '_session_viewer', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function listSessions(Request $request,
                                 ProviderSessionRepository $providerSessionRepository,
                                 ClientRepository $clientRepository,
                                 BounceRepository $bounceRepository,
    RegistrationPageTemplateRepository $pageTemplateRepository,
                                 ModuleRepository $moduleRepository,
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        if ($this->isGranted(ImportVoter::CAN_SELECT_CLIENT, $user)) {
            $clients = $clientRepository->findAll();
        } else {
            $clients = [$user->getClient()];
        }
        usort($clients, function ($a, $b) {
            return strcmp($a->getName(), $b->getName());
        });

        $connectors = $this->clientService->getAllConfigurationHolder($clients);
        usort($connectors, function ($a, $b) {
            return strcasecmp($a->getName(), $b->getName());
        });

        $allRegistrationPageTemplates = $this->isGranted('ROLE_ADMIN')
            ? $pageTemplateRepository->findAll()
            : $pageTemplateRepository->findBy(['client' => $user->getClient()]);

        usort($allRegistrationPageTemplates, function ($a, $b) {
            return strcasecmp($a->getName(), $b->getName());
        });

        $allCatalogs = $this->isGranted('ROLE_ADMIN')
            ? $moduleRepository->findAll()
            : $moduleRepository->findBy(['client' => $user->getClient()]);

        usort($allCatalogs, function ($a, $b) {
            return strcasecmp($a->getName(), $b->getName());
        });

        $importRestrictions = [
            'max_file_size' => RessourceUploader::humanReadableFileSize(ImportSessionService::MAX_FILE_SIZE),
            'allowed_extension' => implode(', ', ImportSessionService::AUTHORIZED_EXTENSION),
            'allowed_max_row' => ImportSessionService::ALLOWED_MAX_ROW,
        ];

        if ($request->query->has('pre-search')) {
            $allowOutdated = $request->query->getBoolean('allow-outdated', false);
            $preSearch = (string) $request->query->get('pre-search');

            $sessions = $providerSessionRepository->globalSearch((string) $request->query->get('pre-search'), $allowOutdated);
        }

        return $this->render('sessions/list.html.twig', [
            'title' => 'Sessions',
            'importRestrictions' => $importRestrictions,
            'allConfigurationHolder' => $connectors,
            'allRegistrationPageTemplates' => $allRegistrationPageTemplates,
            'allCatalogs' => $allCatalogs,
            'clients' => $clients,
            'bouncedMails' => $bounceRepository->findBy(['state' => Bounce::TO_CORRECT]),
            'preSearch' => !empty($preSearch) ? $preSearch : null,
            'allowOutdated' => !empty($allowOutdated) ? $allowOutdated : null,
        ]);
    }

    #[Route('/list-animators/{id}', name: '_session_list_animators', options: ['expose' => true], format: 'json')]
    #[IsGranted(ProviderSessionVoter::ACCESS, subject: 'providerSession')]
    public function listAnimators(ProviderSession $providerSession, ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository): Response
    {
        return new Response(
            $this->serializer->serialize(
                $providerParticipantSessionRoleRepository->findAnimators($providerSession),
                'json',
                ['groups' => 'read_session'],
            )
        );
    }

    #[Route('/list-trainees/{id}', name: '_session_list_trainees', options: ['expose' => true], format: 'json')]
    #[IsGranted(ProviderSessionVoter::ACCESS, subject: 'providerSession')]
    public function listTrainees(ProviderSession $providerSession, ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository): Response
    {
        return new Response(
            $this->serializer->serialize(
                $providerParticipantSessionRoleRepository->findTrainees($providerSession),
                'json',
                ['groups' => 'read_session'],
            )
        );
    }

    #[Route('/global-search', name: '_session_global_search', options: ['expose' => true], methods: ['GET'], format: 'json')]
    #[IsGranted('ROLE_MANAGER')]
    public function globalSearch(Request $request, ProviderSessionRepository $providerSessionRepository): Response
    {
        $viewer = $request->query->get('viewer', 'SessionsList');

        $data['sessions'] = $providerSessionRepository->globalSearch(
            query: (string) $request->query->get('q'),
            allowOutdated: $request->query->getBoolean('allow-outdated', false),
            configurationHolderId: $request->query->getInt('configuration-holder'),
            firstResult: $request->query->getInt('first-result', 0),
            countResult: $request->query->getInt('all', 0) ? 0 : ProviderSession::MAX_RESULT_LIST,
            filtersSearch: json_decode($request->query->get('filters-search'))
        );

        if ($request->query->getInt('need-count', 0)) {
            $data['count'] = $providerSessionRepository->countGlobalSearchResult(
                query: (string) $request->query->get('q'),
                allowOutdated: $request->query->getBoolean('allow-outdated', false),
                configurationHolderId: $request->query->getInt('configuration-holder'),
                filtersSearch: json_decode($request->query->get('filters-search'))
            );
        }

        return new Response(
            $this->serializer->serialize(
                $data,
                'json',
                ['groups' => $viewer === 'SessionsList' ? 'read_session_list' : 'read_session_calendar'],
            )
        );
    }

    #[Route('/import-file-sessions', name: '_session_import_file', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function importFileSessions(Request $request): Response
    {
        try {
            $this->importSessionService->createImportAsyncTasksFromRequest($request);
            $this->entityManager->flush();

            $response = $this->translator->trans('The file has been received. It may take a few seconds or several minutes to process, depending on the number of lines it contains. As soon as it is finished, you will receive the import report by email.');
            $this->addFlash('success', $response);
            $code = Response::HTTP_OK;
        } catch (UploadFileException|MissingClientParameter|InvalidTemplateUploadedForImportException $exception) {
            $response = $exception->getMessage();
            $errorMessage = $this->translator->trans('No import file');
            $this->addFlash('error', $errorMessage);
            $this->logger->error($exception);
            $code = Response::HTTP_BAD_REQUEST;
        } catch (Throwable $exception) {
            $response = $this->translator->trans('The file can not be imported for an unknown error');
            $this->addFlash('error', $response);
            $this->logger->error($exception);
            $code = Response::HTTP_BAD_REQUEST;
        }

        return new Response($response, $code);
    }

    #[Route('/access/{token}', name: '_session_access_plateforme', options: ['expose' => true])]
    public function accessToProviderPlateforme(
        Request $request,
        ProviderParticipantSessionRoleRepository $participantSessionRoleRepository,
        string $token,
        AdobeConnectConnector $adobeConnectConnector,
        AdobeConnectWebinarConnector $adobeConnectWebinarConnector
    ): Response {
        $providerSession = null;

        try {
            /** @var ProviderParticipantSessionRole $providerParticipantSessionRole */
            $providerParticipantSessionRole = $participantSessionRoleRepository->findOneBy(['providerSessionAccessToken.token' => $token]);

            if (empty($providerParticipantSessionRole)) {
                throw new NotFoundHttpException('Unable to connect. Please contact us to solve the issue');
            }

            $providerSession = $providerParticipantSessionRole->getSession();

            $sessionDateStart = $providerSession->getDateStart();
            $currentDateTime = new \DateTime();
            $sessionDateStartMinus30Minutes = clone $sessionDateStart;
            $sessionDateStartMinus30Minutes->sub(new \DateInterval('PT30M'));
            if ($currentDateTime < $sessionDateStartMinus30Minutes && $providerParticipantSessionRole->getRole() === ProviderParticipantSessionRole::ROLE_TRAINEE) {
                // La date actuelle est supérieure à la date de début de session de moins de 30 minutes pour les participants uniquement
                throw new ProviderSessionAccessTokenException($providerParticipantSessionRole->getProviderSessionAccessToken(), []);
            }

            $this->providerParticipantSessionRoleService->hasValidSessionAccessToken($providerParticipantSessionRole);
            //$this->providerParticipantSessionRoleService->isAuthorizedToAccessToTheSession($providerParticipantSessionRole);

            if ($providerSession instanceof AdobeConnectSCO) {
                /** @var AdobeConnectPrincipal $participant */
                $participant = $providerParticipantSessionRole->getParticipant();

                if ($request->isMethod('POST')) {
                    $password = new ResetAdobePasswordModel();
                    $form = $this->createForm(ResetAdobePasswordFormType::class, $password);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $participant->setPassword($password->getPassword());
                        $adobeConnectConnector->call($participant->getConfigurationHolder(), AdobeConnectConnector::ACTION_UPDATE_PRINCIPAL_PASSWORD, $participant);
                        $this->entityManager->persist($participant);
                        $this->entityManager->flush();
                        $this->logger->info('The password have been updated => '.$participant->getPassword());
                    }
                }
                $this->logger->info(' the password for authentification is => '.$participant->getPassword().' || the email is => '.$participant->getEmail());
            } elseif ($providerSession instanceof AdobeConnectWebinarSCO) {
                /** @var AdobeConnectWebinarPrincipal $participant */
                $participant = $providerParticipantSessionRole->getParticipant();

                if ($request->isMethod('POST')) {
                    $password = new ResetAdobePasswordModel();
                    $form = $this->createForm(ResetAdobePasswordFormType::class, $password);
                    $form->handleRequest($request);
                    if ($form->isSubmitted() && $form->isValid()) {
                        $participant->setPassword($password->getPassword());
                        $adobeConnectWebinarConnector->call($participant->getConfigurationHolder(), AdobeConnectWebinarConnector::ACTION_UPDATE_PRINCIPAL_PASSWORD, $participant);
                        $this->entityManager->persist($participant);
                        $this->entityManager->flush();
                        $this->logger->info('The password have been updated => '.$participant->getPassword());
                    }
                }
            }

            $response = $this->middlewareService->call(MiddlewareService::MIDDLEWARE_ACTION_REDIRECT_PARTICIPANT_SESSION_ROLE, $providerParticipantSessionRole);

            if (!$response->isSuccess()) {
                if ($providerSession instanceof AdobeConnectSCO) {
                    /** @var AdobeConnectPrincipal $participant */
                    $participant = $providerParticipantSessionRole->getParticipant();

                    $isAllowToUpdatePassword = $this->adobeConnectService->isAllowToUpdatePassword($participant, $providerSession->getAbstractProviderConfigurationHolder());

                    if ($isAllowToUpdatePassword) {
                        $password = new ResetAdobePasswordModel();
                        $form = $this->createForm(ResetAdobePasswordFormType::class, $password);
                        $this->addFlash('warning', 'We haven’t been able to connect you');

                        return $this->render('sessions/ask_password.html.twig', [
                            'title' => 'Authentication issue',
                            'participant' => $participant,
                            'form' => $form->createView(),
                            'providerSession' => $providerSession,
                        ]);
                    } else {
                        $adobeConnectConnector->call($participant->getConfigurationHolder(), AdobeConnectConnector::ACTION_UPDATE_PRINCIPAL_PASSWORD, $participant);
                        $response = $this->middlewareService->call(MiddlewareService::MIDDLEWARE_ACTION_REDIRECT_PARTICIPANT_SESSION_ROLE, $providerParticipantSessionRole);
                    }
                } elseif ($providerSession instanceof AdobeConnectWebinarSCO) {
                    /** @var AdobeConnectWebinarPrincipal $participant */
                    $participant = $providerParticipantSessionRole->getParticipant();
                    /*$password = new ResetAdobePasswordModel();
                    $form = $this->createForm(ResetAdobePasswordFormType::class, $password);
                    if ($request->isMethod('POST')) {
                        $form->handleRequest($request);
                        if ($form->isSubmitted() && $form->isValid()) {
                            $participant->setPassword($password->getPassword());
                            $response = $adobeConnectWebinarConnector->call($participant->getConfigurationHolder(), AdobeConnectWebinarConnector::ACTION_UPDATE_PRINCIPAL_PASSWORD, $participant);
                            $this->entityManager->persist($participant);
                            $this->entityManager->flush();
                            //if ($response->isSuccess()) {
                            $this->logger->info('The password have been updated => '.$participant->getPassword());
                            //}
                        }
                    }
                    $response = $this->middlewareService->call(MiddlewareService::MIDDLEWARE_ACTION_REDIRECT_PARTICIPANT_SESSION_ROLE, $providerParticipantSessionRole);
                    if (!$response->isSuccess()) {*/
                    $isAllowToUpdatePassword = $this->adobeConnectWebinarService->isAllowToUpdatePassword($participant, $providerSession->getAbstractProviderConfigurationHolder());

                    if ($isAllowToUpdatePassword) {
                        $password = new ResetAdobePasswordModel();
                        $form = $this->createForm(ResetAdobePasswordFormType::class, $password);
                        $this->addFlash('warning', 'We haven’t been able to connect you');

                        return $this->render('sessions/ask_password.html.twig', [
                            'title' => 'Authentication issue',
                            'participant' => $participant,
                            'form' => $form->createView(),
                            'providerSession' => $providerSession,
                        ]);
                    } else {
                        $adobeConnectWebinarConnector->call($participant->getConfigurationHolder(), AdobeConnectWebinarConnector::ACTION_UPDATE_PRINCIPAL_PASSWORD, $participant);
                        $response = $this->middlewareService->call(MiddlewareService::MIDDLEWARE_ACTION_REDIRECT_PARTICIPANT_SESSION_ROLE, $providerParticipantSessionRole);
                    }
                } else {
                    throw $response->getData();
                }
            }

            $url = $response->getData();

            return $this->redirect($url);
        } catch (ProviderSessionAccessTokenException $exception) {
            $this->addFlash('warning', 'The virtual room is not open yet. Please try again in a few minutes.');
            $this->logger->warning('[ProviderSessionAccessToken] Token invalid', [
                'token' => $token,
                'decryptData' => $exception->getDecryptedData(),
                'exceptionClass' => get_class($exception),
            ]);
        } catch (AbstractProviderException $exception) {
            $this->addFlash('error', 'Unable to log in with your account information. Please try again later.');
            $this->logger->error($exception, [
                'token' => $token,
            ]);
        } catch (NotFoundHttpException $exception) {
            $this->addFlash('warning', $exception->getMessage());
            $this->logger->warning("[ProviderSessionAccessToken] {$exception->getMessage()}", [
                'token' => $token,
                'message' => $exception->getMessage(),
            ]);
        } catch (\Exception $exception) {
            $this->addFlash('error', 'An unexpected error occurred when accessing the virtual room. Please try again.');
            $this->logger->error($exception, [
                'token' => $token,
            ]);
        }

        return $this->render('sessions/redirect_to_connector.html.twig', [
            'title' => 'Access to the room',
            'providerSession' => $providerSession,
        ]);
    }

    #[Route('/add-to-calendar/{token}', name: '_session_add_to_calendar', methods: 'GET', options: ['expose' => true])]
    public function addSessionToCalendar(
        ProviderParticipantSessionRoleRepository $participantSessionRoleRepository,
        string $token): Response
    {
        $providerSession = null;

        try {
            /** @var ProviderParticipantSessionRole $providerParticipantSessionRole */
            $providerParticipantSessionRole = $participantSessionRoleRepository->findOneBy(['providerSessionAccessToken.token' => $token]);

            if (empty($providerParticipantSessionRole)) {
                throw new NotFoundHttpException('Unable to connect. Please contact us to solve the issue');
            }

            $providerSession = $providerParticipantSessionRole->getSession();
            $this->providerParticipantSessionRoleService->hasValidSessionAccessToken($providerParticipantSessionRole);
        } catch (ProviderSessionAccessTokenException $exception) {
            $this->addFlash('warning', 'Unable to add this session to your calendar.Please try again.');
            $this->logger->warning('[ProviderSessionAccessToken] Token invalid', [
                'token' => $token,
                'decryptData' => $exception->getDecryptedData(),
                'exceptionClass' => get_class($exception),
            ]);
        } catch (\Exception $exception) {
            $this->addFlash('error', 'An unexpected error occurred while adding this event to your calendar. Please try again.');
            $this->logger->error($exception, [
                'token' => $token,
            ]);
        }

        return $this->render('sessions/add_to_calendar.html.twig', [
            'title' => 'Add event to my calendar',
            'providerSession' => $providerSession,
            'ppsr' => $providerParticipantSessionRole,
            'token' => $token,
        ]);
    }

    #[Route('/download-export/{id}', name: '_session_download_export', options: ['expose' => true], methods: 'POST')]
    #[IsGranted(ProviderSessionVoter::ACCESS, subject: 'session')]
    public function download(Request $request, ExporterService $exporterService, ProviderSession $session): Response
    {
        $type = $request->request->get('export-type') ?? PdfAttendanceReportProviderSessionExporter::EXPORT_TYPE;
        $format = $request->request->get('export-format') ?? ExportFormat::PDF;

        /** @var JsonResponse $response */
        $response = $exporterService->generateArchive($type, $format, [$session]);

        $data = json_decode($response->getContent());

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->addFlash('error', $data->message->text);
            $this->logger->error(new \Exception($data->message->data->exception), [
                'type' => $type,
                'format' => $format,
                'sessionId' => $session->getId(),
            ]);

            return $this->redirectToRoute('_session_get', ['id' => $session->getId()]);
        }

        return $this->redirectToRoute('_download_exports', [
            'token' => $data->message,
        ]);
    }

    #[Route('/send-attendance-report/{id}', name: '_session_send_attendance_report', methods: 'POST')]
    #[IsGranted(ProviderSessionVoter::ACCESS, subject: 'providerSession')]
    public function sendAttendanceReport(Request $request, ProviderSession $providerSession, AttendanceReportService $attendanceReportService): Response
    {
        $model = new SendMailFormModel();
        $sendMailForm = $this->createForm(SendMailFormType::class, $model);

        $sendMailForm->handleRequest($request);
        if ($sendMailForm->isSubmitted() && $sendMailForm->isValid()) {
            $this->activityLogger->initActivityLogContext($providerSession->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
            $attendanceReportService->sendAttendanceReportByMailRequest($providerSession, $model, $this->getUser());
            $infos = [
                'message' => [
                    'key' => 'The attendance report for the <a href="%sessionRoute%">%sessionName%</a> session has been sent to the following recipients %email%',
                    'params' => ['%route%' => $this->router->generate('_session_get', ['id' => $providerSession->getId()]), '%sessionName%' => $providerSession->getName(), '%email%' => $model->getRecipients()['tags']],
                ], 'session' => ['id' => $providerSession->getId(), 'name' => $providerSession->getName(), 'ref' => $providerSession->getRef()],
            ];
            $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SEND_PRESENCE_REPORT, ActivityLog::SEVERITY_INFORMATION, $infos));
            $this->activityLogger->flushActivityLogs();
            $this->addFlash('success', 'The attendance reports were sent by e-mail');
        } else {
            $this->addFlash('error', 'The emails provided are not valid, the attendance reports have not been sent.');
        }

        return $this->redirectToRoute('_session_get', ['id' => $providerSession->getId()]);
    }

    #[Route('/send-convocation-test-email/{id}', name: '_session_send_convocation_test_email', methods: 'POST')]
    #[IsGranted(ProviderSessionVoter::ACCESS, subject: 'providerSession')]
    public function sendConvocationTestEmail(Request $request, ProviderSession $providerSession, ConvocationService $convocationService): Response
    {
        $model = new SendMailFormModel();
        $sendMailForm = $this->createForm(SendMailFormType::class, $model);

        $sendMailForm->handleRequest($request);
        if ($sendMailForm->isSubmitted() && $sendMailForm->isValid()) {
            $convocationService->sendConvocationTestEmailRequest($providerSession, $model, $this->getUser());
            if ($providerSession->getCategory() == CategorySessionType::CATEGORY_SESSION_WEBINAR) {
                $this->addFlash('success', 'The emails of the communication scheme were sent by email.');
            } else {
                $this->addFlash('success', 'The notification test were sent by e-mail');
            }
        } else {
            $this->addFlash('error', 'The emails provided are not valid, the notification test have not been sent.');
        }

        return $this->redirectToRoute('_session_get', ['id' => $providerSession->getId()]);
    }

    #[Route('/send-training-certificate/{id}', name: '_session_send_training_certificate')]
    #[IsGranted(ProviderSessionVoter::ACCESS, subject: 'providerSession')]
    public function sendTrainingCertificate(ProviderSession $providerSession, SendTrainingCertificatesService $sendCertificatesService): Response
    {
        if (!empty($providerSession->getTraineesOnly())) {
            $sendCertificatesService->sendTrainingCertificateParticipant(providerSession: $providerSession, sender: $this->getUser());
            $this->addFlash('success', 'Training certificates have been sent by e-mail');
        } else {
            $this->addFlash('error', 'This session does not contain participants, the training certificates have not been sent.');
            $this->logger->error(
                new \Exception('This session does not contain participants, the training certificates have not been sent.'), [
                'sessionId' => $providerSession->getId(),
            ]);
        }

        return $this->redirectToRoute('_session_get', ['id' => $providerSession->getId()]);
    }

    #[Route('/wizard-create-session', name: '_session_create_from_wizard', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function createFromWizard(ClientRepository $clientRepository): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($this->isGranted(ImportVoter::CAN_SELECT_CLIENT, $user)) {
            $clients = $clientRepository->findAllWithoutArchived();
        } else {
            $clients = [$user->getClient()];
        }

        $importRestrictions = [
            'max_file_size' => RessourceUploader::humanReadableFileSize(ImportSessionService::MAX_FILE_SIZE),
            'allowed_extension' => implode(', ', ImportSessionService::AUTHORIZED_EXTENSION),
            'allowed_max_row' => ImportSessionService::ALLOWED_MAX_ROW,
        ];

        return $this->render('sessions/wizard.html.twig', [
            'listNewTimezones' => $this->providerSessionService->getListTimezones($this->getUser()),
            'title' => 'Assisted session creation',
            'layout' => 'step1',
            'user' => $user,
            'importRestrictions' => $importRestrictions,
            'clients' => $clients,
            'targetClientIdForCurrentUser' => $user->getClient()->getId(),
        ]);
    }

    #[Route('/choice-client-filter', name: '_session_choice_client_filter', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function choiceClientFilterAction(
        Request $request,
        ConvocationRepository $convocationRepository,
        ClientRepository $clientRepository,
        AdobeConnectService $adobeConnectService,
        AdobeConnectWebinarService $adobeConnectWebinarService,
        ProviderSessionService $providerSessionService,
        SerializerInterface $serializer,
        Security $security
    ): Response {
        $requestContent = json_decode($request->getContent());
        $clientId = $requestContent->clientId;
        $dateStart = \DateTime::createFromFormat('Y-m-d\TH:i', substr($requestContent->dateStart, 0, 16));
        $durationParts = explode('h', $requestContent->duration);
        $hours = intval($durationParts[0]);
        $minutes = intval($durationParts[1]);
        $dateEnd = clone $dateStart; // Cloner pour éviter de modifier la date de début
        $dateEnd->add(new \DateInterval("PT{$hours}H{$minutes}M"));

        /** @var Client $client */
        $client = $clientRepository->find($clientId);

        if (!$security->isGranted(ClientVoter::ACCESS, $client) && !$security->isGranted(User::ROLE_HOTLINE_CUSTOMER_SERVICE)) {
            throw new AccessDeniedException();
        }

        $evaluations = $serializer->serialize($client->getEvaluations(), 'json', ['groups' => 'read_client']);

        $dates = $providerSessionService->getConvocationAndRemindersDate($client->getEmailScheduling(), $dateStart, 'Y-m-d\TH:i:s.vp');

        /** @var AbstractProviderConfigurationHolder $configurationHolder */
        $configurationHolder = $client->getFirstConfigurationHolderActive();

        $response = [
            'reference' => ProviderSessionService::generateSessionReference(preg_replace('/[^A-Za-z0-9. -]/', '', $client->getName())),
            'client' => [
                'defaultLang' => $client->getDefaultLang(),
                'evaluations' => $evaluations,
                'categories' => $client->getCategories(),
                'isSendAnAttestationAutomatically' => $client->getEmailOptions()->isSendAnAttestationAutomatically(),
                'organisationDefaultTrainingActionNature' => $client->getOrganisationDefaultTrainingActionNature(),
                'connectors' => $client->getConfigurationHoldersActive()->getValues(),
                'services' => $client->getServices(),
                'billingMode' => $client->getBillingMode(),
                'remainingCredit' => $client->getRemainingCredit(),
                'allowedCredit' => $client->getAllowedCredit(),
                'notificationCreditExhausted' => $client->getOptionFromAlert(Client::NOTIFICATION_NO_MORE_CREDIT_OPTION_LIMIT),
                'modules' => $client->getSelfSubscriptionModules(),
                'selfRegisteringEnabled' => $client->getSelfRegistering()->isEnabled(),
                'registrationPageEnabled' => $client->isEnableRegistrationPage(),
                'registrationPages' => $client->getRegistrationPageTemplates(),
                'timezone' => $client->getTimezone(),
            ],
            'configurationHolder' => [
                'type' => $configurationHolder::getProvider(),
                'id' => $configurationHolder->getId(),
            ],
            'notificationDate' => $dates['convocation'] ?? '',
            'datesReminders' => $dates['reminders'] ?? [],
            'attachmentRestriction' => [
                'allowed_extension' => implode(',', SessionConvocationAttachment::AUTHORIZED_EXTENSIONS),
                'max_file_size' => SessionConvocationAttachment::MAX_FILE_SIZE,
            ],
        ];

        switch (get_class($configurationHolder)) {
            case AdobeConnectConfigurationHolder::class:
                /** @var AdobeConnectConfigurationHolder $configurationHolder */
                $roomModels = $adobeConnectService->getSharedMeetingsTemplate($configurationHolder);
                $allRoomModels = $adobeConnectService->findAllModelsRoom($configurationHolder);

                $response['configurationHolder'] = array_merge($response['configurationHolder'], [
                    'allRoomModels' => $allRoomModels->isSuccess() ? $allRoomModels->getData() : [],
                    'roomModels' => $roomModels->isSuccess() ? $roomModels->getData() : [],
                    'admittedOnRoom' => $configurationHolder->getDefaultConnexionType() ?: null,
                    'defaultRoomModel' => $configurationHolder->getDefaultRoom() ?: null,
                    'folderRoomModel' => $configurationHolder->getFolderDefaultRoom() ?: null,
                ]);
                break;
            case AdobeConnectWebinarConfigurationHolder::class:
                /** @var AdobeConnectWebinarConfigurationHolder $configurationHolder */
                $roomModels = $adobeConnectWebinarService->getSharedMeetingsTemplate($configurationHolder);

                $response['configurationHolder'] = array_merge($response['configurationHolder'], [
                    'roomModels' => $roomModels->isSuccess() ? $roomModels->getData() : [],
                    'admittedOnRoom' => $configurationHolder->getDefaultConnexionType() ?: null,
                    'defaultRoomModel' => $configurationHolder->getDefaultRoom() ?: null,
                ]);
                break;
            case WebexConfigurationHolder::class:
                $response['configurationHolder'] = array_merge($response['configurationHolder'], [
                    'licences' => $configurationHolder->getLicences(),
                    'automaticLicensing' => $configurationHolder->isAutomaticLicensing(),
                ]);
                break;
            case WebexRestConfigurationHolder::class:
                $response['configurationHolder'] = array_merge($response['configurationHolder'], [
                    'licences' => (!empty($dateEnd)) ? $this->webexRestLicensingService->getAvailableLicences($configurationHolder, $dateStart, $dateEnd, 30, null) : $configurationHolder->getLicences(),
                    'automaticLicensing' => $configurationHolder->isAutomaticLicensing(),
                ]);
                break;
            case MicrosoftTeamsConfigurationHolder::class:
                $response['configurationHolder'] = array_merge($response['configurationHolder'], [
                    'licences' => $configurationHolder->getLicences(),
                    'automaticLicensing' => $configurationHolder->isAutomaticLicensing(),
                ]);
                break;
            case MicrosoftTeamsEventConfigurationHolder::class:
                $response['configurationHolder'] = array_merge($response['configurationHolder'], [
                    'licences' => $configurationHolder->getLicences(),
                    'automaticLicensing' => $configurationHolder->isAutomaticLicensing(),
                ]);
                break;
            case WhiteConfigurationHolder::class:
                $response['configurationHolder'] = array_merge($response['configurationHolder'], []);
                break;
        }

        $convocations = $convocationRepository->findBy(['status' => 1, 'client' => $client]);

        /** @var Convocation $convocation */
        foreach ($convocations as $convocation) {
            $response['convocations'][$convocation->getId()] = [
                'id' => $convocation->getId(),
                'name' => $convocation->getName(),
                'lang' => $convocation->getLanguages(),
                'audioCategoryType' => $convocation->getCategoryTypeAudio(),
                'audioType' => $convocation->getTypeAudio(),
                'connectorType' => $convocation->getConnectorType(),
            ];
        }

        $responseSerialized = $this->serializer->serialize($response, 'json', ['groups' => ['read_client', 'read_client_services', 'session_wizard']]);

        return new Response($responseSerialized);
    }

    #[Route('/wizard/get_available_user/{id}', name: '_session_create_from_wizard_get_available_user', options: ['expose' => true])]
    #[IsGranted(ClientVoter::ACCESS, subject: 'client')]
    public function getParticipantForWizard(Client $client, UserRepository $userRepository): Response
    {
        return $this->apiResponse(
            $userRepository->getAllClientUserMinimalData($client)
        );
    }

    #[Route('/wizard/get_available_group/{id}', name: '_session_create_from_wizard_get_available_group', options: ['expose' => true])]
    #[IsGranted(ClientVoter::ACCESS, subject: 'client')]
    public function getGroupForWizard(Client $client): Response
    {
        return $this->apiResponse(
            $this->groupParticipantService->extractDataGroups($this->groupParticipantRepository->getAllClientGroup($client))
        );
    }

    #[Route('/save', name: '_session_save', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function saveAction(
        Request $request,
        SessionRequestHandler $handler,
        UserRepository $userRepository,
        ParticipantSessionSubscriber $participantSessionSubscriber,
        ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        TranslatorInterface $translator,
        ConvocationService $convocationService,
        LoggerInterface $providerLogger,
    ): Response {
        $isObjectiveDraft = $request->query->has('draft') ? $request->query->get('draft') : 'false';

        $data = json_decode($request->getContent(), true);
        if (!is_array($data) || empty($data)) {
            throw new ProviderSessionRequestHandlerException('Content not provided', [], false, 'Content not provided');
        }
        $providerLogger->info('Saving session', ['data' => $data, 'loggedUserId' => $this->getUser()->getId()]);

        $action = array_key_exists('id', $data) ? ActivityLog::ACTION_SESSION_UPDATE : ActivityLog::ACTION_SESSION_CREATION;
        try {
            $session = $handler->handleRequest($data, $this->getUser());
            //throw new ProviderSessionRequestHandlerException('Content not provided', [], false, 'Content not provided');
            $warnings = [];
            if (array_key_exists('additionalTimezones', $data)) {
                $session->setAdditionalTimezones($data['additionalTimezones']);
            }

            if (array_key_exists('participants', $data)) {
                $warnings = $participantSessionSubscriber->massSubscribe($session, $data['participants']);
            }

            if (array_key_exists('additionalTimezones', $data)) {
                $session->setAdditionalTimezones($data['additionalTimezones']);
            }

            if (array_key_exists('animatorsOnly', $data)) {
                foreach ($data['animatorsOnly'] as $participant) {
                    $participantSessionRole = $providerParticipantSessionRoleRepository->find($participant['id']);
                    if (!empty($participantSessionRole) && !$participantSessionSubscriber->isRegisteredWithRole($participantSessionRole->getEmail(), $session, $participantSessionRole->getRole(), intval($participant['situationMixte']))) {
                        $participantSessionSubscriber->updateParticipantSessionRoleRole($participantSessionRole, $participant['role'], intval($participant['situationMixte']));
                    }
                }
            }

            if (array_key_exists('traineesOnly', $data)) {
                foreach ($data['traineesOnly'] as $participant) {
                    $participantSessionRole = $providerParticipantSessionRoleRepository->find($participant['id']);
                    if (!empty($participantSessionRole) && !$participantSessionSubscriber->isRegisteredWithRole($participantSessionRole->getEmail(), $session, $participantSessionRole->getRole(), intval($participant['situationMixte']))) {
                        $participantSessionSubscriber->updateParticipantSessionRoleRole($participantSessionRole, $participant['role'], intval($participant['situationMixte']));
                    }
                }
            }

            if (array_key_exists('groups', $data)) {
                $warnings = $this->sessionRequestHandler->subscribeGroups($session, $data['groups']);
            }

            $configurationHolder = $session->getAbstractProviderConfigurationHolder();
            if (
                $configurationHolder instanceof AdobeConnectConfigurationHolder &&
                empty($session->getProviderAudio()) &&
                $session->getConvocation()->getCategoryTypeAudio() != AbstractProviderAudio::AUDIO_CATEGORY_COMPUTER &&
                $session->getConvocation()->getCategoryTypeAudio() != AbstractProviderAudio::AUDIO_CATEGORY_PRESENTIAL
            ) {
                $warnings[] = $this->translator->trans(
                    'Session \'%sessionName%\' (ref: %sessionRef%) is stuck in draft status because there is no audio profile available',
                    ['%sessionName%' => $session->getName(), '%sessionRef%' => $session->getRef()]
                );
            }

            if ($session->getCategory()->label() == ProviderSession::FINAL_CATEGORY_WEBINAR) {
                if ($action == ActivityLog::ACTION_SESSION_CREATION) {
                    $session->setStatus(ProviderSession::STATUS_DRAFT);
                    $warnings[] = $this->translator->trans('Your session is in Draft mode. To validate it, you can now create its communication scheme: in the session form, “Communication” tab, define the emails, schedule them and test them.');
                } else {
                    try {
                        $typeCommunication = '';
                        $subjectCommunication = '';
                        foreach ($session->getWebinarConvocation() as $communication) {
                            $typeCommunication = $communication->getConvocationTextType();
                            $subjectCommunication = $communication->getSubjectMail();
                            $shortsCodesConvocationContent = $convocationService->checkShortsCodesValid($communication->getContent());
                            if (count($shortsCodesConvocationContent) > 0) {
                                $errorMessage = $this->translator->trans('Unknown short codes have been detected in the content of your').' '.$this->translator->trans($typeCommunication).' " '.$subjectCommunication.' " :  ';
                                foreach ($shortsCodesConvocationContent as $shortsCodes) {
                                    $errorMessage .= '<br> * '.$shortsCodes;
                                }

                                return $this->apiErrorResponse($errorMessage);
                            }
                        }
                    } catch (Throwable $exception) {
                        $errorMessage = $this->translator->trans($exception->getRawMessage()).' __if__is__ '.$this->translator->trans('in message body of').' '.$this->translator->trans($typeCommunication).' " '.$subjectCommunication.' " ';

                        return $this->apiErrorResponse($errorMessage);
                    }

                    if (!$session->hasACommunicationConfirmation()) {
                        $session->setStatus(ProviderSession::STATUS_DRAFT);
                        $this->addFlash('warning', 'You have changed the session category, your session now has the status "Draft"');
                    }
                }
            }

            //Rien n'est fait dans cette condition pour $session instanceof WebexRestMeeting
            if (!$session instanceof WhiteSession && !$session instanceof WebexRestMeeting) {
                $this->licenceChecker->checkLicence($session);
            } elseif ($session instanceof WhiteSession) {
                if ($session->getCategory()->value === CategorySessionType::CATEGORY_SESSION_PRESENTIAL->value) {
                    if (empty($session->getLocation())) {
                        $session->setStatus(ProviderSession::STATUS_DRAFT);
                        $warnings[] = $this->translator->trans('Your session is in \'Draft\' status, it will be validated as soon as you have added the location.');
                    }
                } else {
                    if (empty($session->getJoinSessionWebUrl())) {
                        $session->setStatus(ProviderSession::STATUS_DRAFT);
                        $warnings[] = $this->translator->trans('Your session is in \'Draft\' status, it will be validated as soon as you have added the virtual room link.');
                    }
                }
            }

            if ($isObjectiveDraft == 'true') {
                $session->setStatus(ProviderSession::STATUS_DRAFT);
                $warnings[] = $this->translator->trans(
                    'Session \'%sessionName%\' (ref: %sessionRef%) is in draft status.',
                    ['%sessionName%' => $session->getName(), '%sessionRef%' => $session->getRef()]
                );
            }
            if ($session instanceof WebexRestMeeting) {
                $session->setLicence($session->getMeetingRestHostEmail());
            }
            $this->entityManager->persist($session);
            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();

            if (array_key_exists('updateFields', $data)) {
                if ($this->providerSessionService->isUpdatedFields($data['updateFields'], $session)) {
                    if (array_key_exists('sendEmailForEdit', $data) && !$data['sendEmailForEdit']) {
                        $this->addFlash('success', 'The invitations were not sent back to the participants to inform them of the modification.');
                    } else {
                        if ($session->isRunning() || $session->getReadableState() == ProviderSession::FINAL_STATUS_SENT) {
                            $this->addFlash('success', 'The invitations were sent back to the participants to inform them of the modification.');
                        }
                    }
                }
            }

            return $this->apiResponse([
                'success' => (count($warnings) === 0),
                'warnings' => $warnings,
                'sessionId' => $session->getId(),
            ]);
        } catch (LicenceCheckException $exception) {
            $this->logger->error($exception, ['loggedUserId' => $this->getUser()->getId()]);
            $providerLogger->error($exception, ['data' => $data, 'loggedUserId' => $this->getUser()->getId()]);

            return $this->apiErrorResponse($this->translator->trans($exception->getKey(), $exception->getParams()));
        } catch (ProviderSessionRequestHandlerException $exception) {
            $this->logger->error($exception, ['loggedUserId' => $this->getUser()->getId()]);
            $providerLogger->error($exception, ['data' => $data, 'loggedUserId' => $this->getUser()->getId()]);

            return $this->apiErrorResponse($exception->getMessage());
        } catch (MiddlewareFailureException $exception) {
            $this->logger->error($exception, [$exception->getData()] ?? []);
            $providerLogger->error($exception, ['data' => $data, 'loggedUserId' => $this->getUser()->getId()]);
            if (is_array($exception->getData()) && array_key_exists('licence', $exception->getData())) {
                $returnMixed = $exception->getData()['licence'];

                return $this->apiErrorResponse($returnMixed, Response::HTTP_BAD_REQUEST);
            } else {
                if (is_string($exception->getData())) {
                    $returnMixed = (!empty($exception->getData())) ? $exception->getData() : '';
                } else {
                    $returnMixed = (!empty($exception->getData())) ? $exception->getData()->__toString() : '';
                }

                return $this->apiErrorResponse($exception->getMessage(), Response::HTTP_BAD_REQUEST, $returnMixed);
            }
        } catch (\Throwable $throwable) {
            $this->logger->error($throwable, $data);
            $providerLogger->error($throwable, ['data' => $data, 'loggedUserId' => $this->getUser()->getId()]);

            return $this->apiErrorResponse(
                $throwable->getMessage() !== ''
                    ? $this->translator->trans($throwable->getMessage())
                    : $this->translator->trans('An unknown error occurred while creating the session.')
            );
        }
    }

    #[Route('/generate-archive', name: '_session_generate_archive', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function generateExportSessions(
        Request $request,
        ExporterService $exporterService,
        ProviderSessionRepository $providerSessionRepository,
    ): Response {
        $data = json_decode($request->getContent());
        ini_set('max_execution_time', 0);
        $type = $data->type ?? AbstractSpreadsheetEvaluationReportProviderSessionExporter::EXPORT_TYPE;
        if ($data->sessionsToday) {
            $from = new \DateTimeImmutable('00:00:00');
            $to = new \DateTimeImmutable('23:59:59');
        } else {
            $from = new \DateTimeImmutable($data->from ?? '00:00:00');
            $to = (new \DateTimeImmutable($data->to ?? '23:59:59'))->setTime(23, 59, 59);
        }
        $format = $data->format ?? ExportFormat::XLSX;
        $grouping = $data->grouping ?? false;

        if ($type == 'sessions_export_tpm') {
            $sessions = $providerSessionRepository->findAllBetweenDatesWithOrderByClientAndSessionsStart($from, $to);
        } elseif ($type == 'multi_evaluations_report') {
            $sessions = $providerSessionRepository->findAllBetweenDatesForResultEvaluations($from, $to);
            foreach ($sessions as $key => $session) {
                if (count($session->getEvaluationsProviderSession()) === 0 || !$session->isFinished() || $session->isSessionTest()) {
                    unset($sessions[$key]);
                }
            }
        } else {
            $sessions = $providerSessionRepository->findAllBetweenDates($from, $to);
        }

        return $exporterService->generateArchive($type, $format, $sessions, $grouping);
    }

    #[Route('/generate-archive-replay-statistics/{id}', name: '_session_generate_replay_statistics', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted(ProviderSessionVoter::ACCESS, subject: 'providerSession')]
    public function generateExportReplayStatistics(
        Request $request,
        ProviderSession $providerSession,
        ExporterService $exporterService,
    ): Response {
        return $exporterService->generateArchive('replay_export_statistics', ExportFormat::XLSX, [$providerSession]);
    }

    #[Route('/{id}/update-participants', name: '_session_update_participants', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted(ProviderSessionVoter::UPDATE_PARTICIPANT, subject: 'session')]
    public function updateSessionParticipantRoles(
        Request $request,
        ProviderSession $session,
        SessionRequestHandler $sessionRequestHandler
    ): Response {
        try {
            $this->activityLogger->initActivityLogContext(
                $session->getClient(),
                $this->getUser(),
                ActivityLog::ORIGIN_USER_INTERFACE,
                [
                    'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                ]
            );

            $session = $sessionRequestHandler->handleUpdateParticipantListRequest($request, $session);

            if (!$session instanceof WhiteSession) {
                $this->licenceChecker->checkLicence($session);
            }

            $this->entityManager->flush();

            return $this->apiResponse(['success' => true, 'sessionId' => $session->getId()]);
        } catch (ProviderSessionRequestHandlerException $exception) {
            $this->logger->error($exception, [
                'sessionId' => $session->getId(),
                'loggedUserId' => $this->getUser()->getId(),
            ]);

            return $this->apiErrorResponse($exception->getMessage());
        } catch (Throwable $t) {
            $this->logger->error($t, [
                'sessionId' => $session->getId(),
                'loggedUserId' => $this->getUser()->getId(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An unknown error occurred while updating the participant in the session.'));
        } finally {
            $this->activityLogger->flushActivityLogs();
        }
    }

    #[Route('/details-support', name: '_session_get_details_support', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_MANAGER')]
    public function getDetailsSupportSession(Request $request, ClientRepository $clientRepository): Response
    {
        $clientId = $request->query->get('id_client');
        $sessionDateStart = $request->query->get('sessionDateStart');
        $sessionDateEnd = $request->query->get('sessionDateEnd');
        $standardHoursActive = $request->query->get('standardHoursActive', 'true');
        $outsideStandardHoursActive = $request->query->get('outsideStandardHoursActive', 'false');
        $typeSupport = $request->query->get('typeSupport', ProviderSession::ENUM_SUPPORT_NO);

        $client = $clientRepository->find($clientId);

        try {
            $details = $this->supportService->getDetailsSessionSupport(
                $client,
                new \DateTime($sessionDateStart),
                new \DateTime($sessionDateEnd),
                $standardHoursActive === 'true',
                $outsideStandardHoursActive === 'true',
                intval($typeSupport)
            );
        } catch (Throwable $e) {
            $this->logger->error($e, [
                'clientId' => $clientId,
                'sessionDateStart' => $sessionDateStart,
                'sessionDateEnd' => $sessionDateEnd,
                'standardHoursActive' => $standardHoursActive,
                'outsideStandardHoursActive' => $outsideStandardHoursActive,
                'typeSupport' => $typeSupport,
            ]);

            return new JsonResponse('Unable to retrieve support details', Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse($details);
    }

    #[Route('/convocation/preview/{id}', name: '_session_convocation_preview', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function previewConvocation(Convocation $convocation): Response
    {
        return $this->apiResponse([
            'content' => $this->convocationService->transformWithFakeData($convocation->getContent()),
        ]);
    }

    #[Route('/communication/preview/{id}', name: '_session_communication_webinar_preview', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function previewCommunication(WebinarConvocation $webinarConvocation): Response
    {
        return $this->apiResponse([
            'content' => $this->convocationService->transformWithFakeData($webinarConvocation->getContent()),
        ]);
    }

    #[Route('/{id}/redirect-connector', name: '_session_redirect_connector', requirements: ['id' => '\d+'], methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function redirectToRoom(ProviderSession $providerSession): Response
    {
        try {
            $response = match (get_class($providerSession)) {
                AdobeConnectSCO::class => $this->adobeConnectService->redirectConnector($providerSession),
                WebexMeeting::class => $this->webexMeetingService->getConnectorJoinUrl($providerSession),
                default => (new ProviderResponse(false))
            };

            if ($response->isSuccess()) {
                return $this->redirect($response->getData());
            }

            $this->logger->error(new \Exception('Unable to login and redirect to the room'), [
                'provider' => $providerSession::getProvider(),
                'sessionId' => $providerSession->getId(),
                'data' => $response->getData(),
                'exceptionMessage' => $response->getThrown()?->getMessage(),
                'exceptionClass' => !is_null($response->getThrown())
                    ? get_class($response->getThrown())
                    : null,
            ]);
        } catch (\Throwable $e) {
            $this->logger->error($e, [
                'provider' => $providerSession::getProvider(),
                'sessionId' => $providerSession->getId(),
            ]);
        }

        $this->addFlash('error', 'Unable to login and redirect to the room');

        return $this->redirectToRoute('_session_get', ['id' => $providerSession->getId()]);
    }

    #[Route('/provider-animator-room-model/get/{id}', name: '_animators_room_model', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function getAnimatorsRoomModel(AbstractProviderConfigurationHolder $configurationHolder, AdobeConnectService $adobeConnectService, Request $request): Response
    {
        switch (get_class($configurationHolder)) {
            case AdobeConnectConfigurationHolder::class:
                /** @var AdobeConnectConfigurationHolder $configurationHolder */
                $animatorsRoomModel = $adobeConnectService->getAnimatorsRoomModel($configurationHolder);

                break;
            case AdobeConnectWebinarConfigurationHolder::class:
                /* @var AdobeConnectWebinarConfigurationHolder $configurationHolder */
                break;
        }

        return $this->apiResponse(['content' => $animatorsRoomModel->getData()]);
    }

    #[Route('/save-percentage-attendance_provider_session/{id}', name: '_session_save_percentage_attendance', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function savePercentageAttendanceProviderSession(Request $request, ProviderSession $providerSession): Response
    {
        if (!$request->request->has('percentageAttendance')) {
            return $this->createErrorResponse('Percentage of attendance not provided');
        }

        $percentageAttendance = (int) $request->request->get('percentageAttendance');
        $canUpdateAttendance = $providerSession->getCanUpdatePercentageAttendance();
        if ($canUpdateAttendance['status']) {
            $providerSession->setPercentageAttendance($percentageAttendance);
            $this->entityManager->flush();

            return $this->createSuccessResponse('The percentage of attendance has been saved');
        } else {
            return $this->createErrorResponse($canUpdateAttendance['message']);
        }
    }

    #[Route('/provider-room-models-from-animator/get/{id}', name: '_room_models_from_animator', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function getRoomModelsFromAnimator(AbstractProviderConfigurationHolder $configurationHolder, AdobeConnectService $adobeConnectService, Request $request): Response
    {
        $animatorsRoomModel = [];
        switch (get_class($configurationHolder)) {
            case AdobeConnectConfigurationHolder::class:
                /* @var AdobeConnectConfigurationHolder $configurationHolder */
                if (!empty($request->query->get('scoid'))) {
                    //On remonte les modèles d'un sous-répertoire du scoId de l'animateur
                    $animatorsRoomModel = $adobeConnectService->getRoomModelsFromAnimator($configurationHolder, $request->query->get('scoid'));
                } else {
                    //On remonte les modèles présents à la racine du répertoire Modèles
                    $animatorsRoomModel = $adobeConnectService->getSharedMeetingsTemplate($configurationHolder, 'meeting');
                }

                break;
            case AdobeConnectWebinarConfigurationHolder::class:
                /* @var AdobeConnectWebinarConfigurationHolder $configurationHolder */
                break;
        }

        return $this->apiResponse(['content' => $animatorsRoomModel->getData()]);
    }

    #[Route('/provider/get/{id}', name: '_provider_get_informations', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function getInformationsProviderWizardCreate(AbstractProviderConfigurationHolder $configurationHolder, AdobeConnectService $adobeConnectService, AdobeConnectWebinarService $adobeConnectWebinarService, Request $request): Response
    {
        $dateStart = \DateTime::createFromFormat('Y-m-d\TH:i', substr($request->query->get('dateStart'), 0, 16));
        if (!empty($request->query->get('duration'))) {
            $durationParts = explode('h', $request->query->get('duration'));
            $hours = intval($durationParts[0]);
            $minutes = intval($durationParts[1]);
            $dateEnd = clone $dateStart; // Cloner pour éviter de modifier la date de début
            $dateEnd->add(new \DateInterval("PT{$hours}H{$minutes}M"));
        }

        $configurationHolderProvider['id'] = $configurationHolder->getId();
        $configurationHolderProvider['type'] = $configurationHolder::getProvider();
        switch (get_class($configurationHolder)) {
            case AdobeConnectConfigurationHolder::class:
                /** @var AdobeConnectConfigurationHolder $configurationHolder */
                $roomModels = $adobeConnectService->getSharedMeetingsTemplate($configurationHolder);

                $configurationHolderProvider = array_merge($configurationHolderProvider, [
                    'roomModels' => $roomModels->isSuccess() ? $roomModels->getData() : [],
                    'admittedOnRoom' => $configurationHolder->getDefaultConnexionType() ?: null,
                    'defaultRoomModel' => $configurationHolder->getDefaultRoom() ?: null,
                    'folderRoomModel' => $configurationHolder->getFolderDefaultRoom() ?: null,
                ]);
                break;
            case AdobeConnectWebinarConfigurationHolder::class:
                /** @var AdobeConnectWebinarConfigurationHolder $configurationHolder */
                $roomModels = $adobeConnectWebinarService->getSharedMeetingsTemplate($configurationHolder);

                $configurationHolderProvider = array_merge($configurationHolderProvider, [
                    'roomModels' => $roomModels->isSuccess() ? $roomModels->getData() : [],
                    'admittedOnRoom' => $configurationHolder->getDefaultConnexionType() ?: null,
                    'defaultRoomModel' => $configurationHolder->getDefaultRoom() ?: null,
                ]);
                break;
            case WebexConfigurationHolder::class:
                $configurationHolderProvider = array_merge($configurationHolderProvider, [
                    'licences' => $configurationHolder->getLicences(),
                    'automaticLicensing' => $configurationHolder->isAutomaticLicensing(),
                ]);
                break;
            case WebexRestConfigurationHolder::class:
                $configurationHolderProvider = array_merge($configurationHolderProvider, [
                    //'licences' => $configurationHolder->getLicences(),
                    'licences' => $this->webexRestLicensingService->getAvailableLicences($configurationHolder, $dateStart, $dateEnd, 30, null),
                    'automaticLicensing' => $configurationHolder->isAutomaticLicensing(),
                ]);
                break;
            case MicrosoftTeamsConfigurationHolder::class:
                $configurationHolderProvider = array_merge($configurationHolderProvider, [
                    'licences' => $configurationHolder->getLicences(),
                    'automaticLicensing' => $configurationHolder->isAutomaticLicensing(),
                ]);
                break;
            case MicrosoftTeamsEventConfigurationHolder::class:
                $configurationHolderProvider = array_merge($configurationHolderProvider, [
                    'licences' => $configurationHolder->getLicences(),
                    'automaticLicensing' => $configurationHolder->isAutomaticLicensing(),
                ]);
                break;
            case WhiteConfigurationHolder::class:
                $configurationHolderProvider = array_merge($configurationHolderProvider, []);
                break;
        }

        return $this->apiResponse(['content' => $configurationHolderProvider]);
    }

    #[Route('/{id}/add-communication-webinar', name: '_add_communication_webinar', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function addNewCommunicationWebinar(
        Request $request,
        ProviderSession $session,
        ConvocationService $convocationService,
        ConvocationRepository $convocationRepository
    ): Response {
        try {
            if ($request->query->get('type') == WebinarConvocation::TYPE_CONFIRMATION && $convocationService->existeCommunicationWebinarConfirmationType($session)) {
                return $this->apiErrorResponse($this->translator->trans('you cannot create a new confirmation if a confirmation is already created'));
            }

            if ($request->query->get('type') != WebinarConvocation::TYPE_SMS) {
                $webinarCommunication = $convocationService->addNewCommunicationWebinar(
                    convocation: $convocationRepository->findOneBy(['id' => $request->query->get('idConvocation')]),
                    communicationType: $request->query->get('type'),
                    session: $session,
                    user: $this->getUser()
                );
                $this->addFlash('success', 'A webinar communication has been added');
            } else {
                $webinarCommunication = $convocationService->addNewCommunicationSMSWebinar(
                    communicationType: $request->query->get('type'),
                    session: $session,
                    user: $this->getUser()
                );
                $this->addFlash('success', 'A webinar communication sms has been added');
            }
            $this->entityManager->persist($webinarCommunication);
            $this->entityManager->flush();

            return $this->apiResponse(['success' => true, 'sessionId' => $session->getId()]);
        } catch (Throwable $t) {
            $this->logger->error($t, [
                'sessionId' => $session->getId(),
                'loggedUserId' => $this->getUser()->getId(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An unknown error occurred while added webinar communication in the session.'));
        }
    }

    #[Route('/{id}/validate-draft-communication-webinar', name: '_validate_draft_communication_webinar', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function validateDraftCommunicationWebinar(
        ProviderSession $session
    ): Response {
        try {
            if ($session->hasACommunicationConfirmation()) {
                $session->setStatus(ProviderSession::STATUS_SCHEDULED);
                $this->clientService->useCreditClient($session->getClient(), $session->getTraineesCount());
                $this->entityManager->persist($session);
                $this->entityManager->flush();
                $this->addFlash('success', 'The session has been validated, the emails will go out at the times indicated on the session sheet.');

                return $this->apiResponse(['success' => true, 'sessionId' => $session->getId()]);
            } else {
                return $this->apiErrorResponse($this->translator->trans("You don't have a confirmation type communication"));
            }
        } catch (Throwable $t) {
            $this->logger->error($t, [
                'sessionId' => $session->getId(),
                'loggedUserId' => $this->getUser()->getId(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An error occurred while validate a draft'));
        }
    }

    #[Route('/{id}/validate-update-replay', name: '_validate_replay_updated', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function validateUpdatedReplay(
        Request $request,
        ProviderSession $session
    ): Response {
        try {
            $session->setAccessParticipantReplay($request->request->get('accessParticipantReplay'));
            $session->setPublishedReplay($request->request->get('publishedReplay'));
            $session->setReplayUrlKey($request->request->get('replayUrlKey'));
            $session->setReplayLayoutPlayButton($request->request->get('replayLayoutPlayButton'));
            $session->setReplayVideoPlatform($request->request->get('replayVideoPlatform'));
            $this->entityManager->persist($session);
            $this->entityManager->flush();

            return $this->apiResponse(['success' => true, 'sessionId' => $session->getId()]);
        } catch (Throwable $t) {
            $this->logger->error($t, [
                'sessionId' => $session->getId(),
                'loggedUserId' => $this->getUser()->getId(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An error occurred while saving the replay configuration. Please try again later.'));
        }
    }

    #[Route('/{id}/validate-update-registration-page', name: '_validate_registration_page_updated', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function validateUpdatedRegistrationPage(
        Request $request,
        ProviderSession $session
    ): Response {
        try {
            $registrationPageTemplate = $this->pageTemplateRepository->findOneBy(['id' => $request->request->get('registrationPageTemplateId')]);
            if ($registrationPageTemplate) {
                $session = $this->providerSessionService->updateSessionWithRegistrationPageTemplate(session: $session, request: $request, registrationPageTemplate: $registrationPageTemplate, );
                $this->entityManager->persist($session);
                $this->entityManager->flush();

                return $this->apiResponse(['success' => true, 'sessionId' => $session->getId(), 'registrationPageTemplateBanner' => $session->getRegistrationPageTemplateBanner(), 'registrationPageTemplateIllustration' => $session->getRegistrationPageTemplateIllustration()]);
            }

            return $this->apiErrorResponse($this->translator->trans('You have not chosen a registration page template, you must choose one and save again'));
        } catch (Throwable $t) {
            $this->logger->error($t, [
                'sessionId' => $session->getId(),
                'loggedUserId' => $this->getUser()->getId(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An error occurred while saving the registration page configuration. Please try again later.'));
        }
    }

    #[Route('/{id}/delete-communication-webinar', name: '_delete_communication_webinar', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function deleteCommunicationWebinar(
        Request $request,
        WebinarConvocation $webinarConvocation
    ): Response {
        try {
            $session = $webinarConvocation->getSession();
            $this->entityManager->remove($webinarConvocation);

            if ($webinarConvocation->getConvocationType() == WebinarConvocation::TYPE_CONFIRMATION) {
                $session->setStatus(ProviderSession::STATUS_DRAFT);
            }

            $infos = [
                'message' => ['key' => 'A webinar communication %name% has been deleted', 'params' => ['%name%' => $request->query->get('communicationName')]],
                'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
            ];
            $this->activityLogger->initActivityLogContext($session->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
            $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_ADD_NEW_COMMUNICATION_WEBINAR, ActivityLog::SEVERITY_INFORMATION, $infos), true);

            $this->entityManager->flush();

            if ($webinarConvocation->getConvocationType() == WebinarConvocation::TYPE_SMS) {
                $this->addFlash('success', 'A webinar communication sms has been deleted');
            } else {
                $this->addFlash('success', 'A webinar communication has been deleted');
            }

            return $this->apiResponse(['success' => true, 'sessionId' => $session->getId()]);
        } catch (Throwable $t) {
            $this->logger->error($t, [
                'sessionId' => $session->getId(),
                'loggedUserId' => $this->getUser()->getId(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An unknown error occurred while deleted webinar communication in the session.'));
        } finally {
            $this->activityLogger->flushActivityLogs();
        }
    }

    #[Route('/send-email/participants', name: '_provider_session_send_email_participants', options: ['expose' => true], methods: ['POST'])]
    public function sendEmailToParticipants(Request $request): Response
    {
        try {
            $data = $request->request->all();
            $participants = json_decode($request->request->get('data'), true);
            if (!empty($participants['object'])) {
                $object = $participants['object'];
            } else {
                throw new Exception('Invalid email - the object is empty');
            }

            if (!empty($participants['message'])) {
                $message = $participants['message'];
            } else {
                throw new Exception('Invalid email - the message is empty');
            }
            /** @var UploadedFile $attachment */
            $attachment = (!empty($request->files->get('attachment'))) ? $request->files->get('attachment') : null;
            $response = $this->providerSessionService->sendEmailToParticipants($participants, $attachment);

            return $this->apiResponse([
                'filetarget' => $response['filename'],
                'previewUri' => $response['previewUri'],
            ]);
        } catch (\Exception $exception) {
            $this->logger->error($exception);

            return $this->apiResponse($exception->getMessage());
        }
    }

    #[Route('/{id}/evaluation-synthesis', name: '_session_evaluation_synthesis', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_TRAINEE')]
    public function getEvaluationSynthesis(ProviderSession $session): Response
    {
        return $this->render('sessions/session_evaluation_synthesis.html.twig', [
            'providerSession' => $session,
        ]);
    }

    #[Route('/{id}/evaluation-session-synthesis', name: '_provider_session_evaluation_synthesis', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_TRAINEE')]
    public function getEvaluationSessionSynthesis(EvaluationProviderSession $evaluationProviderSession): Response
    {
        return $this->render('sessions/provider_session_evaluation_synthesis.html.twig', [
            'providerSession' => $evaluationProviderSession->getProviderSession(),
            'evaluationProviderSession' => $evaluationProviderSession,
        ]);
    }

    #[Route('/{id}/evaluation-report-available', name: '_session_get_evaluation_report_available', options: ['expose' => true], methods: ['GET'])]
    public function getEvaluationReportAvailable(ProviderSession $session, EvaluationProviderSessionRepository $repository, Stopwatch $stopwatch): JsonResponse
    {
        return $this->apiResponse($repository->countAnswersByEvaluationProviderSession($session) > 0);
    }

    #[Route('/{id}/evaluations-report-available', name: '_session_get_evaluation_session_report_available', options: ['expose' => true], methods: ['GET'])]
     public function getEvaluationsReportAvailable(ProviderSession $session, EvaluationParticipantAnswerRepository $answerRepository, Stopwatch $stopwatch): JsonResponse
     {
         return $this->apiResponse($answerRepository->getCountSessionEvaluation($session) > 0);
     }

    #[Route('/{id}/participant-role-count', name: '_session_get_participant_role_count', options: ['expose' => true], methods: ['GET'])]
    public function getParticipantRoleCount(ProviderSession $session, ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository): JsonResponse
    {
        return $this->apiResponse([
            'trainees' => $providerParticipantSessionRoleRepository->countTrainees($session),
            'animators' => $providerParticipantSessionRoleRepository->countAnimators($session),
        ]);
    }

    #[Route('/replay-statistics/{id}', name: '_replay_statistics', options: ['expose' => true])]
    #[IsGranted(ProviderSessionVoter::ACCESS, subject: 'providerSession')]
    public function getReplayStatistics(ProviderSession $providerSession): Response
    {
        $reviewedReplays = 0;
        foreach ($providerSession->getReplays() as $replay) {
            $reviewedReplays += $replay->getSeen();
        }

        return $this->apiResponse($reviewedReplays);
    }

    #[Route('/webinar-invitation-statistics/{id}', name: '_webinar_invitation_statistics', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function getWebinarInvitationStatistics(WebinarConvocation $webinarConvocation): Response
    {
        return $this->apiResponse($this->webinarConvocationService->getStatisticsInvitationGuests($webinarConvocation));
    }

    #[Route('/webinar-sms-statistics/{id}', name: '_webinar_sms_statistics', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function getWebinarSmsStatistics(ProviderSession $session): Response
    {
        return $this->apiResponse($this->webinarConvocationService->getSmsStatistics($session));
    }

    #[Route('/get-list-new-additionals-timezones/{id}', name: '_session_get_list_new_additionals_timezones', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['POST'])]
    public function getListNewAdditionalsTimezones(Request $request, ProviderSession $providerSession): Response
    {
        return $this->apiResponse($this->providerSessionService->getListTimezones($this->getUser()));
    }

    #[Route('/upload/graphical-files', name: '_provider_session_upload_graphical_files', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function uploadFile(Request $request, string $registrationPageTemplateLogoUri): Response
    {
        try {
            /** @var UploadedFile $file */
            $file = $request->files->get('file');

            $formatParam = $request->request->get('format', implode(',', RegistrationPageTemplate::AUTHORIZED_EXTENSIONS));
            $this->ressourceUploader->isFileUploadedValid($file, explode(',', $formatParam), RegistrationPageTemplate::MAX_FILE_SIZE);

            $filename = \md5(\random_bytes(32)).'.'.$file->getClientOriginalExtension();

            (new Filesystem())->copy($file->getRealPath(), $this->registrationPageTemplateLogoSavefileDirectory.'/'.$filename, true);

            return $this->apiResponse([
                'filetarget' => $filename,
                'previewUri' => $registrationPageTemplateLogoUri.'/'.$filename,
            ]);
        } catch (UploadFileException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/{id}/{modelId}/add-evaluation-session', name: '_add_evaluation_session', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    #[ParamConverter('evaluation', options: ['mapping' => ['modelId' => 'id']])]
    public function addNewEvaluationSession(
        Request $request,
        ProviderSession $session,
        Evaluation $evaluation
    ): Response {
        try {
            $evaluationProviderSession = $this->evaluationProviderSessionService->createNewEvaluationProviderSession($evaluation, $session, $request->request->all()['type']);
            $this->entityManager->persist($evaluationProviderSession);
            $this->entityManager->flush();

            $this->addFlash('success', 'A evaluation has been added');

            return $this->apiResponse(['success' => true, 'sessionId' => $session->getId()]);
        } catch (Throwable $t) {
            $this->logger->error($t, [
                'sessionId' => $session->getId(),
                'loggedUserId' => $this->getUser()->getId(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An unknown error occurred while added evaluation in the session.'));
        }
    }

    #[Route('/{id}/delete-evaluation-session', name: '_delete_evaluation_session', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function deleteEvaluationSession(
        EvaluationProviderSession $evaluationProviderSession
    ): Response {
        try {
            $idEvaluationProviderSessionForLogger = $evaluationProviderSession->getId();
            $this->entityManager->remove($evaluationProviderSession);
            $this->entityManager->flush();

            $this->addFlash('success', 'The evaluation has been deleted successfully.');

            return $this->apiResponse(['success' => true]);
        } catch (Throwable $throwable) {
            $this->logger->error($throwable, [
                'evaluationId' => $idEvaluationProviderSessionForLogger,
                'loggedUserId' => $this->getUser()->getId(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An unknown error occurred while deleted webinar communication in the session.'));
        }
    }

    private function createSuccessResponse(string $message): Response
    {
        $responseMessage = $this->translator->trans($message);

        return new Response($responseMessage, Response::HTTP_ACCEPTED);
    }

    private function createErrorResponse(string $errorMessage): Response
    {
        $responseMessage = $this->translator->trans($errorMessage);

        return new Response($responseMessage, Response::HTTP_BAD_REQUEST);
    }

    /**
     * @throws SyntaxError
     * @throws LoaderError
     * @throws InvalidArgumentException
     */
    #[Route('/{id}/communication-webinar-test-sms', name: '_communication_webinar_test_sms', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function communicationWebinarTestSMS(Request $request, WebinarConvocation $webinarConvocation): Response
    {
        $requestContent = json_decode($request->getContent(), true);
        $res = $this->webinarConvocationService->testSendSms($webinarConvocation, $requestContent['recipient']);

        if (array_key_exists('error', $res)) {
            return $this->apiErrorResponse($res['error']);
        } else {
            return $this->apiResponse($res['success']);
        }
    }
}
