<?php

namespace App\Controller;

use App\Handler\IDFuseWebhookHandlerInterface;
use App\Security\WebhookAuthenticationInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/webhook-idfuse')]
class WebhookIDFuseController extends AbstractController
{
    public function __construct(
        private IDFuseWebhookHandlerInterface $webhookHandler,
        private ?WebhookAuthenticationInterface $webhookAuthentication = null
    ) {
    }

    #[Route('/handle', name: '_webhook_idfuse_handle', methods: ['POST', 'GET', 'HEAD'])]
    public function handle(Request $request): Response
    {
        // Mandrill will do a quick check that the URL exists by using a HEAD request (not POST).
        // Note that Symfony silently transforms HEAD requests to GET.
        // Enlever les commentaires si l'appel ne se fait pas du navigateur. On peut peut être tester avec postman !
        /*if (in_array($request->getMethod(), ['GET', 'POST', 'HEAD'])) {
            return new Response();
        }*/

        if (!$this->isAuthenticated($request)) {
            throw new AccessDeniedHttpException();
        }

        $this->webhookHandler->handleRequest($request);

        return new Response();
    }

    private function isAuthenticated(Request $request): bool
    {
        if (!$this->webhookAuthentication) {
            return true;
        }

        return $this->webhookAuthentication->isAuthenticated($request);
    }
}
