<?php

namespace App\Controller;

use App\Entity\OnHoldProviderParticipantSessionRole;
use App\Model\ApiResponseTrait;
use App\Service\ActivityLogger;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/on-hold-participant-session-role')]
class OnHoldProviderParticipantSessionRoleController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private TranslatorInterface $translator,
        private LoggerInterface $logger,
        private ActivityLogger $activityLogger
    ) {
    }

    #[Route('/{id}/remove', name: '_unregister_on_hold_participant', requirements: ['id' => "\d+"], options: ['expose' => true], methods: 'GET')]
    public function unregisterParticipant(OnHoldProviderParticipantSessionRole $participant): OnHoldProviderParticipantSessionRole
    {
        try {
            return $participant;
        } catch (Throwable $t) {
            $this->logger->error($t);
            $this->addFlash('error', $this->translator->trans('Unable to remove this participant from this session'));

            return $participant;
        }
    }
}
