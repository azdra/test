<?php

namespace App\Controller;

use App\Entity\ActivityLog;
use App\Entity\CategorySessionType;
use App\Entity\EvaluationProviderSession;
use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Event\TransactionalEmailRequestEventInterface;
use App\Event\TransactionalParticipantEmailRequestEvent;
use App\Model\ApiResponseTrait;
use App\Repository\EvaluationParticipantAnswerRepository;
use App\Repository\EvaluationSessionParticipantAnswerRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Security\ParticipantSessionRoleVoter;
use App\Service\ActivityLogger;
use App\Service\ConvocationService;
use App\Service\Exporter\ExporterFactory;
use App\Service\Exporter\ExporterService;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\Pdf\PdfCertificateProviderParticipantSessionRoleExporter;
use App\Service\MicrosoftTeamsEventService;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\ProviderParticipantSessionRoleService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/participant-session-role')]
class ProviderParticipantSessionRoleController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private ParticipantSessionSubscriber $participantSessionSubscriber,
        private EvaluationSessionParticipantAnswerRepository $evaluationSessionParticipantAnswerRepository,
        private TranslatorInterface $translator,
        private LoggerInterface $logger,
        private ActivityLogger $activityLogger,
        private ConvocationService $convocationService,
        private ExporterFactory $exporterFactory,
        private ExporterService $exporterService,
        private MicrosoftTeamsEventService $microsoftTeamsEventService,
    ) {
    }

    #[Route('/update-manual-presence-status/{id}/{presence}', name: '_participant_update_manual_presence_status', requirements: ['id' => "\d+", 'presence' => '0|1'], options: ['expose' => true])]
    #[IsGranted(ParticipantSessionRoleVoter::ACCESS, subject: 'participant')]
    public function updateManualPresenceStatus(Request $request, ProviderParticipantSessionRole $participant, bool $presence): Response
    {
        $participant->setManualPresenceStatus($presence);
        $this->entityManager->flush();

        if ($presence) {
            $this->addFlash('success', 'Participant is marked as present');
        } else {
            $this->addFlash('success', 'Participant is marked as absent');
        }

        return $this->redirectToRoute(
            '_session_get',
            [
                'id' => $participant->getSession()->getId(),
                'tabName' => $request->query->get('tabName') ?? '',
            ]
        );
    }

    #[Route('/{id}/set-as-license-participant', name: '_set_as_license_participant', requirements: ['id' => "\d+"], options: ['expose' => true], methods: 'GET')]
    #[IsGranted(ParticipantSessionRoleVoter::EDIT, subject: 'participant')]
    public function setAsLicenseParticipant(ProviderParticipantSessionRole $participant): Response
    {
        try {
            if ($participant->getSession()  instanceof MicrosoftTeamsEventSession) {
                //$eventMS = $this->microsoftTeamsEventService->getMeeting();
                $return = $this->microsoftTeamsEventService->changeSessionForNewLicence($participant->getSession(), $participant->getEmail());
                $this->entityManager->flush();
                $this->microsoftTeamsEventService->sendEmailToInformAllAnimatorsForLicenceChange($participant);
            }

            $this->addFlash('success', $this->translator->trans('Session license change was successful'));

            return $this->redirectToRoute(
                '_session_get',
                [
                    'id' => $participant->getSession()->getId(),
                ]
            );
        } catch (Throwable $t) {
            $this->logger->error($t);
            $this->addFlash('error', $this->translator->trans('Unable to assign this presenter as a license'));

            return $this->redirectToRoute('_session_get', ['id' => $participant->getSession()->getId()]);
        }
    }

    #[Route('/{id}/remove', name: '_unregister_participant', requirements: ['id' => "\d+"], options: ['expose' => true], methods: 'GET')]
    #[IsGranted(ParticipantSessionRoleVoter::EDIT, subject: 'participant')]
    public function unregisterParticipant(ProviderParticipantSessionRole $participant): Response
    {
        try {
            $this->activityLogger->initActivityLogContext($participant->getSession()->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
            $participant->setOldRole($participant->getRole());
            $this->participantSessionSubscriber->unsubscribe($participant);
            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();
            $this->addFlash('success', $this->translator->trans('Unregistered participant'));

            return $this->redirectToRoute(
                '_session_get',
                [
                    'id' => $participant->getSession()->getId(),
                ]
            );
        } catch (Throwable $t) {
            $this->logger->error($t);
            $this->addFlash('error', $this->translator->trans('Unable to remove this participant from this session'));

            return $this->redirectToRoute('_session_get', ['id' => $participant->getSession()->getId()]);
        }
    }

    #[Route('/convocation/send/{id}', name: '_convocation_send', options: ['expose' => true])]
    #[IsGranted(ParticipantSessionRoleVoter::ACCESS, subject: 'participantSessionRole')]
    public function sendConvocation(Request $request, ProviderParticipantSessionRole $participantSessionRole, EventDispatcherInterface $eventDispatcher, ActivityLogger $activityLogger): Response
    {
        if ($participantSessionRole->getSession()->getCategory() == CategorySessionType::CATEGORY_SESSION_WEBINAR) {
            $eventDispatcher->dispatch(
                new TransactionalParticipantEmailRequestEvent($participantSessionRole, $this->getUser(), true),
                TransactionalEmailRequestEventInterface::COMMUNICATION_ONE_PARTICIPANT
            );
        } else {
            $eventDispatcher->dispatch(
                new TransactionalParticipantEmailRequestEvent($participantSessionRole, $this->getUser(), true),
                TransactionalEmailRequestEventInterface::CONVOCATION_ONE_PARTICIPANT
            );
        }

        $activityLogger->flushActivityLogs();

        $this->addFlash('success', $this->translator->trans('The convocation was sent to %userName% for the session %session%', [
            '%userName%' => $participantSessionRole->getParticipant()->getFullName(),
            '%session%' => $participantSessionRole->getSession()->getName(),
        ]));

        if ($request->headers->has('referer')) {
            return $this->redirect(
                $request->headers->get('referer')
            );
        } else {
            return $this->redirectToRoute('_session_get', ['id' => $participantSessionRole->getSession()->getId()]);
        }
    }

    #[Route('/convocation/show/{token}', name: '_trainee_convocation_show', options: ['expose' => true])]
    public function showTraineeConvocation(string $token, ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository): Response
    {
        /** @var ProviderParticipantSessionRole $providerParticipantSessionRole */
        $providerParticipantSessionRole = $providerParticipantSessionRoleRepository->findOneBy(['providerSessionAccessToken.token' => $token]);

        if ($providerParticipantSessionRole->getSession()->getCategory() == CategorySessionType::CATEGORY_SESSION_WEBINAR) {
            $convocation = $providerParticipantSessionRole->getSession()->getWebinarConvocation()->first();
        } else {
            $convocation = $providerParticipantSessionRole->getSession()->getConvocation();
        }

        $content = $this->convocationService->transformShortsCodes($convocation->getContent(), $providerParticipantSessionRole);

        return $this->render('emails/participant/convocation.html.twig', [
            'content' => $content,
            'client' => $providerParticipantSessionRole->getSession()->getClient()->getName(),
            'clientService' => $providerParticipantSessionRole->getSession()->getClient(),
            'session' => $providerParticipantSessionRole->getSession(),
            'isAnimator' => $providerParticipantSessionRole->isAnAnimator(),
            'email' => [
                'to' => [
                    0 => [
                        'address' => $providerParticipantSessionRole->getParticipant()->getEmail(),
                    ],
                ],
            ],
        ]);
    }

    #[Route('/convocation/preview/{id}', name: '_trainee_convocation_preview', options: ['expose' => true])]
    public function previewTraineeConvocation(?ProviderParticipantSessionRole $participantSessionRole): Response
    {
        if ($participantSessionRole->getSession()->getCategory() == CategorySessionType::CATEGORY_SESSION_WEBINAR) {
            $convocation = $participantSessionRole->getSession()->getWebinarConvocation()->first();
        } else {
            $convocation = $participantSessionRole->getSession()->getConvocation();
        }

        return $this->apiResponse([
            'content' => $this->convocationService->transformShortsCodes($convocation->getContent(), $participantSessionRole),
        ]);
    }

    #[Route('/is-present-in-session/{id}/{user}', name: '_is_present_in_session', options: ['expose' => true])]
    #[IsGranted('ROLE_TRAINEE')]
    public function isPresentInSession(ProviderSession $providerSession, User $user, ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository): Response
    {
        return new Response($providerParticipantSessionRoleRepository->findBySessionAndUser($providerSession, $user)->isPresent(), Response::HTTP_OK);
    }

    #[Route('/download-training-certificate-participant/{id}/{user}', name: '_download_training_certificate_participant', options: ['expose' => true])]
    #[IsGranted('ROLE_TRAINEE')]
    public function downloadTrainingCertificateParticipant(ProviderSession $providerSession, User $user, ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository, $exportsSavefilesDirectory): Response
    {
        $type = PdfCertificateProviderParticipantSessionRoleExporter::EXPORT_TYPE;
        $format = ExportFormat::PDF;
        $exporter = $this->exporterFactory->getExporter($type, $format);

        $response = $this->exporterService->generateTrainingCertificateParticipant($exporter, $providerParticipantSessionRoleRepository->findBySessionAndUser($providerSession, $user));
        $data = json_decode($response->getContent());

        return $this->redirectToRoute('_download_exports', [
            'token' => $data->message,
        ]);
    }

    #[Route('/get-only-participant-session-roles-by-session/{id}', name: '_get_only_participant_session_roles_by_session', options: ['expose' => true])]
    public function getOnlyParticipantSessionRolesBySession(ProviderSession $providerSession, ProviderParticipantSessionRoleRepository $participantSessionRoleRepository, SerializerInterface $serializer): JsonResponse
    {
        return $this->apiResponse($serializer->normalize($participantSessionRoleRepository->findBy(['session' => $providerSession]), null, ['groups' => 'lite_participant']));
    }

    #[Route('/get-participants-presence-and-evaluation/{id}', name: '_get_only_participant_presence_and_evaluation', options: ['expose' => true])]
    public function getParticipantPresenceAndEvaluation(Request $request, ProviderSession $providerSession, ProviderParticipantSessionRoleRepository $participantSessionRoleRepository, SerializerInterface $serializer, EvaluationParticipantAnswerRepository $answerRepository): JsonResponse
    {
        $participants = $participantSessionRoleRepository->findTrainees($providerSession);

        return $this->apiResponse([
            'total' => count($participants),
            'answer' => $answerRepository->getCountSessionEvaluation($providerSession),
            'participants' => $serializer->normalize($participants, null, ['groups' => 'lite_participant']),
        ]);
    }

    #[Route('/get-participants-evaluation-session/{id}', name: '_get_only_participant_evaluation_session', options: ['expose' => true])]
    public function getParticipantEvaluationSession(Request $request, EvaluationProviderSession $evaluationProviderSession, ProviderParticipantSessionRoleRepository $participantSessionRoleRepository, SerializerInterface $serializer, EvaluationParticipantAnswerRepository $answerRepository): JsonResponse
    {
        $participants = $participantSessionRoleRepository->findTrainees($evaluationProviderSession->getProviderSession());

        return $this->apiResponse([
            'total' => count($participants),
            'answer' => $this->evaluationSessionParticipantAnswerRepository->countAnswersByEvaluationProviderSession($evaluationProviderSession),
            'participants' => $serializer->normalize($participants, null, ['groups' => 'lite_participant']),
        ]);
    }

    #[Route('/session/{session}/who-can-receive-sms', name: '_participant_who_can_receive_sms', options: ['expose' => true])]
    #[IsGranted('ROLE_MANAGER')]
    public function getParticipantWhoCanReceiveSmsForSession(ProviderSession $session, ProviderParticipantSessionRoleService $providerParticipantSessionRoleService): Response
    {
        return $this->apiResponse($providerParticipantSessionRoleService->getCountParticipantWhoCanReceiveSmsBySession($session));
    }
}
