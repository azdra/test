<?php

namespace App\Controller;

use App\Entity\Guest;
use App\Entity\GuestUnsubscribe;
use App\Entity\WebinarConvocation;
use App\Exception\InvalidTemplateUploadedForImportException;
use App\Exception\MissingClientParameter;
use App\Exception\UploadFile\UploadFileException;
use App\Service\Exporter\ExporterService;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetStatisticsGuestsWebinarConvocationExporter;
use App\Service\ImportGuestService;
use App\Service\WebinarConvocationService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/guest')]
class GuestController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ImportGuestService $importGuestService,
        private TranslatorInterface $translator,
        private WebinarConvocationService $webinarConvocationService,
        private ExporterService $exporterService
    ) {
    }

    #[Route('/import-file-guests', name: '_guests_import_file', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function importFileGuests(Request $request, LoggerInterface $logger): Response
    {
        try {
            $this->importGuestService->importGuestsFromRequest($request);
            $this->entityManager->flush();

            $response = $this->translator->trans('The file has been imported successfully.');

            $this->addFlash('success', $response);
            $code = Response::HTTP_OK;
        } catch (UploadFileException|MissingClientParameter|InvalidTemplateUploadedForImportException $e) {
            $logger->error($e);
            $response = $e->getMessage();
            $code = Response::HTTP_BAD_REQUEST;
        } catch (Throwable $e) {
            $logger->error($e);
            $response = $this->translator->trans('The file can not be imported for an unknown error');
            $code = Response::HTTP_BAD_REQUEST;
        }

        return new Response($response, $code);
    }

    #[Route('/unsubscribe-guest/{token}', name: '_client_guest_unsubscribe', options: ['expose' => true], methods: ['GET'])]
    public function unsubscribeClientGuest(string $token, LoggerInterface $logger): Response
    {
        $response = null;
        $isUnsubscribe = false;
        try {
            $guest = $this->entityManager->getRepository(Guest::class)->findOneBy(['token' => $token]);
            if (!$guest) {
                $response = $this->translator->trans('The guest does not exist');
                throw new NotFoundHttpException('The guest does not exist');
            }
            $guestUnsubscribe = $this->entityManager->getRepository(GuestUnsubscribe::class)->findBy(['email' => $guest->getEmail(), 'client' => $guest->getClient()]);
            if ($guestUnsubscribe !== []) {
                $isUnsubscribe = true;
            }

            return $this->render('emails/participant/unsubscribe_page.html.twig', [
                'guestId' => $guest->getId(),
                'isUnsubscribe' => $isUnsubscribe,
            ]);
        } catch (Throwable $exception) {
            $logger->error($exception);
            if (!$response) {
                $response = $this->translator->trans('The guest can not be unsubscribed for an unknown error'.$exception->getMessage());
            }
            $code = Response::HTTP_BAD_REQUEST;
        }

        return new Response($response, $code);
    }

    #[Route('/unsubscribe-guest-valid/{id}', name: '_guest_unsubscribe', options: ['expose' => true], methods: ['POST'])]
    public function unsubscribeGuest(Guest $guest, LoggerInterface $logger): Response
    {
        $response = null;
        try {
            $guestUnsubscribe = $this->entityManager->getRepository(GuestUnsubscribe::class)->findBy(['email' => $guest->getEmail(), 'client' => $guest->getClient()]);
            if ($guestUnsubscribe === []) {
                $this->webinarConvocationService->unsubscribeEmailGuest($guest);
            }
            $this->entityManager->flush();
            $response = $this->translator->trans('The guest has been unsubscribed successfully.');

            $this->addFlash('success', $response);
            $code = Response::HTTP_OK;
        } catch (Throwable $exception) {
            $logger->error($exception);
            if (!$response) {
                $response = $this->translator->trans('The guest can not be unsubscribed for an unknown error'.$exception->getMessage());
            }
            $code = Response::HTTP_BAD_REQUEST;
        }

        return new Response($response, $code);
    }

    #[Route('/export-file-guests', name: '_guests_export_file', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_MANAGER')]
    public function exportFileGuests(Request $request, LoggerInterface $logger): Response
    {
        try {
            $webinarConvocation = $this->entityManager->getRepository(WebinarConvocation::class)->findOneBy(['id' => $request->query->get('webinarConvocationId')]);

            /** @var JsonResponse $response */
            $response = $this->exporterService->generateArchive(
                AbstractSpreadsheetStatisticsGuestsWebinarConvocationExporter::EXPORT_TYPE,
                ExportFormat::XLSX,
                ['webinarConvocation' => $webinarConvocation]
            );

            $data = json_decode($response->getContent());

            if ($response->getStatusCode() !== Response::HTTP_OK) {
                $this->addFlash('error', $data->message->text);
                $logger->error(new \Exception($data->message->data->exception), [
                    'type' => AbstractSpreadsheetStatisticsGuestsWebinarConvocationExporter::EXPORT_TYPE,
                    'format' => ExportFormat::XLSX,
                    'webinarConvocationId' => $webinarConvocation->getId(),
                ]);

                return $this->redirectToRoute('_session_get', ['id' => $webinarConvocation->getSession()->getId(), 'tabName' => 'SessionCommunicationTab']);
            }

            return $this->redirectToRoute('_download_exports', [
                'token' => $data->message,
            ]);
        } catch (Throwable $exception) {
            $logger->error($exception);
            $response = $this->translator->trans('The file can not be exported for an unknown error => '.$exception->getMessage());
            $code = Response::HTTP_BAD_REQUEST;
        }

        return new Response($response, $code);
    }
}
