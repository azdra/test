<?php

namespace App\Controller;

use App\Handler\WebhookHandlerInterface;
use App\Handler\WebhookMandrillInvitationsHandlerInterface;
use App\Security\WebhookAuthenticationInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/webhook')]
class WebhookController extends AbstractController
{
    public function __construct(
        private WebhookHandlerInterface $webhookHandler,
        private WebhookMandrillInvitationsHandlerInterface $webhookMandrillInvitationsHandler,
        private ?WebhookAuthenticationInterface $webhookAuthentication = null
    ) {
    }

    #[Route('/handle', name: '_webhook_mandrill_handle', methods: ['POST', 'GET', 'HEAD'])]
    public function handle(Request $request): Response
    {
        // Mandrill will do a quick check that the URL exists by using a HEAD request (not POST).
        // Note that Symfony silently transforms HEAD requests to GET.
        if (in_array($request->getMethod(), ['GET', 'HEAD'])) {
            return new Response();
        }

        if (!$this->isAuthenticated($request)) {
            throw new AccessDeniedHttpException();
        }

        $this->webhookHandler->handleRequest($request);

        return new Response();
    }

    #[Route('/mandrill-invitations-handle', name: '_webhook_mandrill_invitations_handle', methods: ['POST', 'GET', 'HEAD'])]
    public function mandrillInvitationsHandle(Request $request): Response
    {
        //dd($request);
        // Mandrill will do a quick check that the URL exists by using a HEAD request (not POST).
        // Note that Symfony silently transforms HEAD requests to GET.
        if (in_array($request->getMethod(), ['GET', 'HEAD'])) {
            return new Response();
        }

        if (!$this->isAuthenticated($request)) {
            throw new AccessDeniedHttpException();
        }

        $this->webhookMandrillInvitationsHandler->handleRequest($request);

        return new Response();
    }

    private function isAuthenticated(Request $request): bool
    {
        if (!$this->webhookAuthentication) {
            return true;
        }

        return $this->webhookAuthentication->isAuthenticated($request);
    }
}
