<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface WebhookAuthenticationInterface.
 */
interface WebhookAuthenticationInterface
{
    /**
     * Verifying the request signatures.
     */
    public function isAuthenticated(Request $request): bool;
}
