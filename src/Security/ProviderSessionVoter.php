<?php

namespace App\Security;

use App\Entity\ProviderSession;
use App\Entity\User;
use App\Repository\ProviderParticipantSessionRoleRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class ProviderSessionVoter extends Voter
{
    public const EDIT = 'edit';
    public const ACCESS = 'access';
    public const SEND_CONVOCATION = 'send_convocation';
    public const UPDATE_PARTICIPANT = 'update_participant';
    public const DELETE = 'delete';
    public const CANCEL = 'cancel';
    public const VALIDATE = 'validate';

    public function __construct(private Security $security, private ProviderParticipantSessionRoleRepository $sessionRoleRepository)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::ACCESS, self::EDIT, self::CANCEL, self::DELETE, self::VALIDATE, self::SEND_CONVOCATION, self::UPDATE_PARTICIPANT])) {
            return false;
        }

        if (!$subject instanceof ProviderSession) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $loggedUser = $token->getUser();

        if (!$loggedUser instanceof User) {
            return false;
        }

        /** @var ProviderSession $subject */
        if ($attribute === self::CANCEL && $subject->isADraft()) {
            return false;
        }

        // An admin can do everything
        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            return true;
        }

        if (in_array($attribute, [self::UPDATE_PARTICIPANT, self::SEND_CONVOCATION]) && $subject->isFinished()) {
            return false;
        }

        if (in_array($attribute, [self::DELETE, self::CANCEL, self::EDIT, self::VALIDATE]) && ($subject->isFinished() || $subject->isRunning())) {
            return false;
        }

        // As a manager you must be on the same platform
        if (
            $this->security->isGranted(User::ROLE_MANAGER)
            && $subject->getClient()->getId() === $loggedUser->getClient()->getId()
        ) {
            return true;
        }

        /* @var ProviderSession $subject */
        return match ($attribute) {
            self::ACCESS => $this->sessionRoleRepository->isAnimator($subject, $loggedUser) || $this->security->isGranted(User::ROLE_HOTLINE_CUSTOMER_SERVICE),
            self::SEND_CONVOCATION, self::UPDATE_PARTICIPANT => $this->security->isGranted(User::ROLE_HOTLINE_CUSTOMER_SERVICE),
            default => false
        };
    }
}
