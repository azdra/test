<?php

namespace App\Security;

use App\Entity\ProviderParticipant;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class ProviderParticipantVoter extends Voter
{
    public const EDIT = 'edit';

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if ($attribute != self::EDIT) {
            return false;
        }

        if (!$subject instanceof ProviderParticipant) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $loggedUser = $token->getUser();

        if (!$loggedUser instanceof User) {
            return false;
        }

        // An admin can do everything
        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            return true;
        }

        // You must be manager to do something
        if (!$this->security->isGranted(User::ROLE_MANAGER)) {
            return false;
        }

        // As a manager you must be on the same platform
        /* @var ProviderParticipant $subject */
        return match ($attribute) {
            self::EDIT => $subject->getClient()->getId() === $loggedUser->getClient()->getId(),
            default => false
        };
    }
}
