<?php

namespace App\Security;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class SwitchUserVoter extends Voter
{
    public const CAN_SWITCH_USER = 'CAN_SWITCH_USER';

    public function __construct(
        private Security $security,
        private UserRepository $userRepository
    ) {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        return $attribute === self::CAN_SWITCH_USER
            && $subject instanceof UserInterface;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();

        // if the user is anonymous or if the subject is not a user, do not grant access
        if (!$user instanceof UserInterface || !$subject instanceof UserInterface) {
            return false;
        }

        if ($this->security->isGranted(User::ROLE_MANAGER)) {
            $usersAvailable = $this->userRepository->findBy([
                'email' => $user->getEmail(),
            ]);

            if (in_array($subject, $usersAvailable)) {
                return true;
            }
        }

        return false;
    }
}
