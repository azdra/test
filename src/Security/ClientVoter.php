<?php

namespace App\Security;

use App\Entity\Client\Client;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class ClientVoter extends Voter
{
    public const EDIT = 'edit';
    public const ACCESS = 'access';

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::EDIT, self::ACCESS])) {
            return false;
        }

        if (!$subject instanceof Client) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $loggedUser = $token->getUser();

        if (!$loggedUser instanceof User) {
            return false;
        }

        // An admin can do everything
        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            return true;
        }

        // You must be manager to do something
        if (!$this->security->isGranted(User::ROLE_MANAGER)) {
            return false;
        }

        /* @var Client $subject */
        return match ($attribute) {
            // As a manager you must be on the same platform
            self::ACCESS => $subject->getId() === $loggedUser->getClient()->getId(),
            // As a manager you CAN NOT edit any client, so default case
            default => false
        };
    }
}
