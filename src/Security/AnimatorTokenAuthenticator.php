<?php

namespace App\Security;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Repository\ProviderParticipantSessionRoleRepository;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class AnimatorTokenAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    public const ERROR_LOGIN_ROUTE = '_app_login';
    public const SUPPORTED_ROUTE = '_session_get';

    public function __construct(
        private \Redis $redisClient,
        private UrlGeneratorInterface $urlGenerator,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository
    ) {
    }

    public function supports(Request $request): bool
    {
        return self::SUPPORTED_ROUTE === $request->attributes->get('_route')
               && $request->isMethod('GET')
               && $request->query->has('token');
    }

    /**
     * @throws Exception
     */
    public function onAuthenticationSuccess(
        Request $request,
        TokenInterface $token,
        string $firewallName
    ): RedirectResponse {
        $sessionId = $this->providerParticipantSessionRoleRepository->createQueryBuilder('sessionRole')
            ->select('IDENTITY(sessionRole.session)')
            ->where('sessionRole.providerSessionAccessToken.token = :token')
            ->setParameter('token', $request->query->get('token'))
            ->getQuery()
            ->getSingleScalarResult();

        return new RedirectResponse($this->urlGenerator->generate('_session_get', ['id' => $sessionId]));
    }

    public function authenticate(Request $request): Passport
    {
        $userToken = $request->query->get('token');

        return new Passport(
            new UserBadge($userToken, function ($userIdentifier) {
                /** @var ProviderParticipantSessionRole $sessionRole */
                $sessionRole = $this->providerParticipantSessionRoleRepository
                    ->findOneBy([
                        'providerSessionAccessToken.token' => $userIdentifier,
                    ]);

                if (!$sessionRole || !$sessionRole->isAnAnimator()) {
                    throw new UserNotFoundException('Token could not be found.');
                }

                $user = $sessionRole->getParticipant()->getUser();

                $redisKey = User::REDIS_USER_ACCOUNTS_AVAILABLE_KEY.$user->getEmail();

                if ($this->redisClient->exists($redisKey)) {
                    $this->redisClient->del($redisKey);
                }

                return $user;
            }),
            new CustomCredentials(
                function ($credentials, $user) {
                    return null !== $this->providerParticipantSessionRoleRepository->createQueryBuilder('session_role')
                        ->join('session_role.participant', 'participant')
                        ->where('session_role.providerSessionAccessToken.token = :token')
                        ->andWhere('participant.user = :user')
                        ->setParameter('token', $credentials)
                        ->setParameter('user', $user)
                        ->getQuery()
                        ->getOneOrNullResult();
                }, $userToken),
            [
                new RememberMeBadge(),
            ]
        );
    }

    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate(self::ERROR_LOGIN_ROUTE);
    }
}
