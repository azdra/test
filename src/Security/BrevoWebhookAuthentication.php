<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class BrevoWebhookAuthentication.
 *
 * @see https://mandrill.zendesk.com/hc/en-us/articles/205583257-How-to-Authenticate-Webhook-Requests
 */
class BrevoWebhookAuthentication implements WebhookAuthenticationInterface
{
    public function __construct(
        //private string $webhookIDFuseKey,
        //private string $webhookIDFuseUrl
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function isAuthenticated(Request $request): bool
    {
        /*$requestSignature = $this->getRequestSignature($request->request->all());

        if ($requestSignature === $request->headers->get('X-Mandrill-Signature')) {
            return true;
        }

        return false;*/
        return true;
    }

    /**
     * Generates a base64-encoded signature for a Mandrill web hook request.
     *
     * @param array $webHookRequestData The request's POST parameters
     */
    private function getRequestSignature(array $webHookRequestData = []): string
    {
        /*$signedData = $this->webhookIDFuseUrl;

        ksort($webHookRequestData);

        foreach ($webHookRequestData as $key => $value) {
            $signedData .= $key;
            $signedData .= $value;
        }

        return base64_encode(hash_hmac('sha1', $signedData, $this->webhookIDFuseKey, true));*/
        return '';
    }
}
