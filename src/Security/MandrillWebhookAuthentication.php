<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class MandrillWebhookAuthentication.
 *
 * @see https://mandrill.zendesk.com/hc/en-us/articles/205583257-How-to-Authenticate-Webhook-Requests
 */
class MandrillWebhookAuthentication implements WebhookAuthenticationInterface
{
    public function __construct(
        private string $webhookKey,
        private string $webhookUrl
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function isAuthenticated(Request $request): bool
    {
        $requestSignature = $this->getRequestSignature($request->request->all());

        if ($requestSignature === $request->headers->get('X-Mandrill-Signature')) {
            return true;
        }

        return false;
    }

    /**
     * Generates a base64-encoded signature for a Mandrill web hook request.
     *
     * @param array $webHookRequestData The request's POST parameters
     */
    private function getRequestSignature(array $webHookRequestData = []): string
    {
        $signedData = $this->webhookUrl;

        ksort($webHookRequestData);

        foreach ($webHookRequestData as $key => $value) {
            $signedData .= $key;
            $signedData .= $value;
        }

        return base64_encode(hash_hmac('sha1', $signedData, $this->webhookKey, true));
    }
}
