<?php

namespace App\Security;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Repository\ProviderParticipantSessionRoleRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class ParticipantSessionRoleVoter extends Voter
{
    public const EDIT = 'edit';
    public const ACCESS = 'access';
    public const ACCESS_CONVOCATION = 'access_convocation';

    public function __construct(private Security $security, private ProviderParticipantSessionRoleRepository $sessionRoleRepository)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::EDIT, self::ACCESS])
            && $subject instanceof ProviderParticipantSessionRole;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $userToken = $token->getUser();

        if (!$userToken instanceof User) {
            return false;
        }

        if ($this->security->isGranted(User::ROLE_HOTLINE_CUSTOMER_SERVICE)) {
            return true;
        }

        if (
            $this->security->isGranted(User::ROLE_MANAGER) &&
            $userToken->getClient()->getId() === $subject->getParticipant()->getUser()->getClient()->getId()
        ) {
            return true;
        }

        return match ($attribute) {
            self::ACCESS => $this->sessionRoleRepository->isAnimator($subject->getSession(), $userToken),
            self::ACCESS_CONVOCATION => $this->sessionRoleRepository->isAnimator($subject->getSession(), $userToken) || $this->sessionRoleRepository->isTrainee($subject->getSession(), $userToken),
            default => false
        };
    }
}
