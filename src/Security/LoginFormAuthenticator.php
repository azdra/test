<?php

namespace App\Security;

use App\Entity\User;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserModuleRepository;
use App\Repository\UserRepository;
use App\SelfSubscription\Repository\ModuleRepository;
use App\SelfSubscription\Service\ModuleService;
use App\SelfSubscription\Service\SubjectService;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\CustomCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormAuthenticator extends AbstractLoginFormAuthenticator implements AuthenticationEntryPointInterface
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = '_app_login';
    public const DEFAULT_ROUTE = '_home';

    public function __construct(
        private \Redis $redisClient,
        private UrlGeneratorInterface $urlGenerator,
        private UserRepository $userRepository,
        private UserModuleRepository $userModuleRepository,
        private UserPasswordHasherInterface $passwordHasher,
        private ModuleRepository $moduleRepository,
        private SubjectService $subjectService,
        private ModuleService $moduleService,
        private ProviderSessionRepository $providerSessionRepository
    ) {
    }

    public function supports(Request $request): bool
    {
        return self::LOGIN_ROUTE === $request->attributes->get('_route')
               && $request->isMethod('POST');
    }

    public function getPassword($credentials): ?string
    {
        return $credentials['password'];
    }

    /**
     * @throws Exception
     */
    public function onAuthenticationSuccess(
        Request $request,
        TokenInterface $token,
        string $firewallName
    ): RedirectResponse {
        if ($request->request->has('domain')) {
            $userModuleFound = $this->userModuleRepository
                ->findOneBy([
                    'user' => $token->getUser(),
                    'module' => $this->moduleRepository->findOneBy(['domain' => $request->request->get('domain')]),
                ]);
            if ($userModuleFound) {
                $this->moduleService->updateLastAuthenticationAt($userModuleFound);
            }
            $initialRoute = $this->urlGenerator->generate('_self_subscription_hub_home', ['clientName' => $request->request->get('slugclient'), 'domain' => $request->request->get('domain')]);
            if ($request->request->has('sessionId')) {
                $this->subjectService->subscribeUserFromAuthentification($token->getUser(), $this->providerSessionRepository->findOneBy(['id' => $request->request->get('sessionId')]));
            } elseif ($request->request->has('replay')) {
                $initialRoute = $this->urlGenerator->generate('_self_subscription_hub_home', ['clientName' => $request->request->get('slugclient'), 'domain' => $request->request->get('domain'), 'page' => 'Replay']);
            }
        } else {
            $user = $token->getUser();
            $urlName = $user instanceof User ? $user->getPreferredInitialRoute() : self::DEFAULT_ROUTE;
            $initialRoute = $this->urlGenerator->generate($urlName);
        }

        return new RedirectResponse($initialRoute);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        if ($request->hasSession()) {
            $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
        }

        if ($request->request->has('domain')) {
            if ($request->request->has('sessionId')) {
                return new RedirectResponse(
                    $this->urlGenerator->generate('_self_subscription_hub_home', ['clientName' => $request->request->get('slugclient'), 'domain' => $request->request->get('domain'), 'errorAuthentication' => true])
                );
            } elseif ($request->request->has('replay')) {
                return new RedirectResponse(
                    $this->urlGenerator->generate('_self_subscription_hub_home', ['clientName' => $request->request->get('slugclient'), 'domain' => $request->request->get('domain'), 'errorAuthentication' => true, 'page' => 'Replay'])
                );
            }

            return new RedirectResponse(
                $this->urlGenerator->generate('_self_subscription_hub_login', ['clientName' => $request->request->get('slugclient'), 'domain' => $request->request->get('domain')])
            );
        }

        $url = $this->getLoginUrl($request);

        return new RedirectResponse($url);
    }

    public function authenticate(Request $request): Passport
    {
        $email = $request->request->get('username');
        $password = $request->request->get('password');
        $csrfToken = $request->request->get('csrf_token');

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $email
        );

        if ($request->request->has('domain')) {
            $userFound = $this->userRepository
                ->findOneBy([
                                'email' => $email,
                                'status' => User::STATUS_ACTIVE,
                                'client' => ($this->moduleRepository->findOneBy(['domain' => $request->request->get('domain')]))->getClient(),
                            ]);
            $userModuleFound = $this->userModuleRepository
                ->findOneBy([
                                'user' => $userFound,
                                'module' => $this->moduleRepository->findOneBy(['domain' => $request->request->get('domain')]),
                            ]);
            if ($userModuleFound == null) {
                throw new UserNotFoundException('You are not registered to this domain.');
            }

            if (!password_verify($password, $userModuleFound->getPassword())) {
                throw new AuthenticationException('Invalid password for module.');
            } else {
                return new Passport(
                    new UserBadge($email, function ($userIdentifier) use ($userFound) {
                        $redisKey = User::REDIS_USER_ACCOUNTS_AVAILABLE_KEY.$userFound->getEmail();

                        if ($this->redisClient->exists($redisKey)) {
                            $this->redisClient->del($redisKey);
                        }

                        return $userFound;
                    }),
                    new CustomCredentials(function () { return true; }, $userFound), // Ceci suppose une implémentation qui ignore la vérification
                    [
                        new CsrfTokenBadge('authenticate', $csrfToken),
                        new RememberMeBadge(),
                    ]
                );
            }
        } else {
            $userFound = $this->getUserWithTopRole($this->userRepository->findBy(['email' => $email, 'status' => User::STATUS_ACTIVE]));
        }

        return new Passport(
            new UserBadge($email, function ($userIdentifier) use ($userFound) {
                if ($userFound != null) {
                    $user = $userFound;
                } else {
                    $user = $this->userRepository
                        ->findOneBy([
                            'email' => $userIdentifier,
                            'status' => User::STATUS_ACTIVE,
                        ]);
                }

                if (!$user) {
                    $user = $this->userRepository
                        ->findOneBy([
                            'email' => $userIdentifier,
                        ]);

                    if (!$user) {
                        throw new UserNotFoundException('Email could not be found.');
                    }
                }

                $redisKey = User::REDIS_USER_ACCOUNTS_AVAILABLE_KEY.$user->getEmail();

                if ($this->redisClient->exists($redisKey)) {
                    $this->redisClient->del($redisKey);
                }

                return $user;
            }),
            new PasswordCredentials($password),
            [
                new CsrfTokenBadge('authenticate', $csrfToken),
                new RememberMeBadge(),
            ]
        );
    }

    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }

    public function getUserWithTopRole(array $users): ?User
    {
        $userManagerRole = null;
        $userCustomerServiceRole = null;

        if (count($users) == 0) {
            return null;
        }

        $userTraineeRole = $users[0];
        if (count($users) > 1) {
            foreach ($users as $user) {
                if ($user->isAnAdmin()) {
                    return $user;
                } elseif ($userManagerRole == null && in_array(User::ROLE_MANAGER, $user->getRoles())) {
                    $userManagerRole = $user;
                } elseif ($userCustomerServiceRole == null && in_array(User::ROLE_CUSTOMER_SERVICE, $user->getRoles())) {
                    $userCustomerServiceRole = $user;
                } elseif ($userTraineeRole == $users[0]) {
                    $userTraineeRole = $user;
                }
            }
            if ($userManagerRole != null) {
                return $userManagerRole;
            } elseif ($userCustomerServiceRole != null) {
                return $userCustomerServiceRole;
            } else {
                return $userTraineeRole;
            }
        }

        return $users[0];
    }
}
