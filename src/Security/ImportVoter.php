<?php

namespace App\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ImportVoter extends Voter
{
    public const CAN_SELECT_CLIENT = 'can-select-client';

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::CAN_SELECT_CLIENT])) {
            return false;
        }

        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $loggedUser = $token->getUser();

        if (!$loggedUser instanceof User) {
            return false;
        }

        $user = $subject;

        switch ($attribute) {
            case self::CAN_SELECT_CLIENT:
                return $this->canSelectClient($user, $loggedUser);
        }

        return false;
    }

    private function canSelectClient(User $user, UserInterface $loggedUser): bool
    {
        if ($this->security->isGranted(User::ROLE_HOTLINE_CUSTOMER_SERVICE)) {
            return true;
        }

        return false;
    }
}
