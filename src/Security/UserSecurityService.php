<?php

namespace App\Security;

use App\Entity\User;
use App\Event\UserPasswordEvent;
use App\Model\GeneratorTrait;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class UserSecurityService
{
    use GeneratorTrait;

    public function __construct(
        private EventDispatcherInterface $dispatcher,
        private UserRepository $userRepository,
        private EntityManagerInterface $entityManager
    ) {
    }

    public function checkLastPasswordReset(User $user): bool
    {
        $lastPasswordReset = $user->getLastPasswordReset();
        if ($lastPasswordReset === null) {
            return true;
        }
        $now = new DateTimeImmutable();
        $diff = $now->diff($lastPasswordReset);
        // i = minutes
        if ($diff->i > 3) {
            return true;
        }

        return false;
    }

    public function requestPasswordReset(User $user, string $domain = null, string $slugClient = null): void
    {
        // Is last password reset has been made in the last 10 minutes ?
        if ($user->getLastPasswordReset() && $user->getLastPasswordReset()->add(new \DateInterval('PT2M')) > new \DateTime('now')) {
            return;
        }

        $this->setPasswordToken($user);

        $this->dispatcher->dispatch(new UserPasswordEvent($user, $domain, $slugClient), UserPasswordEvent::RESET);
    }

    public function updatePassword(User $user, string $plainPassword): void
    {
        if ($user->isAnAdmin() || $user->isAManager()) {
            $users = $this->userRepository->findAllUsersManagerAccounts($user);
        } else {
            $users = [$user];
        }

        foreach ($users as $user) {
            $user->setClearPassword($plainPassword)
                ->setPasswordResetToken(null)
                ->setLastPasswordReset(new DateTimeImmutable());
        }
    }

    public function updatePasswordManager(User $user, string $plainPassword): void
    {
        $users = $this->userRepository->findAllUsersManagerAccounts($user);
        foreach ($users as $user) {
            $user->setClearPassword($plainPassword)
                ->setPasswordResetToken(null)
                ->setLastPasswordReset(new DateTimeImmutable());
        }
    }

    public function randomPassword(int $length = 15): string
    {
        return $this->randomString($length);
    }

    public function createOrClonePassword(User $user): bool
    {
        $existingUsers = $this->userRepository->findBy(
            ['email' => $user->getEmail()],
            ['id' => 'ASC']
        );

        if (!empty($existingUsers)) {
            /** @var User $existingUser */
            $existingUser = $existingUsers[array_key_first($existingUsers)];

            $user->setPassword(
                $existingUser->getPassword()
            )->setClearPassword(null);

            // In case of cloning a password (so link an existing user to another account.
            // The first existing link between user and client is now the default "user/client"
            $existingUser->setStatus(User::STATUS_ACTIVE);

            return true;
        } else {
            $this->setPasswordToken($user);
        }

        return false;
    }

    public function setPasswordToken(User $user): void
    {
        $user->setPasswordResetToken(\md5(\random_bytes(32)))
            ->setLastPasswordReset(new DateTimeImmutable());
    }
}
