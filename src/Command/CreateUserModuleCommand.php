<?php

namespace App\Command;

use App\Entity\UserModule;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserModuleRepository;
use App\SelfSubscription\Repository\ModuleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateUserModuleCommand extends Command
{
    protected static $defaultName = 'service:user:create-user-module';

    public function __construct(
        private ProviderSessionRepository $sessionRepository,
        private ModuleRepository $moduleRepository,
        private UserModuleRepository $userModuleRepository,
        private EntityManagerInterface $entityManager,
        private LoggerInterface $cronLogger
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('session', 's', InputOption::VALUE_OPTIONAL, 'Session ID');
        $this->addOption('module', 'm', InputOption::VALUE_OPTIONAL, 'Module ID');
        $this->addOption('role', 'r', InputOption::VALUE_OPTIONAL, 'Participant Or Animator');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $sessions = [];
        $sessionId = $input->getOption('session');
        $moduleId = $input->getOption('module');
        $role = $input->getOption('role');
        $module = $this->moduleRepository->findOneBy(['id' => $moduleId]);
        if ($module === null) {
            $io->error('Module not found');

            return Command::FAILURE;
        }

        if (empty($sessionId)) {
            $sessions = $this->sessionRepository->getAllSessionOfModule($module);
        } else {
            $sessions[] = $this->sessionRepository->find($sessionId);
        }

        $io->info(count($sessions).' sessions to check');
        foreach ($sessions as $session) {
            try {
                $io->info('check session '.$session->getName());
                $participants = $role === 'p' ? $session->getParticipantsOnly() : $session->getAnimatorsOnly();
                foreach ($participants as $trainee) {
                    $user = $trainee->getParticipant()->getUser();
                    $userModule = $this->userModuleRepository->findOneBy(['user' => $user, 'module' => $module]);
                    if ($userModule === null) {
                        $userModule = (new UserModule($user, $module))
                            ->setCreatedAt(new \DateTime())
                            ->setLastAuthenticationAt(new \DateTime())
                            ->setPassword($user->getPassword());
                        $this->entityManager->persist($userModule);
                    }
                }
            } catch (\Throwable $t) {
                $this->cronLogger->error($t, ['session_id' => $session->getId()]);
                $io->error('Error syncing session : '.$t->getMessage());
            }
            $this->entityManager->flush();
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
