<?php

namespace App\Command;

use App\Repository\ActivityLogRepository;
use App\Repository\ProviderSessionRepository;
use App\Service\ActivityLogService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class CheckPurgeActivityLogCommand extends Command
{
    protected static $defaultName = 'service:check:purge:activity-log';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $cronLogger,
        private ProviderSessionRepository $providerSessionRepository,
        private ActivityLogRepository $activityLogRepository,
        private ActivityLogService $activityLogService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('target', InputArgument::REQUIRED, 'Target >> Action : session | client | user');
        $this->addArgument('nbrdays', InputArgument::REQUIRED, 'Nbrdays >> SELECT FROM date - nbrdays');
        $this->addArgument('plusdays', InputArgument::OPTIONAL, 'plusdays >> SÉLECTIONNER À PARTIR DE la date - nbrdays');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("\t> ------Début du traitement-------------");
        $executionStartTime = microtime(true);
        try {
            switch ($input->getArgument('target')) {
                case 'session':
                    $return = $this->activityLogService->cleanActivityLogSession($output, $input->getArgument('nbrdays'));
                    $output->writeln("\t >> Clean Activity Log Session : ".$return.' treated !');
                    break;
                case 'client':
                    $return = $this->activityLogService->cleanActivityLogClient($output, $input->getArgument('nbrdays'));
                    $output->writeln("\t >> Clean Activity Log Client : ".$return.' treated !');
                    break;
                case 'user':
                    $return = $this->activityLogService->cleanActivityLogUser($output, $input->getArgument('nbrdays'));
                    $output->writeln("\t >> Clean Activity Log User : ".$return.' treated !');
                    break;
                case 'all':
                    $return = $this->activityLogService->cleanActivityLogAll($output, $input->getArgument('nbrdays'));
                    $output->writeln("\t >> Clean Activity Log All : ".$return.' treated !');
                    break;
                case 'allWithSession':
                    $return = $this->activityLogService->cleanActivityLogAllWithSession($output, $input->getArgument('nbrdays'), $input->getArgument('plusdays'));
                    $output->writeln("\t >> Clean Activity Log All : ".$return.' treated !');
                    break;
            }

            $output->writeln("\t >> All done");
            $executionEndTime = microtime(true);
            $seconds = $executionEndTime - $executionStartTime;
            $output->writeln("\t> ------Durée du traitement-------------".round($seconds, 3).' secs---|---------------------');

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->cronLogger->error($exception);
            $output->writeln('Unknown error: '.$exception->getMessage());
            $executionEndTime = microtime(true);
            $seconds = $executionEndTime - $executionStartTime;

            return Command::FAILURE;
        }
    }
}
