<?php

namespace App\Command;

use App\Entity\ActivityLog;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ClientRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\AdobeConnectService;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

class AlertNoAnimatorWithHostOnSessionCommand extends Command
{
    protected static $defaultName = 'alert:no-animator-with-host-on-session';

    private ClientRepository $clientRepository;

    private ProviderSessionRepository $providerSessionRepository;

    private UserRepository $userRepository;

    private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository;

    private EntityManagerInterface $entityManager;

    private EventDispatcherInterface $eventDispatcher;

    private ActivityLogger $activityLogger;

    private AdobeConnectService $adobeConnectService;

    private MailerService $mailerService;

    private LoggerInterface $logger;

    private TranslatorInterface $translator;

    private RouterInterface $router;

    public function __construct(
        ClientRepository $clientRepository,
        ProviderSessionRepository $providerSessionRepository,
        UserRepository $userRepository,
        ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        ActivityLogger $activityLogger,
        AdobeConnectService $adobeConnectService,
        MailerService $mailerService,
        LoggerInterface $cronLogger,
        TranslatorInterface $translator,
        RouterInterface $router
    ) {
        parent::__construct();

        $this->clientRepository = $clientRepository;
        $this->providerSessionRepository = $providerSessionRepository;
        $this->userRepository = $userRepository;
        $this->providerParticipantSessionRoleRepository = $providerParticipantSessionRoleRepository;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->activityLogger = $activityLogger;
        $this->logger = $cronLogger;
        $this->adobeConnectService = $adobeConnectService;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->router = $router;
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $from = ( new \DateTime() )->modify('+1 day')->setTime(0, 0);
        $to = ( new \DateTime() )->modify('+7 day')->setTime(23, 59);
        $dDay = ( new \DateTime() );

        try {
            $clients = $this->clientRepository->findAll();

            foreach ($clients as $client) {
                if ($client->isSubscribeToAlert(Client::NOTIFICATION_NO_ANIMATOR_WITH_HOST_ON_SESSION) && (!empty($client->getConfigurationHolders())) && ($client->getConfigurationHolders()->first() instanceof AdobeConnectConfigurationHolder) && ($client->getConfigurationHolders()->first()->getLicenceType() == AdobeConnectConfigurationHolder::LICENCES_CLIENT_NAMED)) {
                    $minusDays = $client->getOptionFromAlert(Client::NOTIFICATION_NO_ANIMATOR_WITH_HOST_ON_SESSION_OPTION_TIME_BEFORE);

                    $sessions = $this->providerSessionRepository->findClientSessionForPeriod($client, $from, $to);

                    foreach ($sessions as $session) {
                        $targetDay = (clone $session->getDateStart())->modify('-'.$minusDays.' days')->setTime(0, 0);
                        if ($targetDay < $session->getDateStart() && $dDay < $session->getDateStart() && $dDay > $targetDay && $session instanceof AdobeConnectSCO) {
                            $hostGroup = $this->adobeConnectService->getPrincipalInHostGroup($client->getConfigurationHolders()->first());
                            $animators = $session->getAnimatorsOnly();

                            $oneAnimatorIsHost = false;
                            foreach ($animators as $animator) {
                                $animatorIsHost = $this->isHostUser($hostGroup->getData(), $animator->getEmail());
                                if ($animatorIsHost) {
                                    $oneAnimatorIsHost = true;
                                }
                            }

                            if (!$oneAnimatorIsHost) {
                                $infos = [
                                    'message' => ['key' => 'There is no animator with the host privilege planned on the session <a href="%routeSession%">%session_name%</a> on %dateSession%', 'params' => ['%routeSession%' => $this->router->generate('_session_get', ['id' => $session->getId()]), '%session_name%' => $session->getName(), '%dateSession%' => $session->getDateStart()->format('d/m/Y H:i')]],
                                    'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                                ];
                                $this->activityLogger->initActivityLogContext($client, null, ActivityLog::ORIGIN_CRON);
                                $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_NO_ANIMATOR_WITH_HOST_ON_SESSION, ActivityLog::SEVERITY_WARNING, $infos), true);
                                $this->activityLogger->flushActivityLogs();

                                $this->sendEmailToUsersManager($session);
                            }
                        }
                    }
                }
            }

            $io->success('All done !');

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->logger->error($exception, ['from' => $from, 'to' => $to, 'dDay' => $dDay]);
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }
    }

    public function isHostUser(array $hostGroup, string $animatorEmail): bool
    {
        foreach ($hostGroup as $host) {
            if ($host->getEmail() == $animatorEmail) {
                return true;
            }
        }

        return false;
    }

    protected function sendEmailToUsersManager(ProviderSession $session): void
    {
        $userManagers = $this->userRepository->findManagerListByClient($session->getClient());
        foreach ($userManagers as $user) {
            if ($user->isSubscribeToAlert(User::EMAIL_NO_ANIMATOR_WITH_HOST_ON_SESSION)) {
                $sendGo = false;
                if ($user->isSubscribeToAlert(User::EMAIL_ONLY_FOR_MY_SESSIONS)) {
                    if ((!empty($session->getCreatedBy()) && $session->getCreatedBy() == $user) || (!empty($session->getUpdatedBy()) && $session->getUpdatedBy() == $user)) {
                        $sendGo = true;
                    }
                } else {
                    $sendGo = true;
                }
                if ($sendGo) {
                    $this->mailerService->sendTemplatedMail(
                        $user->getEmail(),
                        $this->translator->trans('There is no animator with the host privilege'),
                        'emails/alert/no_animator_with_host_on_session.html.twig',
                        context: [
                            'session' => $session,
                            'route' => $this->router->generate('_session_get', ['id' => $session->getId()], 0),
                            'user' => $user,
                            'client' => $session->getClient(),
                        ],
                        metadatas: [
                            'origin' => MailerService::ORIGIN_EMAIL_NO_ANIMATOR_WITH_HOST_ON_SESSION,
                            'user' => $user->getId(),
                            'session' => $session->getId(),
                        ],
                        senderSystem: true
                    );
                }
            }
        }
    }
}
