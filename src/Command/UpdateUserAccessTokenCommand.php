<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class UpdateUserAccessTokenCommand extends Command
{
    protected static $defaultName = 'user:token:regenerate';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $cronLogger,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $users = $this->entityManager->getRepository(User::class)->findAll();

            /** @var User $user */
            foreach ($users as $user) {
                $user->generateToken();
            }

            $this->entityManager->flush();
            $output->writeln("\t >> All token successfully regenerated !");

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->cronLogger->error($exception);
            $output->writeln('Unknown error: '.$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
