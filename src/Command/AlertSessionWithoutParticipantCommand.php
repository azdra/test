<?php

namespace App\Command;

use App\Entity\ActivityLog;
use App\Entity\Client\Client;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ClientRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

class AlertSessionWithoutParticipantCommand extends Command
{
    protected static $defaultName = 'alert:session-without-participant';

    private ClientRepository $clientRepository;

    private ProviderSessionRepository $providerSessionRepository;

    private UserRepository $userRepository;

    private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository;

    private EntityManagerInterface $entityManager;

    private EventDispatcherInterface $eventDispatcher;

    private ActivityLogger $activityLogger;

    private MailerService $mailerService;

    private TranslatorInterface $translator;

    private RouterInterface $router;

    private LoggerInterface $logger;

    public function __construct(
        ClientRepository $clientRepository,
        ProviderSessionRepository $providerSessionRepository,
        UserRepository $userRepository,
        ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        EntityManagerInterface $entityManager,
        LoggerInterface $cronLogger,
        EventDispatcherInterface $eventDispatcher,
        ActivityLogger $activityLogger,
        MailerService $mailerService,
        TranslatorInterface $translator,
        RouterInterface $router
    ) {
        parent::__construct();

        $this->clientRepository = $clientRepository;
        $this->providerSessionRepository = $providerSessionRepository;
        $this->userRepository = $userRepository;
        $this->providerParticipantSessionRoleRepository = $providerParticipantSessionRoleRepository;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->activityLogger = $activityLogger;
        $this->mailerService = $mailerService;
        $this->logger = $cronLogger;
        $this->translator = $translator;
        $this->router = $router;
    }

    protected function configure(): void
    {
        $this->addArgument('role', InputArgument::REQUIRED, 'PPSR Role');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $from = ( new \DateTime() )->modify('+1 day')->setTime(0, 0);
        $to = ( new \DateTime() )->modify('+200 day')->setTime(23, 59);
        $role = $input->getArgument('role');

        try {
            $clients = $this->clientRepository->findAll();
            foreach ($clients as $client) {
                if ($client->isSubscribeToAlert(($role == ProviderParticipantSessionRole::ROLE_ANIMATOR) ? Client::NOTIFICATION_SESSION_WITHOUT_ANIMATOR : Client::NOTIFICATION_SESSION_WITHOUT_TRAINEE)) {
                    $minusDays = $client->getOptionFromAlert(Client::NOTIFICATION_SESSION_WITHOUT_ANIMATOR_OPTION_TIME_BEFORE);

                    $sessions = $this->providerSessionRepository->findClientSessionForPeriod($client, $from, $to);
                    $dDay = ( new \DateTime() );
                    foreach ($sessions as $session) {
                        $targetDay = (clone $session->getDateStart())->modify('-'.$minusDays.' days')->setTime(0, 0);

                        if ($role == ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                            $participantRoles = $session->getAnimatorsOnly();
                        } else {
                            $participantRoles = $session->getTraineesOnly();
                        }

                        if ($targetDay < $session->getDateStart() && $dDay < $session->getDateStart() && $dDay > $targetDay && count($participantRoles) == 0) {
                            $this->addActivityLogForAlert($session, $role);
                            $this->sendEmailToUsersManager($session, $role);
                        }
                    }
                }
            }

            $io->success('All done !');

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->logger->error($exception, ['role' => $role, 'from' => $from->format('Y-m-d_H\hi'), 'to' => $to->format('Y-m-d_H\hi')]);
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }
    }

    protected function addActivityLogForAlert(ProviderSession $session, string $role): void
    {
        $infos = [
            'message' => ['key' => 'Session <a href="%route%">%sessionName%</a> without '.$role, 'params' => ['%route%' => $this->router->generate('_session_get', ['id' => $session->getId()]), '%sessionName%' => $session->getName()]],
            'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
        ];
        $this->activityLogger->initActivityLogContext($session->getClient(), null, ActivityLog::ORIGIN_CRON);
        $this->activityLogger->addActivityLog(new ActivityLogModel(($role == ProviderParticipantSessionRole::ROLE_ANIMATOR) ? ActivityLog::ACTION_ALERT_SESSION_WITHOUT_ANIMATOR : ActivityLog::ACTION_ALERT_SESSION_WITHOUT_TRAINEE, ActivityLog::SEVERITY_WARNING, $infos), true);
        $this->activityLogger->flushActivityLogs();
    }

    protected function sendEmailToUsersManager(ProviderSession $session, string $role): void
    {
        $userManagers = $this->userRepository->findManagerListByClient($session->getClient());
        foreach ($userManagers as $user) {
            if ($user->isSubscribeToAlert(User::EMAIL_SESSION_WITHOUT_ANIMATOR)) {
                $sendGo = false;
                if ($user->isSubscribeToAlert(User::EMAIL_ONLY_FOR_MY_SESSIONS)) {
                    if ((!empty($session->getCreatedBy()) && $session->getCreatedBy() == $user) || (!empty($session->getUpdatedBy()) && $session->getUpdatedBy() == $user)) {
                        $sendGo = true;
                    }
                } else {
                    $sendGo = true;
                }
                if ($sendGo) {
                    $this->mailerService->sendTemplatedMail(
                        $user->getEmail(),
                        $this->translator->trans('Session without '.$role),
                        'emails/alert/session_without_participant.html.twig',
                        context: [
                        'role' => $role,
                        'session' => $session,
                        'client' => $session->getClient(),
                        'route' => $this->router->generate('_session_get', ['id' => $session->getId()], 0),
                        'user' => $user,
                    ],
                        metadatas: [
                        'origin' => MailerService::ORIGIN_EMAIL_SESSION_WITHOUT_PARTICIPANT,
                        'user' => $user->getId(),
                        'session' => $session->getId(),
                    ],
                        senderSystem: true
                    );
                }
            }
        }
    }
}
