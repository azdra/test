<?php

namespace App\Command;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Service\Connector\Microsoft\MicrosoftTeamsConnector;
use App\Service\Connector\Microsoft\MicrosoftTeamsEventConnector;
use App\Service\MicrosoftTeamsEventService;
use App\Service\MicrosoftTeamsService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class CheckMicrosoftTeamsUsersValidCommand extends Command
{
    protected static $defaultName = 'service:microsoft-teams:check:users';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $cronLogger,
        private MicrosoftTeamsService $microsoftTeamsService,
        private MicrosoftTeamsEventService $microsoftTeamsEventService,
        private MicrosoftTeamsConnector $microsoftTeamsConnector,
        private MicrosoftTeamsEventConnector $microsoftTeamsEventConnector
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $microsoftTeamsClients = $this->entityManager->getRepository(Client::class)->findAll();
            foreach ($microsoftTeamsClients as $client) {
                $configurationHolder = $client->getConfigurationHolders()[0];
                if ($configurationHolder instanceof MicrosoftTeamsConfigurationHolder) {
                    $response = $this->microsoftTeamsConnector->call($configurationHolder, MicrosoftTeamsConnector::ACTION_CHECK_ORGANIZERS);
                    if ($response->isSuccess()) {
                        $this->entityManager->flush();
                    }
                } elseif ($configurationHolder instanceof MicrosoftTeamsEventConfigurationHolder) {
                    $response = $this->microsoftTeamsEventConnector->call($configurationHolder, MicrosoftTeamsEventConnector::ACTION_CHECK_ORGANIZERS);
                    if ($response->isSuccess()) {
                        $this->entityManager->flush();
                    }
                }
            }

            $output->writeln("\t >> All done");

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->cronLogger->error($exception);
            $output->writeln('Unknown error: '.$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
