<?php

namespace App\Command\Debug;

use App\Service\BounceService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class TestIDFuseBouncesDebugCommand extends Command
{
    protected static $defaultName = 'debug:test-idfuse-bounces';

    public function __construct(
        private BounceService $bounceService,
        private LoggerInterface $logger,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('type', 't', InputOption::VALUE_OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $typeOption = $input->getOption('type');
            if ($typeOption == 'api') {
                $params = [
                    'campaign_id' => '6',
                ];
                $resultBounces = $this->bounceService->callIDFuseAPIBounces($params);
                //dump($resultBounces);
                $resultUnopens = $this->bounceService->callIDFuseAPIUnopen($params);
                //dump($resultUnopens);
                $resultOpens = $this->bounceService->callIDFuseAPIOpen($params);
                //dump($resultOpens);
                $resultCampaignReportBounceList = $this->bounceService->callIDFuseAPICampaignReportBouceList($params);
                //dump($resultCampaignReportBounceList);
                $params = [
                    'campaign_ids' => ['6'],
                    'date_start' => '2023-04-01 00:00:00',
                    'date_end' => '2023-07-01 00:00:00',
                ];
                $resultTotals = $this->bounceService->callIDFuseAPITotals($params);
            //dump($resultTotals);
            } else {  //webhook
            }
        } catch (Throwable $exception) {
            $io->error($exception->getMessage());
            $this->logger->error($exception);

            return Command::FAILURE;
        }

        $io->success('Generated !');

        return Command::SUCCESS;
    }
}
