<?php

namespace App\Command\Debug;

use App\Service\ConvocationService;
use App\Service\MailerService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mime\Part\DataPart;

class SendTestGoogleCalendarInvite extends Command
{
    private MailerService $mailerService;

    public function __construct(MailerService $mailerService, private readonly ConvocationService $convocationService)
    {
        parent::__construct();
        $this->mailerService = $mailerService;
    }

    protected function configure(): void
    {
        $this
            ->setName('email:google:calendar:invite:send')
            ->setDescription('Send a test email with a Google Calendar invite')
            ->addArgument('emails', InputArgument::IS_ARRAY, 'The email address to send the test email to');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $mails = ['baptiste.brand2002@gmail.com', 'baptiste.brand@live-session.fr', 'baptiste.brand.testemail@gmail.com', 'baptiste.brand.testemail@gmx.fr', 'baptiste.brand.testemail@proton.me', 'baptiste.brand_testemail@yahoo.com', 'baptiste.brand.test@outlook.com', 'baptiste.brand.testemail@tutanota.com', 'baptiste.brand.testmail@zohomail.eu', 'baptiste.brand.testemail@mailo.com', 'baptiste.brand.testemail@mailfence.com'];
        if (!empty($input->getArgument('emails'))) {
            $mails = $input->getArgument('emails');
        }

        foreach ($mails as $mail) {
            try {
                $receiverName = 'Google';
                $receiverEmail = $mail;
                $organizerName = 'Live Session';
                $organizerEmail = 'webinar@mylivesession.com';
                $dateStart = (new \DateTime())->modify('+2 days');
                $dateEnd = (clone $dateStart)->modify('+2 hours');
                $location = 'https://livesession38.webex.com/livesession38-fr/j.php?MTID=maf9297475cd71b56fae55beb156536bf';
                $uid = 'a658e3c9-e011-4b50-9b8c-0f7568c06024-'.uniqid();
                $description = "Bonjour,\n\nJe vous invite à mon événement qui aura lieu le 1er mai 2023. Veuillez trouver ci-joint l\'invitation au format ICS.\n\nCordialement,\nJohn Doe";
                $summary = 'Invitation pour la session: '.uniqid();

                $params = [
                    'receiverName' => $receiverName,
                    'receiverEmail' => $receiverEmail,
                    'organizerName' => $organizerName,
                    'organizerEmail' => $organizerEmail,
                    'dateStart' => $dateStart,
                    'dateEnd' => $dateEnd,
                    'location' => $location,
                    'uid' => $uid,
                    'description' => $description,
                    'summary' => $summary,
                ];

                $ical = $this->convocationService->generateIcal($params);
                $icsDataPartFirst = new DataPart($ical, 'invite.ics', 'text/calendar');
                $icsDataPart = new DataPart($ical, 'invitation.ics', 'text/calendar');

                $this->mailerService->sendTemplatedMail($mail, $summary, 'emails/test_google_calendar_invite_send.html.twig', attachDataPart: [$icsDataPart, $icsDataPartFirst]);
                $output->writeln('Sent to <info>'.$mail.'</info>');
            } catch (\Exception $e) {
                $output->writeln($e->getMessage());
                $output->writeln('Error for <error>'.$mail.'</error>');
            }
        }

        $output->writeln('All mails sent');

        return Command::SUCCESS;
    }
}
