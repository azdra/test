<?php

namespace App\Command\Debug;

    use App\Entity\AsyncTask;
    use App\Repository\AsyncTaskRepository;
    use App\Service\AsyncWorker\TaskInterface;
    use Psr\Log\LoggerInterface;
    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Helper\Table;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Input\InputOption;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\Console\Style\SymfonyStyle;

    class AsyncWorkerTaskStatusDebugCommand extends Command
    {
        protected static $defaultName = 'debug:async-task:status';

        private AsyncTaskRepository $asyncTaskRepository;
        private LoggerInterface $logger;

        public function __construct(AsyncTaskRepository $asyncTaskRepository, LoggerInterface $cronLogger)
        {
            parent::__construct();

            $this->asyncTaskRepository = $asyncTaskRepository;
            $this->logger = $cronLogger;
        }

        protected function configure(): void
        {
            $this->addOption('processing', '-p', InputOption::VALUE_NONE, 'Show only task processing');
            $this->addOption('failed', '-f', InputOption::VALUE_NONE, 'Show only task failed');
        }

        protected function execute(InputInterface $input, OutputInterface $output): int
        {
            $io = new SymfonyStyle($input, $output);

            $showOnlyProcessing = $input->getOption('processing');
            $showOnlyFailed = $input->getOption('failed');

            try {
                if ($showOnlyProcessing) {
                    $tasks = $this->asyncTaskRepository->getLastTaskByStatus(TaskInterface::STATUS_PROCESSING);
                } elseif ($showOnlyFailed) {
                    $tasks = $this->asyncTaskRepository->getLastTaskByStatus(TaskInterface::STATUS_FAILED);
                } else {
                    $tasks = $this->asyncTaskRepository->getNextTasks();
                }

                $table = (new Table($output))->setHeaders([
                     'ID',
                     'Client',
                     'User',
                     'Classname',
                     'Status',
                     'Created at',
                     'Processing start at',
                     'Processing end at',
                 ]);

                /** @var AsyncTask $task */
                foreach ($tasks as $task) {
                    $table->addRow([
                       $task->getId(),
                       $task->getClientOrigin()->getName(),
                       $task->getUserOrigin(),
                       $task->getClassName(),
                       $task->getStatus(),
                       $task->getCreatedAt()->format('c'),
                       $task->getProcessingStartTime()?->format('c'),
                       $task->getProcessingEndTime()?->format('c'),
                   ]);
                }

                $table->render();

                return Command::SUCCESS;
            } catch (\Exception $exception) {
                $this->logger->error($exception);
                $io->error('Unknown error: '.$exception->getMessage());

                return Command::FAILURE;
            }
        }
    }
