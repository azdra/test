<?php

namespace App\Command\Debug;

use App\Entity\Convocation;
use App\Entity\ProviderParticipantSessionRole;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Service\ConvocationService;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Throwable;

class ConvocationShortCodesDebugCommand extends Command
{
    protected static $defaultName = 'debug:convocation:shorts-codes';

    public function __construct(
        private ConvocationService $convocationService,
        private ValidatorInterface $validator,
        private LoggerInterface $cronLogger,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('ppsr', 'p', InputOption::VALUE_OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $ppsr = $input->getOption('ppsr');

        $rawConvocationData = file_get_contents('data/convocations/convocation.html');

        try {
            if (empty($ppsr)) {
                $contentFr = $this->convocationService->transformWithFakeData($rawConvocationData);
                $contentEn = $this->convocationService->transformWithFakeData($rawConvocationData, 'en');

                $convocation = (new Convocation())->setSubjectMail($rawConvocationData)
                                                  ->setContent($rawConvocationData);
            } else {
                /** @var ProviderParticipantSessionRole $role */
                $role = $this->providerParticipantSessionRoleRepository->find($ppsr);

                if (empty($role)) {
                    throw new \Exception('The given provider participant role ID was not found');
                }

                $contentFr = $contentEn = $this->convocationService->transformShortsCodes($rawConvocationData, $role);

                $convocation = $role->getSession()->getConvocation();
            }

            $this->validateConvocationContent($io, $output, $convocation);

            (new Filesystem())->dumpFile('data/convocations/result_fr.html', $contentFr);
            (new Filesystem())->dumpFile('data/convocations/result_en.html', $contentEn);
        } catch (Throwable $exception) {
            $io->error($exception->getMessage());
            $this->cronLogger->error($exception);

            return Command::FAILURE;
        }

        $io->success('Generated !');

        return Command::SUCCESS;
    }

    /**
     * @throws Exception
     */
    private function validateConvocationContent(SymfonyStyle $io, OutputInterface $output, Convocation $convocation): void
    {
        $violations = $this->validator->validate($convocation, groups: ['short_code_validation']);

        if ($violations->count() > 0) {
            $io->error('Invalid convocation content');
            $this->cronLogger->error(new \Exception('An unknown error occurred during execute ConvocationShortCodesDebugCommand in validateConvocationContent'), [
                'message' => 'Invalid convocation content',
            ]);

            $table = (new Table($output))
                ->setHeaders([
                    'Property Path',
                    'Invalid Value',
                    'Message',
                    'Constraint',
                ]);

            /** @var ConstraintViolation $violation */
            foreach ($violations as $violation) {
                $table->addRow([
                    $violation->getPropertyPath(),
                    substr($violation->getInvalidValue(), 0, 20).'...',
                    $violation->getMessage(),
                    get_class($violation->getConstraint()),
                ]);
            }

            $io->block('Contraints violations');
            $table->render();

            throw new Exception('Invalid convocation content');
        }
    }
}
