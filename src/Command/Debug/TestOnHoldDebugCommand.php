<?php

namespace App\Command\Debug;

use App\Entity\CategorySessionType;
use App\Entity\OnHoldProviderParticipantSessionRole;
use App\Entity\OnHoldProviderSession;
use App\Repository\ClientRepository;
use App\Repository\OnHoldProviderParticipantSessionRoleRepository;
use App\Repository\OnHoldProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\OnHoldProviderSessionService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class TestOnHoldDebugCommand extends Command
{
    protected static $defaultName = 'debug:test-on-hold';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private OnHoldProviderSessionService $providerSessionService,
        private LoggerInterface $logger,
        private ClientRepository $clientRepository,
        private UserRepository $userRepository,
        private OnHoldProviderSessionRepository $onHoldProviderSessionRepository,
        private OnHoldProviderParticipantSessionRoleRepository $onHoldProviderParticipantSessionRoleRepository,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('action', 'a', InputOption::VALUE_OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $action = $input->getOption('action');

        switch ($action) {
                case 'add': //bin/console debug:test-on-hold -a add
                    $io->info($this->addOnHoldProviderSession());
                    break;
                case 'del': //bin/console debug:test-on-hold -a del
                    $io->info($this->delOnHoldProviderSession());
                    break;
                case 'upd': //bin/console debug:test-on-hold -a upd
                    $io->info($this->updOnHoldProviderSession());
                    break;
                case 'add-participant': //bin/console debug:test-on-hold -a add-participant
                    $io->info($this->addOnHoldProviderPraticipantSessionRole());
                    break;
                case 'upd-participant': //bin/console debug:test-on-hold -a upd-participant
                    $io->info($this->updOnHoldProviderPraticipantSessionRole());
                    break;
                case 'del-participant': //bin/console debug:test-on-hold -a del-participant
                    $io->info($this->delOnHoldProviderPraticipantSessionRole());
                    break;
                default: //bin/console debug:test-on-hold | 'add'
                    $io->info($this->addOnHoldProviderSession());
                    break;
            }

        $this->logger->info('Test CRUD du fonctionnement de OnHoldProviderSession');

        $io->success('Generated !');

        return Command::SUCCESS;
    }

    private function addOnHoldProviderSession(): string
    {
        try {
            $clientTest = $this->clientRepository->findOneBy(['name' => 'Live Session 2']);
            $userTest = $this->userRepository->findOneBy(['email' => 'ingenierie@live-session.fr', 'client' => $clientTest]);

            $ohps = new OnHoldProviderSession();
            /*$ohps
                ->setClient($clientTest)
                ->setCreatedBy($userTest)
                ->setUpdatedBy($userTest)
                ->setName('Demande test')
                ->setRef('TESTCCU00001')
                ->setDateStart((new \DateTime())->modify('+7 days')->setTime(10, 00))
                ->setDateEnd((new \DateTime())->modify('+7 days')->setTime(11, 00))
                ->setCategory(CategorySessionType::CATEGORY_SESSION_FORMATION)
                ->setDuration(60)
                ->setInformation(null)
                ->setLanguage('fr')
                ->setStatus(OnHoldProviderSession::STATUS_WAIT_FOR_VALIDATION)
                ->setRestlunch(0)
                ->setNbrdays(1)
                ->setDescription(null)
                ->setDescription2(null)
                ->setAdditionalTimezones([]);

            $this->entityManager->persist($ohps);
            $this->entityManager->flush();*/

            return 'OnHoldProviderSession a été ajouté !';
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $exception->getMessage();
        }
    }

    private function updOnHoldProviderSession(): string
    {
        try {
            $clientTest = $this->clientRepository->findOneBy(['name' => 'Live Session 2']);
            $userTest = $this->userRepository->findOneBy(['email' => 'ingenierie@live-session.fr', 'client' => $clientTest]);

            $ohps = $this->onHoldProviderSessionRepository->findOneBy(['id' => 1]);

            //$ohps = new OnHoldProviderSession();
            /*$ohps
                ->setUpdatedBy($userTest)
                ->setName('Demande test modifiée')
                ->setRef('TESTCCU00001')
                ->setDateStart((new \DateTime())->modify('+7 days')->setTime(10, 00))
                ->setDateEnd((new \DateTime())->modify('+7 days')->setTime(11, 00))
                ->setCategory(CategorySessionType::CATEGORY_SESSION_FORMATION)
                ->setDuration(60)
                ->setInformation(null)
                ->setLanguage('fr')
                ->setStatus(OnHoldProviderSession::STATUS_1)
                ->setRestlunch(0)
                ->setNbrdays(1)
                ->setDescription(null)
                ->setDescription2(null)
                ->setAdditionalTimezones([]);

            $this->entityManager->persist($ohps);
            $this->entityManager->flush();*/

            return 'OnHoldProviderSession a été modifié !';
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $exception->getMessage();
        }
    }

    private function delOnHoldProviderSession(): string
    {
        try {
            $ohps = $this->onHoldProviderSessionRepository->findOneBy(['id' => 1]);

            $this->entityManager->remove($ohps);
            $this->entityManager->flush();

            return 'OnHoldProviderSession a été supprimé !';
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $exception->getMessage();
        }
    }

    private function addOnHoldProviderPraticipantSessionRole(): string
    {
        try {
            $onHoldProviderSessionTest = $this->onHoldProviderSessionRepository->findOneBy(['id' => 1]);

            /*$ohppsr = new OnHoldProviderParticipantSessionRole();
            $ohppsr
                ->setSession($onHoldProviderSessionTest)
                ->setRole(OnHoldProviderParticipantSessionRole::ROLE_TRAINEE)
                ->setName('Jones')
                ->setForname('Indiana')
                ->setEmail('in.diana.jaunst@gemmelle.fr')
                ->setSituationMixte(OnHoldProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL);

            $this->entityManager->persist($ohppsr);
            $this->entityManager->flush();*/

            return 'OnHoldProviderParticipantSessionRole a été ajouté !';
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $exception->getMessage();
        }
    }

    private function updOnHoldProviderPraticipantSessionRole(): string
    {
        try {
            $ohppsr = $this->onHoldProviderParticipantSessionRoleRepository->findOneBy(['id' => 1]);

            $ohppsr
                ->setRole(OnHoldProviderParticipantSessionRole::ROLE_ANIMATOR)
                ->setName('Jones')
                ->setForname('Indiana')
                ->setEmail('in.diana.jaunst2@gemmelle.fr')
                ->setSituationMixte(OnHoldProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL);

            $this->entityManager->persist($ohppsr);
            $this->entityManager->flush();

            return 'OnHoldProviderParticipantSessionRole a été ajouté !';
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $exception->getMessage();
        }
    }

    private function delOnHoldProviderPraticipantSessionRole(): string
    {
        try {
            $ohppsr = $this->onHoldProviderParticipantSessionRoleRepository->findOneBy(['id' => 1]);

            $this->entityManager->remove($ohppsr);
            $this->entityManager->flush();

            return 'OnHoldProviderSession a été supprimé !';
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $exception->getMessage();
        }
    }
}
