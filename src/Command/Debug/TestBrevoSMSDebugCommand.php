<?php

namespace App\Command\Debug;

use App\Service\SMSService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class TestBrevoSMSDebugCommand extends Command
{
    protected static $defaultName = 'debug:test-brevo-sms';

    public function __construct(
        private SMSService $SMSService,
        private LoggerInterface $logger,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('phoneto', 't', InputOption::VALUE_OPTIONAL);
    }

    //bin/console debug:test-brevo-sms -t +33601020304
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $params = [
                'sender' => 'LiveSession',
                'recipient' => (empty($input->getOption('phoneto'))) ? '33604143138' : $input->getOption('phoneto'),
                //'sender' => 'Sender Brevo Test',
                'content' => 'test GPA',
                'tag' => json_encode([
                    'type' => 'sendConvocation',
                ]),
            ];
            $sendSMS = $this->SMSService->sendSmsBrevo($params);
            // dump($sendSMS);
            if (isset($sendSMS['error'])) {
                $io->error($sendSMS['error']);

                return Command::FAILURE;
            }
            $returnSendSMS = [
                $sendSMS['reference'],  //"oqw6lqi36ljeseqznd"
                $sendSMS['messageId'], //75234386430827
                $sendSMS['smsCount'],  //1
                $sendSMS['usedCredits'],  //4.5
                $sendSMS['remainingCredits'],  //495.5
            ];
            //dump($returnSendSMS);
        } catch (Throwable $exception) {
            $io->error($exception->getMessage());
            $this->logger->error($exception);

            return Command::FAILURE;
        }

        $io->success('Generated !');

        return Command::SUCCESS;
    }
}
