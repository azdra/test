<?php

namespace App\Command\Debug;

use App\Entity\Client\Client;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'debug:user:create';
    private LoggerInterface $logger;

    public function __construct(private EntityManagerInterface $entityManager, LoggerInterface $cronLogger)
    {
        parent::__construct();
        $this->logger = $cronLogger;
    }

    private function askClient(SymfonyStyle $style): Client
    {
        $clientNames = array_column(
            $this->entityManager->getRepository(Client::class)
                ->createQueryBuilder('client')
                ->select('client.name')
                ->getQuery()
                ->getResult(),
            'name'
        );

        $question = new ChoiceQuestion('Please enter client', $clientNames);
        $clientName = $style->askQuestion($question);
        $client = $this->entityManager->getRepository(Client::class)
            ->findOneBy(['name' => $clientName]);

        if ($client === null) {
            $this->logger->error(new \Exception('An unknown error occurred during execute CreateUserCommand in askClient'), [
                'message' => 'The account cannot be found',
                'Client Name ' => $clientName,
            ]);
            throw new \Exception('The account cannot be found');
        }

        return $client;
    }

    private function askNumberOF(SymfonyStyle $style): int
    {
        $question = new Question('Please enter the number of user to create', 200);
        $numberOf = (int) $style->askQuestion($question);

        if ($numberOf <= 0) {
            $this->logger->error(new \Exception('An unknown error occurred during execute CreateUserCommand in askNumberOF'), [
                'message' => 'You have to type a positive number',
                'numberOf' => $numberOf,
            ]);
            throw new \Exception('You have to type a positive number');
        }

        return $numberOf;
    }

    private function flushAndClear(): void
    {
        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $style = new SymfonyStyle($input, $output);
        try {
            $client = $this->askClient($style);
            $numberOf = $this->askNumberOF($style);

            $clientId = $client->getId();
            $faker = Factory::create('fr_FR');

            $style->progressStart($numberOf);
            $i = 1;
            while ($i <= $numberOf) {
                $user = new User();
                $user->setClient($client)
                    ->setFirstName($faker->firstName)
                    ->setLastName($faker->lastName)
                    ->setEmail($faker->creditCardNumber.$faker->email)
                    ->setPassword($faker->password)
                    ->setRoles(['ROLE_TRAINEE'])
                    ->setReference($faker->creditCardNumber)
                    ->setPreferredLang('fr');
                $this->entityManager->persist($user);
                if ($i % 50 === 0) {
                    $this->flushAndClear();
                    $client = $this->entityManager->getRepository(Client::class)->find($clientId);
                    $style->progressAdvance(50);
                }
                ++$i;
            }
            $this->flushAndClear();
            $style->progressFinish();

            $style->success('Tout le monde a été créé !');

            return Command::SUCCESS;
        } catch (\Exception $exception) {
            $style->error($exception->getMessage());
            $this->logger->error($exception);

            return Command::FAILURE;
        }
    }
}
