<?php

namespace App\Command\Debug;

use App\Service\MailerService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class TestBrevoEmailDebugCommand extends Command
{
    protected static $defaultName = 'debug:test-brevo-email';

    public function __construct(
        private MailerService $mailerService,
        private LoggerInterface $logger,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('emailto', 't', InputOption::VALUE_OPTIONAL);
    }

    //bin/console debug:test-brevo-email -t bouncetest2@live-session.fr
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $sendEmail = $this->mailerService->callBrevoSendTest(['to' => $input->getOption('emailto')], [], '', 'GET');
        } catch (Throwable $exception) {
            $io->error($exception->getMessage());
            $this->logger->error($exception);

            return Command::FAILURE;
        }

        $io->success('Generated !');

        return Command::SUCCESS;
    }
}
