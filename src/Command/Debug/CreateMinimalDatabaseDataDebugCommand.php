<?php

namespace App\Command\Debug;

use App\Entity\Client\Client;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateMinimalDatabaseDataDebugCommand extends Command
{
    protected static $defaultName = 'debug:database:init-data';
    private LoggerInterface $logger;

    public function __construct(private EntityManagerInterface $entityManager, LoggerInterface $cronLogger)
    {
        parent::__construct();
        $this->logger = $cronLogger;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $style = new SymfonyStyle($input, $output);

        try {
            $client = (new Client())
                ->setName($this->ask($style, 'Client name ?', 'Livesession'));
            $user = (new User())
                ->setClient($client)
                ->setRoles([User::ROLE_ADMIN])
                ->setFirstName($this->ask($style, 'User firstname ?'))
                ->setLastName($this->ask($style, 'User lastname ?'))
                ->setEmail($this->ask($style, 'User email ?'))
                ->setClearPassword($this->ask($style, 'User password ?', hidden: true));

            $this->entityManager->persist($client);
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            return Command::SUCCESS;
        } catch (\Exception $exception) {
            $this->logger->error($exception);
            $style->error($exception->getMessage());

            return Command::FAILURE;
        }
    }

    private function ask(SymfonyStyle $style, string $question, ?string $defaultResponse = null, bool $hidden = false): string
    {
        $question = new Question($question, $defaultResponse);

        if ($hidden) {
            $question->setHidden(true);
            $question->setHiddenFallback(false);
        }

        $response = $style->askQuestion($question);

        if (strlen($response) === 0) {
            $this->logger->error(new \Exception('An unknown error occurred during execute CreateMinimalDatabaseDataDebugCommand in ask '), [
                'question' => $question->getQuestion(),
                'message' => 'This question can not be null or empty',
            ]);
            throw new \Exception('This question can not be null or empty');
        }

        return $response;
    }
}
