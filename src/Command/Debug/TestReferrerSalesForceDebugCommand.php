<?php

namespace App\Command\Debug;

use App\Repository\ClientRepository;
use App\Repository\UserRepository;
use App\Service\ReferrerService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class TestReferrerSalesForceDebugCommand extends Command
{
    protected static $defaultName = 'debug:test-referrer-sales-force';

    public function __construct(
        private ReferrerService $referrerService,
        private LoggerInterface $logger,
        private ClientRepository $clientRepository,
        private UserRepository $UserRepository
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('ppsr', 'p', InputOption::VALUE_OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            //francobest@hotmail.fr >> kovosky.eliassaint@first-finance.fr
            //aurelien.etie@socgen.com >> kovosky.eliassaint@first-finance.fr
            //ella.lagingrat.ff1@gmail.com >> vincent.tixier@first-finance.net

            $clientFF = $this->clientRepository->findOneBy(['name' => 'First Finance']);
            /*$user = $this->UserRepository->findOneBy(['email' => 'ella.lagingrat.ff1@gmail.com', 'client' => $clientFF]);
            if (!empty($user)) {
                $io->success($user->getFirstName().' '.$user->getLastName().' | '.$user->getEmail());*/
            $emailReferrer = $this->referrerService->SFDCConnection('francobest@hotmail.fr');
            print_r('*** francobest@hotmail.fr >>> '.$emailReferrer.' ***');
            $this->logger->info('Email referrer test  from command: '.$emailReferrer);
            /*} else {
                $io->warning('User not finded!');
            }*/
        } catch (Throwable $exception) {
            $io->error($exception->getMessage());
            $this->logger->error($exception);

            return Command::FAILURE;
        }

        $io->success('Generated !');

        return Command::SUCCESS;
    }
}
