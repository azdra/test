<?php

namespace App\Command\Debug;

use App\Entity\ActivityLog;
use App\Repository\ClientRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateActivitiesLogDebugCommand extends Command
{
    public function __construct(
        private ClientRepository $clientRepository,
        private UserRepository $userRepository,
        private LoggerInterface $cronLogger,
        private EntityManagerInterface $entityManager
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('debug:activities:update')
             ->addArgument('number', InputArgument::REQUIRED)
            ->addOption('client', 'c', InputOption::VALUE_OPTIONAL, default: 3)
            ->addOption('user', 'u', InputOption::VALUE_OPTIONAL, default: 5);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $number = $input->getArgument('number');
        $infos = [
            ['message' => ['key' => 'key', 'params' => []], 'links' => []],
            [
                'message' => ['key' => 'key', 'params' => []],
                'links' => [
                    [
                        'type' => 'user',
                        'parameters' => ['id' => 54789],
                    ],
                ],
            ],
        ];

        $severities = [
            ActivityLog::SEVERITY_NOTIFICATION,
            ActivityLog::SEVERITY_INFORMATION,
            ActivityLog::SEVERITY_ERROR,
            ActivityLog::SEVERITY_WARNING,
            ];

        $origin = [
            ActivityLog::ORIGIN_CRON,
            ActivityLog::ORIGIN_EXPORT,
            ActivityLog::ORIGIN_IMPORT,
            ActivityLog::ORIGIN_USER_INTERFACE,
        ];

        $actions = [
            ActivityLog::ACTION_IMPORT_SESSIONS,
    ActivityLog::ACTION_IMPORT_USERS,
    ActivityLog::ACTION_SESSION_CREATION,
    ActivityLog::ACTION_SESSION_UPDATE,
    ActivityLog::ACTION_SESSION_DELETE,
    ActivityLog::ACTION_SESSION_CANCEL,
    ActivityLog::ACTION_SESSION_UNCANCEL,
    ActivityLog::ACTION_USER_ADD,
    ActivityLog::ACTION_USER_ANONYMIZE,
    ActivityLog::ACTION_USER_UPDATE,
    ActivityLog::ACTION_USER_DELETE,
    ActivityLog::ACTION_PARTICIPANT_ADD,
    ActivityLog::ACTION_PARTICIPANT_UNSUBSCRIBE,
    ActivityLog::ACTION_PARTICIPANT_REGISTRATION,
    ActivityLog::ACTION_PARTICIPANT_UPDATE_ROLE,
    ActivityLog::ACTION_CLIENT_CREATION,
    ActivityLog::ACTION_CLIENT_UPDATED,
    ActivityLog::ACTION_SEND_CONVOCATION,
    ActivityLog::ACTION_UPDATE_CONVOCATION,
    ActivityLog::ACTION_DELETE_CONVOCATION,
    ActivityLog::ACTION_CREATE_CONVOCATION,
    ActivityLog::ACTION_SEND_REMINDER,
        ];

        $io->progressStart($number);

        try {
            $client = $this->clientRepository->find($input->getOption('client'));
            $user = $this->userRepository->find($input->getOption('user'));

            $users = [
                $user,
                null,
            ];

            for ($i = 0; $i <= $number; ++$i) {
                $random = rand(100, 9999);
                $activity = ( new ActivityLog() )
                    ->setOrigin($origin[array_rand($origin)])
                    ->setSeverity($severities[array_rand($severities)])
                    ->setAction($actions[array_rand($actions)])
                    ->setClient($client)
                    ->setUser($users[array_rand($users)])
                    ->setInfos($infos[array_rand($infos)])
                    ->setCreatedAt(( new \DateTime() )->modify("-$random hours -$random minutes"));

                $this->entityManager->persist($activity);

                if ($i % 100 === 0) {
                    $this->entityManager->flush();
                    $this->entityManager->clear();

                    $client = $this->clientRepository->find($input->getOption('client'));
                    $user = $this->userRepository->find($input->getOption('user'));

                    $users = [
                        $user,
                        null,
                    ];

                    $io->progressAdvance(100);
                }
            }

            $this->entityManager->flush();

            $io->progressFinish();
            $io->success('Tout le monde a été créé !');

            return Command::SUCCESS;
        } catch (\Exception $exception) {
            $io->error($exception->getMessage());
            $this->cronLogger->error($exception);

            return Command::FAILURE;
        }
    }
}
