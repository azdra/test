<?php

namespace App\Command\Debug\Connector;

    use App\Entity\Cisco\WebexMeeting;
    use App\Entity\Client\Client;
    use App\Entity\User;
    use App\Exception\ProviderServiceValidationException;
    use App\Model\ProviderResponse;
    use App\Service\WebexMeetingService;
    use Doctrine\ORM\EntityManagerInterface;
    use Exception;
    use Psr\Log\LoggerInterface;
    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\Console\Style\SymfonyStyle;
    use Symfony\Component\Validator\ConstraintViolation;
    use Throwable;

    class WebexMeetingDebugCommand extends Command
    {
        protected static $defaultName = 'debug:connector:webex-meeting';

        public function __construct(
            private WebexMeetingService $webexMeetingService,
            private EntityManagerInterface $entityManager,
            private LoggerInterface $cronLogger
        ) {
            parent::__construct();
        }

        protected function execute(InputInterface $input, OutputInterface $output): int
        {
            $io = new SymfonyStyle($input, $output);

            try {
                // <editor-folder> Configuration holder
                /** @var Client $client */
                $client = $this->entityManager
                    ->getRepository(Client::class)
                    ->find(1);
                $configurationHolder = $client->getConfigurationHolders()[0];

                $io->table(
                    ['Username', 'Password', 'SiteName', 'API Url'], [
                    [
                        $configurationHolder->getUsername(),
                        $configurationHolder->getPassword(),
                        $configurationHolder->getSiteName(),
                        $configurationHolder->getApiUrl(), ], ]
                );

                //$io->confirm('Continue?', true);

                $output->write('Login in...');

                $this->webexMeetingService->loginUser($configurationHolder);

                $output->writeln('DONE');
                $io->newLine(2);
                // </editor-folder> Configuration holder

                // <editor-folder> Session
                $io->section('Session');

                $webexLicences = $configurationHolder->getLicences();

                /** @var WebexMeeting $session */
                $session = (new WebexMeeting($configurationHolder))
                    ->setLicence('test@test.test')
                    ->setName('Test CCU joinUrl 001')
                    ->setRef('test'.random_int(0, 100000, ))
                    ->setDateStart(new \DateTime('+ 2 hours'))
                    ->setDuration(30)
                    ->setConvocation($configurationHolder->getClient()->getConvocations()->first());

                $session->setLicence($webexLicences[0]);

                // Create session
                $output->write('Create a session...');
                $response = $this->webexMeetingService->createSession($session);

                $this->handleResponse($response, $output);

                $output->writeln('DONE');
                $this->tableSession($io, $session);

                $output->writeln('DONE');
                $this->tableSession($io, $session);

                $output->writeln('Get connexion link');
//                $response = $this->webexMeetingService->fetchHostJoinUrl($session);
//                if( !$response->isSuccess() ){
//                    $output->writeln('FAIL');
//                    throw $response->getThrown();
//                }
//                $output->writeln('Retrieved url for Host: '.$response->getData());

                /*$response = $this->webexMeetingService->getConnectorJoinUrl($session);
                $this->handleResponse($response, $output);
                $output->writeln('Retrieved url for Guest: '.$response->getData());
                $output->writeln('DONE');

                $io->confirm('Continue?', true);

                $io->section('Delete session');
                $response = $this->webexMeetingService->deleteSession($session);
                $this->handleResponse($response, $output);
                $output->writeln('Session deleted !');*/

                $output->writeln('All good Well done !');
                // </editor-folder> User
            } catch (ProviderServiceValidationException $exception) {
                $this->cronLogger->error($exception);
                $io->newLine();
                $io->error('Invalid session');
                $violations = [];
                /** @var ConstraintViolation $violation */
                foreach ($exception->violationList as $violation) {
                    $violations[] = [
                        $violation->getPropertyPath(), $violation->getMessage(),
                    ];
                }
                $io->table(['Property', 'Message'], $violations);
            } catch (Throwable $exception) {
                $io->newLine();
                $className = get_class($exception);
                $this->cronLogger->error($exception);
                $io->error("Unknown error: [$className]".$exception->getMessage());

                return Command::FAILURE;
            }

            $io->success('Generated !');

            return Command::SUCCESS;
        }

        private function handleResponse(ProviderResponse $response, OutputInterface $output): void
        {
            if (!$response->isSuccess()) {
                $output->writeln('FAIL');

                if ($response->getThrown() instanceof Exception) {
                    $this->cronLogger->error($response->getThrown());
                    throw new Exception($response->getThrown()->getMessage());
                }
            }
        }

        private function tableSession(SymfonyStyle $io, WebexMeeting $meeting): void
        {
            $io->table(['Name', 'Date start', 'Date end', 'Duration', 'Webex key', 'Url path'], [
                [$meeting->getName(),
                    $meeting->getDateStart()->format('c'),
                    $meeting->getDateEnd()->format('c'),
                    $meeting->getDuration(),
                    $meeting->getMeetingKey(),
                    $meeting->getMeetingLink(), ],
            ]);
        }
    }
