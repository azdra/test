<?php

namespace App\Command\Debug\Connector;

use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\Convocation;
use App\Model\ProviderResponse;
use App\Service\AdobeConnectService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AdobeConnectConnectorGetFreeAudioDebugCommand extends Command
{
    protected static $defaultName = 'debug:connector:adobe-connect:get-free-audio';

    protected AdobeConnectService $service;

    protected EntityManagerInterface $entityManager;

    public function __construct(AdobeConnectService $adobeConnectService, EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->service = $adobeConnectService;
        $this->entityManager = $entityManager;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var Convocation $convocation */
        $convocation = $this->entityManager->getRepository(Convocation::class)->findOneBy(['id' => 1]);

        // Doc : https://learn.zoho.com/portal/livesession993/knowledge/manual/r%C3%A8gles-m%C3%A9tier/article/api
        $dateBegin = \DateTime::createFromFormat('Y-m-d H:i:s', '2022-11-30 10:00:00');
        $dateEnd = \DateTime::createFromFormat('Y-m-d H:i:s', '2022-11-30 11:00:00');
        $freeAudios = $this->service->getAvailableAudios($dateBegin, $dateEnd, AbstractProviderAudio::AUDIO_PGI, $convocation->getLanguages(), $convocation->getClient()->getConfigurationHolders()[0]);

        return Command::SUCCESS;
    }

    protected function handleConnectorResponse(ProviderResponse $response): void
    {
        if (!$response->isSuccess()) {
            exit(-1);
        }
    }
}
