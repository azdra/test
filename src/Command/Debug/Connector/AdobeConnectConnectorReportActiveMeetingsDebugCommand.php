<?php

namespace App\Command\Debug\Connector;

use App\Service\AdobeConnectService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AdobeConnectConnectorReportActiveMeetingsDebugCommand extends Command
{
    protected static $defaultName = 'debug:connector:adobe-connect:report-active-meetings';

    protected AdobeConnectService $adobeConnectService;

    public function __construct(AdobeConnectService $adobeConnectService)
    {
        parent::__construct();
        $this->adobeConnectService = $adobeConnectService;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->adobeConnectService->reportActiveMeetings();

        return Command::SUCCESS;
    }
}
