<?php

namespace App\Command\Debug\Connector;

    use App\Entity\Cisco\WebexMeeting;
    use App\Entity\Cisco\WebexParticipant;
    use App\Entity\Client\Client;
    use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
    use App\Entity\ProviderParticipantSessionRole;
    use App\Entity\User;
    use App\Exception\ProviderServiceValidationException;
    use App\Model\ProviderResponse;
    use App\Service\WebexMeetingService;
    use DateTime;
    use Doctrine\ORM\EntityManagerInterface;
    use Exception;
    use Psr\Log\LoggerInterface;
    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\Console\Style\SymfonyStyle;
    use Symfony\Component\Validator\ConstraintViolation;
    use Throwable;

    class WebexMeetingApiDebugCommand extends Command
    {
        protected static $defaultName = 'debug:connector:webex-meeting:test';

        public function __construct(
            private WebexMeetingService $webexMeetingService,
            private EntityManagerInterface $entityManager,
            private LoggerInterface $cronLogger
        ) {
            parent::__construct();
        }

        protected function execute(InputInterface $input, OutputInterface $output): int
        {
            $io = new SymfonyStyle($input, $output);

            try {
                // <editor-folder> Configuration holder
                /** @var Client $client */
                $client = $this->entityManager
                    ->getRepository(Client::class)
                    ->find(4);

                /** @var WebexConfigurationHolder $configurationHolder */
                $configurationHolder = ( new WebexConfigurationHolder() )
                    ->setUsername('ingenierie@live-session.fr')
                    ->setPassword('aaaAAA111!')
                    ->setClient($client);

                $configurationHolder->setLicences(['tests@live-session.fr']);

                $configurationHolder->setSiteName('livesession38');

                $io->table(
                    ['Username', 'Password', 'SiteName', 'API Url'], [
                    [
                        $configurationHolder->getUsername(),
                        $configurationHolder->getPassword(),
                        $configurationHolder->getSiteName(),
                        $configurationHolder->getApiUrl(), ], ]
                );

                $io->confirm('Continue?', true);

                $output->write('Login in...');

                $this->webexMeetingService->loginUser($configurationHolder);

                $output->writeln('DONE');
                $io->newLine(2);
                // </editor-folder> Configuration holder

                // <editor-folder> Session
                $io->section('Session');

                $webexLicences = $configurationHolder->getLicences();

                /** @var WebexMeeting $session */
                $session = (new WebexMeeting($configurationHolder))
                    ->setRef(123456789)
                    ->setName('My session')
                    ->setDateStart(new DateTime('+ 2 hours'))
                    ->setDuration(60);

                $session->setLicence($webexLicences[0]);

                // Create session
                $output->write('Create a session...');
                $response = $this->webexMeetingService->createSession($session);

                $this->handleResponse($response, $output);

                $output->writeln('DONE');
                $this->tableSession($io, $session);

                $dumpGetSession = $io->choice('Do you want to puts content of getSession in file ? ', ['yes', 'no'], 0);
                if ($dumpGetSession) {
                    $response = $this->webexMeetingService->getSession($session);
                    $this->handleResponse($response, $output);
                    $fileName = "/srv/var/getSession_{$session->getMeetingKey()}.txt";
                    file_put_contents($fileName, $response->getData());
                    $io->writeln("getSession content dumped in $fileName on server (look into your home project var folder)");
                }

                // Update session
                $session
                    ->setDateStart((new DateTime('+4 hours')))
                    ->setDuration(120);
                $output->write('Update a session...');
                $response = $this->webexMeetingService->updateSession($session);
                $this->handleResponse($response, $output);
                $output->writeln('DONE');
                $this->tableSession($io, $session);
                $io->confirm('Continue?', true);
                // </editor-folder> Session

                // <editor-folder> User
                $io->section('Animators & trainees');
                $output->write('Add user...');
                $user = (new User())
                    ->setEmail('john.doe@gg.com')
                    ->setFirstName('John')
                    ->setLastName('DOE');
                $participant = (new WebexParticipant($configurationHolder))
                    ->setUser($user);
                $participantRole = (new ProviderParticipantSessionRole())
                    ->setParticipant($participant)
                    ->setSession($session);
                $session->addParticipantRole($participantRole);

                $response = $this->webexMeetingService->updateParticipantSessionRole($participantRole);
                $this->handleResponse($response, $output);
                $output->writeln('DONE');
                $this->tableSession($io, $session);

                $output->writeln('Get connexion link');
//                $response = $this->webexMeetingService->fetchHostJoinUrl($session);
//                if( !$response->isSuccess() ){
//                    $output->writeln('FAIL');
//                    throw $response->getThrown();
//                }
//                $output->writeln('Retrieved url for Host: '.$response->getData());
                $response = $this->webexMeetingService->getConnectorJoinUrl($session);
                $this->handleResponse($response, $output);
                $output->writeln('Retrieved url for Guest: '.$response->getData());
                $output->writeln('DONE');

                $io->confirm('Continue?', true);

                $io->section('Delete session');
                $response = $this->webexMeetingService->deleteSession($session);
                $this->handleResponse($response, $output);
                $output->writeln('Session deleted !');

                $output->writeln('All good Well done !');
                // </editor-folder> User
            } catch (ProviderServiceValidationException $exception) {
                $this->cronLogger->error($exception);
                $io->newLine();
                $io->error('Invalid session');
                $violations = [];
                /** @var ConstraintViolation $violation */
                foreach ($exception->violationList as $violation) {
                    $violations[] = [
                        $violation->getPropertyPath(), $violation->getMessage(),
                    ];
                }
                $io->table(['Property', 'Message'], $violations);
            } catch (Throwable $exception) {
                $io->newLine();
                $className = get_class($exception);
                $this->cronLogger->error($exception);
                $io->error("Unknown error: [$className]".$exception->getMessage());

                return Command::FAILURE;
            }

            $io->success('Generated !');

            return Command::SUCCESS;
        }

        private function handleResponse(ProviderResponse $response, OutputInterface $output): void
        {
            if (!$response->isSuccess()) {
                $output->writeln('FAIL');

                if ($response->getThrown() instanceof Exception) {
                    $this->cronLogger->error($response->getThrown());
                    throw new Exception($response->getThrown()->getMessage());
                }
            }
        }

        private function tableSession(SymfonyStyle $io, WebexMeeting $meeting): void
        {
            $io->table(['Name', 'Date start', 'Date end', 'Duration', 'Webex key', 'User count'], [
                [$meeting->getName(),
                    $meeting->getDateStart()->format('c'),
                    $meeting->getDateEnd()->format('c'),
                    $meeting->getDuration(),
                    $meeting->getMeetingKey(),
                    $meeting->getParticipantRoles()->count(), ],
            ]);

            $participants = [];
            /** @var ProviderParticipantSessionRole $participantRole */
            foreach ($meeting->getParticipantRoles() as $participantRole) {
                /** @var WebexParticipant $participant */
                $participant = $participantRole->getParticipant();

                $participants[] = [
                    $participantRole->getRole(),
                    $participant->getName(),
                    $participant->getEmail(),
                    $participant->getContactIdentifier(),
                    $participant->getRole(),
                ];
            }

            $io->table(['Role in session', 'Full name', 'Email', 'WebexUserId', 'Webex role'], $participants);
        }
    }
