<?php

namespace App\Command\Debug\Connector;

use App\Entity\Cisco\WebexMeeting;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Repository\WebexMeetingRepository;
use App\Service\Provider\Webex\WebexLicensingService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class WebexMeetingLicensingDebugCommand extends Command
{
    protected static $defaultName = 'debug:connector:webex-meeting:licensing';

    public function __construct(
        private WebexMeetingRepository $webexMeetingRepository,
        private WebexLicensingService $webexLicensingService,
        private LoggerInterface $cronLogger
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('session', InputArgument::REQUIRED, 'Session ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $sessionId = $input->getArgument('session');
            /** @var WebexMeeting $session */
            $session = $this->webexMeetingRepository->find($sessionId);

            if (!($session instanceof WebexMeeting)) {
                throw new \Exception('The session is not a webex meeting session');
            }

            /** @var WebexConfigurationHolder $configurationHolder */
            $configurationHolder = $session->getAbstractProviderConfigurationHolder();
            $sessionDateStart = $session->getDateStart();
            $sessionDateEnd = $session->getDateEnd();
            $safetyTime = $configurationHolder->getSafetyTime();

            $io->section('Configuration');
            $io->table(
                ['Session date start', 'Session date end', 'Automatic licensing', 'Safety time (min)'],
                [
                    [
                        $sessionDateStart->format('c'),
                        $sessionDateEnd->format('c'),
                        $configurationHolder->isAutomaticLicensing() ? 'On' : 'Off',
                        $safetyTime,
                    ],
                ]
            );

            $io->section('Available licences');
            $availableLicences = $this->webexLicensingService->getAvailableLicences($configurationHolder, $sessionDateStart, $sessionDateEnd, $safetyTime, $sessionId);
            $from = (clone $sessionDateStart)->modify("- $safetyTime minutes");
            $to = (clone $sessionDateEnd)->modify("+ $safetyTime minutes");
            $io->table(['Slot from', 'Slot to'],
            [
                [
                    $from->format('c'), $to->format('c'),
                ],
            ]);
            $io->listing($availableLicences);

            $io->section('Next licences used');
            $nextLicenceUsed = $this->webexLicensingService->getNextAvailableLicence($configurationHolder, $sessionDateStart, $sessionDateEnd, $safetyTime, $sessionId);

            if (!empty($nextLicenceUsed)) {
                $io->success($nextLicenceUsed);
            } else {
                $io->error('No licence available');
            }

            return Command::SUCCESS;
        } catch (\Exception $exception) {
            $io->newLine();
            $className = get_class($exception);
            $io->error("Unknown error: [$className] ".$exception->getMessage());
            $this->cronLogger->error($exception);

            return Command::FAILURE;
        }
    }
}
