<?php

namespace App\Command\Debug\Connector;

use App\Repository\AdobeConnectConfigurationHolderRepository;
use App\Service\AdobeConnectService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AdobeConnectServiceGetOrCreateScoFolderDebugCommand extends Command
{
    protected static $defaultName = 'debug:service:adobe-connect:get-or-create-sco-folder';

    public function __construct(
        private AdobeConnectConfigurationHolderRepository $adobeConnectConfigurationHolderRepository,
        private AdobeConnectService $adobeConnectService,
        private LoggerInterface $cronLogger
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $style = new SymfonyStyle($input, $output);
        try {
            $configurationHolder = $this->adobeConnectConfigurationHolderRepository->find(1);
            $parentFolderScoId = 5795720624;
            $name = '0001MyName';
//            $style->success( $this->adobeConnectService->getOrCreateScoFolder($configurationHolder, $parentFolderScoId,$name ) );
            $style->success($this->adobeConnectService->getSavingFolderScoId($configurationHolder)->getData());

            return Command::SUCCESS;
        } catch (\Exception $exception) {
            $this->cronLogger->error($exception);
            $style->error($exception->getMessage());

            return Command::FAILURE;
        }
    }
}
