<?php

namespace App\Command\Debug\Connector;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderSession;
use App\Repository\AdobeConnectSCORepository;
use App\Repository\ProviderSessionRepository;
use App\Service\AdobeConnectService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AdobeConnectConnectorUpdateFreeAudioSessionDebugCommand extends Command
{
    protected static $defaultName = 'debug:connector:adobe-connect:update-free-audio-session';

    protected AdobeConnectService $service;

    protected EntityManagerInterface $entityManager;
    protected ProviderSessionRepository $providerSessionRepository;
    protected AdobeConnectSCORepository $adobeConnectSCORepository;

    public function __construct(AdobeConnectService $adobeConnectService, EntityManagerInterface $entityManager, ProviderSessionRepository $providerSessionRepository, AdobeConnectSCORepository $adobeConnectSCORepository)
    {
        parent::__construct();
        $this->service = $adobeConnectService;
        $this->entityManager = $entityManager;
        $this->providerSessionRepository = $providerSessionRepository;
        $this->adobeConnectSCORepository = $adobeConnectSCORepository;
    }

    protected function configure(): void
    {
        $this->addOption('nbrDays', 'd', InputOption::VALUE_OPTIONAL, 'Nb Days');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $nbrDays = $input->getOption('nbrDays');
        $dateStart = (new \DateTimeImmutable())->modify('+1 day')->setTime(0, 0);
        $dateEnd = (new \DateTimeImmutable())->modify('+'.$nbrDays.' day')->setTime(23, 59);

        $clients = $this->entityManager->getRepository(Client::class)->findAll();
        foreach ($clients as $client) {
            if ($client->isActive()) {
                foreach ($client->getConfigurationHolders() as $connector) {
                    if ($connector instanceof AdobeConnectConfigurationHolder) {
                        $sessions = $this->providerSessionRepository->findAllBetweenDatesWithConfigurationHolder(from: $dateStart, to: $dateEnd, idConfigurationHolder: $connector->getId());
                        foreach ($sessions as $session) {
                            if ($this->isNeedToUpdateAudio(providerSession: $session, output: $output)) {
                                $freeAudios = $this->service->getAvailableAudios($session->getDateStart(), $session->getDateEnd(), $session->getConvocation()->getTypeAudio(), $session->getConvocation()->getLanguages(), $connector);
                                if (count($freeAudios) > 0) {
                                    $session->setProviderAudio($freeAudios[0]);
                                    $output->writeln("\t> the audio for this session  has bien updated | ");
                                } else {
                                    $output->writeln("\t> we can't update audio for this session | ".$session->getId().' |-------------- ');
                                }
                                $this->entityManager->flush();
                            }
                        }
                    }
                }
            }
        }

        return Command::SUCCESS;
    }

    protected function isNeedToUpdateAudio(ProviderSession $providerSession, OutputInterface $output): bool
    {
        $sessions = $this->adobeConnectSCORepository->getSCOBetweenDateForAudioFromAdobeConnect($providerSession->getDateStart(), $providerSession->getDateEnd(), $providerSession->getConvocation()->getTypeAudio());
        foreach ($sessions as $session) {
            if ($session != $providerSession && $providerSession->getProviderAudio() == $session->getProviderAudio()) {
                $output->writeln("\t> the session with ID | ".$providerSession->getId().' | need to update audio because he have the same audio with => '.$session->getId());

                return true;
            }
        }

        return false;
    }
}
