<?php

namespace App\Command\Debug\Connector;

use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Model\ProviderResponse;
use App\Repository\AdobeConnectWebinarConfigurationHolderRepository;
use App\Service\AdobeConnectWebinarService;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AdobeConnectWebinarServiceCreateWebinarCommand extends Command
{
    protected static $defaultName = 'debug:service:adobe-connect-webinar:create-webinar';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private AdobeConnectWebinarConfigurationHolderRepository $adobeConnectWebinarConfigurationHolderRepository,
        private AdobeConnectWebinarService $adobeConnectWebinarService,
        private LoggerInterface $cronLogger
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $style = new SymfonyStyle($input, $output);
        try {
            /** @var AdobeConnectWebinarConfigurationHolder $configurationHolder */
            $configurationHolder = $this->adobeConnectWebinarConfigurationHolderRepository->find(6);

            $faker = Factory::create('fr_FR');

            $dateStart = \DateTime::createFromFormat('Y-m-d H:i:s', '2022-10-20 03:00:00')->setTimezone(new \DateTimeZone('Europe/Paris'));
            $dateStartUTC = (clone $dateStart)->setTimezone(new \DateTimeZone('UTC'));

            /** @var AdobeConnectWebinarSCO $session */
            $session = new AdobeConnectWebinarSCO($configurationHolder);
            $session->setRef($faker->creditCardNumber)
                    ->setName('My webinar 001');
            $session->setParentScoIdentifier($configurationHolder->getSavingMeetingFolderScoId())
                    ->setRoomModel(5352936956)
                    ->setDateStart($dateStartUTC)
                    ->setDuration(30);

            $output->write('Create a session...');
            $response = $this->adobeConnectWebinarService->createSession($session);

            $this->entityManager->persist($session);
            $this->entityManager->flush();

            $this->handleResponse($response, $output);

            //$parentFolderScoId = 5795720624;
            $name = '0001MyName';
//            $style->success( $this->adobeConnectService->getOrCreateScoFolder($configurationHolder, $parentFolderScoId,$name ) );
            //$style->success($this->adobeConnectWebinarService->getSavingFolderScoId($configurationHolder)->getData());

            return Command::SUCCESS;
        } catch (\Exception $exception) {
            $this->cronLogger->error($exception);
            $style->error($exception->getMessage());

            return Command::FAILURE;
        }
    }

    private function handleResponse(ProviderResponse $response, OutputInterface $output): void
    {
        if (!$response->isSuccess()) {
            $output->writeln('FAIL');

            if ($response->getThrown() instanceof \Exception) {
                $this->cronLogger->error($response->getThrown());
                throw new \Exception($response->getThrown()->getMessage());
            }
        }
    }
}
