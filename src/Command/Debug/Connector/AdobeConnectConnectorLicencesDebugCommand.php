<?php

namespace App\Command\Debug\Connector;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Repository\ProviderConfigurationHolderRepository;
use App\Service\AdobeConnectService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AdobeConnectConnectorLicencesDebugCommand extends Command
{
    protected static $defaultName = 'debug:connector:adobe-connect:licences';

    protected AdobeConnectService $adobeConnectService;
    private LoggerInterface $logger;

    protected ProviderConfigurationHolderRepository $providerConfigurationHolderRepository;

    public function __construct(
        AdobeConnectService $adobeConnectService,
        LoggerInterface $cronLogger,
        ProviderConfigurationHolderRepository $providerConfigurationHolderRepository
    ) {
        parent::__construct();
        $this->adobeConnectService = $adobeConnectService;
        $this->logger = $cronLogger;
        $this->providerConfigurationHolderRepository = $providerConfigurationHolderRepository;
    }

    protected function configure(): void
    {
        $this->addArgument('configuration-holder', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var AdobeConnectConfigurationHolder $configurationHolder */
        $configurationHolder = $this->providerConfigurationHolderRepository->find((int) $input->getArgument('configuration-holder'));

        if (!$configurationHolder) {
            $output->writeln('Unable to find the configuration holder #'.$input->getArgument('configuration-holder'));
            $this->logger->error(new \Exception('Unable to find the configuration holder #'.$input->getArgument('configuration-holder')));

            return Command::FAILURE;
        }

        // Get the principal of the host group
        $response = $this->adobeConnectService
            ->getHostGroupPrincipal($configurationHolder);

        if (!$response->isSuccess()) {
            $output->writeln((string) $response->getThrown());
            $this->logger->error($response->getThrown());

            return Command::FAILURE;
        }

        $table = (new Table($output))->setHeaders(['Principal identifier', 'Name']);

        $principals = $response->getData();
        /** @var AdobeConnectPrincipal $principal */
        foreach ($principals as $principal) {
            $table->addRow([$principal->getPrincipalIdentifier(), $principal->getName()]);
        }

        $table->render();

        // Get all users inside this group
        /** @var AdobeConnectPrincipal $rootPrincipal */
        $rootPrincipal = reset($principals);
        $configurationHolder->setHostGroupScoId($rootPrincipal->getPrincipalIdentifier());

        $response = $this->adobeConnectService->getPrincipalInHostGroup($configurationHolder);

        if (!$response->isSuccess()) {
            $output->writeln((string) $response->getThrown());
            $this->logger->error($response->getThrown());

            return Command::FAILURE;
        }

        $principals = $response->getData();
        $table = (new Table($output))->setHeaders(['Principal identifier', 'Name', 'Email']);
        /** @var AdobeConnectPrincipal $principal */
        foreach ($principals as $principal) {
            $user = $principal->getUser();
            $table->addRow([$principal->getPrincipalIdentifier(), $principal->getName(), $user->getEmail()]);
        }

        $table->render();

        return Command::SUCCESS;
    }
}
