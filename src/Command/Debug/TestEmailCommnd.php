<?php

namespace App\Command\Debug;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\MailerService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\RouterInterface;

class TestEmailCommnd extends Command
{
    protected static $defaultName = 'app:test-email';

    public function __construct(
        private MailerService $mailerService,
        private ProviderSessionRepository $providerSessionRepository,
        private UserRepository $userRepository,
        private RouterInterface $router
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('<info>Remind expiration Webex access token</info>');

        /** @var User $user */
        $user = $this->userRepository->find(1);
        $user->setPasswordResetToken('12');

        /** @var ProviderSession $session */
        $session = $this->providerSessionRepository->find(10);

        /** @var ProviderParticipantSessionRole $ppsr */
        $ppsr = $session->getParticipantRoles()->first();

        $this->mailerService->sendTemplatedMail(
            'test@gmail.com',
            'deferred_actions_task_send_nodata',
            'emails/alert/deferred_actions_task_send_nodata.html.twig',
            context: [
                'lang' => 'fr',
                'user' => $ppsr->getParticipant()->getUser(),
                'client' => $session->getClient(),
                'fileFilterFrom' => '01/02/2023 00:00',
                'fileFilterTo' => '01/02/2023 00:00',
                'fileFormat' => '$type',
            ],
            /*context: [
               // 'url' => 'aaaaaaaaaaaaaa',
                'user' => $ppsr->getParticipant()->getUser(),
                //'ppsr' => $ppsr,
                //'routeSession' => $this->router->generate('_session_get', ['id' => $session->getId()], 0),
                //'routeSession' => $this->router->generate('_session_get', ['id' => $ppsr->getSession()->getId()], 0),
                //'sessionName' => $session->getName(),
                'session' => $session,
                'emailInvalid' => '$emailInvalid',
                'objectInvalid' => '$subject',
                'client' => $session->getClient(),
                //'providerName' => 'aaaaaaaaaaaaaa',
            ],*/
            metadatas: [
                'origin' => MailerService::ORIGIN_EMAIL_SEND_SESSION_UNCANCELED,
                'user' => $ppsr->getParticipant()->getUser()->getId(),
                'sessionName' => $session->getName(),
                'providerName' => 'aaaaaaaaaaaaaa',
            ],
        );

        //
//        $this->mailerService->sendTemplatedMail(
//            'test@gmail.com',
//            'Choose your password',
//            'emails/admin/password_new.html.twig',
//            [
//                'user' => $user,
//                'client' => $user->getClient(),
//            ],
//            metadatas: [
//                'origin' => MailerService::ORIGIN_EMAIL_SEND_CHOOSE_PASSWORD_REQUEST,
//                'user' => $user->getId(),
//            ],
//            senderSystem: true
//        );

        /*$this->mailerService->sendTemplatedMail(
            'test@gmail.com',
            'Choose your password',
            'emails/participant/session_cancel.html.twig',
            context: [
                'session' => $session,
                'lang' => 'fr',
                'participant' => $ppsr->getParticipant(),
                'client' => $session->getClient(),
                'clientTimeZone' => $session->getClient()->getTimezone(),
            ],
            metadatas: [
                'origin' => MailerService::ORIGIN_EMAIL_SEND_SESSION_CANCELED,
                'user' => $ppsr->getUserId(),
                'client' => $ppsr->getParticipant()->getClient()->getId(),
            ]
        );*/

        return Command::SUCCESS;
    }
}
