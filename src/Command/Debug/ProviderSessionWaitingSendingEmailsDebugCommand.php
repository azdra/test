<?php

namespace App\Command\Debug;

use App\Entity\ProviderSession;
use App\Repository\ProviderSessionRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class ProviderSessionWaitingSendingEmailsDebugCommand extends Command
{
    protected static $defaultName = 'debug:session:email:waiting';

    private ProviderSessionRepository $providerSessionRepository;
    private LoggerInterface $logger;

    public function __construct(ProviderSessionRepository $providerSessionRepository, LoggerInterface $cronLogger)
    {
        parent::__construct();
        $this->logger = $cronLogger;
        $this->providerSessionRepository = $providerSessionRepository;
    }

    protected function configure(): void
    {
        $this->addArgument('session', InputArgument::OPTIONAL, 'Session ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $sessionId = $input->getArgument('session');

            $sessions = (empty($sessionId))
                ? $this->providerSessionRepository->findAllSessionsWithPastNotificationSendingDate()
                : [$this->providerSessionRepository->find($sessionId)];

            $table = (new Table($output))->setHeaders([
                'ID',
                'Ref',
                'Name',
                'State',
                'Send convocation at',
                'Send next reminder at',
                'Last send of convocation',
                'Last send of reminder',
            ]);

            /** @var ProviderSession $session */
            foreach ($sessions as $session) {
                $table->addRow([
                    $session->getId(),
                    $session->getRef(),
                    substr($session->getName(), 0, 34).'...',
                    $session->getReadableState(),
                    $session->getNotificationDate()?->format('c'),
                    $session->getNextReminder()?->format('c'),
                    $session->getLastConvocationSent()?->format('c'),
                    $session->getLastReminderSent()?->format('c'),
                ]);
            }

            $table->render();

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $output->writeln('Unknown error: '.$exception->getMessage());
            $this->logger->error($exception);

            return Command::FAILURE;
        }
    }
}
