<?php

namespace App\Command\Debug;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class DumpEnvVarCommand extends Command
{
    protected static $defaultName = 'ls:dump:envvar';

    public function __construct(private ContainerBagInterface $params)
    {
        parent::__construct();
    }

    public function configure(): void
    {
        $this->addArgument('name');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $style = new SymfonyStyle($input, $output);

        $style->info($this->params->get($input->getArgument('name')));

        return Command::SUCCESS;
    }
}
