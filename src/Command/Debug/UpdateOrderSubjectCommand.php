<?php

namespace App\Command\Debug;

use App\SelfSubscription\Repository\ModuleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateOrderSubjectCommand extends Command
{
    protected static $defaultName = 'update:order:subject';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $cronLogger,
        private ModuleRepository $moduleRepository
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $style = new SymfonyStyle($input, $output);

        try {
            $allModules = $this->moduleRepository->findAll();
            $progressBar = new ProgressBar($output, count($allModules));
            $progressBar->start();

            foreach ($allModules as $module) {
                $order = 1;
                foreach ($module->getThemes() as $theme) {
                    foreach ($theme->getSubjects() as $subject) {
                        $subject->setOrderInModule($order++);
                        $this->entityManager->persist($subject);
                    }
                }
                $progressBar->advance();
            }
            $this->entityManager->flush();
            $progressBar->finish();
            $style->success('Order in module updated successfully.');

            return Command::SUCCESS;
        } catch (\Exception $exception) {
            $style->error($exception->getMessage());
            $this->cronLogger->error($exception->getMessage(), ['exception' => $exception]);

            return Command::FAILURE;
        }
    }
}
