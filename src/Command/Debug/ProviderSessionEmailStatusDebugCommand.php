<?php

namespace App\Command\Debug;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\EventSubscriber\TransactionalSessionEmailRequestSubscriber;
use App\Repository\ProviderSessionRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProviderSessionEmailStatusDebugCommand extends Command
{
    protected static $defaultName = 'debug:session:email:participant-status';

    private ProviderSessionRepository $providerSessionRepository;
    private LoggerInterface $logger;

    public function __construct(ProviderSessionRepository $providerSessionRepository, LoggerInterface $cronLogger)
    {
        parent::__construct();
        $this->logger = $cronLogger;
        $this->providerSessionRepository = $providerSessionRepository;
    }

    protected function configure(): void
    {
        $this->addArgument('session', InputArgument::REQUIRED, 'Session ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $sessionId = $input->getArgument('session');
            /** @var ProviderSession $session */
            $session = $this->providerSessionRepository->find($sessionId);

            if (empty($session)) {
                throw new \Exception('Session not found');
            }

            $io->table(
                ['Ref',
                    'Status',
                    'Name',
                    'Send convocation at',
                    'Last send of convocation',
                    'Send next reminder at',
                    'Last send of reminder',
                ],
                [
                    [
                        $session->getRef(),
                        $session->getReadableState(),
                        $session->getName(),
                        $session->getNotificationDate()?->format('c'),
                        $session->getLastConvocationSent()?->format('c'),
                        $session->getNextReminder()?->format('c'),
                        $session->getLastReminderSent()?->format('c'),
                    ],
                ]
            );

            $io->title('Reminders dates');
            $io->listing(
                array_map(
                    fn (\DateTimeImmutable $dateTime) => $dateTime->format('c'),
                    $session->getDateOfSendingReminders()
                ) ?: ['No reminders']
            );

            $io->title('Participants');

            $table = (new Table($output))->setHeaders([
                'ID',
                'Email',
                'Full name',
                'Role',
                'Last send of convocation',
                'Last send of reminder',
                'Next sending is...',
            ]);

            /** @var ProviderParticipantSessionRole $participantRole */
            foreach ($session->getParticipantRoles() as $participantRole) {
                $participant = $participantRole->getParticipant();
                $nextIs = 'Nothing';

                if (TransactionalSessionEmailRequestSubscriber::participantWantToReceiveConvocation($participantRole)) {
                    $nextIs = 'Convocation';
                }

                if (TransactionalSessionEmailRequestSubscriber::participantWantToReceiveReminder($participantRole)) {
                    $nextIs = 'Reminders';
                }

                $table->addRow([
                    $participant->getId(),
                    $participant->getEmail(),
                    $participant->getFullName(),
                    $participantRole->getRole(),
                    $participantRole->getLastConvocationSent()?->format('c'),
                    $participantRole->getLastReminderSent()?->format('c'),
                    $nextIs,
                ]);
            }

            $table->render();

            return Command::SUCCESS;
        } catch (\Exception $exception) {
            $io->error('Unknown error: '.$exception->getMessage());
            $this->logger->error($exception);

            return Command::FAILURE;
        }
    }
}
