<?php

namespace App\Command;

use App\Entity\Cisco\WebexMeeting;
use App\Service\Provider\Webex\ReportWebexMeetingService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class UpdateWebexMeetingReportCommand extends Command
{
    protected static $defaultName = 'connector:webex-meeting:report';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $cronLogger,
        private ReportWebexMeetingService $reportWebexMeetingService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('meeting_key', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $meetingKey = $input->getArgument('meeting_key');

        try {
            $webexMeeting = $this->entityManager->getRepository(WebexMeeting::class)->findOneBy(['id' => $meetingKey]);
            $reportWebexMeeting = $this->reportWebexMeetingService->getReportWebexMeeting($webexMeeting);

            $this->entityManager->flush();
            $output->writeln("\t >> All done");

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->cronLogger->error($exception, ['id' => $meetingKey]);
            $output->writeln('Unknown error: '.$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
