<?php

namespace App\Command;

use App\Service\MailerService;
use App\Service\WebexRestMeetingService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RemindExpirationWebexAccessToken extends Command
{
    protected static $defaultName = 'app:remind-expiration-webex-access-token';

    public function __construct(
        private WebexRestMeetingService $webexRestMeetingService,
        private MailerService $mailerService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('<info>Remind expiration Webex access token</info>');

        $remindExpireAccessToken = $this->webexRestMeetingService->getRemindExpireAccessToken();

        if (!count($remindExpireAccessToken)) {
            $output->writeln('<info>No expiration access token</info>');

            return Command::SUCCESS;
        } else {
            $output->writeln('<info>we have '.count($remindExpireAccessToken).' Expiration access token</info>');
        }

        $this->mailerService->sendTemplatedMail(
            ['ingenierie@live-session.fr', 'serviceclient@live-session.fr'],
            'Rappel des expiration des access token Webex',
            'emails/alert/reming_webex_expiration_access_token.html.twig',
            context: [
                'items' => $this->webexRestMeetingService->getRemindExpireAccessToken(),
            ],
            senderSystem: true
        );

        return Command::SUCCESS;
    }
}
