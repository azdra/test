<?php

namespace App\Command;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Service\Provider\Adobe\ReportAdobeConnectService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class UpdateAdobeConnectScoReportCommand extends Command
{
    protected static $defaultName = 'connector:adobe-connect:sco:report';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $cronLogger,
        private ReportAdobeConnectService $reportAdobeConnectService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('unique-sco-identifier', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $uniqueSCOIdentifier = $input->getArgument('unique-sco-identifier');
            $adobeConnectSCO = $this->entityManager->getRepository(AdobeConnectSCO::class)->findOneBy(['uniqueScoIdentifier' => $uniqueSCOIdentifier]);
            $reportAdobeConnectSCO = $this->reportAdobeConnectService->getReportAdobeConnect($adobeConnectSCO);

            $this->entityManager->flush();
            $output->writeln("\t >> All done");

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->cronLogger->error($exception, ['uniqueSCOIdentifier' => $uniqueSCOIdentifier]);
            $output->writeln('Unknown error: '.$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
