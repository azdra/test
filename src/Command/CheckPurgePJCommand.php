<?php

namespace App\Command;

use App\Repository\ProviderSessionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class CheckPurgePJCommand extends Command
{
    protected static $defaultName = 'service:check:purge:pj';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $cronLogger,
        private ProviderSessionRepository $providerSessionRepository,
        private string $convocationAttachmentSavefilesDirectory,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $sessions = $this->providerSessionRepository->findOldSessionsByMinusDate(30);

            foreach ($sessions as $session) {
                $sessionConvocationAttachments = $session->getAttachments();
                foreach ($sessionConvocationAttachments as $attachment) {
                    unlink($this->convocationAttachmentSavefilesDirectory.'/'.$attachment->getFileName());
                    $attachment->setFileName('empty');
                    $this->entityManager->persist($attachment);
                    $this->entityManager->flush();
                }
            }

            $output->writeln("\t >> All done");

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->cronLogger->error($exception);
            $output->writeln('Unknown error: '.$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
