<?php

namespace App\Command\Async;

use App\Repository\AsyncTaskRepository;
use App\Service\AsyncWorker\TaskInterface;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Lock\LockFactory;

final class AsyncWorkerWatcherCronStoppedCommand extends Command implements ContainerAwareInterface
{
    public const MAX_PROCESSING_TASK = 5;
    public const WAITING_TIME_SECONDS = 10;

    protected static $defaultName = 'async_relaunch:worker:watch';

    private AsyncTaskRepository $asyncTaskRepository;

    private EntityManagerInterface $entityManager;

    private LoggerInterface $logger;

    private ?ContainerInterface $container;

    private LockFactory $lockFactory;

    public function __construct(
        AsyncTaskRepository $asyncTaskRepository,
        EntityManagerInterface $entityManager,
        LoggerInterface $asyncTaskLogger,
        LockFactory $lockFactory,
        private MailerService $mailerService)
    {
        parent::__construct();
        $this->asyncTaskRepository = $asyncTaskRepository;
        $this->entityManager = $entityManager;
        $this->logger = $asyncTaskLogger;
        $this->lockFactory = $lockFactory;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (empty($tasks = $this->asyncTaskRepository->getUnfinishedAndBlockedTasks(new \DateTimeImmutable('- 1 hour')))) {
            $output->writeln('No new task to be relaunched');
            $this->logger->info('No new task to be relaunched');

            return $this->endWorker($output);
        }
        $count = count($tasks);
        $max = self::MAX_PROCESSING_TASK;
        $message = "$count/$max to be relaunched";

        $output->writeln($message);
        $this->logger->info($message);

        $infosTasksRelaunched = [];
        foreach ($tasks as $task) {
            $task->setStatus(TaskInterface::STATUS_READY);
            $infosTasksRelaunched[] = [
                'idTask' => $task->getId(),
                'clientName' => $task->getClientOrigin()->getName(),
                'userName' => $task->getUserOrigin()->getFullName(),
                'fileOrigin' => $task->getFileOrigin(),
                'type' => substr($task->getClassName(), strrpos($task->getClassName(), '\\') + 1),
            ];
        }
        $this->mailerService->sendTemplatedMail(
            ['support@live-session.fr', 'ingenierie@live-session.fr'],
            'Problèmes d\'exécution des tâches et distribution des tâches à traiter',
            'emails/worker_watcher_list_cron.html.twig',
            context: [
                'date' => (new \DateTimeImmutable())->format('d/m/Y H:i:s'),
                'tasks' => $infosTasksRelaunched,
                ]
        );

        $this->entityManager->flush();

        return $this->endWorker($output);
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    private function getWaitingTimeInMicroSecondes(): int
    {
        return self::WAITING_TIME_SECONDS * 1000 * 1000;
    }

    private function endWorker(OutputInterface $output): int
    {
        $waitingTime = self::WAITING_TIME_SECONDS;

        $output->writeln("Exit. Waiting $waitingTime seconde");
        usleep($this->getWaitingTimeInMicroSecondes());

        return Command::SUCCESS;
    }
}
