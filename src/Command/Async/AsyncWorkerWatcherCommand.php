<?php

namespace App\Command\Async;

use App\Entity\AsyncTask;
use App\Exception\AsyncWorker\AsyncTaskException;
use App\Repository\AsyncTaskRepository;
use App\Service\AsyncWorker\TaskInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Lock\LockFactory;

final class AsyncWorkerWatcherCommand extends Command implements ContainerAwareInterface
{
    public const MAX_PROCESSING_TASK = 5;
    public const WAITING_TIME_SECONDS = 10;

    protected static $defaultName = 'async:worker:watch';

    private AsyncTaskRepository $asyncTaskRepository;

    private EntityManagerInterface $entityManager;

    private LoggerInterface $logger;

    private ?ContainerInterface $container;

    private LockFactory $lockFactory;

    public function __construct(AsyncTaskRepository $asyncTaskRepository, EntityManagerInterface $entityManager, LoggerInterface $asyncTaskLogger, LockFactory $lockFactory)
    {
        parent::__construct();
        $this->asyncTaskRepository = $asyncTaskRepository;
        $this->entityManager = $entityManager;
        $this->logger = $asyncTaskLogger;
        $this->lockFactory = $lockFactory;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (empty($tasks = $this->asyncTaskRepository->getNextTask())) {
            $output->writeln('No new task');
            $this->logger->info('No new task');

            return $this->endWorker($output);
        }

        $count = count($tasks);
        $max = self::MAX_PROCESSING_TASK;
        $message = "$count/$max to processing";

        $output->writeln($message);
        $this->logger->info($message);

        foreach ($tasks as $task) {
            $this->executeAsyncTask($output, $task);
        }

        $this->entityManager->clear();

        return $this->endWorker($output);
    }

    private function executeAsyncTask(OutputInterface $output, AsyncTask $task): void
    {
        // In case this command throws an exception, autorelease the lock
        $taskLocker = $this->lockFactory->createLock('async_task_locker', ttl: 15);
        $taskLocker->acquire(true);

        // Using the container (instead of directly instantiating the task class) allows us to use dependency
        // injection in tasks
        $taskClass = $this->container->get($task->getClassName());

        if ($taskClass === null) {
            $message = sprintf('Task with class %s not found in service container. Skipping and passing status to "failed".', $task->getClassName());
            $this->logger->error($message, [
                'taskClassName' => $task->getClassName(),
                'taskId' => $task->getId(),
            ]);
            $output->writeln($message);
            $taskLocker->release();

            return;
        }

        try {
            if (!\is_subclass_of($taskClass, TaskInterface::class)) {
                $message = sprintf('Task %s with id #%d does not implement TaskInterface. Skipping and passing status to "failed".', $taskClass::class, $taskClass->getId());
                $this->logger->error($message, [
                    'taskClass' => $taskClass::class,
                    'taskId' => $task->getId(),
                ]);
                $output->writeln($message);
                $taskLocker->release();

                return;
            }

            $task->setStatus(TaskInterface::STATUS_PROCESSING)
                ->setProcessingStartTime(new \DateTime());
            $this->entityManager->flush();
            $taskLocker->release();

            $message = sprintf('Processing task #%d.', $task->getId());
            $this->logger->info($message, [
                'taskId' => $task->getId(),
            ]);
            $output->writeln($message);
            $taskClass->execute($task);

            $task->setStatus(TaskInterface::STATUS_DONE);
            $message = sprintf('Task #%d finished successfully.', $task->getId());
            $this->logger->info($message, [
                'taskId' => $task->getId(),
            ]);
            $output->writeln($message);
        } catch (AsyncTaskException|\Throwable $exception) {
            $message = sprintf('Task #%d failed. %s', $task->getId(), $exception->getMessage());
            $this->logger->error($message, [
                'class' => get_class($exception),
                'exception' => $exception->getMessage(),
                'taskId' => $task->getId(),
            ]);
            $output->writeln($message);
            $task->setStatus(TaskInterface::STATUS_FAILED);
        } finally {
            $task->setProcessingEndTime(new \DateTime('now'));
            $this->entityManager->flush();
            $taskLocker->release();
        }
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    private function getWaitingTimeInMicroSecondes(): int
    {
        return self::WAITING_TIME_SECONDS * 1000 * 1000;
    }

    private function endWorker(OutputInterface $output): int
    {
        $waitingTime = self::WAITING_TIME_SECONDS;

        $output->writeln("Exit. Waiting $waitingTime seconde");
        usleep($this->getWaitingTimeInMicroSecondes());

        return Command::SUCCESS;
    }
}
