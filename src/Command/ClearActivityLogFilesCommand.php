<?php

namespace App\Command;

use App\Entity\ActivityLog;
use App\Repository\ActivityLogRepository;
use App\Service\ActivityLogService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class ClearActivityLogFilesCommand extends Command
{
    protected static $defaultName = 'activity-log:clean';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $cronLogger,
        private ActivityLogService $activityLogService,
        private ActivityLogRepository $activityLogRepository
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $interval = new \DateInterval(ActivityLogService::OLDER_INTERVAL_TARGET);
            $targetDate = (new \DateTime())->sub($interval);
            $activitiesLogsToDelete = $this->activityLogRepository->findAllOlderCanBeCleanedUp($interval);
            $io->comment('Delete all before '.$targetDate->format('c'));

            $values = [];
            /** @var ActivityLog $activityLog */
            foreach ($activitiesLogsToDelete as $activityLog) {
                $values[] = [
                    $activityLog->getId(), $activityLog->getAction(), $activityLog->getCreatedAt()->format('c'),
                ];
            }

            $io->table(['#', 'Action', 'Created at...'], $values);

            $this->activityLogService->cleanGivenActivityLogs($activitiesLogsToDelete);
            $this->entityManager->flush();

            $io->success('All done');

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->cronLogger->error($exception);
            $io->error('Unknown error: '.$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
