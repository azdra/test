<?php

namespace App\Command;

use App\Entity\ActivityLog;
use App\Entity\Client\Client;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ClientRepository;
use App\Service\ActivityLogger;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\RouterInterface;

class ClientExpirationCredit extends Command
{
    private ClientRepository $clientRepository;
    private EntityManagerInterface $entityManager;
    private ActivityLogger $activityLogger;
    private RouterInterface $router;

    public function __construct(ClientRepository $clientRepository, EntityManagerInterface $entityManager, ActivityLogger $activityLogger, RouterInterface $router)
    {
        $this->clientRepository = $clientRepository;
        $this->entityManager = $entityManager;
        $this->activityLogger = $activityLogger;
        $this->router = $router;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('client:expiration-credit')
            ->setDescription('Check if client credit is expired')
            ->setHelp('This command check if client credit is expired and send email to client');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $clients = $this->clientRepository->getExpiredCreditClients();
        $output->writeln(sprintf('Clients with expired credit: <info>%s</info>', count($clients)));
        /** @var Client $client */
        foreach ($clients as $client) {
            $client->setExpirationDateCredits(new \DateTime());
            $client->setAllowedCreditParticipants($client->getCurrentParticipantsCount());
            $client->setAllowedCredit($client->getCurrentSessionCount());
            $this->entityManager->persist($client);
            $this->entityManager->flush();

            $infos = [
                'message' => ['key' => 'Credits for client <a href="%clientPath%">%clientName%</a> have expired (for session and participant credits).', 'params' => ['%clientName%' => $client->getName(), '%clientPath%' => $this->router->generate('_client_edit', ['id' => $client->getId()])]],
                'client' => ['id' => $client->getId(), 'email' => $client->getName()],
            ];

            $this->activityLogger->initActivityLogContext($client, null, ActivityLog::ORIGIN_CRON);
            $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_EXPIRED_CLIENT_CREDIT, ActivityLog::SEVERITY_WARNING, $infos), true);
            $this->activityLogger->flushActivityLogs();
            $output->writeln(sprintf('Client <info>%s</info> has expired credit', $client->getName()));
        }

        return Command::SUCCESS;
    }
}
