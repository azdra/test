<?php

namespace App\Command;

use App\Repository\OpenAIRepository;
use App\Service\ChatGPTService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OpenAIRecoverCreditGrantCommand extends Command
{
    protected static $defaultName = 'openai:recover-credit-grant';

    public function __construct(private ChatGPTService $chatGPTService, private EntityManagerInterface $entityManager, private OpenAIRepository $openAIRepository)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Recover credit grant from OpenAI')
            ->setHelp('This command allows you to recover credit grant from OpenAI');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            '',
            '===========================',
            'OpenAI Recover Credit Grant',
            '===========================',
            '',
        ]);

        $openai = $this->openAIRepository->findAll();

        if (!count($openai)) {
            return COMMAND::SUCCESS;
        }

        $openai = $openai[0];
        $this->chatGPTService->getCreditGrants($openai->getSessionId());

        return Command::SUCCESS;
    }
}
