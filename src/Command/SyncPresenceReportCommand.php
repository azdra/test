<?php

namespace App\Command;

use App\Repository\ClientRepository;
use App\Repository\ProviderSessionRepository;
use App\Service\Provider\Common\PresenceReportSynchronizeFactory;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SyncPresenceReportCommand extends Command
{
    protected static $defaultName = 'service:session:presence-report:synchronize';

    public function __construct(
        private ProviderSessionRepository $sessionRepository,
        private ClientRepository $clientRepository,
        private PresenceReportSynchronizeFactory $presenceReportSynchronize,
        private EntityManagerInterface $entityManager,
        private LoggerInterface $cronLogger
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('session', 's', InputOption::VALUE_OPTIONAL, 'Session ID');
        $this->addOption('dateStart', 't', InputOption::VALUE_OPTIONAL, 'Date Start for the report');
        $this->addOption('dateEnd', 'd', InputOption::VALUE_OPTIONAL, 'Date End for the report');
        $this->addOption('client', 'c', InputOption::VALUE_OPTIONAL, 'Client ID');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $sessionId = $input->getOption('session');
        $dateStartInput = $input->getOption('dateStart');
        $dateEndInput = $input->getOption('dateEnd');
        $clientId = $input->getOption('client');

        $dateStart = $dateStartInput ? \DateTime::createFromFormat('d/m/Y', $dateStartInput) : null;
        $dateEnd = $dateEndInput ? \DateTime::createFromFormat('d/m/Y', $dateEndInput) : null;

        $client = $clientId ? $this->clientRepository->findOneBy(['id' => $clientId]) : null;

        if ($dateStart && $dateEnd && $clientId) {
            $sessions = $this->sessionRepository->findSessionWithSeveralAnimatorsForPeriodAndClient($dateStart, $dateEnd, $client);
        } elseif ($dateStart && $dateEnd) {
            $sessions = $this->sessionRepository->findSessionForPeriod($dateStart, $dateEnd);
        } elseif (!empty($sessionId)) {
            $sessions = [$this->sessionRepository->find($sessionId)];
        } else {
            $sessions = $this->sessionRepository->findSessionToFetchReportPresence();
        }

        $io->info(count($sessions).' sessions to sync');

        foreach ($sessions as $session) {
            try {
                $io->info('Syncing session '.$session->getName());
                $this->presenceReportSynchronize->synchronizePresenceReport($session);
            } catch (\Throwable $t) {
                $this->cronLogger->error($t->getMessage(), ['session_id' => $session->getId()]);
                $io->error('Error syncing session: '.$t->getMessage());
            }
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
