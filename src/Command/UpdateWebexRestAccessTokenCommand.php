<?php

namespace App\Command;

    use App\Repository\WebexRestMeetingConfigurationHolderRepository;
    use App\Service\WebexRestMeetingService;
    use Doctrine\ORM\EntityManagerInterface;
    use Psr\Log\LoggerInterface;
    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;

    class UpdateWebexRestAccessTokenCommand extends Command
    {
        protected static $defaultName = 'app:update-webex-rest-access-token';

        public function __construct(
            private WebexRestMeetingService $webexRestMeetingService,
            private LoggerInterface $cronLogger,
            private EntityManagerInterface $entityManager,
            private WebexRestMeetingConfigurationHolderRepository $webexRestMeetingConfigurationHolderRepository,
        ) {
            parent::__construct();
        }

        protected function execute(InputInterface $input, OutputInterface $output): int
        {
            $output->writeln('<info>Start update access token</info>');
            $allWebexRestMeetingConfigurationHolder = $this->webexRestMeetingConfigurationHolderRepository->getRemindExpireAccessToken();
            $output->writeln('<info>we have '.count($allWebexRestMeetingConfigurationHolder).' WebexRestMeetingConfigurationHolder</info>');

            foreach ($allWebexRestMeetingConfigurationHolder as $webexRestMeetingConfigurationHolder) {
                try {
                    $output->writeln('<info>Updating access token for '.$webexRestMeetingConfigurationHolder->getSiteName().' | Name: '.$webexRestMeetingConfigurationHolder->getName().' for the client '.$webexRestMeetingConfigurationHolder->getClient()->getName().'</info>');
                    $this->webexRestMeetingService->updateAccessToken($webexRestMeetingConfigurationHolder);
                    $output->writeln('<info>Access token updated for '.$webexRestMeetingConfigurationHolder->getName().' successfully</info>');
                    $this->entityManager->flush();
                } catch (\Throwable $throwable) {
                    $output->writeln('<error>error update access token</error>');
                    $this->cronLogger->error($throwable, ['error' => $throwable->getMessage()]);
                }
            }
            $output->writeln('<info>End update access token</info>');

            return Command::SUCCESS;
        }
    }
