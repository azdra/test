<?php

namespace App\Command;

use App\Entity\ActivityLog;
use App\Entity\CategorySessionType;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ClientRepository;
use App\Repository\UserRepository;
use App\Security\UserSecurityService;
use App\Service\ActivityLogger;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\ProviderSessionService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Routing\RouterInterface;

class CheckChargeTestCommand extends Command
{
    //bin/console check:charge-test 2 1 'Live Session 2'
    protected static $defaultName = 'check:charge-test';

    public function __construct(
        protected ActivityLogger $activityLogger,
        protected ClientRepository $clientRepository,
        protected EntityManagerInterface $entityManager,
        protected LoggerInterface $logger,
        protected ParticipantSessionSubscriber $participantSessionSubscriber,
        protected ProviderSessionService $providerSessionService,
        protected RouterInterface $router,
        protected UserRepository $userRepository,
        private string $endPointDomain,
        protected UserSecurityService $userSecurityService,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('nbrsessions', InputArgument::REQUIRED, 'Set the number sessions')
            ->addArgument('nbrtrainees', InputArgument::REQUIRED, 'Set the number trainees by session')
            ->addArgument('clientname', InputArgument::REQUIRED, 'Set the name account client');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $output->writeln("\t> ------Début du traitement-------------");
        $executionStartTime = microtime(true);

        $client = $this->getClient($input->getArgument('clientname'));
        $output->writeln("\t> -------------| ".$client->getName().' |-------------- ');
        for ($iSession = 0; $iSession < $input->getArgument('nbrsessions'); ++$iSession) {
            $session = $this->createMeeting($client);
            $output->writeln("\t> ----------------| ".$session->getName().' |----------------- ');
            for ($iTrainee = 0; $iTrainee < $input->getArgument('nbrtrainees'); ++$iTrainee) {
                $sessionWithTrainees = $this->addTrainee($session, 'tests.ingenierie+test'.$iTrainee.'@mylivesession.com');
                $output->writeln("\t> -------------| 'tests.ingenierie+test'.$iTrainee.'@mylivesession.com' |-------------- ");
            }
            $output->writeln("\t> ------------------------------------------------------------------------------- ");
        }

        $executionEndTime = microtime(true);
        $seconds = $executionEndTime - $executionStartTime;
        $output->writeln("\t> ------Durée après chargement-------------".round($seconds, 3).' secs---|---------------------');

        $io->success('All done !');

        return Command::SUCCESS;
    }

    private function getClient(string $clientname): Client
    {
        /** @var Client $client */
        $client = $this->clientRepository->findOneBy(['name' => $clientname]);

        return $client;
    }

    private function addTrainee(ProviderSession $providerSession, string $emailtrainee): ?ProviderSession
    {
        $client = $providerSession->getClient();

        $userClient = $this->userRepository->findOneBy(['email' => $emailtrainee, 'client' => $client]);
        $tempTrainee = str_replace('@mylivesession.com', '', $emailtrainee);
        $parts = explode('+', $tempTrainee);

        if (empty($userClient)) { //Si le participant n'existe pas et que l'utilisateur ne fait pas partie du client
            $trainee = new User();
            $trainee->setClient($client)
                ->setFirstName($parts[0])
                ->setLastName($parts[1])
                ->setClearPassword($this->userSecurityService->randomPassword())
                ->setEmail($emailtrainee)
                ->setCompany($client->getName())
                ->setRoles(['ROLE_TRAINEE'])
                ->setPreferredLang('fr');
            $this->entityManager->persist($trainee);
            $this->entityManager->flush();
        } else { //Si le participant n'est pas associé à la session et que l'utilisateur fait partie du client
            $trainee = $userClient;
        }
        $ppsr = $this->participantSessionSubscriber->subscribe($providerSession, $trainee, ProviderParticipantSessionRole::ROLE_TRAINEE);
        $this->entityManager->flush();

        return $providerSession;
    }

    private function createMeeting(Client $client): ?ProviderSession
    {
        $this->activityLogger->initActivityLogContext($client, null, ActivityLog::ORIGIN_CRON);

        $convocationObj = $client->getConvocations()->first();
        $dateStart = ( new \DateTime() )->modify('+1 day')->setTime(1, 0);
        $dateEnd = ( new \DateTime() )->modify('+1 day')->setTime(1, 30);
        $data = [
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
            'url' => '',
            'model' => '',
            'reference' => ProviderSessionService::generateSessionReference($client->getName()),
            'lang' => 'fr',
            'businessNumber' => '',
            'information' => '',
            'tags' => [],
            'personalizedContent' => '',
            'category' => (CategorySessionType::CATEGORY_SESSION_FORMATION)->value,
            'automaticSendingTrainingCertificates' => '',
            'sessionTest' => '',
            'convocationObj' => $convocationObj, //'Convocation Renault VOIP',
            'title' => $this->generateRandomString().$dateStart->format('dmY'),
            'defaultModelId' => null,
            'description' => '',
            'description2' => '',
            'nature' => null,
            'objectives' => null,
            'accessType' => AdobeConnectConnector::GRANT_ACCESS_USER_ONLY,
            'replayVideoPlatform' => null,
            'replayUrlKey' => null,
            'publishedReplay' => false,
            'accessParticipantReplay' => false,
            'replayLayoutPlayButton' => false,
        ];

        /** @var AbstractProviderConfigurationHolder|AdobeConnectConfigurationHolder $configurationHolder */
        $configurationHolder = $client->getConfigurationHolders()[0];
        $session = $this->providerSessionService->createSessionFromArray($configurationHolder, null, $data);

        $this->entityManager->flush();

        $infos = [
            'message' => ['key' => 'Creating the  session  <a href="%route%">%sessionName%</a>', 'params' => ['%route%' => $this->endPointDomain.$this->router->generate('_session_get', ['id' => $session->getId()]), '%sessionName%' => $session->getName()]],
            'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
        ];

        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SESSION_CREATION, ActivityLog::SEVERITY_INFORMATION, $infos), true);
        $this->activityLogger->flushActivityLogs();

        return $session;
    }

    private function generateRandomString(): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 10; ++$i) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
