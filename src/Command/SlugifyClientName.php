<?php

namespace App\Command;

use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class SlugifyClientName extends Command
{
    protected static $defaultName = 'app:slugify-client-name';

    public function __construct(private ClientRepository $clientRepository, private EntityManagerInterface $entityManager, private SluggerInterface $slugger)
    {
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $clients = $this->clientRepository->findAll();

        foreach ($clients as $client) {
            $client->setSlug($this->slugger->slug($client->getName())->lower());
            $this->entityManager->persist($client);
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
