<?php

namespace App\Command;

use App\Entity\ActivityLog;
use App\Entity\Client\Client;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ClientRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

class AlertSeveralAnimatorsOnSameSessionCommand extends Command
{
    protected static $defaultName = 'alert:several-animators-on-same-session';

    private ClientRepository $clientRepository;

    private ProviderSessionRepository $providerSessionRepository;

    private UserRepository $userRepository;

    private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository;

    private EntityManagerInterface $entityManager;

    private EventDispatcherInterface $eventDispatcher;

    private ActivityLogger $activityLogger;

    private MailerService $mailerService;

    private LoggerInterface $logger;

    private TranslatorInterface $translator;

    private RouterInterface $router;

    public function __construct(
        ClientRepository $clientRepository,
        LoggerInterface $cronLogger,
        ProviderSessionRepository $providerSessionRepository,
        UserRepository $userRepository,
        ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        ActivityLogger $activityLogger,
        MailerService $mailerService,
        TranslatorInterface $translator,
        RouterInterface $router
    ) {
        parent::__construct();

        $this->clientRepository = $clientRepository;
        $this->providerSessionRepository = $providerSessionRepository;
        $this->userRepository = $userRepository;
        $this->logger = $cronLogger;
        $this->providerParticipantSessionRoleRepository = $providerParticipantSessionRoleRepository;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->activityLogger = $activityLogger;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->router = $router;
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $from = ( new \DateTime() )->modify('+1 day')->setTime(0, 0);
        $to = ( new \DateTime() )->modify('+7 day')->setTime(23, 59);

        try {
            $clients = $this->clientRepository->findAll();

            foreach ($clients as $client) {
                if ($client->isSubscribeToAlert(Client::NOTIFICATION_SEVERAL_ANIMATORS_ON_SAME_SESSION)) {
                    $minusDays = $client->getOptionFromAlert(Client::NOTIFICATION_SEVERAL_ANIMATORS_ON_SAME_SESSION_OPTION_TIME_BEFORE);

                    $sessions = $this->providerSessionRepository->findSessionWithSeveralAnimatorsForPeriodAndClient($from, $to, $client);
                    $dDay = ( new \DateTime() );
                    foreach ($sessions as $session) {
                        $targetDay = (clone $session->getDateStart())->modify('-'.$minusDays.' days')->setTime(0, 0);
                        if ($targetDay < $session->getDateStart() && $dDay < $session->getDateStart() && $dDay > $targetDay && $session->getAnimatorsCount() > 1) {
                            $infos = [
                                'message' => ['key' => 'Session <a href="%route%">%sessionName%</a> has %count% animators', 'params' => ['%route%' => $this->router->generate('_session_get', ['id' => $session->getId()]), '%sessionName%' => $session->getName(), '%count%' => $session->getAnimatorsCount()]],
                                'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                            ];
                            $this->activityLogger->initActivityLogContext($client, null, ActivityLog::ORIGIN_CRON);
                            $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_ALERT_SEVERAL_ANIMATORS_ON_SAME_SESSION, ActivityLog::SEVERITY_WARNING, $infos), true);
                            $this->activityLogger->flushActivityLogs();

                            $this->sendEmailToUsersManager($session);
                        }
                    }
                }
            }

            $io->success('All done !');

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->logger->error($exception, ['from' => $from->format('Y-m-d_H\hi'), 'to' => $to->format('Y-m-d_H\hi')]);
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }
    }

    protected function sendEmailToUsersManager(ProviderSession $session): void
    {
        $userManagers = $this->userRepository->findManagerListByClient($session->getClient());
        foreach ($userManagers as $user) {
            if ($user->isSubscribeToAlert(User::EMAIL_SEVERAL_ANIMATORS_ON_SAME_SESSION)) {
                $sendGo = false;
                if ($user->isSubscribeToAlert(User::EMAIL_ONLY_FOR_MY_SESSIONS)) {
                    if ((!empty($session->getCreatedBy()) && $session->getCreatedBy() == $user) || (!empty($session->getUpdatedBy()) && $session->getUpdatedBy() == $user)) {
                        $sendGo = true;
                    }
                } else {
                    $sendGo = true;
                }
                if ($sendGo) {
                    $this->mailerService->sendTemplatedMail(
                        $user->getEmail(),
                        $this->translator->trans('Several animators on the same session'),
                        'emails/alert/several_animators_on_same_session.html.twig',
                        context: [
                            'session' => $session,
                            'client' => $session->getClient(),
                            'route' => $this->router->generate('_session_get', ['id' => $session->getId()], 0),
                            'user' => $user,
                        ],
                        metadatas: [
                            'origin' => MailerService::ORIGIN_EMAIL_SEVERAL_ANIMATORS_ON_SAME_SESSION,
                            'user' => $user->getId(),
                            'session' => $session->getId(),
                        ],
                        senderSystem: true
                    );
                }
            }
        }
    }
}
