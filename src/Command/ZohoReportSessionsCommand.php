<?php

namespace App\Command;

use App\Repository\ProviderSessionRepository;
use App\Service\Exporter\ExporterService;
use App\Service\Exporter\ExportFormat;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

//0 22 * * * www-data flock -w 10 /tmp/zoho_report_sessions -c "cd /srv; php bin/console.php zoho:report:sessions >> /logs/zoho_report_sessions.log 2>&1"
class ZohoReportSessionsCommand extends Command
{
    protected static $defaultName = 'zoho:report:sessions';

    public function __construct(
        private LoggerInterface $cronLogger,
        private ProviderSessionRepository $providerSessionRepository,
        private ExporterService $exporterService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '3600');

        try {
            $anneeCourante = date('Y'); // Obtient l'année en cours
            $anneePrecedente = $anneeCourante - 1; // Calcule l'année précédente
            $ds = new \DateTimeImmutable(date('Y-m-d H:i:s', strtotime("{$anneePrecedente}-01-01 00:00:00")));
            $de = new \DateTimeImmutable(date('Y-m-d H:i:s', strtotime("{$anneeCourante}-12-31 23:59:59")));

            $sessions = $this->providerSessionRepository->findByDateBetween($ds, $de);
            $output->writeln("\t >> Nbr sessions = ".count($sessions));

            $return = $this->exporterService->generateArchive('sessions_export_zoho', ExportFormat::XLSX, $sessions, false, false);

            $output->writeln("\t >> All done");

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->cronLogger->error($exception);
            $output->writeln('Unknown error: '.$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
