<?php

namespace App\Command\DataMigration;

use App\CustomerService\Service\HelpNeedService;
use App\Entity\ProviderSession;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class ServiceNeedCommand extends Command
{
    protected static $defaultName = 'date-migration:service_need';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private HelpNeedService $serviceNeedService
    ) {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $sessions = $this->entityManager->createQueryBuilder()
                ->select('session')
                ->from(ProviderSession::class, 'session')
                ->where('session.assistanceType != :no_assistance')
                ->orWhere('session.supportType != :no_support')
                ->setParameter('no_assistance', ProviderSession::ENUM_NO)
                ->setParameter('no_support', ProviderSession::ENUM_SUPPORT_NO)
                ->getQuery()
                ->getResult()
            ;

            /** @var ProviderSession $session */
            foreach ($sessions as $session) {
                $this->serviceNeedService->synchronizeSessionHelpNeed($session);
            }

            $this->entityManager->flush();

            $io->success('All done !');

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $io->error('Unknown error: '.$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
