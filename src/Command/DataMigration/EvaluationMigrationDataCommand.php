<?php

namespace App\Command\DataMigration;

use App\Entity\Evaluation;
use App\Entity\EvaluationProviderSession;
use App\Entity\EvaluationSessionParticipantAnswer;
use App\Entity\ProviderSession;
use App\Repository\EvaluationParticipantAnswerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class EvaluationMigrationDataCommand extends Command
{
    protected static $defaultName = 'date-migration:evaluation_new_structure';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private EvaluationParticipantAnswerRepository $evaluationParticipantAnswerRepository
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Début de la migration des données d\'évaluation');

        try {
            $evaluationsParticipantAnswer = $this->evaluationParticipantAnswerRepository->findAll();
            $total = count($evaluationsParticipantAnswer);
            $io->note('Nombre total de réponses des participants à traiter: '.$total);

            // Initialisation de la ProgressBar
            $progressBar = $io->createProgressBar($total);
            $progressBar->start();

            foreach ($evaluationsParticipantAnswer as $index => $evaluationParticipantAnswer) {
                $participantProviderSession = $evaluationParticipantAnswer->getProviderParticipantSessionRole();
                $evaluationProviderSession = $this->getEvaluationProviderSessionForMigrationData($participantProviderSession->getSession(), $evaluationParticipantAnswer->getEvaluation());
                $evaluationSessionParticipantAnswer = (new EvaluationSessionParticipantAnswer($evaluationProviderSession, $participantProviderSession))
                    ->setReplyDate($evaluationParticipantAnswer->getReplyDate())
                    ->setAnswer($evaluationParticipantAnswer->getAnswer());

                $this->entityManager->persist($evaluationSessionParticipantAnswer);
                if (($index + 1) % 100 === 0) {
                    $this->entityManager->flush();
                    $progressBar->advance(10); // Avance la barre de progression
                }
            }

            $this->entityManager->flush();
            $progressBar->finish(); // Termine la ProgressBar
            $io->success('Migration terminée avec succès !');

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $io->error('Erreur inconnue : '.$exception->getMessage());

            return Command::FAILURE;
        }
    }

    /**
     * Obtient la session d'évaluation du fournisseur pour la migration de données.
     *
     * @param ProviderSession $providerSession la session du fournisseur
     * @param evaluation      $evaluation      L'évaluation concernée
     *
     * @return EvaluationProviderSession|null la session d'évaluation correspondante, ou null si aucune correspondance n'est trouvée
     */
    public function getEvaluationProviderSessionForMigrationData(ProviderSession $providerSession, Evaluation $evaluation): ?EvaluationProviderSession
    {
        $evaluationsProviderSession = $providerSession->getEvaluationsProviderSession();

        foreach ($evaluationsProviderSession as $evaluationProviderSession) {
            if ($evaluationProviderSession->getEvaluation() === $evaluation) {
                return $evaluationProviderSession;
            }
        }

        // Retourne null si aucune evaluation correspondante n'est trouvée
        return null;
    }
}
