<?php

namespace App\Command\DataMigration;

use App\Entity\CategorySessionType;
use App\Entity\ProviderSession;
use App\Repository\ProviderSessionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class PresenceUpdatePercentageCommand extends Command
{
    protected static $defaultName = 'update:presence_percentage_provider_session';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger,
        private ProviderSessionRepository $providerSessionRepository,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Met à jour le pourcentage de présence des sessions de fournisseur en fonction de la durée minimale spécifique à chaque type de session.')
            ->addOption('session_id', 's', InputOption::VALUE_REQUIRED, 'ID de la session à mettre à jour', null);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $sessionId = $input->getOption('session_id');

        $io->title('Modification des données de pourcentage de présence des sessions de formation');

        try {
            if ($sessionId) {
                $session = $this->providerSessionRepository->findOneBy(['id' => $sessionId]);
                if (!$session) {
                    $io->error("Aucune session trouvée avec l'ID $sessionId");

                    return Command::FAILURE;
                }
                $this->processSession(session: $session);
                $io->success("Mise à jour du pourcentage de présence pour la session ID $sessionId réussie !");
            } else {
                $dateFrom = new \DateTimeImmutable('2020-01-01 00:00:00');
                $dateTo = new \DateTimeImmutable('2024-03-04 23:59:59');
                $sessions = $this->providerSessionRepository->findAllBetweenDates($dateFrom, $dateTo);
                $total = count($sessions);
                $io->note("Nombre total de sessions à traiter: $total");

                $progressBar = $io->createProgressBar($total);
                $progressBar->start();

                foreach ($sessions as $session) {
                    $this->processSession(session: $session);
                    $progressBar->advance();
                }

                $progressBar->finish();
                $io->success('Migration réussie pour toutes les sessions !');
            }

            $this->entityManager->flush();

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $io->error("Erreur inconnue : {$exception->getMessage()}");

            return Command::FAILURE;
        }
    }

    private function processSession(ProviderSession $session): void
    {
        $clientName = $session->getClient()->getName();
        $category = $session->getCategory();

        $durationMinimal = 15;

        if ($clientName === 'Renault') {
            $durationMinimal = $category === CategorySessionType::CATEGORY_SESSION_WEBINAR ? 25 : 90;
        }

        $this->updateSessionAttendancePercentage(session: $session, durationMinimal: $durationMinimal);
    }

    private function updateSessionAttendancePercentage(ProviderSession $session, int $durationMinimal): void
    {
        $duration = $session->getDuration();
        if ($duration <= 0) {
            $this->logger->error(sprintf('Durée invalide (ID %s): %d', $session->getId(), $duration));
            $session->setPercentageAttendance(percentageAttendance: 0);
        } else {
            $percentage = round(($durationMinimal / $duration) * 100);
            $session->setPercentageAttendance(percentageAttendance: $percentage);
        }
        $this->entityManager->persist($session);
    }
}
