<?php

namespace App\Command;

use App\Entity\DeferredActionsTask;
use App\Entity\ProviderSession;
use App\Repository\DeferredActionsTaskRepository;
use App\Repository\EvaluationParticipantAnswerRepository;
use App\Repository\ProviderSessionRepository;
use App\Service\DeferredActionsTaskService;
use App\Service\Exporter\ExporterService;
use App\Service\Exporter\ExportFormat;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final class DeferredActionsTaskCommand extends Command implements ContainerAwareInterface
{
    public const MAX_PROCESSING_TASK = 5;
    public const WAITING_TIME_SECONDS = 10;

    protected static $defaultName = 'deferred:actions:task';

    private DeferredActionsTaskRepository $deferredActionsTaskRepository;

    private ProviderSessionRepository $providerSessionRepository;

    private EntityManagerInterface $entityManager;

    private DeferredActionsTaskService $deferredActionsTaskService;

    private ExporterService $exporterService;

    private MailerService $mailerService;

    private TranslatorInterface $translator;

    private LoggerInterface $logger;

    private ?ContainerInterface $container;

    private LockFactory $lockFactory;

    private RouterInterface $router;
    private EvaluationParticipantAnswerRepository $answerRepository;

    public function __construct(
        DeferredActionsTaskRepository $deferredActionsTaskRepository,
        ProviderSessionRepository $providerSessionRepository,
        EntityManagerInterface $entityManager,
        DeferredActionsTaskService $deferredActionsTaskService,
        ExporterService $exporterService,
        MailerService $mailerService,
        TranslatorInterface $translator,
        LoggerInterface $deferredActionsTaskLogger,
        LockFactory $lockFactory,
        RouterInterface $router,
        EvaluationParticipantAnswerRepository $answerRepository
    ) {
        parent::__construct();
        $this->deferredActionsTaskRepository = $deferredActionsTaskRepository;
        $this->providerSessionRepository = $providerSessionRepository;
        $this->entityManager = $entityManager;
        $this->deferredActionsTaskService = $deferredActionsTaskService;
        $this->exporterService = $exporterService;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->logger = $deferredActionsTaskLogger;
        $this->lockFactory = $lockFactory;
        $this->router = $router;
        $this->answerRepository = $answerRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $tasksToDo = $this->deferredActionsTaskRepository->getNextTasksNotDoneAndNotFailed();
        if (empty($tasksToDo)) {
            $output->writeln('No new deferred action task to do');
        } else {
            $output->writeln(count($tasksToDo).' deferred action task.s to do');
            $count = 0;
            foreach ($tasksToDo as $task) {
                ++$count;
                $max = self::MAX_PROCESSING_TASK;
                $message = "$count/$max to processing";
                $output->writeln($message);
                $this->logger->info($message);
                $this->executeDeferredActionsTask($output, $task);
            }
        }

        $this->entityManager->clear();

        return $this->endWorker($output);
    }

    private function executeDeferredActionsTask(OutputInterface $output, DeferredActionsTask $taskToDo): void
    {
        try {
            if ($taskToDo->getStatus() == DeferredActionsTask::STATUS_PROCESSING) {
                $output->writeln('The deferred action task with #ID '.$taskToDo->getId().' is in progress!');
                $this->logger->info('The deferred action task with #ID '.$taskToDo->getId().' is in progress!');
            } else {
                $taskToDo->setStatus(DeferredActionsTask::STATUS_PROCESSING);
                $taskToDo->setProcessingStartTime(new \DateTime());
                $this->entityManager->persist($taskToDo);
                $this->entityManager->flush();

                $output->writeln('The deferred action task with #ID '.$taskToDo->getId().' is launched!');
                $this->logger->info('The deferred action task with #ID '.$taskToDo->getId().' is launched!');

                $from = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s.u', $taskToDo->getData()['from']['date']);
                $to = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s.u', $taskToDo->getData()['to']['date']);

                $type = 'multi_evaluations_report';
                $format = ExportFormat::XLSX;
                $grouping = $taskToDo->getData()['grouping'];
                $crypt = false;

                $sessions = $this->providerSessionRepository->findAllBetweenDatesForResultEvaluations($from, $to);

                /** @var ProviderSession $session */
                foreach ($sessions as $key => $session) {
                    if (count($session->getEvaluationsProviderSession()) === 0 || !$session->isFinished() || $session->isSessionTest()) {
                        unset($sessions[$key]);
                    }
                }
                if (count($sessions) == 0) {
                    $output->writeln('No sessions to do for the deferred action task with #ID '.$taskToDo->getId().'!');
                    $this->logger->info('No sessions to do for the deferred action task with #ID '.$taskToDo->getId().'!');
                    $taskToDo->setStatus(DeferredActionsTask::STATUS_DONE);
                    $infos = [
                        'type' => 'XLSX',
                        'fileName' => '',
                        'generatedAt' => new \DateTime(),
                    ];
                    $taskToDo->setInfos($infos);
                    $taskToDo->setResult(['Success' => ['message' => 'No data']]);
                    $this->exporterService->generateActivityLogDeferredActionsTaskExportEvaluations(user: $taskToDo->getUserOrigin(), status: 'NO DATA', deferredActionsTask: $taskToDo);
                    $this->sendEmailNodata($taskToDo, $infos['type'], $from, $to);
                } else {
                    $output->writeln(count($sessions).' session.s done for the deferred action task with #ID '.$taskToDo->getId().'!');
                    $this->logger->info(count($sessions).' session.s done for the deferred action task with #ID '.$taskToDo->getId().'!');
                    $return = $this->exporterService->generateArchive($type, $format, $sessions, $grouping, $crypt);
                    $infos = [
                        'type' => 'XLSX',
                        'fileName' => '',
                        'generatedAt' => new \DateTime(),
                    ];
                    if ($return->getStatusCode() === Response::HTTP_OK) {
                        $data = json_decode($return->getContent(), true);
                        $message = explode('/', $data['message']);
                        $infos['fileName'] = $message[4];
                        $taskToDo->setStatus(DeferredActionsTask::STATUS_DONE);
                        $this->sendEmailDone($taskToDo, $infos['fileName'], $infos['type'], $from, $to);

                        $this->exporterService->generateActivityLogDeferredActionsTaskExportEvaluations(
                            user: $taskToDo->getUserOrigin(), status: DeferredActionsTask::STATUS_DONE,
                            deferredActionsTask: $taskToDo, filename: $infos['fileName']
                        );
                    } else {
                        $taskToDo->setResult(['Error' => ['message' => $return->getStatusCode()]]);
                        $taskToDo->setStatus(DeferredActionsTask::STATUS_FAILED);
                        $this->exporterService->generateActivityLogDeferredActionsTaskExportEvaluations(user: $taskToDo->getUserOrigin(), status: DeferredActionsTask::STATUS_FAILED, deferredActionsTask: $taskToDo);
                        $this->sendEmailFailed($taskToDo, $return->getStatusCode(), $infos['type'], $from, $to);
                    }
                    $taskToDo->setInfos($infos);
                    $taskToDo->setResult(['Success' => ['message' => 'OK']]);
                }
                $taskToDo->setProcessingEndTime(new \DateTime('now'));
                $this->entityManager->persist($taskToDo);
                $this->entityManager->flush();
            }
        } catch (\Throwable $exception) {
            $this->logger->error($exception);
            $taskToDo->setResult(['Error' => ['message' => $exception->getMessage()]]);
            $taskToDo->setStatus(DeferredActionsTask::STATUS_FAILED);
            $taskToDo->setProcessingEndTime(new \DateTime('now'));
            $this->entityManager->persist($taskToDo);
            $this->entityManager->flush();
            $this->exporterService->generateActivityLogDeferredActionsTaskExportEvaluations(user: $taskToDo->getUserOrigin(), status: DeferredActionsTask::STATUS_FAILED, deferredActionsTask: $taskToDo);
            $this->sendEmailFailed($taskToDo, $exception->getMessage(), null, null, null);
        }
    }

    private function sendEmailDone(DeferredActionsTask $taskToDo, string $filename, string $type, \DateTimeImmutable $from, \DateTimeImmutable $to): void
    {
        $this->mailerService->sendTemplatedMail(
            $taskToDo->getUserOrigin()->getEmail(),
            $this->translator->trans('Evaluations export'),
            'emails/alert/deferred_actions_task_send.html.twig',
            context: [
                'lang' => $taskToDo->getUserOrigin()->getPreferredLang(),
                'linkFile' => $this->router->generate('_download_exports_direct', ['file' => $filename], 0),
                'fileFilterFrom' => $from->format('d/m/Y H:i'),
                'fileFilterTo' => $to->format('d/m/Y H:i'),
                'fileFormat' => $type,
                'user' => $taskToDo->getUserOrigin(),
                'client' => $taskToDo->getUserOrigin()->getClient(),
            ],
            metadatas: [
                'origin' => MailerService::ORIGIN_EMAIL_DEFERRED_ACTIONS_TASK,
                'user' => $taskToDo->getUserOrigin()->getId(),
            ],
            senderSystem: true
        );
    }

    private function sendEmailFailed(DeferredActionsTask $taskToDo, string $error, ?string $type = null, ?\DateTimeImmutable $from = null, ?\DateTimeImmutable $to = null): void
    {
        $this->mailerService->sendTemplatedMail(
            $taskToDo->getUserOrigin()->getEmail(),
            $this->translator->trans('Evaluations export'),
            'emails/alert/deferred_actions_task_send_failed.html.twig',
            context: [
                'lang' => $taskToDo->getUserOrigin()->getPreferredLang(),
                'error' => $error, //$return->getStatusCode(),
                'user' => $taskToDo->getUserOrigin(),
                'client' => $taskToDo->getUserOrigin()->getClient(),
                'fileFilterFrom' => (empty($from)) ? null : $from->format('d/m/Y H:i'),
                'fileFilterTo' => (empty($to)) ? null : $to->format('d/m/Y H:i'),
                'fileFormat' => $type,
            ],
            metadatas: [
                'origin' => MailerService::ORIGIN_EMAIL_DEFERRED_ACTIONS_TASK,
                'user' => $taskToDo->getUserOrigin()->getId(),
            ],
            senderSystem: true
        );
    }

    private function sendEmailNodata(DeferredActionsTask $taskToDo, string $type, \DateTimeImmutable $from, \DateTimeImmutable $to): void
    {
        $this->mailerService->sendTemplatedMail(
            $taskToDo->getUserOrigin()->getEmail(),
            $this->translator->trans('Evaluations export'),
            'emails/alert/deferred_actions_task_send_nodata.html.twig',
            context: [
                'lang' => $taskToDo->getUserOrigin()->getPreferredLang(),
                'user' => $taskToDo->getUserOrigin(),
                'client' => $taskToDo->getUserOrigin()->getClient(),
                'fileFilterFrom' => $from->format('d/m/Y H:i'),
                'fileFilterTo' => $to->format('d/m/Y H:i'),
                'fileFormat' => $type,
            ],
            metadatas: [
                'origin' => MailerService::ORIGIN_EMAIL_DEFERRED_ACTIONS_TASK,
                'user' => $taskToDo->getUserOrigin()->getId(),
            ],
            senderSystem: true
        );
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    private function getWaitingTimeInMicroSecondes(): int
    {
        return self::WAITING_TIME_SECONDS * 1000 * 1000;
    }

    private function endWorker(OutputInterface $output): int
    {
        $waitingTime = self::WAITING_TIME_SECONDS;

        $output->writeln("Exit. Waiting $waitingTime seconde");
        usleep($this->getWaitingTimeInMicroSecondes());

        return Command::SUCCESS;
    }
}
