<?php

namespace App\Command;

use App\Entity\ActivityLog;
use App\Entity\Client\Client;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

class AlertParticipantOnSeveralSessionsCommand extends Command
{
    protected static $defaultName = 'alert:participant-on-several-sessions';

    private ProviderSessionRepository $providerSessionRepository;

    private UserRepository $userRepository;

    private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository;

    private EntityManagerInterface $entityManager;

    private EventDispatcherInterface $eventDispatcher;

    private ActivityLogger $activityLogger;

    private MailerService $mailerService;

    private TranslatorInterface $translator;

    private LoggerInterface $logger;

    private RouterInterface $router;

    public function __construct(
        ProviderSessionRepository $providerSessionRepository,
        UserRepository $userRepository,
        ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        ActivityLogger $activityLogger,
        LoggerInterface $cronLogger,
        MailerService $mailerService,
        TranslatorInterface $translator,
        RouterInterface $router
    ) {
        parent::__construct();

        $this->providerSessionRepository = $providerSessionRepository;
        $this->userRepository = $userRepository;
        $this->providerParticipantSessionRoleRepository = $providerParticipantSessionRoleRepository;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->activityLogger = $activityLogger;
        $this->logger = $cronLogger;
        $this->mailerService = $mailerService;
        $this->translator = $translator;
        $this->router = $router;
    }

    protected function configure(): void
    {
        $this->addArgument('role', InputArgument::REQUIRED, 'PPSR Role');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $role = $input->getArgument('role');

            $ppsr = $this->providerParticipantSessionRoleRepository->findParticipantOnSeveralSession($role);

            $currentPpsrId = 0;
            foreach ($ppsr as $participant) {
                if ($participant->getParticipant()->getId() != $currentPpsrId) {
                    if ($role == ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                        $clientNotification = Client::NOTIFICATION_ANIMATOR_ON_SEVERAL_SESSIONS;
                        $action = ActivityLog::ACTION_ALERT_ANIMATOR_ON_SEVERAL_SESSIONS;
                    } else {
                        $clientNotification = Client::NOTIFICATION_TRAINEE_ON_SEVERAL_SESSIONS;
                        $action = ActivityLog::ACTION_ALERT_TRAINEE_ON_SEVERAL_SESSIONS;
                    }

                    if ($participant->getSession()->getClient()->isSubscribeToAlert($clientNotification)) {
                        $infos = [
                            'message' => [
                                'key' => 'The '.$role.' "%participant_name%" of the session "<a href="%routeSession%">%session_name%</a>" is already present for this same time slot on another session. Consider rescheduling one of the sessions.',
                                'params' => [
                                    '%routeSession%' => $this->router->generate('_session_get', ['id' => $participant->getSession()->getId()]),
                                    '%participant_name%' => $participant->getParticipant()->getUser()->getFirstName().' '.$participant->getParticipant()->getUser()->getLastName(),
                                    '%session_name%' => $participant->getSession()->getName(),
                                ],
                            ],
                            'participant' => [
                                'id' => $participant->getParticipant()->getUser()->getId(),
                                'name' => $participant->getParticipant()->getUser()->getFirstName().' '.$participant->getParticipant()->getUser()->getLastName(),
                                'email' => $participant->getParticipant()->getUser()->getEmail(),
                            ],
                            'session' => [
                                'id' => $participant->getSession()->getId(),
                                'name' => $participant->getSession()->getName(),
                                'ref' => $participant->getSession()->getRef(),
                            ],
                        ];
                        $this->activityLogger->initActivityLogContext($participant->getSession()->getClient(), null, ActivityLog::ORIGIN_CRON);
                        $this->activityLogger->addActivityLog(new ActivityLogModel($action, ActivityLog::SEVERITY_WARNING, $infos), true);
                        $this->activityLogger->flushActivityLogs();

                        $this->sendEmailToUsersManager($participant, $role);
                    }

                    $currentPpsrId = $participant->getParticipant()->getId();
                }
            }

            $io->success('All done !');

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->logger->error($exception, ['role' => $role]);
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }
    }

    protected function sendEmailToUsersManager(ProviderParticipantSessionRole $ppsr, string $role): void
    {
        $userManagers = $this->userRepository->findManagerListByClient($ppsr->getSession()->getClient());
        foreach ($userManagers as $user) {
            if (
                $user->isSubscribeToAlert(($role == ProviderParticipantSessionRole::ROLE_ANIMATOR) ? User::EMAIL_ANIMATOR_ON_SEVERAL_SESSIONS : User::EMAIL_TRAINEE_ON_SEVERAL_SESSIONS)
            ) {
                $sendGo = false;
                if ($user->isSubscribeToAlert(User::EMAIL_ONLY_FOR_MY_SESSIONS)) {
                    if ((!empty($ppsr->getSession()->getCreatedBy()) && $ppsr->getSession()->getCreatedBy() == $user) || (!empty($ppsr->getSession()->getCreatedBy()) && $ppsr->getSession()->getUpdatedBy() == $user)) {
                        $sendGo = true;
                    }
                } else {
                    $sendGo = true;
                }
                if ($sendGo) {
                    $this->mailerService->sendTemplatedMail(
                        $user->getEmail(),
                        $this->translator->trans(($role == ProviderParticipantSessionRole::ROLE_ANIMATOR) ? 'Animator planned on several sessions in parallel' : 'Participant planned on several sessions in parallel'),
                        'emails/alert/participant_on_several_sessions.html.twig',
                        context: [
                            'ppsr' => $ppsr,
                            'client' => $ppsr->getSession()->getClient(),
                            'routeSession' => $this->router->generate('_session_get', ['id' => $ppsr->getSession()->getId()], 0),
                            'user' => $user,
                        ],
                        metadatas: [
                            'origin' => ($role == ProviderParticipantSessionRole::ROLE_ANIMATOR) ? MailerService::ORIGIN_EMAIL_ANIMATOR_ON_SEVERAL_SESSIONS : MailerService::ORIGIN_EMAIL_TRAINEE_ON_SEVERAL_SESSIONS,
                            'user' => $user->getId(),
                        ],
                        senderSystem: true
                    );
                }
            }
        }
    }
}
