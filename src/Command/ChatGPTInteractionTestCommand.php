<?php

namespace App\Command;

use App\Service\ChatGPTService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ChatGPTInteractionTestCommand extends Command
{
    protected static $defaultName = 'chat-gpt:interaction:test';

    public function __construct(private ChatGPTService $chatGPTService)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /*$output->writeln($this->chatGPTService->generateTitle('Formation sur chatgpt en presentiel', ChatGPTService::TYPE_TRAINING, true, ChatGPTService::TONE_PROFESSIONAL));*/
        $output->writeln('<info>Correction du titre :</info>');
        $titleToCorrect = 'Les classe virtuelles en 2023 et leur impact sur la formation des formateurs';
        $responseCorrectTitle = $this->chatGPTService->correctTitle($titleToCorrect);
        $output->writeln("<comment>$titleToCorrect</comment> -> <info>$responseCorrectTitle</info>");

        $output->writeln('<info>Raccourcissement du titre :</info>');
        $responseShortenTitle = $this->chatGPTService->shortenTitle($responseCorrectTitle);
        $output->writeln("<comment>$responseCorrectTitle</comment> -> <info>$responseShortenTitle</info>");

        $output->writeln('<info>Génération de titre pour le texte :</info>');
        $category = ChatGPTService::TYPE_TRAINING;
        $ton = ChatGPTService::TONE_PROFESSIONAL;

        $generatedTitle = $this->chatGPTService->generateTitle($responseShortenTitle, $category, true, $ton);
        $output->writeln("<comment>Texte :</comment> $responseShortenTitle");
        $output->writeln("<comment>Catégorie :</comment> $category");
        $output->writeln('<comment>Inclure une emoji :</comment> true');
        $output->writeln("<comment>Ton :</comment> $ton");
        $output->writeln('<info>Titre généré :</info>');
        $output->writeln($generatedTitle);

        return Command::SUCCESS;
    }
}
