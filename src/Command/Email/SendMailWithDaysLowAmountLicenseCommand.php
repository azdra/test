<?php

namespace App\Command\Email;

use App\Entity\Client\Client;
use App\Repository\ClientRepository;
use App\Service\licensesAvailableService;
use DateTimeImmutable;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendMailWithDaysLowAmountLicenseCommand extends Command
{
    protected static $defaultName = 'email:days-low-amount-license:send';

    public function __construct(
        private ClientRepository $clientRepository,
        private licensesAvailableService $licensesAvailableService,
    ) {
        parent::__construct();
    }

    /**
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var Client[] $clients */
        $clients = $this->clientRepository->findBy(['licensesAvailable.enabled' => true]);

        foreach ($clients as $client) {
            if (
                $client->getLicensesAvailable()->getThresholdMin() === null ||
                !array_key_exists('tags', $client->getLicensesAvailable()->getLicensesReportRecipients()) ||
                empty($client->getLicensesAvailable()->getLicensesReportRecipients()['tags'][0])
            ) {
                continue;
            }
            $configurationsHolders = $this->licensesAvailableService->getWebexConfigurationHolderFromClient($client);
            foreach ($configurationsHolders as $configurationsHolder) {
                $options = [
                    'connectorId' => $configurationsHolder->getId(),
                    'start' => (new DateTimeImmutable())->setTime(0, 0)->format('Y-m-d'),
                    'end' => (new DateTimeImmutable())->modify('+ 3 years')->setTime(23, 59)->format('Y-m-d'),
                    ];

                $timeSlotData = $this->licensesAvailableService->getTimeSlotDataAvailableLicenses($options);
                $slotLowAmountLicense = $this->licensesAvailableService->getSlotLowAmountLicense($timeSlotData);
                if (!empty($slotLowAmountLicense['slotLowAmountLicense'])) {
                    $this->licensesAvailableService->sendDaysLowAmountLicense(configurationHolder: $configurationsHolder, dataToSent: $slotLowAmountLicense);
                }
            }
        }

        return Command::SUCCESS;
    }
}
