<?php

namespace App\Command\Email;

use App\Repository\EvaluationProviderSessionRepository;
use App\Service\ActivityLogger;
use App\Service\EvaluationProviderSessionService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class SendEvaluationMailCommand extends Command
{
    protected static $defaultName = 'email:session:evaluation-email:send';

    public function __construct(
        private EvaluationProviderSessionRepository $evaluationProviderSessionRepository,
        private EvaluationProviderSessionService $evaluationProviderSessionService,
        private EntityManagerInterface $entityManager,
        private ActivityLogger $activityLogger
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $evaluationsToSend = $this->evaluationProviderSessionRepository->findEvaluationsToSend();
            $output->writeln('<info>We have '.count($evaluationsToSend).' evaluations to send </info>');
            foreach ($evaluationsToSend as $evaluationToSend) {
                $output->writeln('<info>Send evaluation to '.$evaluationToSend->getProviderSession()->getName().'</info>');
                $this->evaluationProviderSessionService->sendMailEvaluationToAllParticipant($evaluationToSend);
                $evaluationToSend->setSendAt(new \DateTime())->setSent(true);
            }
            $io->success('All done !');
            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $io->error('Unknown error: '.$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
