<?php

namespace App\Command\Email;

use App\Entity\WebinarConvocation;
use App\Repository\WebinarConvocationRepository;
use App\Service\ActivityLogger;
use App\Service\WebinarConvocationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class SendInvitationCommand extends Command
{
    protected static $defaultName = 'email:session:invitation-webinar:send';

    private EntityManagerInterface $entityManager;

    private ActivityLogger $activityLogger;

    public function __construct(
        private WebinarConvocationRepository $webinarConvocationRepository,
        private WebinarConvocationService $webinarConvocationService,
        EntityManagerInterface $entityManager,
        ActivityLogger $activityLogger
    ) {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->activityLogger = $activityLogger;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $invitations = $this->webinarConvocationRepository->findCommunicationWithTypeToSend(WebinarConvocation::TYPE_INVITATION);
            $output->writeln('<info>We have '.count($invitations).' invitation to send </info>');
            foreach ($invitations as $invitation) {
                $this->webinarConvocationService->sendInvitation($invitation);
            }
            $io->success('All done !');
            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $io->error('Unknown error: '.$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
