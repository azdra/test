<?php

namespace App\Command\Email;

use App\Repository\ProviderSessionRepository;
use App\Service\SendTrainingCertificatesService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class SendTrainingCertificatesCommand extends Command
{
    protected static $defaultName = 'email:session:training-certificates:send';

    public function __construct(
        private ProviderSessionRepository $sessionRepository,
        private LoggerInterface $cronLogger,
        private EntityManagerInterface $entityManager,
        private SendTrainingCertificatesService $sendTrainingCertificatesService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $dateFrom = (new DateTimeImmutable())->modify('- 7 day');
        $dateTo = (new DateTimeImmutable())->modify('- 1 day');
        try {
            $sessions = $this->sessionRepository->findAllBetweenDates($dateFrom, $dateTo);
            $output->writeln("\t >> we have  ".count($sessions).' sessions ');
            foreach ($sessions as $session) {
                $output->writeln("\t >> send for session ".$session->getId());
                if ($session->getIsSendTrainingCertificatesClient() && $session->isAutomaticSendingTrainingCertificates() && $session->getSentTrainingCertificates() == null) {
                    $this->sendTrainingCertificatesService->sendTrainingCertificateParticipant(providerSession: $session);
                    $session->setSentTrainingCertificates(new DateTimeImmutable());
                    $output->writeln("\t >> done -----");
                }
            }
            $output->writeln("\t >> All done");
            $this->entityManager->flush();

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->cronLogger->error($exception);
            $output->writeln("\t Unknown error: ".$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
