<?php

namespace App\Command\Email;

use App\Entity\Client\Client;
use App\Repository\ClientRepository;
use App\Service\MailerService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class SendMailTestForWebhookInvitationCommand extends Command
{
    protected static $defaultName = 'email:test-webhook-invitation:send';

    public function __construct(
        private ClientRepository $clientRepository,
        private MailerService $mailerService,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('client', 'c', InputOption::VALUE_OPTIONAL, 'Client ID')
            ->addOption('toemail', 'm', InputOption::VALUE_OPTIONAL, 'Email to');
    }

    /**
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $clientId = $input->getOption('client');
            $emailTo = (is_null($input->getOption('toemail'))) ? 'ingenierie@live-session.fr' : $input->getOption('toemail');
            /** @var Client $client */
            $client = $this->clientRepository->find($clientId);
            $this->mailerService->sendTemplatedMail(
                [$emailTo],
                'Test du transporteur d\'email',
                'emails/test/send_test_transporter.html.twig',
                context: [
                    'client' => $client,
                ],
                metadatas: [
                    'origin' => MailerService::ORIGIN_EMAIL_SEND_SIGNATURE_LINK,
                    'client' => $client->getId(),
                ]
            );
            $io->success('All done !');

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $io->error('Unknown error: '.$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
