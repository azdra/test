<?php

namespace App\Command\Email;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\MailerService;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendListOfManagingClients extends Command
{
    private UserRepository $userRepository;
    private string $clientsListSavefilesDirectory;
    private MailerService $mailerService;
    protected static $defaultName = 'app:email:send-list-of-managing-clients';

    public function __construct(UserRepository $userRepository, MailerService $mailerService, string $clientsListSavefilesDirectory)
    {
        $this->userRepository = $userRepository;
        $this->clientsListSavefilesDirectory = $clientsListSavefilesDirectory;
        $this->mailerService = $mailerService;
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $users = $this->userRepository->findAllActiveManagingAccounts();
        $date = new \DateTime();
        $fileName = sprintf('%s/%s.xlsx', $this->clientsListSavefilesDirectory, $date->format('YmdHis'));
        try {
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $row = 2;

            $this->createHeader($sheet);

            /** @var User $user */
            foreach ($users as $user) {
                $this->createItem($sheet, $row, $user);
                ++$row;
            }
            $writer = new Xlsx($spreadsheet);
            $writer->save($fileName);
            $this->mailerService->sendTemplatedMail('support@live-session.fr', 'Liste des clients gestionnaires à jour', 'emails/export_client_list.html.twig', context: [
                'date' => $date->format('d/m/Y'),
            ], pathToAttach: $fileName);
            unlink($fileName);
        } catch (\Exception $e) {
            $output->writeln('Error sending email to all managing clients');

            return Command::FAILURE;
        }

        $output->writeln('Sending email to all managing clients');

        return Command::SUCCESS;
    }

    private function createHeader(&$sheet): void
    {
        $sheet->setCellValue('A'. 1, 'Id User');
        $sheet->setCellValue('B'. 1, 'User Full Name');
        $sheet->setCellValue('C'. 1, 'User Email');
        $sheet->setCellValue('D'. 1, 'Client ID');
        $sheet->setCellValue('E'. 1, 'Client Name');
    }

    private function createItem($sheet, $row, User $user): void
    {
        $sheet->setCellValue('A'.$row, $user->getId());
        $sheet->setCellValue('B'.$row, $user->getFullName());
        $sheet->setCellValue('C'.$row, $user->getEmail());
        $sheet->setCellValue('D'.$row, $user->getClient()->getId());
        $sheet->setCellValue('E'.$row, $user->getClient()->getName());
    }
}
