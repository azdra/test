<?php

namespace App\Command\Email;

use App\Entity\ActivityLog;
use App\Entity\Client\Client;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ClientRepository;
use App\Repository\ProviderSessionRepository;
use App\Service\ActivityLogger;
use App\Service\AttendanceReportService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\RouterInterface;

class SendPresenceReportCommand extends Command
{
    protected static $defaultName = 'email:session:presence-report:send';

    public function __construct(
        private ClientRepository $clientRepository,
        private ProviderSessionRepository $sessionRepository,
        private AttendanceReportService $attendanceReportService,
        private ActivityLogger $activityLogger,
        private EntityManagerInterface $entityManager,
        private RouterInterface $router
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var Client[] $clients */
        $clients = $this->clientRepository->findBy(['emailOptions.sendPresenceReportAfterSession' => true]);
        foreach ($clients as $client) {
            if (
                empty($client->getEmailScheduling()->getTimeToSendPresenceReportAfterSession()) ||
                !array_key_exists('tags', $client->getEmailScheduling()->getPresenceReportRecipients()) ||
                empty($client->getEmailScheduling()->getPresenceReportRecipients()['tags'])
            ) {
                continue;
            }

            $this->activityLogger->initActivityLogContext($client, null, ActivityLog::ORIGIN_CRON);

            $dateToSend = $this->buildDateToSend($client->getEmailScheduling()->getTimeToSendPresenceReportAfterSession());
            $sessions = $this->sessionRepository->getSessionToSendPresenceReport($client, $dateToSend);
            $output->writeln('We have #{'.count($sessions).'} sessions to send a attendance report to client : '.$client->getName());
            foreach ($sessions as $session) {
                $this->attendanceReportService->sendAttendanceReportByMail($session, $client->getEmailScheduling()->getPresenceReportRecipients()['tags']);
                $session->setSentPresenceReport(true);
                $infos = [
                    'message' => [
                        'key' => 'The attendance report for the <a href="%sessionRoute%">%sessionName%</a> session has been sent to the following recipients %email%',
                        'params' => ['%route%' => $this->router->generate('_session_get', ['id' => $session->getId()]), '%sessionName%' => $session->getName(), '%email%' => $client->getEmailScheduling()->getPresenceReportRecipients()['tags']],
                    ], 'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                ];
                $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SEND_PRESENCE_REPORT, ActivityLog::SEVERITY_INFORMATION, $infos));
                $output->writeln("Sent a attendance report to session #{$session->getId()}# =====> Done ");
            }

            $this->activityLogger->flushActivityLogs();
            $this->entityManager->flush();
        }

        return Command::SUCCESS;
    }

    private function buildDateToSend(array $configuration): \DateTime
    {
        $hours = $configuration['hours'] ?? 0;
        $days = $configuration['days'] ?? 0;
        $interval = new \DateInterval('P'.$days.'DT'.$hours.'H');

        return (new \DateTime())->sub($interval);
    }
}
