<?php

namespace App\Command\Email;

use App\Entity\CategorySessionType;
use App\Entity\ProviderSession;
use App\Event\TransactionalEmailRequestEventInterface;
use App\Event\TransactionalSessionEmailRequestEvent;
use App\Repository\ProviderSessionRepository;
use App\Service\ActivityLogger;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Throwable;

class SendCommunicationCommand extends Command
{
    protected static $defaultName = 'email:session:communication-webinar:send';

    private ProviderSessionRepository $providerSessionRepository;

    private EntityManagerInterface $entityManager;

    private EventDispatcherInterface $eventDispatcher;

    private ActivityLogger $activityLogger;

    private LoggerInterface $logger;

    public function __construct(
        ProviderSessionRepository $providerSessionRepository,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        LoggerInterface $cronLogger,
        ActivityLogger $activityLogger
    ) {
        parent::__construct();
        $this->providerSessionRepository = $providerSessionRepository;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $cronLogger;
        $this->activityLogger = $activityLogger;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $sessionsWebinar = $this->providerSessionRepository->findAllSessionsWebinarWithStatusScheduled();

            /** @var ProviderSession $session */
            foreach ($sessionsWebinar as $session) {
                if ($session->getClient()->getName() !== 'Renault') {
                    if ($session->getCategory() == CategorySessionType::CATEGORY_SESSION_WEBINAR) {
                        $output->write("> Webinar Session #{$session->getId()} [{$session->getRef()}] \n");

                        $this->eventDispatcher->dispatch(new TransactionalSessionEmailRequestEvent($session), TransactionalEmailRequestEventInterface::COMMUNICATION_ALL_PARTICIPANTS);
                    }
                    $this->entityManager->flush();
                    $this->activityLogger->flushActivityLogs();
                }
            }
            $io->success('All done !');

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $io->error('Unknown error: '.$exception->getMessage());

            return Command::FAILURE;
        }
    }
}
