<?php

namespace App\Command;

use App\Service\ActivityLogService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class CheckPurgeActivityLogDataCommand extends Command
{
    protected static $defaultName = 'service:check:purge:activity-log-data';

    public function __construct(
        private LoggerInterface $cronLogger,
        private ActivityLogService $activityLogService
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        //bin/console service:check:purge:activity-log-data 40 35
        $this->addArgument('minusdays', InputArgument::REQUIRED, 'minusdays >> SELECT FROM date - nbrdays');
        $this->addArgument('plusdays', InputArgument::REQUIRED, 'plusdays >> SELECT FROM date - nbrdays');
        //bin/console service:check:purge:activity-log-data 40 35 -a 15
        $this->addOption('activity_log_id', 'a', InputOption::VALUE_OPTIONAL, 'Activity Log ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("\t> ------Début du traitement-------------");
        $executionStartTime = microtime(true);
        try {
            $alid = $input->getOption('activity_log_id');
            if (empty($alid)) {
                $return = $this->activityLogService->cleanActivityLogData($output, $input->getArgument('minusdays'), $input->getArgument('plusdays'), null);
            } else {
                $return = $this->activityLogService->cleanActivityLogData($output, $input->getArgument('minusdays'), $input->getArgument('plusdays'), $alid);
            }
            $output->writeln("\t >> Clean Activity Log Session : ".$return.' treated !');

            $output->writeln("\t >> All done");
            $executionEndTime = microtime(true);
            $seconds = $executionEndTime - $executionStartTime;
            $output->writeln("\t> ------Durée du traitement-------------".round($seconds, 3).' secs---|---------------------');

            return Command::SUCCESS;
        } catch (Throwable $exception) {
            $this->cronLogger->error($exception);
            $output->writeln('Unknown error: '.$exception->getMessage());
            $executionEndTime = microtime(true);
            $seconds = $executionEndTime - $executionStartTime;

            return Command::FAILURE;
        }
    }
}
