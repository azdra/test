<?php

namespace App\Event;

use App\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;

class UserPasswordEvent extends Event
{
    public const RESET = 'user.reset_password_request';
    public const RESET_FROM_SELF_SUBSCRIPTION = 'user.reset_password_request_self_subscription';
    public const NEW = 'user.new_password_request';
    public const CLONE = 'user.clone_password';

    protected User $user;

    protected ?string $domain;
    protected ?string $slugClient;

    public function __construct(User $user, string $domain = null, string $slugClient = null)
    {
        $this->user = $user;
        $this->domain = $domain;
        $this->slugClient = $slugClient;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function getSlugClient(): ?string
    {
        return $this->slugClient;
    }
}
