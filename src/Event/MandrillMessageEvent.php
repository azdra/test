<?php

namespace App\Event;

use App\Exception\InvalidArgumentException;

/**
 * Class MandrillMessageEvent.
 *
 * @see https://mandrill.zendesk.com/hc/en-us/articles/205583307-Message-Event-Webhook-format
 */
class MandrillMessageEvent
{
    protected string $id;

    protected string $name;

    protected array $message;

    protected \DateTime $date;

    /**
     * @throws InvalidArgumentException
     */
    public static function create(array $data = []): MandrillMessageEvent
    {
        $event = new static();

        if (!empty($data)) {
            $event
                ->setId($data['_id'] ?? '')
                ->setName($data['event'] ?? '')
                ->setMessage($data['msg'] ?? []);

            $date = new \DateTime();
            $date->setTimestamp($data['ts']);

            $event->setDate($date);
        }

        return $event;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): MandrillMessageEvent
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function setName(string $name): MandrillMessageEvent
    {
        if (!in_array($name, MandrillMessageEvents::ALL)) {
            throw new InvalidArgumentException(sprintf('The event name "%s" is not valid. ("%s")', $name, implode(', ', MandrillMessageEvents::ALL)));
        }

        $this->name = $name;

        return $this;
    }

    public function getMessage(): array
    {
        return $this->message;
    }

    public function setMessage(array $message = []): MandrillMessageEvent
    {
        $this->message = $message;

        return $this;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): MandrillMessageEvent
    {
        $this->date = $date;

        return $this;
    }
}
