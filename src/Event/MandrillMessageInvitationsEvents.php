<?php

namespace App\Event;

final class MandrillMessageInvitationsEvents
{
    public const PREFIX = 'mandrill.invitations.message';

    public const OPENED = self::PREFIX.'.open';
    public const CLICKED = self::PREFIX.'.click';

    public const ALL = [
        self::OPENED,
        self::CLICKED,
    ];
}
