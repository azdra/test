<?php

namespace App\Event;

use Symfony\Component\Security\Core\User\UserInterface;

interface TransactionalEmailRequestEventInterface
{
    public const CONVOCATION_ALL_PARTICIPANTS = 'session.convocation';
    public const COMMUNICATION_ALL_PARTICIPANTS = 'session.communication';
    public const REMINDER_ALL_PARTICIPANTS = 'session.reminder';
    public const CANCEL = 'session.cancel';
    public const UNCANCEL = 'session.uncancel';
    public const NOTIFY_PARTICIPANTS_SESSION_IS_UPDATED = 'session.is.updated';

    public const CONVOCATION_ONE_PARTICIPANT = 'participant.convocation';
    public const COMMUNICATION_ONE_PARTICIPANT = 'participant.communication';
    public const REMINDER_ONE_PARTICIPANT = 'participant.reminder';
    public const UNSUBSCRIBE = 'participant.unsubscribe';
    public const ROLE_CHANGE = 'participant.role-change';

    public function getOrigin(): ?UserInterface;

    public function isForce(): bool;
}
