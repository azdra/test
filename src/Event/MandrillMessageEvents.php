<?php

namespace App\Event;

final class MandrillMessageEvents
{
    public const PREFIX = 'mandrill.message';

    public const SEND = self::PREFIX.'.send';
    public const DELAYED = self::PREFIX.'.deferral';
    public const SOFT_BOUNCE = self::PREFIX.'.soft_bounce';
    public const HARD_BOUNCE = self::PREFIX.'.hard_bounce';
    public const SPAM = self::PREFIX.'.spam';
    public const REJECTED = self::PREFIX.'.reject';
    public const UNSUBSCRIBE = self::PREFIX.'.unsub';
    public const OPENED = self::PREFIX.'.open';
    public const CLICKED = self::PREFIX.'.click';
    public const BLACKLIST = self::PREFIX.'.blacklist';
    public const WHITELIST = self::PREFIX.'.whitelist';

    public const ALL = [
        self::SEND,
        self::DELAYED,
        self::SOFT_BOUNCE,
        self::HARD_BOUNCE,
        self::SPAM,
        self::REJECTED,
        self::UNSUBSCRIBE,
        self::OPENED,
        self::CLICKED,
        self::BLACKLIST,
        self::WHITELIST,
    ];
}
