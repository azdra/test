<?php

namespace App\Event;

use App\Entity\Bounce;
use App\Exception\InvalidArgumentException;
use App\Service\BounceService;
use App\Service\MailerService;

class IDFuseMessageEvent
{
    protected string $id;

    protected string $name;

    protected array $message;

    protected \DateTime $date;

    /**
     * @throws InvalidArgumentException
     */
    public static function create(array $data = []): IDFuseMessageEvent
    {
        $event = new static();

        if (!empty($data)) {
            $date = \DateTime::createFromFormat('Y-m-d H:i:sO', $data['bounce_at']);
            $dateTI = \DateTimeImmutable::createFromMutable($date);
            $msg = [
                'email' => $data['from_email'], //$data['vmta'],
                'subject' => $data['dsnDiag'],
                'state' => Bounce::TO_CORRECT,
                'reason' => BounceService::IDFUSE_REASON_REJECTED,
                'date_send' => $dateTI,
                'date_status' => $dateTI,
                'active' => 1,
                'mail_origin' => $data['from_email'],
                'mandrill_event' => $data['event'],
                'metadata' => [
                    'origin' => MailerService::ORIGIN_EMAIL_SEND_CONVOCATION,
                ],
            ];
            $event
                ->setId($data['maildata_id'] ?? '')
                ->setName($data['event'] ?? '')
                ->setMessage($msg);

            $event->setDate($date);
        }

        return $event;
    }

    public static function createFromApiCampaignReportBounceList(array $data = []): IDFuseMessageEvent
    {
        $event = new static();

        if (!empty($data)) {
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $data['tstamp']);
            $dateTI = \DateTimeImmutable::createFromMutable($date);
            $msg = [
                'email' => $data['email'], //$data['vmta'],
                'subject' => $data['reason'],
                'state' => Bounce::TO_CORRECT,
                'reason' => BounceService::IDFUSE_REASON_REJECTED,
                'date_send' => $dateTI,
                'date_status' => $dateTI,
                'active' => 1,
                'mail_origin' => $data['email'],
                'mandrill_event' => $data['event'],
                'metadata' => [
                    'origin' => MailerService::ORIGIN_EMAIL_SEND_CONVOCATION,
                    'user' => $data['user_id'],
                ],
            ];
            $event
                //->setId($data['maildata_id'] ?? '')
                ->setName($data['event'] ?? '')
                ->setMessage($msg);

            $event->setDate($date);
        }

        return $event;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): IDFuseMessageEvent
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function setName(string $name): IDFuseMessageEvent
    {
        if (!in_array($name, IDFuseMessageEvents::ALL)) {
            throw new InvalidArgumentException(sprintf('The event name "%s" is not valid. ("%s")', $name, implode(', ', IDFuseMessageEvents::ALL)));
        }

        $this->name = $name;

        return $this;
    }

    public function getMessage(): array
    {
        return $this->message;
    }

    public function setMessage(array $message = []): IDFuseMessageEvent
    {
        $this->message = $message;

        return $this;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): IDFuseMessageEvent
    {
        $this->date = $date;

        return $this;
    }
}
