<?php

namespace App\Event;

use App\Entity\Bounce;
use App\Exception\InvalidArgumentException;
use App\Service\BounceService;
use App\Service\MailerService;

class BrevoMessageEvent
{
    protected string $id;

    protected string $name;

    protected array $message;

    protected \DateTime $date;

    /**
     * @throws InvalidArgumentException
     */
    public static function create(array $data = []): BrevoMessageEvent
    {
        $event = new static();

        if (!empty($data)) {
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $data['date']);
            $dateTI = \DateTimeImmutable::createFromMutable($date);
            //$dataFromTS = ( new \DateTime() )->setTimestamp($data['ts'])->format('Y-m-d H:i:s');
            //$dataFromTSEvent = ( new \DateTime() )->setTimestamp($data['ts_event'])->format('Y-m-d H:i:s');
            $msg = [
                'email' => $data['email'], //$data['vmta'],
                'subject' => $data['subject'],
                'state' => Bounce::TO_CORRECT,
                'reason' => BounceService::BREVO_REASON_REJECTED,
                'date_send' => $dateTI,
                'date_status' => $dateTI,
                'active' => 1,
                'mail_origin' => '', //$data['from_email'],
                'mandrill_event' => $data['event'],
                'metadata' => [
                    'origin' => MailerService::ORIGIN_EMAIL_SEND_CONVOCATION,
                    'type' => $data['tags'][0],  //Convocation
                    'user' => $data['tags'][1],  //user_id
                ],
            ];
            $event
                ->setId($data['maildata_id'] ?? '')
                ->setName($data['event'] ?? '')
                ->setMessage($msg);

            $event->setDate($date);
        }

        return $event;
    }

    public static function createFromApiCampaignReportBounceList(array $data = []): BrevoMessageEvent
    {
        $event = new static();

        if (!empty($data)) {
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $data['tstamp']);
            $dateTI = \DateTimeImmutable::createFromMutable($date);
            $msg = [
                'email' => $data['email'], //$data['vmta'],
                'subject' => $data['reason'],
                'state' => Bounce::TO_CORRECT,
                'reason' => BounceService::BREVO_REASON_REJECTED,
                'date_send' => $dateTI,
                'date_status' => $dateTI,
                'active' => 1,
                'mail_origin' => $data['email'],
                'mandrill_event' => $data['event'],
                'metadata' => [
                    'origin' => MailerService::ORIGIN_EMAIL_SEND_CONVOCATION,
                    'user' => $data['user_id'],
                ],
            ];
            $event
                //->setId($data['maildata_id'] ?? '')
                ->setName($data['event'] ?? '')
                ->setMessage($msg);

            $event->setDate($date);
        }

        return $event;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): BrevoMessageEvent
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function setName(string $name): BrevoMessageEvent
    {
        if (!in_array($name, BrevoMessageEvents::ALL)) {
            throw new InvalidArgumentException(sprintf('The event name "%s" is not valid. ("%s")', $name, implode(', ', BrevoMessageEvents::ALL)));
        }

        $this->name = $name;

        return $this;
    }

    public function getMessage(): array
    {
        return $this->message;
    }

    public function setMessage(array $message = []): BrevoMessageEvent
    {
        $this->message = $message;

        return $this;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): BrevoMessageEvent
    {
        $this->date = $date;

        return $this;
    }
}
