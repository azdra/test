<?php

namespace App\Event;

use App\Exception\InvalidArgumentException;

class BrevoSmsMessageEvent
{
    protected string $id;

    protected string $name;

    protected string $phone;

    protected array $tag;

    protected \DateTime $date;

    /**
     * @throws InvalidArgumentException
     */
    public static function create(array $data = []): self
    {
        $event = new static();

        if (!empty($data)) {
            $event
                ->setId($data['messageId'])
                ->setName($data['event'])
                ->setPhone($data['to'])
                ->setTag($data['tag']);

            $date = new \DateTime();
            $date->setTimestamp($data['ts_event']);

            $event->setDate($date);
        }

        return $event;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function setName(string $name): self
    {
        if (!in_array($name, BrevoSmsMessageEvents::ALL)) {
            throw new InvalidArgumentException(sprintf('The event name "%s" is not valid. ("%s")', $name, implode(', ', BrevoSmsMessageEvents::ALL)));
        }

        $this->name = $name;

        return $this;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTag(): array
    {
        return $this->tag;
    }

    public function setTag(string $tag): void
    {
        $this->tag = json_decode($tag, true);
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }
}
