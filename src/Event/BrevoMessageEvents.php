<?php

namespace App\Event;

final class BrevoMessageEvents
{
    public const PREFIX = 'brevo.message';

    public const SEND = self::PREFIX.'.request';
    public const SOFT_BOUNCE = self::PREFIX.'.soft_bounce';
    public const HARD_BOUNCE = self::PREFIX.'.hard_bounce';
    public const OPENED = self::PREFIX.'.opened';
    public const CLICKED = self::PREFIX.'.click';

    public const ALL = [
        self::SEND,
        self::SOFT_BOUNCE,
        self::HARD_BOUNCE,
        self::OPENED,
        self::CLICKED,
    ];
}
