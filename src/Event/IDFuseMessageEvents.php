<?php

namespace App\Event;

final class IDFuseMessageEvents
{
    public const PREFIX = 'idfuse.message';

    public const SEND = self::PREFIX.'.send';
    public const SOFT_BOUNCE = self::PREFIX.'.soft';
    public const HARD_BOUNCE = self::PREFIX.'.hard';
    public const OPENED = self::PREFIX.'.open';
    public const CLICKED = self::PREFIX.'.unopen';

    public const ALL = [
        self::SEND,
        self::SOFT_BOUNCE,
        self::HARD_BOUNCE,
        self::OPENED,
        self::CLICKED,
    ];
}
