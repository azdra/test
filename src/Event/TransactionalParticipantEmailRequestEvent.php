<?php

namespace App\Event;

use App\Entity\ProviderParticipantSessionRole;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

class TransactionalParticipantEmailRequestEvent extends Event implements TransactionalEmailRequestEventInterface
{
    private ProviderParticipantSessionRole $participantSessionRole;

    private ?UserInterface $origin;

    private bool $force;

    public function __construct(ProviderParticipantSessionRole $participantSessionRole, ?UserInterface $origin = null, bool $force = false)
    {
        $this->participantSessionRole = $participantSessionRole;
        $this->origin = $origin;
        $this->force = $force;
    }

    public function getParticipantSessionRole(): ProviderParticipantSessionRole
    {
        return $this->participantSessionRole;
    }

    public function getOrigin(): ?UserInterface
    {
        return $this->origin;
    }

    public function isForce(): bool
    {
        return $this->force;
    }
}
