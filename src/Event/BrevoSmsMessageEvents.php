<?php

namespace App\Event;

final class BrevoSmsMessageEvents
{
    public const PREFIX = 'brevo.sms.message';

    public const SOFT_BOUNCE = self::PREFIX.'.soft_bounce';
    public const HARD_BOUNCE = self::PREFIX.'.hard_bounce';

    public const ALL = [
        self::SOFT_BOUNCE,
        self::HARD_BOUNCE,
    ];
}
