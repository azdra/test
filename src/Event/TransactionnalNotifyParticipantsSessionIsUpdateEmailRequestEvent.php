<?php

namespace App\Event;

use App\Entity\ProviderSession;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

class TransactionnalNotifyParticipantsSessionIsUpdateEmailRequestEvent extends Event implements TransactionalEmailRequestEventInterface
{
    public function __construct(
        private ProviderSession $providerSession,
        private ?UserInterface $origin,
        private bool $force,
        private array $changes
    ) {
    }

    public function getProviderSession(): ProviderSession
    {
        return $this->providerSession;
    }

    public function getOrigin(): ?UserInterface
    {
        return $this->origin;
    }

    public function isForce(): bool
    {
        return $this->force;
    }

    public function getChanges(): array
    {
        return $this->changes;
    }
}
