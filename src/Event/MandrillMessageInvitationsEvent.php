<?php

namespace App\Event;

use App\Exception\InvalidArgumentException;

/**
 * Class MandrillMessageInvitationsEvent.
 *
 * @see https://mandrill.zendesk.com/hc/en-us/articles/205583307-Message-Event-Webhook-format
 */
class MandrillMessageInvitationsEvent
{
    protected string $id;

    protected string $name;

    protected array $message;

    protected \DateTime $date;

    /**
     * @throws InvalidArgumentException
     */
    public static function create(array $data = []): MandrillMessageInvitationsEvent
    {
        $event = new static();

        if (!empty($data)) {
            $event
                ->setId($data['_id'] ?? '')
                ->setName($data['event'] ?? '')
                ->setMessage($data['msg'] ?? []);

            $date = new \DateTime();
            $date->setTimestamp($data['ts']);

            $event->setDate($date);
        }

        return $event;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): MandrillMessageInvitationsEvent
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function setName(string $name): MandrillMessageInvitationsEvent
    {
        if (!in_array($name, MandrillMessageInvitationsEvents::ALL)) {
            throw new InvalidArgumentException(sprintf('The event name "%s" is not valid. ("%s")', $name, implode(', ', MandrillMessageInvitationsEvents::ALL)));
        }

        $this->name = $name;

        return $this;
    }

    public function getMessage(): array
    {
        return $this->message;
    }

    public function setMessage(array $message = []): MandrillMessageInvitationsEvent
    {
        $this->message = $message;

        return $this;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): MandrillMessageInvitationsEvent
    {
        $this->date = $date;

        return $this;
    }
}
