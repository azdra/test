<?php

namespace App\Event;

use App\Entity\ProviderSession;
use App\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;

class TransactionalSessionEmailRequestEvent extends Event implements TransactionalEmailRequestEventInterface
{
    private ProviderSession $providerSession;

    private ?User $origin;

    private bool $force;

    public function __construct(ProviderSession $providerSession, ?User $origin = null, bool $force = false)
    {
        $this->providerSession = $providerSession;
        $this->origin = $origin;
        $this->force = $force;
    }

    public function getProviderSession(): ProviderSession
    {
        return $this->providerSession;
    }

    public function getOrigin(): ?User
    {
        return $this->origin;
    }

    public function isForce(): bool
    {
        return $this->force;
    }
}
