<?php

namespace App\CustomerService\Command\Debug;

use App\CustomerService\Service\DashboardService;
use App\Repository\ProviderSessionRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

class TestDashboardDebugCommand extends Command
{
    protected static $defaultName = 'debug:test-dashboard';

    public function __construct(
        private DashboardService $dashboardService,
        private LoggerInterface $logger,
        private ProviderSessionRepository $providerSessionRepository
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $dateStart = (new \DateTimeImmutable())->modify('+0 day')->setTime(0, 0);
            $dateEnd = (new \DateTimeImmutable())->modify('+10 day')->setTime(23, 59);
            $params = [];
            $dataFromSessions = $this->dashboardService->getSessionsIndicators($dateStart, $dateEnd, $params);

            //print_r('*** Nbr sessions >>> '.count($dataFromSessions).' ***');
            //$this->logger->info('Email referrer test  from command: '.$emailReferrer);
        } catch (Throwable $exception) {
            $io->error($exception->getMessage());
            $this->logger->error($exception);

            return Command::FAILURE;
        }

        $io->success('Generated !');

        return Command::SUCCESS;
    }
}
