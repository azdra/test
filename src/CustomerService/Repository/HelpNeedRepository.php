<?php

namespace App\CustomerService\Repository;

use App\CustomerService\Entity\HelpNeed;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HelpNeedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HelpNeed::class);
    }

    public function findBetweenDates(array $options): array
    {
        $resolver = new OptionsResolver();
        $resolver->setRequired('start')
            ->setRequired('end')
            ->setDefault('assistance', 1)
            ->setDefault('support', 1)
            ->setDefault('direct', 1)
            ->setDefault('unassigned', 0)
            ->setDefault('client', null)
            ->setDefault('helperUser', null);
        $options = $resolver->resolve($options);

        if ($options['support'] === false && $options['assistance'] === false && $options['direct'] === false) {
            return [];
        }

        $qb = $this->createQueryBuilder('help_need')
            ->where('help_need.start >= :start')
            ->andWhere('help_need.end <= :end')
            ->setParameter('start', new \DateTimeImmutable($options['start']))
            ->setParameter('end', new \DateTimeImmutable($options['end']));

        $types = array_keys(array_filter([
            HelpNeed::TYPE_ASSISTANCE => (int) $options['assistance'],
            HelpNeed::TYPE_SUPPORT => (int) $options['support'],
            HelpNeed::TYPE_DIRECT_NEED => (int) $options['direct'],
        ]));
        $qb->andWhere('help_need.type in (:types)')
            ->setParameter('types', $types);

        if ($options['unassigned']) {
            $qb->andWhere('help_need.helpTasks IS EMPTY');
        }

        if (!empty($options['client'])) {
            $qb->andWhere('IDENTITY(help_need.client) = :client')
                ->setParameter('client', $options['client']);
        }

        if (!empty($options['helperUser'])) {
            $qb->join('help_need.helpTasks', 'help_tasks')
                ->andWhere('IDENTITY(help_tasks.user) = :user')
                ->setParameter('user', $options['helperUser']);
        }

        return $qb->getQuery()->getResult();
    }

    public function findDirectNeedBetweenDates(\DateTimeImmutable $dateStart, \DateTimeImmutable $dateEnd): array
    {
        $qb = $this->createQueryBuilder('help_need')
            ->where('help_need.start >= :start')
            ->andWhere('help_need.end <= :end')
            ->setParameter('start', $dateStart)
            ->setParameter('end', $dateEnd);

        $qb->andWhere('help_need.type = :types')
            ->setParameter('types', HelpNeed::TYPE_DIRECT_NEED)
            ->andWhere('help_need.helpTasks IS NOT EMPTY');

        return $qb->getQuery()->getResult();
    }

    public function findForMassAssign(\DateTime $start, \DateTime $end, string $lang, array $clientIds): array
    {
        return $this->createQueryBuilder('help_need')
            ->join('help_need.session', 'session')
            ->where('help_need.start >= :start')
            ->andWhere('help_need.end <= :end')
            ->andWhere('session.language = :lang')
            ->andWhere('help_need.client IN (:clientIds)')
            ->andWhere('help_need.type = :supportType')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setParameter('lang', $lang)
            ->setParameter('clientIds', $clientIds)
            ->setParameter('supportType', HelpNeed::TYPE_SUPPORT)
            ->getQuery()
            ->getResult();
    }

    public function findNeedBetweenDates(\DateTimeImmutable $dateStart, \DateTimeImmutable $dateEnd, array $clientIds): array
    {
        $qb = $this->createQueryBuilder('help_need')
            ->where('help_need.start >= :start')
            ->andWhere('help_need.end <= :end')
            ->andWhere('help_need.helpTasks IS NOT EMPTY')
            ->andWhere('help_need.client IN (:clientIds)')
            ->setParameter('clientIds', $clientIds)
            ->setParameter('start', $dateStart)
            ->setParameter('end', $dateEnd);

        return $qb->getQuery()->getResult();
    }
}
