<?php

namespace App\CustomerService\Repository;

use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Entity\HelpTask;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HelpTaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HelpTask::class);
    }

    public function findBetweenDates(array $options): array
    {
        $resolver = new OptionsResolver();
        $resolver->setRequired('start')
            ->setRequired('end')
            ->setDefault('assistance', 1)
            ->setDefault('support', 1)
            ->setDefault('direct', 1)
            ->setDefault('unassigned', 0)
            ->setDefault('client', null)
            ->setDefault('helperUser', null);
        $options = $resolver->resolve($options);

        if ($options['support'] === false && $options['assistance'] === false && $options['direct'] === false) {
            return [];
        }

        $qb = $this->createQueryBuilder('help_task')
            ->where('help_task.start >= :start')
            ->andWhere('help_task.end <= :end')
            ->setParameter('start', new \DateTimeImmutable($options['start']))
            ->setParameter('end', new \DateTimeImmutable($options['end']));

        $qb->leftJoin('help_task.helpNeed', 'help_need');

        $types = array_keys(array_filter([
            HelpNeed::TYPE_ASSISTANCE => (int) $options['assistance'],
            HelpNeed::TYPE_SUPPORT => (int) $options['support'],
            HelpNeed::TYPE_DIRECT_NEED => (int) $options['direct'],
        ]));
        $qb->andWhere('help_need.type in (:types)')
            ->setParameter('types', $types);

        if (!empty($options['client'])) {
            $qb->andWhere('IDENTITY(help_need.client) = :client')
                ->setParameter('client', $options['client']);
        }

        if (!empty($options['helperUser'])) {
            $qb->andWhere('IDENTITY(help_task.user) = :user')
                ->setParameter('user', $options['helperUser']);
        }

        return $qb->getQuery()->getResult();
    }

    public function findDoubleBooking(\DateTimeImmutable $from, \DateTimeImmutable $to, array $params = []): array
    {
        $helpTaskInSameTime = $this->createQueryBuilder('ht2');
        $helpTaskInSameTime = $helpTaskInSameTime->select('COUNT(ht2.user)')
            ->leftJoin('ht2.helpNeed', 'helpNeed')
            ->leftJoin('helpNeed.session', 'session')
            ->where('ht.user = ht2.user')
            ->andWhere('helpNeed.type = :type');

        if (array_key_exists('client', $params) && $params['client']) {
            $helpTaskInSameTime->setParameter('client', $params['client']);
        }

        $helpTaskInSameTime->andWhere(
            $helpTaskInSameTime->expr()->orX(
                'ht.start BETWEEN ht2.start AND ht2.end',
                'ht.end BETWEEN ht2.start AND ht2.end',
                'ht2.start BETWEEN ht.start AND ht.end'
            )
        );
        $qb = $this->createQueryBuilder('ht')
            ->leftJoin('ht.helpNeed', 'htHelpNeed')
            ->leftJoin('htHelpNeed.session', 'htSession')
            ->andWhere('('.$helpTaskInSameTime.') > 1')
            ->andWhere('ht.start >= :from')
            ->andWhere('ht.end <= :to')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->setParameter('type', HelpNeed::TYPE_ASSISTANCE)
            ->orderBy('htSession.dateStart', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function findAllOrderDate(): array
    {
        $qb = $this->createQueryBuilder('help_task')
            ->orderBy(' help_task.end', 'DESC');

        return $qb->getQuery()->getResult();
    }

    public function findBetweenDatesAndUser(User $user, \DateTimeImmutable $dateStart, \DateTimeImmutable $dateEnd): array
    {
        $qb = $this->createQueryBuilder('help_task')
            ->where('help_task.user = :user')
            ->andWhere('((help_task.start BETWEEN :start AND :end) OR (help_task.end BETWEEN :start AND :end))')
            ->setParameter('user', $user)
            ->setParameter('start', $dateStart)
            ->setParameter('end', $dateEnd);

        return $qb->getQuery()->getResult();
    }

    public function findBetweenDatesOnly(\DateTimeImmutable $dateStart, \DateTimeImmutable $dateEnd): array
    {
        $qb = $this->createQueryBuilder('help_task');
        $qb->where($qb->expr()->between('help_task.start', ':start', ':end'))
            ->andWhere($qb->expr()->isNotNull('help_task.answer'))
            ->andWhere($qb->expr()->like('help_task.answer', ':answer'))
            ->setParameters(['start' => $dateStart, 'end' => $dateEnd])
            ->setParameter('answer', '%"data"%');

        return $qb->getQuery()->getResult();
    }
}
