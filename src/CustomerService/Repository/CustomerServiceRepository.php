<?php

namespace App\CustomerService\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class CustomerServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findAllAvailable(): array
    {
        $qb = $this->createQueryBuilder('user');

        $qb->andWhere(
            $qb->expr()->orX(
                'JSON_CONTAINS('.User::ALIAS.'.roles, :role1) = 1',
                'JSON_CONTAINS('.User::ALIAS.'.roles, :role2) = 1',
            )
        )
        ->setParameter('role1', '"ROLE_CUSTOMER_SERVICE"')
        ->setParameter('role2', '"ROLE_ADMIN"');

        return $qb
            ->getQuery()
            ->getResult();
    }
}
