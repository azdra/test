<?php

namespace App\CustomerService\Repository;

use App\CustomerService\Entity\AvailabilityUser;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AvailabilityUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AvailabilityUser::class);
    }

    public function findAvailabilityUserBetweenDates(User $user, \DateTimeImmutable $dateStart, \DateTimeImmutable $dateEnd): array
    {
        $qb = $this->createQueryBuilder('availability_user')
                   ->where('availability_user.start >= :start')
                   ->andWhere('availability_user.end <= :end')
                   ->andWhere('availability_user.user = :user')
                   ->setParameter('user', $user)
                   ->setParameter('start', $dateStart)
                   ->setParameter('end', $dateEnd);

        return $qb->getQuery()->getResult();
    }

    public function findAvailabilityBetweenDates(\DateTimeImmutable $dateStart, \DateTimeImmutable $dateEnd): array
    {
        $qb = $this->createQueryBuilder('availability_user')
            ->where('availability_user.start <= :start')
            ->andWhere('availability_user.end >= :end')
            ->setParameter('start', $dateStart)
            ->setParameter('end', $dateEnd);

        return $qb->getQuery()->getResult();
    }
}
