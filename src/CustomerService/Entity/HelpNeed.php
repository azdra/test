<?php

namespace App\CustomerService\Entity;

use App\Entity\Client\Client;
use App\Entity\ProviderSession;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class HelpNeed
{
    public const TYPE_DIRECT_NEED = 0;
    public const TYPE_ASSISTANCE = 1;
    public const TYPE_SUPPORT = 2;
    public const TYPE_CUSTOMER_REQUIREMENT = 3;

    public const STATUS_NOT_STARTED = 0;
    public const STATUS_STARTED_NOT_TAKEN = 1;
    public const STATUS_TAKEN = 2;
    public const STATUS_ENDED = 3;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'help_need:read', 'help_task:read'])]
    protected int $id;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['help_need:read', 'help_task:read'])]
    protected ?\DateTimeImmutable $start;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['help_need:read', 'help_task:read'])]
    protected ?\DateTimeImmutable $end;

    #[ORM\Column(type: 'integer')]
    #[Groups(['help_need:read', 'help_task:read', 'read_session'])]
    protected int $type = self::TYPE_DIRECT_NEED;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'helpNeeds')]
    #[Groups(['help_need:read'])]
    protected Client $client;

    #[ORM\Column(type: 'object', nullable: true)]
    #[Groups(['help_need:read', 'help_task:read'])]
    protected ?array $additionalInformation = [];

    #[ORM\ManyToOne(targetEntity: ProviderSession::class, inversedBy: 'helpNeeds')]
    #[Groups(['help_need:read'])]
    protected ?ProviderSession $session;

    #[ORM\OneToMany(mappedBy: 'helpNeed', targetEntity: HelpTask::class, cascade: ['all'])]
    #[Groups(['read_session', 'help_need:read'])]
    protected Collection $helpTasks;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->helpTasks = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getStart(): ?\DateTimeImmutable
    {
        return $this->start;
    }

    public function setStart(?\DateTimeImmutable $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeImmutable
    {
        return $this->end;
    }

    public function setEnd(?\DateTimeImmutable $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getSession(): ?ProviderSession
    {
        return $this->session;
    }

    public function setSession(?ProviderSession $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getHelpTasks(): ArrayCollection|Collection
    {
        return $this->helpTasks;
    }

    public function setHelpTasks(ArrayCollection|Collection $helpTasks): self
    {
        $this->helpTasks = $helpTasks;

        return $this;
    }

    public function getAdditionalInformation(): ?array
    {
        return $this->additionalInformation;
    }

    public function setAdditionalInformation(?array $additionalInformation): self
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }

    #[Groups(['help_need:read'])]
    public function getStatus(): int
    {
        $dateNow = new \DateTime();
        if ($dateNow < $this->start) {
            return self::STATUS_NOT_STARTED;
        }

        if ($dateNow > $this->end) {
            return self::STATUS_ENDED;
        }

        /** @var HelpTask $helpTask */
        foreach ($this->helpTasks as $helpTask) {
            if ($dateNow >= $helpTask->getStart() && $dateNow < $helpTask->getEnd() && $helpTask->getDateTakingAssistance()) {
                return self::STATUS_TAKEN;
            }
        }

        return self::STATUS_STARTED_NOT_TAKEN;
    }
}
