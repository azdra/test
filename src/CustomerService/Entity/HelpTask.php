<?php

namespace App\CustomerService\Entity;

use App\Entity\ProviderSession;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class HelpTask
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'help_task:read', 'help_need:read'])]
    protected int $id;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['help_task:read', 'help_need:read'])]
    protected \DateTimeImmutable $start;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['help_task:read', 'help_need:read'])]
    protected \DateTimeImmutable $end;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['help_need:read', 'help_task:read'])]
    protected ?\DateTimeImmutable $startReal;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['help_need:read', 'help_task:read'])]
    protected ?\DateTimeImmutable $endReal;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(['read_session', 'help_need:read', 'help_task:read'])]
    protected ?int $durationReal = 0;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups(['help_need:read', 'help_task:read'])]
    protected ?\DateTimeImmutable $dateTakingAssistance = null;

    #[ORM\ManyToOne(targetEntity: HelpNeed::class, inversedBy: 'helpTasks')]
    #[Groups(['help_task:read'])]
    protected HelpNeed $helpNeed;

    #[ORM\OneToMany(mappedBy: 'helpTask', targetEntity: EvaluationPerformanceAnimatorAnswer::class, cascade: ['all'])]
    protected Collection $evaluationAnimatorAnswer;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'helpTasks')]
    #[Groups(['read_session', 'help_task:read', 'help_need:read'])]
    protected User $user;

    #[ORM\Column(type: 'json')]
    private array $answer = [];

    public function __construct(HelpNeed $helpNeed, User $user)
    {
        $this->helpNeed = $helpNeed;
        $this->user = $user;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getStart(): \DateTimeImmutable
    {
        return $this->start;
    }

    public function setStart(\DateTimeImmutable $start): self
    {
        $this->start = $start;
        $this->setStartReal($start);

        return $this;
    }

    public function getEnd(): \DateTimeImmutable
    {
        return $this->end;
    }

    public function setEnd(\DateTimeImmutable $end): self
    {
        $this->end = $end;
        $this->setEndReal($end);

        return $this;
    }

    public function getHelpNeed(): HelpNeed
    {
        return $this->helpNeed;
    }

    public function setHelpNeed(HelpNeed $helpNeed): self
    {
        $this->helpNeed = $helpNeed;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAnswer(): array
    {
        return $this->answer;
    }

    public function setAnswer(array $answer): void
    {
        $this->answer = $answer;
    }

    public function getStartReal(): ?\DateTimeImmutable
    {
        return $this->startReal;
    }

    public function setStartReal(?\DateTimeImmutable $startReal): self
    {
        $this->startReal = $startReal;
        $this->setDurationReal();

        return $this;
    }

    public function getEndReal(): ?\DateTimeImmutable
    {
        return $this->endReal;
    }

    public function setEndReal(?\DateTimeImmutable $endReal): self
    {
        $this->endReal = $endReal;
        $this->setDurationReal();

        return $this;
    }

    public function getDurationReal(): ?int
    {
        return $this->durationReal;
    }

    public function setDurationReal(?int $durationReal = null): self
    {
        if ($durationReal == null && !empty($this->endReal) && !empty($this->startReal)) {
            $durationReal = $this->getEndReal()->diff($this->getStartReal());
            $this->durationReal = ($durationReal->d * 24 + $durationReal->h) * 60 + $durationReal->i;
        } else {
            $this->durationReal = $durationReal;
        }

        return $this;
    }

    public function getDateTakingAssistance(): ?\DateTimeImmutable
    {
        return $this->dateTakingAssistance;
    }

    public function setDateTakingAssistance(?\DateTimeImmutable $dateTakingAssistance): self
    {
        $this->dateTakingAssistance = $dateTakingAssistance;

        return $this;
    }

    public function getSession(): ?ProviderSession
    {
        return $this->getHelpNeed()->getSession();
    }

    public function getEvaluationAnimatorAnswer(): Collection
    {
        return $this->evaluationAnimatorAnswer;
    }

    public function setEvaluationAnimatorAnswer(Collection $evaluationAnimatorAnswer): self
    {
        $this->evaluationAnimatorAnswer = $evaluationAnimatorAnswer;

        return $this;
    }

    public function getType(): string
    {
        return match ($this->getHelpNeed()->getType()) {
            HelpNeed::TYPE_SUPPORT => 'S',
            HelpNeed::TYPE_ASSISTANCE => 'A',
            default => 'P',
        };
    }
}
