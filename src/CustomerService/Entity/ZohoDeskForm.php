<?php

namespace App\CustomerService\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class ZohoDeskForm
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    protected int $id;

    #[ORM\Column(type: 'text', nullable: true)]
    protected ?string $codeHTML = null;

    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    protected bool $active = true;

    public function __construct()
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCodeHTML(): ?string
    {
        return $this->codeHTML;
    }

    public function setCodeHTML(?string $codeHTML): self
    {
        $this->codeHTML = $codeHTML;

        return $this;
    }
}
