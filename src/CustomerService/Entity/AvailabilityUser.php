<?php

namespace App\CustomerService\Entity;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class AvailabilityUser
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['availability_user:read', 'help_need:read'])]
    protected int $id;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['availability_user:read', 'help_need:read'])]
    protected \DateTimeImmutable $start;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['availability_user:read', 'help_need:read'])]
    protected \DateTimeImmutable $end;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups(['help_need:read', 'availability_user:read'])]
    protected ?int $duration = 0;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'availabilityUsers')]
    #[Groups(['availability_user:read', 'help_need:read'])]
    protected User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getStart(): \DateTimeImmutable
    {
        return $this->start;
    }

    public function setStart(\DateTimeImmutable $start): self
    {
        $this->start = $start;
        $this->setDuration();

        return $this;
    }

    public function getEnd(): \DateTimeImmutable
    {
        return $this->end;
    }

    public function setEnd(\DateTimeImmutable $end): self
    {
        $this->end = $end;
        $this->setDuration();

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration = null): self
    {
        if ($duration == null && !empty($this->end) && !empty($this->start)) {
            $duration = $this->getEnd()->diff($this->getStart());
            $this->duration = ($duration->d * 24 + $duration->h) * 60 + $duration->i;
        } else {
            $this->duration = $duration;
        }

        return $this;
    }
}
