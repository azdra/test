<?php

namespace App\CustomerService\Entity;

use App\Entity\ProviderParticipantSessionRole;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity()]
class EvaluationPerformanceAnimatorAnswer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_evaluation_animator'])]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['read_evaluation_animator'])]
    private string $surveyId;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['read_evaluation_animator'])]
    private \DateTime $replyDate;

    #[ORM\ManyToOne(targetEntity: ProviderParticipantSessionRole::class)]
    #[Groups(['read_evaluation_animator'])]
    private ProviderParticipantSessionRole $providerParticipantSessionRole;

    #[ORM\ManyToOne(targetEntity: HelpTask::class, inversedBy: 'evaluationAnimatorAnswer')]
    #[Groups(['read_evaluation_animator'])]
    protected HelpTask $helpTask;

    #[ORM\Column(type: 'json')]
    #[Groups(['read_evaluation_animator'])]
    private array $answer = [];

    public function __construct(HelpTask $helpTask, ProviderParticipantSessionRole $providerParticipantSessionRole, $evaluationPerformanceAssistanceFormId)
    {
        $this->providerParticipantSessionRole = $providerParticipantSessionRole;
        $this->surveyId = $evaluationPerformanceAssistanceFormId;
        $this->helpTask = $helpTask;
        $this->replyDate = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getReplyDate(): \DateTime
    {
        return $this->replyDate;
    }

    public function setReplyDate(\DateTime $replyDate): self
    {
        $this->replyDate = $replyDate;

        return $this;
    }

    public function getProviderParticipantSessionRole(): ProviderParticipantSessionRole
    {
        return $this->providerParticipantSessionRole;
    }

    public function setProviderParticipantSessionRole(ProviderParticipantSessionRole $providerParticipantSessionRole): self
    {
        $this->providerParticipantSessionRole = $providerParticipantSessionRole;

        return $this;
    }

    public function getAnswer(): array
    {
        return $this->answer;
    }

    public function setAnswer(array $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function getSurveyId(): string
    {
        return $this->surveyId;
    }

    public function setSurveyId(string $surveyId): self
    {
        $this->surveyId = $surveyId;

        return $this;
    }

    public function getHelpTask(): HelpTask
    {
        return $this->helpTask;
    }

    public function setHelpTask(HelpTask $helpTask): self
    {
        $this->helpTask = $helpTask;

        return $this;
    }
}
