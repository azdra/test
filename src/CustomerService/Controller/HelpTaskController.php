<?php

namespace App\CustomerService\Controller;

use App\CustomerService\Entity\EvaluationPerformanceAnimatorAnswer;
use App\CustomerService\Entity\HelpTask;
use App\CustomerService\Entity\ZohoDeskForm;
use App\CustomerService\Repository\AvailabilityUserRepository;
use App\CustomerService\Repository\HelpTaskRepository;
use App\CustomerService\Service\HelpNeedService;
use App\CustomerService\Service\HelpTaskService;
use App\Entity\ActivityLog;
use App\Model\ApiResponseTrait;
use App\Repository\ClientRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\ProviderParticipantSessionRoleService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Throwable;

#[Route('/customer-service/help-task')]
class HelpTaskController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private ActivityLogger $activityLogger,
        private HelpNeedService $helpNeedService,
        private HelpTaskService $helpTaskService,
        private ClientRepository $clientRepository,
        private UserRepository $userRepository,
        private NormalizerInterface $normalizer,
        private ProviderParticipantSessionRoleService $providerParticipantSessionRoleService,
        private HelpTaskRepository $helpTaskRepository,
        private AvailabilityUserRepository $availabilityUserRepository
    ) {
    }

    #[Route('/details/{id}', name: '_help_task_details', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function helpTaskDetails(HelpTask $helpTask): Response
    {
        $fullAnswer = $helpTask->getAnswer();
        $accountConcerned = [];
        if (array_key_exists('clientIds', $helpTask->getHelpNeed()->getAdditionalInformation()) && $helpTask->getHelpNeed()->getAdditionalInformation()['clientIds'] !== '') {
            foreach ($helpTask->getHelpNeed()->getAdditionalInformation()['clientIds'] as $clientId) {
                $currentClient = $this->clientRepository->findOneBy(['id' => $clientId]);
                $accountConcerned[$clientId] = $currentClient->getName();
            }
        }

        return $this->render('customer-service/help-task-details.html.twig', [
            'title' => '',
            'helpTask' => $helpTask,
            'answer' => $fullAnswer['data'] ?? [],
            'zohoDeskForm' => $this->entityManager->getRepository(ZohoDeskForm::class)->findOneBy(['active' => true]),
            'accountConcerned' => $accountConcerned,
        ]);
    }

    #[Route('/update-real-duration/{id}', name: '_help_task_update_real_duration', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function helpTaskUpdateRealDuration(Request $request, HelpTask $helpTask, LoggerInterface $logger): Response
    {
        try {
            if ($request->query->get('dateStartReal') !== null) {
                $helpTask->setStartReal(new DateTimeImmutable($request->query->get('dateStartReal')));
            }
            if ($request->query->get('dateEndReal') !== null) {
                $helpTask->setEndReal(new DateTimeImmutable($request->query->get('dateEndReal')));
            }
            $this->entityManager->flush();
        } catch (Throwable $exception) {
            $logger->error($exception, [
                'help need ID' => $helpTask->getId(),
            ]);

            return $this->apiErrorResponse('An error has occurred during update the real duration.');
        }

        return $this->apiResponse('Real duration have been recorded.');
    }

    #[Route('/{id}/log-book/save', name: '_log_book_save', options: ['expose' => true])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function saveLogBook(HelpTask $helpTask, Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $data['replyDate'] = (new \DateTimeImmutable())->format('m/d/Y H:i');
        $helpTask->setAnswer($data);
        $this->entityManager->flush();

        return $this->apiResponse([]);
    }

    #[Route('/{id}/taking/assistance', name: '_help_task_taking_assistance', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function takingAssistance(Request $request, HelpTask $helpTask, LoggerInterface $logger, NormalizerInterface $normalizer, $evaluationPerformanceAssistanceFormId, $evaluationPerformanceEnglishAssistanceFormId): Response
    {
        try {
            $isAlreadyTaken = $request->query->getBoolean('isAlreadyTaken');

            if ($isAlreadyTaken) {
                $helpTask->setDateTakingAssistance(null);
            } else {
                $helpTask->setDateTakingAssistance(new DateTimeImmutable());
            }
            if ($helpTask->getSession()->getEvaluationPerformanceSent() == null) {
                $this->activityLogger->initActivityLogContext($helpTask->getHelpNeed()->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
                $this->helpTaskService->sendMailEvaluationAssistance($helpTask, $evaluationPerformanceAssistanceFormId, $evaluationPerformanceEnglishAssistanceFormId);
                $this->activityLogger->flushActivityLogs();
            }
            $this->entityManager->flush();
        } catch (Throwable $exception) {
            $message = 'An error has occurred, The taking of assistance is not taken into account';
            $logger->error($exception, [
                'helpNeedID' => $helpTask->getId(),
            ]);
            $this->addFlash('error', $message);

            return $this->apiErrorResponse($message, extraData: $exception->getMessage());
        }

        $session = $helpTask->getSession();

        return $this->apiResponse([
            'session' => $normalizer->normalize($session, context: ['groups' => ['read_session']]),
        ]);
    }

    #[Route('/calendar/personal', name: '_help_task_calendar_personal', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function personalCustomerService(): Response
    {
        return $this->render('customer-service/personal_service_provider.html.twig', [
            'title' => 'My services',
            'select' => 'services',
            'clients' => $this->clientRepository->findAll(),
            'providerServiceUser' => $this->userRepository->findAllProviderServiceAccounts(),
        ]);
    }

    #[Route('/calendar/availability', name: '_help_task_calendar_availability', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function personalAvailabilityCustomerService(Request $request): Response
    {
        if (array_key_exists('userId', $request->query->all())) {
            $userSelectedId = $request->query->get('userId');
        } else {
            $userSelectedId = $this->getUser()->getId();
        }
        $costumerProviderInfos = $this->userRepository->findAllProviderServiceAccountsWithoutAdmins();

        return $this->render('customer-service/personal_availability_provider.html.twig', [
            'title' => 'My availability',
            'select' => 'availability',
            'userSelectedId' => $userSelectedId,
            'costumerProviders' => $costumerProviderInfos,
            'clients' => $this->clientRepository->findAll(),
            'providerServiceUser' => $this->userRepository->findAllProviderServiceAccounts(),
        ]);
    }

    #[Route('/data-calendar/personal', name: '_help_task_personal_calendar_get', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function getPersonalCalendarHelpTask(Request $request): Response
    {
        $options = array_merge($request->query->all(), ['helperUser' => $this->getUser()->getId()]);

        return $this->apiResponse([
            'availabilities' => $this->normalizer->normalize(
                $this->availabilityUserRepository->findAvailabilityUserBetweenDates(
                    $this->getUser(),
                    new \DateTimeImmutable($request->query->get('start')),
                    new \DateTimeImmutable($request->query->get('end'))
                ),
                context: ['groups' => ['availability_user:read']]
            ),
            'personal' => $this->normalizer->normalize(
                $this->helpTaskRepository->findBetweenDates($options),
                context: ['groups' => ['help_task:read']]
            ),
        ]);
    }

    #[Route('/answer/{id}/animator/{token}', name: '_animator_evaluation_answer')]
    public function accessEvaluationPerformanceAnimator(
        EvaluationPerformanceAnimatorAnswer $evaluationAnimatorAnswer,
        string $token,
    ): Response {
        $participantSessionRole = $this->providerParticipantSessionRoleService->getAndValidFromToken($token);

        return $this->render('customer-service/answer.animator.html.twig', [
            'title' => 'End-of-session evaluation form',
            'client' => $participantSessionRole->getSession()->getClient(),
            'evaluationAnimatorAnswer' => $evaluationAnimatorAnswer,
            'answered' => count($evaluationAnimatorAnswer->getAnswer()) != 0,
            'participantSessionRole' => $participantSessionRole,
            'providerSession' => $participantSessionRole->getSession(),
            'answer' => json_encode($evaluationAnimatorAnswer->getAnswer()),
        ]);
    }

    #[Route('/save/{id}/animator/{token}', name: '_animator_evaluation_answer_save', options: ['expose' => true])]
    public function saveEvaluationAnimator(EvaluationPerformanceAnimatorAnswer $evaluationAnimatorAnswer, string $token, Request $request): Response
    {
        $this->providerParticipantSessionRoleService->getAndValidFromToken($token);
        $evaluationAnimatorAnswer->setAnswer(json_decode($request->getContent(), true));
        $evaluationAnimatorAnswer->setReplyDate(new \DateTime());
        $this->entityManager->flush();

        return $this->apiResponse([]);
    }
}
