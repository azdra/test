<?php

namespace App\CustomerService\Controller;

use App\CustomerService\Entity\EvaluationPerformanceAnimatorAnswer;
use App\CustomerService\Entity\ZohoDeskForm;
use App\CustomerService\Repository\HelpNeedRepository;
use App\CustomerService\Repository\HelpTaskRepository;
use App\CustomerService\Service\CustomerServiceService;
use App\CustomerService\Service\DashboardService;
use App\Model\ApiResponseTrait;
use App\Repository\ClientRepository;
use App\Repository\UserRepository;
use DateInterval;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/customer-service')]
class CustomerServiceController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private CustomerServiceService $customerService,
        private DashboardService $dashboardService,
        private ClientRepository $clientRepository,
        private UserRepository $userRepository,
        private HelpNeedRepository $helpNeedRepository
    ) {
    }

    #[Route('/', name: '_service_client', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function homeServiceClientPortail(): Response
    {
        return $this->redirectToRoute('_help_task_calendar_personal');

//        return $this->render('customer-service/home.html.twig', [
//            'title' => 'Customer service portal',
//        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/dashboard', name: '_dashboard_sc', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function dashboardServiceClient(Request $request): Response
    {
        $dateStart = new DateTimeImmutable($request->query->get('startDate') ?: 'now');
        $dateEnd = $request->query->get('endDate') ? new DateTimeImmutable($request->query->get('endDate')) : (new DateTimeImmutable('now'))->add(new DateInterval('P12D'));

        $dateStart = ($dateStart)->setTime(0, 0);
        $dateEnd = ($dateEnd)->setTime(23, 59, 59);
        $params = ['client' => $request->query->get('client')];

        $data = $this->dashboardService->getSessionsIndicators($dateStart, $dateEnd, $params);

        return $this->render('customer-service/dashboard.html.twig', [
            'title' => 'Customer service dashboard',
            'data' => $data,
            'clients' => $this->clientRepository->findBy([], ['name' => 'ASC']),
            'query' => [
                'startDate' => $dateStart->format(DateTime::ATOM),
                'endDate' => $dateEnd->format(DateTime::ATOM),
                'client' => $request->query->get('client'),
            ],
        ]);
    }

    #[Route('/configuration', name: '_configuration_eval_logbook', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function configurationEvalAndLogBook($evaluationPerformanceAssistanceFormId, $logBookFormId, $evaluationPerformanceEnglishAssistanceFormId): Response
    {
        return $this->render('customer-service/configuration_eval_logbook.html.twig', [
            'title' => 'Configuration',
            'evaluationPerformance' => ['name' => "Evaluation of the provider's assistance", 'id' => $evaluationPerformanceAssistanceFormId],
            'evaluationPerformanceEnglish' => ['name' => "Evaluation of the provider's assistance English", 'id' => $evaluationPerformanceEnglishAssistanceFormId],
            'logBook' => ['name' => 'Log Book', 'id' => $logBookFormId],
            'lang' => $this->getUser()->getPreferredLang(),
            'zohoDeskForm' => $this->entityManager->getRepository(ZohoDeskForm::class)->findOneBy(['active' => true]),
        ]);
    }

    #[Route('/zoho-desk-form-update', name: '_update_html_code_form_zoho_desk', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function updateFormZohoDesk(Request $request): Response
    {
        try {
            $this->entityManager->getRepository(ZohoDeskForm::class)->findOneBy(['active' => true])->setCodeHTML($request->request->get('codeHTML'));
            $this->entityManager->flush();

            return $this->apiResponse(['success' => true]);
        } catch (\Throwable $throwable) {
            return $this->apiErrorResponse($throwable->getMessage());
        }
    }

    #[Route('/billing', name: '_billing_claimant', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function billingClaimant(Request $request): Response
    {
        //Si dates vides alors de - 60 jours à + 12 jours
        //$dateStart = $request->query->get('startDate') ? new DateTimeImmutable($request->query->get('startDate')) : (new DateTimeImmutable('now'))->sub(new DateInterval('P60D'));
        //$dateEnd = $request->query->get('endDate') ? new DateTimeImmutable($request->query->get('endDate')) : (new DateTimeImmutable('now'))->add(new DateInterval('P12D'));
        $dateStart = $request->query->get('startDate') ? new DateTimeImmutable($request->query->get('startDate')) : (new DateTimeImmutable('now'))->modify('first day of -3 month');
        $dateEnd = $request->query->get('endDate') ? new DateTimeImmutable($request->query->get('endDate')) : (new DateTimeImmutable('now'));
        $dateStart = ($dateStart)->setTime(0, 0);
        $dateEnd = ($dateEnd)->setTime(23, 59, 59);

        $userCustomerService = $this->getUser();
        //For test in develop
        //$userCustomerService = $this->userRepository->findOneBy(['email' => 'thierry.chmonfils@gmail.com']);
        $params = [
            'user' => $userCustomerService,
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
            'directHelpsNeed' => $this->helpNeedRepository->findDirectNeedBetweenDates($dateStart, $dateEnd),
        ];

        $sessionsFiltered = $this->customerService->getBillingSessions($dateStart, $dateEnd);
        $allDataBillingClaimant = $this->customerService->getBillingClaimant($sessionsFiltered, $params);
        $allDataBillingClaimantFromSA = $this->customerService->getBillingClaimantFromSA($sessionsFiltered, $params);

        $groupData['claimantView'] = $allDataBillingClaimant;
        $groupData['saView'] = $allDataBillingClaimantFromSA;

        return $this->render('customer-service/billing_claimant.html.twig', [
            'title' => 'Billing',
            'lang' => $this->getUser()->getPreferredLang(),
            'allDataBillingClaimant' => $groupData,
            'query' => [
                'startDate' => $dateStart->format(DateTime::ATOM),
                'endDate' => $dateEnd->format(DateTime::ATOM),
            ],
        ]);
    }

    #[Route('/billing-tpm', name: '_billing_export_tpm', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function billingExportTPM(Request $request): Response
    {
        //Si dates vides alors de - 60 jours à + 12 jours
        //$dateStart = $request->query->get('startDate') ? new DateTimeImmutable($request->query->get('startDate')) : (new DateTimeImmutable('now'))->sub(new DateInterval('P60D'));
        //$dateEnd = $request->query->get('endDate') ? new DateTimeImmutable($request->query->get('endDate')) : (new DateTimeImmutable('now'))->add(new DateInterval('P12D'));
        $dateStart = $request->query->get('startDate') ? new DateTimeImmutable($request->query->get('startDate')) : (new DateTimeImmutable('now'))->modify('first day of -3 month');
        $dateEnd = $request->query->get('endDate') ? new DateTimeImmutable($request->query->get('endDate')) : (new DateTimeImmutable('now'));
        $dateStart = ($dateStart)->setTime(0, 0);
        $dateEnd = ($dateEnd)->setTime(23, 59, 59);
        $directHelpsNeed = $this->helpNeedRepository->findDirectNeedBetweenDates($dateStart, $dateEnd);

        $sessionsFiltered = $this->customerService->getBillingSessions($dateStart, $dateEnd);
        $allDataBillingClaimant = $this->customerService->getBillingClaimant($sessionsFiltered, ['dateStart' => $dateStart, 'dateEnd' => $dateEnd, 'directHelpsNeed' => $directHelpsNeed]);
        $allDataBillingClaimantFromSA = $this->customerService->getBillingClaimantFromSA($sessionsFiltered, ['dateStart' => $dateStart, 'dateEnd' => $dateEnd, 'directHelpsNeed' => $directHelpsNeed]);

        $groupData['claimantView'] = $allDataBillingClaimant;
        $groupData['saView'] = $allDataBillingClaimantFromSA;

        return $this->render('customer-service/billing_export_tpm.html.twig', [
            'title' => 'Billing',
            'lang' => $this->getUser()->getPreferredLang(),
            'allDataBillingClaimant' => $groupData,
            'query' => [
                'startDate' => $dateStart->format(DateTime::ATOM),
                'endDate' => $dateEnd->format(DateTime::ATOM),
            ],
        ]);
    }

    #[Route('/evaluations', name: '_evaluation_animators_results', methods: ['GET'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function evaluationAnimatorsResults($evaluationPerformanceAssistanceFormId): Response
    {
        $allEvaluationPerformanceAnimatorAnswer = $this->entityManager->getRepository(EvaluationPerformanceAnimatorAnswer::class)->findAll();

        return $this->render('customer-service/evaluation_animators_results.html.twig', [
            'title' => 'Evaluation',
            'answers' => $this->customerService->getAnswersAnimator($allEvaluationPerformanceAnimatorAnswer),
            'surveyId' => $evaluationPerformanceAssistanceFormId,
            'allEvaluationPerformanceAnimatorAnswer' => $allEvaluationPerformanceAnimatorAnswer,
        ]);
    }

    #[Route('/log-book', name: '_log_book_results', methods: ['GET'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function logBookResults($logBookFormId, HelpTaskRepository $helpTaskRepository): Response
    {
        return $this->render('customer-service/log_book_results.html.twig', [
            'title' => 'Log Book',
            'answers' => $this->customerService->getLogBookAnswers($helpTaskRepository->findAllOrderDate()),
            'surveyId' => $logBookFormId,
        ]);
    }
}
