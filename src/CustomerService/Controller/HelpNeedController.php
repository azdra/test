<?php

namespace App\CustomerService\Controller;

use App\CustomerService\Entity\AvailabilityUser;
use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Entity\HelpTask;
use App\CustomerService\Repository\AvailabilityUserRepository;
use App\CustomerService\Repository\HelpNeedRepository;
use App\CustomerService\Repository\HelpTaskRepository;
use App\CustomerService\Service\AvailabilityCostumerProviderService;
use App\CustomerService\Service\CustomerServiceService;
use App\CustomerService\Service\HelpNeedService;
use App\CustomerService\Service\HelpTaskService;
use App\CustomerService\Service\MassAssignService;
use App\Entity\Client\ClientServices;
use App\Entity\User;
use App\Model\ApiResponseTrait;
use App\Repository\ClientRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\ProviderSessionService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Exception\MissingOptionsException;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/customer-service/help-need')]
#[IsGranted('ROLE_CUSTOMER_SERVICE')]
class HelpNeedController extends AbstractController
{
    use ApiResponseTrait;
    public const GROUP_VIEW = 'customer-service:provider:view';

    public function __construct(
        private HelpNeedRepository $helpNeedRepository,
        private NormalizerInterface $normalizer,
        private ClientRepository $clientRepository,
        private UserRepository $userRepository,
        private LoggerInterface $logger,
        private HelpTaskService $helpTaskService,
        private HelpTaskRepository $helpTaskRepository,
        private ProviderSessionService $providerSessionService,
        private HelpNeedService $helpNeedService,
        private EntityManagerInterface $entityManager,
        private TranslatorInterface $translator,
        private ActivityLogger $activityLogger,
        private MassAssignService $massAssignService,
        private AvailabilityUserRepository $availabilityUserRepository,
        private AvailabilityCostumerProviderService $availabilityCostumerProviderService,
        private CustomerServiceService $customerServiceService,
        private ProviderSessionRepository $providerSessionRepository
    ) {
    }

    #[Route('/calendar', name: '_help_need_calendar', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function planningCostumerService(Request $request): Response
    {
        $active = true;
        if (array_key_exists('active', $request->query->all())) {
            $active = $request->query->get('active') !== 'false';
        }
        $activeUserStatus = $active ? [User::STATUS_ACTIVE] : [User::STATUS_ACTIVE, User::STATUS_INACTIVE];
        $allProviderServiceAccounts = $this->userRepository->findAllProviderServiceAccounts(activeUser: $activeUserStatus);
        $costumerProviderInfos = $this->availabilityCostumerProviderService->availabilityAndHelpTaskUser(activeUser: $activeUserStatus);

        return $this->render('customer-service/planning_service_provider.html.twig', [
            'title' => 'Planning',
            'activeUser' => $active,
            'clients' => $this->clientRepository->findAll(),
            'providerServiceUser' => $allProviderServiceAccounts,
            'languages' => User::LANGUAGES,
            'costumerProviders' => $costumerProviderInfos['costumerProvider'],
            'eventSources' => $costumerProviderInfos['eventSources'],
        ]);
    }

    #[Route('/data-calendar', name: '_help_need_calendar_get', options: ['expose' => true], methods: ['GET'])]
    public function getCalendarHelpNeed(Request $request): Response
    {
        $helpNeed = $this->helpNeedRepository->findBetweenDates($request->query->all());
        $helpNeed = $this->normalizer->normalize(
            $helpNeed,
            context: ['groups' => ['help_need:read']]
        );
        $helpNeed = array_map(function (array $helpNeed) {
            if ($helpNeed['additionalInformation'] && $helpNeed['additionalInformation']['clientIds']) {
                $helpNeed['additionalInformation']['clientIds'] = array_map(function (int $clientId) {
                    return $this->normalizer->normalize(
                        $this->clientRepository->find($clientId),
                        context: ['groups' => ['ultra:lite:read']]);
                }, $helpNeed['additionalInformation']['clientIds']);
            }

            return $helpNeed;
        }, $helpNeed);

        return $this->apiResponse($helpNeed);
    }

    #[Route('/{id}/assign-user/{userId}', name: '_help_need_assigned_user', options: ['expose' => true], methods: ['POST'])]
    #[ParamConverter('user', options: ['mapping' => ['userId' => 'id']])]
    #[IsGranted('ROLE_ADMIN')]
    public function assignedProviderToHelpNeed(HelpNeed $helpNeed, User $user): Response
    {
        $helpTask = $this->helpTaskService->assignedUserToHelpNeed($helpNeed, $user);
        $this->entityManager->flush();

        return $this->apiResponse([
            'helpTask' => $this->normalizer->normalize($helpTask, context: ['groups' => ['help_need:read']]),
            'message' => $this->translator->trans('Service has been successfully created'),
        ]);
    }

    #[Route('/unassigned/{id}', name: '_help_need_unassigned_user', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function unassignedProviderToHelpNeed(HelpTask $service): Response
    {
        $this->entityManager->remove($service);
        $this->entityManager->flush();

        return $this->apiResponse(['message' => $this->translator->trans('Service has been successfully deleted')]);
    }

    #[Route('/mass-assign', name: '_help_need_mass_assign', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function massAssignProviderToHelNeed(Request $request): Response
    {
        try {
            $this->massAssignService->massAssign($request->request->all());

            $this->entityManager->flush();
        } catch (MissingOptionsException $ex) {
            $this->logger->error($ex);

            return $this->apiErrorResponse($this->translator->trans('Missing attributes, fill the data properly.'));
        }

        return $this->apiResponse([]);
    }

    #[Route('/mass-assign-service-delivery', name: '_help_need_assign_service_delivery', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function assignServiceDeliveryToHelNeedDirect(Request $request): Response
    {
        try {
            $helpDirectNeed = $this->massAssignService->assignServiceDelivery($request->request->all());

            $this->entityManager->persist($helpDirectNeed);
            $this->entityManager->flush();
        } catch (MissingOptionsException $ex) {
            $this->logger->error($ex);

            return $this->apiErrorResponse($this->translator->trans('Missing attributes, fill the data properly.'));
        }

        return $this->apiResponse([]);
    }

    #[Route('/help-need-delete/{id}', name: '_help_need_delete', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function deleteHelNeedDirect(Request $request, HelpNeed $helpDirectNeed): Response
    {
        try {
            foreach ($helpDirectNeed->getHelpTasks() as $helpTask) {
                $this->entityManager->remove($helpTask);
            }
            $this->entityManager->remove($helpDirectNeed);
            $this->entityManager->flush();
        } catch (MissingOptionsException $ex) {
            $this->logger->error($ex);

            return $this->apiErrorResponse($this->translator->trans('A problem occurred while deleting the service.'));
        }

        return $this->apiResponse([]);
    }

    #[Route('/declare-availabilities-customer-service', name: '_declare_availabilities_customer_service', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function declareAvailabilitiesCustomerService(Request $request): Response
    {
        try {
            if (
                array_key_exists('dateStart', $request->request->all()) &&
                array_key_exists('dateEnd', $request->request->all()) &&
                array_key_exists('user', $request->request->all())) {
                /** @var User $user */
                $user = $this->userRepository->findOneBy(['id' => $request->request->get('user')]);
                $availability = (new AvailabilityUser($user))
                    ->setStart((new \DateTimeImmutable($request->request->get('dateStart')))->setTimezone(new \DateTimeZone($user->getClient()->getTimezone())))
                    ->setEnd((new \DateTimeImmutable($request->request->get('dateEnd')))->setTimezone(new \DateTimeZone($user->getClient()->getTimezone())));
                $this->entityManager->persist($availability);
                $this->entityManager->flush();
            }
        } catch (MissingOptionsException $ex) {
            $this->logger->error($ex);

            return $this->apiErrorResponse($this->translator->trans('Missing attributes, fill the data properly.'));
        }

        return $this->apiResponse([]);
    }

    #[Route('/data-calendar/availabilities', name: '_availabilities_personal_calendar_get', options: ['expose' => true], methods: ['GET'])]
    public function getPersonalCalendarHelpTask(Request $request): Response
    {
        return $this->apiResponse($this->normalizer->normalize(
            $this->availabilityUserRepository->findAvailabilityUserBetweenDates(
                $this->userRepository->findOneBy(['id' => $request->query->get('user')]),
                new \DateTimeImmutable($request->query->get('start')),
                new \DateTimeImmutable($request->query->get('end'))
            ),
            context: ['groups' => ['availability_user:read']]
        ));
    }

    #[Route('/update-availability-customer-service', name: '_update_availability_customer_service', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function updateAvailabilityCustomerService(Request $request): Response
    {
        try {
            if (array_key_exists('id', $request->request->all()) &&
                array_key_exists('dateStart', $request->request->all()) &&
                array_key_exists('dateEnd', $request->request->all())
            ) {
                $availabilityUser = $this->availabilityUserRepository->findOneBy(['id' => $request->request->get('id')]);
                /** @var User $user */
                $user = $this->getUser();
                $helpTaskUser = $this->helpTaskRepository->findBetweenDatesAndUser($user, $availabilityUser->getStart(), $availabilityUser->getEnd());

                if ($helpTaskUser) {
                    return $this->apiErrorResponse($this->translator->trans('You cannot modify this availability because it is already assigned to a task.'));
                } else {
                    $availabilityUser
                        ->setStart((new \DateTimeImmutable($request->request->get('dateStart')))
                            ->setTimezone(new \DateTimeZone($user->getClient()->getTimezone())))
                        ->setEnd((new \DateTimeImmutable($request->request->get('dateEnd')))
                            ->setTimezone(new \DateTimeZone($user->getClient()->getTimezone())));
                    $this->entityManager->flush();
                }
            }
        } catch (\Throwable $throwable) {
            $this->logger->error($throwable);

            return $this->apiErrorResponse($this->translator->trans($throwable->getMessage()));
        }

        return $this->apiResponse([]);
    }

    #[Route('/delete-availability-customer-service', name: '_delete_availability_customer_service', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_CUSTOMER_SERVICE')]
    public function deleteAvailabilityCustomerService(Request $request): Response
    {
        try {
            if (array_key_exists('id', $request->request->all())) {
                $availabilityUser = $this->availabilityUserRepository->findOneBy(['id' => $request->request->get('id')]);

                $helpTaskUser = $this->helpTaskRepository->findBetweenDatesAndUser($availabilityUser->getUser(), $availabilityUser->getStart(), $availabilityUser->getEnd());
                if ($helpTaskUser) {
                    return $this->apiErrorResponse($this->translator->trans('You cannot delete this availability because it is already assigned to a task.'));
                } else {
                    $this->entityManager->remove($availabilityUser);
                    $this->entityManager->flush();
                }
            }
        } catch (\Throwable $throwable) {
            $this->logger->error($throwable);

            return $this->apiErrorResponse($this->translator->trans($throwable->getMessage()));
        }

        return $this->apiResponse([]);
    }

    #[Route('/data-calendar-information-help-need', name: '_help_need_informations_get', options: ['expose' => true], methods: ['GET'])]
    public function getHelpNeedInformations(Request $request): Response
    {
        $helpNeedInformations = [];
        $clientWithLumpSumContract = $this->entityManager->getRepository(ClientServices::class)->findBy(['lumpSumContract' => true]);
        foreach ($clientWithLumpSumContract as $client) {
            $helpNeedInformations[$client->getId()] = [
                'clientId' => $client->getId(),
                'sessionCount' => 0,
                'participantsCount' => 0,
            ];
        }
        $sessions = $this->providerSessionRepository->findByBetweenDateAndClientsList(
            new \DateTimeImmutable($request->query->get('start')), new \DateTimeImmutable($request->query->get('end')),
            $clientWithLumpSumContract
        );
        foreach ($sessions as $session) {
            $clientId = $session->getClient()->getId();
            ++$helpNeedInformations[$clientId]['sessionCount'];
            $helpNeedInformations[$clientId]['participantsCount'] += count($session->getParticipantRoles());
        }

        return $this->apiResponse($helpNeedInformations);
    }
}
