<?php

namespace App\CustomerService\Controller;

use App\CustomerService\Form\UserHelperFormType;
use App\CustomerService\Repository\AvailabilityUserRepository;
use App\CustomerService\Repository\CustomerServiceRepository;
use App\CustomerService\Repository\HelpTaskRepository;
use App\CustomerService\Service\CustomerServiceService;
use App\Entity\ActivityLog;
use App\Entity\User;
use App\Event\UserPasswordEvent;
use App\Repository\UserRepository;
use App\Security\UserSecurityService;
use App\Service\ActivityLogger;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;

#[Route('/customer-service/provider')]
#[IsGranted('ROLE_CUSTOMER_SERVICE')]
class HelperController extends AbstractController
{
    public const GROUP_VIEW = 'customer-service:provider:view';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private EventDispatcherInterface $dispatcher,
        private ActivityLogger $activityLogger,
        private HelpTaskRepository $helpTaskRepository,
        private UserSecurityService $userSecurityService,
        private AvailabilityUserRepository $availabilityUserRepository,
        private UserRepository $userRepository,
        private CustomerServiceService $customerServiceService
    ) {
    }

    #[Route('/list', name: '_helper_list', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function listHelper(Request $request, UserRepository $userRepository): Response
    {
        $helpers = array_merge($userRepository->findListByRoleTarget(User::ROLE_CUSTOMER_SERVICE), $userRepository->findListByRoleTarget(User::ROLE_HOTLINE_CUSTOMER_SERVICE));
        if ($request->query->has('pre-search')) {
            $preSearch = (string) $request->query->get('pre-search');
        }

        return $this->render('customer-service/list_helpers.html.twig', [
            'title' => 'Service providers',
            'helpers' => $helpers,
            'preSearch' => !empty($preSearch) ? $preSearch : null,
        ]);
    }

    #[Route('/create', name: '_helper_create', methods: ['POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function createUserHelper(Request $request, UserService $userService, LoggerInterface $logger): Response
    {
        $user = $userService->createUserCustomerService();

        $form = $this->createForm(UserHelperFormType::class, $user, [
            'label' => false,
        ]);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                try {
                    $isCloned = $this->userSecurityService->createOrClonePassword($user);
                    $this->entityManager->persist($user);
                    $this->entityManager->flush();

                    if (!$isCloned) {
                        $eventName = UserPasswordEvent::NEW;
                    } else {
                        $eventName = UserPasswordEvent::CLONE;
                    }

                    $this->dispatcher->dispatch(new UserPasswordEvent($user), $eventName);
                    $this->addFlash('success', 'The service provider has been successfully created');
                } catch (Throwable $e) {
                    $logger->error($e, [
                        'userId' => $user->getId(),
                    ]);
                    $this->addFlash('error', 'An error has occurred, the service provider has not been created');
                } finally {
                    return $this->redirectToRoute('_helper_list');
                }
            } else {
                $this->addFlash('error', 'An error occured, this email address already exists for this customer');

                return $this->redirectToRoute('_helper_list');
            }
        }

        return $this->render('customer-service/form_user_helper.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/delete/{id}', name: '_helper_delete', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function delete(Request $request, User $user, LoggerInterface $logger): Response
    {
        $referer = $request->headers->get('referer');

        if ($user === $this->getUser()) {
            $this->addFlash('error', 'An error has occurred, you are trying to delete your own account');

            return $this->redirect(
                $referer ?? $this->generateUrl('_helper_list')
            );
        }
        $this->activityLogger->initActivityLogContext($user->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
        try {
            $this->entityManager->remove($user);
            $this->entityManager->flush();

            $this->activityLogger->flushActivityLogs();

            $this->addFlash('success', 'The service provider has been successfully updated');
        } catch (Throwable $e) {
            $logger->error($e, [
                'userId' => $user->getId(),
            ]);
            $this->addFlash('error', 'An error occurred, the service provider has not been updated');
        }

        return $this->redirect(
            $referer ?? $this->generateUrl('_helper_list')
        );
    }

    #[Route('/activate/{id}', name: '_helper_activate_status', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function activateStatus(User $user, LoggerInterface $logger): Response
    {
        $entityManager = $this->entityManager;

        try {
            $user->setStatus(User::STATUS_ACTIVE);
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'the service provider has been activated');
        } catch (Throwable $e) {
            $logger->error($e, [
                'userId' => $user->getId(),
            ]);
            $this->addFlash('error', 'An error has occurred');
        } finally {
            return $this->redirectToRoute('_helper_list');
        }
    }

    #[Route('/deactivate/{id}', name: '_helper_deactivate_status', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function deactivateStatus(User $user, LoggerInterface $logger): Response
    {
        $entityManager = $this->entityManager;

        try {
            $user->setStatus(User::STATUS_INACTIVE);
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'the service provider has been deactivated');
        } catch (Throwable $e) {
            $logger->error($e, [
                'userId' => $user->getId(),
            ]);
            $this->addFlash('error', 'An error has occurred');
        } finally {
            return $this->redirectToRoute('_helper_list');
        }
    }

    #[Route('/edit/{id}', name: '_user_helper_edit', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_ADMIN')]
    public function edit(
        Request $request,
        LoggerInterface $logger,
        User $user,
    ): Response {
        $form = $this->createForm(UserHelperFormType::class, $user, [
            'label' => false,
        ]);
        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            if ($form->isSubmitted() && $form->isValid()) {
                $userServiceProvider = $form->getData();
                try {
                    $this->entityManager->persist($userServiceProvider);
                    $this->entityManager->flush();

                    $this->addFlash('success', 'The service provider has been successfully updated');
                } catch (Throwable $e) {
                    $logger->error($e, [
                        'userId' => $user->getId(),
                    ]);
                    $this->addFlash('error', 'An error occurred, the service provider has not been updated');
                } finally {
                    return $this->redirect($request->headers->get('referer'));
                }
            }
        }

        return $this->render('customer-service/update_user_helper.html.twig', [
            'form' => $form->createView(),
            'title' => $user->getFullName(),
        ]);
    }

    #[Route('/list-available', name: '_list_available_provider', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function availableProviders(CustomerServiceRepository $customerServiceRepository, SerializerInterface $serializer): Response
    {
        $providers = $customerServiceRepository->findAllAvailable();

        $providerSerialized = $serializer->serialize(
            $providers,
            'json',
            ['groups' => self::GROUP_VIEW]
        );

        return new Response($providerSerialized);
    }

    #[Route('/list-available-users', name: '_list_available_provider_users', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function availableProvidersBetweenDates(Request $request, SerializerInterface $serializer): Response
    {
        $availableProviders = $this->customerServiceService->allAvailableUserToAssign(
            new \DateTimeImmutable($request->query->get('start')),
            new \DateTimeImmutable($request->query->get('end'))
        );
        usort($availableProviders, function ($a, $b) {
            return $a->getFullName() <=> $b->getFullName();
        });
        $providerSerialized = $serializer->serialize(
            $availableProviders,
            'json',
            ['groups' => self::GROUP_VIEW]
        );

        return new Response($providerSerialized);
    }
}
