<?php

namespace App\CustomerService\Service;

use App\CustomerService\Entity\EvaluationPerformanceAnimatorAnswer;
use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Entity\HelpTask;
use App\Entity\ActivityLog;
use App\Entity\Client\Client;
use App\Entity\User;
use App\Model\ActivityLog\ActivityLogModel;
use App\Service\ActivityLogger;
use App\Service\MailerService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class HelpTaskService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private MailerService $mailerService,
        private ActivityLogger $activityLogger,
        private TranslatorInterface $translator,
        private RouterInterface $router
    ) {
    }

    public function assignedUserToHelpNeed(HelpNeed $helpNeed, User $user): HelpTask
    {
        /** @var HelpTask $helpTask */
        $helpTask = $this->entityManager->getRepository(HelpTask::class)->findOneBy(['helpNeed' => $helpNeed, 'user' => $user]);

        if (empty($helpTask)) {
            $helpTask = new HelpTask($helpNeed, $user);
            $helpTask->setStart($helpNeed->getStart());
            $helpTask->setEnd($helpNeed->getEnd());
            $this->entityManager->persist($helpTask);
        }

        return $helpTask;
    }

    public function createEvaluationPerformanceAnimatorAnswer(HelpTask $helpTask, array $animators, $evaluationPerformanceAssistanceFormId, $evaluationPerformanceEnglishAssistanceFormId): void
    {
        $evaluationId = match ($helpTask->getSession()->getLanguage()) {
            Client::ENGLISH_LANGUAGE => $evaluationPerformanceEnglishAssistanceFormId,
            default => $evaluationPerformanceAssistanceFormId,
        };
        foreach ($animators as $animator) {
            $evaluationPerformanceAnimatorAnswer = $this->entityManager->getRepository(EvaluationPerformanceAnimatorAnswer::class)->findOneBy(['providerParticipantSessionRole' => $animator->getId(), 'helpTask' => $helpTask->getId()]);
            if (!$evaluationPerformanceAnimatorAnswer) {
                $evaluationAnimatorAnswer = new EvaluationPerformanceAnimatorAnswer($helpTask, $animator, $evaluationId);
                $this->entityManager->persist($evaluationAnimatorAnswer);
            }
        }
        $this->entityManager->flush();
    }

    public function sendMailEvaluationAssistance(HelpTask $helpTask, $evaluationPerformanceAssistanceFormId, $evaluationPerformanceEnglishAssistanceFormId): void
    {
        $session = $helpTask->getSession();
        $provider = $helpTask->getUser();

        if (empty($session)) {
            return;
        }
        $this->createEvaluationPerformanceAnimatorAnswer($helpTask, $session->getAnimatorsOnly(), $evaluationPerformanceAssistanceFormId, $evaluationPerformanceEnglishAssistanceFormId);

        foreach ($session->getAnimatorsOnly() as $animator) {
            $evaluationPerformanceAnimatorAnswer = $this->entityManager->getRepository(EvaluationPerformanceAnimatorAnswer::class)->findOneBy(['providerParticipantSessionRole' => $animator->getId(), 'helpTask' => $helpTask->getId()]);

            $urlEvaluationAnimator = $this->router->generate('_animator_evaluation_answer', ['id' => $evaluationPerformanceAnimatorAnswer->getId(), 'token' => $evaluationPerformanceAnimatorAnswer->getProviderParticipantSessionRole()->getProviderSessionAccessToken()->getToken()]);
            $params = [
                '%userRoute%' => $this->router->generate('_participant_edit', ['id' => $animator->getParticipant()->getUserId()]),
                '%route%' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                '%sessionName%' => $session->getName(),
                '%fullNameAnimator%' => $animator->getFullName(), ];
            $infos = [
                'message' => ['key' => 'The assistance was taken and the performance assessment link was sent to the recipient <a href="%userRoute%">%fullNameAnimator%</a> for the <a href="%route%">%sessionName%</a> session', 'params' => $params],
                'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
            ];
            $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SEND_EVALUATION_PERFORMANCE, ActivityLog::SEVERITY_INFORMATION, $infos), true);

            $this->mailerService->sendTemplatedMail(
                           $animator->getEmail(),
                           $this->translator->trans('Assistance Rating'),
                           'emails/costumer_service/evaluation_performance_assistance.twig',
                context: [
                               'url' => $urlEvaluationAnimator,
                               'user' => $animator->getParticipant()->getUser(),
                               'sessionName' => $session->getName(),
                               'providerName' => $provider->getFullName(),
                           ],
                metadatas: [
                               'origin' => MailerService::ORIGIN_EMAIL_SEND_SESSION_UNCANCELED,
                               'user' => $animator->getParticipant()->getUser()->getId(),
                               'sessionName' => $session->getName(),
                               'providerName' => $provider->getFullName(),
                           ],
            );
        }

        $session->setEvaluationPerformanceSent(new DateTimeImmutable());
    }
}
