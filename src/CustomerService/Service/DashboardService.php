<?php

namespace App\CustomerService\Service;

use App\CustomerService\Entity\EvaluationPerformanceAnimatorAnswer;
use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Entity\HelpTask;
use App\CustomerService\Repository\HelpNeedRepository;
use App\CustomerService\Repository\HelpTaskRepository;
use App\Entity\EvaluationParticipantAnswer;
use App\Entity\ProviderSession;
use App\Repository\EvaluationParticipantAnswerRepository;
use App\Repository\EvaluationPerformanceAnimatorAnswerRepository;
use App\Repository\ProviderSessionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;

class DashboardService
{
    private EvaluationParticipantAnswerRepository $evaluationParticipantAnswerRepository;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private ProviderSessionRepository $providerSessionRepository,
        private HelpTaskRepository $helpTaskRepository,
        EvaluationParticipantAnswerRepository $evaluationParticipantAnswerRepository,
        private RouterInterface $router,
        private EvaluationPerformanceAnimatorAnswerRepository $evaluationPerformanceAnimatorAnswerRepository,
        private HelpNeedRepository $helpNeedRepository
    ) {
        $this->evaluationParticipantAnswerRepository = $evaluationParticipantAnswerRepository;
    }

    public function getDoubleBookingByDate(\DateTimeImmutable $from, \DateTimeImmutable $to, array $params = []): array
    {
        $helpTasks = $this->helpTaskRepository->findDoubleBooking($from, $to, $params);
        $returnData = [];
        foreach ($helpTasks as $helptask) {
            $returnData[$helptask->getSession()->getDateStart()->format('d.m')][] = [
                'doubleBooking' => [
                    'userId' => $helptask->getUser()->getId(),
                    'userFullname' => $helptask->getUser()->getFirstName().' '.$helptask->getUser()->getLastName(),
                    'userEmail' => $helptask->getUser()->getEmail(),
                    'sessionId' => $helptask->getSession()->getId(),
                    'sessionName' => $helptask->getSession()->getName(),
                    'sessionDateStart' => $helptask->getSession()->getDateStart()->setTimezone(new \DateTimeZone($helptask->getSession()->getClient()->getTimezone()))->format('d/m/Y à H:i'),
                    'helpTaskId' => $helptask->getId(),
                    'helpNeedId' => $helptask->getHelpNeed()->getId(),
                    'urlSession' => $this->router->generate('_session_get', ['id' => $helptask->getSession()->getId()]),
                    'timeSession' => $helptask->getSession()->getDateStart()->setTimezone(new \DateTimeZone($helptask->getSession()->getClient()->getTimezone()))->format('H:i'),
                ],
            ];
        }

        return $returnData;
    }

    public function getSessionsIndicators(\DateTimeImmutable $from, \DateTimeImmutable $to, array $params = []): array
    {
        $sessions = $this->providerSessionRepository->getSessionsDashboardServices($from, $to, $params);

        /*$returnData = [
            'errors' => [
                'doubleBooking' => [
                    'users' => [
                        0 => [
                            'id' => 12,
                            'name' => 'Davy Crocket',
                            'sessions' => [
                                '0' => [
                                    'id' => 10,
                                    'name' => 'Nom de la session',
                                    'dateStart' => '10/10/2023 à 10h00'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];*/

        $returnData['alerts'] = $this->getDoubleBookingByDate($from, $to, $params);

        $nbrDirectNeedHotline = 0;
        $nbrDirectNeedCSC1 = 0;
        $nbrDirectNeedCSC2 = 0;
        $nbrTaskDirectNeedHotline = 0;
        $nbrTaskDirectNeedCSC1 = 0;
        $nbrTaskDirectNeedCSC2 = 0;
        $nbrHoursOfDirectNeedHotline = 0;
        $nbrHoursOfDirectNeedCSC1 = 0;
        $nbrHoursOfDirectNeedCSC2 = 0;

        $sessionWithoutDirectNeedHotline = 0;
        $sessionWithoutDirectNeedCSC1 = 0;
        $sessionWithoutDirectNeedCSC2 = 0;

        $nbrDirectNeedHotlineInExtern = 0;
        $nbrDirectNeedHotlineInSupportIntern = 0;
        $nbrDirectNeedCSC1InExtern = 0;
        $nbrDirectNeedCSC1InSupportIntern = 0;
        $nbrDirectNeedCSC2InExtern = 0;
        $nbrDirectNeedCSC2InSupportIntern = 0;

        $nbrNeedDirectNeeds = 0;
        $nbrTaskDirectNeeds = 0;
        $nbrNeedAssistances = 0;
        $nbrTaskAssistances = 0;
        $nbrNeedSupports = 0;
        $nbrTaskSupports = 0;
        $nbrPersonInAssistanceIntern = 0;
        $nbrPersonInAssistanceExtern = 0;
        $nbrPersonInSupportIntern = 0;
        $nbrPersonInSupportExtern = 0;
        $nbrAnimators = 0;
        $nbrTrainees = 0;
        $nbrHoursOfAssistance = 0;
        $sessionWithoutHelpTaskAssistance = 0;
        $sessionWithoutHelpTaskSupport = 0;

        $nbrLogBooks = 0;
        $nbrLogBooksPositive = 0;
        $averagePositiveFeeling = 0;
        $averageNegativeFeeling = 0;
        $nbrLogBooksNegative = 0;

        /** @var ProviderSession $session */
        foreach ($sessions as $session) {
            $nbrAnimators += count($session->getAnimatorsOnly());
            $nbrTrainees += count($session->getTraineesOnly());

            /** @var HelpNeed $helpNeed */
            foreach ($session->getHelpNeeds() as $helpNeed) {
                if ($helpNeed->getType() == HelpNeed::TYPE_DIRECT_NEED) {     //Type service
                    ++$nbrNeedDirectNeeds;
                    //Session programmée avec un helpNeed service sans helpTask
                    if (count($helpNeed->getHelpTasks()) == 0) {
                        if ($helpNeed->getAdditionalInformation()) {
                            switch ($helpNeed->getAdditionalInformation()['serviceType']) {
                                case 'hotline':
                                    ++$sessionWithoutDirectNeedHotline;
                                    break;
                                case 'csc1':
                                    ++$sessionWithoutDirectNeedCSC1;
                                    break;
                                case 'csc2':
                                    ++$sessionWithoutDirectNeedCSC2;
                                    break;
                            }
                        }
                        //++$sessionWithoutHelpTask;
                        $returnData['alerts'][$session->getDateStart()->format('d.m')][] = [
                            'sessionsWaitDirectNeed' => [
                                'id' => $session->getId(),
                                'name' => $session->getName(),
                                'dateStart' => $helpNeed->getStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y à H:i'),
                                'typeHelpNeed' => HelpNeed::TYPE_DIRECT_NEED,
                                'urlSession' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                                'timeSession' => $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('H:i'),
                            ],
                        ];
                    }
                    $nbrNeedDirectNeeds += count($helpNeed->getHelpTasks());

                    foreach ($helpNeed->getHelpTasks() as $helpTask) {
                        ++$nbrTaskDirectNeeds;
                        if ($helpTask->getUser()->isACustomerService()) {
                            ++$nbrPersonInAssistanceExtern;
                        } else {
                            ++$nbrPersonInAssistanceIntern;
                        }
                    }
                } elseif ($helpNeed->getType() == HelpNeed::TYPE_ASSISTANCE) {    //Type Assistance
                    ++$nbrNeedAssistances;
                    //TODO Sortir la note moyenne récupérer depuis le champ HelpTask >> answer
                    //Session programmée avec un helpNeed assistance sans helpTask
                    if (count($helpNeed->getHelpTasks()) == 0) {
                        ++$sessionWithoutHelpTaskAssistance;
                        $returnData['alerts'][$session->getDateStart()->format('d.m')][] = [
                            'sessionsWaitAssist' => [
                                'id' => $session->getId(),
                                'name' => $session->getName(),
                                'dateStart' => $helpNeed->getStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y à H:i'),
                                'typeHelpNeed' => HelpNeed::TYPE_ASSISTANCE,
                                'urlSession' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                                'timeSession' => $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('H:i'),
                                'clientName' => $session->getClient()->getName(),
                                'urlClient' => $this->router->generate('_client_edit', ['id' => $session->getClient()->getId()]),
                            ],
                        ];
                    }

                    foreach ($helpNeed->getHelpTasks() as $helpTask) {
                        ++$nbrTaskAssistances;
                        $nbrHoursOfAssistance += $helpTask->getDurationReal();
                        if ($helpTask->getUser()->isACustomerService()) {
                            ++$nbrPersonInAssistanceExtern;
                        } else {
                            ++$nbrPersonInAssistanceIntern;
                        }
//                        dump($helpTask->getUser()->getEmail().' | '.$helpTask->getStart()->format('d/m/Y H:i').' > '.$helpTask->getEnd()->format('d/m/Y H:i'));
                        if ($session->getStatus() == ProviderSession::STATUS_CANCELED) {
                            //Session annulée avec un ou plusieurs helpTasks programmés
                            $returnData['alerts'][$session->getDateStart()->format('d.m')][] = [
                                'sessionsCanceledWithAssist' => [
                                    'id' => $session->getId(),
                                    'name' => $session->getName(),
                                    'dateStart' => $helpNeed->getStart()->format('d/m/Y H:i'),
                                    'typeHelpNeed' => HelpNeed::TYPE_ASSISTANCE,
                                    'userHelpTaskEmail' => $helpTask->getUser()->getEmail(),
                                    'userFullname' => $helpTask->getUser()->getFirstName().' '.$helpTask->getUser()->getLastName(),
                                    'helpTaskStart' => $helpTask->getStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y à H:i'),
                                    'helpTaskEnd' => $helpTask->getEnd()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y à H:i'),
                                    'urlSession' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                                    'timeSession' => $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('H:i'),
                                ],
                            ];
                        }
                    }
                } elseif ($helpNeed->getType() == HelpNeed::TYPE_SUPPORT) {   //Type Support
                    ++$nbrNeedSupports;
                    //Session programmée avec un helpNeed support sans helpTask
                    if (count($helpNeed->getHelpTasks()) == 0) {
                        ++$sessionWithoutHelpTaskSupport;
                        $returnData['alerts'][$session->getDateStart()->format('d.m')][] = [
                            'sessionsWaitSupport' => [
                                'id' => $session->getId(),
                                'name' => $session->getName(),
                                'dateStart' => $helpNeed->getStart()->format('d/m/Y H:i'),
                                'typeHelpNeed' => HelpNeed::TYPE_SUPPORT,
                                'urlSession' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                                'timeSession' => $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('H:i'),
                            ],
                        ];
                    }
                }
                /** @var HelpTask $helpTask */
                foreach ($helpNeed->getHelpTasks() as $helpTask) {
                    if ($helpNeed->getType() == HelpNeed::TYPE_SUPPORT) {
                        ++$nbrTaskSupports;
                        if ($helpTask->getUser()->isACustomerService()) {
                            ++$nbrPersonInSupportExtern;
                        } else {
                            ++$nbrPersonInSupportIntern;
                        }
                    }
                }
            }
        }

        $helpNeedBetweenDatesOnly = $this->helpNeedRepository->findDirectNeedBetweenDates($from, $to);
        foreach ($helpNeedBetweenDatesOnly as $helpNeed) {
            if ($helpNeed->getAdditionalInformation()) {
                switch ($helpNeed->getAdditionalInformation()['serviceType']) {
                    case 'hotline':
                        ++$nbrDirectNeedHotline;
                        break;
                    case 'csc1':
                        ++$nbrDirectNeedCSC1;
                        break;
                    case 'csc2':
                        ++$nbrDirectNeedCSC2;
                        break;
                }

                foreach ($helpNeed->getHelpTasks() as $helpTask) {
                    switch ($helpNeed->getAdditionalInformation()['serviceType']) {
                        case 'hotline':
                            ++$nbrTaskDirectNeedHotline;
                            $nbrHoursOfDirectNeedHotline += $helpTask->getDurationReal();
                            if ($helpTask->getUser()->isACustomerService()) {
                                ++$nbrDirectNeedHotlineInExtern;
                            } else {
                                ++$nbrDirectNeedHotlineInSupportIntern;
                            }
                            break;
                        case 'csc1':
                            ++$nbrTaskDirectNeedCSC1;
                            $nbrHoursOfDirectNeedCSC1 += $helpTask->getDurationReal();
                            if ($helpTask->getUser()->isACustomerService()) {
                                ++$nbrDirectNeedCSC1InExtern;
                            } else {
                                ++$nbrDirectNeedCSC1InSupportIntern;
                            }
                            break;
                        case 'csc2':
                            ++$nbrTaskDirectNeedCSC2;
                            $nbrHoursOfDirectNeedCSC2 += $helpTask->getDurationReal();
                            if ($helpTask->getUser()->isACustomerService()) {
                                ++$nbrDirectNeedCSC2InExtern;
                            } else {
                                ++$nbrDirectNeedCSC2InSupportIntern;
                            }
                            break;
                    }
                }
            }
        }

        $helpTasksForFeelingOfAssistance = $this->helpTaskRepository->findBetweenDatesOnly($from, $to);
        foreach ($helpTasksForFeelingOfAssistance as $helpTask) {
            if (($helpTask->getAnswer() != []) && array_key_exists('question_experienced_assistance', $helpTask->getAnswer()['data'])) {
                $nbrLogBooksPositive += in_array($helpTask->getAnswer()['data']['question_experienced_assistance'], EvaluationParticipantAnswer::EVALUATION_LOGBOOK_POSITIVE) ? 1 : 0;
                $nbrLogBooksNegative += in_array($helpTask->getAnswer()['data']['question_experienced_assistance'], EvaluationParticipantAnswer::EVALUATION_LOGBOOK_NEGATIVE) ? 1 : 0;
            }
        }

        if (count($helpTasksForFeelingOfAssistance) > 0) {
            $averagePositiveFeeling = round($nbrLogBooksPositive * 100 / count($helpTasksForFeelingOfAssistance));
            $averageNegativeFeeling = round($nbrLogBooksNegative * 100 / count($helpTasksForFeelingOfAssistance));
        }

        $evaluationAnimatorsAssistances = $this->evaluationPerformanceAnimatorAnswerRepository->getAnswerForEvaluationBetweenDates($from, $to);
        $sumAnimatorsAnswers = 0;
        $countAnimatorsAnswers = 0;

        /** @var EvaluationPerformanceAnimatorAnswer $animatorsAnswers */
        foreach ($evaluationAnimatorsAssistances as $animatorsAnswers) {
            if ($animatorsAnswers->getHelpTask()->getHelpNeed()->getType() === HelpNeed::TYPE_ASSISTANCE) {
                $answers = $animatorsAnswers->getAnswer()['data'];

                if (isset($answers['question3'], $answers['question4'], $answers['question5'], $answers['question6'])) {
                    $values = array_slice($answers, 1, 3); // Récupérer les valeurs entre les clés "question3" et "question6"
                    $values = array_map('intval', $values); // Convertir les valeurs en entiers

                    $sumAnimatorsAnswers += array_sum($values);
                    $countAnimatorsAnswers += count($values);
                }
            }
        }

        $returnData['general'] = [
            'nbrSession' => count($sessions),
            'nbrAnimators' => $nbrAnimators,
            'nbrTrainees' => $nbrTrainees,
            'averagePositiveFeeling' => $averagePositiveFeeling.'%',
            'averageNegativeFeeling' => $averageNegativeFeeling.'%',
            'nbrHoursOfAssistance' => round($nbrHoursOfAssistance / 60, 1),

            'nbrDirectNeedHotline' => $nbrDirectNeedHotline,
            'nbrDirectNeedCSC1' => $nbrDirectNeedCSC1,
            'nbrDirectNeedCSC2' => $nbrDirectNeedCSC2,

            'nbrTaskDirectNeedHotline' => $nbrTaskDirectNeedHotline,
            'nbrTaskDirectNeedCSC1' => $nbrTaskDirectNeedCSC1,
            'nbrTaskDirectNeedCSC2' => $nbrTaskDirectNeedCSC2,

            'nbrHoursOfDirectNeedHotline' => $nbrHoursOfDirectNeedHotline,
            'nbrHoursOfDirectNeedCSC1' => $nbrHoursOfDirectNeedCSC1,
            'nbrHoursOfDirectNeedCSC2' => $nbrHoursOfDirectNeedCSC2,

            'sessionWithoutDirectNeedHotline' => $sessionWithoutDirectNeedHotline,
            'sessionWithoutDirectNeedCSC1' => $sessionWithoutDirectNeedCSC1,
            'sessionWithoutDirectNeedCSC2' => $sessionWithoutDirectNeedCSC2,

            'nbrDirectNeedHotlineInExtern' => $nbrDirectNeedHotlineInExtern,
            'nbrDirectNeedHotlineInSupportIntern' => $nbrDirectNeedHotlineInSupportIntern,

            'nbrDirectNeedCSC1InExtern' => $nbrDirectNeedCSC1InExtern,
            'nbrDirectNeedCSC1InSupportIntern' => $nbrDirectNeedCSC1InSupportIntern,

            'nbrDirectNeedCSC2InExtern' => $nbrDirectNeedCSC2InExtern,
            'nbrDirectNeedCSC2InSupportIntern' => $nbrDirectNeedCSC2InSupportIntern,

            'nbrNeedDirectNeed' => $nbrNeedDirectNeeds,
            'nbrTaskDirectNeed' => $nbrTaskDirectNeeds,
            'nbrNeedAssistances' => $nbrNeedAssistances,
            'nbrTaskAssistances' => $nbrTaskAssistances,
            'nbrNeedSupports' => $nbrNeedSupports,
            'nbrTaskSupports' => $nbrTaskSupports,
            'nbrPersonInAssistanceIntern' => $nbrPersonInAssistanceIntern,
            'nbrPersonInAssistanceExtern' => $nbrPersonInAssistanceExtern,
            'nbrPersonInSupportIntern' => $nbrPersonInSupportIntern,
            'nbrPersonInSupportExtern' => $nbrPersonInSupportExtern,
            'nbrSessionWithoutHelpTaskAssistance' => $sessionWithoutHelpTaskAssistance,
            'nbrSessionWithoutHelpTaskSupport' => $sessionWithoutHelpTaskSupport,
            'averageEvaluationsAssistants' => ($countAnimatorsAnswers ? $sumAnimatorsAnswers / $countAnimatorsAnswers : 0),
            'nbrRessentiPositif' => $nbrLogBooksPositive,
            'nbrRessentiNegatif' => $nbrLogBooksNegative,
            'nbrRessenti' => $nbrLogBooks,
        ];
        $DirectNeedHotlineExternPlusIntern = $nbrDirectNeedHotlineInSupportIntern + $nbrDirectNeedHotlineInExtern;
        $returnData['general']['nbrDirectNeedHotlinePercentExternes'] = ($DirectNeedHotlineExternPlusIntern ? round($nbrDirectNeedHotlineInExtern / ($DirectNeedHotlineExternPlusIntern) * 100, 1) : 0);
        $returnData['general']['nbrDirectNeedHotlinePercentIntern'] = ($DirectNeedHotlineExternPlusIntern ? round($nbrDirectNeedHotlineInSupportIntern / ($DirectNeedHotlineExternPlusIntern) * 100, 1) : 0);

        $DirectNeedCSC1ExternPlusIntern = $nbrDirectNeedCSC1InSupportIntern + $nbrDirectNeedCSC1InExtern;
        $returnData['general']['nbrDirectNeedCSC1PercentExternes'] = ($DirectNeedCSC1ExternPlusIntern ? round($nbrDirectNeedCSC1InExtern / ($DirectNeedCSC1ExternPlusIntern) * 100, 1) : 0);
        $returnData['general']['nbrDirectNeedCSC1PercentIntern'] = ($DirectNeedCSC1ExternPlusIntern ? round($nbrDirectNeedCSC1InSupportIntern / ($DirectNeedCSC1ExternPlusIntern) * 100, 1) : 0);

        $DirectNeedCSC2ExternPlusIntern = $nbrDirectNeedCSC2InSupportIntern + $nbrDirectNeedCSC2InExtern;
        $returnData['general']['nbrDirectNeedCSC2PercentExternes'] = ($DirectNeedCSC2ExternPlusIntern ? round($nbrDirectNeedCSC2InExtern / ($DirectNeedCSC2ExternPlusIntern) * 100, 1) : 0);
        $returnData['general']['nbrDirectNeedCSC2PercentIntern'] = ($DirectNeedCSC2ExternPlusIntern ? round($nbrDirectNeedCSC2InSupportIntern / ($DirectNeedCSC2ExternPlusIntern) * 100, 1) : 0);

        $supportExternPlusIntern = $nbrPersonInSupportExtern + $nbrPersonInSupportIntern;
        $returnData['general']['nbrSupportPercentExternes'] = ($supportExternPlusIntern ? round($nbrPersonInSupportExtern / ($supportExternPlusIntern) * 100, 1) : 0);
        $returnData['general']['nbrSupportPercentInterns'] = ($supportExternPlusIntern ? round($nbrPersonInSupportIntern / ($supportExternPlusIntern) * 100, 1) : 0);
        $assistanceExternPlusIntern = $nbrPersonInAssistanceExtern + $nbrPersonInAssistanceIntern;
        $returnData['general']['nbrAssistancePercentExternes'] = ($assistanceExternPlusIntern ? round($nbrPersonInAssistanceExtern / ($assistanceExternPlusIntern) * 100, 1) : 0);
        $returnData['general']['nbrAssistancePercentInterns'] = ($assistanceExternPlusIntern ? round($nbrPersonInAssistanceIntern / ($assistanceExternPlusIntern) * 100, 1) : 0);

        return $returnData;
    }
}
