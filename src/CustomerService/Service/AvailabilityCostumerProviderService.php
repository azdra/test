<?php

namespace App\CustomerService\Service;

use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Repository\AvailabilityUserRepository;
use App\CustomerService\Repository\HelpTaskRepository;
use App\Entity\User;
use App\Repository\UserRepository;

class AvailabilityCostumerProviderService
{
    public function __construct(
        private UserRepository $userRepository,
        private HelpTaskRepository $helpTaskRepository,
        private AvailabilityUserRepository $availabilityUserRepository
    ) {
    }

    public function availabilityAndHelpTaskUser(array $activeUser = [User::STATUS_ACTIVE, User::STATUS_INACTIVE]): array
    {
        $costumerProviderInfos = [];
        $eventSources = [];
        foreach ($this->userRepository->findAllProviderServiceAccountsWithoutAdmins(activeUser: $activeUser) as $costumerProvider) {
            $costumerProviderInfos[] = [
                'id' => $costumerProvider->getId(),
                'title' => $costumerProvider->getFullName(),
                //'eventColor' => "#".dechex(rand(0x000000, 0xFFFFFF)),
            ];
            $availabilityUsers = $this->availabilityUserRepository->findAvailabilityUserBetweenDates(
                $costumerProvider,
                (new \DateTimeImmutable())->modify('-3 month'),
                (new \DateTimeImmutable())->modify('+6 month')
            );
            foreach ($availabilityUsers as $availabilityUser) {
                $eventSources[] = [
                    'resourceId' => $costumerProvider->getId(),
                    'title' => $costumerProvider->getReference().' - '.$availabilityUser->getStart()->setTimezone(new \DateTimeZone($costumerProvider->getClient()->getTimezone()))->format('H:i').' - '.$availabilityUser->getEnd()->setTimezone(new \DateTimeZone($costumerProvider->getClient()->getTimezone()))->format('H:i'),
                    'start' => $availabilityUser->getStart()->format('Y-m-d\TH:i:s\Z'),
                    'end' => $availabilityUser->getEnd()->format('Y-m-d\TH:i:s\Z'),
                    'display' => 'background',
                    'color' => '#65f582',
                ];
            }
            $helpTaskUser = $this->helpTaskRepository->findBetweenDatesAndUser(
                $costumerProvider,
                (new \DateTimeImmutable())->modify('-3 month'),
                (new \DateTimeImmutable())->modify('+6 month')
            );
            foreach ($helpTaskUser as $helpTask) {
                if ($helpTask->getHelpNeed()->getType() === HelpNeed::TYPE_DIRECT_NEED) {
                    $color = '#fcf800';
                } else {
                    $color = $helpTask->getHelpNeed()->getClient()->getColor();
                }
                $eventSources[] = [
                    'resourceId' => $costumerProvider->getId(),
                    'title' => $helpTask->getType().' - '.$helpTask->getDurationReal().' (min) ',
                    'start' => $helpTask->getStart()->format('Y-m-d\TH:i:s\Z'),
                    'end' => $helpTask->getEnd()->format('Y-m-d\TH:i:s\Z'),
                    'color' => $color,
                    'textColor' => 'black',
                ];
            }
        }

        return [
            'costumerProvider' => $costumerProviderInfos,
            'eventSources' => $eventSources,
        ];
    }
}
