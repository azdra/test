<?php

namespace App\CustomerService\Service;

use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Repository\HelpNeedRepository;
use App\Repository\ClientRepository;
use App\Repository\UserRepository;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MassAssignService
{
    public function __construct(
        private HelpNeedRepository $helpNeedRepository,
        private UserRepository $userRepository,
        private HelpTaskService $helpTaskService,
        private ClientRepository $clientRepository
    ) {
    }

    public function massAssign(array $filterData): void
    {
        $filterData = $this->resolveOptions($filterData, true);

        $helpNeeds = $this->helpNeedRepository->findForMassAssign($filterData['dateStart'], $filterData['dateEnd'], $filterData['lang'], $filterData['clientIds']);

        if (empty($helpNeeds)) {
            return;
        }

        $users = $this->userRepository->findAllProviderServiceAccounts($filterData['userIds']);

        if (empty($users)) {
            return;
        }

        foreach ($helpNeeds as $helpNeed) {
            foreach ($users as $user) {
                $this->helpTaskService->assignedUserToHelpNeed($helpNeed, $user);
            }
        }
    }

    private function resolveOptions(array $options, bool $isMassAssign): array
    {
        $resolver = (new OptionsResolver())
            ->setRequired('dateStart')
            ->setAllowedValues('dateStart', function ($value) {
                return \preg_match('/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}(?:Z|\+\d{2}:\d{2})$/', $value);
            })
            ->setNormalizer('dateStart', function (Options $options, $value) {
                return new \DateTime($value);
            })
            ->setRequired('clientIds')
            ->setRequired('dateEnd')
            ->setAllowedValues('dateEnd', function ($value) {
                return \preg_match('/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}(?:Z|\+\d{2}:\d{2})$/', $value);
            })
            ->setNormalizer('dateEnd', function (Options $options, $value) {
                return new \DateTime($value);
            })
            ->setRequired('lang')
            ->setRequired('userIds')
            ->setAllowedValues('userIds', function ($value) {
                return \preg_match('/^\d+(?:[ \t]*,[ \t]*\d+)*$/', $value);
            })
            ->setNormalizer('userIds', function (Options $options, $value) {
                return explode(',', $value);
            });

        if ($isMassAssign) {
            $resolver->setAllowedValues('clientIds', function ($value) {
                return \preg_match('/^\d+(?:[ \t]*,[ \t]*\d+)*$/', $value);
            })
                     ->setNormalizer('clientIds', function (Options $options, $value) {
                         return explode(',', $value);
                     });
        } else {
            if ($options['clientIds'] !== '') {
                $resolver->setAllowedValues('clientIds', function ($value) {
                    return \preg_match('/^\d+(?:[ \t]*,[ \t]*\d+)*$/', $value);
                })
                         ->setNormalizer('clientIds', function (Options $options, $value) {
                             return explode(',', $value);
                         });
            }
            $resolver->setRequired('serviceType')
                     ->setAllowedValues('serviceType', ['hotline', 'csc1', 'csc2']);
        }

        return $resolver->resolve($options);
    }

    public function assignServiceDelivery(array $filterData): HelpNeed
    {
        $filterData = $this->resolveOptions($filterData, false);
        $liveSessionProductionClient = $this->clientRepository->findOneBy(['id' => 22]); // retrieve the live session production client to assign to HelpDirectNeed

        $helpDirectNeed = new HelpNeed($liveSessionProductionClient);
        $helpDirectNeed->setStart(new \DateTimeImmutable(($filterData['dateStart'])->format('Y-m-d H:i:s')))
                       ->setEnd(new \DateTimeImmutable(($filterData['dateEnd']->format('Y-m-d H:i:s'))))
                       ->setAdditionalInformation([
                                                      'serviceType' => $filterData['serviceType'],
                                                      'clientIds' => $filterData['clientIds'],
                                                      'language' => $filterData['lang'],
                                                  ]);

        $users = $this->userRepository->findAllProviderServiceAccounts($filterData['userIds']);

        if (empty($users)) {
            return $helpDirectNeed;
        }

        foreach ($users as $user) {
            $this->helpTaskService->assignedUserToHelpNeed($helpDirectNeed, $user);
        }

        return $helpDirectNeed;
    }
}
