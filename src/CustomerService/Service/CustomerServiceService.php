<?php

namespace App\CustomerService\Service;

use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Repository\AvailabilityUserRepository;
use App\CustomerService\Repository\HelpTaskRepository;
use App\Entity\User;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\SupportService;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Contracts\Translation\TranslatorInterface;

class CustomerServiceService extends ServiceEntityRepository
{
    public function __construct(
        private TranslatorInterface $translator,
        protected ProviderSessionRepository $providerSessionRepository,
        protected SupportService $supportService,
        private AvailabilityUserRepository $availabilityUserRepository,
        private UserRepository $userRepository,
    ) {
    }

    public function getAnswersAnimator(array $allEvaluationPerformanceAnimatorAnswer): array
    {
        $allAnswers = [];
        foreach ($allEvaluationPerformanceAnimatorAnswer as $evaluationPerformanceAnimatorAnswer) {
            if (isset($evaluationPerformanceAnimatorAnswer->getAnswer()['data'])) {
                $answerAnimator = $evaluationPerformanceAnimatorAnswer->getAnswer()['data'];
                $answerAnimator['assistantName'] = $evaluationPerformanceAnimatorAnswer->getHelpTask()->getUser()->getFullName();
                $answerAnimator['animatorName'] = $evaluationPerformanceAnimatorAnswer->getProviderParticipantSessionRole()->getParticipant()->getUser()->getFullName();
                $answerAnimator['replyDate'] = $evaluationPerformanceAnimatorAnswer->getReplydate()->setTimezone(new \DateTimeZone($evaluationPerformanceAnimatorAnswer->getHelpTask()->getSession()->getClient()->getTimezone()))->format('d/m/y H:i');
                $answerAnimator['sessionName'] = $evaluationPerformanceAnimatorAnswer->getHelpTask()->getHelpNeed()->getSession()->getName();
                $answerAnimator['sessionRef'] = $evaluationPerformanceAnimatorAnswer->getHelpTask()->getHelpNeed()->getSession()->getRef();
                $allAnswers[] = $answerAnimator;
            }
        }

        return $allAnswers;
    }

    public function getLogBookAnswers(array $allHelpTasks): array
    {
        $allAnswers = [];
        foreach ($allHelpTasks as $helpTask) {
            if (($helpTask->getAnswer() != [])) {
                $answerHelpTask = $helpTask->getAnswer()['data'];
                $answerHelpTask['assistantName'] = $helpTask->getUser()->getFullName();
                $answerHelpTask['replyDate'] = (!empty($helpTask->getAnswer()['replyDate'])) ? (new DateTimeImmutable($helpTask->getAnswer()['replyDate']))->setTimezone(new \DateTimeZone($helpTask->getSession()->getClient()->getTimezone()))->format('d/m/Y H:i') : '';
                $answerHelpTask['prestaName'] = $helpTask->getUser()->getFullName();
                $answerHelpTask['sessionName'] = $helpTask->getHelpNeed()->getSession()->getName();
                $answerHelpTask['sessionRef'] = $helpTask->getHelpNeed()->getSession()->getRef();
                $answerHelpTask['sessionDate'] = $helpTask->getHelpNeed()->getSession()->getDateStart()->setTimezone(new \DateTimeZone($helpTask->getSession()->getClient()->getTimezone()))->format('d/m/Y H:i');
                $allAnswers[] = $answerHelpTask;
            }
        }

        return $allAnswers;
    }

    public function getBillingSessions(DateTimeImmutable $from, DateTimeImmutable $to): array
    {
        $sessions = [];
        /*$from = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2022-11-01 00:00:00')->setTimezone(new \DateTimeZone('Europe/Paris'));
        $fromUTC = (clone $from)->setTimezone(new \DateTimeZone('UTC'));
        $to = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2023-03-20 23:59:59')->setTimezone(new \DateTimeZone('Europe/Paris'));
        $toUTC = (clone $to)->setTimezone(new \DateTimeZone('UTC'));*/
        //Filtrer par le service client mec si pas superadmin
        $sessions = $this->providerSessionRepository->findByDateBetween($from, $to);

        return $sessions;
    }

    public function getBillingClaimant(array $sessions = [], array $params = []): array
    {
        /*$allData = [
            '07/22' => [
                'name' => 'Juillet 2022', 'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
                'assistance' => [
                    'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
                'support' => [
                    'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
            ],
            '08/22' => [
                'name' => 'Août 2022', 'quantity' => 3, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
                'assistance' => [
                    'quantity' => 1, 'hs' => 1, 'hhs' => 0.25, 'cancelledMinus24' => 1, 'billed' => 0
                ],
                'support' => [
                    'quantity' => 2, 'hs' => 2, 'hhs' => 2, 'cancelledMinus24' => 0, 'billed' => 0
                ],
            ],
            '09/22' => [
                'name' => 'Septembre 2022', 'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
                'assistance' => [
                    'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
                'support' => [
                    'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
            ],
            '10/22' => [
                'name' => 'Octobre 2022', 'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
                'assistance' => [
                    'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
                'support' => [
                    'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
            ],
            '11/22' => [
                'name' => 'Novembre 2022', 'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
                'assistance' => [
                    'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
                'support' => [
                    'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
            ]
        ];*/

        $allData = [];
        $startDate = $params['dateStart'];
        $endDate = $params['dateEnd'];
        while ($startDate <= $endDate) {
            $monthYear = $startDate->format('F Y');
            $allData[$monthYear] = $this->getNewBlocClaimantView($this->translator->trans($startDate->format('F')).' '.$startDate->format('Y'));
            $startDate = $startDate->modify('first day of next month');
        }

        $indexClaimantView = '';
        $hsAssistance = 0;
        $hhsAssistance = 0;
        $hsSupport = 0;
        $hhsSupport = 0;

        $hsDirectNeed = 0;
        $hhsDirectNeed = 0;
        foreach ($sessions as $session) {
            if ($indexClaimantView != $session->getDateStart()->format('F Y')) {
                $indexClaimantView = $session->getDateStart()->format('F Y');
                $hsAssistance = 0;
                $hhsAssistance = 0;
                $hsSupport = 0;
                $hhsSupport = 0;
                $qtyAssistance = 0;
                $qtySupport = 0;
                $assistanceCancelledMinus24 = 0;
                $supportCancelledMinus24 = 0;
                $directNeedCancelledMinus24 = 0;

                $hsDirectNeed = 0;
                $hhsDirectNeed = 0;
                //$allData[$indexClaimantView] = $this->getNewBlocClaimantView($indexClaimantView);
            }

            foreach ($session->getHelpNeeds() as $helpNeed) {
                if ($helpNeed->getType() == HelpNeed::TYPE_ASSISTANCE) {    //Type Assistance
                    foreach ($helpNeed->getHelpTasks() as $helpTask) {
                        if ($helpTask->getUser()->isACustomerService()) {
                            $keepData = (!empty($params['user']) && $params['user'] != $helpTask->getUser()) ? false : true;
                            if ($keepData) {
                                $slots = $this->supportService->getOverlappedCustomer($helpTask->getStartReal()->setTimezone(new \DateTimeZone('Europe/Paris')), $helpTask->getEndReal()->setTimezone(new \DateTimeZone('Europe/Paris')));
                                ++$qtyAssistance;
                                $hsAssistance += round($slots['slotsResults']['inHSStartAndHSEnd'] / 60, 2);
                                $hhsAssistance += round(($slots['slotsResults']['beforeHS'] + $slots['slotsResults']['afterHS']) / 60, 2);
                                if ($session->isSessionCanceledLessOneDay()) {
                                    ++$assistanceCancelledMinus24;
                                }
                                //dump('ASSISTANCE >>> HS = '.$hsAssistance .' | HHS = '.$hhsAssistance);
                            }
                        }
                    }
                } elseif ($helpNeed->getType() == HelpNeed::TYPE_SUPPORT) {   //Type Support
                    foreach ($helpNeed->getHelpTasks() as $helpTask) {
                        if ($helpTask->getUser()->isACustomerService()) {
                            $keepData = (!empty($params['user']) && $params['user'] != $helpTask->getUser()) ? false : true;
                            if ($keepData) {
                                $slots = $this->supportService->getOverlappedCustomer($helpTask->getStartReal()->setTimezone(new \DateTimeZone('Europe/Paris')), $helpTask->getEndReal()->setTimezone(new \DateTimeZone('Europe/Paris')));
                                ++$qtySupport;
                                $hsSupport += round($slots['slotsResults']['inHSStartAndHSEnd'] / 60, 2);
                                $hhsSupport += round(($slots['slotsResults']['beforeHS'] + $slots['slotsResults']['afterHS']) / 60, 2);
                                if ($session->isSessionCanceledLessOneDay()) {
                                    ++$supportCancelledMinus24;
                                }
                                //dump('SUPPORT >>> HS = '.$hsSupport .' | HHS = '.$hhsSupport);
                            }
                        }
                    }
                }
            }

            $allData[$indexClaimantView]['assistance'] = [
                'quantity' => $qtyAssistance,
                'hs' => $hsAssistance,
                'hhs' => $hhsAssistance,
                'cancelledMinus24' => $assistanceCancelledMinus24,
                'billed' => 0,
                'hs_convert' => $this->convertTime($hsAssistance),
                'hhs_convert' => $this->convertTime($hhsAssistance),
            ];

            $allData[$indexClaimantView]['support'] = [
                'quantity' => $qtySupport,
                'hs' => $hsSupport,
                'hhs' => $hhsSupport,
                'cancelledMinus24' => $supportCancelledMinus24,
                'billed' => 0,
                'hs_convert' => $this->convertTime($hsSupport),
                'hhs_convert' => $this->convertTime($hhsSupport),
            ];

            $allData[$indexClaimantView]['name'] = $this->translator->trans($session->getDateStart()->format('F')).' '.$session->getDateStart()->format('Y');
            $allData[$indexClaimantView]['quantity'] = $allData[$indexClaimantView]['assistance']['quantity'] + $allData[$indexClaimantView]['support']['quantity'] + $allData[$indexClaimantView]['directNeed']['quantity'];
            $allData[$indexClaimantView]['hs'] = $allData[$indexClaimantView]['assistance']['hs'] + $allData[$indexClaimantView]['support']['hs'] + $allData[$indexClaimantView]['directNeed']['hs'];
            $allData[$indexClaimantView]['hhs'] = $allData[$indexClaimantView]['assistance']['hhs'] + $allData[$indexClaimantView]['support']['hhs'] + $allData[$indexClaimantView]['directNeed']['hhs'];
            $allData[$indexClaimantView]['hs_convert'] = $this->convertTime($allData[$indexClaimantView]['hs']);
            $allData[$indexClaimantView]['hhs_convert'] = $this->convertTime($allData[$indexClaimantView]['hhs']);
            $allData[$indexClaimantView]['cancelledMinus24'] = $allData[$indexClaimantView]['assistance']['cancelledMinus24'] + $allData[$indexClaimantView]['support']['cancelledMinus24'] + $allData[$indexClaimantView]['directNeed']['cancelledMinus24'];
            $allData[$indexClaimantView]['billed'] = $allData[$indexClaimantView]['assistance']['billed'] + $allData[$indexClaimantView]['support']['billed'] + $allData[$indexClaimantView]['directNeed']['billed'];
        }

        $qtyDirectNeed = 0;
        $indexClaimantView = '';

        foreach ($params['directHelpsNeed'] as $helpNeed) {
            if ($indexClaimantView != $helpNeed->getStart()->format('F Y')) {
                $indexClaimantView = $helpNeed->getStart()->format('F Y');
                $hsDirectNeed = 0;
                $hhsDirectNeed = 0;
                $qtyDirectNeed = 0;
            }
            foreach ($helpNeed->getHelpTasks() as $helpTask) {
                if ($helpTask->getUser()->isACustomerService()) {
                    $keepData = (!empty($params['user']) && $params['user'] != $helpTask->getUser()) ? false : true;
                    if ($keepData) {
                        $slots = $this->supportService->getOverlappedCustomer($helpTask->getStartReal()->setTimezone(new \DateTimeZone('Europe/Paris')), $helpTask->getEndReal()->setTimezone(new \DateTimeZone('Europe/Paris')));
                        ++$qtyDirectNeed;
                        $hsDirectNeed += round($slots['slotsResults']['inHSStartAndHSEnd'] / 60, 2);
                        $allData[$indexClaimantView]['hs'] += round($slots['slotsResults']['inHSStartAndHSEnd'] / 60, 2);
                        $hhsDirectNeed += round(($slots['slotsResults']['beforeHS'] + $slots['slotsResults']['afterHS']) / 60, 2);
                        $allData[$indexClaimantView]['hhs'] += round(($slots['slotsResults']['beforeHS'] + $slots['slotsResults']['afterHS']) / 60, 2);
                        ++$allData[$indexClaimantView]['quantity'];
                    }
                }
            }

            $allData[$indexClaimantView]['directNeed'] = [
                'quantity' => $qtyDirectNeed,
                'hs' => $hsDirectNeed,
                'hhs' => $hhsDirectNeed,
                'cancelledMinus24' => 0,
                'billed' => 0,
                'hs_convert' => $this->convertTime($hsDirectNeed),
                'hhs_convert' => $this->convertTime($hhsDirectNeed),
            ];
            $allData[$indexClaimantView]['billed'] = $allData[$indexClaimantView]['billed'] + $allData[$indexClaimantView]['directNeed']['billed'];
        }

        krsort($allData);

        return $allData;
    }

    /**
     * @throws \Exception
     */
    public function getBillingClaimantFromSA(array $sessions = [], array $params = []): array
    {
        /*$allData = [
            'userId_14' => [
                'id' => '14', 'name' => 'Aude MAZOLLIER', 'acronym' => 'AMA', 'quantity' => 3, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
                'assistance' => [
                    'quantity' => 1, 'hs' => 1, 'hhs' => 0.25, 'cancelledMinus24' => 1, 'billed' => 0
                ],
                'support' => [
                    'quantity' => 2, 'hs' => 2, 'hhs' => 2, 'cancelledMinus24' => 0, 'billed' => 0
                ],
            ],
            'userId_15' => [
                'id' => '15', 'name' => 'Audrey GERVOISE', 'acronym' => 'AGE', 'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
                'assistance' => [
                    'quantity' => 0, 'hs' => null, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
                'support' => [
                    'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
            ],
            'userId_16' => [
                'id' => '16', 'name' => 'Aurélie OBRIEN', 'acronym' => 'AOB', 'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
                'assistance' => [
                    'quantity' => 0, 'hs' => null, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
                'support' => [
                    'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
            ],
            'userId_17' => [
                'id' => '17', 'name' => 'Elodie BOGET', 'acronym' => 'EBO', 'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
                'assistance' => [
                    'quantity' => 0, 'hs' => null, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
                'support' => [
                    'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'cancelledMinus24' => 0, 'billed' => 0
                ],
            ]
        ];*/
        $allData = [];
        $dataRealSupport = [];
        $endDataRealSupport = [];
        foreach ($sessions as $session) {
            foreach ($session->getHelpNeeds() as $helpNeed) {
                if ($helpNeed->getType() == HelpNeed::TYPE_ASSISTANCE) {    //Type Assistance
                    foreach ($helpNeed->getHelpTasks() as $helpTask) {
                        if ($helpTask->getUser()->isACustomerService()) {
                            $slots = $this->supportService->getOverlappedCustomer($helpTask->getStartReal()->setTimezone(new \DateTimeZone('Europe/Paris')), $helpTask->getEndReal()->setTimezone(new \DateTimeZone('Europe/Paris')));
                            $indexSAView = '_'.strval($helpTask->getUser()->getId());
                            if (empty($allData[$indexSAView])) {
                                $allData[$indexSAView] = $this->getNewBlocSAView($indexSAView, $helpTask->getUser());
                            }
                            ++$allData[$indexSAView]['assistance']['quantity'];
                            $allData[$indexSAView]['assistance']['hs'] += round($slots['slotsResults']['inHSStartAndHSEnd'] / 60, 2);
                            $allData[$indexSAView]['assistance']['hhs'] += round(($slots['slotsResults']['beforeHS'] + $slots['slotsResults']['afterHS']) / 60, 2);
                            $allData[$indexSAView]['assistance']['hs_convert'] = $this->convertTime($allData[$indexSAView]['assistance']['hs']);
                            $allData[$indexSAView]['assistance']['hhs_convert'] = $this->convertTime($allData[$indexSAView]['assistance']['hhs']);
                            if ($session->isSessionCanceledLessOneDay()) {
                                ++$allData[$indexSAView]['assistance']['cancelledMinus24'];
                            }
                            $allData[$indexSAView]['assistance']['billed'] = 0;

                            $allData[$indexSAView]['quantity'] = $allData[$indexSAView]['assistance']['quantity'] + $allData[$indexSAView]['support']['quantity'] + $allData[$indexSAView]['directNeed']['quantity'];
                            $allData[$indexSAView]['hs'] = $allData[$indexSAView]['assistance']['hs'] + $allData[$indexSAView]['support']['hs'] + $allData[$indexSAView]['directNeed']['hs'];
                            $allData[$indexSAView]['hhs'] = $allData[$indexSAView]['assistance']['hhs'] + $allData[$indexSAView]['support']['hhs'] + $allData[$indexSAView]['directNeed']['hhs'];
                            $allData[$indexSAView]['hs_convert'] = $this->convertTime($allData[$indexSAView]['hs']);
                            $allData[$indexSAView]['hhs_convert'] = $this->convertTime($allData[$indexSAView]['hhs']);
                            $allData[$indexSAView]['cancelledMinus24'] = $allData[$indexSAView]['assistance']['cancelledMinus24'] + $allData[$indexSAView]['support']['cancelledMinus24'] + $allData[$indexSAView]['directNeed']['cancelledMinus24'];
                            $allData[$indexSAView]['billed'] = $allData[$indexSAView]['assistance']['billed'] + $allData[$indexSAView]['support']['billed'] + $allData[$indexSAView]['directNeed']['billed'];
                            //dump('ASSISTANCE >>> HS SA'.$indexSAView.' = '.$allData[$indexSAView]['assistance']['hs'].' | HS SA'.$indexSAView.' = '.$allData[$indexSAView]['assistance']['hhs']);
                        }
                    }
                } elseif ($helpNeed->getType() == HelpNeed::TYPE_SUPPORT) {   //Type Support
                    foreach ($helpNeed->getHelpTasks() as $helpTask) {
                        $start = $helpTask->getStartReal()->setTimezone(new \DateTimeZone($helpNeed->getClient()->getTimezone()));
                        $end = $helpTask->getEndReal()->setTimezone(new \DateTimeZone($helpNeed->getClient()->getTimezone()));
                        if (!array_key_exists($helpTask->getUser()->getId(), $endDataRealSupport)) {
                            $endDataRealSupport[$helpTask->getUser()->getId()] = $end;
                            $dataRealSupport[$helpTask->getUser()->getId()][] = ['start' => $start, 'end' => $end, 'helpTask' => $helpTask];
                        }

                        if ($helpTask->getStartReal() >= $endDataRealSupport[$helpTask->getUser()->getId()]) {
                            $endDataRealSupport[$helpTask->getUser()->getId()] = $helpTask->getEndReal();
                            $dataRealSupport[$helpTask->getUser()->getId()][] = ['start' => $start, 'end' => $end, 'helpTask' => $helpTask];
                        } else {
                            $includeInTimeSlots = false;
                            foreach ($dataRealSupport[$helpTask->getUser()->getId()] as $key => $value) {
                                if ($start >= $value['start'] && $end <= $value['end']) {
                                    $includeInTimeSlots = true;
                                } elseif ($start >= $value['start'] && $start <= $value['end'] && $end >= $value['end']) {
                                    $includeInTimeSlots = true;
                                    $dataRealSupport[$helpTask->getUser()->getId()][$key]['end'] = $end;
                                    $dataRealSupport[$helpTask->getUser()->getId()][$key]['helpTask']->setEndReal($end);
                                } elseif ($start <= $value['start'] && $end >= $value['start'] && $end <= $value['end']) {
                                    $includeInTimeSlots = true;
                                    $dataRealSupport[$helpTask->getUser()->getId()][$key]['start'] = $start;
                                    $dataRealSupport[$helpTask->getUser()->getId()][$key]['helpTask']->setStartReal($start);
                                } elseif ($start <= $value['start'] && $end >= $value['end']) {
                                    $includeInTimeSlots = true;
                                    $dataRealSupport[$helpTask->getUser()->getId()][$key]['start'] = $start;
                                    $dataRealSupport[$helpTask->getUser()->getId()][$key]['helpTask']->setStartReal($start);
                                    $dataRealSupport[$helpTask->getUser()->getId()][$key]['end'] = $end;
                                    $dataRealSupport[$helpTask->getUser()->getId()][$key]['helpTask']->setEndReal($end);
                                }
                            }
                            if (!$includeInTimeSlots) {
                                $dataRealSupport[$helpTask->getUser()->getId()][] = ['start' => $start, 'end' => $end, 'helpTask' => $helpTask];
                            }
                        }
                    }
                }
            }
        }
        $dataRealSupport = $this->removeIncludedItem($dataRealSupport);
        foreach ($dataRealSupport as $index => $helpTaskItems) {
            foreach ($helpTaskItems as $helpTaskItem) {
                $helpTask = $helpTaskItem['helpTask'];
                if ($helpTask->getUser()->isACustomerService()) {
                    $slots = $this->supportService->getOverlappedCustomer($helpTask->getStartReal()->setTimezone(new \DateTimeZone($helpTask->getHelpNeed()->getClient()->getTimezone())), $helpTask->getEndReal()->setTimezone(new \DateTimeZone($helpTask->getHelpNeed()->getClient()->getTimezone())));
                    $indexSAView = '_'.strval($index);
                    if (empty($allData[$indexSAView])) {
                        $allData[$indexSAView] = $this->getNewBlocSAView($indexSAView, $helpTask->getUser());
                    }
                    ++$allData[$indexSAView]['support']['quantity'];
                    $allData[$indexSAView]['support']['hs'] += round($slots['slotsResults']['inHSStartAndHSEnd'] / 60, 2);
                    $allData[$indexSAView]['support']['hhs'] += round(($slots['slotsResults']['beforeHS'] + $slots['slotsResults']['afterHS']) / 60, 2);
                    $allData[$indexSAView]['support']['hs_convert'] = $this->convertTime($allData[$indexSAView]['support']['hs']);
                    $allData[$indexSAView]['support']['hhs_convert'] = $this->convertTime($allData[$indexSAView]['support']['hhs']);
                    /*if ($session->isSessionCanceledLessOneDay()) {
                        ++$allData[$indexSAView]['support']['cancelledMinus24'];
                    }*/
                    $allData[$indexSAView]['support']['billed'] = 0; //$allData[$indexSAView]['support']['billed'] + 1;

                    $allData[$indexSAView]['quantity'] = $allData[$indexSAView]['assistance']['quantity'] + $allData[$indexSAView]['support']['quantity'] + $allData[$indexSAView]['directNeed']['quantity'];
                    $allData[$indexSAView]['hs'] = $allData[$indexSAView]['assistance']['hs'] + $allData[$indexSAView]['support']['hs'] + $allData[$indexSAView]['directNeed']['hs'];
                    $allData[$indexSAView]['hhs'] = $allData[$indexSAView]['assistance']['hhs'] + $allData[$indexSAView]['support']['hhs'] + $allData[$indexSAView]['directNeed']['hhs'];
                    $allData[$indexSAView]['hs_convert'] = $this->convertTime($allData[$indexSAView]['hs']);
                    $allData[$indexSAView]['hhs_convert'] = $this->convertTime($allData[$indexSAView]['hhs']);
                    $allData[$indexSAView]['cancelledMinus24'] = $allData[$indexSAView]['assistance']['cancelledMinus24'] + $allData[$indexSAView]['support']['cancelledMinus24'] + $allData[$indexSAView]['directNeed']['cancelledMinus24'];
                    $allData[$indexSAView]['billed'] = $allData[$indexSAView]['assistance']['billed'] + $allData[$indexSAView]['support']['billed'] + $allData[$indexSAView]['directNeed']['billed'];
                }
            }
        }

        if (array_key_exists('directHelpsNeed', $params) && !empty($params['directHelpsNeed'])) {
            foreach ($params['directHelpsNeed'] as $helpNeed) {
                foreach ($helpNeed->getHelpTasks() as $helpTask) {
                    if ($helpTask->getUser()->isACustomerService()) {
                        $slots = $this->supportService->getOverlappedCustomer($helpTask->getStartReal()->setTimezone(new \DateTimeZone('Europe/Paris')), $helpTask->getEndReal()->setTimezone(new \DateTimeZone('Europe/Paris')));
                        $indexSAView = '_'.strval($helpTask->getUser()->getId());
                        if (empty($allData[$indexSAView])) {
                            $allData[$indexSAView] = $this->getNewBlocSAView($indexSAView, $helpTask->getUser());
                        }
                        ++$allData[$indexSAView]['directNeed']['quantity'];
                        $allData[$indexSAView]['directNeed']['hs'] += round($slots['slotsResults']['inHSStartAndHSEnd'] / 60, 2);
                        $allData[$indexSAView]['directNeed']['hhs'] += round(($slots['slotsResults']['beforeHS'] + $slots['slotsResults']['afterHS']) / 60, 2);
                        $allData[$indexSAView]['directNeed']['hs_convert'] = $this->convertTime($allData[$indexSAView]['directNeed']['hs']);
                        $allData[$indexSAView]['directNeed']['hhs_convert'] = $this->convertTime($allData[$indexSAView]['directNeed']['hhs']);
                        $allData[$indexSAView]['directNeed']['billed'] = 0;

                        $allData[$indexSAView]['quantity'] = $allData[$indexSAView]['assistance']['quantity'] + $allData[$indexSAView]['support']['quantity'] + $allData[$indexSAView]['directNeed']['quantity'];
                        $allData[$indexSAView]['hs'] = $allData[$indexSAView]['assistance']['hs'] + $allData[$indexSAView]['support']['hs'] + $allData[$indexSAView]['directNeed']['hs'];
                        $allData[$indexSAView]['hhs'] = $allData[$indexSAView]['assistance']['hhs'] + $allData[$indexSAView]['support']['hhs'] + $allData[$indexSAView]['directNeed']['hhs'];
                        $allData[$indexSAView]['hs_convert'] = $this->convertTime($allData[$indexSAView]['hs']);
                        $allData[$indexSAView]['hhs_convert'] = $this->convertTime($allData[$indexSAView]['hhs']);
                        $allData[$indexSAView]['cancelledMinus24'] = $allData[$indexSAView]['assistance']['cancelledMinus24'] + $allData[$indexSAView]['support']['cancelledMinus24'] + $allData[$indexSAView]['directNeed']['cancelledMinus24'];
                        $allData[$indexSAView]['billed'] = $allData[$indexSAView]['assistance']['billed'] + $allData[$indexSAView]['support']['billed'] + $allData[$indexSAView]['directNeed']['billed'];
                    }
                }
            }
        }

        krsort($allData);

        return $allData;
    }

    public function plageIncluse($plage1, $plage2): bool
    {
        return $plage1['start'] >= $plage2['start'] && $plage1['end'] <= $plage2['end'];
    }

    public function removeIncludedItem(array $dataRealSupport): array
    {
        foreach ($dataRealSupport as $cle1 => $element1) {
            foreach ($element1 as $cle2 => $plage1) {
                foreach ($element1 as $cle3 => $plage2) {
                    if ($cle2 !== $cle3 && $this->plageIncluse($plage1, $plage2)) {
                        // Supprimer l'élément inclus
                        unset($dataRealSupport[$cle1][$cle2]);
                        break; // Sortir de la boucle interne une fois l'élément supprimé
                    }
                }
            }
        }

        return $dataRealSupport;
    }

    private function getNewBlocClaimantView($index): array
    {
        $init = [
                    'name' => $this->translator->trans($index), 'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'hs_convert' => 0, 'hhs_convert' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
                    'assistance' => [
                        'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'hs_convert' => 0, 'hhs_convert' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
                    ],
                    'support' => [
                        'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'hs_convert' => 0, 'hhs_convert' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
                    ],
                    'directNeed' => [
                        'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'hs_convert' => 0, 'hhs_convert' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
                    ],
                ];

        return $init;
    }

    private function getNewBlocSAView($index, User $user): array
    {
        $name = $user->getFirstName().' '.strtoupper($user->getLastName());
        $acronym = $user->getReferenceClient();
        $init = [
            'id' => $index, 'name' => $name, 'acronym' => $acronym, 'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'hs_convert' => 0, 'hhs_convert' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
            'assistance' => [
                'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'hs_convert' => 0, 'hhs_convert' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
            ],
            'support' => [
                'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'hs_convert' => 0, 'hhs_convert' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
            ],
            'directNeed' => [
                'quantity' => 0, 'hs' => 0, 'hhs' => 0, 'hs_convert' => 0, 'hhs_convert' => 0, 'cancelledMinus24' => 0, 'billed' => 0,
            ],
        ];

        return $init;
    }

    private function convertTime(float $value): string
    {
        $hours = floor($value);
        $minutes = ($value - $hours) * 60;

        return $hours.'h'.sprintf('%02d', $minutes).'min';
    }

    public function allAvailableUserToAssign(DateTimeImmutable $start, DateTimeImmutable $end): array
    {
        $availableProviders = [];
        $availabilityUsers = $this->availabilityUserRepository->findAvailabilityBetweenDates($start, $end);
        foreach ($availabilityUsers as $availabilityUser) {
            //$helpTasks = $this->helpTaskRepository->findBetweenDatesAndUser($availabilityUser->getUser(),$start, $end);
            if ($availabilityUser->getUser()->isActive()) {
                $availableProviders[] = $availabilityUser->getUser();
            }
        }

        return array_merge($availableProviders, $this->userRepository->findAllUsersAdmins());
    }
}
