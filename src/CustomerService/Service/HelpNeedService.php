<?php

namespace App\CustomerService\Service;

use App\CustomerService\Entity\HelpNeed;
use App\Entity\Client\ClientServices;
use App\Entity\ProviderSession;
use App\Service\ActivityLogger;
use App\Service\MailerService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class HelpNeedService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ActivityLogger $activityLogger,
        private MailerService $mailerService,
        private TranslatorInterface $translator,
        private RouterInterface $router,
    ) {
    }

    public function removeSessionHelpNeed(ProviderSession $session): void
    {
        foreach ($session->getHelpNeeds() as $helpNeed) {
            $session->removeHelpNeed($helpNeed);
            $this->entityManager->remove($helpNeed);
        }
    }

    public function synchronizeSessionHelpNeed(ProviderSession $session): void
    {
        if (!$session->isSessionTest()) {
            $serviceConfiguration = $session->getClient()->getServices();
            $this->generateSessionAssistanceHelpNeed($session, $serviceConfiguration);
            $this->generateSessionSupportHelpNeed($session, $serviceConfiguration);
        } else {
            $this->removeSessionHelpNeed($session);
        }
    }

    private function generateTimeSlots(DateTimeImmutable $start, DateTimeImmutable $end, ClientServices $services, bool $includeOutsideStandardHours): array
    {
        $startTime = strtotime($start->format('H:i'));
        $endTime = strtotime($end->format('H:i'));

        $startConfiguration = $includeOutsideStandardHours ? $services->getOutsideStandardHours()->getStartTimeAM() : $services->getStandardHours()->getStartTimeAM();
        $startTimeSlot = strtotime($startConfiguration->format('H:i'));
        $endConfiguration = $includeOutsideStandardHours ? $services->getOutsideStandardHours()->getEndTimePM() : $services->getStandardHours()->getEndTimePM();
        $endTimeSlot = strtotime($endConfiguration->format('H:i'));

        if ($startTime < $startTimeSlot) {
            $start = $start->setTime($startConfiguration->format('H'), $startConfiguration->format('i'));
            $startTime = strtotime($start->format('H:i'));
        }

        if ($endTime > $endTimeSlot) {
            $end = $end->setTime($endConfiguration->format('H'), $endConfiguration->format('i'));
            $endTime = strtotime($end->format('H:i'));
        }

        $timeSlots = [];
        if (!$includeOutsideStandardHours) {
            $endAmConfiguration = $services->getStandardHours()->getEndTimeAM();
            $endAmTimeSlot = strtotime($endAmConfiguration->format('H:i'));

            if ($startTime < $endAmTimeSlot) {
                if ($endTime > $endAmTimeSlot) {
                    $slotAmEnd = $start->setTime($endAmConfiguration->format('H'), $endAmConfiguration->format('i'));
                    $timeSlots[] = [$start, $slotAmEnd];
                } else {
                    $timeSlots[] = [$start, $end];
                }
            }

            $startPmConfiguration = $services->getStandardHours()->getStartTimePM();
            $startPmTimeSlot = strtotime($startPmConfiguration->format('H:i'));
            if ($endTime > $startPmTimeSlot) {
                if ($startTime < $startPmTimeSlot) {
                    $slotPmStart = $start->setTime($startPmConfiguration->format('H'), $startPmConfiguration->format('i'));
                    $timeSlots[] = [$slotPmStart, $end];
                } else {
                    $timeSlots[] = [$start, $end];
                }
            }
        } else {
            $timeSlots[] = [$start, $end];
        }

        return $timeSlots;
    }

    private function generateSessionAssistanceHelpNeed(ProviderSession $session, ClientServices $services): void
    {
        $serviceStart = \DateTimeImmutable::createFromMutable($session->getDateStart())
            ->modify("- {$services->getAssistanceStart()} minutes");

        if ($session->getAssistanceType() !== ProviderSession::ENUM_NO) {
            $serviceEnd = match ($session->getAssistanceType()) {
                ProviderSession::ENUM_LAUNCH => $serviceStart->modify("+ {$services->getAssistanceDuration()} minutes"),
                ProviderSession::ENUM_PEDA => \DateTimeImmutable::createFromMutable($session->getDateEnd())->modify('+ '.ClientServices::ASSISTANCE_PEDA_DEFAULT_END_DURATION.' minutes'),
                ProviderSession::ENUM_TOTAL, ProviderSession::ENUM_COANIM => \DateTimeImmutable::createFromMutable($session->getDateEnd()),
            };

            $slots = $this->generateTimeSlots($serviceStart, $serviceEnd, $services, true);
        } else {
            $slots = [];
        }

        $this->generateHelpNeed($session, HelpNeed::TYPE_ASSISTANCE, $slots);
    }

    private function generateSessionSupportHelpNeed(ProviderSession $session, ClientServices $services): void
    {
        $serviceStart = \DateTimeImmutable::createFromMutable($session->getDateStart())
            ->modify("- {$services->getTimeBeforeSession()} minutes");

        if ($session->getSupportType() !== ProviderSession::ENUM_SUPPORT_NO) {
            $serviceEnd = match ($session->getSupportType()) {
                ProviderSession::ENUM_SUPPORT_LAUNCH => $serviceStart->modify("+ {$services->getTimeSupportDurationSession()} minutes"),
                ProviderSession::ENUM_SUPPORT_TOTAL => \DateTimeImmutable::createFromMutable($session->getDateEnd()),
            };

            $slots = $this->generateTimeSlots($serviceStart, $serviceEnd, $services, $session->isOutsideStandardHours());
        } else {
            $slots = [];
        }

        $this->generateHelpNeed($session, HelpNeed::TYPE_SUPPORT, $slots);
    }

    private function generateHelpNeed(ProviderSession $session, int $type, array $slots): void
    {
        $existingHelpNeeds = array_values($session->getHelpNeedsByType($type));

        foreach ($slots as $index => $slot) {
            if (array_key_exists($index, $existingHelpNeeds)) {
                $helpNeed = $existingHelpNeeds[$index];
            } else {
                $helpNeed = new HelpNeed($session->getClient());
                $helpNeed->setSession($session)
                    ->setType($type);
                $session->addHelpNeed($helpNeed);
            }

            $helpNeed
                ->setStart($slot[0])
                ->setEnd($slot[1]);
            $this->updateDateTimeHelpTask(helpTasks: $helpNeed->getHelpTasks(), start: $slot[0], end: $slot[1]);
            $this->entityManager->persist($helpNeed);
        }

        if (count($existingHelpNeeds) > count($slots)) {
            foreach ($existingHelpNeeds as $index => $existingHelpNeed) {
                if ($index < count($slots)) {
                    continue;
                }

                $session->removeHelpNeed($existingHelpNeed);
                $this->entityManager->remove($existingHelpNeed);
            }
        }
    }

    public function updateDateTimeHelpTask($helpTasks, DateTimeImmutable $start, DateTimeImmutable $end): void
    {
        foreach ($helpTasks as $helpTask) {
            $helpTask->setStart($start);
            $helpTask->setEnd($end);
        }
    }
}
