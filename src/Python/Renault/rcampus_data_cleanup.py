import pandas as pd
from datetime import datetime
import time
import os  # Import the os module

# Mark the start of execution
start_time = time.time()

print('Start of filtering the Renault R-Campus Excel file')
# Get the day number of the week (Monday=1, Tuesday=2, ..., Saturday=6, Sunday=0)
day_number = (datetime.now().weekday() + 1) % 7

print(f'Day number of the week: {day_number}')
# Path to the Excel file, with the day number of the week
file_path = f'/srv/data/renault_savefiles/RCAMPUS_{day_number}.xlsx'
print(f'Reading file: {file_path}')

# Try to read the file, handle potential errors
try:
    df = pd.read_excel(file_path)
except FileNotFoundError:
    print(f"Error: File '{file_path}' not found.")
except PermissionError:
    print(f"Error: Script lacks permissions to read the file.")
except Exception as e:
    print(f"Error: An unexpected error occurred: {e}")
else:
    # File read successfully, continue with your code
    print('File read successfully.')

    print('Converting "Date début formation" column to datetime...')
    # Convert "Date début formation" column to datetime
    df['R'] = pd.to_datetime(df['Date début formation'], errors='coerce')
    print('Conversion completed.')

    print('Calculating the date 7 days ago...')
    # Calculate the date 7 days ago
    date_7_days_ago = pd.Timestamp(datetime.now().date()) - pd.Timedelta(days=7)
    print(f'Date 7 days ago: {date_7_days_ago.date()}')

    print('Applying filters...')
    # Filter rows where the date in column "R" is after the date 7 days ago
    df_filtered_date = df[df['R'] > date_7_days_ago]

    # Additional filter for the "Titre formation" column
    prefixes = ['CVC', 'CVT', 'WEB']
    prefix_condition = df_filtered_date['Titre formation'].str.startswith(tuple(prefixes))
    df_final_filtered = df_filtered_date[prefix_condition]

    # Save the filtered file
    filtered_file_name = f'/srv/data/renault_savefiles/RCAMPUS_filtered_{day_number}.xlsx'
    df_final_filtered.to_excel(filtered_file_name, index=False)
    print(f'File saved: {filtered_file_name}')

    # Change the file permissions to be read-write for owner, and read-only for others
    print('Modifying file permissions...')
    os.chmod(filtered_file_name, 0o644)
    print('Permissions modified.')

# Mark the end of execution and calculate the duration
end_time = time.time()
duration = end_time - start_time
print(f'Execution time: {duration:.2f} seconds.')
