<?php

namespace App\Repository;

use App\Entity\ProviderSession;
use App\Entity\WebinarConvocation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class WebinarConvocationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WebinarConvocation::class);
    }

    /**
     * @return WebinarConvocation[]
     */
    public function findCommunicationWithTypeToSend(string $type): array
    {
        $now = new \DateTimeImmutable();

        return $this->createQueryBuilder('wc')
            ->join('wc.session', 'session')
            ->andWhere('wc.convocationType = :convocationType')
            ->andWhere('wc.sent = :sent')
            ->andWhere('wc.convocationDate <= :now')
            ->andWhere('session.status = :status')
            ->setParameter('convocationType', $type)
            ->setParameter('sent', false)
            ->setParameter('now', $now)
            ->setParameter('status', ProviderSession::STATUS_SCHEDULED)
            ->getQuery()
            ->getResult();
    }
}
