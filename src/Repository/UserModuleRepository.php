<?php

namespace App\Repository;

use App\Entity\Client\Client;
use App\Entity\User;
use App\Entity\UserModule;
use App\SelfSubscription\Entity\Module;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

class UserModuleRepository extends ServiceEntityRepository
{
    protected Security $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, UserModule::class);

        $this->security = $security;
    }

    public function findUserModulesByUser(User $user): array
    {
        return $this->createQueryBuilder('um')
            ->where('um.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    public function findUserModulesByClient(Client $client): array
    {
        return $this->createQueryBuilder('um')
            ->join('um.user', 'u')
            ->where('u.client = :client')
            ->setParameter('client', $client)
            ->getQuery()
            ->getResult();
    }

    public function findUserModulesByUserAndClient(User $user, Client $client): array
    {
        return $this->createQueryBuilder('um')
            ->where('um.user = :user')
            ->andWhere('um.client = :client')
            ->setParameter('user', $user)
            ->setParameter('client', $client)
            ->getQuery()
            ->getResult();
    }

    public function findUserModuleByModule(Module $module): ?array
    {
        return $this->createQueryBuilder('um')
            ->where('um.module = :module')
            ->setParameter('module', $module)
            ->getQuery()
            ->getResult();
    }
}
