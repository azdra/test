<?php

namespace App\Repository;

use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class WebexRestMeetingConfigurationHolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WebexRestConfigurationHolder::class);
    }

    public function getRemindExpireAccessToken(): array
    {
        $qb = $this->createQueryBuilder('webex_rest_meeting_configuration_holder');
        $qb->andWhere(
            $qb->expr()->orX(
                'webex_rest_meeting_configuration_holder.clientAccessToken != :empty',
                'webex_rest_meeting_configuration_holder.clientAccessToken IS NULL',
            )
        );
        $qb->setParameter('empty', '');
        $qb->andWhere('webex_rest_meeting_configuration_holder.expireAtToken <= :date');

        $datePlus = (new \DateTime())->modify('+7 days');
        $datePlus->setTime(0, 0);
        $qb->setParameter('date', $datePlus);

        return $qb->getQuery()->getResult();
    }
}
