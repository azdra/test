<?php
/**
 * @author:    Emmanuel SMITH <emmanuel.smith@live-session.fr>
 * project:    m3
 * file:    ProviderParticipantRepository.php
 * Date:    17/09/2021
 * Time:    16:47
 */

namespace App\Repository;

use App\Entity\ProviderParticipant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProviderParticipantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderParticipant::class);
    }
}
