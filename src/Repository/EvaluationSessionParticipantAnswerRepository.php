<?php

namespace App\Repository;

use App\Entity\EvaluationProviderSession;
use App\Entity\EvaluationSessionParticipantAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class EvaluationSessionParticipantAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EvaluationSessionParticipantAnswer::class);
    }

    public function countAnswersByEvaluationProviderSession(EvaluationProviderSession $evaluationProviderSession): int
    {
        return $this->createQueryBuilder('a')
            ->select('count(a.id)')
            ->where('a.evaluationProviderSession = :evaluationProviderSession')
            ->setParameter('evaluationProviderSession', $evaluationProviderSession)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
