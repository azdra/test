<?php

namespace App\Repository;

use App\Entity\Client\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function findFirstForStatus(): int
    {
        return $this->createQueryBuilder('client')
            ->select('client.id')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function findAllWithoutArchived(): array
    {
        $qb = $this->createQueryBuilder('c');
        $qb->where('c.active = 1');

        return $qb->getQuery()->getResult();
    }

    public function globalSearch(?string $query): array
    {
        $qb = $this->createQueryBuilder('c');

        if ($query) {
            $qb->where(
                $qb->expr()->orX(
                    $qb->expr()->like('c.name', ':query')
                )
            );
            $qb->setParameter('query', '%'.$query.'%');
            $qb->andWhere('c.active = 1');
        }

        return $qb->getQuery()->getResult();
    }

    public function getValueForSelectInput(?Client $client): array
    {
        $qb = $this->createQueryBuilder('client', 'client.id')
            ->select(['client.id', 'client.name', 'client.defaultLang']);
        if (null !== $client) {
            $qb->andWhere('client = :client')
                ->setParameter('client', $client);
        }

        return $qb->getQuery()->getResult();
    }

    public function getExpiredCreditClients(): array
    {
        $qb = $this->createQueryBuilder('client');
        $qb->where($qb->expr()->lte('client.expirationDateCredits', ':now'));
        $qb->andWhere($qb->expr()->orX(
            $qb->expr()->neq('client.allowedCreditParticipants', 'client.currentParticipantsCount'),
            $qb->expr()->neq('client.allowedCredit', 'client.currentSessionCount'),
        ));

        $qb->setParameter('now', (new \DateTime())->modify('-1 year'));

        return $qb->getQuery()->getResult();
    }
}
