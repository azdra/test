<?php

namespace App\Repository;

use App\Entity\ActivityLog;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\CategorySessionType;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Client\Client;
use App\Entity\Convocation;
use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Entity\WebinarConvocation;
use App\RegistrationPage\Entity\RegistrationPageTemplate;
use App\SelfSubscription\Entity\Module;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ProviderSessionRepository extends ServiceEntityRepository
{
    protected Security $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, ProviderSession::class);
        $this->security = $security;
    }

    public function findFirstForStatus(): int
    {
        return $this->createQueryBuilder('provider_session')
            ->select('provider_session.id')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function findLicenceACScoUsedInSessions(string $licence): mixed
    {
        $qb = $this->getEntityManager()->getRepository(AdobeConnectSCO::class)
            ->createQueryBuilder('ps')
            ->where('ps.licence = :licence')
            ->setParameter('licence', $licence)
            ->getQuery()
            ->getResult();

        return count($qb);
    }

    public function findSessionsWebinarsRenaultToOpen(): mixed
    {
        $qb = $this->getEntityManager()->getRepository(AdobeConnectWebinarSCO::class)
            ->createQueryBuilder('ps')
            ->where('ps.status = :status')
            ->andWhere('ps.dateStart <= :dateTo')
            ->andWhere('ps.dateStart >= :dateFrom')
            ->setParameters(
                [
                    'status' => ProviderSession::STATUS_SCHEDULED,
                    'dateFrom' => (new \DateTime() )->modify('- 120 minutes')->format('Y-m-d H:i:s'),
                    'dateTo' => (new \DateTime() )->modify('+ 24 hours')->format('Y-m-d H:i:s'),
                ]);

        return $qb->getQuery()
            ->getResult();
    }

    public function findLicenceMSTeamsUsedInSessions(string $licence): mixed
    {
        $qb = $this->getEntityManager()->getRepository(MicrosoftTeamsSession::class)
            ->createQueryBuilder('ps')
            ->where('ps.licence = :licence')
            ->setParameter('licence', $licence)
            ->getQuery()
            ->getResult();

        return count($qb);
    }

    public function findLicenceMSTeamsEventUsedInSessions(string $licence): mixed
    {
        $qb = $this->getEntityManager()->getRepository(MicrosoftTeamsEventSession::class)
            ->createQueryBuilder('ps')
            ->where('ps.licence = :licence')
            ->setParameter('licence', $licence)
            ->getQuery()
            ->getResult();

        return count($qb);
    }

    public function findLicenceWebexMeetingUsedInSessions(string $licence): mixed
    {
        $qb = $this->getEntityManager()->getRepository(WebexMeeting::class)
            ->createQueryBuilder('ps')
            ->where('ps.licence = :licence')
            ->setParameter('licence', $licence)
            ->getQuery()
            ->getResult();

        return count($qb);
    }

    public function findLicenceWebexRestMeetingUsedInSessions(string $licence): mixed
    {
        $qb = $this->getEntityManager()->getRepository(WebexRestMeeting::class)
            ->createQueryBuilder('ps')
            ->where('ps.licence = :licence')
            ->setParameter('licence', $licence)
            ->getQuery()
            ->getResult();

        return count($qb);
    }

    public function getGlobalSearchQB(
        ?string $query,
        bool $allowOutdated = false,
        ?UserInterface $user = null,
        ?string $from = null,
        ?string $to = null,
        ?int $configurationHolderId = null,
        ?int $client = null,
        ?int $category = null,
        ?string $test = null,
        ?string $status = null,
        ?string $serviceType = null,
        ?string $registrationType = null,
        ?string $operationCompare = null,
        ?int $numberCompare = null,
        ?array $tags = null,
        ?string $registrationModal = null,
        ?string $selectedRegistration = null,
        ?string $sessionAnimator = null
    ): QueryBuilder {
        $qb = $this->createQueryBuilder(ProviderSession::ALIAS);

        $queryConditions = [];
        if (!$this->security->isGranted(User::ROLE_HOTLINE_CUSTOMER_SERVICE)) {
            $qb = $this->getAddActiveClientCondition($qb);
        } else {
            $qb->join(ProviderSession::ALIAS.'.abstractProviderConfigurationHolder', 'pch')
                ->join('pch.client', 'client')
                ->andwhere('pch.archived = :archived')
                ->setParameter('archived', false);
            $queryConditions[] = $qb->expr()->like('client.name', ':query');
        }

        if ($query) {
            $queryConditions[] = $qb->expr()->like(ProviderSession::ALIAS.'.name', ':query');
            $queryConditions[] = $qb->expr()->like(ProviderSession::ALIAS.'.ref', ':query');
            $queryConditions[] = $qb->expr()->like(ProviderSession::ALIAS.'.businessNumber', ':query');
            $qb->andWhere($qb->expr()->orX(...$queryConditions));
            $qb->setParameter('query', '%'.$query.'%');
        }

        if (!$allowOutdated) {
            $now = new DateTime();
            $now->setTime(0, 0);
            $qb->andWhere(ProviderSession::ALIAS.'.dateEnd > :now')
                ->setParameter('now', $now);
        }

        if ($user !== null) {
            $qb->join(ProviderSession::ALIAS.'.participantRoles', 'participantRoles')
                ->join('participantRoles.participant', 'participant')
                ->andWhere('participant.user = :user')
                ->setParameter('user', $user);
        }

        if ($from !== null && $to !== null) {
            $from = (new \DateTime($from));
            $to = (new \DateTime($to));
            $qb->andWhere('provider_session.dateStart <= :to')
                ->andWhere('provider_session.dateStart >= :from')
                ->setParameter('to', $to)
                ->setParameter('from', $from);
        }

        if ($configurationHolderId) {
            $qb->andWhere('provider_session.abstractProviderConfigurationHolder = :configurationHolder')
                ->setParameter('configurationHolder', $configurationHolderId);
        }

        if ($client !== null) {
            $qb->andWhere('provider_session.client = :client')
                ->setParameter('client', $client);
        }

        if ($category !== null) {
            $qb->andWhere('provider_session.category = :category')
                ->setParameter('category', $category);
        }

        if ($test) {
            $qb->andWhere('provider_session.sessionTest = '.$test);
        }

        if ($tags !== null) {
            foreach ($tags as $tag) {
                $qb->andWhere($qb->expr()->orX('JSON_CONTAINS(provider_session.tags, :tag) = 1'))
                    ->setParameter('tag', '"'.$tag.'"');
            }
        }

        if ($status !== null) {
            switch ($status) {
                case ProviderSession::FINAL_STATUS_DRAFT:
                    $qb->andWhere('provider_session.status = :status')
                        ->setParameter('status', ProviderSession::STATUS_DRAFT);
                    break;
                case ProviderSession::FINAL_STATUS_SCHEDULED:
                    $qb->andWhere('provider_session.status = :status')
                        ->andWhere('provider_session.dateStart > :dateNew')
                        ->andWhere('provider_session.lastConvocationSent is null')
                        ->setParameter('status', ProviderSession::STATUS_SCHEDULED)
                        ->setParameter('dateNew', new \DateTime());
                    break;
                case ProviderSession::FINAL_STATUS_SENT:
                    $qb->andWhere('provider_session.status = :status')
                        ->andWhere('provider_session.dateStart > :dateNew')
                        ->andWhere('provider_session.lastConvocationSent is not null')
                        ->setParameter('status', ProviderSession::STATUS_SCHEDULED)
                        ->setParameter('dateNew', new \DateTime());
                    break;
                case ProviderSession::FINAL_STATUS_RUNNING:
                    $qb->andWhere('provider_session.status = :status')
                        ->andWhere('provider_session.dateStart <= :dateNewStart')
                        ->andWhere('provider_session.dateEnd >= :dateNewEnd')
                        ->setParameter('status', ProviderSession::STATUS_SCHEDULED)
                        ->setParameter('dateNewStart', new \DateTime())
                        ->setParameter('dateNewEnd', new \DateTime());
                    break;
                case ProviderSession::FINAL_STATUS_FINISHED:
                    $qb->andWhere('provider_session.status = :status')
                        ->andWhere('provider_session.dateEnd < :dateNewEnd')
                        ->setParameter('status', ProviderSession::STATUS_SCHEDULED)
                        ->setParameter('dateNewEnd', new \DateTime());
                    break;
                case ProviderSession::FINAL_STATUS_CANCELED:
                    $qb->andWhere('provider_session.status = :status')
                        ->setParameter('status', ProviderSession::STATUS_CANCELED);
                    break;
            }
        }

        if ($serviceType !== null) {
            switch ($serviceType) {
                case 'assistance':
                    $qb->andWhere('provider_session.assistanceType > :enum_no')
                        ->setParameter('enum_no', ProviderSession::ENUM_NO);
                    break;
                case 'support':
                    $qb->andWhere('provider_session.supportType > :enum_support_no')
                        ->setParameter('enum_support_no', ProviderSession::ENUM_SUPPORT_NO);
                    break;
            }
        }

        if ($registrationType !== null && $operationCompare !== null && $numberCompare !== null) {
            switch ($registrationType) {
                case 'participantNumber':
                    $qb->join(ProviderSession::ALIAS.'.participantRoles', 'participantRoles')
                       ->join('participantRoles.participant', 'participant')
                       ->groupBy('participantRoles.session')
                       ->having('COUNT(participantRoles.id) '.$operationCompare.' :numberCompare')
                       ->andWhere('participantRoles.role = \'trainee\'')
                       ->setParameter('numberCompare', $numberCompare);
                    break;
                case 'animatorNumber':
                    $qb->join(ProviderSession::ALIAS.'.participantRoles', 'participantRoles')
                       ->join('participantRoles.participant', 'participant')
                       ->groupBy('participantRoles.session')
                       ->having('COUNT(participantRoles.id) '.$operationCompare.' :numberCompare')
                       ->andWhere('participantRoles.role = \'animator\'')
                       ->setParameter('numberCompare', $numberCompare);
                    break;
            }
        }

        if ($registrationModal !== null && $selectedRegistration !== null) {
            switch ($registrationModal) {
                case 'page':
                    $qb->andWhere('provider_session.registrationPageTemplate = :registrationPageTemplate')
                        ->setParameter('registrationPageTemplate', $selectedRegistration);
                    break;
                case 'catalog':
                    $qb->join('provider_session.subject', 'subject')
                       ->join('subject.theme', 'theme')
                       ->join('theme.module', 'module')
                       ->andWhere('module = :selectedModule')
                       ->setParameter('selectedModule', $selectedRegistration);
                    break;
            }
        }

        if ($sessionAnimator) {
            if ($sessionAnimator !== 'false') {
                $qb->join(ProviderSession::ALIAS.'.participantRoles', 'participantRoles')
                    ->join('participantRoles.participant', 'participant')
                    ->andWhere('participantRoles.role = \'animator\'')
                    ->andWhere('participant.user = :user')
                    ->setParameter('user', $sessionAnimator);
            }
        }

        return $qb;
    }

    public function globalSearch(
        ?string $query,
        bool $allowOutdated = false,
        ?UserInterface $user = null,
        ?string $from = null,
        ?string $to = null,
        ?int $configurationHolderId = null,
        int $firstResult = 0,
        int $countResult = 0,
        $filtersSearch = [],
    ): array {
        $client = null;
        $category = null;
        $test = false;
        $status = null;
        $serviceType = null;
        $registrationType = null;
        $registrationModal = null;
        $selectedRegistration = null;
        $operationCompare = null;
        $numberCompare = null;
        $tags = null;
        $sessionAnimator = false;
        if (!empty($filtersSearch)) {
            $allowOutdated = !$filtersSearch->futureSessions;
            $configurationHolderId = $filtersSearch->connector;
            $from = $filtersSearch->dateStart;
            $to = $filtersSearch->dateEnd;
            $client = $filtersSearch->client;
            $category = $filtersSearch->categoryType;
            $test = $filtersSearch->sessionTest;
            $status = $filtersSearch->statusSession;
            $serviceType = $filtersSearch->serviceType;
            $registrationType = $filtersSearch->registrationType;
            $selectedRegistration = $filtersSearch->selectedRegistration;
            $registrationModal = $filtersSearch->registrationModal;
            $operationCompare = $filtersSearch->operationCompare;
            $numberCompare = $filtersSearch->NumberCompare;
            $tags = $filtersSearch->tags;
            $sessionAnimator = $filtersSearch->sessionAnimator;
        }
        $qb = $this->getGlobalSearchQB($query, $allowOutdated, $user, $from, $to, $configurationHolderId, $client, $category, $test, $status, $serviceType, $registrationType, $operationCompare, $numberCompare, $tags, $registrationModal, $selectedRegistration, $sessionAnimator);

        if ($countResult > 0) {
            $qb->setFirstResult($firstResult)
                ->setMaxResults($countResult);
        }

        $qb->orderBy(ProviderSession::ALIAS.'.dateStart', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function countGlobalSearchResult(
        ?string $query,
        bool $allowOutdated = false,
        ?UserInterface $user = null,
        ?string $from = null,
        ?string $to = null,
        ?int $configurationHolderId = null,
        $filtersSearch = [],
    ): int {
        $client = null;
        $category = null;
        $test = false;
        $status = null;
        $serviceType = null;
        $registrationType = null;
        $operationCompare = null;
        $numberCompare = null;
        $registrationModal = null;
        $selectedRegistration = null;
        $tags = null;
        $sessionAnimator = false;

        if (!empty($filtersSearch)) {
            $allowOutdated = !$filtersSearch->futureSessions;
            $configurationHolderId = $filtersSearch->connector;
            $from = $filtersSearch->dateStart;
            $to = $filtersSearch->dateEnd;
            $client = $filtersSearch->client;
            $category = $filtersSearch->categoryType;
            $test = $filtersSearch->sessionTest;
            $status = $filtersSearch->statusSession;
            $selectedRegistration = $filtersSearch->selectedRegistration;
            $registrationModal = $filtersSearch->registrationModal;
            $serviceType = $filtersSearch->serviceType;
            $registrationType = $filtersSearch->registrationType;
            $operationCompare = $filtersSearch->operationCompare;
            $numberCompare = $filtersSearch->NumberCompare;
            $tags = $filtersSearch->tags;
            $sessionAnimator = $filtersSearch->sessionAnimator;
        }
        $result = $this->getGlobalSearchQB($query, $allowOutdated, $user, $from, $to, $configurationHolderId, $client, $category, $test, $status, $serviceType, $registrationType, $operationCompare, $numberCompare, $tags, $registrationModal, $selectedRegistration, $sessionAnimator)
                       ->select('COUNT(provider_session.id)')
                       ->getQuery();

        if ($registrationType) {
            return count($result->getResult());
        } else {
            return $result->getSingleScalarResult();
        }
    }

    public function findOldSessionsByMinusDate(int $minusDate): array
    {
        $dateMinPurge = (new \DateTimeImmutable())->modify('-'.$minusDate.' day')->setTime(23, 59);

        $qb = $this->createQueryBuilder(ProviderSession::ALIAS);

        $qb->andWhere(ProviderSession::ALIAS.'.dateEnd < :dateEndFilter')
            ->setParameter('dateEndFilter', $dateMinPurge);
        //printf($qb->getQuery()->getSQL() . ' >>>> [' . $qb->getQuery()->getParameters()[0]->getValue()->format('Y-m-d H:i:s') . ']');
        return $qb->getQuery()->getResult();
    }

    public function findSessionForPeriod(DateTime $dateStart, DateTime $dateEnd): array
    {
        $qb = $this->createQueryBuilder(ProviderSession::ALIAS);

        if (!$this->security->isGranted(User::ROLE_ADMIN)) {
            $qb = $this->getAddActiveClientCondition($qb);
        } else {
            $qb->join(ProviderSession::ALIAS.'.abstractProviderConfigurationHolder', 'pch')
                ->join('pch.client', 'client');
        }
        $qb->andWhere(ProviderSession::ALIAS.'.dateEnd < :dateEndFilter')
            ->setParameter('dateEndFilter', $dateEnd);
        $qb->andWhere(ProviderSession::ALIAS.'.dateStart > :dateStartFilter')
            ->setParameter('dateStartFilter', $dateStart);

        return $qb->getQuery()->getResult();
    }

    public function findSessionWithSeveralAnimatorsForPeriodAndClient(DateTime $dateStart, DateTime $dateEnd, Client $client): array
    {
        $qb = $this->createQueryBuilder(ProviderSession::ALIAS);

        $qb->join(ProviderSession::ALIAS.'.abstractProviderConfigurationHolder', 'pch')
                ->join('pch.client', 'client');

        $qb->andWhere(ProviderSession::ALIAS.'.dateEnd < :dateEndFilter')
            ->setParameter('dateEndFilter', $dateEnd);
        $qb->andWhere(ProviderSession::ALIAS.'.dateStart > :dateStartFilter')
            ->setParameter('dateStartFilter', $dateStart);
        $qb->andWhere('pch.client = :client')
            ->setParameter('client', $client);

        return $qb->getQuery()->getResult();
    }

    public function findSessionToFetchReportPresence(): array
    {
        $dateEnd = (new \DateTime())->modify('-1 day');
        $qb = $this->createQueryBuilder('provider_session')
            ->andWhere('provider_session.dateEnd < :dateEnd')
            ->andWhere('provider_session.lastPresenceStatusSynchronization IS NULL')
            ->setParameter('dateEnd', $dateEnd);

        $qb->andWhere($qb->expr()->orX(
            'provider_session INSTANCE OF '.AdobeConnectSCO::class,
            'provider_session INSTANCE OF '.AdobeConnectWebinarSCO::class,
            'provider_session INSTANCE OF '.WebexMeeting::class,
            'provider_session INSTANCE OF '.WebexRestMeeting::class,
            'provider_session INSTANCE OF '.MicrosoftTeamsSession::class,
            'provider_session INSTANCE OF '.MicrosoftTeamsEventSession::class
        ));

        return $qb->getQuery()->getResult();
    }

    public function findUserAccessibleSessionsForPeriod(DateTime $dateStart, DateTime $dateEnd, User $user): array
    {
        return $this->createQueryBuilder(ProviderSession::ALIAS)
            ->join(ProviderSession::ALIAS.'.abstractProviderConfigurationHolder', 'pch')
            ->where('pch.client = :client')
            ->setParameter('client', $user->getClient())
            ->andWhere(ProviderSession::ALIAS.'.dateEnd < :dateEndFilter')
            ->setParameter('dateEndFilter', $dateEnd)
            ->andWhere(ProviderSession::ALIAS.'.dateStart > :dateStartFilter')
            ->setParameter('dateStartFilter', $dateStart)
            ->getQuery()
            ->getResult();
    }

    public function findAllSessionForPeriod(DateTime $dateStart, DateTime $dateEnd): array
    {
        return $this->createQueryBuilder(ProviderSession::ALIAS)
            ->join(ProviderSession::ALIAS.'.abstractProviderConfigurationHolder', 'pch')
            ->join('pch.client', 'client')
            ->andWhere(ProviderSession::ALIAS.'.dateEnd < :dateEndFilter')
            ->setParameter('dateEndFilter', $dateEnd)
            ->andWhere(ProviderSession::ALIAS.'.dateStart > :dateStartFilter')
            ->setParameter('dateStartFilter', $dateStart)
            ->getQuery()
            ->getResult();
    }

    public function findClientSessionForPeriod(Client $client, DateTime $dateStart, DateTime $dateEnd): array
    {
        $qb = $this->createQueryBuilder(ProviderSession::ALIAS);

        $qb->join(ProviderSession::ALIAS.'.abstractProviderConfigurationHolder', 'pch')
            ->where('pch.client = :client')
            ->setParameter('client', $client);

        $qb->andWhere(ProviderSession::ALIAS.'.dateEnd < :dateEndFilter')
            ->setParameter('dateEndFilter', $dateEnd);
        $qb->andWhere(ProviderSession::ALIAS.'.dateStart > :dateStartFilter')
            ->setParameter('dateStartFilter', $dateStart);
        $qb->andWhere(ProviderSession::ALIAS.'.sessionTest = :isTest')
            ->setParameter('isTest', 0);

        return $qb->getQuery()->getResult();
    }

    public function findConfigurationHolderWithSessionForPeriod(\DateTimeImmutable $from, \DateTimeImmutable $to, int $idConfigurationHolder): array
    {
        $qb = $this->createQueryBuilder('s');
        $qb->where('s.abstractProviderConfigurationHolder = :idProviderConfigurationHolder')
            ->setParameter('idProviderConfigurationHolder', $idConfigurationHolder);
        $qb->andWhere(
            $qb->expr()->orX(
                // Vérifie si la session commence dans la période spécifiée
                $qb->expr()->between('s.dateStart', ':from', ':to'),

                // Vérifie si la session se termine dans la période spécifiée
                $qb->expr()->between('s.dateEnd', ':from', ':to'),

                // Vérifie si la session couvre entièrement la période spécifiée
                $qb->expr()->andX(
                    $qb->expr()->lte('s.dateStart', ':from'),
                    $qb->expr()->gte('s.dateEnd', ':to')
                )
            )
        )
            ->setParameter('from', $from)
            ->setParameter('to', $to);

        return $qb->getQuery()
            ->getResult();
    }

    public function findComingAndRunning(?User $user = null): array
    {
        return $this->globalSearch(null, false, $user);
    }

    public function findWithConvocation($id): array
    {
        $qb = $this->createQueryBuilder(ProviderSession::ALIAS);

        if (!$this->security->isGranted(User::ROLE_ADMIN)) {
            $qb = $this->getAddActiveClientCondition($qb);
        }

        $qb->andWhere(ProviderSession::ALIAS.'.convocation = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getResult();
    }

    public function getAddActiveClientCondition(QueryBuilder $qb): QueryBuilder
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $client = $user->getClient();

        $qb->join(ProviderSession::ALIAS.'.abstractProviderConfigurationHolder', 'pch')
           ->where('pch.client = :client')
           ->andwhere('pch.archived = :archived')
           ->setParameter('archived', false)
           ->setParameter('client', $client);

        return $qb;
    }

    public function findAllSessionsWithPastNotificationSendingDate(): array
    {
        $qb = $this->createQueryBuilder('s');

        return $qb
            ->where('s.notificationDate <= :now')
            ->andWhere(':now  <= s.dateEnd')
            ->andWhere('s.status = :status')
            ->setParameter('now', new DateTime())
            ->setParameter('status', ProviderSession::STATUS_SCHEDULED)
            ->getQuery()
            ->getResult();
    }

    public function findAllSessionsWebinarWithStatusScheduled(): array
    {
        $qb = $this->createQueryBuilder('s');

        return $qb
            ->join('s.webinarConvocation', 'webinar_convocation')
            ->where(':now  <= s.dateEnd')
            ->orWhere('webinar_convocation.convocationType = :thank_you AND webinar_convocation.sent = :sent')
            ->andWhere('s.category = :category')
            ->setParameter('now', (new DateTime()))
            ->setParameter('category', CategorySessionType::CATEGORY_SESSION_WEBINAR->value)
            ->setParameter('thank_you', WebinarConvocation::TYPE_THANK_YOU)
            ->setParameter('sent', false)
            ->getQuery()
            ->getResult();
    }

    public function findAllBetweenDates(\DateTimeImmutable $from, \DateTimeImmutable $to): array
    {
        /** @var User $currentUser */
        $currentUser = $this->security->getUser();

        $qb = $this->createQueryBuilder('s')
                   ->where('s.dateStart >= :from')
            ->andWhere('s.dateEnd <= :to')
            ->setParameter('from', $from->format('Y-m-d H:i:s'))
            ->setParameter('to', $to->format('y-m-d H:i:s'));

        if (!empty($currentUser)) {
            if (!$currentUser->isAnAdmin()) {
                $qb->join('s.abstractProviderConfigurationHolder', 'ch')
                    ->andWhere('ch.client = :client')
                    ->setParameter('client', $currentUser->getClient()->getId());
            }
        }

        return $qb->getQuery()
            ->getResult();
    }

    public function findAllBetweenDatesForResultEvaluations(\DateTimeImmutable $from, \DateTimeImmutable $to): array
    {
        /** @var User $currentUser */
        $currentUser = $this->security->getUser();

        $qb = $this->createQueryBuilder('s')
            ->where('s.dateStart >= :from')
            ->andWhere('s.dateEnd <= :to')
            ->setParameter('from', $from->format('Y-m-d H:i:s'))
            ->setParameter('to', $to->format('y-m-d H:i:s'));

        if (!empty($currentUser)) {
            if (!$currentUser->isAnAdmin()) {
                $qb->join('s.abstractProviderConfigurationHolder', 'ch')
                    ->andWhere('ch.client = :client')
                    ->setParameter('client', $currentUser->getClient()->getId());
            }
        }

        return $qb->getQuery()
            ->getResult();
    }

    public function findAllBetweenDatesWithOrderByClientAndSessionsStart(\DateTimeImmutable $from, \DateTimeImmutable $to): array
    {
        /** @var User $currentUser */
        $currentUser = $this->security->getUser();

        $qb = $this->createQueryBuilder('s')
            ->where('s.dateStart >= :from')
            ->andWhere('s.dateEnd <= :to')
            ->setParameter('from', $from->format('Y-m-d H:i:s'))
            ->setParameter('to', $to->format('Y-m-d H:i:s'));

        if (!empty($currentUser)) {
            $qb->join('s.abstractProviderConfigurationHolder', 'ch');
            if (!$currentUser->isAnAdmin()) {
                $qb->andWhere('ch.client = :client')
                    ->setParameter('client', $currentUser->getClient()->getId());
            }
            $qb->orderBy('ch.client, s.dateStart', 'ASC');
        } else {
            $qb->orderBy('s.dateStart', 'ASC');
        }

        return $qb->getQuery()
            ->getResult();
    }

    public function findAllBetweenDatesWithoutAnimator(\DateTimeImmutable $from, \DateTimeImmutable $to): array
    {
        /** @var User $currentUser */
        $currentUser = $this->security->getUser();

        $qbSearchAnimatorForSession = $this->getEntityManager()->createQueryBuilder('query_builder')
            ->select('1')
            ->from(ProviderParticipantSessionRole::class, 'ppsr')
            ->where('ppsr.role = \'animator\'')
            ->andWhere('ppsr.session = s.id');

        $qb = $this->createQueryBuilder('s')
            ->where('s.dateStart >= :from')
            ->andWhere('s.dateEnd <= :to')
            ->andWhere('NOT EXISTS('.$qbSearchAnimatorForSession.')')
            ->setParameter('from', $from->format('Y-m-d H:i:s'))
            ->setParameter('to', $to->format('y-m-d H:i:s'));

        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            $qb->join('s.abstractProviderConfigurationHolder', 'ch')
                ->andWhere('ch.client = :client')
                ->setParameter('client', $currentUser->getClient()->getId());
        }

        return $qb->getQuery()
            ->getResult();
    }

    public function currentSessionQB(Client $client, ?\DateTimeImmutable $start, ?\DateTimeImmutable $end): QueryBuilder
    {
        $qb = $this->createQueryBuilder('provider_session')
            ->join('provider_session.abstractProviderConfigurationHolder', 'abstract_provider_configuration_holder')
            ->where('abstract_provider_configuration_holder.client = :client')
            ->setParameter('client', $client)
        ;

        if (!empty($start) && !empty($end)) {
            $qb->andWhere(
                $qb->expr()->orX(
                    'provider_session.dateStart BETWEEN :start AND :end',
                    'provider_session.dateEnd BETWEEN :start AND :end',
                    'provider_session.dateStart < :start AND provider_session.dateEnd > :end'
                )
            )
                ->setParameter('start', $start)
                ->setParameter('end', $end);
        } elseif (!empty($start)) {
            $qb->andWhere('provider_session.dateStart > :start')
                ->setParameter('start', $start);
        } elseif (!empty($end)) {
            $qb->andWhere('provider_session.dateEnd < :end')
                ->setParameter('end', $end);
        }

        return $qb;
    }

    public function getCurrentSession(Client $client, \DateTimeImmutable $start = null, \DateTimeImmutable $end = null): array
    {
        return $this->currentSessionQB($client, $start, $end)
            ->select('provider_session')
            ->orderBy('provider_session.dateStart', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getPeriodStatistics(Client $client, \DateTimeImmutable $start = null, \DateTimeImmutable $end = null): array
    {
        return array_merge(
            $this->currentSessionQB($client, $start, $end)
                ->select('COUNT(DISTINCT provider_session.id) as count_session')
                ->addSelect('SUM(provider_session.duration) as sum_duration')
                ->getQuery()
                ->getSingleResult(),
            $this->currentSessionQB($client, $start, $end)
                ->select('COUNT(DISTINCT provider_session.id) as count_session')
                ->leftJoin('provider_session.participantRoles', 'participant_roles')
                ->addSelect('COUNT(participant_roles) as count_participant')
                ->getQuery()
                ->getSingleResult()
        );
    }

    public function findClientSessionsMultipleAnimatorsForPeriod(Client $client, \DateTimeImmutable $start, \DateTimeImmutable $end): array
    {
        $qb = $this->createQueryBuilder(ProviderSession::ALIAS);

        $qb->join(ProviderSession::ALIAS.'.participantRoles', 'participantRoles')
            ->join('participantRoles.participant', 'participant')
            ->groupBy('participantRoles.session')
            ->having('countAnimator > :some_count')
            ->setParameter('some_count', 1)
            ->addSelect('COUNT(participantRoles.id) as countAnimator')
            ->where('participantRoles.role = \'animator\'');
        $qb->join(ProviderSession::ALIAS.'.abstractProviderConfigurationHolder', 'pch')
            ->where('pch.client = :client')
            ->setParameter('client', $client);

        return $qb->getQuery()->getResult();
    }

    public function getEvaluationsAnswerByPeriod(Client $client, \DateTimeImmutable $start, \DateTimeImmutable $end, string $surveyId): array
    {
        $results = $this->currentSessionQB($client, $start, $end)
            ->select('answers.answer')
            ->innerJoin('provider_session.participantRoles', 'participant_roles')
            ->innerJoin('participant_roles.answers', 'answers')
            ->innerJoin('answers.evaluation', 'evaluation')
            ->andWhere('evaluation.surveyId = :survey')
            ->setParameter('survey', $surveyId)
            ->getQuery()
            ->getResult();

        return array_column($results, 'answer');
    }

    public function getSessionToSendPresenceReportOld(Client $client, DateTime $dateToSend): array
    {
        $qbSearchActivityLogForSession = $this->getEntityManager()->createQueryBuilder('query_builder')
            ->select('1')
            ->from(ActivityLog::class, 'activity_log')
            ->where('activity_log.action = :sendReportPresenceAction')
            ->andWhere("JSON_EXTRACT(activity_log.infos, '$.session.id') = provider_session.id");

        return $this->createQueryBuilder('provider_session')
            ->join('provider_session.abstractProviderConfigurationHolder', 'configuration_holder')
            ->where('configuration_holder.client = :client')
            ->andWhere('provider_session.dateEnd <= :dateToSend')
            ->andWhere("NOT EXISTS($qbSearchActivityLogForSession)")
            ->setParameter('client', $client)
            ->setParameter('dateToSend', $dateToSend)
            ->setParameter('sendReportPresenceAction', ActivityLog::ACTION_SEND_PRESENCE_REPORT)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getSessionToSendPresenceReport(Client $client, DateTime $dateStart): array
    {
        $qb = $this->createQueryBuilder(ProviderSession::ALIAS);

        $qb->join(ProviderSession::ALIAS.'.abstractProviderConfigurationHolder', 'pch')
            ->where('pch.client = :client')
            ->setParameter('client', $client);
        $qb->andWhere(ProviderSession::ALIAS.'.dateEnd < :dateStartFilter')
            ->andWhere(ProviderSession::ALIAS.'.sentPresenceReport = :sent')
            ->andWhere(ProviderSession::ALIAS.'.status = :status')
            ->setParameter('sent', false)
            ->setParameter('dateStartFilter', $dateStart)
            ->setParameter('status', ProviderSession::STATUS_SCHEDULED);

        return $qb->getQuery()->getResult();
    }

    public function findAllSessionByUser(User $user): array
    {
        $qb = $this->createQueryBuilder('s')
            ->innerJoin('s.participantRoles', 'psr')
            ->innerJoin('psr.participant', 'participant')
            ->where('participant.user = :user')
            ->setParameters([
                'user' => $user,
            ]);

        return $qb->getQuery()
            ->getResult();
    }

    public function getSubscribedSessionOfModuleQB(Module $module, User $user): QueryBuilder
    {
        return $this->createQueryBuilder('session')
            ->join('session.subject', 'subject')
            ->join('subject.theme', 'theme')
            ->join('session.participantRoles', 'subscribe')
            ->join('subscribe.participant', 'participant')
            ->where('theme.module = :module')
            ->andWhere('participant.user = :user')
            ->setParameter('module', $module)
            ->setParameter('user', $user);
    }

    public function getRunningSubscribedBySubject(Module $module, User $user): array
    {
        $qb = $this->getSubscribedSessionOfModuleQB($module, $user);

        return $qb->indexBy('session', 'session.subject')
            ->andWhere('session.status = :scheduled')
            ->andWhere('session.dateEnd > CURRENT_TIMESTAMP()')
            ->setParameter('scheduled', ProviderSession::STATUS_SCHEDULED)
            ->getQuery()
            ->getResult();
    }

    public function getAllSubscribedSessionOfModule(Module $module, User $user): array
    {
        return $this->getSubscribedSessionOfModuleQB($module, $user)
            ->getQuery()
            ->getResult();
    }

    public function findAllBetweenDatesWithConfigurationHolder(\DateTimeImmutable $from, \DateTimeImmutable $to, int $idConfigurationHolder): array
    {
        $qb = $this->createQueryBuilder('s')
                   ->where('s.dateStart >= :from')
                   ->andWhere('s.dateEnd <= :to')
                   ->andWhere('s.abstractProviderConfigurationHolder = :idProviderConfigurationHolder')
                   ->setParameter('from', $from->format('Y-m-d H:i:s'))
                   ->setParameter('to', $to->format('y-m-d H:i:s'))
                   ->setParameter('idProviderConfigurationHolder', $idConfigurationHolder);

        return $qb->getQuery()
                  ->getResult();
    }

    public function getSessionsDashboardServices(\DateTimeImmutable $from, \DateTimeImmutable $to, array $params = []): array
    {
        $qb = $this->createQueryBuilder('s');
        $qb = $qb->where('s.dateStart >= :from')
            ->andWhere('s.dateEnd <= :to')
            ->setParameter('from', $from->format('Y-m-d H:i:s'))
            ->setParameter('to', $to->format('Y-m-d H:i:s'))
        ;

        if (array_key_exists('client', $params) && $params['client']) {
            $qb->andWhere($qb->expr()->eq('s.client', ':client'))
                ->setParameter('client', $params['client']);
        }

        $qb->orderBy('s.dateStart', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getProviderSessionsConvocation(Convocation $convocation): array
    {
        $query = $this->createQueryBuilder('s')
            ->where('s.convocation = :idConvocation')
            ->setParameter('idConvocation', $convocation->getId());

        return $query->getQuery()->getResult();
    }

    public function findByDateBetween(\DateTimeImmutable $start, \DateTimeImmutable $end): array
    {
        $qb = $this->createQueryBuilder('e');
        $qb->where('e.dateStart BETWEEN :start AND :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end);
        $qb->orderBy('e.dateStart', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function findByBetweenDateAndClientsList(\DateTimeImmutable $from, \DateTimeImmutable $to, array $clientIds): array
    {
        $qb = $this->createQueryBuilder('s');
        $qb->where('s.dateStart BETWEEN :start AND :end')
            ->andWhere('s.client IN (:clientIds)')
            ->setParameter('start', $from)
            ->setParameter('end', $to)
            ->setParameter('clientIds', $clientIds);

        return $qb->getQuery()->getResult();
    }

    public function getSessionByRegistrationPageTemplate(RegistrationPageTemplate $registrationPageTemplate, bool $allowOutdated = false, string|null $query = null): array
    {
        $qb = $this->createQueryBuilder('s');
        $qb->where('s.registrationPageTemplate = :registrationPageTemplate')
            ->setParameter('registrationPageTemplate', $registrationPageTemplate);

        if ($query !== 'null') {
            $queryConditions[] = $qb->expr()->like('s.name', ':query');
            $queryConditions[] = $qb->expr()->like('s.ref', ':query');
            $queryConditions[] = $qb->expr()->like('s.businessNumber', ':query');
            $qb->andWhere($qb->expr()->orX(...$queryConditions));
            $qb->setParameter('query', '%'.$query.'%');
        }

        if (!$allowOutdated) {
            $now = new DateTime();
            $now->setTime(0, 0);
            $qb->andWhere('s.dateEnd > :now')
                ->setParameter('now', $now);
        }

        return $qb->getQuery()->getResult();
    }

    public function getAllSessionOfModule(Module $module): array
    {
        return $this->createQueryBuilder('session')
            ->join('session.subject', 'subject')
            ->join('subject.theme', 'theme')
            ->where('theme.module = :module')
            ->setParameter('module', $module)
            ->getQuery()
            ->getResult();
    }
}
