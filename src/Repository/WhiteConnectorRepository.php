<?php

namespace App\Repository;

use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Entity\ProviderSession;
use App\Entity\White\WhiteSession;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class WhiteConnectorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WhiteSession::class);
    }

    /*public function getUsedLicencesOnSlot(WhiteConfigurationHolder $configurationHolder, DateTime $from, DateTime $to, int $safetyTime, ?int $sessionIdToExclude = null): array
    {
        $qb = $this->createQueryBuilder('p');

        $from = (clone $from)->modify("- $safetyTime minutes");
        $to = (clone $to)->modify("+ $safetyTime minutes");
        $query = $qb->select('p.id, p.licence')
           ->where('p.status = :status')
            ->andWhere('p.abstractProviderConfigurationHolder = :configurationHolder')
           ->andWhere('p.dateStart <= :dateTo')
           ->andWhere('p.dateEnd >= :dateFrom')
           ->setParameters(
               [
                   'status' => ProviderSession::STATUS_SCHEDULED,
                   'dateFrom' => $from->format('Y-m-d H:i:s'),
                   'dateTo' => $to->format('Y-m-d H:i:s'),
                   'configurationHolder' => $configurationHolder,
               ]);

        if (!is_null($sessionIdToExclude)) {
            $query->andWhere('p.id != :sessionId')
                ->setParameter('sessionId', $sessionIdToExclude);
        }

        return $query->getQuery()
                     ->getArrayResult();
    }*/
}
