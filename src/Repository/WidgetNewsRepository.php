<?php

namespace App\Repository;

use App\Entity\WidgetNews;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

class WidgetNewsRepository extends ServiceEntityRepository
{
    protected Security $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, WidgetNews::class);
        $this->security = $security;
    }

    public function findByLangs(array $lang, int $limit = null): array
    {
        $qb = $this->createQueryBuilder('widget_news');
        $qb->orderBy('widget_news.date', 'DESC')
            ->where($qb->expr()->in('widget_news.languages', ':lang'))
            ->setParameter('lang', $lang)
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }
}
