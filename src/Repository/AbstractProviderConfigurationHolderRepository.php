<?php

namespace App\Repository;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AbstractProviderConfigurationHolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AbstractProviderConfigurationHolder::class);
    }
}
