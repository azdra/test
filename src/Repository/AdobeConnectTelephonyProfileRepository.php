<?php

namespace App\Repository;

use App\Entity\Adobe\AdobeConnectTelephonyProfile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AdobeConnectTelephonyProfileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdobeConnectTelephonyProfile::class);
    }
}
