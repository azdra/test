<?php

namespace App\Repository;

use App\Command\Async\AsyncWorkerWatcherCommand;
use App\Entity\AsyncTask;
use App\Service\AsyncWorker\TaskInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AsyncTaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AsyncTask::class);
    }

    public function getNextTask(): array
    {
        return $this->createQueryBuilder('at')
            ->where('at.status = :statusReady')
            ->orderBy('at.id', 'ASC')
            ->setMaxResults(AsyncWorkerWatcherCommand::MAX_PROCESSING_TASK)
            ->setParameter('statusReady', TaskInterface::STATUS_READY)
            ->getQuery()
            ->getResult();
    }

    public function getUnfinishedAndBlockedTasks(\DateTimeImmutable $limitDate): array
    {
        return $this->createQueryBuilder('at')
            ->where('at.status = :statusProcessing')
            ->andWhere('at.createdAt < :limitDate')
            ->andWhere('at.result LIKE :emptyResult')
            ->orderBy('at.id', 'ASC')
            ->setMaxResults(AsyncWorkerWatcherCommand::MAX_PROCESSING_TASK)
            ->setParameter('statusProcessing', TaskInterface::STATUS_PROCESSING)
            ->setParameter('limitDate', $limitDate)
            ->setParameter('emptyResult', '[]')
            ->getQuery()
            ->getResult();
    }

    public function getLastTaskByStatus(string $status): array
    {
        return $this->createQueryBuilder('at')
                    ->where('at.status = :statusReady')
            ->andWhere('at.createdAt >= :date')
                    ->orderBy('at.id', 'ASC')
                    ->setParameter('statusReady', $status)
                    ->setParameter('date', (new \DateTime())->modify('- 1 day'))
                    ->getQuery()
            ->getResult();
    }

    public function getNextTasks(): array
    {
        return $this->createQueryBuilder('at')
                    ->where('at.status = :statusReady')
                    ->orderBy('at.id', 'ASC')
                    ->setParameter('statusReady', TaskInterface::STATUS_READY)
                    ->getQuery()
                    ->getResult();
    }
}
