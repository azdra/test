<?php

namespace App\Repository;

use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class WebexMeetingConfigurationHolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WebexConfigurationHolder::class);
    }
}
