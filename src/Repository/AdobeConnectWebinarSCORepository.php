<?php

namespace App\Repository;

use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AdobeConnectWebinarSCORepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdobeConnectWebinarSCO::class);
    }

    public function getChildrenUniqueScoIdentifier(AdobeConnectWebinarSCO $root): array
    {
        return $this->createQueryBuilder('acs')
            ->select('acs.uniqueScoIdentifier')
            ->where('acs.parent = :root')
            ->setParameter('root', $root)
            ->getQuery()->getArrayResult();
    }

    public function getActiveMeetings(): array
    {
        $qb = $this->createQueryBuilder('acs');
        $qb->where($qb->expr()->lte('acs.dateStart', ':now'))
            ->andWhere($qb->expr()->gt('acs.dateEnd', ':now'))
            ->andWhere('acs.type = :meetingType')
            ->setParameter('now', new \DateTime())
            ->setParameter('meetingType', AdobeConnectWebinarSCO::TYPE_MEETING);

        return $qb->getQuery()->getResult();
    }

    public function getSCOBetweenDateForAudioFromAdobeConnect(\DateTime $start, \DateTime $stop, string $type): array
    {
        $qb = $this->createQueryBuilder('acs');

        $qb
            ->andWhere('acs.dateStart BETWEEN :start AND :stop')
            ->setParameter('start', $start)
            ->setParameter('stop', $stop)
            ->innerJoin('acs.providerAudio', 'provider_audio')
            ->andWhere('provider_audio.connectionType = :type')
            ->setParameter('type', $type);

        return $qb->getQuery()->getResult();
    }

    public function checkProviderAudioAvailabilityForDate(AdobeConnectWebinarSCO $adobeConnectWebinarSCO, \DateTime $start, \DateTime $end): bool
    {
        return null === $this->createQueryBuilder('sco')
            ->select('1')
            ->join('sco.providerAudio', 'provider_audio')
            ->where('sco != :currentSco')
            ->andWhere('sco.dateStart BETWEEN :start AND :end')
            ->andWhere('provider_audio.profileIdentifier = :providerAudioProfileIdentifier')
            ->setParameter('currentSco', $adobeConnectWebinarSCO)
            ->setParameter('providerAudioProfileIdentifier', $adobeConnectWebinarSCO->getProviderAudio())
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getUsedLicenceOnSLot(AdobeConnectWebinarConfigurationHolder $configurationHolder, array $licences, \DateTimeImmutable $from, \DateTimeImmutable $to, ?string $sessionIdToExclude): array
    {
        $qb = $this->createQueryBuilder('adobe_connect_webinar_sco')
            ->join('adobe_connect_webinar_sco.participantRoles', 'participant_roles')
            ->join('participant_roles.participant', 'participant')
            ->join('participant.user', 'user')
            ->select('user.email')
            ->distinct()
            ->andWhere('participant_roles.role = :role_animator')
            ->andWhere('user.email IN (:licences)')
            ->andWhere('adobe_connect_webinar_sco.dateStart <= :to')
            ->andWhere('adobe_connect_webinar_sco.dateEnd >= :from')
            ->andWhere('adobe_connect_webinar_sco.abstractProviderConfigurationHolder = :configurationHolder')
            ->setParameter('role_animator', ProviderParticipantSessionRole::ROLE_ANIMATOR)
            ->setParameter('licences', $licences)
            ->setParameter('to', $to)
            ->setParameter('from', $from)
            ->setParameter('configurationHolder', $configurationHolder);

        if ($sessionIdToExclude) {
            $qb->andWhere('adobe_connect_webinar_sco.id <> :sessionId')
            ->setParameter('sessionId', $sessionIdToExclude);
        }

        return array_column($qb->getQuery()->getResult(), 'email');
    }
}
