<?php

namespace App\Repository;

use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AdobeConnectWebinarConfigurationHolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdobeConnectWebinarConfigurationHolder::class);
    }
}
