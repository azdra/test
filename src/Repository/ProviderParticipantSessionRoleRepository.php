<?php

namespace App\Repository;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class ProviderParticipantSessionRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderParticipantSessionRole::class);
    }

    protected function findAnimatorsQueryBuilder(ProviderSession $providerSession): QueryBuilder
    {
        return $this->createQueryBuilder('roles')
            ->andWhere('roles.session = :session')
            ->andWhere('roles.role = :animatorRole')
            ->setParameter('animatorRole', ProviderParticipantSessionRole::ROLE_ANIMATOR)
            ->setParameter('session', $providerSession);
    }

    public function findAnimators(ProviderSession $providerSession): array
    {
        return $this->findAnimatorsQueryBuilder($providerSession)->getQuery()->getResult();
    }

    public function countAnimators(ProviderSession $providerSession): int
    {
        $qb = $this->findAnimatorsQueryBuilder($providerSession);

        return $qb->select($qb->expr()->count('roles'))->getQuery()->getSingleScalarResult();
    }

    public function isAnimator(ProviderSession $providerSession, User $user): bool
    {
        return null !== $this->findAnimatorsQueryBuilder($providerSession)
            ->select('1')
            ->join('roles.participant', 'participant')
            ->andWhere('participant.user = :user')
            ->setParameter('user', $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    protected function findByRoleQueryBuilder(ProviderSession $providerSession, string $role, string $search = null): QueryBuilder
    {
        $qb = $this->createQueryBuilder('roles')
            ->join('roles.participant', 'participant')
            ->join('participant.user', 'user')
            ->andWhere('roles.session = :session')
            ->andWhere('roles.role = :traineeRole')
            ->setParameter('traineeRole', $role)
            ->setParameter('session', $providerSession);

        if ($search) {
            $queryConditions[] = $qb->expr()->like(User::ALIAS.'.firstName', ':search');
            $queryConditions[] = $qb->expr()->like(User::ALIAS.'.lastName', ':search');
            $queryConditions[] = $qb->expr()->like(User::ALIAS.'.email', ':search');
            $qb->andWhere($qb->expr()->orX(...$queryConditions));
            $qb->setParameter('search', '%'.$search.'%');
        }

        $qb->orderBy(User::ALIAS.'.lastName', 'ASC');

        return $qb;
    }

    protected function findTraineesQueryBuilder(ProviderSession $providerSession, string $search = null): QueryBuilder
    {
        return $this->findByRoleQueryBuilder($providerSession, ProviderParticipantSessionRole::ROLE_TRAINEE, $search);
    }

    protected function findSearchAnimatorsQueryBuilder(ProviderSession $providerSession, string $search = null): QueryBuilder
    {
        return $this->findByRoleQueryBuilder($providerSession, ProviderParticipantSessionRole::ROLE_ANIMATOR, $search);
    }

    public function findTraineesStrictEmailQueryBuilder(ProviderSession $providerSession, string $search = null): array
    {
        $result = [];

        if ($search) {
            $qb = $this->createQueryBuilder('roles')
                ->join('roles.participant', 'participant')
                ->join('participant.user', 'user')
                ->andWhere('roles.session = :session')
                ->andWhere('roles.role = :traineeRole')
                ->setParameter('traineeRole', ProviderParticipantSessionRole::ROLE_TRAINEE)
                ->setParameter('session', $providerSession);

            $queryConditions[] = $qb->expr()->like(User::ALIAS.'.email', ':search');
            $qb->andWhere($qb->expr()->orX(...$queryConditions))
                ->setParameter('search', $search);

            $result = $qb->getQuery()
                ->getResult();
        }

        return $result;
    }

    public function findTrainees(ProviderSession $providerSession): array
    {
        return $this->findTraineesQueryBuilder($providerSession)->getQuery()->getResult();
    }

    public function countTrainees(ProviderSession $providerSession, string $search = null): int
    {
        $qb = $this->findTraineesQueryBuilder(providerSession: $providerSession, search: $search);

        return $qb->select(
            $qb->expr()->count('roles')
        )->getQuery()->getSingleScalarResult();
    }

    public function findBySessionAndUser(ProviderSession $providerSession, User $user): ProviderParticipantSessionRole
    {
        return $this->createQueryBuilder('role')
            ->join('role.participant', 'participant')
            ->where('role.session = :session')
            ->andWhere('participant.user = :user')
            ->setParameter('session', $providerSession)
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleResult();
    }

    public function findParticipantOnSeveralSession(string $role): array
    {
        $qbFindSessionInSameTime = $this->createQueryBuilder('ppsr2')
            ->select('COUNT(ppsr2.id)')
            ->join('ppsr2.session', 'ps2')
            ->where('ppsr.participant = ppsr2.participant')
            ->andWhere('ppsr.session <> ppsr2.session');
        $qbFindSessionInSameTime->andWhere(
            $qbFindSessionInSameTime->expr()->orX(
                'ps.dateStart BETWEEN ps2.dateStart AND ps2.dateEnd',
                'ps.dateEnd BETWEEN ps2.dateStart AND ps2.dateEnd',
                'ps2.dateStart BETWEEN ps.dateStart AND ps.dateEnd'
            )
        )->andWhere('ppsr.role = \''.$role.'\'');

        $qb = $this->createQueryBuilder('ppsr')
            ->join('ppsr.session', 'ps')
            ->where('ps.dateStart > CURRENT_TIMESTAMP()')
            ->andWhere('('.$qbFindSessionInSameTime.') > 0')
            ->addOrderBy('ppsr.participant', 'ASC')
            ->addOrderBy('ppsr.session', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function isRegisteredOntoSession(int $providerSessionId, int $providerParticipantId): bool
    {
        $qb = $this->createQueryBuilder('pprs')
            ->where('pprs.session = :sessionId')
            ->andWhere('pprs.participant = :participantId')
            ->setParameter('sessionId', $providerSessionId)
            ->setParameter('participantId', $providerParticipantId);

        return $qb->select($qb->expr()->count('pprs'))->getQuery()->getSingleScalarResult();
    }

    public function getDataForUser(User $user): array
    {
        $roles = $this->createQueryBuilder('pprs')
            ->select('pprs.id, IDENTITY(pprs.session) as session_id, pprs.manualPresenceStatus as manual_presence_status, IDENTITY(participant.user) as userId, pprs.providerSessionAccessToken.token as token')
            ->join('pprs.participant', 'participant', Join::WITH, 'participant.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();

        $sessionIds = array_map(function ($role) { return $role['session_id']; }, $roles);

        return array_combine($sessionIds, $roles);
    }

    public function findList(string $search, ProviderSession $providerSession, int $firstResult = 0, int $countResult = User::MAX_RESULT_LIST): array
    {
        $qb = $this->findTraineesQueryBuilder(providerSession: $providerSession, search: $search);
        if ($countResult > 0) {
            $qb->setFirstResult($firstResult)
                ->setMaxResults($countResult);
        }

        return $qb->getQuery()
            ->getResult();
    }

    public function findListAnimators(string $search, ProviderSession $providerSession): array
    {
        $qb = $this->findSearchAnimatorsQueryBuilder(providerSession: $providerSession, search: $search);

        return $qb->getQuery()
                  ->getResult();
    }

    public function countAnimatorsWithSearch(ProviderSession $providerSession, string $search = null): int
    {
        $qb = $this->findSearchAnimatorsQueryBuilder(providerSession: $providerSession, search: $search);

        return $qb->select(
            $qb->expr()->count('roles')
        )->getQuery()->getSingleScalarResult();
    }

    public function getLicenceFromSessionAndEmail(ProviderSession $providerSession, string $email): ProviderParticipantSessionRole
    {
        return $this->createQueryBuilder('role')
            ->join('role.participant', 'participant')
            ->join('participant.user', 'user')
            ->where('role.session = :session')
            ->andWhere('user.email = :email')
            ->setParameter('session', $providerSession)
            ->setParameter('email', $email)
            ->getQuery()
            ->getSingleResult();
    }

    public function getParticipantWithSessionAndEmail(ProviderSession $providerSession, string $email): ProviderParticipantSessionRole|null
    {
        return $this->createQueryBuilder('role')
                    ->join('role.participant', 'participant')
                    ->join('participant.user', 'user')
                    ->where('role.session = :session')
                    ->andWhere('user.email = :email')
                    ->setParameter('session', $providerSession)
                    ->setParameter('email', $email)
                    ->getQuery()
                    ->getOneOrNullResult();
    }

    public function getParticipantWithSessionAndEmailAndAnimator(ProviderSession $providerSession, string $email): ProviderParticipantSessionRole|null
    {
        return $this->createQueryBuilder('role')
            ->join('role.participant', 'participant')
            ->join('participant.user', 'user')
            ->where('role.session = :session')
            ->andWhere('user.email = :email')
            ->andWhere('role.role = :role')
            ->setParameter('session', $providerSession)
            ->setParameter('email', $email)
            ->setParameter('role', ProviderParticipantSessionRole::ROLE_ANIMATOR)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function countParticipantsBySession(ProviderSession $session): array
    {
        $qb = $this->createQueryBuilder('ppsr')
            ->select('COUNT(DISTINCT ppsr) as totalParticipants')
            ->join('ppsr.participant', 'participant')
            ->join('participant.user', 'user')
            ->where('ppsr.session = :session')
            ->setParameter('session', $session);

        $qbPhone = clone $qb;
        $participantsWhoCanReceiveSms = $qbPhone
            ->select('COUNT(DISTINCT ppsr) as participantsWhoCanReceiveSms')
            ->andWhere('user.phone IS NOT NULL')
            ->andWhere(
                $qbPhone->expr()->orX(
                    $qbPhone->expr()->like('user.phone', ':phone_pattern_1'),
                    $qbPhone->expr()->like('user.phone', ':phone_pattern_2'),
                    $qbPhone->expr()->like('user.phone', ':phone_pattern_3')
                )
            )
            ->setParameter('phone_pattern_1', '06%')
            ->setParameter('phone_pattern_2', '07%')
            ->setParameter('phone_pattern_3', '+33%')
            ->getQuery()
            ->getSingleScalarResult();

        $totalParticipants = $qb->getQuery()->getSingleScalarResult();

        return [
            'participantsWhoCanReceiveSms' => $participantsWhoCanReceiveSms,
            'totalParticipants' => $totalParticipants,
        ];
    }
}
