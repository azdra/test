<?php

namespace App\Repository;

use App\Entity\Adobe\AdobeConnectWebinarTelephonyProfile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AdobeConnectWebinarTelephonyProfileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdobeConnectWebinarTelephonyProfile::class);
    }
}
