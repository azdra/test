<?php

namespace App\Repository;

use App\Entity\ActivityLog;
use App\Entity\Client\Client;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ActivityLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ActivityLog::class);
    }

    public function searchResultImportForSendEmail(string $flagOrigin, array $actions): array
    {
        $query = $this->createQueryBuilder('al')
            ->where('al.reportsEmailSent = :reportsEmailSent')
            ->andWhere("JSON_EXTRACT(al.infos, '$.flagOrigin') = :flagOrigin")
            ->andWhere('al.action IN (:actions)')
            ->setParameter('reportsEmailSent', false)
            ->setParameter('flagOrigin', $flagOrigin)
            ->setParameter('actions', $actions)
            ->orderBy('al.origin, al.createdAt', 'ASC')
            ->getQuery();

        return $query->getResult();
    }

    public function findAllLogsBefore(string $dateMin = null, string $dateMax = null, array $infoFilters = []): array
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.createdAt > :minusDate')
            ->setParameter('minusDate', $dateMin)
            ->andWhere('a.createdAt <> :plusDate')
            ->setParameter('plusDate', $dateMax);

        foreach ($infoFilters as $mainKey => $filter) {
            $filterName = 'filter_'.str_replace('.', '_', $mainKey);
            $qb->andWhere("JSON_EXTRACT(a.infos, '$.$mainKey') = :$filterName")
                ->setParameter($filterName, $filter);
        }

        return $qb->orderBy('a.createdAt', 'DESC')
            ->getQuery()
            //->setMaxResults(100)
            ->getResult();
    }

    public function findAllBefore(Client $client, ?string $date = null, array $infoFilters = []): array
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.client = :client')
            ->setParameter('client', $client);

        if (null !== $date && $date !== 'undefined') {
            $qb->andWhere('a.createdAt < :beforeDate')
                ->setParameter('beforeDate', $date);
        }

        foreach ($infoFilters as $mainKey => $filter) {
            $filterName = 'filter_'.str_replace('.', '_', $mainKey);
            $qb->andWhere("JSON_EXTRACT(a.infos, '$.$mainKey') = :$filterName")
                ->setParameter($filterName, $filter);
        }

        $results = $qb->orderBy('a.createdAt', 'DESC')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult();

        $count = $qb->select('COUNT(a.id)')
            ->getQuery()
            ->getSingleScalarResult();

        return [
            'count' => $count,
            'results' => $results,
        ];
    }

    public function findAllBeforeForSessionOnly(Client $client, string $dateStartLimit, ?string $date = null, array $infoFilters = []): array
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.client = :client')
            ->setParameter('client', $client);

        if (null !== $date) {
            $qb->andWhere('a.createdAt < :beforeDate')
                ->setParameter('beforeDate', $date);
        }

        $qb->andWhere('a.createdAt > :dateLimitStartLog')
            ->setParameter('dateLimitStartLog', $dateStartLimit);

        foreach ($infoFilters as $mainKey => $filter) {
            $filterName = 'filter_'.str_replace('.', '_', $mainKey);
            $qb->andWhere("JSON_EXTRACT(a.infos, '$.$mainKey') = :$filterName")
                ->setParameter($filterName, $filter);
        }
        $results = $qb->orderBy('a.createdAt', 'DESC')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult();

        $count = $qb->select('COUNT(a.id)')
            ->getQuery()
            ->getSingleScalarResult();

        return [
            'count' => $count,
            'results' => $results,
        ];
    }

    public function findAlertNotification(Client $client, ?string $date = null): array
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.client = :client')
            ->setParameter('client', $client);

        if (null !== $date) {
            $qb->andWhere('a.createdAt < :beforeDate')
                ->setParameter('beforeDate', $date);
        }

        $qb->andWhere("a.severity = '".ActivityLog::SEVERITY_WARNING."' OR a.severity = '".ActivityLog::SEVERITY_ERROR."'");

        return $qb->orderBy('a.id', 'DESC')
            ->getQuery()
            ->setMaxResults(100)
            ->getResult();
    }

    public function findAllActivityLogForUser(User $user): array
    {
        $qb = $this->createQueryBuilder('al');

        $qb->where(
            $qb->expr()->orX('al.user = :user'),
            $qb->expr()->orX('al.infos LIKE :user_like'),
        );

        $qb->setParameters([
            'user' => $user,
            'user_like' => "%{$user}%",
        ]);

        return $qb->orderBy('al.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findAllOlderCanBeCleanedUp(\DateInterval $interval): array
    {
        $dateTarget = (new \DateTime())
            ->sub($interval);

        return $this->createQueryBuilder('al')
            ->where('al.createdAt <= :now')
            ->andWhere('al.action IN (:actions)')
            ->setParameters([
                'now' => $dateTarget->format('c'),
                'actions' => ActivityLog::ALLOWED_ACTION_TO_BE_CLEANED,
            ])
            ->orderBy('al.createdAt')
            ->getQuery()
            ->getResult();
    }

    public function findAllUserLogToClean(?string $date = null): array
    {
        $query = $this->createQueryBuilder('al')
            ->andWhere('al.createdAt < :beforeDate')
            ->andWhere('al.infos NOT LIKE :session')
            ->setParameter('session', '%"session":%')
            ->setParameter('beforeDate', $date)
            ->setMaxResults(1000)
            ->getQuery();

        return $query->getResult();
    }

    public function findAllLogWithSessionToClean(?string $dateEnd = null, ?string $dateStart = null): array
    {
        $query = $this->createQueryBuilder('al')
            ->andWhere('al.createdAt < :beforeDate')
            ->andWhere('al.createdAt > :startDate')
            ->andWhere('al.infos LIKE :session')
            ->setParameter('session', '%"session":%')
            ->setParameter('beforeDate', $dateEnd)
            ->setParameter('startDate', $dateStart)
            ->getQuery();

        return $query->getResult();
    }
}
