<?php

namespace App\Repository;

use App\Entity\Bounce;
use App\Entity\Client\Client;
use App\Entity\ProviderSession;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

class BounceRepository extends ServiceEntityRepository
{
    protected Security $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, Bounce::class);

        $this->security = $security;
    }

    public function findAll(): mixed
    {
        return $this->createQueryBuilder(Bounce::ALIAS)
            ->orderBy(Bounce::ALIAS.'.id', 'DESC')
            ->getQuery()->getResult();
    }

    public function findAllBounces(): mixed
    {
        if ($this->security->isGranted(User::ROLE_HOTLINE_CUSTOMER_SERVICE)) {
            return $this->findAll();
        }

        /** @var User $user */
        $user = $this->security->getUser();

        return $this->createQueryBuilder(Bounce::ALIAS)
            ->join(Bounce::ALIAS.'.user', User::ALIAS)
            ->andWhere(User::ALIAS.'.client = :client')
            ->setParameter('client', $user->getClient())
            ->orderBy(Bounce::ALIAS.'.id', 'DESC')
            ->getQuery()->getResult();
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getNumberOfBounceToCorrect(): int
    {
        /** @var User $user */
        $user = $this->security->getUser();

        $query = $this->createQueryBuilder(Bounce::ALIAS)
            ->where(Bounce::ALIAS.'.state = :state_to_correct')
            ->setParameter('state_to_correct', Bounce::TO_CORRECT);

        if (!$this->security->isGranted(User::ROLE_ADMIN)) {
            $query->join(Bounce::ALIAS.'.user', User::ALIAS)
                ->andWhere(User::ALIAS.'.client = :client')
                ->setParameter('client', $user->getClient());
        }

        return $query->select($query->expr()->count(Bounce::ALIAS))
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function globalSearch(?string $search): array
    {
        $qb = $this->createQueryBuilder(Bounce::ALIAS)
        ->join(Bounce::ALIAS.'.session', ProviderSession::ALIAS);

        if (!$this->security->isGranted(User::ROLE_HOTLINE_CUSTOMER_SERVICE)) {
            $qb = $this->getAddActiveClientCondition($qb);
        }

        if ($search) {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like(Bounce::ALIAS.'.email', ':query'),
                    $qb->expr()->like(ProviderSession::ALIAS.'.ref', ':query')
                )
            );
            $qb->setParameter('query', "%$search%");
        }

        return $qb->orderBy(Bounce::ALIAS.'.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    protected function getAddActiveClientCondition(QueryBuilder $qb): QueryBuilder
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $client = $user->getClient();

        $qb->join('bounce.user', User::ALIAS)
            ->where(User::ALIAS.'.client = :client')
            ->setParameter('client', $client);

        return $qb;
    }

    public function markAsProcessedForUser(User $user): void
    {
        $this->createQueryBuilder('b')
             ->update(Bounce::class, 'b')
             ->set('b.state', Bounce::CORRECTED)
             ->where('b.user = :user')
             ->setParameter('user', $user->getId())
             ->getQuery()
             ->execute();
    }

    public function getMailInErrorForSessionInPeriod(Client $client, \DateTimeImmutable $start, \DateTimeImmutable $end): int
    {
        $qb = $this->createQueryBuilder('bounce')
            ->join('bounce.session', 'provider_session')
            ->join('provider_session.abstractProviderConfigurationHolder', 'abstract_provider_configuration_holder')
            ->where('abstract_provider_configuration_holder.client = :client')
            ->andWhere('bounce.state = 1')
            ->setParameter('client', $client)
        ;

        $qb->andWhere(
            $qb->expr()->orX(
                'provider_session.dateStart BETWEEN :start AND :end',
                'provider_session.dateEnd BETWEEN :start AND :end',
                'provider_session.dateStart < :start AND provider_session.dateEnd > :end'
            )
        )
            ->setParameter('start', $start)
            ->setParameter('end', $end)
        ;

        $qb->select('count(bounce.id)');

        return $qb->getQuery()->getSingleScalarResult();
    }
}
