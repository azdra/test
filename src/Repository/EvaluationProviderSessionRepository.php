<?php

namespace App\Repository;

use App\Entity\EvaluationProviderSession;
use App\Entity\ProviderSession;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\Persistence\ManagerRegistry;

class EvaluationProviderSessionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EvaluationProviderSession::class);
    }

    public function findEvaluationsToSend(): array
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.sent = false')
            ->andWhere('e.sendAt < :now')
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getResult()
        ;
    }

    public function getEvaluationProviderSessionForSession(ProviderSession $session): array
    {
        $qb = $this->createQueryBuilder('eps')
            ->select('eps', 'evaluation', 'evaluationSessionAnswers', 'providerParticipantSessionRole', 'participant', 'user')
            ->innerJoin('eps.evaluation', 'evaluation')
            ->innerJoin('eps.evaluationSessionAnswers', 'evaluationSessionAnswers')
            ->innerJoin('evaluationSessionAnswers.providerParticipantSessionRole', 'providerParticipantSessionRole')
            ->innerJoin('providerParticipantSessionRole.participant', 'participant')
            ->innerJoin('participant.user', 'user')
            ->where('eps.providerSession = :session')
            ->setParameter('session', $session);

        return $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    public function countAnswersByEvaluationProviderSession(ProviderSession $session): int
    {
        $qb = $this->createQueryBuilder('eps')
            ->select('count(evaluationSessionAnswers.id)')
            ->innerJoin('eps.evaluationSessionAnswers', 'evaluationSessionAnswers')
            ->where('eps.providerSession = :session')
            ->setParameter('session', $session);

        return $qb->getQuery()->getSingleScalarResult();
    }
}
