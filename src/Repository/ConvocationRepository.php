<?php

namespace App\Repository;

use App\Entity\Client\Client;
use App\Entity\Convocation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ConvocationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Convocation::class);
    }

    /**
     * @throws \Exception
     */
    public function isConvocationExistForClient(?int $clientId, ?string $convocationName): bool
    {
        $qb = $this->createQueryBuilder('c')
                ->leftjoin('c.client', 'client')
                ->select('COUNT(c.id) as count_convocation');

        if ($clientId) {
            $qb->andWhere('client.id = :clientId')
                ->setParameter('clientId', $clientId);
        }

        if ($convocationName) {
            $qb->andWhere('c.name = :convocationName')
                ->setParameter('convocationName', $convocationName);
        }

        $result = $qb->getQuery()->getOneOrNullResult();

        if ($result['count_convocation'] > 0) {
            return true;
        }

        return false;
    }

    /**
     * @throws \Exception
     */
    public function findConvocation(Client $client, ?string $convocationName, ?string $configurationHolderType = null): ?Convocation
    {
        $query = $this->createQueryBuilder('conv')
            ->where('conv.name = :name')
            ->andWhere('conv.client = :client')
            ->setParameters([
                'name' => $convocationName,
                'client' => $client,
            ]);

        if (!is_null($configurationHolderType)) {
            $query->andWhere('conv.connectorType = :configurationHolderType')
                  ->setParameter('configurationHolderType', $configurationHolderType);
        }

        return $query->getQuery()
                      ->getOneOrNullResult();
    }
}
