<?php

namespace App\Repository;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProviderConfigurationHolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AbstractProviderConfigurationHolder::class);
    }

    public function getAdobeConfigurationHolderForClient(Client $client): mixed
    {
        return $this->createQueryBuilder('pch')
            ->where('pch INSTANCE OF '.AdobeConnectConfigurationHolder::class)
            ->andWhere('pch.client = :client')
            ->setParameter('client', $client)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getWebexConfigurationHolders(): mixed
    {
        return $this->createQueryBuilder('pch')
            ->where('pch INSTANCE OF '.WebexConfigurationHolder::class)
            ->getQuery()
            ->getResult();
    }

    public function getWebexRestConfigurationHolders(): mixed
    {
        return $this->createQueryBuilder('pch')
            ->where('pch INSTANCE OF '.WebexRestConfigurationHolder::class)
            ->getQuery()
            ->getResult();
    }
}
