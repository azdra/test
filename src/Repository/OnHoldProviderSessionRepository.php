<?php

namespace App\Repository;

use App\Entity\Client\Client;
use App\Entity\OnHoldProviderSession;
use App\Entity\ProviderSession;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class OnHoldProviderSessionRepository extends ServiceEntityRepository
{
    protected Security $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, OnHoldProviderSession::class);
        $this->security = $security;
    }

    public function searchAll(Client $client, ?string $from = null, ?string $to = null): array
    {
        $qb = $this->createQueryBuilder('ohps');
        $qb->where('ohps.client = :client')
            ->setParameter('client', $client);
        if (!is_null($from) && !is_null($to)) {
            $qb->andWhere(
                $qb->expr()->between('ohps.createdAt', ':from', ':to')
            );
            $from = \DateTime::createFromFormat('Y-m-d\TH:i:s', $from);
            $to = \DateTime::createFromFormat('Y-m-d\TH:i:s', $to);
            $qb->setParameter('from', $from)
                ->setParameter('to', $to);
        }

        return $qb->getQuery()->getResult();
    }

    /*public function globalSearch(
        ?string $query,
        bool $allowOutdated = false,
        ?UserInterface $user = null,
        ?string $from = null,
        ?string $to = null,
        ?int $configurationHolderId = null,
        int $firstResult = 0,
        int $countResult = 0,
                       $filtersSearch = [],
    ): array {
        $client = null;
        $category = null;
        $test = false;
        $status = null;
        $serviceType = null;
        $registrationType = null;
        $registrationModal = null;
        $selectedRegistration = null;
        $operationCompare = null;
        $numberCompare = null;
        $tags = null;
        if (!empty($filtersSearch)) {
            $allowOutdated = !$filtersSearch->futureSessions;
            $configurationHolderId = $filtersSearch->connector;
            $from = $filtersSearch->dateStart;
            $to = $filtersSearch->dateEnd;
            $client = $filtersSearch->client;
            $category = $filtersSearch->categoryType;
            $test = $filtersSearch->sessionTest;
            $status = $filtersSearch->statusSession;
            $serviceType = $filtersSearch->serviceType;
            $registrationType = $filtersSearch->registrationType;
            $selectedRegistration = $filtersSearch->selectedRegistration;
            $registrationModal = $filtersSearch->registrationModal;
            $operationCompare = $filtersSearch->operationCompare;
            $numberCompare = $filtersSearch->NumberCompare;
            $tags = $filtersSearch->tags;
        }
        $qb = $this->getGlobalSearchQB($query, $allowOutdated, $user, $from, $to, $configurationHolderId, $client, $category, $test, $status, $serviceType, $registrationType, $operationCompare, $numberCompare, $tags, $registrationModal, $selectedRegistration);

        if ($countResult > 0) {
            $qb->setFirstResult($firstResult)
                ->setMaxResults($countResult);
        }

        $qb->orderBy(OnHoldProviderSession::ALIAS.'.createdAt', 'ASC');

        return $qb->getQuery()->getResult();
    }*/

    public function getGlobalSearchQB(
        ?string $query,
        bool $allowOutdated = false,
        ?UserInterface $user = null,
        ?string $from = null,
        ?string $to = null,
        ?int $configurationHolderId = null,
        ?int $client = null,
        ?int $category = null,
        ?string $test = null,
        ?string $status = null,
        ?string $serviceType = null,
        ?string $registrationType = null,
        ?string $operationCompare = null,
        ?int $numberCompare = null,
        ?array $tags = null,
        ?string $registrationModal = null,
        ?string $selectedRegistration = null,
    ): QueryBuilder {
        $qb = $this->createQueryBuilder(OnHoldProviderSession::ALIAS);

        $queryConditions = [];
        if (!$this->security->isGranted(User::ROLE_HOTLINE_CUSTOMER_SERVICE)) {
            $qb = $this->getAddActiveClientCondition($qb);
        } else {
            $qb->join(OnHoldProviderSession::ALIAS.'.client', 'client')
                //->andwhere('pch.archived = :archived')
                //->setParameter('archived', false)
            ;
            $queryConditions[] = $qb->expr()->like('client.name', ':query');
        }

        if ($query) {
            $queryConditions[] = $qb->expr()->like(OnHoldProviderSession::ALIAS.'.name', ':query');
            $queryConditions[] = $qb->expr()->like(OnHoldProviderSession::ALIAS.'.ref', ':query');
            $queryConditions[] = $qb->expr()->like(OnHoldProviderSession::ALIAS.'.businessNumber', ':query');
            $qb->andWhere($qb->expr()->orX(...$queryConditions));
            $qb->setParameter('query', '%'.$query.'%');
        }

        if (!$allowOutdated) {
            $now = new DateTime();
            $now->setTime(0, 0);
            $qb->andWhere(ProviderSession::ALIAS.'.dateEnd > :now')
                ->setParameter('now', $now);
        }

        if ($user !== null) {
            $qb->join(ProviderSession::ALIAS.'.participantRoles', 'participantRoles')
                ->join('participantRoles.participant', 'participant')
                ->andWhere('participant.user = :user')
                ->setParameter('user', $user);
        }

        if ($from !== null && $to !== null) {
            $from = (new \DateTime($from));
            $to = (new \DateTime($to));
            $qb->andWhere(OnHoldProviderSession::ALIAS.'.dateStart <= :to')
                ->andWhere('provider_session.dateStart >= :from')
                ->setParameter('to', $to)
                ->setParameter('from', $from);
        }

        if ($configurationHolderId) {
            $qb->andWhere(OnHoldProviderSession::ALIAS.'.abstractProviderConfigurationHolder = :configurationHolder')
                ->setParameter('configurationHolder', $configurationHolderId);
        }

        if ($client !== null) {
            $qb->andWhere(OnHoldProviderSession::ALIAS.'.client = :client')
                ->setParameter('client', $client);
        }

        if ($category !== null) {
            $qb->andWhere(OnHoldProviderSession::ALIAS.'.category = :category')
                ->setParameter('category', $category);
        }

        if ($test) {
            $qb->andWhere(OnHoldProviderSession::ALIAS.'.sessionTest = '.$test);
        }

        if ($tags !== null) {
            foreach ($tags as $tag) {
                $qb->andWhere($qb->expr()->orX('JSON_CONTAINS('.OnHoldProviderSession::ALIAS.'.tags, :tag) = 1'))
                    ->setParameter('tag', '"'.$tag.'"');
            }
        }

        if ($status !== null) {
            switch ($status) {
                case ProviderSession::FINAL_STATUS_DRAFT:
                    $qb->andWhere(OnHoldProviderSession::ALIAS.'.status = :status')
                        ->setParameter('status', ProviderSession::STATUS_DRAFT);
                    break;
                case ProviderSession::FINAL_STATUS_SCHEDULED:
                    $qb->andWhere(OnHoldProviderSession::ALIAS.'.status = :status')
                        ->andWhere(OnHoldProviderSession::ALIAS.'.dateStart > :dateNew')
                        ->andWhere(OnHoldProviderSession::ALIAS.'.lastConvocationSent is null')
                        ->setParameter('status', ProviderSession::STATUS_SCHEDULED)
                        ->setParameter('dateNew', new \DateTime());
                    break;
                case ProviderSession::FINAL_STATUS_SENT:
                    $qb->andWhere(OnHoldProviderSession::ALIAS.'.status = :status')
                        ->andWhere(OnHoldProviderSession::ALIAS.'.dateStart > :dateNew')
                        ->andWhere(OnHoldProviderSession::ALIAS.'.lastConvocationSent is not null')
                        ->setParameter('status', ProviderSession::STATUS_SCHEDULED)
                        ->setParameter('dateNew', new \DateTime());
                    break;
                case ProviderSession::FINAL_STATUS_RUNNING:
                    $qb->andWhere(OnHoldProviderSession::ALIAS.'.status = :status')
                        ->andWhere(OnHoldProviderSession::ALIAS.'.dateStart <= :dateNewStart')
                        ->andWhere(OnHoldProviderSession::ALIAS.'.dateEnd >= :dateNewEnd')
                        ->setParameter('status', ProviderSession::STATUS_SCHEDULED)
                        ->setParameter('dateNewStart', new \DateTime())
                        ->setParameter('dateNewEnd', new \DateTime());
                    break;
                case ProviderSession::FINAL_STATUS_FINISHED:
                    $qb->andWhere(OnHoldProviderSession::ALIAS.'.status = :status')
                        ->andWhere(OnHoldProviderSession::ALIAS.'.dateEnd < :dateNewEnd')
                        ->setParameter('status', ProviderSession::STATUS_SCHEDULED)
                        ->setParameter('dateNewEnd', new \DateTime());
                    break;
                case ProviderSession::FINAL_STATUS_CANCELED:
                    $qb->andWhere(OnHoldProviderSession::ALIAS.'.status = :status')
                        ->setParameter('status', ProviderSession::STATUS_CANCELED);
                    break;
            }
        }

        if ($serviceType !== null) {
            switch ($serviceType) {
                case 'assistance':
                    $qb->andWhere(OnHoldProviderSession::ALIAS.'.assistanceType > :enum_no')
                        ->setParameter('enum_no', ProviderSession::ENUM_NO);
                    break;
                case 'support':
                    $qb->andWhere(OnHoldProviderSession::ALIAS.'.supportType > :enum_support_no')
                        ->setParameter('enum_support_no', ProviderSession::ENUM_SUPPORT_NO);
                    break;
            }
        }

        if ($registrationType !== null && $operationCompare !== null && $numberCompare !== null) {
            switch ($registrationType) {
                case 'participantNumber':
                    $qb->join(ProviderSession::ALIAS.'.participantRoles', 'participantRoles')
                        ->join('participantRoles.participant', 'participant')
                        ->groupBy('participantRoles.session')
                        ->having('COUNT(participantRoles.id) '.$operationCompare.' :numberCompare')
                        ->andWhere('participantRoles.role = \'trainee\'')
                        ->setParameter('numberCompare', $numberCompare);
                    break;
                case 'animatorNumber':
                    $qb->join(ProviderSession::ALIAS.'.participantRoles', 'participantRoles')
                        ->join('participantRoles.participant', 'participant')
                        ->groupBy('participantRoles.session')
                        ->having('COUNT(participantRoles.id) '.$operationCompare.' :numberCompare')
                        ->andWhere('participantRoles.role = \'animator\'')
                        ->setParameter('numberCompare', $numberCompare);
                    break;
            }
        }

        if ($registrationModal !== null && $selectedRegistration !== null) {
            switch ($registrationModal) {
                case 'page':
                    $qb->andWhere('provider_session.registrationPageTemplate = :registrationPageTemplate')
                        ->setParameter('registrationPageTemplate', $selectedRegistration);
                    break;
                case 'catalog':
                    $qb->join('provider_session.subject', 'subject')
                        ->join('subject.theme', 'theme')
                        ->join('theme.module', 'module')
                        ->andWhere('module = :selectedModule')
                        ->setParameter('selectedModule', $selectedRegistration);
                    break;
            }
        }

        return $qb;
    }

    public function getAddActiveClientCondition(QueryBuilder $qb): QueryBuilder
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $client = $user->getClient();

        $qb->where(OnHoldProviderSession::ALIAS.'.client = :client')
            //->andwhere('pch.archived = :archived')
            //->setParameter('archived', false)
            ->setParameter('client', $client);

        return $qb;
    }
}
