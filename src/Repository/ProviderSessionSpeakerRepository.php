<?php

namespace App\Repository;

use App\Entity\ProviderSessionSpeaker;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

class ProviderSessionSpeakerRepository extends ServiceEntityRepository
{
    protected Security $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, ProviderSessionSpeaker::class);

        $this->security = $security;
    }
}
