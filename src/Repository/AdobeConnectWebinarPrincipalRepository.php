<?php

namespace App\Repository;

use App\Entity\Adobe\AdobeConnectWebinarPrincipal;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AdobeConnectWebinarPrincipalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdobeConnectWebinarPrincipal::class);
    }

    public function getPrincipalWithConfigurationHolder(string $principalIdentifier, AdobeConnectWebinarConfigurationHolder $adobeConnectWebinarCH): mixed
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.principalIdentifier = :principalIdentifier')
            ->andWhere('a.configurationHolder = :configurationHolder')
            //->join('provider_participant.abstractProviderConfigurationHolder', 'abstract_provider_configuration_holder')
            //->andWhere('abstract_provider_configuration_holder.id = :configurationHolder')
            ->setParameter('principalIdentifier', $principalIdentifier)
            ->setParameter('configurationHolder', $adobeConnectWebinarCH)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
