<?php

namespace App\Repository;

use App\Entity\Adobe\AdobeConnectWebinarMeetingAttendee;
use App\Entity\Adobe\AdobeConnectWebinarPrincipal;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

class AdobeConnectWebinarMeetingAttendeeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdobeConnectWebinarMeetingAttendee::class);
    }

    public function alreadyExists(AdobeConnectWebinarSCO $adobeConnectWebinarSCO, AdobeConnectWebinarPrincipal $adobeConnectWebinarPrincipal, \DateTimeImmutable $sessionStart, \DateTimeImmutable $sessionEnd): bool
    {
        $qb = $this->createQueryBuilder('acma');
        $qb->select('1')
            ->join('acma.adobeConnectWebinarSCO', 'sco', Join::WITH, 'sco = :sco')
            ->setParameter('sco', $adobeConnectWebinarSCO)
            ->andWhere('acma.adobeConnectWebinarPrincipal = :principal')
            ->andWhere('acma.sessionStart = :start')
            ->andWhere('acma.sessionEnd = :end')
            ->setParameter('principal', $adobeConnectWebinarPrincipal)
            ->setParameter('start', $sessionStart)
            ->setParameter('end', $sessionEnd);

        return !empty($qb->getQuery()->getResult());
    }
}
