<?php

namespace App\Repository;

use App\Entity\VideoReplay;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class VideoReplayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VideoReplay::class);
    }

    public function findReplayForPeriod(\DateTime $dateStart, \DateTime $dateEnd): array
    {
        $qb = $this->createQueryBuilder('vr');
        $qb->where('vr.publishedDate BETWEEN :dateStart AND :dateEnd')
            ->setParameter('dateStart', $dateStart)
            ->setParameter('dateEnd', $dateEnd)
            ->orderBy('vr.publishedDate', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function findVideosFordModule(int $moduleId): array
    {
        $qb = $this->createQueryBuilder('vr')
            ->innerJoin('vr.module', 'm')
            ->where('m.id = :moduleId')
            ->setParameter('moduleId', $moduleId);

        return $qb->getQuery()->getResult();
    }
}
