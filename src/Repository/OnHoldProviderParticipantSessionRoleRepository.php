<?php

namespace App\Repository;

use App\Entity\OnHoldProviderParticipantSessionRole;
use App\Entity\OnHoldProviderSession;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class OnHoldProviderParticipantSessionRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OnHoldProviderParticipantSessionRole::class);
    }

    protected function findAnimatorsQueryBuilder(OnHoldProviderSession $onHoldProviderSession): QueryBuilder
    {
        return $this->createQueryBuilder('roles')
            ->andWhere('roles.session = :session')
            ->andWhere('roles.role = :animatorRole')
            ->setParameter('animatorRole', OnHoldProviderParticipantSessionRole::ROLE_ANIMATOR)
            ->setParameter('session', $onHoldProviderSession);
    }

    public function findAnimators(OnHoldProviderSession $onHoldProviderSession): array
    {
        return $this->findAnimatorsQueryBuilder($onHoldProviderSession)->getQuery()->getResult();
    }

    public function countAnimators(OnHoldProviderSession $onHoldProviderSession): int
    {
        $qb = $this->findAnimatorsQueryBuilder($onHoldProviderSession);

        return $qb->select($qb->expr()->count('roles'))->getQuery()->getSingleScalarResult();
    }

    public function isAnimator(OnHoldProviderSession $onHoldProviderSession, User $user): bool
    {
        return null !== $this->findAnimatorsQueryBuilder($onHoldProviderSession)
            ->select('1')
            ->join('roles.participant', 'participant')
            ->andWhere('participant.user = :user')
            ->setParameter('user', $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    protected function findByRoleQueryBuilder(OnHoldProviderSession $onHoldProviderSession, string $role, string $search = null): QueryBuilder
    {
        $qb = $this->createQueryBuilder('ohppsr')
            ->andWhere('ohppsr.session = :session')
            ->andWhere('ohppsr.role = :traineeRole')
            ->setParameter('traineeRole', $role)
            ->setParameter('session', $onHoldProviderSession);

        $qb->orderBy('ohppsr.name', 'ASC');

        return $qb;
    }

    protected function findTraineesQueryBuilder(OnHoldProviderSession $onHoldProviderSession, string $search = null): QueryBuilder
    {
        return $this->findByRoleQueryBuilder($onHoldProviderSession, OnHoldProviderParticipantSessionRole::ROLE_TRAINEE, $search);
    }

    protected function findSearchAnimatorsQueryBuilder(OnHoldProviderSession $onHoldProviderSession, string $search = null): QueryBuilder
    {
        return $this->findByRoleQueryBuilder($onHoldProviderSession, OnHoldProviderParticipantSessionRole::ROLE_ANIMATOR, $search);
    }

    public function findList(string $search, OnHoldProviderSession $onHoldProviderSession, int $firstResult = 0, int $countResult = User::MAX_RESULT_LIST): array
    {
        $qb = $this->findTraineesQueryBuilder(onHoldProviderSession: $onHoldProviderSession, search: $search);
        if ($countResult > 0) {
            $qb->setFirstResult($firstResult)
                ->setMaxResults($countResult);
        }

        return $qb->getQuery()
            ->getResult();
    }

    public function findListAnimators(string $search, OnHoldProviderSession $onHoldProviderSession): array
    {
        $qb = $this->findSearchAnimatorsQueryBuilder(onHoldProviderSession: $onHoldProviderSession, search: $search);

        return $qb->getQuery()
                  ->getResult();
    }
}
