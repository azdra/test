<?php

namespace App\Repository;

use App\Entity\Client\Client;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface, UserLoaderInterface
{
    protected Security $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, User::class);

        $this->security = $security;
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
    }

    public function globalSearch(?string $search, bool $isAdmin = false): array
    {
        $qb = $this->createQueryBuilder(User::ALIAS);

        if (!$this->security->isGranted(User::ROLE_ADMIN)) {
            $qb = $this->getAddActiveClientCondition($qb);
        }

        if ($search) {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like(User::ALIAS.'.firstName', ':query'),
                    $qb->expr()->like(User::ALIAS.'.lastName', ':query'),
                )
            );
            $qb->setParameter('query', "%$search%");
        }

        if ($isAdmin) {
            $qb->andWhere(
                $qb->expr()->orX(
                    'JSON_CONTAINS('.User::ALIAS.'.roles, :role1) = 1',
                    'JSON_CONTAINS('.User::ALIAS.'.roles, :role2) = 1',
                )
            )
            ->setParameter('role1', '"ROLE_MANAGER"')
            ->setParameter('role2', '"ROLE_ADMIN"');
        }

        return $qb->getQuery()->getResult();
    }

    public function globalSearchByRoleTarget(?string $search, string $role, ?Client $client = null): array
    {
        $qb = $this->createQueryBuilder(User::ALIAS);

        if (!$this->security->isGranted(User::ROLE_ADMIN)) {
            $qb = $this->getAddActiveClientCondition($qb);
        }

        if (!empty($client)) {
            $qb->andWhere(User::ALIAS.'.client = :client')
                ->setParameter('client', $client);
        }

        if ($search) {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like(User::ALIAS.'.firstName', ':query'),
                    $qb->expr()->like(User::ALIAS.'.lastName', ':query'),
                    $qb->expr()->like(User::ALIAS.'.email', ':query')
                )
            )
                ->setParameter('query', "%$search%");
        }

        $qb->andWhere('JSON_CONTAINS('.User::ALIAS.'.roles, :role) = 1')
            ->setParameter('role', "\"$role\"");

        return $qb->getQuery()->getResult();
    }

    public function findListByRolesAdministrators(): array
    {
        $qb = $this->createQueryBuilder(User::ALIAS);

        $qb->andWhere(
            $qb->expr()->orX(
            'JSON_CONTAINS('.User::ALIAS.'.roles, :role1) = 1',
            'JSON_CONTAINS('.User::ALIAS.'.roles, :role2) = 1',
            'JSON_CONTAINS('.User::ALIAS.'.roles, :role3) = 1',
            )
        )
        ->setParameter('role1', '"ROLE_MANAGER"')
        ->setParameter('role2', '"ROLE_ADMIN"')
        ->setParameter('role3', '"ROLE_ADMIN_MANAGER"');

        return $qb->getQuery()->getResult();
    }

    public function getListQueryBuilder(string $search): QueryBuilder
    {
        $qb = $this->createQueryBuilder(User::ALIAS);

        if (!$this->security->isGranted(User::ROLE_HOTLINE_CUSTOMER_SERVICE)) {
            $qb = $this->getAddActiveClientCondition($qb);
        }

        if (strlen($search) > 0) {
            $qb->join(User::ALIAS.'.client', 'client')
                ->andWhere(
                'LOWER(CONCAT('.
                    User::ALIAS.".firstName, ' ',  ".
                    User::ALIAS.".lastName, ' ', ".
                    User::ALIAS.'.firstName,'.
                    User::ALIAS.'.email,'.
                    'client.name'.
                ')) LIKE :search'
            )
            ->setParameter('search', "%{$search}%", );
        }

        return $qb;
    }

    public function findList(string $search, int $firstResult = 0, int $countResult = User::MAX_RESULT_LIST): array
    {
        $qb = $this->getListQueryBuilder($search);

        if ($countResult > 0) {
            $qb->setFirstResult($firstResult)
                ->setMaxResults($countResult);
        }

        return $qb->getQuery()
            ->getResult();
    }

    public function findAllUserClient(Client $client, ?string $search = ''): array
    {
        $qb = $this->createQueryBuilder(User::ALIAS);

        $qb->where(User::ALIAS.'.client = :client')
            ->setParameter('client', $client);

        if (strlen($search) > 0) {
            $qb->join(User::ALIAS.'.client', 'client')
                ->andWhere(
                    'LOWER(CONCAT('.
                    User::ALIAS.".firstName, ' ',  ".
                    User::ALIAS.".lastName, ' ', ".
                    User::ALIAS.'.firstName,'.
                    User::ALIAS.'.email,'.
                    'client.name'.
                    ')) LIKE :search'
                )
                ->setParameter('search', "%{$search}%", );
        }

        return $qb->getQuery()
                  ->getResult();
    }

    public function countListResult(string $search): int
    {
        return $this->getListQueryBuilder($search)
            ->select('COUNT('.User::ALIAS.'.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findListByRoleTarget(string $role): array
    {
        $role = mb_strtoupper($role);

        $qb = $this->createQueryBuilder(User::ALIAS);

        if (!$this->security->isGranted(User::ROLE_ADMIN)) {
            $qb = $this->getAddActiveClientCondition($qb);
        }

        return $qb->andWhere('JSON_CONTAINS('.User::ALIAS.'.roles, :role) = 1')
            ->setParameter('role', "\"$role\"")
            ->getQuery()
            ->getResult();
    }

    public function findManagerListByClient(Client $client): array
    {
        return $this->createQueryBuilder(User::ALIAS)
            ->andWhere('JSON_CONTAINS('.User::ALIAS.'.roles, :role) = 1')
            ->andWhere('user.client = :client')
            ->setParameters([
                'client' => $client,
                'role' => '"'.User::ROLE_MANAGER.'"',
            ])
            ->getQuery()
            ->getResult();
    }

    public function findAllUserAccounts(User $user): mixed
    {
        return $this->createQueryBuilder(User::ALIAS)
            ->where(User::ALIAS.'.status IN (:status)')
            ->setParameter('status', [User::STATUS_ACTIVE, User::STATUS_INACTIVE])
            ->andWhere(User::ALIAS.'.email = :email')
            ->setParameter('email', $user->getEmail())
            ->getQuery()
            ->getResult();
    }

    public function findAllUsersManagerAccounts(User $user): mixed
    {
        $qb = $this->createQueryBuilder(User::ALIAS)
            ->where(User::ALIAS.'.status IN (:status)')
            ->setParameter('status', [User::STATUS_ACTIVE, User::STATUS_INACTIVE])
            ->andWhere(User::ALIAS.'.email = :email')
            ->setParameter('email', $user->getEmail());

        $qb->andWhere(
            $qb->expr()->orX(
                'JSON_CONTAINS('.User::ALIAS.'.roles, :role1) = 1',
                'JSON_CONTAINS('.User::ALIAS.'.roles, :role2) = 1',
            )
        )
        ->setParameter('role1', '"'.User::ROLE_ADMIN.'"')
        ->setParameter('role2', '"'.User::ROLE_MANAGER.'"');

        return $qb->getQuery()->getResult();
    }

    public function findAllActiveManagingAccounts(): array|float|int|string
    {
        $qb = $this->createQueryBuilder('user');
        $qb->where($qb->expr()->eq('user.status', ':status'))
            ->setParameter('status', User::STATUS_ACTIVE);

        $qb->andWhere(
            $qb->expr()->orX(
                'JSON_CONTAINS(user.roles, :adminRole) = 1',
                'JSON_CONTAINS(user.roles, :managerRole) = 1',
            )
        )
            ->setParameter('adminRole', '"'.User::ROLE_ADMIN.'"')
            ->setParameter('managerRole', '"'.User::ROLE_MANAGER.'"');

        return $qb->getQuery()->getResult();
    }

    public function findAllProviderServiceAccounts(array $userIds = null, array $activeUser = [User::STATUS_ACTIVE, User::STATUS_INACTIVE]): mixed
    {
        $qb = $this->createQueryBuilder(User::ALIAS)
            ->where(User::ALIAS.'.status IN (:status)')
            ->setParameter('status', $activeUser);

        $qb->andWhere(
                $qb->expr()->orX(
                    'JSON_CONTAINS('.User::ALIAS.'.roles, :role1) = 1',
                    'JSON_CONTAINS('.User::ALIAS.'.roles, :role2) = 1',
                    'JSON_CONTAINS('.User::ALIAS.'.roles, :role3) = 1',
                )
            )->setParameter('role1', '"'.User::ROLE_ADMIN.'"')
            ->setParameter('role2', '"'.User::ROLE_HOTLINE_CUSTOMER_SERVICE.'"')
            ->setParameter('role3', '"'.User::ROLE_CUSTOMER_SERVICE.'"');
        if ($userIds != null) {
            $qb->andWhere(
                User::ALIAS.'.id IN (:id)'
            )
            ->setParameter('id', $userIds);
        }

        return $qb->getQuery()->getResult();
    }

    public function findAllProviderServiceAccountsWithoutAdmins(array $userIds = null, array $activeUser = [User::STATUS_ACTIVE, User::STATUS_INACTIVE]): mixed
    {
        $qb = $this->createQueryBuilder(User::ALIAS)
            ->where(User::ALIAS.'.status IN (:status)')
            ->setParameter('status', $activeUser);

        $qb->andWhere(
            $qb->expr()->orX(
                'JSON_CONTAINS('.User::ALIAS.'.roles, :role2) = 1',
                'JSON_CONTAINS('.User::ALIAS.'.roles, :role3) = 1',
            )
        )->setParameter('role2', '"'.User::ROLE_HOTLINE_CUSTOMER_SERVICE.'"')
            ->setParameter('role3', '"'.User::ROLE_CUSTOMER_SERVICE.'"');
        if ($userIds != null) {
            $qb->andWhere(
                User::ALIAS.'.id IN (:id)'
            )
                ->setParameter('id', $userIds);
        }

        return $qb->getQuery()->getResult();
    }

    protected function getAddActiveClientCondition(QueryBuilder $qb): QueryBuilder
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $client = $user->getClient();

        $qb->where(User::ALIAS.'.client = :client')
            ->setParameter('client', $client);

        return $qb;
    }

    public function getAllClientUserMinimalData(Client $client): array
    {
        return $this->createQueryBuilder(User::ALIAS, User::ALIAS.'.id')
            ->select(User::ALIAS.'.firstName')
            ->addSelect(User::ALIAS.'.lastName')
            ->addSelect(User::ALIAS.'.email')
            ->addSelect(User::ALIAS.'.id')
            ->addSelect('LOWER(CONCAT('.
                User::ALIAS.".lastName, ' ', ".
                User::ALIAS.".firstName, ' ', ".
                User::ALIAS.'.email'.
                ')) as searchableField'
            )
            ->where(User::ALIAS.'.client = :client')
            ->setParameter('client', $client)
            ->getQuery()
            ->getArrayResult();
    }

    public function findByIdIndexById(array $ids): array
    {
        return $this->createQueryBuilder('user', 'user.id')
            ->where('user.id in (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }

    public function loadUserByIdentifier(string $identifier): ?User
    {
        return $this->getEntityManager()->createQuery(
            'SELECT u
             FROM App\Entity\User u
             WHERE u.id = :userId'
        )
        ->setParameter('userId', $identifier)
        ->getOneOrNullResult();
    }

    public function getActiveUserByEmail($email): ?array
    {
        return $this->createQueryBuilder('u')
            ->where('u.email = :email')
            ->andWhere('u.status = :status')
            ->setParameters([
                'email' => $email,
                'status' => User::STATUS_ACTIVE,
            ])
            ->getQuery()
            ->getResult();
    }

    public function findAllUsersAdmins(): mixed
    {
        $qb = $this->createQueryBuilder(User::ALIAS)
            ->where(User::ALIAS.'.status IN (:status)')
            ->setParameter('status', [User::STATUS_ACTIVE, User::STATUS_INACTIVE]);

        $qb->andWhere(
            $qb->expr()->orX(
                'JSON_CONTAINS('.User::ALIAS.'.roles, :role1) = 1'
            )
        )
            ->setParameter('role1', '"'.User::ROLE_ADMIN.'"');

        return $qb->getQuery()->getResult();
    }
}
