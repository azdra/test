<?php

namespace App\Repository;

use App\Entity\Evaluation;
use App\Entity\EvaluationParticipantAnswer;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class EvaluationParticipantAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EvaluationParticipantAnswer::class);
    }

    public function getSessionAnswerForEvaluation(Evaluation $evaluation, ProviderSession $session): array
    {
        $qb = $this->createQueryBuilder('epa')
            ->select('epa.answer', 'epa.replyDate', 'user.firstName', 'user.lastName', 'user.email')
            ->innerJoin('epa.providerParticipantSessionRole', 'ppsr')
            ->innerJoin('ppsr.participant', 'participant')
            ->innerJoin('participant.user', 'user')
            ->where('epa.evaluation = :evaluation')
            ->andWhere('ppsr.session = :session')
            ->setParameters([
                'evaluation' => $evaluation,
                'session' => $session,
            ]);

        return $qb->getQuery()->getArrayResult();
    }

    public function getSessionEvaluationAnswerForAssistant(Evaluation $evaluation, ProviderSession $session, array $surveryIds): array
    {
        $qb = $this->createQueryBuilder('epa')
            ->select('epa.answer', 'evaluation.surveyId')
            ->innerJoin('epa.providerParticipantSessionRole', 'ppsr')
            ->innerJoin('ppsr.participant', 'participant')
            ->innerJoin('epa.evaluation', 'evaluation');
        $qb->where($qb->expr()->eq('epa.evaluation', ':evaluation'));
        $qb->andWhere($qb->expr()->eq('ppsr.session', ':session'));
        $qb->andWhere($qb->expr()->eq('ppsr.role', ':ppsr_role'));
        $qb->andWhere($qb->expr()->in('evaluation.surveyId', ':surveyId'));
        $qb->setParameters([
            'evaluation' => $evaluation,
            'session' => $session,
            'surveyId' => $surveryIds,
            'ppsr_role' => ProviderParticipantSessionRole::ROLE_ANIMATOR,
        ]);

        return $qb->getQuery()->getArrayResult();
    }

    public function getCountSessionEvaluation(ProviderSession $session): int
    {
        $qb = $this->createQueryBuilder('epa')
            ->select('COUNT(epa)')
            ->innerJoin('epa.providerParticipantSessionRole', 'ppsr');
        $qb->where('ppsr.session = :session')
            ->setParameter('session', $session);
        if ($session->getEvaluations()->first()) {
            $qb->andWhere('epa.evaluation = :evaluation')
                ->setParameter('evaluation', $session->getEvaluations()->first());
        }

        return $qb->getQuery()
            ->getSingleScalarResult();
    }

    public function getSessionEvaluationAnswerLogBook(\DateTimeImmutable $dateStart, \DateTimeImmutable $dateEnd): array
    {
        $qb = $this->createQueryBuilder('epa')
            ->select('epa.answer', 'evaluation.surveyId')
            ->innerJoin('epa.evaluation', 'evaluation');
        $qb->andWhere($qb->expr()->in('evaluation.surveyId', ':surveyId'));
        $qb->setParameters([
            'surveyId' => Evaluation::EVALUATION_LOGBOOK,
        ]);

        return $qb->getQuery()->getArrayResult();
    }
}
