<?php

namespace App\Repository;

use App\Entity\Cisco\WebexMeetingTelephonyProfile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class WebexMeetingTelephonyProfileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WebexMeetingTelephonyProfile::class);
    }
}
