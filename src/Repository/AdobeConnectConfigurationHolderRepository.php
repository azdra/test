<?php

namespace App\Repository;

use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AdobeConnectConfigurationHolderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdobeConnectConfigurationHolder::class);
    }
}
