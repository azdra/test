<?php

namespace App\Repository;

use App\Entity\SmsBounce;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

class SmsBounceRepository extends ServiceEntityRepository
{
    protected Security $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, SmsBounce::class);

        $this->security = $security;
    }

    public function countBouncesBySession($session): int
    {
        return $this->createQueryBuilder('sms_bounce')
            ->select('COUNT(sms_bounce.id)')
            ->andWhere('sms_bounce.session = :session')
            ->setParameter('session', $session)
            ->getQuery()->getSingleScalarResult();
    }
}
