<?php

namespace App\Repository;

use App\Entity\Client\Client;
use App\Entity\GroupParticipant;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

class GroupParticipantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private Security $security)
    {
        parent::__construct($registry, GroupParticipant::class);
    }

    public function findList(string $search, int $firstResult = 0, int $countResult = GroupParticipant::MAX_RESULT_LIST): array
    {
        $qb = $this->getListQueryBuilder($search);

        if ($countResult > 0) {
            $qb->setFirstResult($firstResult)
               ->setMaxResults($countResult);
        }

        return $qb->getQuery()
                  ->getResult();
    }

    private function addSearch(QueryBuilder $qb, string $search): void
    {
        $qb->andWhere('group_participant.name LIKE :search')
           ->setParameter('search', '%'.$search.'%');
    }

    public function countListResult(string $search): int
    {
        $qb = $this->getListQueryBuilder($search);

        return (int) $qb->select('COUNT(group_participant.id)')
                        ->getQuery()
                        ->getSingleScalarResult();
    }

    public function getListQueryBuilder(string $search): QueryBuilder
    {
        $qb = $this->createQueryBuilder('group_participant')
                   ->select('group_participant')
                   ->orderBy('group_participant.name', 'ASC');

        if (!$this->security->isGranted(User::ROLE_HOTLINE_CUSTOMER_SERVICE)) {
            $qb = $this->getAddActiveClientCondition($qb);
        }

        $this->addSearch($qb, $search);

        return $qb;
    }

    protected function getAddActiveClientCondition(QueryBuilder $qb): QueryBuilder
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $client = $user->getClient();

        $qb->where('group_participant.client = :client')
           ->setParameter('client', $client);

        return $qb;
    }

    public function findAllGroupClient(): array
    {
        $qb = $this->createQueryBuilder('group_participant')
                   ->select('group_participant')
                   ->orderBy('group_participant.name', 'ASC');
        $qb = $this->getAddActiveClientCondition($qb);

        return $qb->getQuery()
                  ->getResult();
    }

    public function getAllClientGroup(Client $client): array
    {
        $qb = $this->createQueryBuilder('group_participant')
            ->select('group_participant')
            ->orderBy('group_participant.name', 'ASC')
                   ->where('group_participant.client = :client')
                   ->setParameter('client', $client);

        return $qb->getQuery()
                  ->getResult();
    }
}
