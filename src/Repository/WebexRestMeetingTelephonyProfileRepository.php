<?php

namespace App\Repository;

use App\Entity\Cisco\WebexRestMeetingTelephonyProfile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class WebexRestMeetingTelephonyProfileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WebexRestMeetingTelephonyProfile::class);
    }
}
