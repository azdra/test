<?php

namespace App\Repository;

use App\Command\DeferredActionsTaskCommand;
use App\Entity\DeferredActionsTask;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DeferredActionsTaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeferredActionsTask::class);
    }

    public function getNextTask(): array
    {
        return $this->createQueryBuilder('at')
            ->where('at.status = :statusReady')
            ->orderBy('at.createdAt', 'ASC')
            ->setMaxResults(DeferredActionsTaskCommand::MAX_PROCESSING_TASK)
            ->setParameter('statusReady', DeferredActionsTask::STATUS_READY)
            ->getQuery()
            ->getResult();
    }

    public function getNextTasksNotDoneAndNotFailed(): array
    {
        return $this->createQueryBuilder('at')
            ->where('at.status != :statusDone')
            ->andWhere('at.status != :statusFailed')
            ->setParameter('statusDone', DeferredActionsTask::STATUS_DONE)
            ->setParameter('statusFailed', DeferredActionsTask::STATUS_FAILED)
            ->orderBy('at.createdAt', 'ASC')
            ->setMaxResults(DeferredActionsTaskCommand::MAX_PROCESSING_TASK)
            ->getQuery()
            ->getResult();
    }

    public function getLastTaskByStatus(string $status): array
    {
        return $this->createQueryBuilder('at')
                    ->where('at.status = :statusReady')
            ->andWhere('at.createdAt >= :date')
                    ->orderBy('at.id', 'DESC')
                    ->setParameter('statusReady', $status)
                    ->setParameter('date', (new \DateTime())->modify('- 1 day'))
                    ->getQuery()
            ->getResult();
    }

    public function getNextTasks(): array
    {
        return $this->createQueryBuilder('at')
                    ->where('at.status = :statusReady')
                    ->orderBy('at.id', 'ASC')
                    ->setParameter('statusReady', DeferredActionsTask::STATUS_READY)
                    ->getQuery()
                    ->getResult();
    }
}
