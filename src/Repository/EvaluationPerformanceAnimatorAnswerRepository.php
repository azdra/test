<?php

namespace App\Repository;

use App\CustomerService\Entity\EvaluationPerformanceAnimatorAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class EvaluationPerformanceAnimatorAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EvaluationPerformanceAnimatorAnswer::class);
    }

    public function getAnswerForEvaluationBetweenDates(\DateTimeImmutable $dateStart, \DateTimeImmutable $dateEnd): array
    {
        $qb = $this->createQueryBuilder('e')
            ->where('e.replyDate >= :dateStart')
            ->andWhere('e.replyDate <= :dateEnd')
            ->setParameter('dateStart', $dateStart)
            ->setParameter('dateEnd', $dateEnd)
            ->getQuery();

        return $qb->getResult();
    }
}
