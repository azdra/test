<?php

namespace App\Repository;

use App\Entity\Adobe\AdobeConnectMeetingAttendee;
use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

class AdobeConnectMeetingAttendeeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdobeConnectMeetingAttendee::class);
    }

    public function alreadyExists(AdobeConnectSCO $adobeConnectSCO, AdobeConnectPrincipal $adobeConnectPrincipal, \DateTimeImmutable $sessionStart, \DateTimeImmutable $sessionEnd): bool
    {
        $qb = $this->createQueryBuilder('acma');
        $qb->select('1')
            ->join('acma.adobeConnectSCO', 'sco', Join::WITH, 'sco = :sco')
            ->setParameter('sco', $adobeConnectSCO)
            ->andWhere('acma.adobeConnectPrincipal = :principal')
            ->andWhere('acma.sessionStart = :start')
            ->andWhere('acma.sessionEnd = :end')
            ->setParameter('principal', $adobeConnectPrincipal)
            ->setParameter('start', $sessionStart)
            ->setParameter('end', $sessionEnd);

        return !empty($qb->getQuery()->getResult());
    }
}
