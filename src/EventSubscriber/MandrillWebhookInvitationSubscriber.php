<?php

namespace App\EventSubscriber;

use App\Entity\Guest;
use App\Event\MandrillMessageInvitationsEvent;
use App\Event\MandrillMessageInvitationsEvents;
//use App\Service\BounceService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MandrillWebhookInvitationSubscriber implements EventSubscriberInterface
{
    public function __construct(
        //private Guest BounceService $bounceService,
        private EntityManagerInterface $entityManager
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            MandrillMessageInvitationsEvents::OPENED => 'hydrateInvitation',
            MandrillMessageInvitationsEvents::CLICKED => 'hydrateInvitation',
        ];
    }

    public function hydrateInvitation(MandrillMessageInvitationsEvent $event): void
    {
        $message = $event->getMessage();
        if (!array_key_exists('metadata', $message) || !array_key_exists('guest_id', $message['metadata'])) {
            return;
        }

        $guest = $this->entityManager->getRepository(Guest::class)
            ->find($message['metadata']['guest_id']);
        if (!empty($guest)) {
            if (count($message['opens']) > 0) {
                //$message['opens'][0]['ts'];
                $guest->setOpened($guest->getOpened() + count($message['opens']));
            }
            if (count($message['clicks']) > 0) {
                //$message['clicks'][0]['ts'];
                $guest->setClicked($guest->getClicked() + count($message['clicks']));
            }
        }
        $this->entityManager->persist($guest);
        $this->entityManager->flush();
    }
}
