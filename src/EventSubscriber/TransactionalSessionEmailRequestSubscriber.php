<?php

namespace App\EventSubscriber;

use App\Entity\ActivityLog;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Entity\WebinarConvocation;
use App\Event\TransactionalEmailRequestEventInterface;
use App\Event\TransactionalParticipantEmailRequestEvent;
use App\Event\TransactionalSessionEmailRequestEvent;
use App\Event\TransactionnalNotifyParticipantsSessionIsUpdateEmailRequestEvent;
use App\Exception\InvalidArgumentException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Service\ActivityLogger;
use App\Service\ConvocationService;
use App\Service\MailerService;
use App\Service\ReferrerService;
use DateTime;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;

class TransactionalSessionEmailRequestSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private ConvocationService $convocationService,
        private MailerService $mailerService,
        private LoggerInterface $logger,
        private TranslatorInterface $translator,
        private ReferrerService $referrerService,
        private ActivityLogger $activityLogger,
        private RouterInterface $router,
        private string $endPointDomain
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            TransactionalEmailRequestEventInterface::CONVOCATION_ALL_PARTICIPANTS => 'onConvocationRequestForAllParticipants',
            TransactionalEmailRequestEventInterface::COMMUNICATION_ALL_PARTICIPANTS => 'onCommunicationRequestForAllParticipants',
            TransactionalEmailRequestEventInterface::CONVOCATION_ONE_PARTICIPANT => 'onConvocationRequestToParticipant',
            TransactionalEmailRequestEventInterface::COMMUNICATION_ONE_PARTICIPANT => 'onCommunicationRequestToParticipant',
            TransactionalEmailRequestEventInterface::REMINDER_ALL_PARTICIPANTS => 'onReminderRequestForAllParticipants',
            TransactionalEmailRequestEventInterface::REMINDER_ONE_PARTICIPANT => 'onReminderRequestToParticipant',
            TransactionalEmailRequestEventInterface::CANCEL => 'onCancelRequest',
            TransactionalEmailRequestEventInterface::UNCANCEL => 'onUncancelRequest',
            TransactionalEmailRequestEventInterface::UNSUBSCRIBE => 'onUnsubscribeRequest',
            TransactionalEmailRequestEventInterface::NOTIFY_PARTICIPANTS_SESSION_IS_UPDATED => 'onSessionUpdatedNotifyParticipants',
            TransactionalEmailRequestEventInterface::ROLE_CHANGE => 'onParticipantRoleChange',
        ];
    }

    public function onConvocationRequestForAllParticipants(TransactionalSessionEmailRequestEvent $event): void
    {
        $providerSession = $event->getProviderSession();
        if (!$providerSession->getClient()->isEnableSendEmail()) {
            return;
        }
        try {
            if (self::sessionNeedToSendConvocation($providerSession, $event->isForce())) {
                /** @var ProviderParticipantSessionRole $participantRole */
                foreach ($providerSession->getParticipantRoles() as $participantRole) {
                    $this->sendConvocationToParticipant($participantRole, $event->getOrigin(), $event->isForce());
                }

                $providerSession->setLastConvocationSent(new \DateTimeImmutable());
            }
        } catch (Throwable $exception) {
            $this->logger->error($exception, [
                'sessionId' => $providerSession->getId(),
                'sessionRed' => $providerSession->getRef(),
                'isForce' => $event->isForce(),
                'author' => $event->getOrigin()?->getId(),
            ]);
        }
    }

    public function onCommunicationRequestForAllParticipants(TransactionalSessionEmailRequestEvent $event): void
    {
        $providerSession = $event->getProviderSession();
        if (!$providerSession->getClient()->isEnableSendEmail()) {
            return;
        }
        try {
            if (self::sessionNeedToSendCommunication($providerSession, $event->isForce())) {
                foreach ($providerSession->getWebinarConvocation() as $communication) {
                    if (new DateTime() >= $communication->getConvocationDate() && $communication->getConvocationType() !== WebinarConvocation::TYPE_INVITATION) {
                        /** @var ProviderParticipantSessionRole $participantRole */
                        foreach ($providerSession->getParticipantRoles() as $participantRole) {
                            self::updateStatReminderSentForNewParticipant($communication, $participantRole);
                            if (self::participantNeedToSendCommunication($communication, $participantRole)) {
                                $this->sendCommunicationToParticipant(participantRole: $participantRole, communication: $communication, origin: $event->getOrigin(), isForce: $event->isForce());
                            }
                        }
                        $communication->setSent(true);
                    }
                }
                //$providerSession->setLastConvocationSent(new \DateTimeImmutable());
            }
        } catch (Throwable $exception) {
            $this->logger->error($exception, [
                'sessionId' => $providerSession->getId(),
                'sessionRed' => $providerSession->getRef(),
                'isForce' => $event->isForce(),
                'author' => $event->getOrigin()?->getId(),
            ]);
        }
    }

    public function onConvocationRequestToParticipant(TransactionalParticipantEmailRequestEvent $event): void
    {
        $participantRole = $event->getParticipantSessionRole();
        if (!$participantRole->getSession()->getClient()->isEnableSendEmail()) {
            return;
        }
        try {
            $this->sendConvocationToParticipant($participantRole, $event->getOrigin(), $event->isForce());
        } catch (Throwable $exception) {
            $participant = $participantRole->getParticipant();

            $this->logger->error($exception, [
                'participantRoleId' => $participantRole->getId(),
                'participantId' => $participant->getId(),
                'participantMail' => $participant->getEmail(),
                'isForce' => $event->isForce(),
                'author' => $event->getOrigin()?->getId(),
            ]);
        }
    }

    public function onCommunicationRequestToParticipant(TransactionalParticipantEmailRequestEvent $event): void
    {
        $participantRole = $event->getParticipantSessionRole();
        if (!$participantRole->getSession()->getClient()->isEnableSendEmail()) {
            return;
        }
        try {
            foreach ($participantRole->getSession()->getWebinarConvocation() as $communication) {
                if ($communication->getConvocationType() == WebinarConvocation::TYPE_CONFIRMATION) {
                    $this->sendCommunicationToParticipant(participantRole: $participantRole, communication: $communication, origin: $event->getOrigin(), isForce: $event->isForce());
                }
            }
        } catch (Throwable $exception) {
            $participant = $participantRole->getParticipant();

            $this->logger->error($exception, [
                'participantRoleId' => $participantRole->getId(),
                'participantId' => $participant->getId(),
                'participantMail' => $participant->getEmail(),
                'isForce' => $event->isForce(),
                'author' => $event->getOrigin()?->getId(),
            ]);
        }
    }

    public function onReminderRequestForAllParticipants(TransactionalSessionEmailRequestEvent $event): void
    {
        $providerSession = $event->getProviderSession();
        if (!$providerSession->getClient()->isEnableSendEmail()) {
            return;
        }
        try {
            if (self::sessionNeedToSendReminders($providerSession, $event->isForce())) {
                /** @var ProviderParticipantSessionRole $participantRole */
                foreach ($providerSession->getParticipantRoles() as $participantRole) {
                    $this->sendReminderToParticipant($participantRole, $event->getOrigin(), $event->isForce());
                }

                $providerSession->setLastReminderSent(new \DateTimeImmutable());
            }
        } catch (Throwable $exception) {
            $this->logger->error($exception, [
                'sessionId' => $providerSession->getId(),
                'sessionRef' => $providerSession->getRef(),
                'isForce' => $event->isForce(),
                'author' => $event->getOrigin()?->getId(),
            ]);
        }
    }

    public function onReminderRequestToParticipant(TransactionalParticipantEmailRequestEvent $event): void
    {
        $participantRole = $event->getParticipantSessionRole();
        if (!$participantRole->getSession()->getClient()->isEnableSendEmail()) {
            return;
        }
        try {
            $this->sendReminderToParticipant($event->getParticipantSessionRole(), $event->getOrigin(), $event->isForce());
        } catch (Throwable $exception) {
            $participant = $participantRole->getParticipant();

            $this->logger->error($exception, [
                'participantRoleId' => $participantRole->getId(),
                'participantId' => $participant->getId(),
                'participantEmail' => $participant->getEmail(),
                'isForce' => $event->isForce(),
                'author' => $event->getOrigin()?->getId(),
            ]);
        }
    }

    public function onCancelRequest(TransactionalSessionEmailRequestEvent $event): void
    {
        $providerSession = $event->getProviderSession();
        if (!$providerSession->getClient()->isEnableSendEmail()) {
            return;
        }
        if (!$providerSession->getClient()->getEmailOptions()->isSendACancellationEmailToTrainees()) {
            return;
        }

        /** @var ProviderParticipantSessionRole $participantRole */
        foreach ($providerSession->getParticipantRoles() as $participantRole) {
            $this->sendCancelToParticipant($participantRole, $event->getOrigin(), $event->isForce());
        }
    }

    public function onUncancelRequest(TransactionalSessionEmailRequestEvent $event): void
    {
        // TODO Set the correct rules
        $providerSession = $event->getProviderSession();
        if (!$providerSession->getClient()->isEnableSendEmail()) {
            return;
        }
        try {
            /** @var ProviderParticipantSessionRole $participantRole */
            foreach ($providerSession->getParticipantRoles() as $participantRole) {
                $sended = $this->sendConvocationToParticipant($participantRole, $event->getOrigin());

                if ($sended) {
                    $participant = $participantRole->getParticipant();

                    $this->logger->info("[TransactionalSessionEmailRequestSubscriber] Uncancel mail sent to #{$participant->getId()} $participant {$participant->getEmail()}", [
                        'sessionId' => $participantRole->getSession()->getId(),
                        'author' => $event->getOrigin()?->getId(),
                    ]);
                }
            }
        } catch (Throwable $exception) {
            $this->logger->error($exception, [
                'sessionId' => $providerSession->getId(),
                'sessionRef' => $providerSession->getRef(),
                'isForce' => $event->isForce(),
                'author' => $event->getOrigin()?->getId(),
            ]);
        }
    }

    public function onUnsubscribeRequest(TransactionalParticipantEmailRequestEvent $event): void
    {
        $participantRole = $event->getParticipantSessionRole();
        if (!$participantRole->getSession()->getClient()->isEnableSendEmail()) {
            return;
        }
        if (!$participantRole->getSession()->getAbstractProviderConfigurationHolder()->getClient()->getEmailOptions()->isSendAnUnsubscribeEmailToParticipants()) {
            return;
        }

        $this->sendCancelToParticipant($participantRole, $event->getOrigin(), $event->isForce());
    }

    public function onParticipantRoleChange(TransactionalParticipantEmailRequestEvent $event): void
    {
        $participantRole = $event->getParticipantSessionRole();
        if (!$participantRole->getSession()->getClient()->isEnableSendEmail()) {
            return;
        }
        $participantRole->setLastConvocationSent(null);

        $this->onConvocationRequestToParticipant($event);
    }

    public function onSessionUpdatedNotifyParticipants(TransactionnalNotifyParticipantsSessionIsUpdateEmailRequestEvent $event): void
    {
        $session = $event->getProviderSession();
        $changes = $event->getChanges();
        if (!$session->getClient()->isEnableSendEmail()) {
            return;
        }

        /** @var ProviderParticipantSessionRole $participantRole */
        foreach ($session->getParticipantRoles() as $participantRole) {
            $this->activityLogger->initActivityLogContext($session->getClient(), null, ActivityLog::ORIGIN_USER_INTERFACE);

            $participant = $participantRole->getParticipant();
            $this->convocationService->sendUpdateSessionMail($participantRole, $session, $changes);

            $infos = [
                'message' => ['key' => 'Send change in invite to attendee <a href="%userRoute%">%userName%</a> for session <a href="%sessionRoute%">%sessionName%</a>',
                    'params' => [
                        '%userRoute%' => $this->endPointDomain.$this->router->generate('_participant_get', ['id' => $participant->getId()]),
                        '%userName%' => $participant->getUser()->getFirstName().' '.$participant->getUser()->getLastName(),
                        '%sessionRoute%' => $this->endPointDomain.$this->router->generate('_session_get', ['id' => $session->getId()]),
                        '%sessionName%' => $session->getName(),
                    ], ],
                'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
            ];
            $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SESSION_CREATION, ActivityLog::SEVERITY_INFORMATION, $infos), true);
            $this->activityLogger->flushActivityLogs();
        }
    }

    /**
     * @throws InvalidArgumentException
     * @throws LoaderError
     * @throws SyntaxError
     */
    private function sendConvocationToParticipant(ProviderParticipantSessionRole $participantRole, ?User $origin, bool $isForce = false): bool
    {
        $aSendingOccurred = false;
        if (self::participantWantToReceiveConvocation($participantRole, $isForce)) {
            $aSendingOccurred = true;

            $ccs = $this->referrerService->getReferrerFromUser($participantRole);
            $this->convocationService->sendConvocation($participantRole, origin: $origin, ccs: $ccs);

            $participant = $participantRole->getParticipant();
            $this->logger->info(
                "[TransactionalSessionEmailRequestSubscriber] Convocation sent to #{$participant->getId()} $participant {$participant->getEmail()}",
                [
                    'sessionId' => $participantRole->getSession()->getId(),
                    'isForce' => $isForce,
                    'author' => $origin?->getId(),
                ]
            );
        }

        return $aSendingOccurred;
    }

    /**
     * @throws InvalidArgumentException
     * @throws LoaderError
     * @throws SyntaxError
     */
    private function sendCommunicationToParticipant(ProviderParticipantSessionRole $participantRole, WebinarConvocation $communication, ?User $origin, bool $isForce = false): void
    {
        $this->convocationService->sendCommunication(participantRole: $participantRole, communication: $communication, origin: $origin);

        $participant = $participantRole->getParticipant();
        $this->logger->info(
            "[TransactionalSessionEmailRequestSubscriber] Communication sent to #{$participant->getId()} $participant {$participant->getEmail()}",
            [
                'sessionId' => $participantRole->getSession()->getId(),
                'isForce' => $isForce,
                'author' => $origin?->getId(),
            ]
        );
    }

    /**
     * @throws InvalidArgumentException
     * @throws LoaderError
     * @throws SyntaxError
     */
    private function sendReminderToParticipant(ProviderParticipantSessionRole $participantRole, ?User $origin, bool $isForce = false): bool
    {
        $providerSession = $participantRole->getSession();
        $aSendingOccurred = false;
        if ($providerSession->getClient()->isEnableSendEmail()) {
            if (self::participantWantToReceiveReminder($participantRole, $isForce)) {
                $aSendingOccurred = true;
                $this->convocationService->sendConvocation($participantRole, true, origin: $origin);

                $participant = $participantRole->getParticipant();
                $this->logger->info("[TransactionalSessionEmailRequestSubscriber] Reminder sent to #{$participant->getId()} $participant {$participant->getEmail()}", [
                    'sessionId' => $providerSession->getId(),
                    'isForce' => $isForce,
                    'author' => $origin?->getId(),
                ]);
            }
        }

        return $aSendingOccurred;
    }

    private function sendCancelToParticipant(ProviderParticipantSessionRole $participantRole, ?User $origin, bool $isForce = false): void
    {
        $session = $participantRole->getSession();
        if (!$session->getClient()->isEnableSendEmail()) {
            return;
        }
        if ($session->isScheduled() && $session->isConvocationSent()) {
            try {
                $participant = $participantRole->getParticipant();
                $lang = $session->getLanguage();
                $address = new Address(
                            $participant->getEmail(),
                            "{$participant->getFirstName()} {$participant->getLastName()}"
                        );

                $this->mailerService->sendTemplatedMail(
                    $address,
                    $this->translator->trans('Important : You are no longer registered for the "%meeting_name%" session',
                        ['%meeting_name%' => $session->getName()], null, $lang),
                    'emails/participant/session_cancel.html.twig',
                    context: [
                        'session' => $session,
                        'lang' => $lang,
                        'participant' => $participant,
                        'client' => $session->getClient(),
                        'clientTimeZone' => $session->getClient()->getTimezone(),
                    ],
                    metadatas: [
                        'origin' => MailerService::ORIGIN_EMAIL_SEND_SESSION_CANCELED,
                        'user' => $participant->getUser()->getId(),
                        'client' => $participant->getClient()->getId(),
                    ],
                    ccs: $this->referrerService->getReferrerFromUser($participantRole)
                );

                $transKey = match ($participantRole->getOldRole()) {
                    ProviderParticipantSessionRole::ROLE_TRAINEE => 'Sending the participant unregistration %userName% in the session <a href="%sessionroute%">%session_name%</a>',
                    ProviderParticipantSessionRole::ROLE_ANIMATOR => 'Sending the animator unregistration %userName% in the session <a href="%sessionroute%">%session_name%</a>'
                };
                $infos = [
                    'message' => ['key' => $transKey, 'params' => ['%routeSession%' => $this->router->generate('_session_get', ['id' => $session->getId()]), '%session_name%' => $session->getName(), '%userName%' => $participantRole->getFullName()]],
                    'user' => ['id' => $participantRole->getParticipant()->getUserId(), 'name' => $participantRole->getFullName()],
                    'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                ];
                $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_PARTICIPANT_UPDATE_ROLE, ActivityLog::SEVERITY_INFORMATION, $infos));
                $this->logger->info("[TransactionalSessionEmailRequestSubscriber] Cancel mail sent to #{$participant->getId()} $participant {$participant->getEmail()}",
                    [
                        'sessionId' => $participantRole->getSession()->getId(),
                        'author' => $origin?->getId(),
                    ]);
            } catch (Throwable $exception) {
                $this->logger->error($exception,
                    [
                        'participantRoleId' => $participantRole->getId(),
                        'participantId' => $participant->getId(),
                        'participantEmail' => $participant->getEmail(),
                        'isForce' => $isForce,
                        'author' => $origin?->getId(),
                    ]);
            }
        }
    }

    public static function sessionNeedToSendConvocation(ProviderSession $session, bool $isForce = false): bool
    {
        if ($isForce) {
            return true;
        }

        if ($session->isScheduled() || $session->isRunning()) {
            if (!is_null($session->getNotificationDate()) && new DateTime() >= $session->getNotificationDate()) {
                return true;
            }
        }

        return false;
    }

    public function sessionNeedToSendCommunication(ProviderSession $session, bool $isForce = false): bool
    {
        if ($isForce || $session->isScheduled() || $session->isRunning() || $this->containsAnUnsentThankCommunication($session)) {
            return true;
        }

        return false;
    }

    public function containsAnUnsentThankCommunication(ProviderSession $session): bool
    {
        foreach ($session->getWebinarConvocation() as $communication) {
            if ($communication->getConvocationType() == WebinarConvocation::TYPE_THANK_YOU) {
                if (!$communication->isSent()) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function participantNeedToSendCommunication(WebinarConvocation $communication, ProviderParticipantSessionRole $participantRole, bool $isForce = false): bool
    {
        if ($isForce) {
            return true;
        }

        switch ($communication->getConvocationType()) {
            case WebinarConvocation::TYPE_CONFIRMATION:
                if (is_null($participantRole->getLastCommunicationSent()) || ($participantRole->getLastCommunicationSent() < $communication->getConvocationDate())) {
                    return true;
                }
                break;
            case WebinarConvocation::TYPE_REMINDER:
                if (!is_null($participantRole->getLastCommunicationSent())) {
                    if (is_null($participantRole->getLastReminderCommunicationSent()) || ($participantRole->getLastReminderCommunicationSent() < $communication->getConvocationDate())) {
                        return true;
                    }
                } else {
                    $participantRole->setLastReminderCommunicationSent(new \DateTimeImmutable());
                }
                break;
            case WebinarConvocation::TYPE_THANK_YOU:
                if (!$communication->isSent()) {
                    return match ($communication->getSendCommunicationTo()) {
                        WebinarConvocation::SEND_TO_PRESENCES => $participantRole->isPresent(),
                        WebinarConvocation::SEND_TO_ABSENCES => !$participantRole->isPresent(),
                        default => true,
                    };
                }
                break;
        }

        return false;
    }

    public static function updateStatReminderSentForNewParticipant(WebinarConvocation $communication, ProviderParticipantSessionRole $participantRole): void
    {
        $isNewParticipant = false;
        foreach ($participantRole->getSession()->getWebinarConvocation() as $communicationItem) {
            if ($communicationItem->getConvocationType() == WebinarConvocation::TYPE_REMINDER && $communication->getConvocationType() == WebinarConvocation::TYPE_CONFIRMATION && $participantRole->getLastCommunicationSent() == null) {
                $isNewParticipant = true;
            }
        }

        if ($isNewParticipant) {
            $participantRole->setLastReminderCommunicationSent(new \DateTimeImmutable());
        }
    }

    public static function participantWantToReceiveConvocation(ProviderParticipantSessionRole $participantRole, bool $isForce = false): bool
    {
        if ($isForce) {
            return true;
        }

        $session = $participantRole->getSession();

        if (self::sessionNeedToSendConvocation($session, $isForce)) {
            return is_null($participantRole->getLastConvocationSent());
        }

        return false;
    }

    public static function sessionNeedToSendReminders(ProviderSession $session, bool $isForce = false): bool
    {
        if ($isForce) {
            return true;
        }

        if ($session->isScheduled() || $session->isRunning()) {
            if (!is_null($session->getLastConvocationSent())) {
                $nextReminder = $session->getNextReminder();
                if (!is_null($nextReminder) && $nextReminder <= new DateTime()
                     && (
                         is_null($session->getLastReminderSent()) || $nextReminder >= $session->getLastReminderSent()
                     )) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function participantWantToReceiveReminder(ProviderParticipantSessionRole $participantRole, bool $isForce = false): bool
    {
        if ($isForce) {
            return true;
        }

        $session = $participantRole->getSession();
        if (self::sessionNeedToSendReminders($session, $isForce)) {
            $nextReminder = $session->getNextReminder();

            return !is_null($participantRole->getLastConvocationSent()) && (is_null($participantRole->getLastReminderSent()) || $nextReminder >= $participantRole->getLastReminderSent());
        }

        return false;
    }
}
