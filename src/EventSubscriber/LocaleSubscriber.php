<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LocaleSubscriber implements EventSubscriberInterface
{
    private string $defaultLocale;
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger, string $defaultLocale = 'fr')
    {
        $this->defaultLocale = $defaultLocale;
        $this->logger = $logger;
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        dd($event->getRequest()->getClientIp()); // Debugging (remove it after use)
        $this->logger->error('Request event', ['client_ip' => $event->getRequest()->getClientIp(), 'url' => $event->getRequest()->getUri()]);
        $request = $event->getRequest();
        if (!$request->hasPreviousSession()) {
            return;
        }

        if ($locale = $request->attributes->get('_locale')) {
            $request->getSession()->set('_locale', $locale);
        } else {
            $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 20]],
        ];
    }
}
