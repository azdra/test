<?php

namespace App\EventSubscriber;

use App\Event\BrevoMessageEvent;
use App\Event\BrevoMessageEvents;
use App\Service\BounceService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BrevoWebHookSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private BounceService $bounceService,
        private EntityManagerInterface $entityManager
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            BrevoMessageEvents::SEND => 'hydrateBounce',
            BrevoMessageEvents::SOFT_BOUNCE => 'hydrateBounce',
            BrevoMessageEvents::HARD_BOUNCE => 'hydrateBounce',
            BrevoMessageEvents::OPENED => 'hydrateBounce',
            BrevoMessageEvents::CLICKED => 'hydrateBounce',

            /*BrevoMessageEvents::SENT => 'hydrateBounce',
            BrevoMessageEvents::DELIVERED => 'hydrateBounce',
            BrevoMessageEvents::OPENED => 'hydrateBounce',
            BrevoMessageEvents::CLICKED => 'hydrateBounce',
            BrevoMessageEvents::SOFT_BOUNCE => 'hydrateBounce',
            BrevoMessageEvents::HARD_BOUNCE => 'hydrateBounce',
            BrevoMessageEvents::INVALID_EMAIL => 'hydrateBounce',
            BrevoMessageEvents::DIFERRED => 'hydrateBounce',
            BrevoMessageEvents::COMPLAINT => 'hydrateBounce',
            BrevoMessageEvents::UNSUBSCRIBED => 'hydrateBounce',
            BrevoMessageEvents::BLOCKED => 'hydrateBounce',
            BrevoMessageEvents::ERROR => 'hydrateBounce',*/
        ];
    }

    public function hydrateBounce(BrevoMessageEvent $event): void
    {
        $bounce = $this->bounceService->createBounce(
            $event->getName(),
            $event->getMessage()
        );

        $this->entityManager->persist($bounce);
        $this->entityManager->flush($bounce);
    }
}
