<?php

namespace App\EventSubscriber;

use App\Event\BrevoSmsMessageEvent;
use App\Event\BrevoSmsMessageEvents;
use App\Service\SmsBounceService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BrevoSmsWebHookSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private SmsBounceService $bounceService,
        private EntityManagerInterface $entityManager
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            BrevoSmsMessageEvents::SOFT_BOUNCE => 'hydrateBounce',
            BrevoSmsMessageEvents::HARD_BOUNCE => 'hydrateBounce',
        ];
    }

    public function hydrateBounce(BrevoSmsMessageEvent $event): void
    {
        $bounce = $this->bounceService->createBounce($event);

        $this->entityManager->persist($bounce);
        $this->entityManager->flush($bounce);
    }
}
