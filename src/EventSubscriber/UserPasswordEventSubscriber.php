<?php

namespace App\EventSubscriber;

use App\Entity\User;
use App\Event\UserPasswordEvent;
use App\Exception\InvalidArgumentException;
use App\Service\MailerService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserPasswordEventSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly MailerService $mailerService,
        private readonly AuthorizationCheckerInterface $authorizationChecker,
        private readonly UrlGeneratorInterface $urlGenerator
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            UserPasswordEvent::RESET => 'onResetPasswordRequest',
            UserPasswordEvent::RESET_FROM_SELF_SUBSCRIPTION => 'onResetPasswordRequestFromSelfSubscription',
            UserPasswordEvent::NEW => 'onPasswordRequest',
            UserPasswordEvent::CLONE => 'onAssociatingExistingUserToClient',
        ];
    }

    public function onResetPasswordRequest(UserPasswordEvent $event): void
    {
        $user = $event->getUser();

        if ($event->getDomain() === null) {
            $backUrl = $this->urlGenerator->generate('_password_reset_action', ['token' => $user->getPasswordResetToken()], UrlGeneratorInterface::ABSOLUTE_URL);
        } else {
            $backUrl = $this->urlGenerator->generate('_self_subscription_hub_reset_password', ['clientName' => $event->getSlugClient(), 'domain' => $event->getDomain(), 'reset-token' => $user->getPasswordResetToken()], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        $this->mailerService->sendTemplatedMail(
            $user->getEmail(),
            $this->translator->trans('Reset of your password', locale: $user->getPreferredLang()),
            'emails/manager_client/password_reset.html.twig',
            [
                'user' => $user,
                'client' => $user->getClient(),
                'backUrl' => $backUrl,
            ],
            metadatas: [
                'origin' => MailerService::ORIGIN_EMAIL_SEND_RESET_PASSWORD_REQUEST,
                'user' => $user->getId(),
            ],
            senderSystem: true
        );
    }

    public function onResetPasswordRequestFromSelfSubscription(UserPasswordEvent $event): void
    {
        $user = $event->getUser();

        $this->mailerService->sendTemplatedMail(
            $user->getEmail(),
            $this->translator->trans('Reset of your password', locale: $user->getPreferredLang()),
            'emails/manager_client/password_reset.html.twig',
            [
                'user' => $user,
                'client' => $user->getClient(),
                'backUrl' => $this->urlGenerator->generate('_password_reset_action', ['token' => $user->getPasswordResetToken()], UrlGeneratorInterface::ABSOLUTE_URL),
            ],
            metadatas: [
                'origin' => MailerService::ORIGIN_EMAIL_SEND_RESET_PASSWORD_REQUEST,
                'user' => $user->getId(),
            ],
            senderSystem: true
        );
    }

    public function onPasswordRequest(UserPasswordEvent $event): void
    {
        $user = $event->getUser();
        if ($user->isAnAdmin()) {
            $templateMail = 'emails/admin/password_new.html.twig';
        } elseif ($this->authorizationChecker->isGranted(User::ROLE_CUSTOMER_SERVICE)) {
            $templateMail = 'emails/costumer_service/password_new.html.twig';
        } else {
            $templateMail = 'emails/manager_client/password_new.html.twig';
        }
        $this->mailerService->sendTemplatedMail(
            $user->getEmail(),
            $this->translator->trans('Choose your password', locale: $user->getPreferredLang()),
            $templateMail,
            [
                'user' => $user,
                'client' => $user->getClient(),
            ],
            metadatas: [
                'origin' => MailerService::ORIGIN_EMAIL_SEND_CHOOSE_PASSWORD_REQUEST,
                'user' => $user->getId(),
            ],
            senderSystem: true
        );
    }

    /**
     * @throws InvalidArgumentException
     */
    public function onAssociatingExistingUserToClient(UserPasswordEvent $event): void
    {
        $user = $event->getUser();

        $this->mailerService->sendTemplatedMail(
            $user->getEmail(),
            $this->translator->trans('Your user account has been linked to a new account', locale: $user->getPreferredLang()),
            'emails/manager_client/new_association_user_to_account.html.twig',
            [
                'user' => $user,
                'client' => $user->getClient(),
            ],
            metadatas: [
                'origin' => MailerService::ORIGIN_EMAIL_SEND_INFO_USER_LINK_TO_NEW_CLIENT,
                'user' => $user->getId(),
            ],
            senderSystem: true
        );
    }
}
