<?php

namespace App\EventSubscriber;

use App\Event\IDFuseMessageEvent;
use App\Event\IDFuseMessageEvents;
use App\Service\BounceService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class IDFuseWebHookSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private BounceService $bounceService,
        private EntityManagerInterface $entityManager
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            IDFuseMessageEvents::SEND => 'hydrateBounce',
            IDFuseMessageEvents::SOFT_BOUNCE => 'hydrateBounce',
            IDFuseMessageEvents::HARD_BOUNCE => 'hydrateBounce',
            IDFuseMessageEvents::OPENED => 'hydrateBounce',
            IDFuseMessageEvents::CLICKED => 'hydrateBounce',
        ];
    }

    public function hydrateBounce(IDFuseMessageEvent $event): void
    {
        $bounce = $this->bounceService->createBounce(
            $event->getName(),
            $event->getMessage()
        );

        $this->entityManager->persist($bounce);
        $this->entityManager->flush($bounce);
    }
}
