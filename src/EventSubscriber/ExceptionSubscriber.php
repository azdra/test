<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(private LoggerInterface $logger, private TokenStorageInterface $tokenStorage, private RequestStack $requestStack)
    {
    }

    public static function getSubscribedEvents(): array
    {
        // Écoute l'événement kernel.exception
        return [
            ExceptionEvent::class => 'onKernelException',
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        if ($exception instanceof AccessDeniedHttpException) {
            $userEmail = 'Anonyme';
            $token = $this->tokenStorage->getToken();
            if (null !== $token && is_object($user = $token->getUser())) {
                $userEmail = $user->getUsername();
            }

            $request = $this->requestStack->getCurrentRequest();
            $route = $request->attributes->get('_route');

            $this->logger->error('Accès refusé.', [
                'userEmail' => $userEmail,
                'route' => $route,
                'message' => $exception->getMessage(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
            ]);
        }
    }
}
