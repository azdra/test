<?php

namespace App\EventSubscriber;

use App\Entity\Guest;
use App\Event\MandrillMessageEvent;
use App\Event\MandrillMessageEvents;
use App\Service\BounceService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MandrillWebHookSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private BounceService $bounceService,
        private EntityManagerInterface $entityManager
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            MandrillMessageEvents::SEND => 'hydrateBounce',
            MandrillMessageEvents::DELAYED => 'hydrateBounce',
            MandrillMessageEvents::SOFT_BOUNCE => 'hydrateBounce',
            MandrillMessageEvents::HARD_BOUNCE => 'hydrateBounce',
            MandrillMessageEvents::SPAM => 'hydrateBounce',
            MandrillMessageEvents::REJECTED => 'hydrateBounce',
            MandrillMessageEvents::UNSUBSCRIBE => 'hydrateBounce',
            MandrillMessageEvents::OPENED => 'hydrateBounce',
            MandrillMessageEvents::CLICKED => 'hydrateBounce',
            MandrillMessageEvents::BLACKLIST => 'hydrateBounce',
            MandrillMessageEvents::WHITELIST => 'hydrateBounce',
        ];
    }

    public function hydrateBounce(MandrillMessageEvent $event): void
    {
        $message = $event->getMessage();
        if (
            is_array($message['metadata']) &&
            array_key_exists('guest_id', $message['metadata']) &&
            array_key_exists('type', $message['metadata']) &&
            $message['metadata']['type'] == 'invitation'
        ) {
            $guest = $this->entityManager->getRepository(Guest::class)->find($message['metadata']['guest_id']);
            if (!empty($guest)) {
                $guest->setIsBounce(true);
                $this->entityManager->persist($guest);
                $this->entityManager->flush($guest);
            }
        }

        $bounce = $this->bounceService->createBounce(
            $event->getName(),
            $event->getMessage()
        );

        $this->entityManager->persist($bounce);
        $this->entityManager->flush($bounce);
    }
}
