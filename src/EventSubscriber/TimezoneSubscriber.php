<?php

namespace App\EventSubscriber;

use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Intl\Timezones;
use Symfony\Component\Security\Core\Security;
use Twig\Environment;
use Twig\Extension\CoreExtension;

class TimezoneSubscriber implements EventSubscriberInterface
{
    public const DEFAULT_TIMEZONE = 'Europe/Paris';

    public function __construct(private Security $security, private Environment $twig)
    {
    }

    public function onKernelRequest(ControllerEvent $event): void
    {
        if (!$this->security->isGranted(User::ROLE_TRAINEE)) {
            return;
        }

        /** @var User $user */
        $user = $this->security->getUser();
        $timezone = $user->getClient()->getTimezone();
        if (!Timezones::exists($timezone)) {
            $timezone = self::DEFAULT_TIMEZONE;
        }

        $this->twig->getExtension(CoreExtension::class)->setTimezone($timezone);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => [['onKernelRequest', 20]],
        ];
    }
}
