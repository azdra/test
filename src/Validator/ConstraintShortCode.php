<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ConstraintShortCode extends Constraint
{
    public string $message = 'This content has a mistake. Check these short code and starting / ending blocks';
    public $groups = ['Default', 'short_code_validation'];
}
