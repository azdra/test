<?php

namespace App\Validator;

use App\Service\ConvocationService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;

class ConstraintShortCodeValidator extends ConstraintValidator
{
    private ConvocationService $convocationService;

    public function __construct(ConvocationService $convocationService, protected TranslatorInterface $translator)
    {
        $this->convocationService = $convocationService;
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ConstraintShortCode) {
            throw new UnexpectedTypeException($constraint, ConstraintShortCode::class);
        }

        if (!empty($value)) {
            try {
                $shortsCodesConvocationContent = $this->convocationService->checkShortsCodesValid($value);
                if (count($shortsCodesConvocationContent) > 0) {
                    $message = ' :';
                    foreach ($shortsCodesConvocationContent as $shortsCodes) {
                        $message .= ' | '.$shortsCodes;
                    }
                    $this->context->buildViolation($this->translator->trans('Unknown short codes have been detected').$message)
                                  ->addViolation();
                }
            } catch (LoaderError|SyntaxError $e) {
                $this->context->buildViolation($constraint->message)
                              ->addViolation();
            }
        }
    }
}
