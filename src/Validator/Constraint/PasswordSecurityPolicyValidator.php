<?php

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class PasswordSecurityPolicyValidator extends ConstraintValidator
{
    private const PASSWORD_MINIMUM_LENGTH = 8;

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof PasswordSecurityPolicy) {
            throw new UnexpectedTypeException($constraint, PasswordSecurityPolicy::class);
        }

        $isValid = \preg_match('/[a-z]/', $value)
                && \preg_match('/[A-Z]/', $value)
                && \preg_match('/\d/', $value)
                && \preg_match('/[!\\/@#$%^&*(),.?:{}|<>]/', $value)
                && (int) (strlen($value) > self::PASSWORD_MINIMUM_LENGTH);

        if (!$isValid) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('length', (string) self::PASSWORD_MINIMUM_LENGTH)
                ->addViolation();
        }
    }
}
