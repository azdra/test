<?php

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD)]
class PasswordSecurityPolicy extends Constraint
{
    public string $message = 'The password must contain at least one letter in lowercase, one in uppercase, one number, one special character, and must be at least {{ length }} characters long.';
}
