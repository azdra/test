<?php

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ServiceScheduleValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ServiceSchedule) {
            throw new UnexpectedTypeException($constraint, ServiceSchedule::class);
        }

        $isValid = true;
        $emptyFields = [];

        /** @var \App\Entity\ServiceSchedule $value */
        if ($value->isActive()) {
            if ($value->getStartTimeAM() === null) {
                $emptyFields[] = 'StartDateAM';
            }

            if ($value->getEndTimeAM() === null) {
                $emptyFields[] = 'EndDateAM';
            }

            if ($value->getStartTimeLH() === null) {
                $emptyFields[] = 'StartDateLH';
            }

            if ($value->getEndTimeLH() === null) {
                $emptyFields[] = 'EndDateLH';
            }

            if ($value->getStartTimePM() === null) {
                $emptyFields[] = 'StartDatePM';
            }

            if ($value->getEndTimePM() === null) {
                $emptyFields[] = 'EndDatePM';
            }

            if (count($emptyFields) > 0) {
                $isValid = false;
            }
        }

        if (!$isValid) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ fields }}', implode(', ', $emptyFields))
                ->setParameter('{{ options }}', 'active')
                ->addViolation();
        }
    }
}
