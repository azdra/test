<?php

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD)]
class ScheduleConstraint extends Constraint
{
    public string $message = 'This field must have days, hours and minutes entry';
    public bool $useMinutes = true;
}
