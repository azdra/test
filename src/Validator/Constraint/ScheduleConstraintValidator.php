<?php

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ScheduleConstraintValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ScheduleConstraint) {
            throw new UnexpectedTypeException($constraint, ScheduleConstraint::class);
        }

        $isValid = is_null($value)
            || (
                is_array($value)
                && array_key_exists('days', $value)
                && is_int($value['days'])
                && array_key_exists('hours', $value)
                && is_int($value['hours'])
            );

        if (is_array($value) && $constraint->useMinutes) {
            $isValid = $isValid
                && array_key_exists('minutes', $value)
                && is_int($value['minutes']);
        }

        if (!$isValid) {
            $this->context->buildViolation($constraint->message)
                          ->addViolation();
        }
    }
}
