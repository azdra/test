<?php

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_METHOD)]
class ServiceSchedule extends Constraint
{
    public string $message = 'the fields relating to the hours of the support must be completed if you check the option outside standard hours';

    public function __construct(array $options = null, string $message = null, bool $allowNull = null, callable $normalizer = null, array $groups = null, $payload = null)
    {
        parent::__construct($options ?? [], $groups, $payload);
    }
}
