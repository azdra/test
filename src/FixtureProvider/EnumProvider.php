<?php

namespace App\FixtureProvider;

use App\Entity\CategorySessionType;

class EnumProvider
{
    public function categorySessionType(int $categorySession): ?CategorySessionType
    {
        return CategorySessionType::from($categorySession);
    }
}
