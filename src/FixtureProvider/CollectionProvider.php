<?php

namespace App\FixtureProvider;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class CollectionProvider
{
    public function collection(array $array): Collection
    {
        return new ArrayCollection($array);
    }
}
