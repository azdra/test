<?php

namespace App\FixtureProvider;

class ParametersProvider
{
    public function __construct(
        private string $adobeConnectUsername,
        private string $adobeConnectPassword,

        private string $ciscoWebexConnectorHostWebexId,
        private string $ciscoWebexConnectorPassword,
        private string $ciscoWebexConnectorSiteName,
        private string $ciscoWebexConnectorWebexId,

        private string $microsoftTeamsBaseUrl,
        private string $microsoftTeamsTokenUrl,
        private string $microsoftTeamsTenantId,
        private string $microsoftTeamsScope,
        private string $microsoftTeamsGrantType,
        private string $microsoftTeamsOrganizer,
        private string $microsoftTeamsOrganizerId,
        private string $microsoftTeamsClientId,
        private string $microsoftTeamsClientSecret,

        private string $endPointDomain
    ) {
    }

    public function params(string $name): string
    {
        return $this->{ $name };
    }
}
