<?php

namespace App\Handler;

use App\Event\BrevoSmsMessageEvent;
use App\Event\BrevoSmsMessageEvents;
use App\Model\JsonUtilsTrait;
use App\Repository\BounceRepository;
use App\Repository\UserRepository;
use App\Service\BounceService;
use App\Service\MailerService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class BrevoSmsWebhookHandler implements BrevoSmsWebhookHandlerInterface
{
    use JsonUtilsTrait;

    public function __construct(
        private EventDispatcherInterface $eventDispatcher,
        private MailerService $mailerService,
        private BounceRepository $bounceRepository,
        private UserRepository $userRepository,
        private BounceService $bounceService,
        private LoggerInterface $logger
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function handleRequest(Request $request): void
    {
        $this->logger->info('Webhook SMS Brevo');
        $this->logger->info('message', $request->request->all());

        /*$testDataDelivered = [
            "status" => "OK",
            "msg_status" => "delivered", // accepted | delivered | hard_bounce
            "description" => "delivered", // accepted | delivered
            "to"=> "33600000000",
            "reference" => [
                "1" => "15t8tkwqdbziiseqzng"
            ],
            "ts_event" => 1713362551,
            "date" => "2024-04-17 16:02:31",
            "messageId" => 106840062976345,
            "tag" => "{\"type\":\"testSendSms\",\"session\":74}"
        ];

        $testDataBounce = [
            "status" => "OK",
            "msg_status" => "hard_bounce", // accepted | delivered | hard_bounce
            "errorCode" => 31,
            "description" => "Failed to deliver message for reasons unknown", // accepted | delivered
            "bounce_type" => "hard_bounce",
            "to"=> "33600000000",
            "reference" => [
                "1" => "15t8tkwqdbziiseqzng"
            ],
            "ts_event" => 1713362551,
            "date" => "2024-04-17 16:02:31",
            "messageId" => 106840062976345,
            "tag" => "{\"type\":\"testSendSms\",\"session\":74}"
        ];*/

        $encodedEvents = $request->request->all();
        if (in_array($encodedEvents['msg_status'], ['hard_bounce'])) {
            $events = BrevoSmsMessageEvent::create(array_merge($encodedEvents, [
                'event' => BrevoSmsMessageEvents::PREFIX.'.'.$encodedEvents['msg_status'],
            ]));
            $this->handleMessage($events);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function handleMessage(BrevoSmsMessageEvent $event): void
    {
        $this->eventDispatcher->dispatch($event, $event->getName());
    }
}
