<?php

namespace App\Handler;

use App\Entity\Bounce;
use App\Event\IDFuseMessageEvent;
use App\Event\IDFuseMessageEvents;
use App\Exception\InvalidArgumentException;
use App\Model\JsonUtilsTrait;
use App\Repository\BounceRepository;
use App\Repository\UserRepository;
use App\Service\BounceService;
use App\Service\MailerService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class IDFuseWebhookHandler implements IDFuseWebhookHandlerInterface
{
    use JsonUtilsTrait;

    public function __construct(
        private EventDispatcherInterface $eventDispatcher,
        private MailerService $mailerService,
        private BounceRepository $bounceRepository,
        private UserRepository $userRepository,
        private BounceService $bounceService,
        private LoggerInterface $logger
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function handleRequest(Request $request): void
    {
        /*$requestHard1 = '{"maildata_id":"51905470","company_id":"407","bounce_at":"2023-06-21 11:33:33+0200","bounce_code":"4.2.1","bounce_cat":"inactive-mailbox","bounce_type":"hard","bounce_reason":"4.2.1 (mailbox disabled, not accepting messages)","from_email":"***@*****","vmta":"******","jobId":"407","dsnStatus":"4.2.1 (mailbox disabled, not accepting messages)","dsnDiag":"smtp;450 4.2.1 <******>: Recipient address rejected: this mailbox is inactive and has been disabled","dsnMta":"smtpz4.laposte.net (160.92.124.66)","vmtaPool":null,"sourceIP":"185.245.221.70"} ';

        $requestHard2 = '{"maildata_id":"9109502","company_id":"942","bounce_at":"2023-06-21 11:33:54+0200","bounce_code":"5.1.1","bounce_cat":"bad-mailbox","bounce_type":"hard","bounce_reason":"5.1.1 (bad destination mailbox address)","from_email":"***@*****","vmta":"****","jobId":"942","dsnStatus":"5.1.1 (bad destination mailbox address)","dsnDiag":"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient\'s email address for typos or unnecessary spaces. Learn more at https:\/\/support.google.com\/mail\/?p=NoSuchUser le8-20020a170902fb0800b001ac84f87b0fsi206708plb.339 - gsmtp","dsnMta":"gmail-smtp-in.l.google.com (142.251.10.27)","vmtaPool":null,"sourceIP":"185.245.221.68"}';

        $requestSoft1 = '{"maildata_id":"283221016","company_id":"342","bounce_at":"2023-06-21 11:38:56+0200","bounce_code":"5.0.0","bounce_cat":"quota-issues","bounce_type":"soft","bounce_reason":"5.0.0 (SMTP reply matched bounce-rcpt pattern rule)","from_email":"***@***","vmta":"****","jobId":"342","dsnStatus":"5.0.0 (SMTP reply matched bounce-rcpt pattern rule)","dsnDiag":"smtp;452 4.2.2 The email account that you tried to reach is over quota. Please direct the recipient to https:\/\/support.google.com\/mail\/?p=OverQuotaTemp s5-20020a170903214500b001b3e1afe77fsi3587520ple.103 - gsmtp","dsnMta":"gmail-smtp-in.l.google.com (142.251.10.26)","vmtaPool":null,"sourceIP":"185.245.221.21"}';*/

        $this->logger->info('Webhook IDFuse');
        $this->logger->info('message', $request->request->all());

        $events = [];

        try {
            $requestData = [
                'method' => $request->getMethod(),
                'uri' => $request->getUri(),
                'headers' => $request->headers->all(),
                'cookies' => $request->cookies->all(),
                'parameters' => $request->request->all(),
                'files' => $request->files->all(),
                'content' => $request->getContent(),
                'attributes' => $request->attributes->all(),
                'query' => $request->query->all(),
                'server' => $request->server->all(),
            ];

            $this->mailerService->sendTemplatedMail(
                'ingenierie@live-session.fr',
                'Déclenchement d\'un webhook sur IDFuse',
                'emails/alert/alert_webhook_idfuse.html.twig',
                context: [
                    'dateSend' => ( new \DateTime('now') )->format('d/m/Y H:i'),
                    'requestData' => $requestData,
                ],
                metadatas: [
                    'origin' => MailerService::ORIGIN_EMAIL_ALERT_IDFUSE_WEBHOOK,
                ],
                senderSystem: true
            );

            //$events = $this->getIDFuseMessageEvents($request);
            /** Bouchon faisant appel à l'API IDFuse qui remonte la liste des bounces */
            $events = $this->getIDFuseMessageEventsFromApiCampaignReportBounceList($request);
            /* Fin bouchon */

            foreach ($events as $event) {
                $this->handleMessage($event);
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function handleMessage(IDFuseMessageEvent $event): void
    {
        $this->eventDispatcher->dispatch($event, $event->getName());
    }

    /**
     * @throws InvalidArgumentException
     * @throws BadRequestHttpException
     */
    protected function getIDFuseMessageEvents(Request $request): array
    {
        $encodedEvents = $request->request->all(); //$request->request->get('idfuse_events');

        if (!$encodedEvents) {
            throw new BadRequestHttpException('The "idfuse_events" key should not be missing.');
        }

        $events = [];
        $event = $encodedEvents['bounce_type'];
        $events[] = IDFuseMessageEvent::create(array_merge($encodedEvents, [
                'event' => IDFuseMessageEvents::PREFIX.'.'.$event,
            ]));

        return $events;
    }

    protected function getIDFuseMessageEventsFromApiCampaignReportBounceList(Request $request): array
    {
        $params = [
            'campaign_id' => '6',
        ];
        $listBouncesIDFuse = $this->bounceService->callIDFuseAPICampaignReportBouceList($params);

        if (count($listBouncesIDFuse) == 0) {
            throw new BadRequestHttpException('The "idfuse CampaignReportBounceList" is empty.');
        }

        $events = [];
        foreach ($listBouncesIDFuse as $bounce) {
            if (is_array($bounce) && array_key_exists('email', $bounce)) {
                $users = $this->userRepository->findBy(['email' => $bounce['email']]);
                foreach ($users as $user) {
                    $bouncesUser = $this->bounceRepository->findBy(['email' => $bounce['email'], 'active' => 1]);
                    if (empty($bouncesUser)) {
                        $events[] = IDFuseMessageEvent::createFromApiCampaignReportBounceList(array_merge($bounce, [
                            'event' => IDFuseMessageEvents::PREFIX.'.'.$bounce['type'],
                            'user_id' => $user->getId(),
                        ]));
                    }
                }
                /*$users = $this->userRepository->findBy(['email' => $bounce['email']]);
                if (!empty($users)) {
                    $bouncesUser = $this->bounceRepository->findBy(['state' => Bounce::TO_CORRECT, 'email' => $bounce['email'], 'active' => 1]);
                    if (empty($users)) {
                        $events[] = IDFuseMessageEvent::createFromApiCampaignReportBounceList(array_merge($bounce, [
                            'event' => IDFuseMessageEvents::PREFIX.'.'.$bounce['type'],
                        ]));
                    }
                }*/
            }
        }

        return $events;
    }

    private function isMultidimensionalArray(array $array): bool
    {
        foreach ($array as $value) {
            if (is_array($value)) {
                return true; // Si une valeur est un tableau, alors c'est un tableau multidimensionnel
            }
        }

        return false; // Sinon, c'est un tableau unidimensionnel
    }
}
