<?php

namespace App\Handler;

use App\Entity\Bounce;
use App\Event\BrevoMessageEvent;
use App\Event\BrevoMessageEvents;
use App\Exception\InvalidArgumentException;
use App\Model\JsonUtilsTrait;
use App\Repository\BounceRepository;
use App\Repository\UserRepository;
use App\Service\BounceService;
use App\Service\MailerService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class BrevoWebhookHandler implements BrevoWebhookHandlerInterface
{
    use JsonUtilsTrait;

    public function __construct(
        private EventDispatcherInterface $eventDispatcher,
        private MailerService $mailerService,
        private BounceRepository $bounceRepository,
        private UserRepository $userRepository,
        private BounceService $bounceService,
        private LoggerInterface $logger
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function handleRequest(Request $request): void
    {
        $requestTestBrevo1 = '{"event": "hard_bounce","email": "example@domain.com","id": 123456789,"date": "2024-11-21 01:00:00","ts":1604933619, "message-id": "201798300811.5787683@relay.domain.com","ts_event": 1604933654,"subject": "My first Transactional","X-Mailin-custom": "some_custom_header","sending_ip": "xxx.xxx.xxx.xxx","template_id": 22,"tags": ["Convocation", "14"],"reason": "server is down","ts_epoch":1604933653}';
        $this->logger->info('Webhook Brevo');
        $this->logger->info('message', $request->request->all());

        $events = [];

        try {
            $requestData = [
                'method' => $request->getMethod(),
                'uri' => $request->getUri(),
                'headers' => $request->headers->all(),
                'cookies' => $request->cookies->all(),
                'parameters' => $request->request->all(),
                'files' => $request->files->all(),
                'content' => $request->getContent(),
                'attributes' => $request->attributes->all(),
                'query' => $request->query->all(),
                'server' => $request->server->all(),
            ];

            $this->mailerService->sendTemplatedMail(
                'ingenierie@live-session.fr',
                'Déclenchement d\'un webhook sur Brevo',
                'emails/alert/alert_webhook_brevo.html.twig',
                context: [
                    'dateSend' => ( new \DateTime('now') )->format('d/m/Y H:i'),
                    'requestData' => $requestData,
                ],
                metadatas: [
                    'origin' => MailerService::ORIGIN_EMAIL_ALERT_BREVO_WEBHOOK,
                ],
                senderSystem: true
            );

            //$events = $this->getBrevoMessageEvents($request);
            /** Bouchon faisant appel à l'API Brevoqui remonte la liste des bounces */
            //$events = $this->getBrevoMessageEventsFromApiCampaignReportBounceList($request);
            /* Fin bouchon */
            $events = $this->getBrevoMessageEvents($request);

            foreach ($events as $event) {
                $this->handleMessage($event);
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function handleMessage(BrevoMessageEvent $event): void
    {
        $this->eventDispatcher->dispatch($event, $event->getName());
    }

    /**
     * @throws InvalidArgumentException
     * @throws BadRequestHttpException
     */
    protected function getBrevoMessageEvents(Request $request): array
    {
        $encodedEvents = $request->request->all(); //$request->request->get('brevo_events');

        if (!$encodedEvents) {
            throw new BadRequestHttpException('The "brevo_events" key should not be missing.');
        }

        $events = [];
        $event = $encodedEvents['event'];
        $events[] = BrevoMessageEvent::create(array_merge($encodedEvents, [
                'event' => BrevoMessageEvents::PREFIX.'.'.$event,
            ]));

        return $events;
    }

    protected function getBrevoMessageEventsFromApiCampaignReportBounceList(Request $request): array
    {
        /*$params = [
            'campaign_id' => '6',
        ];
        $listBouncesBrevo = $this->bounceService->callBrevoAPICampaignReportBounceList($params);

        if (count($listBouncesBrevo) == 0) {
            throw new BadRequestHttpException('The "brevo CampaignReportBounceList" is empty.');
        }*/

        $events = [];
        /*foreach ($listBouncesBrevo as $bounce) {
            if (is_array($bounce) && array_key_exists('email', $bounce)) {
                $users = $this->userRepository->findBy(['email' => $bounce['email']]);
                foreach ($users as $user) {
                    $bouncesUser = $this->bounceRepository->findBy(['email' => $bounce['email'], 'active' => 1]);
                    if (empty($bouncesUser)) {
                        $events[] = BrevoMessageEvent::createFromApiCampaignReportBounceList(array_merge($bounce, [
                            'event' => BrevoMessageEvents::PREFIX.'.'.$bounce['type'],
                            'user_id' => $user->getId(),
                        ]));
                    }
                }
                $users = $this->userRepository->findBy(['email' => $bounce['email']]);
                if (!empty($users)) {
                    $bouncesUser = $this->bounceRepository->findBy(['state' => Bounce::TO_CORRECT, 'email' => $bounce['email'], 'active' => 1]);
                    if (empty($users)) {
                        $events[] = IDFuseMessageEvent::createFromApiCampaignReportBounceList(array_merge($bounce, [
                            'event' => IDFuseMessageEvents::PREFIX.'.'.$bounce['type'],
                        ]));
                    }
                }
            }
        }*/

        return $events;
    }

    private function isMultidimensionalArray(array $array): bool
    {
        foreach ($array as $value) {
            if (is_array($value)) {
                return true; // Si une valeur est un tableau, alors c'est un tableau multidimensionnel
            }
        }

        return false; // Sinon, c'est un tableau unidimensionnel
    }
}
