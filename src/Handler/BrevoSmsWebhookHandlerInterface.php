<?php

namespace App\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Interface BrevoWebhookHandlerInterface.
 */
interface BrevoSmsWebhookHandlerInterface
{
    /**
     * Handles a request.
     *
     * @throws BadRequestHttpException   When the request is invalid
     * @throws AccessDeniedHttpException When the request signature is invalid
     */
    public function handleRequest(Request $request): void;
}
