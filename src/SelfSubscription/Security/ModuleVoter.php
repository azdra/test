<?php

namespace App\SelfSubscription\Security;

use App\Entity\User;
use App\SelfSubscription\Entity\Module;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class ModuleVoter extends Voter
{
    public const EDIT = 'edit';
    public const ACCESS_HUB = 'access_hub';

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::EDIT, self::ACCESS_HUB])) {
            return false;
        }

        if (!$subject instanceof Module) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $loggedUser = $token->getUser();

        if (!$loggedUser instanceof User) {
            return false;
        }

        // An admin can do everything
        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            return true;
        }

        // You must be manager to edit something
        if ($attribute === self::EDIT && !$this->security->isGranted(User::ROLE_MANAGER)) {
            return false;
        }

        // As a manager you must be on the same platform
        /* @var Module $subject */
        return match ($attribute) {
            self::EDIT => $subject->getClient()->getId() === $loggedUser->getClient()->getId(),
            self::ACCESS_HUB => $this->canAccessHub($loggedUser, $subject),
            default => false
        };
    }

    private function canAccessHub(User $user, Module $module): bool
    {
        if ($user->getClient()->getId() !== $module->getClient()->getId()) {
            return false;
        }

        return $module->isEmailAllowed($user->getEmail());
    }
}
