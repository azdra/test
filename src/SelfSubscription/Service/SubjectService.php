<?php

namespace App\SelfSubscription\Service;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Exception\AsyncWorker\AsyncTaskException;
use App\SelfSubscription\Entity\Subject;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\Error;

class SubjectService
{
    public function __construct(
        private SubjectRepository $subjectRepository,
        private ParticipantSessionSubscriber $subscriber,
        protected TranslatorInterface $translator,
        private EntityManagerInterface $entityManager,
        private ParticipantSessionSubscriber $participantSessionSubscriber,
    ) {
    }

    public function unsubscribeUserFromSubject(User $user, Subject $subject): void
    {
        /** @var ProviderParticipantSessionRole[] $participantSessionRoles */
        $participantSessionRoles = $this->subjectRepository->findCurrentSessionOfSubjectForUser($user, $subject);

        foreach ($participantSessionRoles as $participantSessionRole) {
            $this->subscriber->unsubscribe($participantSessionRole);
        }
    }

    /**
     * @throws Error
     * @throws AsyncTaskException
     */
    public function subscribeUserFromAuthentification(User $user, ProviderSession $providerSession): void
    {
        if ($providerSession->getSubscriptionMax() !== 0 && $providerSession->getTraineesCount() >= $providerSession->getSubscriptionMax()) {
            throw new \ErrorException($this->translator->trans('The session is full.', domain: 'self-subscription'));
        }

        if (null === $providerSession->getSubject()) {
            throw new \ErrorException($this->translator->trans('You can not subscribe to this subject.', domain: 'self-subscription'));
        }

        $this->unsubscribeUserFromSubject($user, $providerSession->getSubject());

        $this->entityManager->flush();

        $this->participantSessionSubscriber->subscribe(
            $providerSession,
            $user,
            ProviderParticipantSessionRole::ROLE_TRAINEE,
            $providerSession->isSessionPresential() ? ProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL : ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL
        );

        $this->entityManager->flush();
    }

    public function updateOrder(Subject $subject, int $newOrder): void
    {
        $currentOrder = $subject->getOrderInModule();
        $module = $subject->getTheme()->getModule();
        $allThemes = $module->getThemes();

        foreach ($allThemes as $theme) {
            $allSubjects = $theme->getSubjects();

            foreach ($allSubjects as $iterSubject) {
                $iterOrder = $iterSubject->getOrderInModule();
                if ($newOrder < $currentOrder && $iterOrder >= $newOrder && $iterOrder < $currentOrder) {
                    $iterSubject->setOrderInModule($iterOrder + 1);
                } elseif ($newOrder > $currentOrder && $iterOrder <= $newOrder && $iterOrder > $currentOrder) {
                    $iterSubject->setOrderInModule($iterOrder - 1);
                }
            }
        }

        $subject->setOrderInModule($newOrder);
    }
}
