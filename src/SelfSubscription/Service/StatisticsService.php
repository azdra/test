<?php

namespace App\SelfSubscription\Service;

use App\Entity\ProviderSession;
use App\SelfSubscription\Entity\Module;

class StatisticsService
{
    public function __construct(
    ) {
    }

    public function statisticsModule(Module $module): array
    {
        $Registered = [];
        $statistics['sessionCreateNumber'] = 0;
        $statistics['sessionUnlimitedNumber'] = 0;
        $statistics['sessionSentNumber'] = 0;
        $statistics['sessionFinishedNumber'] = 0;
        $statistics['sessionCompletedNumber'] = 0;
        $statistics['sessionAvailableNumber'] = 0;
        $statistics['participantsPlacesAvailableNumber'] = 0;
        $statistics['participantsRegisteredNumber'] = 0;
        $statistics['participantsUniqueRegisteredNumber'] = 0;

        foreach ($module->getThemes() as $theme) {
            foreach ($theme->getSubjects() as $subject) {
                foreach ($subject->getSessions()->getValues() as $session) {
                    ++$statistics['sessionCreateNumber'];
                    if ($session->getSubscriptionMax() == 0) {
                        ++$statistics['sessionUnlimitedNumber'];
                    }
                    switch ($session->getReadableState()) {
                        case ProviderSession::FINAL_STATUS_SENT:
                            ++$statistics['sessionSentNumber'];
                            if ($session->getSubscriptionMax() != 0) {
                                $statistics['participantsPlacesAvailableNumber'] += ($session->getSubscriptionMax() - count($session->getTraineesOnly()));
                                if (count($session->getTraineesOnly()) >= $session->getSubscriptionMax()) {
                                    ++$statistics['sessionCompletedNumber'];
                                } else {
                                    ++$statistics['sessionAvailableNumber'];
                                }
                            } else {
                                ++$statistics['sessionAvailableNumber'];
                            }
                            foreach ($session->getTraineesOnly() as $trainee) {
                                $Registered[] = $trainee->getParticipant();
                            }
                            break;
                        case ProviderSession::FINAL_STATUS_FINISHED:
                            ++$statistics['sessionFinishedNumber'];
                            break;
                    }
                }
            }
        }
        $statistics['participantsRegisteredNumber'] += count($Registered);
        $statistics['participantsUniqueRegisteredNumber'] += count(array_unique($Registered));

        return $statistics;
    }
}
