<?php

namespace App\SelfSubscription\Service;

use App\Entity\ActivityLog;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Model\ActivityLog\ActivityLogModel;
use App\SelfSubscription\Entity\Module;
use App\Service\ActivityLogger;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ModuleService
{
    public function __construct(
        private TranslatorInterface $translator,
        private ParticipantSessionSubscriber $participantSessionSubscriber,
        private ActivityLogger $activityLogger,
        private RouterInterface $router,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function validate(Module $module): array
    {
        $errors = ['AppearanceTab' => []];
        if ($module->getPersonalization()->isEnabledReplay() && !$module->getPersonalization()->getReplayTitle()) {
            $errors['AppearanceTab']['menu_item.replay'] = $this->translator->trans('You must set a replay title.');
        }

        if ($module->getPersonalization()->isEnabledFaq() && !$module->getPersonalization()->getFaqTitle()) {
            $errors['AppearanceTab']['menu_item.faq'] = $this->translator->trans('You must set a FAQ title.');
        }

        if ($module->getPersonalization()->isEnabledSessions() && !$module->getPersonalization()->getSessionsTitle()) {
            $errors['AppearanceTab']['menu_item.mysession'] = $this->translator->trans('You must set a session title.');
        }

        return $errors;
    }

    public function subscribe(ProviderSession $session, User $user): void
    {
        $this->participantSessionSubscriber->subscribe(
            $session,
            $user,
            ProviderParticipantSessionRole::ROLE_TRAINEE,
            $session->isSessionPresential() ? ProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL : ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL
        );

        $infos = [
            'message' => [
                'key' => 'The participant <a href="%participantRoute%">%participantName%</a> has subscribed to the session <a href="%sessionRoute%">%sessionName%</a> via the catalog <a href="%catalogRoute%">%catalogName%</a>',
                'params' => [
                    '%participantRoute%' => $this->router->generate('_participant_edit', ['id' => $user->getId()]),
                    '%participantName%' => "{$user->getFirstName()} {$user->getLastName()}",
                    '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                    '%sessionName%' => $session->getName(),
                    '%catalogRoute%' => $this->router->generate('_module_edit', ['id' => $session->getSubject()->getModuleId()]),
                    '%catalogName%' => $session->getSubject()->getTheme()->getModule()->getName(),
                ],
            ],
            'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
        ];
        $this->activityLogger->initActivityLogContext($session->getClient(), $user, ActivityLog::ORIGIN_USER_INTERFACE);
        $this->activityLogger->clearLogModels();
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_PARTICIPANT_JOIN_SESSION_FROM_CATALOG, ActivityLog::SEVERITY_INFORMATION, $infos));
        $this->activityLogger->flushActivityLogs();
    }

    public function updateLastAuthenticationAt(object $userModuleFound): void
    {
        $userModuleFound->setLastAuthenticationAt(new \DateTime());
        $this->entityManager->flush();
    }
}
