<?php

namespace App\SelfSubscription\Serializer;

use App\SelfSubscription\Entity\Subject;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SubjectNormalizer implements NormalizerInterface
{
    public function __construct(protected ObjectNormalizer $normalizer, private string $selfSubscriptionSubjectSpeakerSUri, private string $selfSubscriptionSubjectIllustrationUri)
    {
    }

    public function normalize($object, string $format = null, array $context = []): ?array
    {
        $data = $this->normalizer->normalize($object, $format, $context);
        $data = is_array($data) ? $data : $data->getArrayCopy();

        if (array_key_exists('speakers', $data)) {
            foreach ($data['speakers'] as $key => $speaker) {
                $data['speakers'][$key]['photoUri'] = $speaker['photo'] ? $this->selfSubscriptionSubjectSpeakerSUri.$speaker['photo'] : null;
            }
        }

        if (array_key_exists('program', $data) && array_key_exists('illustration', $data['program']) && $data['program']['illustration'] !== null) {
            $data['program']['illustrationUri'] = $this->selfSubscriptionSubjectIllustrationUri.$data['program']['illustration'];
        }

        return $data;
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof Subject;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [
            Subject::class => true,
        ];
    }
}
