<?php

namespace App\SelfSubscription\Serializer;

use App\SelfSubscription\Entity\Module;
use App\Service\Utils\ImageUtils;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ModuleNormalizer implements NormalizerInterface
{
    public function __construct(protected ObjectNormalizer $normalizer, private string $selfSubscriptionModuleLogoUri, private string $selfSubscriptionModuleLogoDirectory)
    {
    }

    public function normalize($object, string $format = null, array $context = []): ?array
    {
        $data = $this->normalizer->normalize($object, $format, $context);
        $data = is_array($data) ? $data : $data->getArrayCopy();

        foreach (['banner', 'logo'] as $key => $value) {
            $data[sprintf('%sUri', $value)] = (!empty($data[$value])) ? $this->selfSubscriptionModuleLogoUri.$data[$value] : null;
        }

        foreach (['trainingBanner', 'replayBanner', 'sessionsBanner'] as $key => $value) {
            $data['personalization'][sprintf('%sUri', $value)] = (!empty($data['personalization'][$value])) ? $this->selfSubscriptionModuleLogoUri.$data['personalization'][$value] : null;
        }

        foreach (['logo'] as $key => $value) {
            $data[sprintf('%sFormat', $value)] = (!empty($data[$value])) && ImageUtils::isSquare($this->selfSubscriptionModuleLogoDirectory.$data[$value]) ? 'square' : 'rectangle';
        }

        return $data;
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof Module;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [
            Module::class => true,
        ];
    }
}
