<?php

namespace App\SelfSubscription\Controller;

use App\Entity\ReplayCategory;
use App\Exception\UploadFile\UploadFileException;
use App\Model\ApiResponseTrait;
use App\Repository\ClientRepository;
use App\Repository\UserModuleRepository;
use App\Repository\VideoReplayRepository;
use App\SelfSubscription\Entity\Module;
use App\SelfSubscription\Repository\ModuleRepository;
use App\SelfSubscription\Repository\SubjectRepository;
use App\SelfSubscription\Security\ModuleVoter;
use App\SelfSubscription\Service\BlackAndWhiteListImporterService;
use App\SelfSubscription\Service\ModuleService;
use App\SelfSubscription\Service\StatisticsService;
use App\SelfSubscription\Service\SubjectService;
use App\Service\ImportSessionService;
use App\Service\RessourceUploader\RessourceUploader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/self-subscription/module')]
#[Security('is_granted(\'ROLE_ADMIN\') or (user.getClient().getSelfRegistering().isEnabled() and is_granted(\'ROLE_MANAGER\')) ')]
class ModuleController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private RessourceUploader $ressourceUploader,
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger,
        private string $selfSubscriptionModuleLogoSavefileDirectory,
        private string $selfSubscriptionSubjectIllustrationSavefilesDirectory,
        private string $selfSubscriptionModuleLogoUri,
        private string $selfSubscriptionSubjectIllustrationUri,
        private NormalizerInterface $normalizer,
        private DenormalizerInterface $denormalizer,
        private ModuleRepository $moduleRepository,
        private ValidatorInterface $validator,
        private SubjectRepository $subjectRepository,
        private SerializerInterface $serializer,
        private BlackAndWhiteListImporterService $blackAndWhiteListImporter,
        private TranslatorInterface $translator,
        private ClientRepository $clientRepository,
        private StatisticsService $statisticsService,
        private VideoReplayRepository $videoReplayRepository,
        private ModuleService $moduleService,
        private SubjectService $subjectService,
        private UserModuleRepository $userModuleRepository,
    ) {
    }

    #[Route('/', name: '_module_list', methods: ['GET'])]
    public function listModule(): Response
    {
        $client = $this->clientRepository->getValueForSelectInput($this->isGranted('ROLE_ADMIN') ? null : $this->getUser()->getClient());

        return $this->render('self-subscription/module/list.html.twig', [
            'client' => $client,
            'title' => 'Catalogs',
        ]);
    }

    #[Route('/search', name: '_module_search', options: ['expose' => true], methods: ['GET'])]
    public function searchModule(Request $request): Response
    {
        $search = $request->query->all();
        if (!$this->isGranted('ROLE_ADMIN')) {
            $search['client'] = $this->getUser()->getClient();
        }
        $modules = $this->moduleRepository->globalSearch($search);

        return $this->apiResponse($this->normalizer->normalize($modules, context: ['groups' => ['ui:module:listing']]));
    }

    #[Route('/edit/{id}', name: '_module_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ModuleVoter::EDIT, subject: 'module')]
    public function editModule(Module $module, Request $request): Response
    {
        $viewTab = $request->query->get('viewTab', 'GeneralTab');

        $importRestrictions = [
            'max_file_size' => RessourceUploader::humanReadableFileSize(ImportSessionService::MAX_FILE_SIZE),
            'allowed_extension' => implode(', ', ImportSessionService::AUTHORIZED_EXTENSION),
            'allowed_max_row' => ImportSessionService::ALLOWED_MAX_ROW,
        ];

        $statistics = $this->statisticsService->statisticsModule($module);

        return $this->render('self-subscription/module/edit.html.twig', [
            'title' => $module->getName(),
            'viewTab' => $viewTab,
            'module' => $module,
            'statistics' => $statistics,
            'importRestrictions' => $importRestrictions,
        ]);
    }

    #[Route('/save', name: '_module_save', options: ['expose' => true], methods: ['POST'])]
    public function saveModule(Request $request): Response
    {
        $request = json_decode($request->getContent(), true);

        $request['subscriptionMax'] = (int) $request['subscriptionMax'];
        $request['alertLimit'] = (int) $request['alertLimit'];
        $request['closeSubscriptionsBeforeNbrDays'] = (int) $request['closeSubscriptionsBeforeNbrDays'];
        $context = [
            AbstractNormalizer::GROUPS => 'ui:module:update',
            AbstractNormalizer::DEFAULT_CONSTRUCTOR_ARGUMENTS => [
                Module::class => ['client' => $this->getUser()->getClient()],
            ],
        ];

        if (array_key_exists('id', $request)) {
            $module = $this->moduleRepository->find($request['id']);
            if (empty($module) || !$this->isGranted(ModuleVoter::EDIT, $module)) {
                throw new AccessDeniedException();
            }

            $context[AbstractNormalizer::OBJECT_TO_POPULATE] = $module;
            $this->denormalizer->denormalize($request, Module::class, context: $context);
        } else {
            foreach (['companyLabel', 'companyPlaceholder', 'emailLabel', 'emailPlaceholder'] as $key => $value) {
                $request[$value] = $this->translator->trans($request[$value], locale: $request['lang']);
            }

            foreach (['faqTitle', 'faqDescription', 'trainingTitle', 'replayTitle', 'replayDescription', 'sessionsTitle'] as $key => $value) {
                $request['personalization'][$value] = $this->translator->trans($request['personalization'][$value], locale: $request['lang']);
            }

            /** @var Module $module */
            $module = $this->denormalizer->denormalize($request, Module::class, context: $context);

            if (
                $this->isGranted('ROLE_ADMIN')
                && array_key_exists('client', $request)
                && array_key_exists('id', $request['client'])
                && $this->getUser()->getClient()->getId() !== $request['client']['id']
            ) {
                $client = $this->clientRepository->find($request['client']['id']);
                if ($client !== null) {
                    $module->setClient($client);
                }
            }
        }

        $moduleValidation = $this->moduleService->validate($module);
        $violations = $this->validator->validate($module);
        if ($violations->count() === 0 && count($moduleValidation['AppearanceTab']) === 0) {
            $this->entityManager->persist($module);
            $this->entityManager->flush();

            return $this->apiResponse($this->normalizer->normalize([$module], context: ['groups' => ['ui:module:read']]));
        } else {
            if (count($moduleValidation) > 0) {
                $extraDatas = [];
                foreach ($violations as $violation) {
                    $extraDatas[$violation->getPropertyPath()] = $violation->getMessage();
                }
            }

            if (count($moduleValidation)) {
                $extraDatas = array_merge($extraDatas, $moduleValidation);
            }

            return $this->apiErrorResponse($this->translator->trans('Check your data'), extraData: $extraDatas);
        }
    }

    #[Route('/delete/{id}', name: '_module_delete', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ModuleVoter::EDIT, subject: 'module')]
    public function deleteModule(Module $module): Response
    {
        $this->subjectRepository->unlinkSessions(module: $module);
        $this->entityManager->remove($module);
        $this->entityManager->flush();

        return $this->apiResponse([]);
    }

    #[Route('/upload/logo', name: '_module_upload_files', options: ['expose' => true], methods: ['POST'])]
    public function uploadLogo(Request $request): Response
    {
        try {
            /** @var UploadedFile $file */
            $file = $request->files->get('file');

            $formatParam = $request->request->get('format', implode(',', Module::AUTHORIZED_EXTENSIONS));
            $this->ressourceUploader->isFileUploadedValid($file, explode(',', $formatParam), Module::MAX_FILE_SIZE);

            $filename = \md5(\random_bytes(32)).'.'.$file->getClientOriginalExtension();

            ( new Filesystem() )->copy($file->getRealPath(), $this->selfSubscriptionModuleLogoSavefileDirectory.'/'.$filename, true);

            return $this->apiResponse([
                'filetarget' => $filename,
                'previewUri' => $this->selfSubscriptionModuleLogoUri.$filename,
            ]);
        } catch (UploadFileException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/import-file-mail-module/{id}', name: '_white_black_list_import_file', options: ['expose' => true], methods: ['POST'])]
    public function importFileMailModule(Request $request, Module $module): Response
    {
        try {
            $this->blackAndWhiteListImporter->importMailsForWhiteAndBackList($request, $module);
            $this->entityManager->flush();

            return new Response($this->serializer->serialize(
                $module,
                'json',
                ['groups' => 'ui:module:read']
            ));
        } catch (UploadFileException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($this->translator->trans('No import file'));
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($this->translator->trans('The file can not be imported for an unknown error'));
        }
    }

    #[Route('/upload/replay-image-video/', name: '_video_replay_image_upload_files', options: ['expose' => true], methods: ['POST'])]
    public function uploadReplayImage(Request $request): Response
    {
        try {
            /** @var UploadedFile $file */
            $file = $request->files->get('file');

            $formatParam = $request->request->get('format', implode(',', Module::AUTHORIZED_EXTENSIONS));
            $this->ressourceUploader->isFileUploadedValid($file, explode(',', $formatParam), Module::MAX_FILE_SIZE);

            $filename = \md5(\random_bytes(32)).'.'.$file->getClientOriginalExtension();

            (new Filesystem())->copy($file->getRealPath(), $this->selfSubscriptionModuleLogoSavefileDirectory.'/'.$filename, true);
            $this->entityManager->flush();

            return $this->apiResponse([
                'filetarget' => $filename,
                'previewUri' => $this->selfSubscriptionModuleLogoSavefileDirectory.$filename,
            ]);
        } catch (UploadFileException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/upload/subject/illustration', name: '_module_upload_subject_illustration', options: ['expose' => true], methods: ['POST'])]
    public function uploadSubjectIllustration(Request $request): Response
    {
        try {
            /** @var UploadedFile $file */
            $file = $request->files->get('file');

            $formatParam = $request->request->get('format', implode(',', Module::AUTHORIZED_EXTENSIONS));
            $this->ressourceUploader->isFileUploadedValid($file, explode(',', $formatParam), Module::MAX_FILE_SIZE);

            $filename = \md5(\random_bytes(32)).'.'.$file->getClientOriginalExtension();

            ( new Filesystem() )->copy($file->getRealPath(), $this->selfSubscriptionSubjectIllustrationSavefilesDirectory.'/'.$filename, true);

            return $this->apiResponse([
                'filetarget' => $filename,
                'previewUri' => $this->selfSubscriptionSubjectIllustrationUri.$filename,
            ]);
        } catch (UploadFileException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/add-new-catalog-replay/{id}', name: '_new_catalog_replay', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted(ModuleVoter::EDIT, subject: 'module')]
    public function addNewCatalogReplay(Request $request, Module $module, LoggerInterface $logger): Response
    {
        try {
            $existingCategory = $this->entityManager->getRepository(ReplayCategory::class)->findOneBy(['categoryName' => $request->query->get('catalogueName'), 'module' => $module]);
            if (null === $existingCategory) {
                $categoryReplay = new ReplayCategory();
                $categoryReplay->setCategoryName($request->query->get('categoryName'))
                    ->setModule($module);
                $this->entityManager->persist($categoryReplay);
                $this->entityManager->flush();
            } else {
                return $this->apiErrorResponse('This category already exists, searcher it in the inactive list.');
            }

            return $this->apiResponse(['message' => 'Add new category replay successfully.',
                'categoryReplay' => $this->serializer->serialize(
                    $categoryReplay,
                    'json',
                    ['groups' => 'ui:module:read']
                ), ]);
        } catch (Throwable $exception) {
            $logger->error($exception, [
                'A error when add new category replay with client ID' => $module->getClient()->getId(),
            ]);

            return $this->apiErrorResponse('An error occurred when add new category replay.');
        }
    }

    #[Route('/update-catalog-replay/{id}', name: '_update_catalog_replay', options: ['expose' => true], methods: ['POST'])]
    public function updateCatalogReplay(Request $request, ReplayCategory $replayCategory, LoggerInterface $logger): Response
    {
        try {
            $replayCategory->setCategoryName($request->query->get('categoryName'));
            $this->entityManager->persist($replayCategory);
            $this->entityManager->flush();

            return $this->apiResponse(['message' => 'Update a category replay successfully.',
                'categoryReplay' => $this->serializer->serialize(
                    $replayCategory,
                    'json',
                    ['groups' => 'ui:module:read']
                ), ]);
        } catch (Throwable $exception) {
            $logger->error($exception, [
                'A error when update a catalog replay with replay category ID' => $replayCategory->getId(),
            ]);

            return $this->apiErrorResponse('An error occurred when update category replay.');
        }
    }

    #[Route('/update-status-catalog-replay/{id}', name: '_update_status_catalog_replay', options: ['expose' => true], methods: ['POST'])]
    public function updateStatusCatalogReplay(ReplayCategory $replayCategory, LoggerInterface $logger): Response
    {
        try {
            $replayCategory->setActive(!$replayCategory->isActive());
            $this->entityManager->persist($replayCategory);
            $this->entityManager->flush();

            return $this->apiResponse(['message' => 'Update a status category replay successfully.']);
        } catch (Throwable $exception) {
            $logger->error($exception, [
                'A error when update a catalog replay with replay category ID' => $replayCategory->getId(),
            ]);

            return $this->apiErrorResponse('An error occurred when update status category replay.');
        }
    }

    #[Route('/delete-catalog-replay/{id}', name: '_delete_catalog_replay', options: ['expose' => true], methods: ['POST'])]
    public function deleteCatalogReplay(ReplayCategory $replayCategory, LoggerInterface $logger): Response
    {
        try {
            $videoReplays = $this->videoReplayRepository->findVideosFordModule($replayCategory->getModule()->getId());
            foreach ($videoReplays as $videoReplay) {
                $catalogsReplay = $videoReplay->getCatalogsReplay();
                $catalogsReplay = array_diff($catalogsReplay, [$replayCategory->getId()]);
                $videoReplay->setCatalogsReplay($catalogsReplay);
                $this->entityManager->persist($videoReplay);
            }
            $this->entityManager->remove($replayCategory);
            $this->entityManager->flush();

            return $this->apiResponse(['message' => 'Delete a category replay successfully.']);
        } catch (Throwable $exception) {
            $logger->error($exception, [
                'A error when delete a catalog replay with replay category ID' => $replayCategory->getId(),
            ]);

            return $this->apiErrorResponse('An error occurred when delete category replay.');
        }
    }

    #[Route('/update-order', name: '_update_order_subject', options: ['expose' => true], methods: ['POST'])]
    public function updateOrderSubjectInModule(Request $request): Response
    {
        try {
            $newOrder = $request->request->get('newOrder');
            $subjectId = $request->request->get('idSubject');
            $subject = $this->subjectRepository->findOneBy(['id' => $subjectId]);
            if ($newOrder !== null && $subject !== null) {
                $this->subjectService->updateOrder($subject, $newOrder);
                $this->entityManager->flush();

                return $this->apiResponse('Update order successfully.');
            } else {
                return $this->apiResponse('Missing newOrder parameter.', Response::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $e) {
            return $this->apiResponse('An error occurred: '.$e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    #[Route('/get_registered_user/{id}', name: '_get_registered_user_module', options: ['expose' => true])]
    #[IsGranted(ModuleVoter::ACCESS_HUB, subject: 'module')]
    public function getParticipantForWizard(Module $module): Response
    {
        return $this->apiResponse($this->normalizer->normalize($this->userModuleRepository->findUserModuleByModule($module), context: ['groups' => ['read_user_module']]));
    }
}
