<?php

namespace App\SelfSubscription\Controller;

use App\Entity\VideoReplay;
use App\Model\ApiResponseTrait;
use App\Repository\ProviderSessionRepository;
use App\SelfSubscription\Entity\FaqEntry;
use App\SelfSubscription\Entity\Module;
use App\SelfSubscription\Entity\Subject;
use App\SelfSubscription\Entity\Theme;
use App\SelfSubscription\Repository\FaqEntryRepository;
use App\SelfSubscription\Repository\ModuleRepository;
use App\SelfSubscription\Repository\SubjectRepository;
use App\SelfSubscription\Repository\ThemeRepository;
use App\SelfSubscription\Repository\VideoRepository;
use App\SelfSubscription\Security\ModuleVoter;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/self-subscription/module/{id}/content')]
#[IsGranted('ROLE_MANAGER')]
class ModuleContentController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private ThemeRepository $themeRepository,
        private SubjectRepository $subjectRepository,
        private ProviderSessionRepository $sessionRepository,
        private FaqEntryRepository $faqEntryRepository,
        private VideoRepository $videoRepository,
        private DenormalizerInterface $denormalizer,
        private EntityManagerInterface $entityManager,
        private NormalizerInterface $normalizer,
        private ValidatorInterface $validator,
        private TranslatorInterface $translator,
        private ModuleRepository $moduleRepository,
    ) {
    }

    #[Route('/faq/save', name: '_faq_save', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted(ModuleVoter::EDIT, subject: 'module')]
    public function saveFAQ(Module $module, Request $request): Response
    {
        $request = json_decode($request->getContent(), true);
        $context = [
            AbstractNormalizer::GROUPS => 'ui:faq:update',
            AbstractNormalizer::DEFAULT_CONSTRUCTOR_ARGUMENTS => [
                FaqEntry::class => ['module' => $module],
            ],
        ];
        if (array_key_exists('id', $request)) {
            /** @var FaqEntry $faq */
            $faq = $this->faqEntryRepository->find($request['id']);
            if (empty($faq) || $faq->getModuleId() !== $module->getId()) {
                throw new AccessDeniedException();
            }

            $context[AbstractNormalizer::OBJECT_TO_POPULATE] = $faq;
            $this->denormalizer->denormalize($request, FaqEntry::class, context: $context);
        } else {
            $faq = $this->denormalizer->denormalize($request, FaqEntry::class, context: $context);
        }

        $violations = $this->validator->validate($faq);
        if ($violations->count() === 0) {
            $this->entityManager->persist($faq);
            $this->entityManager->flush();

            return $this->apiResponse($this->normalizer->normalize($faq, context: ['groups' => ['ui:module:read']]));
        } else {
            $extraDatas = [];
            foreach ($violations as $violation) {
                $extraDatas[$violation->getPropertyPath()] = $violation->getMessage();
            }

            return $this->apiErrorResponse($this->translator->trans('Check your data'), extraData: $extraDatas);
        }
    }

    #[Route('/faq/{faqId}/delete', name: '_faq_delete', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ModuleVoter::EDIT, subject: 'module')]
    public function deleteFaq(Module $module, int $faqId): Response
    {
        // I'll prefer to have autowiring but not working because of constructor args...
        /** @var FaqEntry $faq */
        $faq = $this->faqEntryRepository->find($faqId);
        if (empty($faq) || $faq->getModuleId() !== $module->getId()) {
            throw new AccessDeniedException();
        }

        $this->entityManager->remove($faq);
        $this->entityManager->flush();

        return $this->apiResponse([]);
    }

    #[Route('/theme/save', name: '_theme_save', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted(ModuleVoter::EDIT, subject: 'module')]
    public function saveTheme(Module $module, Request $request): Response
    {
        $request = json_decode($request->getContent(), true);
        $context = [
            AbstractNormalizer::GROUPS => 'ui:theme:update',
            AbstractNormalizer::DEFAULT_CONSTRUCTOR_ARGUMENTS => [
                Theme::class => ['module' => $module],
            ],
        ];
        if (array_key_exists('id', $request)) {
            /** @var Theme $theme */
            $theme = $this->themeRepository->find($request['id']);
            if (empty($theme) || $theme->getModuleId() !== $module->getId()) {
                throw new AccessDeniedException();
            }

            $context[AbstractNormalizer::OBJECT_TO_POPULATE] = $theme;
            $this->denormalizer->denormalize($request, Theme::class, context: $context);
        } else {
            $theme = $this->denormalizer->denormalize($request, Theme::class, context: $context);
        }

        $violations = $this->validator->validate($theme);
        if ($violations->count() === 0) {
            $this->entityManager->persist($theme);
            $this->entityManager->flush();

            return $this->apiResponse($this->normalizer->normalize($theme, context: ['groups' => ['ui:module:read']]));
        } else {
            $extraDatas = [];
            foreach ($violations as $violation) {
                $extraDatas[$violation->getPropertyPath()] = $violation->getMessage();
            }

            return $this->apiErrorResponse($this->translator->trans('Check your data'), extraData: $extraDatas);
        }
    }

    #[Route('/theme/{themeId}/delete', name: '_theme_delete', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ModuleVoter::EDIT, subject: 'module')]
    public function deleteTheme(Module $module, int $themeId): Response
    {
        // I'll prefer to have autowiring but not working because of constructor args...
        /** @var Theme $theme */
        $theme = $this->themeRepository->find($themeId);
        if (empty($theme) || $theme->getModuleId() !== $module->getId()) {
            throw new AccessDeniedException();
        }

        $this->subjectRepository->unlinkSessions(theme: $theme);
        $this->entityManager->remove($theme);
        $this->entityManager->flush();

        return $this->apiResponse([]);
    }

    #[Route('/theme/{themeId}/subject/save', name: '_subject_save', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted(ModuleVoter::EDIT, subject: 'module')]
    public function saveSubject(Module $module, int $themeId, Request $request): Response
    {
        // I'll prefer to have autowiring but not working because of constructor args...
        /** @var Theme $theme */
        $theme = $this->themeRepository->find($themeId);

        $request = json_decode($request->getContent(), true);
        $request['categorySession'] = (int) $request['categorySession'];
        $request['duration'] = (int) $request['duration'];
        $context = [
            AbstractNormalizer::GROUPS => 'ui:subject:update',
            AbstractNormalizer::DEFAULT_CONSTRUCTOR_ARGUMENTS => [
                Subject::class => ['theme' => $theme],
            ],
        ];
        if (array_key_exists('id', $request)) {
            /** @var Subject $subject */
            $subject = $this->subjectRepository->find($request['id']);
            if (empty($subject) || $subject->getThemeId() !== $theme->getId() || $theme->getModuleId() !== $module->getId()) {
                throw new AccessDeniedException();
            }

            $context[AbstractNormalizer::OBJECT_TO_POPULATE] = $subject;
            $this->denormalizer->denormalize($request, Subject::class, context: $context);

            $videoContext = [
                AbstractNormalizer::GROUPS => 'ui:video:replay:update',
                AbstractNormalizer::DEFAULT_CONSTRUCTOR_ARGUMENTS => [
                    VideoReplay::class => ['module' => $module, 'subject' => $subject],
                ],
            ];
            $videoContext[AbstractNormalizer::OBJECT_TO_POPULATE] = $subject->getVideoReplay();
            $this->denormalizer->denormalize($request['videoReplay'], VideoReplay::class, context: $videoContext);
        } else {
            $subject = $this->denormalizer->denormalize($request, Subject::class, context: $context);
        }

        $violations = $this->validator->validate($subject);
        if ($violations->count() === 0) {
            $this->entityManager->persist($subject);
            $this->entityManager->flush();

            return $this->apiResponse($this->normalizer->normalize($subject, context: ['groups' => ['ui:module:read']]));
        } else {
            $extraDatas = [];
            foreach ($violations as $violation) {
                $extraDatas[$violation->getPropertyPath()] = $violation->getMessage();
            }

            return $this->apiErrorResponse($this->translator->trans('Check your data'), extraData: $extraDatas);
        }
    }

    #[Route('/theme/{themeId}/subject/{subjectId}/delete', name: '_subject_delete', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ModuleVoter::EDIT, subject: 'module')]
    public function deleteSubject(Module $module, int $themeId, int $subjectId): Response
    {
        // I'll prefer to have autowiring but not working because of constructor args...
        /** @var Theme $theme */
        $theme = $this->themeRepository->find($themeId);
        /** @var Subject $subject */
        $subject = $this->subjectRepository->find($subjectId);

        if (empty($theme) || $theme->getId() !== $subject->getThemeId() || $theme->getModuleId() !== $module->getId()) {
            throw new AccessDeniedException();
        }

        $this->subjectRepository->unlinkSessions(subject: $subject);
        $this->entityManager->remove($subject);
        $this->entityManager->flush();

        return $this->apiResponse([]);
    }

    #[Route('/theme/{themeId}/subject/{subjectId}', name: '_module_create_or_edit_subject', requirements: ['subjectId' => '\d+|'], options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ModuleVoter::EDIT, subject: 'module')]
    public function createOrEditModuleSubject(Module $module, int $themeId, ?string $subjectId, Request $request): Response
    {
        /** @var Theme $theme */
        $theme = $this->themeRepository->find($themeId);

        if (!$subjectId) {
            $subject = new Subject($theme);
            $subject->setOrderInModule($this->moduleRepository->countAllSubjectsFromModule($module) + 1);
        } else {
            $subject = $this->subjectRepository->find($subjectId);
        }

        if (empty($theme) || $theme->getId() !== $subject->getThemeId() || $theme->getModuleId() !== $module->getId()) {
            throw new AccessDeniedException();
        }

        return $this->render('self-subscription/module/edit_subject.html.twig', [
            'module' => $module,
            'subject' => $subject,
            'theme' => $theme,
        ]);
    }

    #[Route('/video/save', name: '_video_save', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted(ModuleVoter::EDIT, subject: 'module')]
    public function saveVideo(Module $module, Request $request): Response
    {
        $request = json_decode($request->getContent(), true);
        $context = [
            AbstractNormalizer::GROUPS => 'ui:video:replay:update',
            AbstractNormalizer::DEFAULT_CONSTRUCTOR_ARGUMENTS => [
                VideoReplay::class => ['module' => $module],
            ],
        ];

        if (array_key_exists('id', $request)) {
            /** @var VideoReplay $video */
            $video = $this->videoRepository->find($request['id']);
            if (empty($video) || $video->getModuleId() !== $module->getId()) {
                throw new AccessDeniedException();
            }

            $context[AbstractNormalizer::OBJECT_TO_POPULATE] = $video;
            $this->denormalizer->denormalize($request, VideoReplay::class, context: $context);
            $video->setCatalogsReplay($request['catalogsReplay']);

            if (array_key_exists('subjectId', $request) && $request['subjectId']) {
                /** @var Subject $subject */
                $subject = $this->subjectRepository->find($request['subjectId']);
                if (empty($subject) || $subject->getModuleId() !== $video->getModuleId()) {
                    throw new AccessDeniedException();
                }

                if ($video->getPublished() && $subject->hasFutureSessions()) {
                    return $this->apiErrorResponse($this->translator->trans('You cannot publish a video if the subject has future sessions'));
                }
            }
        } else {
            $video = $this->denormalizer->denormalize($request, VideoReplay::class, context: $context);
            $video->setCatalogsReplay($request['catalogsReplay']);
            $video->setModule($module);

            if (array_key_exists('subjectId', $request) && $request['subjectId']) {
                /** @var Subject $subject */
                $subject = $this->subjectRepository->find($request['subjectId']);
                if (empty($subject) || $subject->getModuleId() !== $video->getModuleId()) {
                    throw new AccessDeniedException();
                }
                $video->setSubject($subject);
            }
        }

        $violations = $this->validator->validate($video);
        if ($violations->count() === 0) {
            $this->entityManager->persist($video);
            $this->entityManager->flush();

            return $this->apiResponse($this->normalizer->normalize($video, context: ['groups' => ['ui:video:replay:update']]));
        } else {
            $extraDatas = [];
            foreach ($violations as $violation) {
                $extraDatas[$violation->getPropertyPath()] = $violation->getMessage();
            }

            return $this->apiErrorResponse($this->translator->trans('Check your data'), extraData: $extraDatas);
        }
    }

    #[Route('/video/{videoId}/delete', name: '_video_delete', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(ModuleVoter::EDIT, subject: 'module')]
    public function deleteVideo(Module $module, int $videoId): Response
    {
        // I'll prefer to have autowiring but not working because of constructor args...
        /** @var VideoReplay $video */
        $video = $this->videoRepository->find($videoId);
        if (empty($video) || $video->getModuleId() !== $module->getId()) {
            throw new AccessDeniedException();
        }
        foreach ($video->getVideosReplaysViewers() as $videoReplaysViewer) {
            $this->entityManager->remove($videoReplaysViewer);
        }
        $this->entityManager->remove($video);
        $this->entityManager->flush();

        return $this->apiResponse([]);
    }
}
