<?php

namespace App\SelfSubscription\Controller;

use App\Model\ApiResponseTrait;
use App\SelfSubscription\Entity\BlackListEntry;
use App\SelfSubscription\Entity\Module;
use App\SelfSubscription\Security\ModuleVoter;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/self-subscription/module/{id}/blacklist')]
#[IsGranted(ModuleVoter::EDIT, subject: 'module')]
class BlackListController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger,
        private SerializerInterface $serializer,
        private TranslatorInterface $translator
    ) {
    }

    #[Route('/add', name: '_new_prohibited_black_list', options: ['expose' => true], methods: ['POST'])]
    public function addNewProhibitedInBlackList(Request $request, Module $module): Response
    {
        try {
            if (!$request->query->has('prohibitedMail')) {
                return $this->apiErrorResponse($this->translator->trans('The field of the email form cannot be empty'));
            }
            $module->addBlackListEntry($request->query->get('prohibitedMail'));
            $this->entityManager->flush();

            return new Response($this->serializer->serialize(
                $module,
                'json',
                ['groups' => 'ui:module:read']
            ));
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/{blacklistId}/remove', name: '_delete_prohibited_black_list', options: ['expose' => true], methods: ['POST'])]
    #[ParamConverter('blackList', options: ['mapping' => ['blacklistId' => 'id']])]
    public function deleteProhibitedInBlackList(Module $module, BlackListEntry $blackList): Response
    {
        try {
            if ($module !== $blackList->getModule()) {
                $this->logger->error(new \Exception('BlackListEntry does not belongs to given Module, maybe someone is playing with url'));

                return $this->apiErrorResponse('An unknown error occurred during delete a prohibited in black List');
            }

            $module->removeBlackListEntry($blackList);
            $this->entityManager->remove($blackList);
            $this->entityManager->flush();

            return new Response($this->serializer->serialize(
                $module,
                'json',
                ['groups' => 'ui:module:read']
            ));
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }
}
