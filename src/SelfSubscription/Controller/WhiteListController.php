<?php

namespace App\SelfSubscription\Controller;

use App\Model\ApiResponseTrait;
use App\SelfSubscription\Entity\Module;
use App\SelfSubscription\Entity\WhiteListEntry;
use App\SelfSubscription\Security\ModuleVoter;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/self-subscription/module/{id}/whitelist')]
#[IsGranted(ModuleVoter::EDIT, subject: 'module')]
class WhiteListController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger,
        private SerializerInterface $serializer,
        private TranslatorInterface $translator
    ) {
    }

    #[Route('/add', name: '_new_allowed_white_list', options: ['expose' => true], methods: ['POST'])]
    public function addNewAllowedInWhiteList(Request $request, Module $module): Response
    {
        try {
            if (!$request->query->has('allowedMail')) {
                return $this->apiErrorResponse($this->translator->trans('The field of the email form cannot be empty'));
            }
            $module->addWhiteListEntry($request->query->get('allowedMail'));
            $this->entityManager->flush();

            return new Response($this->serializer->serialize(
                $module,
                'json',
                ['groups' => 'ui:module:read']
            ));
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/{whitelistId}/remove', name: '_delete_allowed_white_list', options: ['expose' => true], methods: ['POST'])]
    #[ParamConverter('whiteList', options: ['mapping' => ['whitelistId' => 'id']])]
    public function deleteAllowedInWhiteList(Module $module, WhiteListEntry $whiteList): Response
    {
        try {
            if ($module !== $whiteList->getModule()) {
                $this->logger->error(new \Exception('WhiteList does not belongs to given Module, maybe someone is playing with url'));

                return $this->apiErrorResponse('An unknown error occurred during delete a allowed in white List');
            }
            $module->removeWhiteListEntry($whiteList);

            $this->entityManager->remove($whiteList);
            $this->entityManager->flush();

            return new Response($this->serializer->serialize(
                $module,
                'json',
                ['groups' => 'ui:module:read']
            ));
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }
}
