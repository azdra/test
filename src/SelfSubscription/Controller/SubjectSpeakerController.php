<?php

namespace App\SelfSubscription\Controller;

use App\Exception\UploadFile\UploadFileException;
use App\Model\ActivityLog\ActivityLogTrait;
use App\Model\ApiResponseTrait;
use App\Model\ValidatorTrait;
use App\SelfSubscription\Entity\SubjectSpeaker;
use App\SelfSubscription\Repository\SubjectRepository;
use App\SelfSubscription\Repository\SubjectSpeakerRepository;
use App\Service\ActivityLogger;
use App\Service\ActivityLogService;
use App\Service\RessourceUploader\RessourceUploader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/subject-speaker')]
class SubjectSpeakerController extends AbstractController
{
    use ApiResponseTrait;
    use ActivityLogTrait;
    use ValidatorTrait;

    public function __construct(
        private SubjectSpeakerRepository $subjectSpeakerRepository,
        private NormalizerInterface $normalizer,
        private LoggerInterface $logger,
        private SerializerInterface $serializer,
        private EntityManagerInterface $entityManager,
        private RessourceUploader $ressourceUploader,
        private $selfSubscriptionSubjectSpeakerSavefilesDirectory,
        private SubjectRepository $subjectRepository,
        private ActivityLogService $activityLogService,
        private ActivityLogger $activityLogger,
    ) {
    }

    #[Route('/upload/files', name: '_subject_speaker_upload_files', options: ['expose' => true], methods: ['POST'])]
    public function uploadFile(Request $request): Response
    {
        try {
            /** @var UploadedFile $file */
            $file = $request->files->get('file');

            $formatParam = $request->request->get('format', implode(',', SubjectSpeaker::AUTHORIZED_EXTENSIONS));
            $this->ressourceUploader->isFileUploadedValid($file, explode(',', $formatParam), SubjectSpeaker::MAX_FILE_SIZE);

            $filename = \md5(\random_bytes(32)).'.'.$file->getClientOriginalExtension();

            (new Filesystem())->copy($file->getRealPath(), $this->selfSubscriptionSubjectSpeakerSavefilesDirectory.'/'.$filename, true);

            return $this->apiResponse([
                'filetarget' => $filename,
                'previewUri' => $this->selfSubscriptionSubjectSpeakerSavefilesDirectory.$filename,
            ]);
        } catch (UploadFileException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/remove/{id}', name: '_subject_speaker_remove', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['POST'])]
    public function remove(Request $request, SubjectSpeaker $subjectSpeaker): Response
    {
        try {
            $subject = $subjectSpeaker->getSubject();
            $subject->removeSpeaker($subjectSpeaker);
            $this->entityManager->remove($subjectSpeaker);
            $this->entityManager->flush();

            return $this->apiResponse($this->normalizer->normalize([$subject->getSpeakers()], context: ['groups' => ['ui:edit:subject']]));
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'ID subject' => $subject->getId(),
            ]);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/save/{id}', name: '_subject_speaker_save', requirements: ['id' => '\d+'], options: ['expose' => true], methods: ['POST'])]
    public function save(Request $request, int $id): Response
    {
        try {
            $subject = $this->subjectRepository->findOneBy(['id' => $id]);
            $data = json_decode($request->getContent(), true);
            $speaker = new SubjectSpeaker();

            if (array_key_exists('speakerId', $data) && !is_null($data['speakerId'])) {
                /** @var SubjectSpeaker $speaker */
                $speaker = $this->subjectSpeakerRepository->findOneBy(['id' => $data['speakerId']]);
                if (!empty($speaker->getPhoto()) && empty($data['speakerPhoto'])) {
                    unlink($this->selfSubscriptionSubjectSpeakerSavefilesDirectory.'/'.$speaker->getPhoto());
                }
            }
            $speaker->setSubject($subject);
            $speaker->setName($data['speakerName']);
            $speaker->setInformation($data['speakerInformation']);
            $speaker->setPhoto($data['speakerPhoto']);
            $speaker->setTitle($data['speakerTitle']);
            $subject->addSpeaker($speaker);
            $this->entityManager->persist($subject);
            $this->entityManager->flush();

            return $this->apiResponse([
                'speakerId' => $speaker->getId(),
                'speakerName' => $speaker->getName(),
                'speakerInformation' => $speaker->getInformation(),
                'speakerPhoto' => $speaker->getPhoto(),
                'speakers' => $this->normalizer->normalize([$subject->getSpeakers()], context: ['groups' => ['ui:edit:subject']]),
            ]);
        } catch (\Exception $exception) {
            $this->logger->error($exception, [
                'ID subject' => $subject->getId(),
            ]);
            $this->addFlash('error', 'An unknown error occurred during subject validation.');

            return $this->apiErrorResponse($exception->getMessage());
        }
    }
}
