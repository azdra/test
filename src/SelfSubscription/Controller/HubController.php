<?php

namespace App\SelfSubscription\Controller;

use App\Entity\ActivityLog;
use App\Entity\CategorySessionType;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Entity\UserModule;
use App\Event\TransactionalEmailRequestEventInterface;
use App\Event\TransactionalParticipantEmailRequestEvent;
use App\Exception\UploadFile\UploadFileException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Model\ApiResponseTrait;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserModuleRepository;
use App\Repository\UserRepository;
use App\Security\LoginFormAuthenticator;
use App\Security\UserSecurityService;
use App\SelfSubscription\Entity\Module;
use App\SelfSubscription\Entity\Subject;
use App\SelfSubscription\Repository\SubjectRepository;
use App\SelfSubscription\Security\ModuleVoter;
use App\SelfSubscription\Service\ModuleService;
use App\SelfSubscription\Service\SubjectService;
use App\Service\ActivityLogger;
use App\Service\ConvocationService;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\RessourceUploader\RessourceUploader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/hub/{clientName}/{domain}')]
class HubController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private LoginFormAuthenticator $authenticator,
        private UserPasswordHasherInterface $passwordHasher,
        private ProviderSessionRepository $sessionRepository,
        private SubjectRepository $subjectRepository,
        private ParticipantSessionSubscriber $participantSessionSubscriber,
        private TranslatorInterface $translator,
        private ActivityLogger $activityLogger,
        private LoggerInterface $logger,
        private NormalizerInterface $normalizer,
        private EntityManagerInterface $entityManager,
        private SubjectService $subjectService,
        private ProviderParticipantSessionRoleRepository $sessionRoleRepository,
        private ConvocationService $convocationService,
        private EventDispatcherInterface $eventDispatcher,
        private readonly UserRepository $userRepository,
        private readonly UserModuleRepository $userModuleRepository,
        private readonly UserSecurityService $userSecurityService,
        private RouterInterface $router,
        private ModuleService $moduleService
    ) {
    }

    #[Route('/register', name: '_self_subscription_hub_sign_up', options: ['expose' => true])]
    public function register(Request $request, Module $module): Response
    {
        return $this->render('self-subscription/hub/register.html.twig', [
            'module' => $module,
        ]);
    }

    #[Route('/login', name: '_self_subscription_hub_login', options: ['expose' => true])]
    public function login(Request $request, Module $module): Response
    {
        if ($request->getSession()->has(Security::AUTHENTICATION_ERROR) &&
            $request->getSession()->get(Security::AUTHENTICATION_ERROR) instanceof BadCredentialsException
        ) {
            $error = $this->translator->trans('Your email or password is not valid');
            $request->getSession()->remove(Security::AUTHENTICATION_ERROR);
        }

        return $this->render('self-subscription/hub/login.html.twig', [
            'module' => $module,
            'error' => $error ?? '',
            'passwordRequest' => $request->query->get('password_requested'),
            'resettingPassword' => (bool) $request->query->get('resetting'),
        ]);
    }

    #[Route('/reset-password', name: '_self_subscription_hub_reset_password', options: ['expose' => true])]
    public function resettingPassword(Request $request, Module $module): Response
    {
        $user = false;
        if ($request->query->has('reset-token')) {
            $user = $this->userRepository->findOneBy(['passwordResetToken' => $request->query->get('reset-token')]);
            if ($user && $this->userSecurityService->checkLastPasswordReset($user)) {
                $user = false;
            }
        }

        return $this->render('self-subscription/hub/reset_password.html.twig', [
            'module' => $module,
            'resetToken' => $request->query->get('reset-token'),
            'invalidResetToken' => $request->query->get('reset-token') && !$user,
            'haveManagerAccounts' => $request->query->get('reset-token') && $user && ($user->isAnAdmin() || $user->isAManager()) && count($this->userRepository->findAllUsersManagerAccounts($user)) >= 2,
        ]);
    }

    #[Route('/embed', name: '_self_subscription_hub_home_embed', options: ['expose' => true])]
    public function embedHome(Module $module, Request $request, string $selfSubscriptionModuleLogoUri, string $selfSubscriptionSubjectIllustrationUri): Response
    {
        if (!$module->isPublic()) {
            throw new AccessDeniedException();
        }

        $user = (new User())
            ->setId(User::ID_USER_GUEST)
            ->setLastName($this->translator->trans('Guest'))
            ->setFirstName($this->translator->trans('Log in'))
            ->setEmail('guest.user@domain.com')
            ->setRoles(['ROLE_TRAINEE']);

        $request->setLocale($module->getLang());

        $pageAccess = [
            'Search' => true,
            'Replay' => $module->getPersonalization()->isEnabledReplay(),
            'Current' => $module->getPersonalization()->isEnabledSessions(),
            'Previous' => $module->getPersonalization()->isEnabledSessions(),
            'FAQ' => $module->getPersonalization()->isEnabledFaq(),
        ];

        $pageKey = $request->query->get('page') ?? 'Search';
        $validPages = array_keys($pageAccess);
        $page = $pageKey && in_array($pageKey, $validPages) && array_key_exists($pageKey, $pageAccess) && $pageAccess[$pageKey] ? $pageKey : 'Search';

        return $this->render(
            'self-subscription/hub/home.html.twig',
            [
                'module' => $module,
                'error' => null,
                'subjects' => $this->subjectRepository->findAllFromModule($module),
                'allUserSubscriptions' => $this->sessionRepository->getAllSubscribedSessionOfModule($module, $user),
                'user' => $user,
                'selfSubscriptionModuleLogoUri' => $selfSubscriptionModuleLogoUri,
                'selfSubscriptionModuleSubjectIllustration' => $selfSubscriptionSubjectIllustrationUri,
                'page' => $page,
                'isEmbed' => true,
                'subject' => null,
            ]
        );
    }

    #[Route('/subscribe/{id}', name: '_self_subscription_hub_subscription', options: ['expose' => true], methods: ['GET'])]
    #[ParamConverter('module', options: ['mapping' => ['domain' => 'domain']])]
    #[IsGranted(ModuleVoter::ACCESS_HUB, 'module')]
    public function userSelfSubscription(Module $module, ProviderSession $session): Response
    {
        try {
            if ($session->getSubscriptionMax() !== 0 && $session->getTraineesCount() >= $session->getSubscriptionMax()) {
                return $this->apiErrorResponse($this->translator->trans('The session is full.', domain: 'self-subscription'));
            }

            if (null === $session->getSubject() && $session->getSubject()->getModuleId() === $module->getId()) {
                return $this->apiErrorResponse($this->translator->trans('You can not subscribe to this subject.', domain: 'self-subscription'));
            }

            $this->subjectService->unsubscribeUserFromSubject($this->getUser(), $session->getSubject());

            $this->entityManager->flush();

            $this->moduleService->subscribe($session, $this->getUser());

            return $this->apiResponse($this->normalizer->normalize($session, context: ['groups' => 'read_session']));
        } catch (Throwable $t) {
            $this->logger->error($t, [
                'class' => get_class($t),
                'sessionId' => $session->getId(),
                'loggedUserId' => $this->getUser()->getId(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An unknown error occurred while subscribing you to the session.', domain: 'self-subscription'));
        }
    }

    #[Route('/unsubscribe/{subjectId}', name: '_self_subscription_hub_unsubscribe', options: ['expose' => true], methods: ['GET'])]
    #[ParamConverter('module', options: ['mapping' => ['domain' => 'domain']])]
    #[IsGranted(ModuleVoter::ACCESS_HUB, 'module')]
    public function userSelfUnsubscription(Module $module, int $subjectId): Response
    {
        try {
            // I'll prefer to have autowiring but not working because of constructor args...
            /** @var Subject $subject */
            $subject = $this->subjectRepository->find($subjectId);

            if (null === $subject || $subject->getModuleId() !== $module->getId()) {
                return $this->apiErrorResponse($this->translator->trans('You can not subscribe to this subject.', domain: 'self-subscription'));
            }

            $this->subjectService->unsubscribeUserFromSubject($this->getUser(), $subject);

            $participantSessionRoles = $this->subjectRepository->findCurrentSessionOfSubjectForUser($this->getUser(), $subject);
            $this->activityLogger->clearLogModels();

            $this->activityLogger->initActivityLogContext($this->getUser()->getClient(), $this->getUser(), ActivityLog::ORIGIN_USER_INTERFACE);
            /** @var ProviderParticipantSessionRole $participantSessionRole */
            foreach ($participantSessionRoles as $participantSessionRole) {
                $session = $participantSessionRole->getSession();
                $infos = [
                    'message' => [
                        'key' => 'The participant <a href="%participantRoute%">%participantName%</a> has unsubscribed from the session <a href="%sessionRoute%">%sessionName%</a> via the catalog <a href="%catalogRoute%">%catalogName%</a>',
                        'params' => [
                            '%participantRoute%' => $this->router->generate('_participant_edit', ['id' => $this->getUser()->getId()]),
                            '%participantName%' => "{$this->getUser()->getFirstName()} {$this->getUser()->getLastName()}",
                            '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                            '%sessionName%' => $session->getName(),
                            '%catalogRoute%' => $this->router->generate('_module_edit', ['id' => $module->getId()]),
                            '%catalogName%' => $module->getName(),
                        ],
                    ],
                    'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                ];
                $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_NO_ANIMATOR_WITH_HOST_ON_SESSION, ActivityLog::SEVERITY_INFORMATION, $infos));
            }
            $this->activityLogger->flushActivityLogs();
            $this->entityManager->flush();

            return $this->apiResponse([]);
        } catch (Throwable $t) {
            $this->logger->error($t, [
                'class' => get_class($t),
                'subjectId' => $subject->getId(),
                'loggedUserId' => $this->getUser()->getId(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An unknown error occurred while unsubscribing.', domain: 'self-subscription'));
        }
    }

    #[Route('/session/{id}/convocation/preview', name: '_self_subscription_hub_session_preview_convocation', options: ['expose' => true])]
    #[ParamConverter('module', options: ['mapping' => ['domain' => 'domain']])]
    #[IsGranted(ModuleVoter::ACCESS_HUB, 'module')]
    public function previewConvocation(Module $module, ProviderSession $providerSession): Response
    {
        if (null === $providerSession->getSubject() || $providerSession->getSubject()->getModuleId() !== $module->getId()) {
            return $this->apiErrorResponse($this->translator->trans('You are not allowed to do this action', domain: 'self-subscription'));
        }

        $participantSessionRole = $this->sessionRoleRepository->findBySessionAndUser($providerSession, $this->getUser());

        $convocation = $participantSessionRole->getSession()->getConvocation();

        return $this->apiResponse([
            'content' => $this->convocationService->transformShortsCodes($convocation->getContent(), $participantSessionRole),
        ]);
    }

    #[Route('/session/get_token/{id}', name: '_self_subscription_hub_get_token', options: ['expose' => true])]
    #[ParamConverter('module', options: ['mapping' => ['domain' => 'domain']])]
    #[IsGranted(ModuleVoter::ACCESS_HUB, 'module')]
    public function getTokenForSession(Module $module, ProviderSession $providerSession, $endPointDomain): Response
    {
        if (null === $providerSession->getSubject() || $providerSession->getSubject()->getModuleId() !== $module->getId()) {
            return $this->apiErrorResponse($this->translator->trans('You are not allowed to do this action', domain: 'self-subscription'));
        }

        $participantSessionRole = $this->sessionRoleRepository->findBySessionAndUser($providerSession, $this->getUser());

        return $this->apiResponse([
            'token' => $participantSessionRole->getProviderSessionAccessToken()->getToken(),
            'endPointDomain' => $endPointDomain,
        ]);
    }

    #[Route('/session/{id}/convocation/send', name: '_self_subscription_hub_session_send_convocation', options: ['expose' => true])]
    #[ParamConverter('module', options: ['mapping' => ['domain' => 'domain']])]
    #[IsGranted(ModuleVoter::ACCESS_HUB, 'module')]
    public function sendConvocation(Module $module, ProviderSession $providerSession): Response
    {
        if (null === $providerSession->getSubject() || $providerSession->getSubject()->getModuleId() !== $module->getId()) {
            return $this->apiErrorResponse($this->translator->trans('You are not allowed to do this action', domain: 'self-subscription'));
        }

        $participantSessionRole = $this->sessionRoleRepository->findBySessionAndUser($providerSession, $this->getUser());

        if ($providerSession->getCategory() == CategorySessionType::CATEGORY_SESSION_WEBINAR) {
            $this->eventDispatcher->dispatch(
                new TransactionalParticipantEmailRequestEvent($participantSessionRole, $this->getUser(), true),
                TransactionalEmailRequestEventInterface::COMMUNICATION_ONE_PARTICIPANT
            );
        } else {
            $this->eventDispatcher->dispatch(
                new TransactionalParticipantEmailRequestEvent($participantSessionRole, $this->getUser(), true),
                TransactionalEmailRequestEventInterface::CONVOCATION_ONE_PARTICIPANT
            );
        }

        return $this->apiResponse([]);
    }

    #[Route('/register-new-trainee', name: '_self_subscription_hub_register', options: ['expose' => true], methods: ['POST'])]
    public function registerTrainee(
        Request $request,
        Module $module,
        UserAuthenticatorInterface $authenticatorManager
    ): Response {
        try {
            if (!$request->request->has('register-lastname') ||
                !$request->request->has('register-firstname') ||
                !$request->request->has('register-email') ||
                !$request->request->has('register-password') ||
                !$module->isEmailAllowed($request->request->get('register-email'))
            ) {
                throw new \InvalidArgumentException('Incomplete registration form values');
            }

            $email = $request->request->get('register-email');
            $phone = $request->request->get('register-phone');
            $client = $module->getClient();

            /** @var ?User $existingUser */
            $existingUser = $this->userRepository->findOneBy([
                'email' => $email,
                'client' => $client,
            ]);

            $existingUserModule = $this->userModuleRepository->findOneBy(['user' => $existingUser, 'module' => $module]);

            if ($request->request->has('register-form-responses')) {
                $registrationFormResponsesJson = $request->request->get('register-form-responses');
                $registrationFormResponses = array_filter(json_decode($registrationFormResponsesJson, true), function ($item) {
                    return !in_array($item['type'], User::TYPES_TO_EXCLUDE);
                });
            } else {
                $registrationFormResponses = [];
            }

            if ($existingUser !== null) {
                if ($existingUserModule !== null) {
                    if (!$existingUser->isActive()) {
                        return $this->apiErrorResponse(
                            $this->translator->trans('The email address entered is attached to an account that is no longer active in the application. Please contact us for any reactivation request.')
                        );
                    } else {
                        return $this->apiErrorResponse(
                            $this->translator->trans('The email address entered is already used for a user account. <a href="%resetUrl%">Do you want to reset your password ?</a>', ['%resetUrl%' => $this->generateUrl('_self_subscription_hub_password_reset_request', ['clientName' => $module->getSlugClient(), 'domain' => $module->getDomain(), 'username' => $email])]),
                        );
                    }
                } else {
                    $userModule = (new UserModule($existingUser, $module))
                        ->setCreatedAt(new \DateTime())
                        ->setLastAuthenticationAt(new \DateTime())
                        ->setPassword($this->passwordHasher->hashPassword($existingUser, $request->request->get('register-password')))
                        ->setRegistrationFormCatalogResponses($registrationFormResponses);

                    $this->entityManager->persist($userModule);
                    $this->entityManager->flush();

                    $rememberMe = new RememberMeBadge();
                    $rememberMe->enable();
                    $authenticatorManager->authenticateUser($existingUser, $this->authenticator, $request, [$rememberMe]);

                    return $this->apiResponse('Your account has been created. You can now login.');
                }
            }

            $user = (new User())
                ->setLastName($request->request->get('register-lastname'))
                ->setFirstName($request->request->get('register-firstname'))
                ->setCompany($request->request->get('register-company'))
                ->setEmail($email)
                ->setPhone($phone)
                ->setClient($client)
                ->setPreferredLang($client->getDefaultLang())
                ->setClearPassword($request->request->get('register-password'))
                ->setRegistrationFormCatalogResponses($registrationFormResponses)
                ->setStatus(User::STATUS_ACTIVE);

            $userModule = (new UserModule($user, $module))
                ->setCreatedAt(new \DateTime())
                ->setLastAuthenticationAt(new \DateTime())
                ->setPassword($this->passwordHasher->hashPassword($user, $request->request->get('register-password')))
                ->setRegistrationFormCatalogResponses($registrationFormResponses);

            $this->entityManager->persist($user);
            $this->entityManager->persist($userModule);
            $this->entityManager->flush();

            $rememberMe = new RememberMeBadge();
            $rememberMe->enable();
            $authenticatorManager->authenticateUser($user, $this->authenticator, $request, [$rememberMe]);

            return $this->apiResponse('Your account has been created. You can now login.');
        } catch (\Exception $exception) {
            $this->logger->critical($exception, [
                'client' => $module->getClient()->getName(),
                'module' => $module->getName(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An error has occurred. The registration could not be completed'), extraData: $exception->getMessage());
        }
    }

    #[Route('/register-new-trainee-public', name: '_self_subscription_hub_register_new_participant', options: ['expose' => true], methods: ['POST'])]
    public function registerTraineePublic(
        Request $request,
        Module $module,
        UserAuthenticatorInterface $authenticatorManager,
        ProviderSessionRepository $providerSessionRepository
    ): Response {
        try {
            if (!$request->request->has('register-lastname') ||
                !$request->request->has('register-firstname') ||
                !$request->request->has('register-email') ||
                !$request->request->has('register-password') ||
                !$module->isEmailAllowed($request->request->get('register-email'))
            ) {
                throw new \InvalidArgumentException('Incomplete registration form values');
            }
            $email = $request->request->get('register-email');
            $phone = $request->request->get('register-phone');
            $session = $providerSessionRepository->findOneBy(['id' => $request->request->get('sessionId')]);

            $client = $module->getClient();

            /** @var ?User $existingUser */
            $existingUser = $this->userRepository->findOneBy([
                'email' => $email,
                'client' => $client,
            ]);

            $existingUserModule = $this->userModuleRepository->findOneBy(['user' => $existingUser, 'module' => $module]);

            if ($request->request->has('register-form-responses')) {
                $registrationFormResponsesJson = $request->request->get('register-form-responses');
                $registrationFormResponses = array_filter(json_decode($registrationFormResponsesJson, true), function ($item) {
                    return !in_array($item['type'], User::TYPES_TO_EXCLUDE);
                });
            } else {
                $registrationFormResponses = [];
            }

            if ($existingUser !== null) {
                if ($existingUserModule !== null) {
                    if (!$existingUser->isActive()) {
                        return $this->apiErrorResponse(
                            $this->translator->trans('The email address entered is attached to an account that is no longer active in the application. Please contact us for any reactivation request.')
                        );
                    } else {
                        return $this->apiErrorResponse(
                            $this->translator->trans('The email address entered is already used for a user account. <a href="%resetUrl%">Do you want to reset your password ?</a>', ['%resetUrl%' => $this->generateUrl('_self_subscription_hub_password_reset_request', ['clientName' => $module->getSlugClient(), 'domain' => $module->getDomain(), 'username' => $email])]),
                        );
                    }
                } else {
                    $userModule = (new UserModule($existingUser, $module))
                        ->setCreatedAt(new \DateTime())
                        ->setLastAuthenticationAt(new \DateTime())
                        ->setPassword($this->passwordHasher->hashPassword($existingUser, $request->request->get('register-password')))
                        ->setRegistrationFormCatalogResponses($registrationFormResponses);

                    $this->entityManager->persist($userModule);
                    $this->entityManager->flush();
                    $user = $existingUser;
                }
            } else {
                $user = (new User())
                    ->setLastName($request->request->get('register-lastname'))
                    ->setFirstName($request->request->get('register-firstname'))
                    ->setCompany($request->request->get('register-company'))
                    ->setEmail($email)
                    ->setClient($client)
                    ->setPhone($phone)
                    ->setRegistrationFormCatalogResponses($registrationFormResponses)
                    ->setPreferredLang($client->getDefaultLang())
                    ->setClearPassword($request->request->get('register-password'))
                    ->setStatus(User::STATUS_ACTIVE);

                $userModule = (new UserModule($user, $module))
                    ->setCreatedAt(new \DateTime())
                    ->setLastAuthenticationAt(new \DateTime())
                    ->setPassword($this->passwordHasher->hashPassword($user, $request->request->get('register-password')))
                    ->setRegistrationFormCatalogResponses($registrationFormResponses);

                $this->entityManager->persist($user);
                $this->entityManager->persist($userModule);
                $this->entityManager->flush();
            }

            if ($session->getSubscriptionMax() !== 0 && $session->getTraineesCount() >= $session->getSubscriptionMax()) {
                return $this->apiErrorResponse($this->translator->trans('The session is full.', domain: 'self-subscription'));
            }

            if (null === $session->getSubject() && $session->getSubject()->getModuleId() === $module->getId()) {
                return $this->apiErrorResponse($this->translator->trans('You can not subscribe to this subject.', domain: 'self-subscription'));
            }

            $this->subjectService->unsubscribeUserFromSubject($user, $session->getSubject());

            $this->entityManager->flush();

            $this->participantSessionSubscriber->subscribe(
                $session,
                $user,
                ProviderParticipantSessionRole::ROLE_TRAINEE,
                $session->isSessionPresential() ? ProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL : ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL
            );

            $this->entityManager->flush();

            $this->moduleService->subscribe($session, $user);

            $rememberMe = new RememberMeBadge();
            $rememberMe->enable();
            $authenticatorManager->authenticateUser($user, $this->authenticator, $request, [$rememberMe]);

            return $this->apiResponse('Your account has been created. You can now login.');
        } catch (\Exception $exception) {
            $this->logger->critical($exception, [
                'client' => $module->getClient()->getName(),
                'module' => $module->getName(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An error has occurred. The registration could not be completed'), extraData: $exception->getMessage());
        }
    }

    #[Route('/register-new-trainee-public-replay', name: '_self_subscription_hub_register_new_participant_replay', options: ['expose' => true], methods: ['POST'])]
    public function registerTraineePublicReplay(
        Request $request,
        Module $module,
        UserAuthenticatorInterface $authenticatorManager,
    ): Response {
        try {
            if (!$request->request->has('register-lastname') ||
                !$request->request->has('register-firstname') ||
                !$request->request->has('register-email') ||
                !$request->request->has('register-password') ||
                !$module->isEmailAllowed($request->request->get('register-email'))
            ) {
                throw new \InvalidArgumentException('Incomplete registration form values');
            }
            $email = $request->request->get('register-email');
            $phone = $request->request->get('register-phone');

            $client = $module->getClient();

            /** @var ?User $existingUser */
            $existingUser = $this->userRepository->findOneBy([
                'email' => $email,
                'client' => $client,
            ]);

            $existingUserModule = $this->userModuleRepository->findOneBy(['user' => $existingUser, 'module' => $module]);

            if ($request->request->has('register-form-responses')) {
                $registrationFormResponsesJson = $request->request->get('register-form-responses');
                $registrationFormResponses = array_filter(json_decode($registrationFormResponsesJson, true), function ($item) {
                    return !in_array($item['type'], User::TYPES_TO_EXCLUDE);
                });
            } else {
                $registrationFormResponses = [];
            }

            if ($existingUser !== null) {
                if ($existingUserModule !== null) {
                    if (!$existingUser->isActive()) {
                        return $this->apiErrorResponse(
                            $this->translator->trans('The email address entered is attached to an account that is no longer active in the application. Please contact us for any reactivation request.')
                        );
                    } else {
                        return $this->apiErrorResponse(
                            $this->translator->trans('The email address entered is already used for a user account. <a href="%resetUrl%">Do you want to reset your password ?</a>', ['%resetUrl%' => $this->generateUrl('_self_subscription_hub_password_reset_request', ['clientName' => $module->getSlugClient(), 'domain' => $module->getDomain(), 'username' => $email])]),
                        );
                    }
                } else {
                    $userModule = (new UserModule($existingUser, $module))
                        ->setCreatedAt(new \DateTime())
                        ->setLastAuthenticationAt(new \DateTime())
                        ->setPassword($this->passwordHasher->hashPassword($existingUser, $request->request->get('register-password')))
                        ->setRegistrationFormCatalogResponses($registrationFormResponses);

                    $this->entityManager->persist($userModule);
                    $this->entityManager->flush();

                    $rememberMe = new RememberMeBadge();
                    $rememberMe->enable();
                    $authenticatorManager->authenticateUser($existingUser, $this->authenticator, $request, [$rememberMe]);

                    return $this->apiResponse('Your account has been created. You can now login.');
                }
            }

            $user = (new User())
                ->setLastName($request->request->get('register-lastname'))
                ->setFirstName($request->request->get('register-firstname'))
                ->setCompany($request->request->get('register-company'))
                ->setEmail($email)
                ->setClient($client)
                ->setPhone($phone)
                ->setRegistrationFormCatalogResponses($registrationFormResponses)
                ->setPreferredLang($client->getDefaultLang())
                ->setClearPassword($request->request->get('register-password'))
                ->setStatus(User::STATUS_ACTIVE);

            $userModule = (new UserModule($user, $module))
                ->setCreatedAt(new \DateTime())
                ->setLastAuthenticationAt(new \DateTime())
                ->setPassword($this->passwordHasher->hashPassword($user, $request->request->get('register-password')))
                ->setRegistrationFormCatalogResponses($registrationFormResponses);

            $this->entityManager->persist($user);
            $this->entityManager->persist($userModule);
            $this->entityManager->flush();

            $rememberMe = new RememberMeBadge();
            $rememberMe->enable();
            $authenticatorManager->authenticateUser($user, $this->authenticator, $request, [$rememberMe]);

            return $this->apiResponse('Your account has been created. You can now login.');
        } catch (\Exception $exception) {
            $this->logger->critical($exception, [
                'client' => $module->getClient()->getName(),
                'module' => $module->getName(),
            ]);

            return $this->apiErrorResponse($this->translator->trans('An error has occurred. The registration could not be completed'), extraData: $exception->getMessage());
        }
    }

    #[Route('/profile/update', name: '_self_subscription_profile_update', options: ['expose' => true])]
    #[ParamConverter('module', options: ['mapping' => ['domain' => 'domain']])]
    #[IsGranted(ModuleVoter::ACCESS_HUB, 'module')]
    public function updateProfile(Request $request, Module $module): Response
    {
        if (!$request->request->has('lastName') ||
            (($request->request->get('lastName') ?? '') === '') ||
            !$request->request->has('firstName') ||
            (($request->request->get('firstName') ?? '') === '') ||
            !$request->request->has('company') ||
            (($request->request->get('company') ?? '') === '') ||
            !$request->request->has('email') ||
            (($request->request->get('email') ?? '') === '') ||
            !$module->isEmailAllowed($request->request->get('email'))
        ) {
            return $this->apiErrorResponse($this->translator->trans('Some fields are missing', domain: 'self-subscription'));
        }

        /** @var User $user */
        $user = $this->getUser();
        $user->setLastName($request->request->get('lastName'))
            ->setFirstName($request->request->get('firstName'))
            ->setCompany($request->request->get('company'))
            ->setEmail($request->request->get('email'));

        $this->entityManager->flush();

        return $this->apiResponse([]);
    }

    #[Route('/profile/image/update', name: '_self_subscription_profile_upload_picture', options: ['expose' => true])]
    #[ParamConverter('module', options: ['mapping' => ['domain' => 'domain']])]
    #[IsGranted(ModuleVoter::ACCESS_HUB, 'module')]
    public function changeImageProfile(Request $request, Module $module, RessourceUploader $ressourceUploader): Response
    {
        if (empty($request->files->get('file')) || empty($request->files->get('file')->getClientOriginalExtension())) {
            return $this->apiErrorResponse($this->translator->trans('Not file is present'));
        }

        $file = $request->files->get('file');
        try {
            $formatParam = $request->request->get('format', 'jpg,png');
            $ressourceUploader->isFileUploadedValid($file, explode(',', $formatParam), 2000000);
        } catch (UploadFileException $ex) {
            return $this->apiErrorResponse($ex->getMessage());
        }

        $directory = (string) $this->getParameter('profile_pictures_directory');
        $newFileName = $ressourceUploader->upload($file, $directory);
        /** @var User $user */
        $user = $this->getUser();
        $user->setProfilePictureFilename($newFileName);

        $this->entityManager->flush();

        return $this->apiResponse($newFileName);
    }

    #[Route('/password/reset-request', name: '_self_subscription_hub_password_reset_request', options: ['expose' => true])]
    public function resetPasswordRequest(Request $request, Module $module): Response
    {
        if (!$request->query->has('username')) {
            throw new \InvalidArgumentException('Incomplete registration form values');
        }

        $user = $this->userRepository->findOneBy([
            'email' => $request->query->get('username'),
            'status' => User::STATUS_ACTIVE,
            'client' => $module->getClient(),
        ]);

        if ($user) {
            $this->userSecurityService->requestPasswordReset($user, $module->getDomain(), $module->getSlugClient());
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('_self_subscription_hub_login', ['clientName' => $module->getSlugClient(), 'domain' => $module->getDomain(), 'password_requested' => $request->query->get('username')]);
    }

    #[Route('/password/reset/{token}', name: '_self_subscription_hub_password_reset', options: ['expose' => true])]
    public function resetPassword(Module $module, Request $request, string $token, UserAuthenticatorInterface $authenticator): Response
    {
        if (null === $user = $this->userRepository->findOneBy(['passwordResetToken' => $token])) {
            throw new AccessDeniedException();
        }

        if (!$request->request->has('password') ||
            (($request->request->get('password') ?? '') === '') ||
            !$request->request->has('confirm') ||
            (($request->request->get('confirm') ?? '') === '') ||
            $request->request->get('password') !== $request->request->get('confirm')
        ) {
            throw new \InvalidArgumentException('Incomplete registration form values');
        }

        $this->userSecurityService->updatePassword($user, $request->request->get('password'));
        $this->entityManager->flush();

        return $this->apiResponse(['location' => $this->generateUrl('_self_subscription_hub_login', ['clientName' => $module->getSlugClient(), 'domain' => $module->getDomain(), 'resetting' => 1])]);
    }

    #[Route('/check-subscription-user/{id}', name: '_check_user_existe', options: ['expose' => true], methods: ['GET'])]
    public function checkUserInscrit(
        Request $request,
        ProviderSession $session): Response
    {
        try {
            $user = $this->userRepository->findOneBy(['email' => $request->query->get('email'), 'client' => $session->getClient()]);
            $userModule = $this->userModuleRepository->findOneBy(['user' => $user, 'module' => $session->getSubject()->getTheme()->getModule()]);

            return $this->apiResponse(['isInscrit' => !($userModule === null)]);
        } catch (\Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[ParamConverter('module', options: ['mapping' => ['domain' => 'domain']])]
    #[Route('/check-subscription-user-replay/', name: '_check_user_existe_replay', options: ['expose' => true], methods: ['GET'])]
    public function checkUserInscritReplay(
        Request $request,
        Module $module): Response
    {
        try {
            $user = $this->userRepository->findOneBy(['email' => $request->query->get('email'), 'client' => $module->getClient()]);
            $userModule = $this->userModuleRepository->findOneBy(['user' => $user, 'module' => $module]);

            return $this->apiResponse(['isInscrit' => !($userModule === null)]);
        } catch (\Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    // à bien laisser à la fin pour éviter les conflits de routes et avoir un Accès refusé sinon mettre un priority: 1 dans le configuration de la route
    #[Route('/', name: '_self_subscription_hub_home', options: ['expose' => true])]
    #[Route('/{subject}', name: '_self_subscription_hub_home_subject', options: ['expose' => true])]
    public function home(Module $module, Request $request, string $selfSubscriptionModuleLogoUri, string $selfSubscriptionSubjectIllustrationUri, ?string $subject = null): Response
    {
        $error = null;
        if (array_key_exists('errorAuthentication', $request->query->all()) &&
            $request->query->all()['errorAuthentication'] === '1'
        ) {
            $error = $this->translator->trans('Your email or password is not valid');
        }
        if ((!$this->isGranted('IS_AUTHENTICATED') || !$this->isGranted(ModuleVoter::ACCESS_HUB, $module)) && $module->isPublic()) {
            $user = (new User())
                ->setId(User::ID_USER_GUEST)
                ->setLastName($this->translator->trans('Guest'))
                ->setFirstName($this->translator->trans('Log in'))
                ->setEmail('guest.user@domain.com')
                ->setRoles(['ROLE_TRAINEE']);
        } elseif (!$this->isGranted('IS_AUTHENTICATED')) {
            return $this->redirectToRoute('_self_subscription_hub_login', ['clientName' => $module->getSlugClient(), 'domain' => $module->getDomain()]);
        } else {
            $user = $this->getUser();
        }
        $request->setLocale($module->getLang());

        $pageAccess = [
            'Search' => true,
            'Replay' => $module->getPersonalization()->isEnabledReplay(),
            'Current' => $module->getPersonalization()->isEnabledSessions(),
            'Previous' => $module->getPersonalization()->isEnabledSessions(),
            'FAQ' => $module->getPersonalization()->isEnabledFaq(),
        ];

        $pageKey = $request->query->get('page') ?? 'Search';
        $validPages = array_keys($pageAccess);
        $page = $pageKey && in_array($pageKey, $validPages) && array_key_exists($pageKey, $pageAccess) && $pageAccess[$pageKey] ? $pageKey : 'Search';

        if ($subject) {
            $subject = $this->subjectRepository->find($subject);
            if ($subject === null || $subject->getModuleId() !== $module->getId()) {
                throw new AccessDeniedException();
            }
        }

        return $this->render(
            'self-subscription/hub/home.html.twig',
            [
                'module' => $module,
                'error' => $error,
                'subjects' => $this->subjectRepository->findAllFromModule($module),
                'allUserSubscriptions' => $this->sessionRepository->getAllSubscribedSessionOfModule($module, $user),
                'user' => $user,
                'selfSubscriptionModuleLogoUri' => $selfSubscriptionModuleLogoUri,
                'selfSubscriptionModuleSubjectIllustration' => $selfSubscriptionSubjectIllustrationUri,
                'page' => $page,
                'onlyOneSubject' => $subject,
            ]
        );
    }
}
