<?php

namespace App\SelfSubscription\EventListener;

use App\SelfSubscription\Entity\Module;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Security;

class ModuleListener
{
    public function __construct(private Security $security)
    {
    }

    public function preUpdate(Module $module, LifecycleEventArgs $event): void
    {
        $module->setUpdatedAt(new \DateTimeImmutable());
        $module->setUpdatedBy($this->security->getUser());
    }
}
