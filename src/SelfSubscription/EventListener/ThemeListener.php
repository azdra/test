<?php

namespace App\SelfSubscription\EventListener;

use App\SelfSubscription\Entity\Theme;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class ThemeListener
{
    public function preUpdate(Theme $theme, LifecycleEventArgs $event): void
    {
        $theme->setUpdatedAt(new \DateTimeImmutable());
    }
}
