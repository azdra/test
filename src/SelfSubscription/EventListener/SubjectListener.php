<?php

namespace App\SelfSubscription\EventListener;

use App\Model\GeneratorTrait;
use App\SelfSubscription\Entity\Subject;
use App\SelfSubscription\Repository\SubjectRepository;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class SubjectListener
{
    use GeneratorTrait;

    public function __construct(private SubjectRepository $subjectRepository)
    {
    }

    public function prePersist(Subject $subject, LifecycleEventArgs $eventArgs): void
    {
        $importCode = $this->randomHexString(8);
        while ($this->subjectRepository->count(['importCode' => $importCode]) !== 0) {
            $importCode = $this->randomHexString(8);
        }
        $subject->setImportCode($importCode);
    }

    public function preUpdate(Subject $subject, LifecycleEventArgs $event): void
    {
        $subject->setUpdatedAt(new \DateTimeImmutable());
    }
}
