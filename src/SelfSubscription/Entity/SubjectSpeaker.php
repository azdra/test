<?php

namespace App\SelfSubscription\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity()]
#[UniqueEntity(fields: ['name', 'subject'], message: 'This speaker already exists with this subject', errorPath: 'name')]
class SubjectSpeaker
{
    public const AUTHORIZED_EXTENSIONS = ['jpg', 'jpeg', 'png'];
    public const MAX_FILE_SIZE = 20000000;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['ui:edit:subject', 'ui:subjects:hub'])]
    protected int $id;

    #[ORM\Column]
    #[Assert\NotBlank]
    #[Groups(['ui:edit:subject', 'ui:subjects:hub'])]
    private string $name;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:edit:subject', 'ui:subjects:hub'])]
    private ?string $information = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:edit:subject', 'ui:subjects:hub'])]
    private ?string $photo = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:edit:subject', 'ui:subjects:hub'])]
    private ?string $title = null;

    #[ORM\ManyToOne(targetEntity: Subject::class, inversedBy: 'speakers')]
    #[ORM\JoinColumn(name: 'subject_id', referencedColumnName: 'id')]
    private Subject $subject;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getInformation(): ?string
    {
        return $this->information;
    }

    public function setInformation(?string $information): void
    {
        $this->information = $information;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): void
    {
        $this->photo = $photo;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getSubject(): Subject
    {
        return $this->subject;
    }

    public function setSubject(Subject $subject): void
    {
        $this->subject = $subject;
    }
}
