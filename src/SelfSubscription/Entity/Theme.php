<?php

namespace App\SelfSubscription\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class Theme
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'read_client', 'ui:module:read', 'ui:subjects:hub', 'ui:module:hub'])]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: Module::class, inversedBy: 'themes')]
    protected Module $module;

    #[ORM\OneToMany(mappedBy: 'theme', targetEntity: Subject::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['read_session', 'read_client', 'ui:module:read'])]
    protected Collection $subjects;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups(['read_session', 'read_client', 'ui:module:read', 'ui:theme:update', 'ui:subjects:hub', 'ui:module:hub'])]
    protected string $name;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups(['read_client', 'ui:module:read', 'ui:theme:update', 'ui:subjects:hub'])]
    protected string $code;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank()]
    #[Groups(['read_client', 'ui:module:read', 'ui:theme:update', 'ui:subjects:hub'])]
    protected string $description;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read_client', 'ui:module:read', 'ui:theme:update', 'ui:subjects:hub'])]
    protected bool $active = true;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['read_client', 'ui:module:read', 'ui:subjects:hub'])]
    protected \DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['read_client', 'ui:module:read', 'ui:subjects:hub'])]
    protected \DateTimeImmutable $updatedAt;

    public function __construct(Module $module)
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->module = $module;
        $this->subjects = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getModule(): Module
    {
        return $this->module;
    }

    public function setModule(Module $module): self
    {
        $this->module = $module;

        return $this;
    }

    public function getSubjects(): ArrayCollection|Collection
    {
        return $this->subjects;
    }

    public function setSubjects(ArrayCollection|Collection $subjects): self
    {
        $this->subjects = $subjects;

        return $this;
    }

    public function addSubject(Subject $subject): self
    {
        if (!$this->subjects->contains($subject)) {
            $this->subjects->add($subject);
        }

        return $this;
    }

    public function removeSubject(Subject $subject): self
    {
        if ($this->subjects->contains($subject)) {
            $this->subjects->removeElement($subject);
        }

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    #[Groups('ui:module:read')]
    public function getModuleId(): int
    {
        return $this->module->getId();
    }
}
