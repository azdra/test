<?php

namespace App\SelfSubscription\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class FaqEntry
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read_session', 'read_client', 'ui:module:read', 'ui:module:hub'])]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: Module::class, inversedBy: 'faqs')]
    protected Module $module;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups(['read_session', 'read_client', 'ui:module:read', 'ui:faq:update', 'ui:module:hub'])]
    protected string $question;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank()]
    #[Groups(['read_client', 'ui:module:read', 'ui:faq:update', 'ui:module:hub'])]
    protected string $answer;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read_client', 'ui:module:read', 'ui:faq:update', 'ui:module:hub'])]
    protected bool $active = true;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['read_client', 'ui:module:read'])]
    protected \DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['read_client', 'ui:module:read'])]
    protected \DateTimeImmutable $updatedAt;

    public function __construct(Module $module)
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->module = $module;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getModule(): Module
    {
        return $this->module;
    }

    public function setModule(Module $module): self
    {
        $this->module = $module;

        return $this;
    }

    public function getQuestion(): string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getAnswer(): string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    #[Groups('ui:module:read')]
    public function getModuleId(): int
    {
        return $this->module->getId();
    }
}
