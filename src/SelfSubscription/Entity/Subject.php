<?php

namespace App\SelfSubscription\Entity;

use App\Entity\CategorySessionType;
use App\Entity\ProviderSession;
use App\Entity\VideoReplay;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
#[ORM\UniqueConstraint(columns: ['import_code'])]
class Subject
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['ui:module:read', 'read_session', 'read_client', 'ui:subjects:hub', 'help_need:read', 'ui:edit:subject'])]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: Theme::class, inversedBy: 'subjects')]
    #[Groups(['ui:subjects:hub'])]
    protected Theme $theme;

    #[ORM\OneToMany(mappedBy: 'subject', targetEntity: ProviderSession::class)]
    #[Groups(['ui:module:read', 'ui:subjects:hub'])]
    protected Collection $sessions;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups(['read_session', 'read_client', 'ui:module:read', 'ui:subject:update', 'ui:subjects:hub', 'help_need:read', 'ui:edit:subject'])]
    protected string $name = '';

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups(['read_client', 'ui:module:read', 'ui:subject:update', 'ui:subjects:hub', 'ui:edit:subject'])]
    protected string $code;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank()]
    #[Groups(['read_client', 'ui:module:read', 'ui:subject:update', 'ui:subjects:hub', 'ui:edit:subject'])]
    protected string $description;

    #[ORM\Column(type: 'string')]
    #[Groups(['read_client', 'ui:module:read', 'ui:subject:update', 'ui:subjects:hub', 'ui:edit:subject'])]
    protected string $importCode;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read_client', 'ui:module:read', 'ui:subject:update', 'ui:subjects:hub', 'ui:edit:subject'])]
    protected bool $active = true;

    #[ORM\Column(type: 'integer')]
    #[Groups(['read_client', 'ui:module:read', 'ui:subject:update', 'ui:subjects:hub', 'ui:edit:subject'])]
    protected int $duration = 0;

    #[ORM\Column(type: 'integer')]
    #[Groups(['ui:module:read', 'ui:subject:update', 'ui:subjects:hub', 'ui:edit:subject'])]
    protected int $orderInModule = 0;

    #[ORM\Column()]
    #[Groups(['ui:module:read', 'ui:subject:update', 'ui:subjects:hub', 'ui:edit:subject'])]
    protected CategorySessionType $categorySession = CategorySessionType::CATEGORY_SESSION_FORMATION;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['ui:module:read', 'ui:subjects:hub', 'ui:edit:subject'])]
    protected \DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['ui:module:read', 'ui:subjects:hub', 'ui:edit:subject'])]
    protected \DateTimeImmutable $updatedAt;

    #[ORM\Embedded(class: SubjectProgram::class)]
    #[Groups(['ui:subject:update', 'ui:edit:subject', 'ui:subjects:hub'])]
    private SubjectProgram $program;

    #[ORM\OneToMany(mappedBy: 'subject', targetEntity: SubjectSpeaker::class, cascade: ['all'])]
    #[Groups(['ui:edit:subject', 'ui:subjects:hub'])]
    private Collection $speakers;

    #[ORM\OneToOne(mappedBy: 'subject', targetEntity: VideoReplay::class, cascade: ['persist'])]
    #[Groups(['ui:edit:subject', 'read_client', 'read_session', 'ui:subjects:hub'])]
    private ?VideoReplay $videoReplay = null;

    public function __construct(Theme $theme)
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->theme = $theme;
        $this->sessions = new ArrayCollection();
        $this->speakers = new ArrayCollection();
        $this->program = new SubjectProgram();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTheme(): Theme
    {
        return $this->theme;
    }

    public function setTheme(Theme $theme): self
    {
        $this->theme = $theme;

        return $this;
    }

    public function getSessions(): ArrayCollection|Collection
    {
        return $this->sessions;
    }

    public function setSessions(ArrayCollection|Collection $sessions): self
    {
        $this->sessions = $sessions;

        return $this;
    }

    public function addSession(ProviderSession $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions->add($session);
        }

        return $this;
    }

    public function removeSession(ProviderSession $session): self
    {
        if ($this->sessions->contains($session)) {
            $this->sessions->removeElement($session);
        }

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImportCode(): string
    {
        return $this->importCode;
    }

    public function setImportCode(string $importCode): self
    {
        $this->importCode = $importCode;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getCategorySession(): CategorySessionType
    {
        return $this->categorySession;
    }

    public function setCategorySession(CategorySessionType $categorySession): self
    {
        $this->categorySession = $categorySession;

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    #[Groups(['ui:module:read', 'ui:subject:update', 'read_session', 'ui:edit:subject'])]
    public function getThemeId(): int
    {
        return $this->theme->getId();
    }

    #[Groups(['ui:module:read', 'ui:subject:update', 'read_session', 'ui:subjects:hub', 'ui:edit:subject'])]
    public function getModuleId(): int
    {
        return $this->theme->getModuleId();
    }

    #[Groups(['ui:module:read', 'ui:subject:update', 'read_session', 'ui:edit:subject'])]
    public function getThemeTitle(): string
    {
        return $this->theme->getName();
    }

    #[Groups(['ui:module:read', 'ui:subject:update', 'read_session', 'ui:subjects:hub', 'ui:edit:subject'])]
    public function getModuleTitle(): string
    {
        return $this->theme->getModule()->getName();
    }

    #[Groups(['ui:module:read', 'ui:subject:update', 'read_session', 'ui:subjects:hub', 'ui:edit:subject'])]
    public function getSlugClient(): string
    {
        return $this->theme->getModule()->getClient()->getSlug();
    }

    #[Groups(['ui:module:read', 'ui:subject:update', 'read_session', 'ui:subjects:hub', 'ui:edit:subject'])]
    public function getModuleDomain(): string
    {
        return $this->theme->getModule()->getDomain();
    }

    #[Groups(['ui:module:read', 'ui:subject:update', 'read_session', 'ui:subjects:hub', 'ui:edit:subject'])]
    public function getClientTitle(): string
    {
        return $this->theme->getModule()->getClient()->getName();
    }

    #[Groups(['ui:subjects:hub'])]
    public function getModuleLightData(): array
    {
        $module = [
            'id' => $this->theme->getModuleId(),
            'isCloseSubscriptions' => $this->theme->getModule()->isCloseSubscriptions(),
            'closeSubscriptionsBeforeNbrDays' => $this->theme->getModule()->getCloseSubscriptionsBeforeNbrDays(),
        ];

        return $module;
    }

    #[Groups(['ui:subjects:hub', 'ui:edit:subject'])]
    public function getReadableDuration(): string
    {
        $clientLang = $this->getTheme()->getModule()->getClient()->getDefaultLang();
        $minutes = $this->duration % 60;
        $hours = floor($this->duration / 60);

        return ($hours < 10 ? '0' : '').$hours.($clientLang === 'fr' ? 'h' : ':').($minutes < 10 ? '0' : '').$minutes;
    }

    public function getSpeakers(): Collection
    {
        return $this->speakers;
    }

    public function setSpeakers(Collection $speakers): self
    {
        $this->speakers = $speakers;

        return $this;
    }

    public function addSpeaker(SubjectSpeaker $speaker): self
    {
        if (!$this->speakers->contains($speaker)) {
            $this->speakers->add($speaker);
        }

        return $this;
    }

    public function removeSpeaker(SubjectSpeaker $speaker): self
    {
        if ($this->speakers->contains($speaker)) {
            $this->speakers->removeElement($speaker);
        }

        return $this;
    }

    public function getProgram(): SubjectProgram
    {
        return $this->program;
    }

    public function setProgram(SubjectProgram $program): void
    {
        $this->program = $program;
    }

    public function getVideoReplay(): ?VideoReplay
    {
        return $this->videoReplay;
    }

    public function setVideoReplay(?VideoReplay $videoReplay): void
    {
        $this->videoReplay = $videoReplay;
    }

    #[Groups(['ui:edit:subject'])]
    public function hasFutureSessions(): bool
    {
        return (bool) count($this->sessions->filter(fn (ProviderSession $session) => $session->getDateStart() > new \DateTimeImmutable()));
    }

    public function getOrderInModule(): int
    {
        return $this->orderInModule;
    }

    public function setOrderInModule(int $orderInModule): Subject
    {
        $this->orderInModule = $orderInModule;

        return $this;
    }
}
