<?php

namespace App\SelfSubscription\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class WhiteListEntry
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups('ui:module:read')]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: Module::class, inversedBy: 'whiteList')]
    protected Module $module;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups('ui:module:read')]
    protected string $allowed;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups('ui:module:read')]
    protected \DateTimeImmutable $createdAt;

    public function __construct(Module $module, string $allowed)
    {
        $this->module = $module;
        $this->allowed = $allowed;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getModule(): Module
    {
        return $this->module;
    }

    public function setModule(Module $module): self
    {
        $this->module = $module;

        return $this;
    }

    public function getAllowed(): string
    {
        return $this->allowed;
    }

    public function setAllowed(string $allowed): self
    {
        $this->allowed = $allowed;

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
