<?php

namespace App\SelfSubscription\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Embeddable]
class ModulePersonalization
{
    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private bool $enabledFaq = true;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private ?string $faqDescription = null;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private ?string $faqTitle = 'FAQ';

    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private bool $enabledTraining = true;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private ?string $trainingDescription = null;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => 'The trainings'])]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private ?string $trainingTitle = 'The trainings';

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    protected ?string $trainingBanner = null;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => 'center'])]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private ?string $trainingBannerPosition = 'center';

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private bool $enabledReplay = false;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private ?string $replayDescription = null;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => 'Replay'])]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private ?string $replayTitle = 'Replay';

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    protected ?string $replayBanner = null;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => 'center'])]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private ?string $replayBannerPosition = 'center';

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => 'My Sessions'])]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private ?string $sessionsTitle = 'My Sessions';

    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private bool $enabledSessions = true;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private ?string $sessionsDescription = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    protected ?string $sessionsBanner = null;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => 'center'])]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private ?string $sessionsBannerPosition = 'center';

    public function getFaqDescription(): ?string
    {
        return $this->faqDescription;
    }

    public function setFaqDescription(?string $faqDescription): void
    {
        $this->faqDescription = $faqDescription;
    }

    public function getTrainingDescription(): ?string
    {
        return $this->trainingDescription;
    }

    public function setTrainingDescription(?string $trainingDescription): void
    {
        $this->trainingDescription = $trainingDescription;
    }

    public function getTrainingTitle(): ?string
    {
        return $this->trainingTitle;
    }

    public function setTrainingTitle(?string $trainingTitle): void
    {
        $this->trainingTitle = $trainingTitle;
    }

    public function getFaqTitle(): ?string
    {
        return $this->faqTitle;
    }

    public function setFaqTitle(?string $faqTitle): self
    {
        $this->faqTitle = $faqTitle;

        return $this;
    }

    public function getReplayDescription(): ?string
    {
        return $this->replayDescription;
    }

    public function setReplayDescription(?string $replayDescription): self
    {
        $this->replayDescription = $replayDescription;

        return $this;
    }

    public function getReplayTitle(): ?string
    {
        return $this->replayTitle;
    }

    public function setReplayTitle(?string $replayTitle): self
    {
        $this->replayTitle = $replayTitle;

        return $this;
    }

    public function isEnabledFaq(): bool
    {
        return $this->enabledFaq;
    }

    public function setEnabledFaq(bool $enabledFaq): void
    {
        $this->enabledFaq = $enabledFaq;
    }

    public function isEnabledTraining(): bool
    {
        return $this->enabledTraining;
    }

    public function setEnabledTraining(bool $enabledTraining): void
    {
        $this->enabledTraining = $enabledTraining;
    }

    public function isEnabledReplay(): bool
    {
        return $this->enabledReplay;
    }

    public function setEnabledReplay(bool $enabledReplay): void
    {
        $this->enabledReplay = $enabledReplay;
    }

    public function getSessionsTitle(): ?string
    {
        return $this->sessionsTitle;
    }

    public function setSessionsTitle(?string $sessionsTitle): void
    {
        $this->sessionsTitle = $sessionsTitle;
    }

    public function getSessionsDescription(): ?string
    {
        return $this->sessionsDescription;
    }

    public function setSessionsDescription(?string $sessionsDescription): void
    {
        $this->sessionsDescription = $sessionsDescription;
    }

    public function isEnabledSessions(): bool
    {
        return $this->enabledSessions;
    }

    public function setEnabledSessions(bool $enabledSessions): void
    {
        $this->enabledSessions = $enabledSessions;
    }

    public function getTrainingBanner(): ?string
    {
        return $this->trainingBanner;
    }

    public function setTrainingBanner(?string $trainingBanner): void
    {
        $this->trainingBanner = $trainingBanner;
    }

    public function getTrainingBannerPosition(): ?string
    {
        return $this->trainingBannerPosition;
    }

    public function setTrainingBannerPosition(?string $trainingBannerPosition): void
    {
        $this->trainingBannerPosition = $trainingBannerPosition;
    }

    public function getReplayBanner(): ?string
    {
        return $this->replayBanner;
    }

    public function setReplayBanner(?string $replayBanner): void
    {
        $this->replayBanner = $replayBanner;
    }

    public function getReplayBannerPosition(): ?string
    {
        return $this->replayBannerPosition;
    }

    public function setReplayBannerPosition(?string $replayBannerPosition): void
    {
        $this->replayBannerPosition = $replayBannerPosition;
    }

    public function getSessionsBanner(): ?string
    {
        return $this->sessionsBanner;
    }

    public function setSessionsBanner(?string $sessionsBanner): void
    {
        $this->sessionsBanner = $sessionsBanner;
    }

    public function getSessionsBannerPosition(): ?string
    {
        return $this->sessionsBannerPosition;
    }

    public function setSessionsBannerPosition(?string $sessionsBannerPosition): void
    {
        $this->sessionsBannerPosition = $sessionsBannerPosition;
    }
}
