<?php

namespace App\SelfSubscription\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class BlackListEntry
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups('ui:module:read')]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: Module::class, inversedBy: 'blackList')]
    protected Module $module;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups('ui:module:read')]
    protected string $prohibited;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups('ui:module:read')]
    protected \DateTimeImmutable $createdAt;

    public function __construct(Module $module, string $prohibited)
    {
        $this->module = $module;
        $this->prohibited = $prohibited;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getModule(): Module
    {
        return $this->module;
    }

    public function setModule(Module $module): self
    {
        $this->module = $module;

        return $this;
    }

    public function getProhibited(): string
    {
        return $this->prohibited;
    }

    public function setProhibited(string $prohibited): self
    {
        $this->prohibited = $prohibited;

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
