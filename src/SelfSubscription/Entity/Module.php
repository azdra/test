<?php

namespace App\SelfSubscription\Entity;

use App\Entity\Client\Client;
use App\Entity\ReplayCategory;
use App\Entity\User;
use App\Entity\UserModule;
use App\Entity\VideoReplay;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Embedded;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class Module
{
    public const AUTHORIZED_EXTENSIONS = ['jpg', 'png', 'jpeg'];
    public const MAX_FILE_SIZE = 20000000;

    public const ROUNDED_EDGE = 'rounded';
    public const SQUARE_EDGE = 'square';

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['ui:module:read', 'read_session', 'read_client', 'ui:module:hub', 'ui:module:listing', 'read_session_list', 'ui:edit:subject'])]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'selfSubscriptionModules')]
    #[Groups(['ui:module:read', 'ui:module:listing', 'ui:module:hub', 'ui:edit:subject'])]
    protected Client $client;

    #[ORM\OneToMany(mappedBy: 'module', targetEntity: Theme::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['ui:module:read', 'read_session', 'read_client', 'ui:module:hub'])]
    protected Collection $themes;

    #[ORM\OneToMany(mappedBy: 'module', targetEntity: FaqEntry::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['ui:module:read', 'read_session', 'read_client', 'ui:module:hub'])]
    protected Collection $faqs;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups(['ui:module:read', 'read_session', 'read_client', 'read_session_list', 'ui:module:update', 'ui:module:hub', 'ui:module:listing'])]
    protected string $name;

    #[ORM\Column(type: 'string', options: ['default' => Client::FRENCH_LANGUAGE])]
    #[Assert\NotBlank()]
    #[Groups(['ui:module:update', 'ui:module:hub', 'ui:module:listing', 'ui:module:read', 'ui:edit:subject'])]
    protected string $lang;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Assert\Length(max: 120, maxMessage: 'Description must not exceed 120 character')]
    #[Groups(['ui:module:read', 'read_client', 'ui:module:update', 'ui:module:hub', 'ui:module:listing'])]
    protected ?string $description = null;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['ui:module:read', 'read_client', 'read_session_list', 'ui:module:update', 'ui:module:hub', 'ui:module:listing'])]
    protected bool $active = true;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['ui:module:read', 'read_client', 'ui:module:update', 'ui:module:hub', 'ui:module:listing'])]
    protected bool $public = false;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups(['ui:module:read', 'read_client', 'ui:module:update', 'ui:module:hub', 'ui:module:listing'])]
    protected string $domain;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups(['ui:module:read', 'read_client', 'ui:module:update', 'ui:module:hub', 'ui:module:listing'])]
    protected string $mainColor;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups(['ui:module:read', 'read_client', 'ui:module:update', 'ui:module:hub', 'ui:module:listing'])]
    protected string $secondaryColor;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:module:read', 'read_client', 'read_session', 'read_session_list', 'ui:module:update', 'ui:module:hub', 'ui:module:listing'])]
    protected ?string $logo = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:module:read', 'read_client', 'read_session', 'ui:module:update', 'ui:module:hub', 'ui:module:listing'])]
    protected ?string $banner = null;

    #[ORM\Column(type: 'string', length: 255, options: ['default' => 'Entreprise'])]
    #[Groups(['ui:module:read', 'read_client', 'read_session', 'ui:module:update', 'ui:module:hub'])]
    private string $companyLabel = 'Entreprise';

    #[ORM\Column(type: 'string', length: 255, options: ['default' => 'Entreprise'])]
    #[Groups(['ui:module:read', 'read_client', 'read_session', 'ui:module:update', 'ui:module:hub'])]
    private string $companyPlaceholder = 'Entreprise';

    #[ORM\Column(type: 'string', length: 255, options: ['default' => 'Adresse mail'])]
    #[Groups(['ui:module:read', 'read_client', 'read_session', 'ui:module:update', 'ui:module:hub'])]
    private string $emailLabel = 'Adresse mail';

    #[ORM\Column(type: 'string', length: 255, options: ['default' => 'Adresse mail'])]
    #[Groups(['ui:module:read', 'read_client', 'read_session', 'ui:module:update', 'ui:module:hub'])]
    private string $emailPlaceholder = 'Adresse mail';

    #[ORM\Column(type: 'integer')]
    #[Groups(['ui:module:read', 'read_client', 'read_session', 'ui:module:update'])]
    protected int $subscriptionMax = 0;

    #[ORM\Column(type: 'integer')]
    #[Groups(['ui:module:read', 'read_client', 'read_session', 'ui:module:update'])]
    protected int $alertLimit = 0;

    #[ORM\OneToMany(mappedBy: 'module', targetEntity: WhiteListEntry::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups('ui:module:read')]
    private Collection $whiteList;

    #[ORM\OneToMany(mappedBy: 'module', targetEntity: BlackListEntry::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups('ui:module:read')]
    private Collection $blackList;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['ui:module:read', 'read_client'])]
    protected \DateTimeImmutable $createdAt;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'created_by', referencedColumnName: 'id')]
    #[Groups(['ui:module:read'])]
    protected ?User $createdBy = null;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['ui:module:read', 'read_client'])]
    protected \DateTimeImmutable $updatedAt;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'updated_by', referencedColumnName: 'id')]
    #[Groups(['ui:module:read'])]
    protected ?User $updatedBy = null;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['ui:module:read', 'read_client', 'ui:module:update', 'ui:module:hub'])]
    protected bool $closeSubscriptions = false;

    #[ORM\Column(type: 'integer')]
    #[Groups(['ui:module:read', 'read_client', 'read_session', 'ui:module:update', 'ui:module:hub'])]
    protected int $closeSubscriptionsBeforeNbrDays = 0;

    #[Embedded(class: ModulePersonalization::class)]
    #[Groups(['ui:module:read', 'ui:module:update', 'ui:module:hub'])]
    private ModulePersonalization $personalization;

    #[ORM\OneToMany(mappedBy: 'module', targetEntity: VideoReplay::class, cascade: ['all'])]
    #[Groups(['ui:module:read', 'read_session', 'read_client', 'ui:module:hub'])]
    private Collection $videosReplay;

    #[ORM\OneToMany(mappedBy: 'module', targetEntity: ReplayCategory::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['ui:module:read', 'read_session', 'read_client', 'ui:module:hub', 'ui:edit:subject'])]
    private Collection $catalogsReplay;

    #[ORM\Column(type: 'json')]
    #[Groups(['ui:module:update', 'ui:module:read', 'read_session', 'read_client', 'ui:module:hub', 'ui:edit:subject'])]
    private ?array $registrationForm = [];

    #[ORM\Column(type: 'string', nullable: false, options: ['default' => self::SQUARE_EDGE])]
    #[Assert\Choice(
            choices: [
                self::ROUNDED_EDGE,
                self::SQUARE_EDGE,
            ])]
    #[Groups(['ui:module:read', 'read_session', 'read_client', 'ui:module:hub', 'ui:module:update'])]
    private string $templateDesign = self::SQUARE_EDGE;

    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => true])]
    #[Groups(['ui:module:read', 'read_session', 'read_client', 'ui:module:hub', 'ui:module:update'])]
    public bool $showClientLogo = true;

    #[ORM\Column(type: 'boolean', nullable: false, options: ['default' => true])]
    #[Groups(['ui:module:read', 'read_session', 'read_client', 'ui:module:hub', 'ui:module:update'])]
    public bool $showClientContactDetails = true;

    #[ORM\OneToMany(mappedBy: 'module', targetEntity: UserModule::class, cascade: ['persist'])]
    #[Groups(['read_session', 'read_lobby_signature', 'session:evaluation:synthesis'])]
    protected Collection $userModules;

    public function __construct(Client $client)
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->client = $client;
        $this->themes = new ArrayCollection();
        $this->faqs = new ArrayCollection();
        $this->whiteList = new ArrayCollection();
        $this->addWhiteListentry('*');
        $this->blackList = new ArrayCollection();
        $this->videosReplay = new ArrayCollection();
        $this->registrationForm = $this->newRegistrationForm();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getThemes(): ArrayCollection|Collection
    {
        return $this->themes;
    }

    public function setThemes(ArrayCollection|Collection $themes): self
    {
        $this->themes = $themes;

        return $this;
    }

    public function addTheme(Theme $theme): self
    {
        if (!$this->themes->contains($theme)) {
            $this->themes->add($theme);
        }

        return $this;
    }

    public function removeTheme(Theme $theme): self
    {
        if ($this->themes->contains($theme)) {
            $this->themes->removeElement($theme);
        }

        return $this;
    }

    public function getFaqs(): ArrayCollection|Collection
    {
        return $this->faqs;
    }

    public function setFaqs(ArrayCollection|Collection $faqs): self
    {
        $this->faqs = $faqs;

        return $this;
    }

    public function addFaqEntry(FaqEntry $faq): self
    {
        if (!$this->faqs->contains($faq)) {
            $this->faqs->add($faq);
        }

        return $this;
    }

    public function removeFaqEntry(FaqEntry $faq): self
    {
        if ($this->faqs->contains($faq)) {
            $this->faqs->removeElement($faq);
        }

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getMainColor(): string
    {
        return $this->mainColor;
    }

    public function setMainColor(string $mainColor): self
    {
        $this->mainColor = $mainColor;

        return $this;
    }

    public function getSecondaryColor(): string
    {
        return $this->secondaryColor;
    }

    public function setSecondaryColor(string $secondaryColor): self
    {
        $this->secondaryColor = $secondaryColor;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): void
    {
        $this->logo = $logo;
    }

    public function getBanner(): ?string
    {
        return $this->banner;
    }

    public function setBanner(?string $banner): void
    {
        $this->banner = $banner;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getWhiteList(): Collection
    {
        return $this->whiteList;
    }

    public function setWhiteList(Collection $whiteList): self
    {
        $this->whiteList = $whiteList;

        return $this;
    }

    public function getBlackList(): Collection
    {
        return $this->blackList;
    }

    public function setBlackList(Collection $blackList): self
    {
        $this->blackList = $blackList;

        return $this;
    }

    public function addWhiteListEntry(string $mail): self
    {
        $whiteListEntry = new WhiteListEntry($this, $mail);
        if ($this->whiteList->count() == 1 && $this->getWhiteList()->first()->getAllowed() == '*') {
            $this->whiteList->removeElement($this->getWhiteList()->first());
        }
        if (!$this->whiteList->contains($whiteListEntry)) {
            $this->whiteList->add($whiteListEntry);
        }

        return $this;
    }

    public function removeWhiteListEntry(WhiteListEntry $whiteListEntry): self
    {
        if ($this->whiteList->contains($whiteListEntry)) {
            $this->whiteList->removeElement($whiteListEntry);
        }

        if ($this->whiteList->count() === 0) {
            $this->whiteList->add(new WhiteListEntry($this, '*'));
        }

        return $this;
    }

    public function addBlackListEntry(string $mail): self
    {
        $blackListEntry = new BlackListEntry($this, $mail);

        if (!$this->blackList->contains($blackListEntry)) {
            $this->blackList->add($blackListEntry);
        }

        return $this;
    }

    public function removeBlackListEntry(BlackListEntry $blackListEntry): self
    {
        if ($this->blackList->contains($blackListEntry)) {
            $this->blackList->removeElement($blackListEntry);
        }

        return $this;
    }

    public function getSubscriptionMax(): int
    {
        return $this->subscriptionMax;
    }

    public function setSubscriptionMax(int $subscriptionMax): self
    {
        $this->subscriptionMax = $subscriptionMax;

        return $this;
    }

    public function getAlertLimit(): int
    {
        return $this->alertLimit;
    }

    public function setAlertLimit(int $alertLimit): self
    {
        $this->alertLimit = $alertLimit;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?User $updatedBy): self
    {
        if ($this->createdBy === null) {
            $this->createdBy = $updatedBy;
        }

        $this->updatedBy = $updatedBy;

        return $this;
    }

    #[Groups(['ui:module:read', 'read_session', 'read_client', 'ui:module:hub', 'ui:module:listing'])]
    public function getSlugClient(): string
    {
        return preg_replace('/[^a-z0-9]/i', '-', $this->client->getName());
    }

    public function isEmailAllowed(string $email): bool
    {
        return !$this->isBlackListed($email) && $this->isWhiteListed($email);
    }

    private function isBlackListed(string $email): bool
    {
        $blackListEntries = $this->blackList
            ->map(function (BlackListEntry $entry) {
                return trim($entry->getProhibited());
            })
            ->toArray();

        foreach ($blackListEntries as $entry) {
            if (str_starts_with($entry, '*')) {
                $entry = substr_replace($entry, '.*', 0, 1);
            }
            if (preg_match("/$entry/", $email)) {
                return true;
            }
        }

        return false;
    }

    private function isWhiteListed(string $email): bool
    {
        $entries = $this->whiteList
            ->map(function (WhiteListEntry $entry) {
                return trim($entry->getAllowed());
            })
            ->toArray();

        foreach ($entries as $entry) {
            if (str_starts_with($entry, '*')) {
                $entry = substr_replace($entry, '.*', 0, 1);
            }
            if (preg_match("/$entry/", $email)) {
                return true;
            }
        }

        return false;
    }

    public function isCloseSubscriptions(): bool
    {
        return $this->closeSubscriptions;
    }

    public function setCloseSubscriptions(bool $closeSubscriptions): self
    {
        $this->closeSubscriptions = $closeSubscriptions;

        return $this;
    }

    public function getCloseSubscriptionsBeforeNbrDays(): int
    {
        return $this->closeSubscriptionsBeforeNbrDays;
    }

    public function setCloseSubscriptionsBeforeNbrDays(int $closeSubscriptionsBeforeNbrDays): self
    {
        $this->closeSubscriptionsBeforeNbrDays = $closeSubscriptionsBeforeNbrDays;

        return $this;
    }

    #[Groups(['ui:module:hub'])]
    public function getPathLogoClient(): ?string
    {
        return $this->client->getLogo();
    }

    #[Groups(['ui:module:hub'])]
    public function getCategoryAvailableClient(): ?array
    {
        return $this->client->getCategories();
    }

    #[Groups(['ui:module:hub'])]
    public function getDedicatedSupportClient(): ?array
    {
        return ['isActive' => $this->client->getServices()->isDedicatedSupport(),
            'mailSupport' => $this->client->getServices()->getMailSupport(),
            'phoneSupport' => $this->client->getServices()->getPhoneSupport(),
        ];
    }

    public function getCompanyLabel(): string
    {
        return $this->companyLabel;
    }

    public function setCompanyLabel(string $companyLabel): void
    {
        $this->companyLabel = $companyLabel;
    }

    public function getCompanyPlaceholder(): string
    {
        return $this->companyPlaceholder;
    }

    public function setCompanyPlaceholder(string $companyPlaceholder): void
    {
        $this->companyPlaceholder = $companyPlaceholder;
    }

    public function getEmailLabel(): string
    {
        return $this->emailLabel;
    }

    public function setEmailLabel(string $emailLabel): void
    {
        $this->emailLabel = $emailLabel;
    }

    public function getEmailPlaceholder(): string
    {
        return $this->emailPlaceholder;
    }

    public function setEmailPlaceholder(string $emailPlaceholder): void
    {
        $this->emailPlaceholder = $emailPlaceholder;
    }

    public function getLang(): string
    {
        return $this->lang;
    }

    public function setLang(string $lang): void
    {
        $this->lang = $lang;
    }

    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }

    public function getPersonalization(): ModulePersonalization
    {
        return $this->personalization;
    }

    public function setPersonalization(ModulePersonalization $personalization): void
    {
        $this->personalization = $personalization;
    }

    public function getVideosReplay(): Collection
    {
        return $this->videosReplay;
    }

    public function setVideosReplay(Collection $videosReplay): self
    {
        $this->videosReplay = $videosReplay;

        return $this;
    }

    public function getTemplateDesign(): string
    {
        return $this->templateDesign;
    }

    public function setTemplateDesign(string $templateDesign): void
    {
        $this->templateDesign = $templateDesign;
    }

    public function getCatalogsReplay(): Collection
    {
        return $this->catalogsReplay;
    }

    public function setCatalogsReplay(Collection $catalogsReplay): Module
    {
        $this->catalogsReplay = $catalogsReplay;

        return $this;
    }

    public function getRegistrationForm(): ?array
    {
        return $this->registrationForm;
    }

    public function setRegistrationForm(?array $registrationForm): Module
    {
        $this->registrationForm = $registrationForm;

        return $this;
    }

    public function isShowClientLogo(): bool
    {
        return $this->showClientLogo;
    }

    public function setShowClientLogo(bool $showClientLogo): void
    {
        $this->showClientLogo = $showClientLogo;
    }

    public function isShowClientContactDetails(): bool
    {
        return $this->showClientContactDetails;
    }

    public function setShowClientContactDetails(bool $showClientContactDetails): void
    {
        $this->showClientContactDetails = $showClientContactDetails;
    }

    public function getUserModules(): Collection
    {
        return $this->userModules;
    }

    public function setUserModules(Collection $userModules): Module
    {
        $this->userModules = $userModules;

        return $this;
    }

    private function newRegistrationForm(): array
    {
        return [
            [
                'type' => 'firstName',
                'order' => 0,
                'options' => [
                    ['response' => ''],
                ],
                'question' => 'First name',
                'response' => '',
                'standard' => true,
                'displayed' => true,
                'obligatory' => true,
            ],
            [
                'type' => 'lastName',
                'order' => 1,
                'options' => [
                    ['response' => ''],
                ],
                'question' => 'Last name',
                'response' => '',
                'standard' => true,
                'displayed' => true,
                'obligatory' => true,
            ],
            [
                'type' => 'company',
                'order' => 2,
                'options' => [
                    ['response' => ''],
                ],
                'question' => 'Company',
                'response' => '',
                'standard' => true,
                'displayed' => true,
                'obligatory' => false,
            ],
            [
                'type' => 'phone_system',
                'order' => 3,
                'options' => [
                    ['response' => ''],
                ],
                'question' => 'Cellphone',
                'response' => '',
                'standard' => true,
                'displayed' => false,
                'obligatory' => false,
            ],
            [
                'type' => 'email_system',
                'order' => 4,
                'options' => [
                    ['response' => ''],
                ],
                'question' => 'Email address',
                'response' => '',
                'standard' => true,
                'displayed' => true,
                'obligatory' => true,
            ],
            [
                'type' => 'password',
                'order' => 5,
                'options' => [
                    ['response' => ''],
                ],
                'question' => 'Password',
                'response' => '',
                'standard' => true,
                'displayed' => true,
                'obligatory' => true,
            ],
        ];
    }
}
