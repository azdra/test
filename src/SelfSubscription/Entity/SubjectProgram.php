<?php

namespace App\SelfSubscription\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Embeddable]
class SubjectProgram
{
    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['ui:subject:update', 'ui:edit:subject', 'ui:subjects:hub'])]
    private ?string $description = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:subject:update', 'ui:edit:subject', 'ui:subjects:hub'])]
    private ?string $illustration = null;

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(?string $illustration): void
    {
        $this->illustration = $illustration;
    }
}
