<?php

namespace App\SelfSubscription\Repository;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\SelfSubscription\Entity\Module;
use App\SelfSubscription\Entity\Subject;
use App\SelfSubscription\Entity\Theme;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class SubjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Subject::class);
    }

    public function unlinkSessions(Module $module = null, Theme $theme = null, Subject $subject = null): void
    {
        if (null === $module && null === $theme && null === $subject) {
            return;
        }

        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->update()
            ->from(ProviderSession::class, 'session')
            ->set('session.subject', 'null');

        $subjectIdQB = $this->createQueryBuilder('subject')
                ->join('subject.theme', 'theme')
                ->join('theme.module', 'module');

        if (null !== $module) {
            $subjectIdQB->andWhere('module = :module');
            $qb->setParameter('module', $module);
        }

        if (null !== $theme) {
            $subjectIdQB->andWhere('theme = :theme');
            $qb->setParameter('theme', $theme);
        }

        if (null !== $subject) {
            $subjectIdQB->andWhere('subject = :subject');
            $qb->setParameter('subject', $subject);
        }

        $qb->where('IDENTITY(session.subject) in ('.$subjectIdQB.')')
            ->getQuery()
            ->execute();
    }

    public function findAllFromModule(Module $module): array
    {
        return $this->createQueryBuilder('subject')
            ->join('subject.theme', 'theme')
            ->where('theme.module = :module')
            ->andWhere('theme.active = true')
            ->andWhere('subject.active = true')
            ->setParameter('module', $module)
            ->getQuery()
            ->getResult();
    }

    public function findCurrentSessionOfSubjectForUser(User $user, Subject $subject): array
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->from(ProviderParticipantSessionRole::class, 'role')
            ->select('role')
            ->join('role.session', 'session')
            ->join('role.participant', 'participant')
            ->where('session.subject = :subject')
            ->andWhere('participant.user = :user')
            ->andWhere('session.dateEnd > CURRENT_TIMESTAMP()')
            ->setParameter('user', $user)
            ->setParameter('subject', $subject)
            ->getQuery()
            ->getResult();
    }
}
