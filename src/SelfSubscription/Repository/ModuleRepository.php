<?php

namespace App\SelfSubscription\Repository;

use App\SelfSubscription\Entity\Module;
use App\SelfSubscription\Entity\Subject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModuleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Module::class);
    }

    public function globalSearch(array $search): array
    {
        $resolver = new OptionsResolver();

        $resolver->setDefault('text', null)
            ->setDefault('onlyEnable', 'true')
            ->setAllowedValues('onlyEnable', ['true', 'false'])
            ->setDefault('client', null);
        $search = $resolver->resolve($search);

        $qb = $this->createQueryBuilder('module')
            ->join('module.client', 'client');

        if ($search['client'] !== null) {
            $qb->andWhere('module.client = :client')
                ->setParameter('client', $search['client']);
        }

        if (!empty($search['text'])) {
            $orX = ['module.name LIKE :text', 'module.description LIKE :text', 'module.domain LIKE :text'];
            if ($search['client'] === null) {
                $orX[] = 'client.name LIKE :text';
            }

            $qb->andWhere($qb->expr()->orX(...$orX))
                ->setParameter('text', "%{$search['text']}%");
        }

        if ($search['onlyEnable'] === 'true') {
            $qb->andWhere('module.active = :enable')
                ->setParameter('enable', true);
        }

        return $qb->getQuery()->getResult();
    }

    public function countAllSubjectsFromModule(Module $module): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('count(subject.id)')
            ->from(Subject::class, 'subject')
            ->join('subject.theme', 'theme')
            ->where('theme.module = :module')
            ->setParameter('module', $module);

        return $qb->getQuery()->getSingleScalarResult();
    }
}
