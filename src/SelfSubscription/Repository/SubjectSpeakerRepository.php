<?php

namespace App\SelfSubscription\Repository;

use App\SelfSubscription\Entity\SubjectSpeaker;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

class SubjectSpeakerRepository extends ServiceEntityRepository
{
    protected Security $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, SubjectSpeaker::class);

        $this->security = $security;
    }
}
