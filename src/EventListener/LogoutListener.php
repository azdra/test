<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class LogoutListener
{
    public function __construct(private RouterInterface $router)
    {
    }

    public function onSymfonyComponentSecurityHttpEventLogoutEvent(LogoutEvent $event): void
    {
        if ($event->getRequest()->query->has('domain')) {
            $event->setResponse(new RedirectResponse($this->router->generate('_self_subscription_hub_login', ['clientName' => $event->getRequest()->query->get('slugclient'), 'domain' => $event->getRequest()->query->get('domain')], UrlGeneratorInterface::ABSOLUTE_URL)));
        } else {
            $event->setResponse(new RedirectResponse($this->router->generate('_app_login', referenceType: UrlGeneratorInterface::ABSOLUTE_URL)));
        }
    }
}
