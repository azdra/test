<?php

namespace App\EventListener;

use App\Entity\ActivityLog;
use App\Entity\CategorySessionType;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Event\TransactionalEmailRequestEventInterface;
use App\Event\TransactionalParticipantEmailRequestEvent;
use App\EventSubscriber\TransactionalSessionEmailRequestSubscriber;
use App\Exception\ClientNoMoreCreditAvailableException;
use App\Service\ActivityLogger;
use App\Service\ClientService;
use App\Service\ProviderParticipantService;
use App\Service\Utils\EntityUtils;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Security;

class ProviderParticipantSessionRoleListener
{
    public function __construct(
        private ProviderParticipantService $providerParticipantService,
        private EventDispatcherInterface $eventDispatcher,
        private Security $security,
        private ActivityLogger $activityLogger,
        private EntityUtils $entityUtils,
        private EntityManagerInterface $entityManager,
        private ClientService $clientService,
    ) {
    }

    public function prePersist(ProviderParticipantSessionRole $providerParticipantSessionRole, LifecycleEventArgs $event): void
    {
        $this->providerParticipantService->setProviderSessionAccessToken($providerParticipantSessionRole);
    }

    public function preUpdate(ProviderParticipantSessionRole $providerParticipantSessionRole, LifecycleEventArgs $event): void
    {
        foreach ($this->entityUtils->getEntityChanges($providerParticipantSessionRole) as $field => $values) {
            switch ($field) {
                case 'role':
                    $this->providerParticipantService->setProviderSessionAccessToken($providerParticipantSessionRole);
                    break;
            }
        }
    }

    public function postUpdate(ProviderParticipantSessionRole $providerParticipantSessionRole, LifecycleEventArgs $event): void
    {
        $alreadySent = false;
        foreach ($this->entityUtils->getEntityChanges($providerParticipantSessionRole) as $field => $values) {
            switch ($field) {
                case 'role':
                    if (!$alreadySent) {
                        $this->eventDispatcher->dispatch(new TransactionalParticipantEmailRequestEvent($providerParticipantSessionRole, force: true), TransactionalEmailRequestEventInterface::CONVOCATION_ONE_PARTICIPANT);
                        $alreadySent = true;
                    }
                    break;
                case 'situationMixte':
                    if (!$alreadySent
                        && $providerParticipantSessionRole->getLastConvocationSent() !== null
                        && $providerParticipantSessionRole->getSession()->getCategory() === CategorySessionType::CATEGORY_SESSION_MIXTE
                    ) {
                        $this->eventDispatcher->dispatch(new TransactionalParticipantEmailRequestEvent($providerParticipantSessionRole, force: true), TransactionalEmailRequestEventInterface::CONVOCATION_ONE_PARTICIPANT);
                        $alreadySent = true;
                    }
                    break;
            }
        }
    }

    /**
     * @throws ClientNoMoreCreditAvailableException
     */
    public function postPersist(ProviderParticipantSessionRole $providerParticipantSessionRole, LifecycleEventArgs $event): void
    {
        $session = $providerParticipantSessionRole->getSession();
        $participant = $providerParticipantSessionRole->getParticipant();

        if (!$this->activityLogger->isActivityLogContextInitiated() || ($this->activityLogger->getActivityLogModels() && $this->activityLogger->getActivityLogModels()[0]->data && !array_key_exists('user', $this->activityLogger->getActivityLogModels()[0]->data))) {
            $this->activityLogger->initActivityLogContext(
                $session->getClient(),
                null,
                ActivityLog::ORIGIN_CRON,
                [
                    'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                    'user' => ['id' => $participant->getUserId(), 'name' => $participant->getFullName()],
                ]
            );
        }

        if (!$session->isSessionTest()
            && $session->getCategory() !== CategorySessionType::CATEGORY_SESSION_REUNION
            && $session->getStatus() === ProviderSession::STATUS_SCHEDULED
            && $providerParticipantSessionRole->getRole() !== ProviderParticipantSessionRole::ROLE_ANIMATOR) {
            $this->clientService->clientUseCreditParticipants($session->getClient());
        }

        if (TransactionalSessionEmailRequestSubscriber::participantWantToReceiveConvocation($providerParticipantSessionRole)) {
            $this->eventDispatcher->dispatch(new TransactionalParticipantEmailRequestEvent($providerParticipantSessionRole), TransactionalEmailRequestEventInterface::CONVOCATION_ONE_PARTICIPANT);
        }
        if (!$this->activityLogger->isActivityLogContextInitiated()) {
            $this->activityLogger->flushActivityLogs();
        }
        $this->entityManager->flush();
    }

    public function preRemove(ProviderParticipantSessionRole $providerParticipantSessionRole, LifecycleEventArgs $event): void
    {
        $session = $providerParticipantSessionRole->getSession();
        if (!$session->isSessionTest()
            && $session->getCategory() !== CategorySessionType::CATEGORY_SESSION_REUNION
            && $session->getStatus() === ProviderSession::STATUS_SCHEDULED
            && $providerParticipantSessionRole->getRole() !== ProviderParticipantSessionRole::ROLE_ANIMATOR) {
            $providerParticipantSessionRole->getSession()->getClient()->decrementParticipantCount();
        }

        if ($providerParticipantSessionRole->getParticipant()->isAllowToSendCanceledMail()) {
            $this->eventDispatcher->dispatch(new TransactionalParticipantEmailRequestEvent($providerParticipantSessionRole, $this->security->getUser()), TransactionalEmailRequestEventInterface::UNSUBSCRIBE);
        }
    }
}
