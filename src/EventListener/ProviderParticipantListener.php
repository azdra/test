<?php

namespace App\EventListener;

use App\Entity\ProviderParticipant;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class ProviderParticipantListener
{
    public function __construct(
    ) {
    }

    public function preRemove(ProviderParticipant $providerParticipant, LifecycleEventArgs $event): void
    {
        $providerParticipant->setAllowToSendCanceledMail(true);
    }
}
