<?php

namespace App\EventListener;

use App\Entity\Client\Client;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\String\Slugger\SluggerInterface;

class ClientListener implements EventSubscriber
{
    public function __construct(private SluggerInterface $slugger)
    {
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $client = $args->getObject();

        if (!$client instanceof Client) {
            return;
        }

        $client->setSlug($this->slugger->slug($client->getName())->lower()->toString());
    }
}
