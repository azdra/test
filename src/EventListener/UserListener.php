<?php

namespace App\EventListener;

use App\Entity\ActivityLog;
use App\Entity\User;
use App\Model\ActivityLog\ActivityLogModel;
use App\Service\ActivityLogger;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Routing\RouterInterface;

class UserListener
{
    public function __construct(private ActivityLogger $activityLogger, private RouterInterface $router)
    {
    }

    public function postPersist(User $user, LifecycleEventArgs $eventArgs): void
    {
        $infos = [
            'message' => ['key' => 'Creating a participant <a href="%route%">%userName%</a>', 'params' => ['%route%' => $this->router->generate('_participant_edit', ['id' => $user->getId()]), '%userName%' => $user->getFullName()]],
            'user' => ['id' => $user->getId(), 'email' => $user->getEmail()],
            ];
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_USER_ADD, ActivityLog::SEVERITY_INFORMATION, $infos), true);
    }

    public function postUpdate(User $user, LifecycleEventArgs $event): void
    {
        $infos = [
            'message' => ['key' => 'Editing a participant <a href="%route%">%userName%</a>', 'params' => ['%route%' => $this->router->generate('_participant_edit', ['id' => $user->getId()]), '%userName%' => $user->getFullName()]],
            'user' => ['id' => $user->getId(), 'email' => $user->getEmail()],
            ];
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_USER_UPDATE, ActivityLog::SEVERITY_INFORMATION, $infos), true);
    }

    public function preRemove(User $user, LifecycleEventArgs $event): void
    {
        $infos = [
            'message' => ['key' => 'Participant deleted <a href="#">%userName%</a>', 'params' => ['%userName%' => $user->getFullName()]],
            'user' => ['id' => $user->getId(), 'email' => $user->getEmail()],
            ];
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_USER_DELETE, ActivityLog::SEVERITY_INFORMATION, $infos), true);
    }
}
