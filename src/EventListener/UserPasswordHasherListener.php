<?php

namespace App\EventListener;

use App\Entity\User;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserPasswordHasherListener
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function prePersist(User $user, LifecycleEventArgs $event): void
    {
        $this->hashUserClearPassword($user);
    }

    public function preUpdate(User $user, LifecycleEventArgs $event): void
    {
        $this->hashUserClearPassword($user);
    }

    private function hashUserClearPassword(User $user): void
    {
        if (!empty($user->getClearPassword())) {
            $user->setPassword($this->passwordHasher->hashPassword($user, $user->getClearPassword()));
            $user->setClearPassword(null);
        }
    }
}
