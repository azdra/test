<?php

namespace App\EventListener;

use App\Entity\ActivityLog;
use App\Entity\CategorySessionType;
use App\Entity\Client\ClientServices;
use App\Entity\ProviderSession;
use App\Exception\ClientNoMoreCreditAvailableException;
use App\Exception\InvalidArgumentException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Service\ActivityLogger;
use App\Service\ClientService;
use App\Service\MailerService;
use App\Service\SupportService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProviderSessionListener
{
    public function __construct(
        private Security $security,
        private ActivityLogger $activityLogger,
        private RouterInterface $router,
        private MailerService $mailerService,
        private SupportService $supportService,
        private TranslatorInterface $translator,
        private string $supportMail,
        private string $endPointDomain,
        private ClientService $clientService,
        private EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * @throws ClientNoMoreCreditAvailableException
     */
    public function prePersist(ProviderSession $session, LifecycleEventArgs $event): void
    {
        if (null !== ($user = $this->security->getUser())) {
            $session->setCreatedBy($user);
            $session->setUpdatedBy($user);
        }

        $session->setUpdatedAt(new \DateTime());

        if (!$session->isSessionTest() && $session->getCategory() !== CategorySessionType::CATEGORY_SESSION_REUNION) {
            $this->clientService->useCreditClient($session->getClient(), $session->getTraineesCount());
        }
    }

    /**
     * @throws InvalidArgumentException
     */
    public function postPersist(ProviderSession $session, LifecycleEventArgs $eventArgs): void
    {
        $message_key = 'Creating the  session  <a href="%route%">%sessionName%</a> (ref: %ref%)';
        if ($session->getCategory()->value === CategorySessionType::CATEGORY_SESSION_WEBINAR->value && !$session->hasACommunicationConfirmation()) {
            $message_key = 'Creating the  session  <a href="%route%">%sessionName%</a> (ref: %ref%) is locked in the draft state until you add a communication scheme';
        }
        $infos = [
             'message' => ['key' => $message_key, 'params' => ['%route%' => $this->endPointDomain.$this->router->generate('_session_get', ['id' => $session->getId()]), '%sessionName%' => $session->getName(), '%ref%' => $session->getRef()]],
             'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
         ];

        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SESSION_CREATION, ActivityLog::SEVERITY_INFORMATION, $infos), true);

        if ($session->getClient()->getServices()->getAssistanceType() === ClientServices::ENUM_ON_DEMAND && $session->getAssistanceType() === ProviderSession::ENUM_LAUNCH) {
            $this->mailerService->sendTemplatedMail(
                $this->supportMail,
                $this->translator->trans('A new assistance request for a session'),
                'emails/session_new_assistance_request.html.twig',
                    context: [
                    'session' => $session,
                    'routeSession' => $this->router->generate('_session_get', ['id' => $session->getId()], 0),
                    'lang' => $session->getClient()->getDefaultLang(),
                ],
                    metadatas: [
                    'origin' => MailerService::ORIGIN_EMAIL_SESSION_NEW_ASSISTANCE_REQUEST,
                    'session' => $session->getId(),
                ],
            );
        }

        if ($session->getSupportType() == ProviderSession::ENUM_SUPPORT_LAUNCH || $session->getSupportType() == ProviderSession::ENUM_SUPPORT_TOTAL) {
            $details = $this->supportService->getDetailsSessionSupport($session->getClient(), $session->getDateStart(), $session->getDateEnd(), $session->isStandardHours(), $session->isOutsideStandardHours(), $session->getSupportType());
            if (!empty($details['alertSupport'])) {
                $infos = [
                    'message' => ['key' => 'The session is scheduled outside the standard hours. Support will be provided during the standard hours in accordance with your contract. To benefit from this service, you can adjust the  <a href="%route%">%session%</a> session hours.', 'params' => ['%route%' => $this->router->generate('_session_get', ['id' => $session->getId()]), '%session%' => $session->getName()]],
                    'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                ];
                $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_ALERT_NEW_SUPPORT_REQUEST, ActivityLog::SEVERITY_INFORMATION, $infos), true);

                $this->mailerService->sendTemplatedMail(
                    $this->supportMail,
                    $this->translator->trans('A new support request for a session'),
                    'emails/session_new_support_request.html.twig',
                    context: [
                    'session' => $session,
                    'routeSession' => $this->router->generate('_session_get', ['id' => $session->getId()], 0),
                    'lang' => 'fr',
                ],
                    metadatas: [
                    'origin' => MailerService::ORIGIN_EMAIL_SESSION_NEW_SUPPORT_REQUEST,
                    'session' => $session->getId(),
                ],
                );
            }
        }
    }

    /**
     * @throws ClientNoMoreCreditAvailableException
     */
    public function preUpdate(ProviderSession $session, PreUpdateEventArgs $event): void
    {
        if (null !== ($user = $this->security->getUser())) {
            $session->setUpdatedBy($user);
        }

        $changesSet = $event->getEntityChangeSet();
        if (array_key_exists('sessionTest', $changesSet)) {
            $oldValueTestCheck = $changesSet['sessionTest'][0];
            $newValueTestCheck = $changesSet['sessionTest'][1];
            if ($oldValueTestCheck !== $newValueTestCheck && $session->getCategory() !== CategorySessionType::CATEGORY_SESSION_REUNION) {
                if ($newValueTestCheck) {
                    $session->getClient()->decrementCreditClient($session->getTraineesCount());
                } else {
                    $this->clientService->useCreditClient($session->getClient(), $session->getTraineesCount());
                }
            }
        } elseif (array_key_exists('category', $changesSet)) {
            $oldValueCategoryType = $changesSet['category'][0];
            $newValueCategoryType = $changesSet['category'][1];
            if ($oldValueCategoryType !== $newValueCategoryType && !$session->isSessionTest()) {
                if ($newValueCategoryType == CategorySessionType::CATEGORY_SESSION_REUNION->value) {
                    $session->getClient()->decrementCreditClient($session->getTraineesCount());
                } elseif ($oldValueCategoryType === CategorySessionType::CATEGORY_SESSION_REUNION->value) {
                    $this->clientService->useCreditClient($session->getClient(), $session->getTraineesCount());
                }
            }
        } elseif (array_key_exists('status', $changesSet)) {
            $oldValueStatus = $changesSet['status'][0];
            $newValueStatus = $changesSet['status'][1];
            if ($oldValueStatus !== $newValueStatus && !$session->isSessionTest()) {
                if ($oldValueStatus === ProviderSession::STATUS_SCHEDULED && ($newValueStatus === ProviderSession::STATUS_DRAFT || $newValueStatus === ProviderSession::STATUS_CANCELED)) {
                    $session->getClient()->decrementCreditClient($session->getTraineesCount());
                } elseif (($oldValueStatus === ProviderSession::STATUS_DRAFT || $oldValueStatus === ProviderSession::STATUS_CANCELED) && $newValueStatus === ProviderSession::STATUS_SCHEDULED) {
                    $this->clientService->useCreditClient($session->getClient(), $session->getTraineesCount());
                }
            }
        }
    }

    public function preRemove(ProviderSession $session, LifecycleEventArgs $event): void
    {
        $infos = [
            'session' => $session->dumpSessionBeforeDelete(),
            'message' => ['key' => 'Deleting the <a href="#">%sessionName%</a> session (ref: %ref%)', 'params' => ['%sessionName%' => $session->getName(), '%ref' => $session->getRef()]],
            'links' => [['type' => 'component', 'name' => 'ModalSessionDeleteInfo']],
        ];
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SESSION_DELETE, ActivityLog::SEVERITY_INFORMATION, $infos), true);

        if (!$session->isSessionTest() && $session->getStatus() === ProviderSession::STATUS_SCHEDULED) {
            $session->getClient()->decrementCreditClient($session->getTraineesCount());
        }
    }
}
