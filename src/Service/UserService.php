<?php

namespace App\Service;

use App\Entity\Client\Client;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Exception\AsyncWorker\AsyncTaskException;
use App\Exception\UnknownProviderConfigurationHolderException;
use App\Repository\ParticipantSessionSignatureRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Security\UserSecurityService;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Security\Core\Security;

class UserService
{
    public function __construct(
        private UserSecurityService $userSecurityService,
        private UserRepository $userRepository,
        private ProviderSessionRepository $providerSessionRepository,
        private ParticipantSessionSubscriber $participantSessionSubscriber,
        private EntityManagerInterface $entityManager,
        private Security $security,
        private ParticipantSessionSignatureRepository $sessionSignatureRepository,
        private MailerService $mailer,
    ) {
    }

    public function createUser(?array $roles = null): User
    {
        $user = new User();

        $this->initUser($user, $roles);

        return $user;
    }

    public function createUserCustomerService(): User
    {
        $user = new User();
        $user->setClearPassword($this->userSecurityService->randomPassword());
        $user->setRoles(['ROLE_CUSTOMER_SERVICE']);
        $user->setStatus(User::STATUS_ACTIVE);
        $user->setPreferredInitialRoute('_service_client');

        return $user;
    }

    public function initUser(User $user, ?array $roles = null): void
    {
        $user->setClearPassword($this->userSecurityService->randomPassword());

        if (!empty($roles)) {
            $user->setRoles($roles);
        }

        /** @var User $loggedUser */
        $loggedUser = $this->security->getUser();
        $user->setClient($loggedUser->getClient());
    }

    public function changeDefaultClient(User $user, Client $selectedClient): User
    {
        $accounts = $this->userRepository->findAllUserAccounts($user);

        /** @var User $account */
        foreach ($accounts as $account) {
            if ($account->getClient() === $selectedClient) {
                $account->setStatus(User::STATUS_ACTIVE);
            } else {
                $account->setStatus(User::STATUS_INACTIVE);
            }

            $this->entityManager->persist($account);
        }

        return $user;
    }

    public function anonymize(User $user): void
    {
        $anonymizationKey = uniqid();

        $user
            ->setEmail('rgpd_'.(new \DateTime())->format('YmdHis').'_'.$anonymizationKey.'@live-session.fr')
            ->setFirstName($anonymizationKey.'_FirstName')
            ->setLastName($anonymizationKey.'_LastName');

        $this->sessionSignatureRepository->anonymizeUserSignature($user);
    }

    public function getUserProfil(array $allUserEmail): ?User
    {
        foreach ($allUserEmail as $userEmail) {
            if ($userEmail->isAnAdmin()) {
                return $userEmail;
            }
        }

        if (empty($allUserEmail)) {
            return null;
        }

        return $allUserEmail[0];
    }

    public function sendMailToClientUsersSubscribeToEmail(Client $client, string $emailType, string $subject, string $template, array $context = [], null|array|string $pathToAttach = null, array $metadatas = null, Address|string|array $ccs = null, Address|string|array $bccs = null): void
    {
        /** @var User[] $userManagers */
        $userManagers = $this->userRepository->findManagerListByClient($client);
        foreach ($userManagers as $user) {
            if ($user->isSubscribeToAlert($emailType)) {
                $this->mailer->sendTemplatedMail($user->getEmail(), $subject, $template, $context, $pathToAttach, $metadatas, $ccs, $bccs, true);
            }
        }
    }

    public function isValidMail(User $user): bool
    {
        return (bool) preg_match(
            "/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,9}$/ix", $user->getEmail()
        );
    }

    public function isUpdatedEmail(User $user): bool
    {
        return $user->getEmail() !== $this->entityManager->getUnitOfWork()->getOriginalEntityData($user)['email'];
    }

    /**
     * @throws UnknownProviderConfigurationHolderException
     * @throws AsyncTaskException
     */
    public function updateSessionsOldEmail(array $dataUserSession, User $user): void
    {
        $rolesData = $this->unsubscribeOldParticipantInAlSessions($dataUserSession, $user);
        $this->subscribeNewParticipantInAlSessions($dataUserSession, $user, $rolesData);
    }

    public function unsubscribeOldParticipantInAlSessions(array $dataUserSession, User $user): array
    {
        $rolesData = [];
        foreach ($dataUserSession as $sessionData) {
            $session = $this->providerSessionRepository->findOneBy(['id' => $sessionData['session_id']]);
            if ($session->getReadableState() != ProviderSession::FINAL_STATUS_FINISHED && $session->getReadableState() != ProviderSession::FINAL_STATUS_CANCELED) {
                $ppsr = $this->findParticipantSessionRole(session: $session, user: $user);
                if ($ppsr != null) {
                    $rolesData[$sessionData['session_id']] = $ppsr->getRole();
                    $ppsr->getParticipant()->setAllowToSendCanceledMail(false);
                    $this->participantSessionSubscriber->unsubscribe($ppsr);
                    $this->entityManager->remove($ppsr->getParticipant());
                }
            }
        }
        $this->entityManager->flush();

        return $rolesData;
    }

    public function subscribeNewParticipantInAlSessions(array $dataUserSession, User $user, array $rolesData): void
    {
        foreach ($dataUserSession as $sessionData) {
            $session = $this->providerSessionRepository->findOneBy(['id' => $sessionData['session_id']]);
            if ($session->getReadableState() != ProviderSession::FINAL_STATUS_FINISHED && $session->getReadableState() != ProviderSession::FINAL_STATUS_CANCELED) {
                $this->participantSessionSubscriber->subscribe($session, $user, $rolesData[$sessionData['session_id']]);
                $this->entityManager->flush();
            }
        }
    }

    public function findParticipantSessionRole(ProviderSession $session, User $user): ?ProviderParticipantSessionRole
    {
        foreach ($session->getParticipantRoles() as $participantRole) {
            if ($participantRole->getParticipant()->getUser()->getEmail() == $user->getEmail()) {
                return $participantRole;
            }
        }

        return null;
    }
}
