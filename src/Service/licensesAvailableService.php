<?php

namespace App\Service;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\User;
use App\Exception\InvalidArgumentException;
use App\Repository\AbstractProviderConfigurationHolderRepository;
use App\Repository\ProviderSessionRepository;
use DateTimeImmutable;
use Symfony\Contracts\Translation\TranslatorInterface;

class licensesAvailableService
{
    public function __construct(
        private readonly AbstractProviderConfigurationHolderRepository $configurationHolderRepository,
        private readonly ProviderSessionRepository $providerSessionRepository,
        private readonly MailerService $mailerService,
        private readonly TranslatorInterface $translator,
    ) {
    }

    public function getWebexConfigurationHolder(?User $user): array
    {
        $configurationsHolderWebex = [];
        if (!$user) {
            return $configurationsHolderWebex;
        }

        return $this->getWebexConfigurationHolderFromClient($user->getClient());
    }

    public function getWebexConfigurationHolderFromClient(?Client $client): array
    {
        $configurationsHolderWebex = [];
        if (!$client) {
            return $configurationsHolderWebex;
        }
        foreach ($client->getConfigurationHolders()->getValues() as $configurationHolder) {
            if (($configurationHolder instanceof WebexConfigurationHolder || $configurationHolder instanceof WebexRestConfigurationHolder) && !$configurationHolder->isArchived()) {
                $configurationsHolderWebex[] = $configurationHolder;
            }
        }

        return $configurationsHolderWebex;
    }

    /**
     * @throws \Exception
     */
    public function getTimeSlotDataAvailableLicenses(array $options): array
    {
        $configurationHolder = $this->configurationHolderRepository->findOneBy(['id' => $options['connectorId']]);
        $pullerLicenses = $configurationHolder->getLicences();
        $dataClient = $this->getDataClient(count($pullerLicenses), $configurationHolder->getClient());
        $sessions = $this->providerSessionRepository->findAllBetweenDatesWithConfigurationHolder(from: new DateTimeImmutable($options['start']), to: new DateTimeImmutable($options['end']), idConfigurationHolder: $options['connectorId']);

        $licensesUsed = $this->getLicensesUsedData(sessions: $sessions, pullerLicenses: $pullerLicenses);

        return $this->orderAndCompletDataLicensesAvailable(licensesUsed: $licensesUsed, endCalendarDate: (new DateTimeImmutable($options['end']))->format('Y-m-d'), dataClient: $dataClient);
    }

    public function orderAndCompletDataLicensesAvailable(array $licensesUsed, string $endCalendarDate, array $dataClient): array
    {
        $dataLicensesAvailableOrdered = [];
        $immediateDate = (new DateTimeImmutable())->format('Y-m-d');
        $day = 0;
        while ($immediateDate != $endCalendarDate) {
            if (array_key_exists($immediateDate, $licensesUsed)) {
                foreach ($licensesUsed[$immediateDate] as $time => $nombreLicensesUsed) {
                    $dataLicensesAvailableOrdered[] = $this->getTimeSlotInformations(immediateDate: $immediateDate, time: $time, nombreLicensesUsed: $nombreLicensesUsed, dataClient: $dataClient);
                }
            } else {
                $dataLicensesAvailableOrdered[] = $this->getTimeSlotInformations(immediateDate: $immediateDate, time: 'morning', nombreLicensesUsed: 0, dataClient: $dataClient);
                $dataLicensesAvailableOrdered[] = $this->getTimeSlotInformations(immediateDate: $immediateDate, time: 'afternoon', nombreLicensesUsed: 0, dataClient: $dataClient);
            }
            ++$day;
            $immediateDate = (new DateTimeImmutable())->modify('+ '.$day.' day')->format('Y-m-d');
        }

        return $dataLicensesAvailableOrdered;
    }

    public function getTimeSlotInformations(string $immediateDate, string $time, int $nombreLicensesUsed, array $dataClient): array
    {
        $timeStartEvent = match ($time) {
            'morning' => '09:00:00',
          'afternoon' => '13:00:00',
        };
        $timeEndEvent = match ($time) {
            'morning' => '12:00:00',
            'afternoon' => '17:00:00',
        };

        return [
            'start' => $immediateDate.' '.$timeStartEvent,
            'end' => $immediateDate.' '.$timeEndEvent,
            'numberLicensesAvailable' => $dataClient['pullerLicensesCount'] - $nombreLicensesUsed,
            'thresholdMiddle' => $dataClient['thresholdMiddle'],
            'thresholdMin' => $dataClient['thresholdMin'],
            'numberLicensesUsed' => $nombreLicensesUsed,
            'pullerLicensesCount' => $dataClient['pullerLicensesCount'],
        ];
    }

    public function getLicensesUsedData(array $sessions, array $pullerLicenses): array
    {
        $licensesUsed = [];
        foreach ($sessions as $session) {
            $numberAnimatorLicenses = $this->getNumberAnimatorLicenseOnly(animators: $session->getAnimatorsOnly(), pullerLicenses: $pullerLicenses);
            $dateStart = (clone $session->getDateStart())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()));
            $hoursStart = $dateStart->format('H');
            $hoursEnd = (clone $session->getDateEnd())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('H');
            if (array_key_exists($dateStart->format('Y-m-d'), $licensesUsed)) {
                if ($hoursStart < 13 && $hoursEnd < 13) {
                    $licensesUsed[$dateStart->format('Y-m-d')]['morning'] += $numberAnimatorLicenses;
                } elseif ($hoursStart >= 13) {
                    $licensesUsed[$dateStart->format('Y-m-d')]['afternoon'] += $numberAnimatorLicenses;
                } else {
                    $licensesUsed[$dateStart->format('Y-m-d')]['morning'] += $numberAnimatorLicenses;
                    $licensesUsed[$dateStart->format('Y-m-d')]['afternoon'] += $numberAnimatorLicenses;
                }
            } else {
                if ($hoursStart < 13 && $hoursEnd < 13) {
                    $licensesUsed[$dateStart->format('Y-m-d')]['morning'] = $numberAnimatorLicenses;
                    $licensesUsed[$dateStart->format('Y-m-d')]['afternoon'] = 0;
                } elseif ($hoursStart >= 13) {
                    $licensesUsed[$dateStart->format('Y-m-d')]['morning'] = 0;
                    $licensesUsed[$dateStart->format('Y-m-d')]['afternoon'] = $numberAnimatorLicenses;
                } else {
                    $licensesUsed[$dateStart->format('Y-m-d')]['morning'] = $numberAnimatorLicenses;
                    $licensesUsed[$dateStart->format('Y-m-d')]['afternoon'] = $numberAnimatorLicenses;
                }
            }
        }

        return $licensesUsed;
    }

    public function getNumberAnimatorLicenseOnly(array $animators, array $pullerLicenses): int
    {
        $animatorLicenses = [];
        foreach ($animators as $animator) {
            if (($animator->getParticipant()->getUser()->isAWebexMeetingLicence() || $animator->getParticipant()->getUser()->isAWebexRestMeetingLicence()) && in_array($animator->getParticipant()->getUser()->getEmail(), $pullerLicenses)) {
                $animatorLicenses[] = $animator->getParticipant()->getUser()->getEmail();
            }
        }

        return count($animatorLicenses);
    }

    public function getDataClient(int $countPullerLicenses, Client $client): array
    {
        $dataClient['pullerLicensesCount'] = $countPullerLicenses;
        $dataClient['thresholdMiddle'] = $client->getLicensesAvailable()->getThresholdMiddle();
        $dataClient['thresholdMin'] = $client->getLicensesAvailable()->getThresholdMin();

        return $dataClient;
    }

    /**
     * @throws \Exception
     */
    public function getSlotLowAmountLicense(array $timeSlotData): array
    {
        $resultSearch['haveThresholdMin'] = false;
        $resultSearch['thresholdMiddle'] = false;
        $slotLowAmountLicense = [];
        foreach ($timeSlotData as $slotDatum) {
            if ($slotDatum['numberLicensesAvailable'] <= $slotDatum['thresholdMin']) {
                $resultSearch['haveThresholdMin'] = true;
                $slotLowAmountLicense[] = [
                    'time' => $this->getTimeIdentifier($slotDatum['start']),
                    'numberLicensesAvailable' => $slotDatum['numberLicensesAvailable'],
                    'date' => (new DateTimeImmutable(substr($slotDatum['start'], 0, 10)))->format('d/m/Y'),
                    'status' => 'alert',
                ];
            } elseif ($slotDatum['numberLicensesAvailable'] <= $slotDatum['thresholdMiddle']) {
                $resultSearch['thresholdMiddle'] = true;
                $slotLowAmountLicense[] = [
                    'time' => $this->getTimeIdentifier($slotDatum['start']),
                    'numberLicensesAvailable' => $slotDatum['numberLicensesAvailable'],
                    'date' => (new DateTimeImmutable(substr($slotDatum['start'], 0, 10)))->format('d/m/Y'),
                    'status' => 'warning',
                ];
            }
        }
        $resultSearch['slotLowAmountLicense'] = $slotLowAmountLicense;

        return $resultSearch;
    }

    public function getTimeIdentifier(string $date): string
    {
        if (substr($date, 11, 2) == '09') {
            return 'morning';
        }

        return 'afternoon';
    }

    /**
     * @throws InvalidArgumentException
     */
    public function sendDaysLowAmountLicense(AbstractProviderConfigurationHolder $configurationHolder, array $dataToSent): void
    {
        $subject = match ($dataToSent['thresholdMiddle']) {
            true => $this->translator->trans('List of slots that have less than %threshold% licenses available', [
                '%threshold%' => $configurationHolder->getClient()->getLicensesAvailable()->getThresholdMiddle(),
            ]),
            default => $this->translator->trans('List of slots that have less than %threshold% licenses available', [
                '%threshold%' => $configurationHolder->getClient()->getLicensesAvailable()->getThresholdMin(),
            ])
        };

        $context = [
            'configurationHolderName' => $configurationHolder->getName(),
            'client' => $configurationHolder->getClient(),
            'daysToSendMail' => $dataToSent['slotLowAmountLicense'],
            'showMediumLess' => $dataToSent['thresholdMiddle'],
            'showMinLess' => $dataToSent['haveThresholdMin'],
            'thresholdMin' => $configurationHolder->getClient()->getLicensesAvailable()->getThresholdMin(),
            'thresholdMiddle' => $configurationHolder->getClient()->getLicensesAvailable()->getThresholdMiddle(),
        ];
        foreach ($configurationHolder->getClient()->getLicensesAvailable()->getLicensesReportRecipients() as $reception) {
            $this->mailerService->sendTemplatedMail(
               address: $reception,
               subject: $subject,
               template: 'emails/licensesAvailable/listDaysLowAmountMail.html.twig',
               context: $context,
           );
        }
    }
}
