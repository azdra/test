<?php

namespace App\Service;

use App\Entity\Client\Client;
use App\Entity\ProviderSession;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SupportService
{
    public function __construct(
        private TranslatorInterface $translator,
        private LoggerInterface $logger,
    ) {
    }

    public function getOverlappedCustomer(\DateTimeImmutable $sessionDateStart, \DateTimeImmutable $sessionDateEnd): array
    {
        /**
         *       OUTSIDE STANDARDS HOURS <| STANDARDS HOURS     |> OUTSIDE STANDARDS HOURS
         * beforeAM  <| HHSAMStart > HHSAMEnd   | HSAMStart > HSAMEnd | HHSLHStart > HHSLHSEnd  | HSPMStart > HSPMEnd  | HHSPMStart > HHSPMEnd   |> afterPM
         * before 8h30 <| 8h30 (am) > 17h30 (pm) |> after 17h30 (pm)
         * HS           |----------------------|
         * HHS *********|----------------------|*********.
         */
        $dateBeforeHS = ( new \DateTimeImmutable() )->setTime(00, 00);
        $dateSessionStart = ( new \DateTimeImmutable() )->setTime($sessionDateStart->format('G'), $sessionDateStart->format('i'));
        $dateSessionEnd = ( new \DateTimeImmutable() )->setTime($sessionDateEnd->format('G'), $sessionDateEnd->format('i'));

        //dump($dateSessionStart->format('G:i').' | '.$dateSessionEnd->format('G:i'));

        $dateHSStart = ( new \DateTimeImmutable() )->setTime(8, 30);
        $dateHSEnd = ( new \DateTimeImmutable() )->setTime(17, 30);

        $dateAfterHS = ( new \DateTimeImmutable() )->setTime(23, 59);

        $timeSlotsMinutes = [
            'slotsDetails' => [
                'beforeHS' => $dateBeforeHS->format('H:i').' > '.$dateHSStart->format('H:i'),
                'inHSStartAndHSEnd' => $dateHSStart->format('H:i').' > '.$dateHSEnd->format('H:i'),
                'afterHS' => $dateHSEnd->format('H:i').' > '.$dateAfterHS->format('H:i'),
            ],
            'slotsResults' => [
                'beforeHS' => $this->overlapInMinutes($dateSessionStart, $dateSessionEnd, $dateBeforeHS, $dateHSStart),
                'inHSStartAndHSEnd' => $this->overlapInMinutes($dateSessionStart, $dateSessionEnd, $dateHSStart, $dateHSEnd),
                'afterHS' => $this->overlapInMinutes($dateSessionStart, $dateSessionEnd, $dateHSEnd, $dateAfterHS),
            ],
        ];

        return $timeSlotsMinutes;
    }

    public function getOverlapped(Client $client, \DateTime $sessionDateStart, \DateTime $sessionDateEnd): array
    {
        /**
         *       OUT <| OUTSIDE STANDARDS HOURS | STANDARDS HOURS     | OUTSIDE STANDARDS HOURS | STANDARDS HOURS      | OUTSIDE STANDARDS HOURS | OUT
         * beforeAM  <| HHSAMStart > HHSAMEnd   | HSAMStart > HSAMEnd | HHSLHStart > HHSLHSEnd  | HSPMStart > HSPMEnd  | HHSPMStart > HHSPMEnd   |> afterPM
         * before 7h <|    7h (am) > 9h (am)    | 9h (am) > 12h30 (pm)| 12h30 (pm) > 14 (pm)    | 14 (pm) > 17h30 (pm) | 17h30 (pm) > 19h (pm)   |> after 19h (pm)
         * HS            |---------|    |---------|
         * HHS      |****|---------|****|---------|****|
         * OUT XXXXX|****|---------|****|---------|****|XXXXX.
         */
        $dateBeforeAM = ( new \DateTime() )->setTime(00, 00);
        $dateSessionStart = ( new \DateTime() )->setTime($sessionDateStart->format('G'), $sessionDateStart->format('i'));
        $dateSessionEnd = ( new \DateTime() )->setTime($sessionDateEnd->format('G'), $sessionDateEnd->format('i'));

        $dateHHSAMStart = ( new \DateTime() )->setTime($client->getServices()->getOutsideStandardHours()->getStartTimeAM()->format('G'), $client->getServices()->getOutsideStandardHours()->getStartTimeAM()->format('i'));
        $dateHHSAMEnd = ( new \DateTime() )->setTime($client->getServices()->getOutsideStandardHours()->getEndTimeAM()->format('G'), $client->getServices()->getOutsideStandardHours()->getEndTimeAM()->format('i'));

        $dateHSAMStart = ( new \DateTime() )->setTime($client->getServices()->getStandardHours()->getStartTimeAM()->format('G'), $client->getServices()->getStandardHours()->getStartTimeAM()->format('i'));
        $dateHSAMEnd = ( new \DateTime() )->setTime($client->getServices()->getStandardHours()->getEndTimeAM()->format('G'), $client->getServices()->getStandardHours()->getEndTimeAM()->format('i'));

        $dateHHSLHStart = ( new \DateTime() )->setTime($client->getServices()->getOutsideStandardHours()->getStartTimeLH()->format('G'), $client->getServices()->getOutsideStandardHours()->getStartTimeLH()->format('i'));
        $dateHHSLHEnd = ( new \DateTime() )->setTime($client->getServices()->getOutsideStandardHours()->getEndTimeLH()->format('G'), $client->getServices()->getOutsideStandardHours()->getEndTimeLH()->format('i'));

        $dateHSPMStart = ( new \DateTime() )->setTime($client->getServices()->getStandardHours()->getStartTimePM()->format('G'), $client->getServices()->getStandardHours()->getStartTimePM()->format('i'));
        $dateHSPMEnd = ( new \DateTime() )->setTime($client->getServices()->getStandardHours()->getEndTimePM()->format('G'), $client->getServices()->getStandardHours()->getEndTimePM()->format('i'));

        $dateHHSPMStart = ( new \DateTime() )->setTime($client->getServices()->getOutsideStandardHours()->getStartTimePM()->format('G'), $client->getServices()->getOutsideStandardHours()->getStartTimePM()->format('i'));
        $dateHHSPMEnd = ( new \DateTime() )->setTime($client->getServices()->getOutsideStandardHours()->getEndTimePM()->format('G'), $client->getServices()->getOutsideStandardHours()->getEndTimePM()->format('i'));

        $dateAfterPM = ( new \DateTime() )->setTime(23, 59);

        $timeSlotsMinutes = [
            'slotsDetails' => [
                'beforeHHS' => $dateBeforeAM->format('H:i').' > '.$dateHHSAMStart->format('H:i'),
                'inHHSAMStartAndHSAMStart' => $dateHHSAMStart->format('H:i').' > '.$dateHHSAMEnd->format('H:i'),
                'inHSAMStartAndHHSLHStart' => $dateHSAMStart->format('H:i').' > '.$dateHSAMEnd->format('H:i'),
                'inHHSLHStartHAndHSPMStart' => $dateHHSLHStart->format('H:i').' > '.$dateHHSLHEnd->format('H:i'),
                'inHSPMStartAndHHSPMStart' => $dateHSPMStart->format('H:i').' > '.$dateHSPMEnd->format('H:i'),
                'inHHSPMStartHandHHSPMEnd' => $dateHHSPMStart->format('H:i').' > '.$dateHHSPMEnd->format('H:i'),
                'afterHHS' => $dateHHSPMEnd->format('H:i').' > '.$dateAfterPM->format('H:i'),
            ],
            'slotsResults' => [
                'beforeHHS' => $this->overlapInMinutes($dateSessionStart, $dateSessionEnd, $dateBeforeAM, $dateHHSAMStart),
                'inHHSAMStartAndHSAMStart' => $this->overlapInMinutes($dateSessionStart, $dateSessionEnd, $dateHHSAMStart, $dateHHSAMEnd),
                'inHSAMStartAndHHSLHStart' => $this->overlapInMinutes($dateSessionStart, $dateSessionEnd, $dateHSAMStart, $dateHSAMEnd),
                'inHHSLHStartHAndHSPMStart' => $this->overlapInMinutes($dateSessionStart, $dateSessionEnd, $dateHHSLHStart, $dateHHSLHEnd),
                'inHSPMStartAndHHSPMStart' => $this->overlapInMinutes($dateSessionStart, $dateSessionEnd, $dateHSPMStart, $dateHSPMEnd),
                'inHHSPMStartHandHHSPMEnd' => $this->overlapInMinutes($dateSessionStart, $dateSessionEnd, $dateHHSPMStart, $dateHHSPMEnd),
                'afterHHS' => $this->overlapInMinutes($dateSessionStart, $dateSessionEnd, $dateHHSPMEnd, $dateAfterPM),
            ],
        ];

        return $timeSlotsMinutes;
    }

    public function getDetailsSessionSupport(Client $client, \DateTime $sessionDateStart, \DateTime $sessionDateEnd, bool $sessionStandardHours = true, bool $sessionOutsideStandardHours = false, int $typeSupport = ProviderSession::ENUM_SUPPORT_NO): array
    {
        try {
            $launchBeforeSession = $client->getServices()->getTimeBeforeSession();
            $durationAfterLaunch = $client->getServices()->getTimeSupportDurationSession();
            if ($typeSupport === ProviderSession::ENUM_SUPPORT_LAUNCH) {
                $realStartSupport = (clone $sessionDateStart)->modify('-'.$launchBeforeSession.' mins');
                $realEndSupport = (clone $realStartSupport)->modify('+'.$durationAfterLaunch.' mins');
            } else {
                $realStartSupport = clone $sessionDateStart;
                $realEndSupport = clone $sessionDateEnd;
            }
            $overlaps = $this->getOverlapped($client, $realStartSupport, $realEndSupport);
            $alertSupport = null;

            if ($client->getServices()->getOutsideStandardHours()->isActive()) {    //HHS Active
                if ($overlaps['slotsResults']['afterHHS'] != 0 || $overlaps['slotsResults']['beforeHHS'] != 0) {
                    $alertSupport = $this->translator->trans('The session is scheduled outside the standard hours. Support will be provided during the standard hours in accordance with your contract. To benefit from this service, you can adjust the session hours.');
                }
            } else {                                                                  //HHS Inactive
                if ($overlaps['slotsResults']['afterHHS'] != 0 || $overlaps['slotsResults']['beforeHHS'] != 0 || $overlaps['slotsResults']['inHHSAMStartAndHSAMStart'] != 0 ||
                    $overlaps['slotsResults']['inHHSLHStartHAndHSPMStart'] != 0 || $overlaps['slotsResults']['inHHSPMStartHandHHSPMEnd'] != 0) {
                    $alertSupport = $this->translator->trans('The session is scheduled outside the standard hours. Support will be provided during the standard hours in accordance with your contract. To benefit from this service, you can adjust the session hours.');
                }
            }

            $detailsSessionSupport = [
                'clientData' => [
                    'launchBeforeSession' => $launchBeforeSession,
                    'durationAfterLaunch' => $durationAfterLaunch,
                    'HS' => $client->getServices()->getStandardHours()->isActive(),
                    'HHS' => $client->getServices()->getOutsideStandardHours()->isActive(),
                ],
                'providerSession' => [
                    'sessionTimeStart' => $sessionDateStart->format('H:i'),
                    'sessionTimeEnd' => $sessionDateEnd->format('H:i'),
                    'realTimeStartSupport' => $realStartSupport->format('H:i'),
                    'realTimeEndSupport' => $realEndSupport->format('H:i'),
                    'typeSupport' => intval($typeSupport),
                    'HS' => $sessionStandardHours,
                    'HHS' => $sessionOutsideStandardHours,
                ],
                'overlaps' => $overlaps,
                'alertSupport' => ($typeSupport == 0) ? null : $alertSupport,
            ];
        } catch (\Throwable $exception) {
            $this->logger->error($exception);
            $detailsSessionSupport = [];
        }

        return $detailsSessionSupport;
    }

    public function overlapInMinutes($startSession, $endSession, $startPeriod, $endPeriod): int
    {
        $overlap = max(min($endSession->getTimestamp(), $endPeriod->getTimestamp()) - max($startSession->getTimestamp(), $startPeriod->getTimestamp()), 0);

        return $overlap != 0 ? $overlap / 60 : 0;
    }
}
