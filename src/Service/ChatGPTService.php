<?php

namespace App\Service;

use App\Entity\CategorySessionType;
use App\Entity\OpenAI;
use App\Entity\ProviderSession;
use App\Repository\OpenAIRepository;
use App\SelfSubscription\Entity\Subject;
use App\Service\Utils\DateUtils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ChatGPTService
{
    private string $model;
    private string $chatGPTAccessToken;
    private string $openAIOrganization;

    public const TYPE_WEBINAR = 'Webinar';
    public const TYPE_TRAINING = 'Formation';

    public const TONE_PROFESSIONAL = 'Professionnel';
    public const TONE_CONVIVIAL = 'Convivial';
    public const TONE_COURTEOUS = 'Courtois';

    public function __construct(private TranslatorInterface $translator, private HttpClientInterface $client, private OpenAIRepository $openAIRepository, private EntityManagerInterface $entityManager)
    {
        $this->model = 'gpt-4';
        $this->chatGPTAccessToken = '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';
        $this->openAIOrganization = 'org-UF5dVCmKD0A3DtAxcDyYvJ5T';
    }

    private function makeRequest(string $prompt): array
    {
        $url = 'https://api.openai.com/v1/chat/completions';

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
            'model' => $this->model,
            'messages' => [['role' => 'user', 'content' => $prompt]],
//            'max_tokens' => 10, // Le nombre maximum de tokens dans la réponse
        ]));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Bearer '.$this->chatGPTAccessToken,
            'OpenAI-Organization: '.$this->openAIOrganization,
        ]);

        $response = json_decode(curl_exec($ch), true);
        curl_close($ch);

        if (array_key_exists('error', $response)) {
            throw new \Exception($response['error']);
        }

        return $response;
    }

    private function emptyCreditGrant(?string $message = null): array
    {
        return [
            'error' => $message,
            'object' => 'credit_summary',
            'total_granted' => 20.0,
            'total_used' => 0.18,
            'total_available' => 19.82,
            'total_paid_available' => 15.0,
            'grants' => [
                'object' => 'list',
                'data' => [
                    [
                        'object' => 'credit_grant',
                        'id' => '1cb26cd0-ec15-421c-89f6-a0eb05881c56',
                        'grant_amount' => 15.0,
                        'used_amount' => 0.0,
                        'effective_at' => 1694649600.0,
                        'expires_at' => 1727740800.0,
                    ],
                    [
                        'object' => 'credit_grant',
                        'id' => 'dd1b786e-cf39-40e5-af1b-254ffda04e8b',
                        'grant_amount' => 5.0,
                        'used_amount' => 0.18,
                        'effective_at' => 1694649600.0,
                        'expires_at' => 1704067200.0,
                    ],
                ],
            ],
        ];
    }

    private function makeSessionRequest(string $url, OpenAI $openAI): array
    {
        try {
            $response = $this->client->request('GET', $url, [
                'headers' => [
                    'Authorization' => "Bearer {$openAI->getSessionId()}",
                ],
            ]);

            return $response->toArray();
        } catch (ClientException $e) {
            $error = json_decode($e->getResponse()->getContent(false), true);
            $openAI->setError($error['error']['message']);

            return $openAI->toArray();
        }
    }

    /**
     * @throws \Exception
     */
    public function correctTitle(string $title, string $lang = 'fr'): string
    {
        $lang = $lang === 'fr' ? 'français' : 'anglais';
        $prompt = "
            Corrige les fautes d'orthographe dans le titre suivant : '$title'.
            Contraintes dans ta réponse :
            - Ta réponse doit contenir uniquement la correction (Pas de commentaires de ta part)
            - Si tu utilises le caractère :  et que le texte doit être en français alors ajoute toujours un espace. avant car c'est la règle en français.
            - Si le titre ne contient pas de fautes d'orthographe alors réponds : $title
            - Ne rien ajouter en plus dans ta réponse
            - Ta réponse ne doit RIEN contenir avant et/ou après le titre
            - Restitue ta réponse en $lang
        ";

        $response = $this->makeRequest($prompt);

        return $response['choices'][0]['message']['content'];
    }

    /**
     * @throws \Exception
     */
    public function shortenTitle(string $title, string $lang = 'fr'): string
    {
        $lang = $lang === 'fr' ? 'français' : 'anglais';
        $prompt = "
            Réduis la longueur du titre suivant : $title.
            Contraintes dans ta réponse :
            - Ta réponse doit contenir uniquement le titre raccourci (Pas de commentaires de ta part)
            - Si tu utilises le caractère :  et que le texte doit être en français alors ajoute toujours un espace. avant car c'est la règle en français.
            - Ne rien ajouter en plus dans ta réponse
            - Ta réponse ne doit RIEN contenir avant et/ou après le titre
            - Restitue ta réponse en $lang
        ";

        $response = $this->makeRequest($prompt);

        return $response['choices'][0]['message']['content'];
    }

    /**
     * @param string $title
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function generateTitle(string $additionalComment, int $category, bool $emoji, string $tone, string $lang = 'fr'): array
    {
        $category = ($category == CategorySessionType::CATEGORY_SESSION_WEBINAR->value ? 'Webinar' : 'Formation');
        $lang = $lang === 'fr' ? 'français' : 'anglais';
        $textEmoji = $emoji ? 'Ajoute 1 emojis devant le texte et éventuellement un autre à un autre endroit dans le texte.' : '';

        $prompt = "
            Type d'évènement : $category
            Sujet : $additionalComment
            Ton : $tone
            
            Tâche : crée 3 propositions d'intitulé. La 3ème sera plus créative que les autres
            
            Langue de la réponse : $lang
            
            Contraintes :
            Utiliser au maximum 150 caractères par propositions et 9 mots.
            Ne pas utiliser de ponctuation dans chaque proposition.
            $textEmoji
            N'utilise pas le caractère \" dans ta réponse.
            Ne pas utiliser les lettres capitales, sauf pour le premier mot de chaque phrase.
            si tu écris \" : \" alors ajoute toujours un espace avant car c'est la règle en français.
            
            Format de la réponse : la réponse doit être au format json stricte comprenant comme clé un incrément partant de 0 et comme valeur le texte de la proposition
        ";

        $res = $this->makeRequest($prompt);
        $response = json_decode($res['choices'][0]['message']['content'], true);

        if (json_last_error() !== JSON_ERROR_NONE || !array_key_exists(0, $response)) {
            throw new \Exception('Invalid response');
        }

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function generateSessionRegistrationProgram(array &$data, ProviderSession $session): string
    {
        $description = $data['description'] ? 'Description : '.strip_tags($data['description'])."\n" : '';
        $animators = $animatorsTitle = '';
        if (count($session->getSpeakers())) {
            $animators = 'Animateur·s (Nom, Fonction) : '.implode('; ', $session->getSpeakers()->map(fn ($speaker) => $speaker->getName().', '.$speaker->getTitle())->toArray());
            $animatorsTitle = implode(' & ', $session->getSpeakers()->map(fn ($speaker) => $speaker->getName())->toArray());
            $animators .= "\n";
        }
        $additionalInformation = $data['additionalInformation'] ? 'Information additionnelle : '.$data['additionalInformation']."\n" : '';

        $dateStart = $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('l j F Y à H:i');
        $duration = DateUtils::humanizeDuration($session->getDuration());

        $ton = $this->translator->trans($data['tone']);
        $emoji = $data['emoji'] ? '- Utiliser quelques émojis' : '- La réponse ne doit pas contenir d\'émojis';
        $programFormat = $data['programFormat'];

        $category = $session->getCategory()->value == CategorySessionType::CATEGORY_SESSION_WEBINAR ? 'Webinar' : 'Formation';
        $lang = $session->getLanguage() === 'fr' ? 'français' : 'anglais';

        $prompt = "
            Oublie tout ce que je t'ai demandé précédemment.
            Agis comme un expert marketing qui crée un catalogue d'événements (formation présentiel, classe virtuelle, webinar...). Ce catalogue est disponible sur une plateforme en ligne.
            Type d'évènement: $category
            Sujet : {$session->getName()}
            $description$animators$additionalInformation
            Date : $dateStart
            Durée : $duration
            
            Tâche : crée une présentation du programme de cet événement (sans reprendre le titre)

            EXEMPLE 1 - PROGRAMME FORMAT DÉTAILLÉ :
            Vous cherchez à dynamiser vos formations ou vos conférences à distance ? Apprenez comment l'Intelligence Artificielle peut révolutionner l’engagement et l'interaction avec les participants pendant vos sessions en live ! 🚀🎓 
            
            Bien que ces applications puissent être utilisées dans tous les domaines de compétences, nous avons sélectionné deux exemples pour illustrer leur utilisation : 
            - la conduite du changement dans les projets numériques 
            - la formation des professionnels de santé.
            
            Nous partagerons bien sûr notre analyse de leurs points forts et de leurs points à améliorer. 
            
            Ce webinar est conçu pour vous si vous : 
            🧐 Êtes curieux(se) d'apprendre comment l'IA peut vous aider à concevoir vos prochaines animations,
            💡 Cherchez de nouvelles méthodes pour améliorer l'engagement et l'interaction lors de vos sessions en live,
            🌟 Recherchez des moyens pour être plus performant avec des outils novateurs.
            
            Rejoignez-nous le $dateStart pour cette session unique et interactive. 
            
            PS : en vous inscrivant, vous recevrez une fiche mémo des outils d'IA présentés ainsi qu'un accès à l'enregistrement du webinar pour le consulter ultérieurement. N'hésitez pas à partager cette invitation avec vos collègues et votre réseau professionnel. Inscrivez-vous dès maintenant ! 📲💻
            
            Nous avons hâte de vous retrouver lors de notre webinar,

            $animatorsTitle
            
            EXEMPLE 2 - PROGRAMME FORMAT CHAPITRÉ :
            Vous voulez booster vos formations en direct ? Découvrez les secrets des équipes performantes avec $animatorsTitle ! 🌐
            
            **Au programme :** 
            - Les rôles essentiels d'une équipe de formation.
            - Stratégies pour maximiser l'engagement.
            - Exemples concrets de sessions réussies.
            
            **C'est pour vous si** :
            🎯 Vous visez l'excellence en formation live.
            🌱 Vous voulez renforcer la dynamique d'équipe.
            ⭐ Vous cherchez à innover dans vos sessions.
            
            **Rendez-vous** le $dateStart.
            
            **Bonus** : Inscription = ressources exclusives + accès à l'enregistrement. 📞🖥️
            
            Hâte de partager avec vous,
            $animatorsTitle
            
            Contraintes dans ta réponse :
            - Prendre comme exemple PROGRAMME FORMAT $programFormat
            - Ton : $ton
            $emoji
            - Ne pas rappeler le sujet dans la description de l'événement
            - Ne pas indiquer le timing en minutes ni un programme détaillé
            - Ne pas utiliser du texte entre crochet []
            - si tu utilises le caractère :  et que le texte doit être en français alors ajoute toujours un espace. avant car c'est la règle en français.
            - ta réponse doit contenir uniquement le programme (et pas le sujet reformulé et pas non plus de commentaires de ta part)
            - le texte doit toujours avoir la même taille de police
            - Restitue ta réponse en $lang
            - Le format des date doivent être en $lang
            - Génère ta réponse avec des balises HTML (des paragrapheres, des listes) et transforme le markdown en bon format pour le HTML. Met bien bien aussi ta réponse avec le bon format d'exemple
        ";
        $response = $this->makeRequest($prompt);

        return $response['choices'][0]['message']['content'];
    }

    public function generateCatalogSubjectProgram(array &$data, Subject $subject): string
    {
        $category = $subject->getCategorySession()->value == CategorySessionType::CATEGORY_SESSION_WEBINAR ? 'Webinar' : 'Formation';

        $description = $data['description'] ? 'Description : '.strip_tags($data['description'])."\n" : '';
        $animators = $animatorsTitle = '';
        if (count($subject->getSpeakers())) {
            $animators = 'Animateur·s (Nom, Fonction) : '.implode('; ', $subject->getSpeakers()->map(fn ($speaker) => $speaker->getName().', '.$speaker->getTitle())->toArray());
            $animatorsTitle = implode(' & ', $subject->getSpeakers()->map(fn ($speaker) => $speaker->getName())->toArray());
            $animators .= "\n";
        }
        $additionalInformation = $data['additionalInformation'] ? 'Information additionnelle : '.$data['additionalInformation']."\n" : '';

        $ton = $this->translator->trans($data['tone']);
        $emoji = $data['emoji'] ? '- Utiliser quelques émojis' : '- La réponse ne doit pas contenir d\'émojis';
        $programFormat = $data['programFormat'];
        $lang = $subject->getTheme()->getModule()->getLang() === 'fr' ? 'français' : 'anglais';

        $prompt = "
            Oublie tout ce que je t'ai demandé précédemment.
            Agis comme un expert marketing qui crée un catalogue d'événements (formation présentiel, classe virtuelle, webinar...). Ce catalogue est disponible sur une plateforme en ligne.
            Type d'évènement : $category
            Sujet : {$subject->getName()}
            $description$animators$additionalInformation
            Durée : {$data['duration']}
            
            Tâche : crée une présentation du programme de cet événement (sans reprendre le titre)

            EXEMPLE 1 - PROGRAMME FORMAT DÉTAILLÉ :
            Vous cherchez à dynamiser vos formations ou vos conférences à distance ? Apprenez comment l'Intelligence Artificielle peut révolutionner l’engagement et l'interaction avec les participants pendant vos sessions en live ! 🚀🎓 
            
            Bien que ces applications puissent être utilisées dans tous les domaines de compétences, nous avons sélectionné deux exemples pour illustrer leur utilisation : 
            - la conduite du changement dans les projets numériques 
            - la formation des professionnels de santé.
            
            Nous partagerons bien sûr notre analyse de leurs points forts et de leurs points à améliorer. 
            
            Ce webinar est conçu pour vous si vous : 
            🧐 Êtes curieux(se) d'apprendre comment l'IA peut vous aider à concevoir vos prochaines animations,
            💡 Cherchez de nouvelles méthodes pour améliorer l'engagement et l'interaction lors de vos sessions en live,
            🌟 Recherchez des moyens pour être plus performant avec des outils novateurs.
            
            PS : en vous inscrivant, vous recevrez une fiche mémo des outils d'IA présentés ainsi qu'un accès à l'enregistrement du webinar pour le consulter ultérieurement. N'hésitez pas à partager cette invitation avec vos collègues et votre réseau professionnel. Inscrivez-vous dès maintenant ! 📲💻
            
            Nous avons hâte de vous retrouver lors de notre webinar,
    
            $animatorsTitle
            
            EXEMPLE 2 - PROGRAMME FORMAT CHAPITRÉ :
            Vous voulez booster vos formations en direct ? Découvrez les secrets des équipes performantes avec $animatorsTitle ! 🌐
            
            **Au programme :** 
            - Les rôles essentiels d'une équipe de formation.
            - Stratégies pour maximiser l'engagement.
            - Exemples concrets de sessions réussies.
            
            **C'est pour vous si** :
            🎯 Vous visez l'excellence en formation live.
            🌱 Vous voulez renforcer la dynamique d'équipe.
            ⭐ Vous cherchez à innover dans vos sessions.
            
            **Bonus** : Inscription = ressources exclusives + accès à l'enregistrement. 📞🖥️
            
            Hâte de partager avec vous,
            $animatorsTitle
            
            Contraintes dans ta réponse :
            - Prendre comme exemple PROGRAMME FORMAT $programFormat
            - Ton : $ton
            $emoji
            - Ne pas rappeler le sujet dans la description de l'événement
            - Ne pas indiquer le timing en minutes ni un programme détaillé
            - Ne pas utiliser du texte entre crochet []
            - si tu utilises le caractère :  et que le texte doit être en français alors ajoute toujours un espace. avant car c'est la règle en français.
            - ta réponse doit contenir uniquement le programme (et pas le sujet reformulé et pas non plus de commentaires de ta part)
            - le texte doit toujours avoir la même taille de police
            - Restitue ta réponse en $lang
            - Le format des date doivent être en $lang
            - Génère ta réponse avec des balises HTML (des paragrapheres, des listes) et transforme le markdown en bon format pour le HTML. Met bien bien aussi ta réponse avec le bon format d'exemple
        ";
        $response = $this->makeRequest($prompt);

        return $response['choices'][0]['message']['content'];
    }

    public function generateCatalogSubjectDescription(array &$data, Subject $subject): string
    {
        $eventType = $subject->getCategorySession()->value == CategorySessionType::CATEGORY_SESSION_WEBINAR ? 'Webinar' : 'Formation';
        $additionalInformation = $data['additionalInformation'] ? 'Information additionnelle : '.$data['additionalInformation']."\n" : '';
        $useEmoji = $data['emoji'] ? '- Utiliser quelques émojis Choix' : '- La réponse ne doit pas contenir d\'émojis';

        $lang = $subject->getTheme()->getModule()->getLang() === 'fr' ? 'français' : 'anglais';

        $prompt = "Oublie tout ce que je t'ai demandé précédemment.
        Agis comme un expert marketing qui crée un catalogue d'événements (formation présentiel, classe virtuelle, webinar...). Ce catalogue est disponible sur une plateforme en ligne.
        Type d'évènement : $eventType
        Sujet : {$subject->getName()}
        $additionalInformation
        
        Tâche : crée une description de cet événement (sans reprendre le titre et la durée)

        Contraintes dans ta réponse :
        - 60 mots maximum
        - Ton : {$data['tone']}
        $useEmoji
        - Utiliser le gras pour certains mots clés
        - Restituer uniquement la description
        - Si tu utilises le caractère :  ou ! et que le texte doit être en français alors ajoute toujours un espace. avant car c'est la règle en français.
        - Le texte doit toujours avoir la même taille de police
        - Met la réponse sous forme de text
        - Restitue ta réponse en $lang
        ";

        $response = $this->makeRequest($prompt);

        return $response['choices'][0]['message']['content'];
    }

    public function getCreditGrants(string $sessionKey, ?float $limit = 5.0): OpenAI
    {
        $openai = $this->openAIRepository->findAll();
        $openai = $openai[0] ?? new OpenAI();
        $openai->setSessionId($sessionKey);
        $openai->setCreditThreshold($limit);

        $data = $this->makeSessionRequest('https://api.openai.com/dashboard/billing/credit_grants', $openai);

        if (!array_key_exists('error', $data)) {
            $openai
                ->setTotalGranted($data['total_granted'])
                ->setTotalUsed($data['total_used'])
                ->setTotalAvailable($data['total_available'])
                ->setSessionId($sessionKey)
                ->setBilling($data['grants']['data'])
                ->setTotalPaidAvailable($data['total_paid_available'])
                ->setError(null);
        }

        $this->entityManager->persist($openai);
        $this->entityManager->flush();

        return $openai;
    }

    public function getAllAlert(): array
    {
        $openai = $this->openAIRepository->findAll();
        $openai = $openai[0] ?? new OpenAI();

        return [
            'error' => $openai->getError(),
            'thresholdReached' => $openai->getSessionId() && $openai->getTotalAvailable() < $openai->getCreditThreshold(),
        ];
    }
}
