<?php

namespace App\Service;

use App\Entity\ActivityLog;
use App\Entity\CategorySessionType;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\OnHoldProviderSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Repository\ConvocationRepository;
use App\Repository\ProviderParticipantRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Security\UserSecurityService;
use App\Serializer\ProviderSessionNormalizer;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OnHoldProviderSessionService
{
    public function __construct(
        //private AdobeConnectService $adobeConnectService,
        private ActivityLogger $activityLogger,
        private ConvocationRepository $convocationRepository,
        private EntityManagerInterface $entityManager,
        private ProviderParticipantRepository $providerParticipantRepository,
        private ParticipantSessionSubscriber $participantSessionSubscriber,
        //private ProviderSessionNormalizer $providerSessionNormalizer,
        private ProviderSessionService $providerSessionService,
        private ProviderSessionRepository $providerSessionRepository,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        private LoggerInterface $logger,
        private Security $security,
        private UserRepository $userRepository,
        private UserSecurityService $userSecurityService,
        //private ValidatorInterface $validator,
    ) {
    }

    public function setParticipantsDiff(OnHoldProviderSession $onHoldProviderSession): OnHoldProviderSession
    {
        $newParticipants = [
            'animators' => [],
            'trainees' => [],
        ];
        foreach ($onHoldProviderSession->getParticipants()['animators'] as $animator) {
            $newAnimator = [
                'name' => (!empty($animator['name'])) ? $animator['name'] : null,
                'email' => (!empty($animator['email'])) ? $animator['email'] : null,
                'megaId' => (!empty($animator['megaId'])) ? $animator['megaId'] : null,
                'forname' => (!empty($animator['forname'])) ? $animator['forname'] : null,
                'principalAnimator' => (!empty($animator['principalAnimator'])) ? $animator['principalAnimator'] : 'false',
            ];
            $newParticipants['animators'][] = $newAnimator;
        }

        foreach ($onHoldProviderSession->getParticipants()['trainees'] as $trainee) {
            $newTrainee = [
                'name' => (!empty($trainee['name'])) ? $trainee['name'] : null,
                'email' => (!empty($trainee['email'])) ? $trainee['email'] : null,
                'megaId' => (!empty($trainee['megaId'])) ? $trainee['megaId'] : null,
                'forname' => (!empty($trainee['forname'])) ? $trainee['forname'] : null,
                //'principalAnimator' => (!empty($animator['principalAnimator'])) ? $animator['principalAnimator'] : 'false',
            ];
            $newParticipants['trainees'][] = $newTrainee;
        }

        $onHoldProviderSession->setParticipants($newParticipants);

        return $onHoldProviderSession;
    }

    public function setSlotsDatesDiff(OnHoldProviderSession $onHoldProviderSession): OnHoldProviderSession
    {
        $union = 'h';
        $newSlotsDates = [];
        foreach ($onHoldProviderSession->getSlotsDates() as $slotDate) {
            $dateStart = \DateTime::createFromFormat('Y-m-d\TH:i:sP', $slotDate['dateStart']);
            $dateEnd = \DateTime::createFromFormat('Y-m-d\TH:i:sP', $slotDate['dateEnd']);
            $diff = $dateEnd->diff($dateStart);
            $newSlotDate = [
                'megaId' => $slotDate['megaId'],
                'dateStart' => $slotDate['dateStart'],
                'dateEnd' => $slotDate['dateEnd'],
                'statusValidation' => $slotDate['statusValidation'],
                'sessionReference' => (!empty($slotDate['sessionReference'])) ? $slotDate['sessionReference'] : null,
                'sessionId' => (!empty($slotDate['sessionId'])) ? $slotDate['sessionId'] : null,
                'modelACSelected' => (!empty($slotDate['modelACSelected'])) ? $slotDate['modelACSelected'] : null,
                'animatorTeamsSelected' => (!empty($slotDate['animatorTeamsSelected'])) ? $slotDate['animatorTeamsSelected'] : null,
                'diff' => $diff ? $diff->h.$union.($diff->i < 10 ? '0'.$diff->i : $diff->i) : '',
            ];
            $newSlotsDates[] = $newSlotDate;
        }
        $onHoldProviderSession->setSlotsDates($newSlotsDates);

        return $onHoldProviderSession;
    }

    public function checkStatusValidationValidatedSlots(OnHoldProviderSession $onHoldProviderSession): int
    {
        $nbrSlotsDatesValidated = 0;
        foreach ($onHoldProviderSession->getSlotsDates() as &$slot) {
            if ($slot['statusValidation'] != OnHoldProviderSession::STATUS_WAIT_FOR_VALIDATION) {
                ++$nbrSlotsDatesValidated;
            }
        }

        return $nbrSlotsDatesValidated;
    }

    public function selectAnimatorOnDemand(OnHoldProviderSession $onHoldProviderSession, string $animatorMegaId, string $slotDateMegaId): OnHoldProviderSession
    {
        $providerSession = $this->createSession($onHoldProviderSession, $animatorMegaId, $slotDateMegaId);

        $newSlotsDates = [];

        foreach ($onHoldProviderSession->getSlotsDates() as &$slot) {
            if ($slot['megaId'] === $slotDateMegaId) {
                if (!empty($providerSession)) {
                    $slot['animatorTeamsSelected'] = $animatorMegaId;
                    $slot['statusValidation'] = OnHoldProviderSession::STATUS_VALIDATED;
                    $slot['sessionReference'] = $providerSession->getRef();
                    $slot['sessionId'] = $providerSession->getId();
                }
            }
            $newSlotsDates[] = $slot;
        }
        $sortSlotsDates = $this->compareDatesSort($newSlotsDates, 'Asc');
        $onHoldProviderSession->setSlotsDates($sortSlotsDates);

        return $onHoldProviderSession;
    }

    public function selectModelACOnDemand(OnHoldProviderSession $onHoldProviderSession, ?string $modelACId = null, string $slotDateMegaId): OnHoldProviderSession
    {
        $providerSession = $this->createSession($onHoldProviderSession, $modelACId, $slotDateMegaId);

        $newSlotsDates = [];
        foreach ($onHoldProviderSession->getSlotsDates() as &$slot) {
            if ($slot['megaId'] == $slotDateMegaId) {
                if (!empty($providerSession)) {
                    $slot['modelACSelected'] = $modelACId;
                    $slot['statusValidation'] = OnHoldProviderSession::STATUS_VALIDATED;
                    $slot['sessionReference'] = $providerSession->getRef();
                    $slot['sessionId'] = $providerSession->getId();
                }
            }
            $newSlotsDates[] = $slot;
        }
        $sortSlotsDates = $this->compareDatesSort($newSlotsDates, 'Asc');
        $onHoldProviderSession->setSlotsDates($sortSlotsDates);

        return $onHoldProviderSession;
    }

    public function selectConnectorType(OnHoldProviderSession $onHoldProviderSession, string $connectorType): OnHoldProviderSession
    {
        $allSlotsNotTreated = ($this->checkStatusValidationValidatedSlots($onHoldProviderSession) == 0) ? true : false;
        if ($allSlotsNotTreated) {
            $onHoldProviderSession->setConnectorType($connectorType);
        }

        return $onHoldProviderSession;
    }

    public function deleteSession(OnHoldProviderSession $onHoldProviderSession): void
    {
        //Supprime toutes les sessions depuis $onHoldProviderSession->getSlotsDates()
        foreach ($onHoldProviderSession->getSlotsDates() as $slotDate) {
            if ($slotDate['statusValidation'] == 2 && !is_null($slotDate['sessionId'])) {
                $session = $this->providerSessionRepository->find($slotDate['sessionId']);
                //dump('DELETE OHPS : '.'Je supprime la session '.$slotDate['sessionId'].' pour le slotDate '.$slotDate['megaId']);
                $this->providerSessionService->deleteSession($session);
                $this->entityManager->flush();
            }
        }
        $this->entityManager->remove($onHoldProviderSession);
    }

    public function createSession(OnHoldProviderSession $onHoldSession, ?string $modelOrAnimator = null, string $slotDateMegaId): ?ProviderSession
    {
        $user = $this->security->getUser();
        $this->activityLogger->initActivityLogContext($onHoldSession->getClient(), $user, ActivityLog::ORIGIN_USER_INTERFACE);
        try {
            $configurationHolderTarget = null;
            if ($onHoldSession->getConnectorType() == OnHoldProviderSession::CONNECTOR_TYPE_ADOBE_CONNECT_SCO) {
                foreach ($onHoldSession->getClient()->getConfigurationHolders() as $configurationHolder) {
                    if ($configurationHolder instanceof AdobeConnectConfigurationHolder && !$configurationHolder->isArchived()) {
                        /** @var AdobeConnectConfigurationHolder $configurationHolder */
                        $configurationHolderTarget = $configurationHolder;
                        break;
                    }
                }
            } elseif ($onHoldSession->getConnectorType() == OnHoldProviderSession::CONNECTOR_TYPE_MICROSOFT_TEAMS_EVENT) {
                foreach ($onHoldSession->getClient()->getConfigurationHolders() as $configurationHolder) {
                    if ($configurationHolder instanceof MicrosoftTeamsEventConfigurationHolder && !$configurationHolder->isArchived()) {
                        /** @var MicrosoftTeamsEventConfigurationHolder $configurationHolder */
                        $configurationHolderTarget = $configurationHolder;
                        break;
                    }
                }
            }

            if (empty($configurationHolderTarget)) {
                throw new \Exception('No configuration holder found for this connector type');
            }

            $convocation = $this->convocationRepository->find($configurationHolderTarget->getDefaultConvocation());
            $slotDateTarget = [];
            foreach ($onHoldSession->getSlotsDates() as $slotDate) {
                if ($slotDate['megaId'] === $slotDateMegaId) {
                    $slotDateTarget = $slotDate;
                    break;
                }
            }

            $data = [
                'dateStart' => \DateTime::createFromFormat('Y-m-d\TH:i:sP', $slotDateTarget['dateStart']),
                'dateEnd' => \DateTime::createFromFormat('Y-m-d\TH:i:sP', $slotDateTarget['dateEnd']),
                //'name' => $currentRow->getName(),
                'url' => '',
                'model' => '',
                'reference' => ProviderSessionService::generateSessionReference($onHoldSession->getClient()->getName()),
                'lang' => 'fr',
                'businessNumber' => '',
                'information' => '',
                'tags' => [],
                'personalizedContent' => '',
                'category' => (CategorySessionType::CATEGORY_SESSION_FORMATION)->value,
                'automaticSendingTrainingCertificates' => '',
                'sessionTest' => '',
                'convocationObj' => $convocation,
                'title' => $onHoldSession->getTitle(),
                'defaultModelId' => $modelOrAnimator,
                'description' => 'megaId:'.$slotDateTarget['megaId'],
                'description2' => null,
                'nature' => null,
                'objectives' => null,
                'replayVideoPlatform' => null,
                'replayUrlKey' => null,
                'publishedReplay' => false,
                'accessParticipantReplay' => false,
                'replayLayoutPlayButton' => false,
                'subscriptionMax' => $onHoldSession->getNbrMaxParticipants(),
                'alertLimit' => $onHoldSession->getNbrMinParticipants(),
            ];

            if ($onHoldSession->getConnectorType() == OnHoldProviderSession::CONNECTOR_TYPE_ADOBE_CONNECT_SCO) {
                $data['accessType'] = AdobeConnectConnector::GRANT_ACCESS_USER_ONLY;
                $data['defaultModelId'] = $modelOrAnimator;
            } elseif ($onHoldSession->getConnectorType() == OnHoldProviderSession::CONNECTOR_TYPE_MICROSOFT_TEAMS_EVENT) {
                $data['userLicence'] = $modelOrAnimator;

                if (!$configurationHolderTarget->isAutomaticLicensing() || !empty($modelOrAnimator)) {
                    $data['isLicenceValid'] = array_key_exists($modelOrAnimator, $configurationHolderTarget->getLicences());
                    if (array_key_exists($modelOrAnimator, $configurationHolderTarget->getLicences())) {
                        if (isset($configurationHolderTarget->getLicences()[$modelOrAnimator]['attributed']) && $configurationHolderTarget->getLicences()[$modelOrAnimator]['attributed']) {
                            $data['licence'] = $configurationHolderTarget->getLicences()[$modelOrAnimator]['licenceAttributed'];
                        }
                    }
                }
            }

            $session = $this->providerSessionService->createSessionFromArray($configurationHolderTarget, $user, $data);

            $session = $this->providerSessionService->applyRemindersAndConvocationPreference($onHoldSession->getClient()->getEmailScheduling(), $session);

            $this->entityManager->flush();
            $this->activityLogger->flushActivityLogs();

            return $session;
        } catch (\Throwable $exception) {
            $this->logger->error($exception);
            throw $exception;
        }
    }

    public function updateSessionFromArray(OnHoldProviderSession $onHoldProviderSession, array &$data): OnHoldProviderSession
    {
        return $onHoldProviderSession;
    }

    public function updateSession(OnHoldProviderSession $session, ?array $params = []): OnHoldProviderSession
    {
        try {
            return $this->updateSessionFromArray($session, $params);
        } catch (\Throwable $exception) {
            $this->logger->error($exception);
            throw $exception;
        }
    }

    public function updateParticipantsToSession(ProviderSession $providerSession, OnHoldProviderSession $onHoldProviderSession, array $newDataParticipants, ?string $type = 'animator'): void
    {
        $this->updateAnimatorsToSession($providerSession, $onHoldProviderSession, $newDataParticipants, $type);
        $this->updateTraineesToSession($providerSession, $onHoldProviderSession, $newDataParticipants, 'trainee');
    }

    public function updateSlotsDateToSessions(OnHoldProviderSession $onHoldProviderSession, array $newDataSlotsDates = [], array $newDataParticipants = []): void
    {
        foreach ($onHoldProviderSession->getSlotsDates() as $oldSlotDate) {
            $found = false;
            foreach ($newDataSlotsDates as $newSlotDate) {
                //dump('SLOTDATE : '.$oldSlotDate['megaId'] .' vs '. $newSlotDate['megaId']);
                if ($oldSlotDate['megaId'] == $newSlotDate['megaId']) {
                    $found = true;
                    //dump('SLOTDATE : '.$oldSlotDate['statusValidation'].' vs '.OnHoldProviderSession::STATUS_VALIDATED);
                    if ($oldSlotDate['statusValidation'] == OnHoldProviderSession::STATUS_VALIDATED) {
                        $session = $this->providerSessionRepository->find($oldSlotDate['sessionId']);
                        $session->setName($onHoldProviderSession->getTitle());
                        $session->setSubscriptionMax($onHoldProviderSession->getNbrMaxParticipants());
                        $session->setAlertLimit($onHoldProviderSession->getNbrMinParticipants());
                        $this->entityManager->persist($session);
                        $this->entityManager->flush();

                        //dump('SLOTDATE : '.$oldSlotDate['dateStart'] .' vs '. $newSlotDate['dateStart'] .' && '. $oldSlotDate['dateEnd'] .' vs '. $newSlotDate['dateEnd']);
                        if ($oldSlotDate['dateStart'] == $newSlotDate['dateStart'] && $oldSlotDate['dateEnd'] == $newSlotDate['dateEnd']) {
                            //if ($oldSlotDate['statusValidation'] == OnHoldProviderSession::STATUS_VALIDATED) { //Si le slotDate correspond à une session
                            $this->updateParticipantsToSession($session, $onHoldProviderSession, $newDataParticipants); //On met à jour les participant de la session
                            //}
                        } else {
                            //dump('SLOTDATE : '.'Les dates pour la session '.$oldSlotDate['sessionId'].' ont été changées pour le slotDate >>> Pas possible');
                        }
                    } else {
                        //dump('SLOTDATE : '.'Je devrai créer une session '.$newSlotDate['megaId']);
                    }
                    break;
                }
            }
            if (!$found) {
                if ($oldSlotDate['statusValidation'] == OnHoldProviderSession::STATUS_VALIDATED) {
                    $session = $this->providerSessionRepository->find($oldSlotDate['sessionId']);
                    //dump('SLOTDATE : '.'Je supprime la session '.$session->getId().' pour le slotDate '.$oldSlotDate['megaId']);
                    $this->providerSessionService->deleteSession($session);
                    $this->entityManager->flush();
                }
            }
        }
    }

    public function checkSlotsDatesAllValidate(OnHoldProviderSession $onHoldProviderSession): bool
    {
        $allValide = true;

        foreach ($onHoldProviderSession->getSlotsDates() as $slotsDate) {
            if ($slotsDate['statusValidation'] == 1) {
                $allValide = false;
            }
        }

        return $allValide;
    }

    public function compareDatesSort($slotsDates, $sortType = 'Asc'): array
    {
        usort($slotsDates, static function (array $a, array $b) use (&$sortType) {
            $dateA = strtotime($a['dateStart']);
            $dateB = strtotime($b['dateStart']);

            if ($sortType == 'Asc') {
                return $dateA - $dateB;
            } else {
                return $dateB - $dateA;
            }
        });

        return $slotsDates;
    }

    public function convertSlotsDateToUTC(array $slotsDates = []): array
    {
        foreach ($slotsDates as &$slot) {
            $dateStart = new \DateTime($slot['dateStart'], new \DateTimeZone('Europe/Paris'));
            $dateStart->setTimezone(new \DateTimeZone('UTC'));
            $slot['dateStart'] = $dateStart->format('Y-m-d\TH:i:sP');

            $dateEnd = new \DateTime($slot['dateEnd'], new \DateTimeZone('Europe/Paris'));
            $dateEnd->setTimezone(new \DateTimeZone('UTC'));
            $slot['dateEnd'] = $dateEnd->format('Y-m-d\TH:i:sP');
        }

        return $slotsDates;
    }

    public function convertSlotsDateToTimezone(array $slotsDates = [], string $timezone): array
    {
        foreach ($slotsDates as &$slot) {
            $dateStart = new \DateTime($slot['dateStart'], new \DateTimeZone('UTC'));
            $dateStart->setTimezone(new \DateTimeZone($timezone));
            $slot['dateStart'] = $dateStart->format('Y-m-d\TH:i:sP');

            $dateEnd = new \DateTime($slot['dateEnd'], new \DateTimeZone('UTC'));
            $dateEnd->setTimezone(new \DateTimeZone($timezone));
            $slot['dateEnd'] = $dateEnd->format('Y-m-d\TH:i:sP');
        }

        return $slotsDates;
    }

    private function updateAnimatorsToSession(ProviderSession $providerSession, OnHoldProviderSession $onHoldProviderSession, array $newDataParticipants, ?string $type = 'animator'): void
    {
        // Recherche des animateurs à supprimer et à mettre à jour
        foreach ($providerSession->getAnimatorsOnly() as $animator) {
            $found = false;
            foreach ($newDataParticipants['animators'] as $participant) {
                //dump('PARTICIPANT : '.$animator->getParticipant()->getEmail() .' vs '. $participant['email']);
                if ($animator->getParticipant()->getEmail() == $participant['email']) {
                    $found = true;
                    //dump('PARTICIPANT : '.$animator->getParticipant()->getUser()->getLastName().' vs '.$participant['name']);
                    if ($animator->getParticipant()->getUser()->getLastName() != $participant['name']) {
                        //dump('PARTICIPANT : '.'Dans la session '.$providerSession->getId().' on met à jour le name de l\'animateur '.$animator->getParticipant()->getEmail());
                    }
                    //dump('PARTICIPANT : '.$animator->getParticipant()->getUser()->getFirstName().' vs '.$participant['forname']);
                    if ($animator->getParticipant()->getUser()->getFirstName() != $participant['forname']) {
                        //dump('PARTICIPANT : '.'Dans la session '.$providerSession->getId().' on met à jour le forname de l\'animateur '.$animator->getParticipant()->getEmail());
                    }
                    break;
                }
            }
            if (!$found) {
                $userClient = $this->userRepository->findOneBy(['email' => $animator->getParticipant()->getEmail(), 'client' => $providerSession->getClient()]);
                $ppsr = $this->providerParticipantSessionRoleRepository->findBySessionAndUser($providerSession, $userClient);
                //dump('PARTICIPANT : '.'Dans la session '.$providerSession->getId().' on supprime l\'animateur '.$ppsr->getEmail());
                $this->participantSessionSubscriber->unsubscribe($ppsr);
                $this->entityManager->flush();
            }
        }

        // Recherche des nouveaux participants à ajouter
        foreach ($newDataParticipants['animators'] as $participant) {
            $found = false;
            foreach ($providerSession->getAnimatorsOnly() as $animator) {
                //dump('PARTICIPANT : '.$animator->getParticipant()->getEmail() .' vs '. $participant['email']);
                if ($animator->getParticipant()->getEmail() == $participant['email']) {
                    $found = true;
                }
            }
            if (!$found) {
                //dump('PARTICIPANT : '.'Dans la session '.$providerSession->getId().' on ajoute l\'animateur '.$participant['email']);
                //On vérifie que le couple client/user existe
                $userClient = $this->userRepository->findOneBy(['email' => $participant['email'], 'client' => $onHoldProviderSession->getClient()]);
                if (empty($userClient)) {
                    $participantUser = new User();
                    $participantUser->setClient($onHoldProviderSession->getClient())
                        ->setFirstName($participant['forname'])
                        ->setLastName($participant['name'])
                        ->setClearPassword($this->userSecurityService->randomPassword())
                        ->setEmail($participant['email'])
                        ->setRoles(['ROLE_TRAINEE'])
                        ->setPreferredLang('fr');
                    //dump('PARTICIPANT : '.'Dans la session '.$providerSession->getId().' je viens de crééer le user '.$participant['email'].' pour le compte '.$onHoldProviderSession->getClient()->getName());
                    $this->entityManager->persist($participantUser);
                    $this->entityManager->flush();
                } else {
                    $participantUser = $userClient;
                }
                //S'il n'existe pas nous créons le user sinon on le récupère = $participant
                $ppsrAnimator = $this->participantSessionSubscriber->subscribe($providerSession, $participantUser, ProviderParticipantSessionRole::ROLE_ANIMATOR);
                //dump('PARTICIPANT : '.'Dans la session '.$providerSession->getId().' on ajoute au ppsr '.$ppsrAnimator->getEmail());
                $this->entityManager->flush();
            }
        }
    }

    private function updateTraineesToSession(ProviderSession $providerSession, OnHoldProviderSession $onHoldProviderSession, array $newDataParticipants, ?string $type = 'trainee'): void
    {
        // Recherche des participants à supprimer et à mettre à jour
        foreach ($providerSession->getTraineesOnly() as $trainee) {
            $found = false;
            foreach ($newDataParticipants['trainees'] as $participant) {
                //dump('PARTICIPANT : '.$trainee->getParticipant()->getEmail() .' vs '. $participant['email']);
                if ($trainee->getParticipant()->getEmail() == $participant['email']) {
                    $found = true;
                    //dump('PARTICIPANT : '.$trainee->getParticipant()->getUser()->getLastName().' vs '.$participant['name']);
                    if ($trainee->getParticipant()->getUser()->getLastName() != $participant['name']) {
                        //dump('PARTICIPANT : '.'Dans la session '.$providerSession->getId().' on met à jour le name du participant '.$trainee->getParticipant()->getEmail());
                    }
                    //dump('PARTICIPANT : '.$trainee->getParticipant()->getUser()->getFirstName().' vs '.$participant['forname']);
                    if ($trainee->getParticipant()->getUser()->getFirstName() != $participant['forname']) {
                        //dump('PARTICIPANT : '.'Dans la session '.$providerSession->getId().' on met à jour le forname du participant '.$trainee->getParticipant()->getEmail());
                    }
                    break;
                }
            }
            if (!$found) {
                $userClient = $this->userRepository->findOneBy(['email' => $trainee->getParticipant()->getEmail(), 'client' => $providerSession->getClient()]);
                $ppsr = $this->providerParticipantSessionRoleRepository->findBySessionAndUser($providerSession, $userClient);
                //dump('PARTICIPANT : '.'Dans la session '.$providerSession->getId().' on supprime le participant '.$ppsr->getEmail());
                $this->participantSessionSubscriber->unsubscribe($ppsr);
                $this->entityManager->flush();
            }
        }

        // Recherche des nouveaux participants à ajouter
        foreach ($newDataParticipants['trainees'] as $participant) {
            $found = false;
            foreach ($providerSession->getAnimatorsOnly() as $trainee) {
                //dump('PARTICIPANT : '.$trainee->getParticipant()->getEmail() .' vs '. $participant['email']);
                if ($trainee->getParticipant()->getEmail() == $participant['email']) {
                    $found = true;
                }
            }
            if (!$found) {
                //dump('PARTICIPANT : '.'Dans la session '.$providerSession->getId().' on ajoute le participant '.$participant['email']);
                //On vérifie que le couple client/user existe
                $userClient = $this->userRepository->findOneBy(['email' => $participant['email'], 'client' => $onHoldProviderSession->getClient()]);
                if (empty($userClient)) {
                    $participantUser = new User();
                    $participantUser->setClient($onHoldProviderSession->getClient())
                        ->setFirstName($participant['forname'])
                        ->setLastName($participant['name'])
                        ->setClearPassword($this->userSecurityService->randomPassword())
                        ->setEmail($participant['email'])
                        ->setRoles(['ROLE_TRAINEE'])
                        ->setPreferredLang('fr');
                    //dump('PARTICIPANT : '.'Dans la session '.$providerSession->getId().' je viens de crééer le user '.$participant['email'].' pour le compte '.$onHoldProviderSession->getClient()->getName());
                    $this->entityManager->persist($participantUser);
                    $this->entityManager->flush();
                } else {
                    $participantUser = $userClient;
                }
                //S'il n'existe pas nous créons le user sinon on le récupère = $participant
                $ppsrTrainee = $this->participantSessionSubscriber->subscribe($providerSession, $participantUser, ProviderParticipantSessionRole::ROLE_TRAINEE);
                //dump('PARTICIPANT : '.'Dans la session '.$providerSession->getId().' on ajoute au ppsr '.$ppsrTrainee->getEmail());
                $this->entityManager->flush();
            }
        }
    }
}
