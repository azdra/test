<?php

namespace App\Service;

use App\Entity\CategorySessionType;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Cisco\WebexRestParticipant;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Exception\LicenceCheckException;
use App\Exception\ProviderServiceValidationException;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Model\ProviderResponse;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\WebexRestMeetingConfigurationHolderRepository;
use App\Service\Connector\Cisco\WebexRestMeetingConnector;
use App\Service\Provider\Model\LicenceCheckerInterface;
use App\Service\Provider\Webex\WebexRestLicensingService;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class WebexRestMeetingService extends AbstractProviderService implements LicenceCheckerInterface
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected ValidatorInterface $validator,
        private WebexRestLicensingService $webexRestLicensingService,
        private WebexRestMeetingConfigurationHolderRepository $webexRestMeetingConfigurationHolderRepository,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        WebexRestMeetingConnector $webexRestMeetingConnector,
        LoggerInterface $logger
    ) {
        parent::__construct($entityManager, $validator, $webexRestMeetingConnector, $logger);
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof WebexRestConfigurationHolder;
    }

    /**
     * @throws ProviderServiceValidationException
     * @throws ProviderSessionRequestHandlerException
     */
    public function createSession(ProviderSession $providerSession): ProviderResponse
    {
        if (!($providerSession instanceof WebexRestMeeting)) {
            return $this->makeUnsupportedProviderSessionResponse($providerSession);
        }

        /* @var WebexRestMeeting $providerSession */
        /** @var WebexRestConfigurationHolder $configurationHolder */
        $configurationHolder = $providerSession->getAbstractProviderConfigurationHolder();
        $safetyTime = $configurationHolder->getSafetyTime();
        $availableLicences = $this->webexRestLicensingService->getAvailableLicences($configurationHolder, $providerSession->getDateStart(), $providerSession->getDateEnd(), $safetyTime, null);
        if ($configurationHolder->isAutomaticLicensing()) {
            if (!empty($availableLicences)) {
                if (in_array($providerSession->getLicence(), $availableLicences)) {
                    $providerSession->setMeetingRestHostEmail($providerSession->getLicence());
                } else {
                    $providerSession->setMeetingRestHostEmail(reset($availableLicences));
                    $providerSession->setLicence(reset($availableLicences));
                }
            } else {
                throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');
            }
        } else {
            if (in_array($providerSession->getLicence(), $availableLicences)) {
                $providerSession->setMeetingRestHostEmail($providerSession->getLicence());
            } else {
                throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');
            }
        }
        if (!empty($availableLicences)) {
            if (!$configurationHolder->isAutomaticLicensing()) {
                if (in_array($providerSession->getLicence(), $availableLicences)) {
                    $providerSession->setMeetingRestHostEmail($providerSession->getLicence());
                } else {
                    $providerSession->setMeetingRestHostEmail(reset($availableLicences));
                    $providerSession->setLicence(reset($availableLicences));
                }
            } else {
                $providerSession->setMeetingRestHostEmail(reset($availableLicences));
                $providerSession->setLicence(reset($availableLicences));
            }
            $this->webexRestLicensingService->addParticipantRole($configurationHolder, $providerSession);
        }
        /*dump('On attribue une licence');
        / @var WebexRestMeeting $providerSession /
        dump('getMeetingRestHostEmail = ' . $providerSession->getMeetingRestHostEmail() . ' | getLicence = ' . $providerSession->getLicence());
        $this->webexRestLicensingService->licensingASession($providerSession, null);
        if($providerSession->getMeetingRestHostEmail() != $providerSession->getLicence()){
            //Normalement en création nous ne devrions pas passer ici par rapport à ce qui a été fait dans licensingASession
            dump('On supprime la session du coté de Webex pour en recréer une !');
        }*/
        //throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot.');

        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['create']))) {
            throw new ProviderServiceValidationException($list);
        }

        if ($providerSession->getCategory() == CategorySessionType::CATEGORY_SESSION_WEBINAR) {
            $providerSession->setMeetingRestScheduledType(WebexRestMeeting::SCHEDULED_TYPE_WEBINAR);
        } else {
            $providerSession->setMeetingRestScheduledType(WebexRestMeeting::SCHEDULED_TYPE_MEETING);
        }

        $response = $this->connector->call($providerSession->getAbstractProviderConfigurationHolder(),
            WebexRestMeetingConnector::ACTION_REST_CREATE_MEETING,
            $providerSession);
        if ($response->isSuccess()) {
            $responseAudio = $this->connector->call(
                $providerSession->getAbstractProviderConfigurationHolder(),
                WebexRestMeetingConnector::ACTION_REST_TELEPHONY_PROFILE_INFO,
                $providerSession
            );
            $providerSession->setProviderAudio($responseAudio->getData());

            $this->entityManager->persist($providerSession);
        }

        return $response;
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     * @throws ProviderServiceValidationException
     */
    public function updateSession(ProviderSession $providerSession): ProviderResponse
    {
        if (!($providerSession instanceof WebexRestMeeting)) {
            return $this->makeUnsupportedProviderSessionResponse($providerSession);
        }
        /** @var WebexRestConfigurationHolder $configurationHolder */
        $configurationHolder = $providerSession->getAbstractProviderConfigurationHolder();
        $safetyTime = $configurationHolder->getSafetyTime();
        $oldLicence = $providerSession->getLicence();

        $uow = $this->entityManager->getUnitOfWork();
        $uow->computeChangeSets(); // do not compute changes if inside a listener
        $changeset = $uow->getEntityChangeSet($providerSession);

        //SI la licence initiale n'a pas été modifiée ET SI la date n'a pas été modifiée ET SI la durée n'a pas été modifiée ALORS
        if (
            !array_key_exists('licence', $changeset) &&
            !array_key_exists('duration', $changeset) &&
            (!array_key_exists('dateStart', $changeset) || $changeset['dateStart'][0] == $changeset['dateStart'][1])
        ) {
            if (0 !== count($list = $this->validator->validate($providerSession, groups: ['update']))) {
                throw new ProviderServiceValidationException($list);
            }

            $response = $this->connector->call(
                $providerSession->getAbstractProviderConfigurationHolder(),
                WebexRestMeetingConnector::ACTION_REST_UPDATE_MEETING,
                $providerSession
            );

            if ($response->isSuccess()) {
                if (!empty($providerSession->getLicenceParticipantRole()) && !$this->entityManager->contains($providerSession->getLicenceParticipantRole())) {
                    $responseParticipantRole = $this->updateParticipantSessionRole(
                        $providerSession->getLicenceParticipantRole()
                    );
                    if (!$responseParticipantRole->isSuccess()) {
                        return $responseParticipantRole;
                    }
                }
            }

            return $response;
        } else { //la licence initiale a été modifiée OU SI la date a été modifiée OU SI la durée a été modifiée
            if (array_key_exists('licence', $changeset) && $changeset['licence'][0] != $changeset['licence'][1]) {
                $availableLicences = $this->webexRestLicensingService->getAvailableLicences($configurationHolder, $providerSession->getDateStart(), $providerSession->getDateEnd(), $safetyTime, $providerSession->getId());
            } elseif (
                !array_key_exists('licence', $changeset) &&
                array_key_exists('dateStart', $changeset) &&
                (($changeset['dateStart'][0] != $changeset['dateStart'][1]) ||
                array_key_exists('duration', $changeset) &&
                ($changeset['duration'][0] != $changeset['duration'][1]))
            ) {
                $availableLicences = $this->webexRestLicensingService->getAvailableLicences($configurationHolder, $providerSession->getDateStart(), $providerSession->getDateEnd(), $safetyTime, $providerSession->getId());
            } else {
                $availableLicences = $this->webexRestLicensingService->getAvailableLicences($configurationHolder, $providerSession->getDateStart(), $providerSession->getDateEnd(), $safetyTime, null);
            }

            if (array_key_exists('licence', $changeset) && in_array($changeset['licence'][1], $availableLicences)) {
                $providerSession->setLicence($changeset['licence'][1]);
                $this->entityManager->flush();
                $response = $this->changeSessionForNewLicence($providerSession);
            } else {
                if (!empty($availableLicences)) {
                    if (in_array($providerSession->getLicence(), $availableLicences)) {
                        $response = $this->connector->call(
                            $providerSession->getAbstractProviderConfigurationHolder(),
                            WebexRestMeetingConnector::ACTION_REST_UPDATE_MEETING,
                            $providerSession
                        );
                    } else {
                        if ($configurationHolder->isAutomaticLicensing()) {
                            $this->entityManager->flush();
                            $providerSession->setLicence(reset($availableLicences));
                            $response = $this->changeSessionForNewLicence($providerSession);
                        } else {
                            throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session because this license is not available. Schedule a session in another time slot');
                        }
                    }
                } else {
                    throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');
                }
            }
            if (!empty($oldLicence) && $oldLicence != $providerSession->getLicence()) {
                /** @var ProviderParticipantSessionRole $ppsrOldLicence */
                $ppsrOldLicence = $this->providerParticipantSessionRoleRepository->getLicenceFromSessionAndEmail($providerSession, $oldLicence);
                $this->entityManager->remove($ppsrOldLicence);
            }
        }

        /* @var WebexRestMeeting $providerSession */
        /*dump('On met à jour dans le service');
        dump('getMeetingRestHostEmail = ' . $providerSession->getMeetingRestHostEmail() . ' | getLicence = ' . $providerSession->getLicence());

        $this->webexRestLicensingService->licensingASession($providerSession, $providerSession->getId());
        if($providerSession->getMeetingRestHostEmail() != $providerSession->getLicence()){
            dump('On supprime la session du coté de Webex pour en recréer une !');
            $response = $this->changeSessionForNewLicence($providerSession);
            $this->entityManager->persist($providerSession);
            dump('getMeetingRestHostEmail = ' . $providerSession->getMeetingRestHostEmail() . ' | getLicence = ' . $providerSession->getLicence());
            //$providerSession->setLicence($providerSession->getMeetingRestHostEmail());

            dump($response);

            dump($providerSession->getLicence().' | '.$providerSession->getMeetingRestHostEmail());

            return $response;

        }else{
            if (0 !== count($list = $this->validator->validate($providerSession, groups: ['update']))) {
                throw new ProviderServiceValidationException($list);
            }

            $response = $this->connector->call(
                $providerSession->getAbstractProviderConfigurationHolder(),
                WebexRestMeetingConnector::ACTION_REST_UPDATE_MEETING,
                $providerSession
            );

            if ($response->isSuccess()) {
                if (!empty($providerSession->getLicenceParticipantRole()) && !$this->entityManager->contains($providerSession->getLicenceParticipantRole())) {
                    $responseParticipantRole = $this->updateParticipantSessionRole(
                        $providerSession->getLicenceParticipantRole()
                    );
                    if (!$responseParticipantRole->isSuccess()) {
                        return $responseParticipantRole;
                    }
                }
            }

            return $response;
        }*/
        //throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot.');

        //J'ai commenté la ligne ci-dessous car je crois que ça n'a plus d'usage. A vérifier
        //$this->webexRestLicensingService->licensingASession($providerSession, $providerSession->getId());
        //dump($providerSession->getLicence().' | '.$providerSession->getMeetingRestHostEmail());
        //C'est peut-être ici qu'il faut supprimer la session pour la recrééer avec Webex
        //Dans ce cas, nous aurions
        /*dump(!empty($providerSession->getLicence()) &&
            !empty($providerSession->getMeetingRestHostEmail()) &&
            $providerSession->getMeetingRestHostEmail() != $providerSession->getLicence());*/
        //throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot.');

        /*if (!empty($providerSession->getLicence()) &&
            !empty($providerSession->getMeetingRestHostEmail()) &&
            $providerSession->getMeetingRestHostEmail() != $providerSession->getLicence()
        ){
            $response = $this->changeSessionForNewLicence($providerSession);
        } else {*/
        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        /*$response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            WebexRestMeetingConnector::ACTION_REST_UPDATE_MEETING,
            $providerSession
        );*/
        //}

        if ($response->isSuccess()) {
            if (!empty($providerSession->getLicenceParticipantRole()) && !$this->entityManager->contains($providerSession->getLicenceParticipantRole())) {
                $responseParticipantRole = $this->updateParticipantSessionRole(
                    $providerSession->getLicenceParticipantRole()
                );
                if (!$responseParticipantRole->isSuccess()) {
                    return $responseParticipantRole;
                }
            }
        }

        return $response;
    }

    public function getSession(ProviderSession $providerSession): ProviderResponse
    {
        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            WebexRestMeetingConnector::ACTION_REST_GET_MEETING_BY_ID,
            $providerSession
        );
    }

    /**
     * @throws ProviderServiceValidationException
     */
    public function deleteSession(ProviderSession $providerSession): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['delete']))) {
            throw new ProviderServiceValidationException($list);
        }

        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            WebexRestMeetingConnector::ACTION_REST_DELETE_MEETING,
            $providerSession
        );
    }

    public function changeSessionForNewLicence(WebexRestMeeting $providerSession): ProviderResponse
    {
        $response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            WebexRestMeetingConnector::ACTION_REST_DELETE_MEETING,
            $providerSession
        );
        $oldLicence = $providerSession->getMeetingRestHostEmail();

        $providerSession->setMeetingRestHostEmail($providerSession->getLicence());
        if ($response->isSuccess()) {
            $response = $this->connector->call($providerSession->getAbstractProviderConfigurationHolder(),
                WebexRestMeetingConnector::ACTION_REST_CREATE_MEETING,
                $providerSession);
            if ($response->isSuccess()) {
                /** @var WebexRestConfigurationHolder $configurationHolder */
                $configurationHolder = $providerSession->getAbstractProviderConfigurationHolder();
                $this->webexRestLicensingService->addParticipantRole($configurationHolder, $providerSession);
                $responseAudio = $this->connector->call(
                    $providerSession->getAbstractProviderConfigurationHolder(),
                    WebexRestMeetingConnector::ACTION_REST_TELEPHONY_PROFILE_INFO,
                    $providerSession
                );
                $providerSession->setProviderAudio($responseAudio->getData());

                //On supprime l'ancienne licence des participants
                foreach ($providerSession->getParticipantRoles() as $ppsr) {
                    if ($ppsr->getEmail() == $oldLicence) {
                        $providerSession->removeParticipantRole($ppsr);
                        break;
                    }
                }
                $providerSession->setLicence($providerSession->getMeetingRestHostEmail());
            }
        }

        return $response;
    }

    public function getListMeetingInvitees(ProviderSession $providerSession): ProviderResponse
    {
        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            WebexRestMeetingConnector::ACTION_REST_LIST_MEETING_INVITEES,
            $providerSession
        );
    }

    public function getParticipant(WebexRestParticipant $providerParticipant): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($providerParticipant, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        // We don't persist here because this action is intended to update already existing participants
        return $this->connector->call(
            $providerParticipant->getConfigurationHolder(),
            WebexRestMeetingConnector::ACTION_REST_GET_A_MEETING_INVITEE,
            $providerParticipant
        );
    }

    /*public function fetchHostJoinUrl(WebexRestMeeting $meeting): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($meeting, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        return $this->connector->call(
            $meeting->getAbstractProviderConfigurationHolder(),
            WebexRestMeetingConnector::ACTION_GET_HOST_URL_MEETING,
            $meeting
        );
    }*/

    public function fetchGuestJoinUrl(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate([$participantSessionRole->getSession(), $participantSessionRole->getParticipant()], [new Valid()], ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        return $this->connector->call(
            $participantSessionRole->getAbstractProviderConfigurationHolder(),
            WebexRestMeetingConnector::ACTION_REST_GET_JOIN_URL_MEETING,
            $participantSessionRole
        );
    }

    /*public function getConnectorJoinUrl(WebexRestMeeting $webexRestMeeting): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($webexRestMeeting, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        /** @var WebexRestConfigurationHolder $configurationHolder
        $configurationHolder = $webexRestMeeting->getAbstractProviderConfigurationHolder();

        return $this->connector->call(
            $configurationHolder,
            WebexRestMeetingConnector::ACTION_REST_GET_CONNECTOR_JOIN_URL_MEETING,
            $webexRestMeeting->getMeetingKey()
        );
    }*/

    /*public function listAttendee(WebexRestMeeting $meeting): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($meeting, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        return $this->connector->call(
            $meeting->getAbstractProviderConfigurationHolder(),
            WebexRestMeetingConnector::ACTION_LIST_MEETING_ATTENDEE,
            $meeting
        );
    }*/

    public function createParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return (new ProviderResponse())->setSuccess(true);
    }

    public function updateParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return (new ProviderResponse())->setSuccess(true);
    }

    public function deleteParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        $sessionRoles = $providerParticipant->getSessionRoles();

        /** @var ProviderParticipantSessionRole $sessionRole */
        foreach ($sessionRoles as $index => $sessionRole) {
            $this->connector->call(
                $sessionRole->getAbstractProviderConfigurationHolder(),
                WebexRestMeetingConnector::ACTION_REST_DELETE_A_MEETING_INVITEE,
                [
                    'webexRestMeeting' => $sessionRole->getSession(),
                    'participantSessionRole' => $sessionRole->setRole(ProviderParticipantSessionRole::ROLE_REMOVE),
                ]
            );

            $this->entityManager->remove($sessionRole);
        }

        $this->entityManager->remove($providerParticipant);

        return (new ProviderResponse())->setSuccess(true);
    }

    public function updateParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole
    ): ProviderResponse {
        if (0 !== count($list = $this->validator->validate([$participantSessionRole->getSession(), $participantSessionRole->getParticipant()], [new Valid()], ['update']))) {
            throw new ProviderServiceValidationException($list);
        }
        /** @var WebexRestMeeting $webexRestMeeting */
        $webexRestMeeting = $participantSessionRole->getSession();

        if ($participantSessionRole->getRole() === ProviderParticipantSessionRole::ROLE_REMOVE) {
            return $this->connector->call(
                $participantSessionRole->getAbstractProviderConfigurationHolder(),
                WebexRestMeetingConnector::ACTION_REST_DELETE_A_MEETING_INVITEE,
                [
                    'webexRestMeeting' => $webexRestMeeting,
                    'participantSessionRole' => $participantSessionRole,
                ]
            );
        } else {
            $role = $participantSessionRole->getRole();

            $this->connector->call(
                $participantSessionRole->getAbstractProviderConfigurationHolder(),
                WebexRestMeetingConnector::ACTION_REST_DELETE_A_MEETING_INVITEE,
                [
                    'webexRestMeeting' => $webexRestMeeting,
                    'participantSessionRole' => $participantSessionRole,
                ]
            );

            return $this->connector->call(
                $participantSessionRole->getAbstractProviderConfigurationHolder(),
                WebexRestMeetingConnector::ACTION_REST_CREATE_A_MEETING_INVITEE,
                [
                    'webexRestMeeting' => $webexRestMeeting,
                    'participantSessionRole' => $participantSessionRole->setRole($role),
                ]
            );
        }
    }

    public function redirectParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole
    ): ProviderResponse {
        /** @var WebexRestMeeting $webexRestMeeting */
        $webexRestMeeting = $participantSessionRole->getSession();

        $response = $this->connector->call(
            $participantSessionRole->getAbstractProviderConfigurationHolder(),
            WebexRestMeetingConnector::ACTION_REST_GET_JOIN_URL_MEETING,
            $participantSessionRole
        );

        return $response;
    }

    public function loginUser(WebexRestConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            WebexRestMeetingConnector::ACTION_TEST_CONNECTOR
        );
    }

    public function reportMeetingAttendance(WebexRestMeeting $webexRestMeeting): ProviderResponse
    {
        $response = $this->connector->call(
            $webexRestMeeting->getAbstractProviderConfigurationHolder(),
            WebexRestMeetingConnector::ACTION_REST_LIST_MEETING_ATTENDEES,
            $webexRestMeeting
        );

        return $response;
    }

    public function makeUnsupportedProviderSessionResponse(ProviderSession $providerSession): ProviderResponse
    {
        return (new ProviderResponse())
            ->setSuccess(false)
            ->setThrown(new InvalidArgumentException(sprintf('%s is not supported by %s.', get_class($providerSession), self::class)));
    }

    public function checkLicence(ProviderSession $providerSession): void
    {
//        // TODO: Implement checkLicence() method.
//        /** @var WebexRestMeeting $session */
//        $session = $providerSession;
//        /** @var WebexRestConfigurationHolder $configurationHolder */
//        $configurationHolder = $session->getAbstractProviderConfigurationHolder();
//        $availableLicence = $this->webexRestLicensingService->getAvailableLicences($configurationHolder, $session->getDateStart(), $session->getDateEnd(), 30, null);
//        $valid = false;
//        /*if( !empty($availableLicence) ){
//            foreach ($availableLicence as $licence) {
//                if ($licence["licence"] === $session->getLicence()) {
//                    $valid = true;
//                    break;
//                }
//            }
//            if(!$valid){ // if the licence is not valid, we take the first available licence
//                $session->setLicence($availableLicence[0]["licence"]);
//                $this->entityManager->persist($session);
//            }
//        }else{*/
//        $session->setLicence(null);
//        $this->entityManager->persist($session);
//        //throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot.');
//        //}
    }

    public function getAvailableLicence(
        AbstractProviderConfigurationHolder $configurationHolder,
        \DateTime $dateStart,
        \DateTime $dateEnd,
        ?int $excludeSessionId = null
    ): array {
        /** @var WebexRestConfigurationHolder $configurationHolder */
        /*if (!$configurationHolder->isAutomaticLicensing()) {
            return [];
        }*/
        $availableLicences = $this->webexRestLicensingService->getAvailableLicences($configurationHolder, $dateStart, $dateEnd, 30, $excludeSessionId);
        if (count($availableLicences) === 0) {
            throw new LicenceCheckException('You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot', [], 'No licence available for time slot');
        }

        return $availableLicences;
    }

    public function getRemindExpireAccessToken(): array
    {
        return array_reduce($this->webexRestMeetingConfigurationHolderRepository->getRemindExpireAccessToken(), function ($carry, $item) {
            /** @var WebexRestConfigurationHolder $item */
            $color = ($item->getExpireAtToken() <= ((new \DateTime())->modify('+2 days'))->setTime(0, 0)) ? 'alert-danger' : 'alert-warning';
            $carry[] = ['color' => $color, 'name' => $item->getName(), 'client' => $item->getClient()->getName(), 'expireAt' => $item->getExpireAtToken()->format('Y-m-d')];

            return $carry;
        }, []);
    }

    public function updateAccessToken(WebexRestConfigurationHolder $webexRestConfigurationHolder): ProviderResponse
    {
        $response = $this->connector->call(
            $webexRestConfigurationHolder,
            WebexRestMeetingConnector::ACTION_REST_UPDATE_ACCESS_TOKEN,
            $webexRestConfigurationHolder
        );

        if ($response->isSuccess()) {
            $webexRestConfigurationHolder->setClientAccessToken($response->getData()['access_token']);
            $webexRestConfigurationHolder->setExpireAtToken((new \DateTime())->setTimestamp(time() + $response->getData()['expires_in']));
        }

        return $response;
    }
}
