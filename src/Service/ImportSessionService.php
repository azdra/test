<?php

namespace App\Service;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Repository\AbstractProviderConfigurationHolderRepository;
use App\Repository\ClientRepository;
use App\Service\AsyncWorker\AsyncTaskService;
use App\Service\AsyncWorker\Tasks\ActionsFromImportSessionTask;
use App\Service\Provider\Adobe\AdobeSessionImportDataReader;
use App\Service\Provider\Adobe\AdobeWebinarSessionImportDataReader;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamAdobeSessionImportDataReader;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamAdobeWebinarSessionImportDataReader;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamMicrosoftTeamsEventSessionImportDataReader;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamMicrosoftTeamsSessionImportDataReader;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamWebexRestSessionImportDataReader;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamWebexSessionImportDataReader;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamWhiteSessionImportDataReader;
use App\Service\Provider\Microsoft\MicrosoftTeamsEventSessionImportDataReader;
use App\Service\Provider\Microsoft\MicrosoftTeamsSessionImportDataReader;
use App\Service\Provider\Model\ImportDataReader;
use App\Service\Provider\Webex\WebexRestSessionImportDataReader;
use App\Service\Provider\Webex\WebexSessionImportDataReader;
use App\Service\Provider\White\WhiteSessionImportDataReader;
use App\Service\RessourceUploader\RessourceUploader;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class ImportSessionService extends AbstractImportService
{
    public const AUTHORIZED_EXTENSION_OLD = ['xls', 'xlsx', 'csv', 'ods'];
    public const AUTHORIZED_EXTENSION = ['xls', 'xlsx'];
    public const MAX_FILE_SIZE = 4000000;
    public const ALLOWED_MAX_ROW = 2000;

    public function __construct(
        AsyncTaskService $asyncTaskService,
        string $importsSavefilesDirectory,
        Security $security,
        ClientRepository $clientRepository,
        AbstractProviderConfigurationHolderRepository $configurationHolderRepository,
        TranslatorInterface $translator,
        RessourceUploader $ressourceUploader,
        private AdobeSessionImportDataReader $adobeSessionImportDataReader,
        private AdobeWebinarSessionImportDataReader $adobeWebinarSessionImportDataReader,
        private WebexSessionImportDataReader $webexSessionImportDataReader,
        private WebexRestSessionImportDataReader $webexRestSessionImportDataReader,
        private MicrosoftTeamsSessionImportDataReader $microsoftTeamsSessionImportDataReader,
        private MicrosoftTeamsEventSessionImportDataReader $microsoftTeamsEventSessionImportDataReader,
        private WhiteSessionImportDataReader $whiteSessionImportDataReader,
        private IfcamAdobeSessionImportDataReader $ifcamAdobeSessionImportDataReader,
        private IfcamAdobeWebinarSessionImportDataReader $ifcamAdobeWebinarSessionImportDataReader,
        private IfcamWebexSessionImportDataReader $ifcamWebexSessionImportDataReader,
        private IfcamWebexRestSessionImportDataReader $ifcamWebexRestSessionImportDataReader,
        private IfcamMicrosoftTeamsSessionImportDataReader $ifcamMicrosoftTeamsSessionImportDataReader,
        private IfcamMicrosoftTeamsEventSessionImportDataReader $ifcamMicrosoftTeamsEventSessionImportDataReader,
        private IfcamWhiteSessionImportDataReader $ifcamWhiteSessionImportDataReader,
    ) {
        parent::__construct($asyncTaskService, $importsSavefilesDirectory, $security, $clientRepository, $configurationHolderRepository, $translator, $ressourceUploader);
    }

    protected function getDataReader(AbstractProviderConfigurationHolder $configurationHolder): ImportDataReader
    {
        if ($configurationHolder->getClient()->getId() == Client::ID_CLIENT_IFCAM) {
            return match (get_class($configurationHolder)) {
                AdobeConnectConfigurationHolder::class => $this->ifcamAdobeSessionImportDataReader,
                AdobeConnectWebinarConfigurationHolder::class => $this->ifcamAdobeWebinarSessionImportDataReader,
                WebexConfigurationHolder::class => $this->ifcamWebexSessionImportDataReader,
                WebexRestConfigurationHolder::class => $this->ifcamWebexRestSessionImportDataReader,
                MicrosoftTeamsConfigurationHolder::class => $this->ifcamMicrosoftTeamsSessionImportDataReader,
                MicrosoftTeamsEventConfigurationHolder::class => $this->ifcamMicrosoftTeamsEventSessionImportDataReader,
                WhiteConfigurationHolder::class => $this->ifcamWhiteSessionImportDataReader,
                default => throw new InvalidProviderConfigurationHandler()
            };
        } else {
            return match (get_class($configurationHolder)) {
                AdobeConnectConfigurationHolder::class => $this->adobeSessionImportDataReader,
                AdobeConnectWebinarConfigurationHolder::class => $this->adobeWebinarSessionImportDataReader,
                WebexConfigurationHolder::class => $this->webexSessionImportDataReader,
                WebexRestConfigurationHolder::class => $this->webexRestSessionImportDataReader,
                MicrosoftTeamsConfigurationHolder::class => $this->microsoftTeamsSessionImportDataReader,
                MicrosoftTeamsEventConfigurationHolder::class => $this->microsoftTeamsEventSessionImportDataReader,
                WhiteConfigurationHolder::class => $this->whiteSessionImportDataReader,
                default => throw new InvalidProviderConfigurationHandler()
            };
        }
    }

    protected function getAllowedExtensions(): array
    {
        return self::AUTHORIZED_EXTENSION;
    }

    protected function getMaxFileSize(): int
    {
        return self::MAX_FILE_SIZE;
    }

    protected function getRowLimit(): int
    {
        return self::ALLOWED_MAX_ROW;
    }

    protected function getFlagOrigin(): string
    {
        return 'ImportSessions_'.$this->asyncTaskService->randomFlag(20);
    }

    protected function getAsyncTaskType(): string
    {
        return ActionsFromImportSessionTask::class;
    }
}
