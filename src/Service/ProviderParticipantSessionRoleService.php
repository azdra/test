<?php

namespace App\Service;

use App\Entity\EvaluationProviderSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Exception\ProviderSessionAccessToken\DecryptProviderSessionAccessTokenException;
use App\Exception\ProviderSessionAccessToken\InvalidProviderSessionAccessTokenException;
use App\Exception\ProviderSessionAccessToken\UnauthorizedProviderAccessTokenException;
use App\Repository\ProviderParticipantSessionRoleRepository;

class ProviderParticipantSessionRoleService
{
    public function __construct(
        private ProviderParticipantService $providerSessionService,
        private string $appSecret,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository
    ) {
    }

    /**
     * @throws DecryptProviderSessionAccessTokenException
     * @throws UnauthorizedProviderAccessTokenException
     * @throws InvalidProviderSessionAccessTokenException
     */
    public function getAndValidFromToken(string $token): ?ProviderParticipantSessionRole
    {
        /** @var ProviderParticipantSessionRole $providerParticipantSessionRole */
        $participantSessionRole = $this->providerParticipantSessionRoleRepository->findOneBy(['providerSessionAccessToken.token' => $token]);

        $this->hasValidSessionAccessToken($participantSessionRole);
        $this->isAuthorizedToAccessToTheSession($participantSessionRole);

        return $participantSessionRole;
    }

    /**
     * @throws DecryptProviderSessionAccessTokenException
     * @throws UnauthorizedProviderAccessTokenException
     * @throws InvalidProviderSessionAccessTokenException
     */
    public function getAndValidFromTokenForEvaluationProviderSession(EvaluationProviderSession $evaluationProviderSession, string $token): ?ProviderParticipantSessionRole
    {
        /** @var ProviderParticipantSessionRole $providerParticipantSessionRole */
        $participantSessionRole = $this->providerParticipantSessionRoleRepository->findOneBy(['providerSessionAccessToken.token' => $token]);

        $this->hasValidSessionAccessToken($participantSessionRole);
        $this->isAuthorizedToAccessToTheEvaluation($evaluationProviderSession, $participantSessionRole);

        return $participantSessionRole;
    }

    /**
     * @throws InvalidProviderSessionAccessTokenException
     * @throws DecryptProviderSessionAccessTokenException
     */
    public function hasValidSessionAccessToken(ProviderParticipantSessionRole $providerParticipantSessionRole): bool
    {
        $providerSession = $providerParticipantSessionRole->getSession();
        $providerParticipant = $providerParticipantSessionRole->getParticipant();

        $data = $this->providerSessionService->decryptSessionAccessToken($this->appSecret,
            $providerParticipantSessionRole->getProviderSessionAccessToken());

        $providerParticipantEmail = $data[0];
        $providerSessionReference = $data[1];
        $providerParticipantSessionRoleRole = $data[2];

        if ($providerParticipantEmail !== $providerParticipant->getEmail()
            || $providerSessionReference !== $providerSession->getRef()
            || $providerParticipantSessionRoleRole !== $providerParticipantSessionRole->getRole()
            || $providerParticipantSessionRole->getRole() === ProviderParticipantSessionRole::ROLE_REMOVE) {
            throw new InvalidProviderSessionAccessTokenException($providerParticipantSessionRole->getProviderSessionAccessToken(), $data);
        }

        return true;
    }

    /**
     * @throws DecryptProviderSessionAccessTokenException
     * @throws UnauthorizedProviderAccessTokenException
     */
    public function isAuthorizedToAccessToTheSession(ProviderParticipantSessionRole $providerParticipantSessionRole): bool
    {
        $isAuthorizedToAccess = true;
        $providerSession = $providerParticipantSessionRole->getSession();

        $data = $this->providerSessionService->decryptSessionAccessToken($this->appSecret,
            $providerParticipantSessionRole->getProviderSessionAccessToken());

        if ($providerParticipantSessionRole->isATrainee()) {
            $minimumAccessDate = (clone $providerSession->getDateStart())->modify('- 30min');
            $isAuthorizedToAccess = new \DateTime() >= $minimumAccessDate;
        }

        if (!$isAuthorizedToAccess) {
            throw new UnauthorizedProviderAccessTokenException($providerParticipantSessionRole->getProviderSessionAccessToken(), $data);
        }

        return true;
    }

    public function isAuthorizedToAccessToTheEvaluation(EvaluationProviderSession $evaluationProviderSession, ProviderParticipantSessionRole $providerParticipantSessionRole): bool
    {
        $providerSession = $providerParticipantSessionRole->getSession();
        $currentDate = new \DateTime();

        $data = $this->providerSessionService->decryptSessionAccessToken(
            $this->appSecret,
            $providerParticipantSessionRole->getProviderSessionAccessToken()
        );

        $evaluationType = $evaluationProviderSession->getEvaluationType();
        $isAuthorizedToAccess = match ($evaluationType) {
            EvaluationProviderSession::EVALUATION_POSITIONING => true,
            EvaluationProviderSession::EVALUATION_KNOWLEDGE,
            EvaluationProviderSession::EVALUATION_HOT,
            EvaluationProviderSession::EVALUATION_ACQUIRED => $currentDate >= $providerSession->getDateStart(),
            EvaluationProviderSession::EVALUATION_COLD => $currentDate >= $providerSession->getDateEnd(),
            default => false
        };

        if (!$isAuthorizedToAccess) {
            throw new UnauthorizedProviderAccessTokenException($providerParticipantSessionRole->getProviderSessionAccessToken(), $data);
        }

        return $isAuthorizedToAccess;
    }

    public function getCountParticipantWhoCanReceiveSmsBySession(ProviderSession $session): array
    {
        return $this->providerParticipantSessionRoleRepository->countParticipantsBySession($session);
    }
}
