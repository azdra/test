<?php

namespace App\Service;

use App\Entity\Client\Client;
use App\Service\Utils\StrUtils;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SMSService
{
    private const API_URL = 'https://api.brevo.com/v3/transactionalSMS/sms';

    public function __construct(
        private LoggerInterface $logger,
        private HttpClientInterface $httpClient,
        private string $brevoMailerApiKey,
        private EntityManagerInterface $entityManager,
        private RouterInterface $router,
        private string $endPointDomain
    ) {
    }

    public function sendSmsBrevo(array $params): array
    {
        $this->validateParams($params);
        $recipient = $this->validateAndFormatRecipient($params['recipient']);

        if (!$recipient) {
            return ['error' => 'Invalid recipient number: '.$params['recipient']];
        }

        if (array_key_exists('client', $params) && $params['client'] instanceof Client) {
            if ($params['client']->getCreditSms() <= 0.0) {
                return ['error' => 'Insufficient credit for sending SMS.'];
            }
        }

        $body = $this->prepareRequestBody(StrUtils::senderFormated($params['sender']), $recipient, $params);

        try {
            $response = $this->httpClient->request('POST', self::API_URL, [
                'body' => json_encode($body),
                'headers' => [
                    'accept' => 'application/json',
                    'api-key' => $this->brevoMailerApiKey,
                    'content-type' => 'application/json',
                ],
            ]);

            if (array_key_exists('client', $params) && $params['client'] instanceof Client) {
                $this->decrementSmsCredit($params['client'], $response->toArray()['usedCredits']);
            }

            return $response->toArray();
        } catch (\Throwable $throwable) {
            $this->logger->error($throwable->getMessage());

            return ['error' => 'Failed to send SMS: '.$throwable->getMessage()];
        }
    }

    private function prepareRequestBody(string $sender, string $recipient, array $params): array
    {
        $body = [
            'sender' => $sender,
            'recipient' => $recipient,
            'content' => $params['content'],
            'type' => 'transactional',
            'tag' => $params['tag'],
            'unicodeEnabled' => true,
            'webUrl' => $this->endPointDomain.$this->router->generate('_webhook_brevo_sms_handle'),
//            'webUrl' => 'https://webhook.site/c51aff8b-432a-4982-bd2e-7f1d238f8455'
        ];

        return $body;
    }

    private function validateAndFormatRecipient(string $number): ?string
    {
        $number = preg_replace('/[^0-9+]/', '', $number);

        if (preg_match('/^0(6|7)\d{8}$/', $number)) {
            return '+33'.substr($number, 1);
        } elseif (preg_match('/^\+33(6|7)\d{8}$/', $number)) {
            return $number;
        } elseif (preg_match('/^33(6|7)\d{8}$/', $number)) {
            return '+'.$number;
        } elseif (preg_match('/^(6|7)\d{8}$/', $number)) {
            return '+33'.$number;
        }

        return null;
    }

    private function validateParams(array $params): void
    {
        if (empty($params['sender'])) {
            throw new \InvalidArgumentException('Invalid sender: sender is required and must be 11 characters or less.');
        }

        if (empty($params['recipient'])) {
            throw new \InvalidArgumentException('Invalid recipient: recipient is required.');
        }

        if (!isset($params['content']) || $params['content'] === '') {
            throw new \InvalidArgumentException('Invalid content: content is required and cannot be empty.');
        }
    }

    private function decrementSmsCredit(Client $client, int $credit): void
    {
        $client->decrementCreditsSms($credit);
        $this->entityManager->persist($client);
        $this->entityManager->flush();
    }
}
