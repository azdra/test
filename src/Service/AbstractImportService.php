<?php

namespace App\Service;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Exception\InvalidTemplateUploadedForImportException;
use App\Exception\MissingClientParameter;
use App\Exception\UploadFile\UploadFileNoFoundInRequestException;
use App\Exception\UploadFile\UploadFileNotAllowedExtException;
use App\Exception\UploadFile\UploadFileOverSizeException;
use App\Exception\UploadFile\UploadFileTooManyRowException;
use App\Repository\AbstractProviderConfigurationHolderRepository;
use App\Repository\ClientRepository;
use App\Service\AsyncWorker\AsyncTaskService;
use App\Service\Provider\Model\ImportDataReader;
use App\Service\RessourceUploader\RessourceUploader;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Ods;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractImportService
{
    public const MAX_ROW_BY_TASK = 50;

    public function __construct(
        protected AsyncTaskService $asyncTaskService,
        protected string $importsSavefilesDirectory,
        protected Security $security,
        protected ClientRepository $clientRepository,
        protected AbstractProviderConfigurationHolderRepository $configurationHolderRepository,
        protected TranslatorInterface $translator,
        protected RessourceUploader $ressourceUploader
    ) {
    }

    /**
     * @throws UploadFileOverSizeException
     * @throws UploadFileNotAllowedExtException
     * @throws MissingClientParameter
     * @throws UploadFileNoFoundInRequestException
     * @throws UploadFileTooManyRowException
     * @throws InvalidTemplateUploadedForImportException
     */
    public function createImportAsyncTasksFromRequest(Request $request): void
    {
        $this->validateRequest($request);
        /* @var Client $client */
        $client = $this->clientRepository->find($request->request->get('client'));

        /* @var AbstractProviderConfigurationHolder $configurationHolder */
        if ($request->request->has('configurationHolder')) {
            $configurationHolder = $this->configurationHolderRepository->find($request->request->get('configurationHolder'));
        } else {
            $configurationHolder = $client->getConfigurationHolders()->first();
        }

        $file = $request->files->get('file');

        $this->validateFile($file);

        $spreadsheet = $this->getSpreadsheet($file);
        $dataReader = $this->getDataReader($configurationHolder);

        $sheet = $dataReader->getSheet($spreadsheet);

        $dataReader->validateTemplateLoadedIsValid($sheet);
        $this->validateData($sheet);

        $flagOrigin = $this->getFlagOrigin();
        $this->saveFile($file, $flagOrigin);

        $rowsData = $dataReader->readData($sheet, $client);

        $this->createAsyncTasks($rowsData, $file, $client, $flagOrigin, $configurationHolder);
    }

    /**
     * @throws MissingClientParameter
     * @throws UploadFileNoFoundInRequestException
     */
    private function validateRequest(Request $request): void
    {
        if (empty($request->request->get('client'))) {
            throw new MissingClientParameter('No account is associated with the import');
        }

        if (empty($request->files->get('file')) || empty($request->files->get('file')->getClientOriginalExtension())) {
            throw new UploadFileNoFoundInRequestException($this->translator->trans('Not file is present'));
        }
    }

    /**
     * @throws UploadFileNotAllowedExtException
     * @throws UploadFileOverSizeException
     */
    public function validateFile(UploadedFile $file): void
    {
        $this->ressourceUploader->isFileUploadedValid($file, $this->getAllowedExtensions(), $this->getMaxFileSize());
    }

    /**
     * @throws UploadFileTooManyRowException
     */
    public function validateData(Worksheet $sheet): void
    {
        if ($this->getRowLimit() > 0 && $sheet->getHighestRow() > $this->getRowLimit()) {
            throw new UploadFileTooManyRowException($this->translator->trans('The file contains too many lines, the maximum allowed is %limit%', ['%limit%' => $this->getRowLimit()]));
        }
    }

    private function getSpreadsheet(UploadedFile $file): Spreadsheet
    {
        $reader = match ($file->getClientOriginalExtension()) {
            'xls' => new Xls(),
            'xlsx' => new Xlsx(),
            'csv' => new Csv(),
            'ods' => new Ods()
        };
        $reader->setReadDataOnly(true);
        $reader->setReadEmptyCells(false);

        return $reader->load($file->getRealPath());
    }

    private function saveFile(UploadedFile $file, string $newBasename): void
    {
        ( new Filesystem() )->copy(
            $file->getRealPath(),
            $this->importsSavefilesDirectory.'/'.$newBasename.'.'.$file->getClientOriginalExtension(),
            true
        );
    }

    private function createAsyncTasks(array $rows, UploadedFile $file, Client $client, string $flagOrigin, AbstractProviderConfigurationHolder $configurationHolder): void
    {
        $currentUser = $this->security->getUser();
        $infos = [
            'numberRows' => count($rows),
            'configurationHolder' => $configurationHolder->getId(),
        ];

        foreach (array_chunk($rows, self::MAX_ROW_BY_TASK) as $bloc) {
            $this->asyncTaskService->publish($this->getAsyncTaskType(), [
                'rows' => $bloc,
            ], $flagOrigin, $client, $flagOrigin.'.'.$file->getClientOriginalExtension(), $currentUser, $infos);
        }
    }

    abstract protected function getAllowedExtensions(): array;

    abstract protected function getMaxFileSize(): int;

    abstract protected function getRowLimit(): int;

    abstract protected function getFlagOrigin(): string;

    abstract protected function getDataReader(AbstractProviderConfigurationHolder $configurationHolder): ImportDataReader;

    abstract protected function getAsyncTaskType(): string;
}
