<?php

namespace App\Service;

use App\Entity\ProviderSession;
use App\Entity\User;
use App\Exception\Exporter\ExporterNotFoundException;
use App\Exception\InvalidArgumentException;
use App\Model\SendMailFormModel;
use App\Service\Exporter\ExporterFactory;
use App\Service\Exporter\Pdf\PdfAttendanceReportProviderSessionExporter;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetAttendanceReportProviderSessionExporter;
use App\Service\Exporter\Spreadsheet\Xlsx\XslxSpreadsheetAttendanceReportProviderSessionExporter;
use Symfony\Contracts\Translation\TranslatorInterface;

class AttendanceReportService
{
    public function __construct(
        private MailerService $mailerService,
        private TranslatorInterface $translator,
        private ExporterFactory $exporterFactory
    ) {
    }

    public function sendAttendanceReportByMailRequest(
        ProviderSession $providerSession,
        SendMailFormModel $mails,
        User $sender
    ): void {
        $this->sendAttendanceReportByMail($providerSession, $mails->getRecipients()['tags'], $mails->getCc()['tags'], $mails->getBcc()['tags'], $sender);
    }

    /**
     * @throws ExporterNotFoundException
     * @throws InvalidArgumentException
     */
    public function sendAttendanceReportByMail(ProviderSession $providerSession, array $recipients, array $ccs = [], array $bccs = [], ?User $sender = null): void
    {
        $attachments = [];
        $attachments[] = $this->exporterFactory
            ->getExporter(PdfAttendanceReportProviderSessionExporter::EXPORT_TYPE, PdfAttendanceReportProviderSessionExporter::EXPORT_FORMAT)
            ->createExportFile($providerSession);
        $attachments[] = $this->exporterFactory
            ->getExporter(AbstractSpreadsheetAttendanceReportProviderSessionExporter::EXPORT_TYPE, XslxSpreadsheetAttendanceReportProviderSessionExporter::EXPORT_FORMAT)
            ->createExportFile($providerSession);

        $context = ['session' => $providerSession, 'client' => $providerSession->getClient()];
        if (!empty($sender)) {
            $context['user'] = $sender;
        }
        $this->mailerService->sendTemplatedMail(
            address: $recipients,
            subject: $this->generateSubject($providerSession),
            template: 'emails/manager_client/attendance_report.html.twig',
            context: $context,
            pathToAttach: $attachments,
            ccs: $ccs,
            bccs: $bccs,
            senderSystem: true
        );

        foreach ($attachments as $attachment) {
            unlink($attachment);
        }
    }

    private function generateSubject(ProviderSession $session): string
    {
        return $this->translator->trans('Attendance report - %session% of %date% (%ref%)', [
            '%session%' => $session->getName(),
            '%date%' => $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i'),
            '%ref%' => $session->getRef(),
        ]);
    }
}
