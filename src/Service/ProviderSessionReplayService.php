<?php

namespace App\Service;

use App\Entity\ProviderSessionAccessToken;
use App\Entity\ProviderSessionReplay;
use App\Model\GeneratorSessionTrait;
use Doctrine\ORM\EntityManagerInterface;

class ProviderSessionReplayService
{
    use GeneratorSessionTrait;

    private string $appSecret;

    public function __construct(string $appSecret, private readonly EntityManagerInterface $entityManager)
    {
        $this->appSecret = $appSecret;
    }

    public function generateSessionReplay(?ProviderSessionReplay &$providerSessionReplay, array &$params): void
    {
        if (!$providerSessionReplay) {
            $providerSessionReplay = new ProviderSessionReplay();
        }

        $providerSessionReplay->setSession($params['session'])
                              ->setFirstname($params['firstname'])
                              ->setLastname($params['lastname'])
                              ->setEmail($params['email'])
                              ->setRegistrationFormResponses($params['registrationFormResponses']);

        $this->setProviderSessionReplayToken($providerSessionReplay);

        $this->entityManager->persist($providerSessionReplay);
        $this->entityManager->flush();
    }

    public function setProviderSessionReplayToken(ProviderSessionReplay $providerSessionReplay): ProviderSessionAccessToken
    {
        return $this->encryptSessionReplayToken($this->appSecret, $providerSessionReplay);
    }
}
