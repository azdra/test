<?php

namespace App\Service\Connector;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Model\ProviderResponse;

/**
 * Interface ConnectorInterface.
 */
interface ConnectorInterface
{
    public function call(AbstractProviderConfigurationHolder $configurationHolder, string $action, mixed $parameters = null): ProviderResponse;
}
