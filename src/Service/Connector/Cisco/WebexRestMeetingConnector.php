<?php

namespace App\Service\Connector\Cisco;

use App\Entity\Cisco\WebexMeetingTelephonyProfile;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Cisco\WebexRestMeetingTelephonyProfile;
use App\Entity\Cisco\WebexRestParticipant;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderParticipantSessionRole;
use App\Exception\AbstractProviderException;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\ProviderGenericException;
use App\Exception\UnexpectedProviderActionErrorException;
use App\Model\ProviderResponse;
use App\Repository\WebexRestParticipantRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class WebexRestMeetingConnector extends AbstractWebexConnector
{
    /*public const ACTION_CREATE_MEETING = 'meeting.CreateMeeting';
    public const ACTION_TELEPHONY_PROFILE_INFO = 'meeting.GetMeeting';
    public const ACTION_SET_MEETING = 'meeting.SetMeeting';
    public const ACTION_GET_MEETING = 'meeting.GetMeeting';
    public const ACTION_DELETE_MEETING = 'meeting.DelMeeting';
    public const ACTION_GET_HOST_URL_MEETING = 'meeting.GethosturlMeeting';
    public const ACTION_GET_JOIN_URL_MEETING = 'meeting.GetjoinurlMeeting';
    public const ACTION_GET_CONNECTOR_JOIN_URL_MEETING = 'meeting.GetHostjoinurlMeeting';
    public const ACTION_LIST_MEETING_ATTENDEE = 'attendee.LstMeetingAttendee';
    public const ACTION_LIST_MEETING_ATTENDEE_HISTORY = 'history.LstmeetingattendeeHistory';
    public const ACTION_CREATE_MEETING_ATTENDEE = 'attendee.CreateMeetingAttendee';
    public const ACTION_DELETE_MEETING_ATTENDEE = 'attendee.DelMeetingAttendee';
    public const ACTION_GET_USER = 'user.GetUser';
    public const ACTION_USER_LOGIN_TICKET = 'user.GetLoginTicket';
    public const ACTION_GET_MEETING_LINK = 'meeting.GetMeetingLink';*/

    public const ACTION_TEST_CONNECTOR = 'webexServiceApp.testConnector';
    public const ACTION_REST_GET_ACCESS_TOKEN = 'webexServiceApp.regenateAccesstoken';
    public const ACTION_REST_GET_LIST_MEETINGS = 'meeting.getListMeetings';
    public const ACTION_REST_GET_MEETING_BY_ID = 'meeting.getMeetingById';
    public const ACTION_REST_CREATE_MEETING = 'meeting.createMeeting';
    public const ACTION_REST_UPDATE_MEETING = 'meeting.updateMeeting';
    public const ACTION_REST_DELETE_MEETING = 'meeting.deleteMeeting';
    public const ACTION_REST_UPDATE_ACCESS_TOKEN = 'meeting.updateAccessToken';
    public const ACTION_REST_JOIN_A_MEETING = 'meeting.joinAMeeting';
    public const ACTION_REST_GET_LIST_MEETING_TEMPLATES = 'meeting.getListMeetingTemplates';
    public const ACTION_REST_GET_MEETING_TEMPLATE = 'meeting.getMeetingTemplate';
    public const ACTION_REST_GET_MEETING_CONTROL_STATUS = 'meeting.getMeetingControlStatus';
    public const ACTION_REST_LIST_MEETING_SESSION_TYPES = 'meeting.listMeetingSessionTypes';
    public const ACTION_REST_GET_MEETING_SESSION_TYPE = 'meeting.getMeetingSessionType';
    public const ACTION_REST_GET_REGISTRATION_FORM_FOR_A_MEETING = 'meeting.getRegistrationFormForAMeeting';
    public const ACTION_REST_UPDATE_MEETING_REGISTRATION_FORM = 'meeting.updateMeetingRegistrationForm';
    public const ACTION_REST_DELETE_MEETING_REGISTRATION_FORM = 'meeting.deleteMeetingRegistrationForm';
    public const ACTION_REST_REGISTER_A_MEETING_REGISTRANTS = 'meeting.registerAMeetingRegistrants';
    public const ACTION_REST_BATCH_REGISTER_MEETING_REGISTRANTS = 'meeting.batchRegisterMeetingRegistrants';
    public const ACTION_REST_GET_A_MEETING_REGISTRANTS_DETAIL_INFORMATION = 'meeting.getAMeetingRegistrantsDetailInformation';
    public const ACTION_REST_GET_LIST_MEETING_REGISTRANTS = 'meeting.getListMeetingRegistrants';
    public const ACTION_REST_QUERY_MEETING_REGISTRANTS = 'meeting.queryMeetingRegistrants';
    public const ACTION_REST_BATCH_UPDATE_MEETING_REGISTRANTS_STATUS = 'meeting.batchUpdateMeetingRegistrantsStatus';
    public const ACTION_REST_DELETE_A_MEETING_REGISTRANT = 'meeting.deleteAMeetingRegistrant';
    public const ACTION_REST_UPDATE_MEETING_SIMULTANEOUS_INTERPRETATION = 'meeting.updateMeetingSimultaneousInterpretation';
    public const ACTION_REST_CREATE_A_MEETING_INTERPRETER = 'meeting.createAMeetingInterpreter';
    public const ACTION_REST_GET_A_MEETING_INTERPRETER = 'meeting.getAMeetingInterpreter';
    public const ACTION_REST_LIST_MEETING_INTERPRETERS = 'meeting.listMeetingInterpreters';
    public const ACTION_REST_UPDATE_A_MEETING_INTERPRETER = 'meeting.updateAMeetingInterpreter';
    public const ACTION_REST_DELETE_A_MEETING_INTERPRETER = 'meeting.deleteAMeetingInterpreter';
    public const ACTION_REST_UPDATE_MEETING_BREAKOUT_SESSIONS = 'meeting.updateMeetingBreakoutSessions';
    public const ACTION_REST_LIST_MEETING_BREAKOUT_SESSIONS = 'meeting.listMeetingBreakoutSessions';
    public const ACTION_REST_DELETE_MEETING_BREAKOUT_SESSIONS = 'meeting.deleteMeetingBreakoutSessions';
    public const ACTION_REST_GET_A_MEETING_SURVEY = 'meeting.getAMeetingSurvey';
    public const ACTION_REST_LIST_MEETING_SURVEY_RESULTS = 'meeting.listMeetingSurveyResults';
    public const ACTION_REST_CREATE_INVITATION_SOURCES = 'meeting.createInvitationSources';
    public const ACTION_REST_LIST_INVITATION_SOURCES = 'meeting.listInvitationSources';
    public const ACTION_REST_LIST_MEETING_TRACKING_CODES = 'meeting.listMeetingTrackingCodes';
    public const ACTION_REST_REASSIGN_MEETINGS_TO_A_NEW_HOST = 'meeting.reassignMeetingsToANewHost';
    public const ACTION_REST_LIST_MEETING_INVITEES = 'meetingInvitees.listMeetingInvitees';
    public const ACTION_REST_CREATE_A_MEETING_INVITEE = 'meetingInvitees.createAMeetingInvitee';
    public const ACTION_REST_CREATE_MEETING_INVITEES = 'meetingInvitees.createMeetingInvitees';
    public const ACTION_REST_GET_A_MEETING_INVITEE = 'meetingInvitees.getAMeetingInvitee';
    public const ACTION_REST_UPDATE_A_MEETING_INVITEE = 'meetingInvitees.updateAMeetingInvitee';
    public const ACTION_REST_DELETE_A_MEETING_INVITEE = 'meetingInvitees.deleteAMeetingInvitee';
    public const ACTION_REST_LIST_MEETING_PARTICIPANTS = 'meetingParticipants.listMeetingParticipants';
    public const ACTION_REST_LIST_MEETING_ATTENDEES = 'meetingParticipants.listMeetingAttendees';
    public const ACTION_REST_QUERY_MEETING_PARTICIPANTS_WITH_EMAIL = 'meetingParticipants.queryMeetingParticipantsWithEmail';
    public const ACTION_REST_GET_MEETING_PARTICIPANT_DETAILS = 'meetingParticipants.getMeetingParticipantDetails';
    public const ACTION_REST_UPDATE_A_PARTICIPANT = 'meetingParticipants.updateAParticipant';
    public const ACTION_REST_ADMIT_PARTICIPANTS = 'meetingParticipants.admitParticipants';
    public const ACTION_REST_TELEPHONY_PROFILE_INFO = 'meeting.telephonyProfileInfo';

    public const ACTION_REST_GET_JOIN_URL_MEETING = 'meeting.getJoinUrlMeeting';

    /**
     * WebexMeetingConnector constructor.
     */
    public function __construct(HttpClientInterface $httpClient, private Environment $twig, private WebexRestParticipantRepository $webexRestParticipantRepository, protected LoggerInterface $logger, private LoggerInterface $providerLogger)
    {
        parent::__construct($httpClient);
    }

    /**
     * @throws InvalidProviderConfigurationHandler
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    public function call(AbstractProviderConfigurationHolder $configurationHolder, string $action, mixed $parameters = null): ProviderResponse
    {
        if (!($configurationHolder instanceof WebexRestConfigurationHolder)) {
            throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
        }

        if (!$configurationHolder->isValid()) {
            throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
        }

        $connectorResponse = new ProviderResponse();
        try {
            $connectorResponse->setData(match ($action) {
                self::ACTION_TEST_CONNECTOR => $this->testConnector($configurationHolder),
                //self::ACTION_REST_GET_ACCESS_TOKEN => $this->getAccessToken($configurationHolder),
                self::ACTION_REST_GET_LIST_MEETINGS => $this->getListMeetings($configurationHolder),
                self::ACTION_REST_GET_MEETING_BY_ID => $this->getMeetingById($configurationHolder, $parameters),
                self::ACTION_REST_CREATE_MEETING => $this->createMeeting($configurationHolder, $parameters),
                self::ACTION_REST_UPDATE_MEETING => $this->updateMeeting($configurationHolder, $parameters),
                self::ACTION_REST_UPDATE_ACCESS_TOKEN => $this->updateAccessToken($configurationHolder),
                self::ACTION_REST_DELETE_MEETING => $this->deleteMeeting($configurationHolder, $parameters),
                self::ACTION_REST_TELEPHONY_PROFILE_INFO => $this->telephonyProfileInfo($configurationHolder, $parameters),
                /*self::ACTION_REST_JOIN_A_MEETING => $this->joinAMeeting($configurationHolder, $parameters),
                self::ACTION_REST_GET_LIST_MEETING_TEMPLATES => $this->getListMeetingTemplates($configurationHolder, $parameters),
                self::ACTION_REST_GET_MEETING_TEMPLATE => $this->getMeetingTemplate($configurationHolder, $parameters),
                self::ACTION_REST_GET_MEETING_CONTROL_STATUS => $this->getMeetingControlStatus($configurationHolder, $parameters),
                self::ACTION_REST_LIST_MEETING_SESSION_TYPES => $this->listMeetingSessionTypes($configurationHolder, $parameters),
                self::ACTION_REST_GET_MEETING_SESSION_TYPE => $this->getMeetingSessionType($configurationHolder, $parameters),
                self::ACTION_REST_GET_REGISTRATION_FORM_FOR_A_MEETING => $this->getRegistrationFormForAMeeting($configurationHolder, $parameters),
                self::ACTION_REST_UPDATE_MEETING_REGISTRATION_FORM => $this->updateMeetingRegistrationForm($configurationHolder, $parameters),
                self::ACTION_REST_DELETE_MEETING_REGISTRATION_FORM => $this->deleteMeetingRegistrationForm($configurationHolder, $parameters),
                self::ACTION_REST_REGISTER_A_MEETING_REGISTRANTS => $this->registerAMeetingRegistrants($configurationHolder, $parameters),
                self::ACTION_REST_BATCH_REGISTER_MEETING_REGISTRANTS => $this->batchRegisterMeetingRegistrants($configurationHolder, $parameters),
                self::ACTION_REST_GET_A_MEETING_REGISTRANTS_DETAIL_INFORMATION => $this->getAMeetingRegistrantsDetailInformation($configurationHolder, $parameters),
                self::ACTION_REST_GET_LIST_MEETING_REGISTRANTS => $this->getListMeetingRegistrants($configurationHolder, $parameters),
                self::ACTION_REST_QUERY_MEETING_REGISTRANTS => $this->queryMeetingRegistrants($configurationHolder, $parameters),
                self::ACTION_REST_BATCH_UPDATE_MEETING_REGISTRANTS_STATUS => $this->batchUpdateMeetingRegistrantsStatus($configurationHolder, $parameters),
                self::ACTION_REST_DELETE_A_MEETING_REGISTRANT => $this->deleteAMeetingRegistrant($configurationHolder, $parameters),
                self::ACTION_REST_UPDATE_MEETING_SIMULTANEOUS_INTERPRETATION => $this->updateMeetingSimultaneousInterpretation($configurationHolder, $parameters),
                self::ACTION_REST_CREATE_A_MEETING_INTERPRETER => $this->createAMeetingInterpreter($configurationHolder, $parameters),
                self::ACTION_REST_GET_A_MEETING_INTERPRETER => $this->getAMeetingInterpreter($configurationHolder, $parameters),
                self::ACTION_REST_LIST_MEETING_INTERPRETERS => $this->listMeetingInterpreters($configurationHolder, $parameters),
                self::ACTION_REST_UPDATE_A_MEETING_INTERPRETER => $this->updateAMeetingInterpreter($configurationHolder, $parameters),
                self::ACTION_REST_DELETE_A_MEETING_INTERPRETER => $this->deleteAMeetingInterpreter($configurationHolder, $parameters),
                self::ACTION_REST_UPDATE_MEETING_BREAKOUT_SESSIONS => $this->updateMeetingBreakoutSessions($configurationHolder, $parameters),
                self::ACTION_REST_LIST_MEETING_BREAKOUT_SESSIONS => $this->listMeetingBreakoutSessions($configurationHolder, $parameters),
                self::ACTION_REST_DELETE_MEETING_BREAKOUT_SESSIONS => $this->deleteMeetingBreakoutSessions($configurationHolder, $parameters),
                self::ACTION_REST_GET_A_MEETING_SURVEY => $this->getAMeetingSurvey($configurationHolder, $parameters),
                self::ACTION_REST_LIST_MEETING_SURVEY_RESULTS => $this->listMeetingSurveyResults($configurationHolder, $parameters),
                self::ACTION_REST_CREATE_INVITATION_SOURCES => $this->createInvitationSources($configurationHolder, $parameters),
                self::ACTION_REST_LIST_INVITATION_SOURCES => $this->listInvitationSources($configurationHolder, $parameters),
                self::ACTION_REST_LIST_MEETING_TRACKING_CODES => $this->listMeetingTrackingCodes($configurationHolder, $parameters),
                self::ACTION_REST_REASSIGN_MEETINGS_TO_A_NEW_HOST => $this->reassignMeetingsToANewHost($configurationHolder, $parameters),*/
                self::ACTION_REST_LIST_MEETING_INVITEES => $this->listMeetingInvitees($configurationHolder, $parameters),
                self::ACTION_REST_CREATE_A_MEETING_INVITEE => $this->createAMeetingInvitee($configurationHolder, $parameters),
                self::ACTION_REST_CREATE_MEETING_INVITEES => $this->createMeetingInvitees($configurationHolder, $parameters),
                self::ACTION_REST_GET_A_MEETING_INVITEE => $this->getAMeetingInvitee($configurationHolder, $parameters),
                self::ACTION_REST_UPDATE_A_MEETING_INVITEE => $this->updateAMeetingInvitee($configurationHolder, $parameters),
                self::ACTION_REST_DELETE_A_MEETING_INVITEE => $this->deleteAMeetingInvitee($configurationHolder, $parameters),
                self::ACTION_REST_LIST_MEETING_PARTICIPANTS => $this->listMeetingParticipants($configurationHolder, $parameters),
                self::ACTION_REST_LIST_MEETING_ATTENDEES => $this->listMeetingAttendees($configurationHolder, $parameters),
                /*self::ACTION_REST_QUERY_MEETING_PARTICIPANTS_WITH_EMAIL => $this->queryMeetingParticipantsWithEmail($configurationHolder, $parameters),
                self::ACTION_REST_GET_MEETING_PARTICIPANT_DETAILS => $this->getMeetingParticipantDetails($configurationHolder, $parameters),
                self::ACTION_REST_UPDATE_A_PARTICIPANT => $this->updateAParticipant($configurationHolder, $parameters),
                self::ACTION_REST_ADMIT_PARTICIPANTS => $this->admitParticipants($configurationHolder, $parameters)*/

                /*self::ACTION_CREATE_MEETING => $this->createMeeting($configurationHolder, $parameters),
                self::ACTION_TELEPHONY_PROFILE_INFO => $this->telephonyProfileInfo($configurationHolder, $parameters),
                self::ACTION_SET_MEETING => $this->setMeeting($configurationHolder, $parameters),
                self::ACTION_GET_MEETING => $this->getMeeting($configurationHolder, $parameters),
                self::ACTION_DELETE_MEETING => $this->deleteMeeting($configurationHolder, $parameters),
                self::ACTION_GET_HOST_URL_MEETING => $this->getHostUrlMeeting($configurationHolder, $parameters),
                self::ACTION_GET_JOIN_URL_MEETING => $this->getJoinUrlMeeting($configurationHolder, $parameters),
                self::ACTION_GET_CONNECTOR_JOIN_URL_MEETING => $this->getConnectorJoinUrlMeeting($configurationHolder, $parameters),
                self::ACTION_LIST_MEETING_ATTENDEE => $this->listMeetingAttendee($configurationHolder, $parameters),
                self::ACTION_LIST_MEETING_ATTENDEE_HISTORY => $this->listMeetingAttendeeHistory($configurationHolder, $parameters),
                self::ACTION_CREATE_MEETING_ATTENDEE => $this->createMeetingAttendee($configurationHolder, $parameters),
                self::ACTION_DELETE_MEETING_ATTENDEE => $this->deleteMeetingAttendee($configurationHolder, $parameters),
                self::ACTION_GET_USER => $this->getUser($configurationHolder, $parameters),
                self::ACTION_USER_LOGIN_TICKET => $this->loginUser($configurationHolder),
                self::ACTION_GET_MEETING_LINK => $this->getMeetingLink($configurationHolder, $parameters),*/

                self::ACTION_REST_GET_JOIN_URL_MEETING => $this->getJoinUrlMeeting($configurationHolder, $parameters)
            });
        } catch (AbstractProviderException $exception) {
            $this->logger->error($exception);
            $connectorResponse->setData($exception->getMessage());
            $connectorResponse->setSuccess(false);
            $connectorResponse->setThrown($exception);
        }

        return $connectorResponse;
    }

    /*private function getAuthorizationCode(WebexRestConfigurationHolder $configurationHolder): string
    {
        $url = 'https://webexapis.com/v1/oauth/authorize';
        $params = [
            'response_type' => 'code',
            'client_id' => $configurationHolder->getClientId(),
            'redirect_uri' => $configurationHolder->getRedirectUri(),
            'scope' => 'spark:all',
        ];

        return $url.'?'.http_build_query($params);
    }*/

    public function sendCurlRequest(string $url, array $headers = [], string $method = 'GET', ?array $data = null): ?array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $method = strtoupper($method);
        if ($method === 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        } elseif ($method === 'PUT') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        } elseif ($method === 'DELETE') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }

        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            $error = curl_error($ch);
            curl_close($ch);
            throw new \Exception('Erreur lors de la requête cURL : '.$error);
        }

        curl_close($ch);

        $responseData = json_decode($response, true);

        return $responseData;
    }

    private function testConnector(WebexRestConfigurationHolder $configurationHolder): ProviderResponse
    {
        /*$url = 'https://api.webex.com/v1/refreshToken';
        $headers = [
            'Content-Type: application/x-www-form-urlencoded',
        ];
        $data = http_build_query([
            'grant_type' => 'refresh_token',
            'client_id' => $configurationHolder->getClientId(),
            'client_secret' => $configurationHolder->getClientSecret(),
            'refresh_token' => $configurationHolder->getClientRefreshToken(),
        ]);

        $response = $this->sendCurlRequest($url, $headers, 'POST', $data);
        $responseData = json_decode($response, true);

        if (isset($responseData['error'])) {
            $errorMessage = $responseData['error_description'] ?? 'Une erreur inconnue s\'est produite';
            throw new Exception('Erreur lors de la régénération des tokens : ' . $errorMessage);
        }

        return [
            'access_token' => $responseData['access_token'],
            'refresh_token' => $responseData['refresh_token'],
            'expires_in' => $responseData['expires_in'],
        ];*/

        $connectorResponse = new ProviderResponse();
        $connectorResponse->setData([]);
        $connectorResponse->setSuccess(false);

        return $connectorResponse;
    }

    /*private function getAccessToken(WebexRestConfigurationHolder $configurationHolder): mixed
    {
        $url = 'https://api.webex.com/v1/refreshToken';
        $headers = [
            'Content-Type: application/x-www-form-urlencoded',
        ];
        $data = http_build_query([
            'grant_type' => 'refresh_token',
            'client_id' => $configurationHolder->getClientId(),
            'client_secret' => $configurationHolder->getClientSecret(),
            'refresh_token' => $configurationHolder->getClientRefreshToken(),
        ]);

        $response = $this->sendCurlRequest($url, $headers, 'POST', $data);
        $responseData = json_decode($response, true);

        if (isset($responseData['error'])) {
            $errorMessage = $responseData['error'][0]['description'] ?? 'Une erreur inconnue s\'est produite';
            throw new \Exception('Erreur lors de la régénération des tokens : '.$errorMessage);
        }

        return [
            'access_token' => $responseData['access_token'],
            'refresh_token' => $responseData['refresh_token'],
            'expires_in' => $responseData['expires_in'],
        ];
    }*/

    /**
     * https://developer.webex.com/docs/api/v1/meetings/list-meetings.
     */
    private function getListMeetings(WebexRestConfigurationHolder $configurationHolder): mixed
    {
        //GET https://webexapis.com/v1/meetings?meetingNumber=123456789&webLink=https%253A%252F%252Fgo.webex.com%252Fgo%252Fj.php%253FMTID%253Dm066878aef4343e74c98b48439b71acfd&roomId=Y2lzY29zcGFyazovL3VzL1JPT00vNDMzZjk0ZjAtOTZhNi0xMWViLWJhOTctOTU3OTNjZDhiY2Q2&meetingType=%5Bobject%20Object%5D,%5Bobject%20Object%5D,%5Bobject%20Object%5D&state=%5Bobject%20Object%5D,%5Bobject%20Object%5D,%5Bobject%20Object%5D,%5Bobject%20Object%5D,%5Bobject%20Object%5D,%5Bobject%20Object%5D,%5Bobject%20Object%5D,%5Bobject%20Object%5D&scheduledType=%5Bobject%20Object%5D,%5Bobject%20Object%5D,%5Bobject%20Object%5D&isModified=false&hasChat=false&hasRecording=false&hasTranscription=false&hasClosedCaption=false&hasPolls=false&hasQA=false&current=false&from=2019-03-18T09:30:00+08:00&to=2019-03-25T09:30:00+08:00&max=100&hostEmail=john.andersen@example.com&siteUrl=example.webex.com&integrationTag=dbaeceebea5c4a63ac9d5ef1edfe36b9
        $url = 'https://webexapis.com/v1/meetings';

        $headers = [
            'Authorization: Bearer '.$configurationHolder->getClientAccessToken(),
            'Content-Type: application/json',
        ];

        $response = $this->sendCurlRequest($url, $headers, 'GET');

        if (isset($response['errors'])) {
            $errorMessage = $response['errors'][0]['message'] ?? 'Une erreur inconnue s\'est produite';
            throw new \Exception('Erreur Webex API : '.$errorMessage);
        }

        return $response['items'] ?? [];
    }

    /**
     * https://developer.webex.com/docs/api/v1/meetings/get-a-meeting.
     */
    private function getMeetingById(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        $meetingRestId = $webexRestMeeting->getMeetingRestId();

        //GET https://webexapis.com/v1/meetings/25bbf831-5be9-4c25-b4b0-9b592c8a086b?current=false&hostEmail=john.andersen@example.com
        $url = 'https://webexapis.com/v1/meetings/'.$meetingRestId.'?hostEmail='.$webexRestMeeting->getMeetingRestHostEmail();

        $headers = [
            'Authorization: Bearer '.$configurationHolder->getClientAccessToken(),
            'Content-Type: application/json',
        ];

        $response = $this->sendCurlRequest($url, $headers, 'GET');

        if (isset($response['errors'])) {
            $errorMessage = $response['message'] ?? 'Une erreur inconnue s\'est produite';
            throw new \Exception('Erreur Webex API : '.$errorMessage);
        }

        return $response;
    }

    private function telephonyProfileInfo(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): WebexRestMeetingTelephonyProfile
    {
        $meeting = $this->getMeetingById($configurationHolder, $webexRestMeeting);

        $webexRestMeetingTelephonyProfile = new WebexRestMeetingTelephonyProfile();
        switch ($webexRestMeeting->getConvocation()->getTypeAudio()) {
            case AbstractProviderAudio::AUDIO_CALLIN:
                $webexRestMeetingTelephonyProfile
                    ->setWebexRestMeetingDefaultPhoneNumber($meeting['telephony']['callInNumbers'][0]['callInNumber'])
                    ->setCodeAnimator($meeting['hostKey'])
                    ->setCodeParticipant($meeting['telephony']['accessCode'])
                    ->setPhoneNumber($meeting['telephony']['callInNumbers'][0]['callInNumber'])
                    ->setConnectionType(AbstractProviderAudio::AUDIO_CALLIN);
                break;
        }

        return $webexRestMeetingTelephonyProfile;
    }

    private function setOptionsMeeting(WebexRestMeeting $webexRestMeeting): array
    {
        /** @var WebexRestConfigurationHolder $configurationHolder */
        $configurationHolder = $webexRestMeeting->getAbstractProviderConfigurationHolder();
        $optionsConfigurationHolder = $configurationHolder->getDefaultOptions();
        $openTime = 0; // 0 par défaut, 15 pour meeting et 60 pour webinar
        $enabledJoinBeforeHost = false;

        if (array_key_exists('optionJoinTeleconfBeforeHost', $optionsConfigurationHolder)) {
            if ($optionsConfigurationHolder['optionJoinTeleconfBeforeHost']) {
                $enabledJoinBeforeHost = true;
                if ($webexRestMeeting->getMeetingRestScheduledType() == WebexRestMeeting::SCHEDULED_TYPE_WEBINAR) {
                    $openTime = 60;
                } else {
                    $openTime = WebexRestMeeting::DEFAULT_OPEN_TIME;
                }
            }
        }

        $options = [
            'enabledAutoRecordMeeting' => false,
            'allowAnyUserToBeCoHost' => false,
            'enabledJoinBeforeHost' => $enabledJoinBeforeHost,
            'enableConnectAudioBeforeHost' => false,
            'joinBeforeHostMinutes' => $openTime,
            'excludePassword' => false,
            'publicMeeting' => false,
            'reminderTime' => 0,
            //'unlockedMeetingJoinSecurity' => 'allowJoin',
            //'enableAutomaticLock' => false,
            //'automaticLockMinutes' => 0,
            'allowFirstUserToBeCoHost' => false,
            'allowAuthenticatedDevices' => false,
        ];

        return $options;
    }

    /**
     * https://developer.webex.com/docs/api/v1/meetings/create-a-meeting.
     *
     * @return mixed
     */
    private function createMeeting(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): WebexRestMeeting
    {
        $meetingOptions = $this->setOptionsMeeting($webexRestMeeting);
        $meeting = [
            'title' => $webexRestMeeting->getName(), //'Titre de la réunion',
            'start' => $webexRestMeeting->getDateStart()->setTimezone(new \DateTimeZone($webexRestMeeting->getClient()->getTimezone()))->format('Y-m-d H:i:s'),
            'end' => $webexRestMeeting->getDateEnd()->setTimezone(new \DateTimeZone($webexRestMeeting->getClient()->getTimezone()))->format('Y-m-d H:i:s'),
            'timezone' => $configurationHolder->getClient()->getTimezone(),
            'hostEmail' => $webexRestMeeting->getMeetingRestHostEmail(),
            'scheduledType' => $webexRestMeeting->getMeetingRestScheduledType(),
            'sendEmail' => false,
            'siteUrl' => $configurationHolder->getSiteName().'.webex.com',
        ];
        $meetingData = array_merge($meetingOptions, $meeting);
        $invitees = [];

        foreach ($webexRestMeeting->getParticipantRoles() as $ppsr) {
            $invitees[] = [
                'email' => $ppsr->getEmail(),
                'displayName' => $ppsr->getFirstName().' '.$ppsr->getLastName(),
                'coHost' => false,
                'panelist' => false,
            ];
        }
        if (count($invitees) > 0) {
            $meetingData['invitees'] = $invitees;
        }

        //POST https://webexapis.com/v1/meetings
        $url = 'https://webexapis.com/v1/meetings';

        $headers = [
            'Authorization: Bearer '.$configurationHolder->getClientAccessToken(),
            'Content-Type: application/json',
        ];
        /*var_dump($meetingData);
        var_dump($url);
        var_dump($headers);*/
        $response = $this->sendCurlRequest($url, $headers, 'POST', $meetingData);

        if (isset($response['errors'])) {
            $errorMessage = $response['errors'][0]['description'] ?? 'Une erreur inconnue s\'est produite';
            throw new \Exception('Erreur Webex API : '.$errorMessage);
        }

        $webexRestMeeting
            ->setMeetingRestId($response['id'])
            ->setMeetingRestNumber($response['meetingNumber'])
            ->setMeetingRestWebLink($response['webLink'])
            ->setMeetingRestPassword($response['password'])
            ->setMeetingRestHostEmail($response['hostEmail'])
            ->setLicence($response['hostEmail']);

        return $webexRestMeeting;
    }

    /**
     * https://developer.webex.com/docs/api/v1/meetings/update-a-meeting.
     *
     * @return mixed
     */
    private function updateMeeting(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): WebexRestMeeting
    {
        $meetingOptions = $this->setOptionsMeeting($webexRestMeeting);
        //$meetingRemote = $this->getMeetingById($configurationHolder, $webexRestMeeting);
        $meeting = [
            'title' => $webexRestMeeting->getName(),
            'start' => $webexRestMeeting->getDateStart()->setTimezone(new \DateTimeZone($webexRestMeeting->getClient()->getTimezone()))->format('Y-m-d H:i:s'),
            'end' => $webexRestMeeting->getDateEnd()->setTimezone(new \DateTimeZone($webexRestMeeting->getClient()->getTimezone()))->format('Y-m-d H:i:s'),
            'timezone' => $configurationHolder->getClient()->getTimezone(),
            'hostEmail' => $webexRestMeeting->getMeetingRestHostEmail(),
            //'siteUrl' => $meetingRemote['siteUrl']
            'sendEmail' => false,
            'siteUrl' => $configurationHolder->getSiteName().'.webex.com',
        ];
        $meetingData = array_merge($meetingOptions, $meeting);

        foreach ($webexRestMeeting->getParticipantRoles() as $ppsr) {
            $invitees[] = [
                'email' => $ppsr->getEmail(),
                'displayName' => $ppsr->getFirstName().' '.$ppsr->getLastName(),
                'coHost' => false,
                'panelist' => false,
            ];
        }
        if (count($invitees) > 0) {
            $meetingData['invitees'] = $invitees;
        }

        //PUT https://webexapis.com/v1/meetings/1d824a4a205042eba9574e00b711b226_20200127T120000Z
        $url = 'https://webexapis.com/v1/meetings/'.$webexRestMeeting->getMeetingRestId();

        $headers = [
            'Authorization: Bearer '.$configurationHolder->getClientAccessToken(),
            'Content-Type: application/json',
        ];

        $response = $this->sendCurlRequest($url, $headers, 'PUT', $meetingData);
        if (isset($response['errors'])) {
            $errorMessage = $response['errors'][0] ?? 'Une erreur inconnue s\'est produite';
            throw new \Exception('Erreur Webex API : '.$errorMessage);
        }

        return $webexRestMeeting;
    }

    /**
     * https://developer.webex.com/docs/integrations#using-the-refresh-token.
     *
     * @throws \Exception
     */
    private function updateAccessToken(WebexRestConfigurationHolder $configurationHolder): mixed
    {
        $url = 'https://webexapis.com/v1/access_token';
        $headers = [
            'Content-Type: application/json',
        ];
        $data = [
            'grant_type' => 'refresh_token',
            'client_id' => $configurationHolder->getClientId(),
            'client_secret' => $configurationHolder->getClientSecret(),
            'refresh_token' => $configurationHolder->getClientRefreshToken(),
        ];

        $responseData = $this->sendCurlRequest($url, $headers, 'POST', $data);

        if (isset($responseData['error'])) {
            $errorMessage = $responseData['error_description'] ?? 'Une erreur inconnue s\'est produite';
            throw new \Exception('Erreur lors de la régénération des tokens : '.$errorMessage);
        }

        return [
            'access_token' => $responseData['access_token'],
            'refresh_token' => $responseData['refresh_token'],
            'expires_in' => $responseData['expires_in'],
        ];
    }

    private function deleteMeeting(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): bool
    {
        //DELETE https://webexapis.com/v1/meetings/{meetingId}{?hostEmail,sendEmail}
        $url = 'https://webexapis.com/v1/meetings/'.$webexRestMeeting->getMeetingRestId().'?hostEmail='.$webexRestMeeting->getMeetingRestHostEmail().'&sendEmail=false';

        $headers = [
            'Authorization: Bearer '.$configurationHolder->getClientAccessToken(),
        ];

        $response = $this->sendCurlRequest($url, $headers, 'DELETE');

        return true;
    }

    /*private function joinAMeeting(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //POST https://webexapis.com/v1/meetings/join

        return [];
    }

    private function getListMeetingTemplates(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/templates{?templateType,locale,isDefault,isStandard,hostEmail,siteUrl}

        return [];
    }

    private function getMeetingTemplate(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/templates/{templateId}{?hostEmail}

        return [];
    }

    private function getMeetingControlStatus(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/controls{?meetingId}

        return [];
    }

    private function listMeetingSessionTypes(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/sessionTypes{?hostEmail,siteUrl}

        return [];
    }

    private function getMeetingSessionType(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/sessionTypes/{sessionTypeId}{?hostEmail,siteUrl}

        return [];
    }

    private function getRegistrationFormForAMeeting(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/{meetingId}/registration

        return [];
    }

    private function updateMeetingRegistrationForm(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //PUT https://webexapis.com/v1/meetings/{meetingId}/registration

        return [];
    }

    private function deleteMeetingRegistrationForm(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //DELETE https://webexapis.com/v1/meetings/{meetingId}/registration

        return [];
    }

    private function registerAMeetingRegistrants(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //POST https://webexapis.com/v1/meetings/{meetingId}/registrants

        return [];
    }

    private function batchRegisterMeetingRegistrants(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //POST https://webexapis.com/v1/meetings/{meetingId}/registrants/bulkInsert

        return [];
    }

    private function getAMeetingRegistrantsDetailInformation(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/{meetingId}/registrants/{registrantId}

        return [];
    }

    private function getListMeetingRegistrants(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/{meetingId}/registrants{?max,email,registerTimeFrom,registerTimeTo}

        return [];
    }

    private function queryMeetingRegistrants(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //POST https://webexapis.com/v1/meetings/{meetingId}/registrants/query{?max}

        return [];
    }

    private function batchUpdateMeetingRegistrantsStatus(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //POST https://webexapis.com/v1/meetings/{meetingId}/registrants/{statusOpType}

        return [];
    }

    private function deleteAMeetingRegistrant(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //DELETE https://webexapis.com/v1/meetings/{meetingId}/registrants/{statusOpType}

        return [];
    }

    private function updateMeetingSimultaneousInterpretation(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //PUT https://webexapis.com/v1/meetings/{meetingId}/simultaneousInterpretation

        return [];
    }

    private function createAMeetingInterpreter(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //POST https://webexapis.com/v1/meetings/{meetingId}/interpreters

        return [];
    }

    private function getAMeetingInterpreter(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/{meetingId}/interpreters/{interpreterId}

        return [];
    }

    private function listMeetingInterpreters(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/{meetingId}/interpreters{?hostEmail}

        return [];
    }
    private function updateAMeetingInterpreter(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //PUT https://webexapis.com/v1/meetings/{meetingId}/interpreters/{interpreterId}

        return [];
    }

    private function deleteAMeetingInterpreter(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //DELETE https://webexapis.com/v1/meetings/{meetingId}/interpreters/{interpreterId}

        return [];
    }

    private function updateMeetingBreakoutSessions(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //PUT https://webexapis.com/v1/meetings/{meetingId}/breakoutSessions

        return [];
    }

    private function listMeetingBreakoutSessions(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/{meetingId}/breakoutSessions

        return [];
    }

    private function deleteMeetingBreakoutSessions(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //DELETE https://webexapis.com/v1/meetings/{meetingId}/breakoutSessions{?sendEmail}

        return [];
    }

    private function getAMeetingSurvey(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/{meetingId}/survey

        return [];
    }

    private function listMeetingSurveyResults(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/{meetingId}/surveyResults

        return [];
    }

    private function createInvitationSources(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //POST https://webexapis.com/v1/meetings/{meetingId}/invitationSources

        return [];
    }

    private function listInvitationSources(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/{meetingId}/invitationSources

        return [];
    }

    private function listMeetingTrackingCodes(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetings/trackingCodes{?siteUrl,service,hostEmail}

        return [];
    }

    private function reassignMeetingsToANewHost(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //POST https://webexapis.com/v1/meetings/reassignHost

        return [];
    }*/

    private function listMeetingInvitees(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetingInvitees{?meetingId,max,hostEmail,panelist}
        $url = 'https://webexapis.com/v1/meetingInvitees?meetingId='.$webexRestMeeting->getMeetingRestId().'&hostEmail='.$webexRestMeeting->getMeetingRestHostEmail();

        $headers = [
            'Authorization: Bearer '.$configurationHolder->getClientAccessToken(),
        ];

        $response = $this->sendCurlRequest($url, $headers, 'GET');

        return $response;
    }

    private function createAMeetingInvitee(WebexRestConfigurationHolder $configurationHolder, array $parameters): bool
    {
        $webexRestMeeting = $parameters['webexRestMeeting']; //$participantSessionRole->getSession();
        /* @var WebexRestMeeting $webexRestMeeting */
        $meetingRestId = $webexRestMeeting->getMeetingRestId();
        $meetingRestHostEmail = $webexRestMeeting->getMeetingRestHostEmail();

        /* @var WebexRestParticipant $participant */
        $participant = $parameters['participantSessionRole']->getParticipant();

        $inviteeData = [
            'email' => $participant->getEmail(),
            'displayName' => $participant->getFirstName().' '.$participant->getLastName(),
            'meetingId' => $meetingRestId,
            'coHost' => false,
            'hostEmail' => $meetingRestHostEmail,
            'panelist' => false,
            'sendEmail' => false,
        ];

        //POST https://webexapis.com/v1/meetingInvitees
        $url = 'https://webexapis.com/v1/meetingInvitees';

        $headers = [
            'Authorization: Bearer '.$configurationHolder->getClientAccessToken(),
            'Content-Type: application/json',
        ];

        $response = $this->sendCurlRequest($url, $headers, 'POST', $inviteeData);

        if (isset($response['errors'])) {
            $errorMessage = $response['errors'][0]['description'] ?? 'Une erreur inconnue s\'est produite';
            throw new \Exception('Erreur Webex API : '.$errorMessage);
        }

        return true;
    }

    private function createMeetingInvitees(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //POST https://webexapis.com/v1/meetingInvitees/bulkInsert

        return [];
    }

    private function getAMeetingInvitee(WebexRestConfigurationHolder $configurationHolder, ProviderParticipant $providerParticipant): mixed
    {
        //GET https://webexapis.com/v1/meetingInvitees/{meetingInviteeId}{?hostEmail}
        /*$url = 'https://webexapis.com/v1/meetingInvitees/' . $webexRestMeeting->getMeetingRestId().'?hostEmail='.$webexRestMeeting->getMeetingRestHostEmail();

        $headers = [
            'Authorization: Bearer ' . $configurationHolder->getClientAccessToken(),
        ];

        $response = $this->sendCurlRequest($url, $headers, 'GET');

        return $response;*/

        return [];
    }

    private function updateAMeetingInvitee(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //PUT https://webexapis.com/v1/meetingInvitees/{meetingInviteeId}

        return [];
    }

    private function deleteAMeetingInvitee(WebexRestConfigurationHolder $configurationHolder, array $parameters): bool
    {
        $meetingRestHostEmail = $parameters['webexRestMeeting']->getMeetingRestHostEmail();

        /* @var WebexRestParticipant $participant */
        $participant = $parameters['participantSessionRole']->getParticipant();

        $meetingRemote = $this->listMeetingInvitees($configurationHolder, $parameters['webexRestMeeting']);

        $inviteeId = null;
        foreach ($meetingRemote['items'] as $invitee) {
            if ($invitee['email'] == $participant->getEmail()) {
                $inviteeId = $invitee['id'];
            }
        }

        if (is_null($inviteeId)) {
            return false;
        }

        //DELETE https://webexapis.com/v1/meetingInvitees/{meetingInviteeId}{?hostEmail,sendEmail}
        $url = 'https://webexapis.com/v1/meetingInvitees/'.$inviteeId.'?hostEmail='.$meetingRestHostEmail.'&sendEmail=false';

        $headers = [
            'Authorization: Bearer '.$configurationHolder->getClientAccessToken(),
            'Content-Type: application/json',
        ];

        $response = $this->sendCurlRequest($url, $headers, 'DELETE');

        if (isset($response['errors'])) {
            $errorMessage = $response['errors'][0] ?? 'Une erreur inconnue s\'est produite';
            throw new \Exception('Erreur Webex API : '.$errorMessage);
        }

        return true;
    }

    private function getJoinUrlMeeting(WebexRestConfigurationHolder $configurationHolder, ProviderParticipantSessionRole $participantSessionRole): array|string|null
    {
        /** @var WebexRestMeeting $webexRestMeeting */
        $webexRestMeeting = $participantSessionRole->getSession();
        $meetingRestId = $webexRestMeeting->getMeetingRestId();

        /* @var WebexRestParticipant $participant */
        $participant = $participantSessionRole->getParticipant();

        $inviteeData = [
            'email' => $participant->getEmail(),
            'displayName' => $participant->getFirstName().' '.$participant->getLastName(),
            'meetingId' => $meetingRestId,
            'joinDirectly' => false,
            'expirationMinutes' => 60,
            'password' => $webexRestMeeting->getMeetingRestPassword(),
        ];

        //POST https://webexapis.com/v1/meetings/join
        $url = 'https://webexapis.com/v1/meetings/join';

        $headers = [
            'Authorization: Bearer '.$configurationHolder->getClientAccessToken(),
            'Content-Type: application/json',
        ];

        $response = $this->sendCurlRequest($url, $headers, 'POST', $inviteeData);

        if (isset($response['errors'])) {
            $errorMessage = $response['errors'][0]['description'] ?? 'Une erreur inconnue s\'est produite';
            throw new \Exception('Erreur Webex API : '.$errorMessage);
        }

        return $response['joinLink'];
    }

    private function getMeetingsBySeriesId(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        $urlParams = [
            'hostEmail' => $webexRestMeeting->getMeetingRestHostEmail(),
            'meetingType' => 'meeting',
            'meetingSeriesId' => $webexRestMeeting->getMeetingRestId(),
            'form' => $webexRestMeeting->getDateStart()->setTimezone(new \DateTimeZone($webexRestMeeting->getClient()->getTimezone()))->modify('-3 hours')->format('Y-m-d\TH:i:s\Z'),
            'to' => $webexRestMeeting->getDateEnd()->setTimezone(new \DateTimeZone($webexRestMeeting->getClient()->getTimezone()))->modify('+3 hours')->format('Y-m-d\TH:i:s\Z'),
        ];

        $url = 'https://webexapis.com/v1/meetings?'.http_build_query($urlParams);

        $headers = [
            'Authorization: Bearer '.$configurationHolder->getClientAccessToken(),
            'Content-Type: application/json',
        ];

        $response = $this->sendCurlRequest($url, $headers, 'GET');

        if (isset($response['errors'])) {
            $errorMessage = $response['errors'][0] ?? 'Une erreur inconnue s\'est produite';
            throw new \Exception('Erreur Webex API : '.$errorMessage);
        }

        // { "items": [meeting1, meeting2, ...] }
        return $response['items'] ?? [];
    }

    private function listMeetingParticipants(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): array
    {
        $items = $this->getMeetingsBySeriesId($configurationHolder, $webexRestMeeting);

        $createdAttendees = [];
        foreach ($items as $item => $meeting) {
            $urlParams = [
                'hostEmail' => $webexRestMeeting->getMeetingRestHostEmail(),
                'meetingId' => $meeting['id'],
            ];
            $url = 'https://webexapis.com/v1/meetingParticipants?'.http_build_query($urlParams);

            $headers = [
                'Authorization: Bearer '.$configurationHolder->getClientAccessToken(),
                'Content-Type: application/json',
            ];

            $response = $this->sendCurlRequest($url, $headers, 'GET');

            if (isset($response['errors'])) {
                $errorMessage = $response['errors'][0] ?? 'Une erreur inconnue s\'est produite';
                throw new \Exception('Erreur Webex API : '.$errorMessage);
            }

            if (count($response) != 0) {
                foreach ($response['items'] as $participation) {
                    $identifier = $participation['email'];
                    foreach ($participation['devices'] as $device) {
                        if (array_key_exists($identifier, $createdAttendees)) {
                            $createdAttendees[$identifier] = [
                                'email' => $identifier,
                                'presenceDateStart' => min(\DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s\Z', $device['joinedTime']), $createdAttendees[$identifier]['presenceDateStart']),
                                'presenceDateEnd' => max(\DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s\Z', $device['leftTime']), $createdAttendees[$identifier]['presenceDateEnd']),
                                'duration' => $createdAttendees[$identifier]['duration'] + (int) $device['durationSecond'],
                            ];
                        } else {
                            $createdAttendees[$identifier] = [
                                'email' => $identifier,
                                'presenceDateStart' => \DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s\Z', $device['joinedTime']),
                                'presenceDateEnd' => \DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s\Z', $device['leftTime']),
                                'duration' => (int) $device['durationSecond'],
                            ];
                        }
                    }
                }
            }
        }

        return $createdAttendees;
    }

    private function listMeetingAttendees(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): array
    {
        $dataStartBegin = $webexRestMeeting->getDateStart()->setTimezone(new \DateTimeZone($webexRestMeeting->getClient()->getTimezone()))->modify('-3 hours')->format('Y-m-d\TH:i:s\Z');
        $dataStartEnd = $webexRestMeeting->getDateEnd()->setTimezone(new \DateTimeZone($webexRestMeeting->getClient()->getTimezone()))->modify('+3 hours')->format('Y-m-d\TH:i:s\Z');
        $createdAttendees = [];
        $urlParams = [
            'siteUrl' => $configurationHolder->getSiteName().'.webex.com',
            'meetingId' => $webexRestMeeting->getMeetingRestId(),
            'form' => $dataStartBegin,
            'to' => $dataStartEnd,
            'max' => 1000,
        ];
        $url = 'https://webexapis.com/v1/meetingReports/attendees?'.http_build_query($urlParams);

        $headers = [
            'Authorization: Bearer '.$configurationHolder->getClientAccessToken(),
            'Content-Type: application/json',
        ];

        $response = $this->sendCurlRequest($url, $headers, 'GET');
        if (isset($response['errors'])) {
            $errorMessage = $response['errors'][0] ?? 'Une erreur inconnue s\'est produite';
            throw new \Exception('Erreur Webex API : '.$errorMessage);
        }
        if (count($response) != 0) {
            foreach ($response['items'] as $participation) {
                $identifier = $participation['email'];
                if (array_key_exists($identifier, $createdAttendees)) {
                    $createdAttendees[$identifier] = [
                        'email' => $identifier,
                        'presenceDateStart' => min(\DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s\Z', $participation['joinedTime']), $createdAttendees[$identifier]['presenceDateStart']),
                        'presenceDateEnd' => max(\DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s\Z', $participation['leftTime']), $createdAttendees[$identifier]['presenceDateEnd']),
                        'duration' => $createdAttendees[$identifier]['duration'] + (int) $participation['duration'],
                    ];
                } else {
                    $createdAttendees[$identifier] = [
                        'email' => $identifier,
                        'presenceDateStart' => \DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s\Z', $participation['joinedTime']),
                        'presenceDateEnd' => \DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s\Z', $participation['leftTime']),
                        'duration' => (int) $participation['duration'],
                    ];
                }
            }
        }

        return $createdAttendees;
    }

    /*private function queryMeetingParticipantsWithEmail(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //POST https://webexapis.com/v1/meetingParticipants/query{?meetingId,meetingStartTimeFrom,meetingStartTimeTo,hostEmail,joinTimeFrom,joinTimeTo}

        return [];
    }

    private function getMeetingParticipantDetails(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //GET https://webexapis.com/v1/meetingParticipants/{participantId}{?hostEmail}

        return [];
    }

    private function updateAParticipant(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //PUT https://webexapis.com/v1/meetingParticipants/{participantId}

        return [];
    }

    private function admitParticipants(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $webexRestMeeting): mixed
    {
        //POST https://webexapis.com/v1/meetingParticipants/admit
    }*/

    /*
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!createmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws UnexpectedProviderActionErrorException
     */
    /*private function createMeeting(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): WebexMeeting
    {
        $diff = $meeting->getDateEnd()?->diff($meeting->getDateStart());

        $xml = $this->twig->render('connectors/cisco/webex_meeting_set_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_CREATE_MEETING,
            'meeting' => $meeting,
            'meetingDuration' => $diff === null ?: $diff->i + $diff->h * 60,
        ]);

        $response = $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
            'body' => $xml,
        ]);

        $this->providerLogger->info('Call WebexMeetingConnector createMeeting', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'content' => $xml,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_CREATE_MEETING);

        $key = $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:meetingkey')->text();
        $meeting->setMeetingKey((int) $key);
        $password = $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:meetingPassword')->text();
        $meeting->setMeetingPassword($password);

        $meeting->setHostICalendarUrl(
            $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:host')->text()
        );

        $meeting->setAttendeeICalendarUrl(
            $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:attendee')->text()
        );

        return $meeting;
    }

    private function telephonyProfileInfo(WebexConfigurationHolder $configurationHolder, WebexMeeting $providerSession): WebexMeetingTelephonyProfile
    {
        $crawler = $this->getMeeting($configurationHolder, $providerSession);

        $webexMeetingTelephonyProfile = new WebexMeetingTelephonyProfile();
        switch ($providerSession->getConvocation()->getTypeAudio()) {
            case AbstractProviderAudio::AUDIO_CALLIN:
                $phoneNumber = $crawler->matches('serv|message serv|body serv|bodyContent meet|telephony meet|callInNum serv|tollNum')
                    ? $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:telephony/meet:callInNum/serv:tollNum')->text()
                    : null;
                $animatorCode = $crawler->matches('serv|message serv|body serv|bodyContent meet|hostKey')
                    ? $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:hostKey')->text()
                    : null;
                $participantCode = $crawler->matches('serv|message serv|body serv|bodyContent meet|meetingkey')
                    ? $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:meetingkey')->text()
                    : null;

                $webexMeetingTelephonyProfile
                    ->setWebexMeetingDefaultPhoneNumber($phoneNumber)
                    ->setCodeAnimator($animatorCode)
                    ->setCodeParticipant($participantCode)
                    ->setPhoneNumber($phoneNumber)
                    ->setConnectionType(AbstractProviderAudio::AUDIO_CALLIN);
                break;
        }

        return $webexMeetingTelephonyProfile;
    }*/

    /*
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!setmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws UnexpectedProviderActionErrorException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    /*private function setMeeting(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): WebexMeeting
    {
        $diff = $meeting->getDateEnd()?->diff($meeting->getDateStart());

        $xml = $this->twig->render('connectors/cisco/webex_meeting_set_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_SET_MEETING,
            'meeting' => $meeting,
            'meetingDuration' => $diff === null ?: $diff->i + $diff->h * 60,
        ]);

        $response = $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
            'body' => $xml,
        ]);
        $this->providerLogger->info('Call WebexMeetingConnector setMeeting', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'content' => $xml,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_SET_MEETING);

        $meeting->setHostICalendarUrl(
            $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:host')->text()
        );

        $meeting->setAttendeeICalendarUrl(
            $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:attendee')->text()
        );

        return $meeting;
    }*/

    /*
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!gethosturlmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws UnexpectedProviderActionErrorException
     */
    /*private function getHostUrlMeeting(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): string
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_get_host_url.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_GET_HOST_URL_MEETING,
            'meeting' => $meeting,
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_GET_HOST_URL_MEETING
        );

        return $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:hostMeetingURL')->text();
    }*/

    /*
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!getjoinurlmeeting/getjoinurlmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    /*private function getJoinUrlMeeting(WebexConfigurationHolder $configurationHolder, ProviderParticipantSessionRole $participantSessionRole): string
    {
        /** @var WebexMeeting $session
        $session = $participantSessionRole->getSession();

        $xml = $this->twig->render('connectors/cisco/webex_meeting_get_join_url_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_GET_JOIN_URL_MEETING,
            'meetingKey' => $session->getMeetingKey(),
            'fullName' => $participantSessionRole->getParticipant()->getFullName(),
            'email' => $participantSessionRole->getEmail(),
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_GET_JOIN_URL_MEETING
        );

        return $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:joinMeetingURL')->text();
    }*/

    /*
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!getjoinurlmeeting/getjoinurlmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    /*private function getConnectorJoinUrlMeeting(WebexConfigurationHolder $configurationHolder, int $meetingKey): string
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_get_join_url_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_GET_JOIN_URL_MEETING,
            'meetingKey' => $meetingKey,
            'fullName' => $configurationHolder->getUsername(),
            'email' => $configurationHolder->getUsername(),
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_GET_JOIN_URL_MEETING
        );

        return $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:joinMeetingURL')->text();
    }*/

    /*
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!delmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws UnexpectedProviderActionErrorException
     */
    /*private function deleteMeeting(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): Crawler
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_delete_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_DELETE_MEETING,
            'meeting' => $meeting,
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_DELETE_MEETING
        );

        return $crawler;
    }

    private function getMeetingLink(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): WebexMeeting
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_get_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_GET_MEETING,
            'meeting' => $meeting,
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_GET_MEETING_LINK
        );

        $meetingLink = $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:meetingLink')->text();
        $meeting->setMeetingLink($meetingLink);

        return $meeting;
    }*/

    /*
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!delmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws UnexpectedProviderActionErrorException
     */
    /*private function getMeeting(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): Crawler
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_get_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_GET_MEETING,
            'meeting' => $meeting,
        ]);

        $response = $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
            'body' => $xml,
        ]);
        $this->providerLogger->info('Call WebexMeetingConnector getMeeting', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'content' => $xml,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);
        $crawler = $this->handleResponse($response, self::ACTION_GET_MEETING);

        return $crawler;
    }*/

    /*
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!lstmeetingattendee
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    /*private function listMeetingAttendee(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): array
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_list_meeting_attendee.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_LIST_MEETING_ATTENDEE,
            'meeting' => $meeting,
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_LIST_MEETING_ATTENDEE
        );

        $createdAttendees = [];
        $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/att:attendee')->each(function (Crawler $node) use (&$createdAttendees) {
            $contactIdentifier = (int) $node->filterXPath('//att:contactID')->text();

            $createdAttendees[$contactIdentifier] = [
                'email' => $contactIdentifier,
            ];
        });

        return $createdAttendees;
    }*/

    /*
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!lstmeetingattendeehistory
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    /*private function listMeetingAttendeeHistory(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): array
    {
        // FIXME: Create the following file. It's found currently
        $xml = $this->twig->render('connectors/cisco/webex_meeting_list_meeting_attendee_history.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_LIST_MEETING_ATTENDEE_HISTORY,
            'meeting' => $meeting,
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_LIST_MEETING_ATTENDEE_HISTORY
        );

        $createdAttendees = [];
        $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/history:meetingAttendeeHistory')->each(function (Crawler $node) use (&$createdAttendees) {
            if ($node->filterXPath('//history:email')->getNode(0) != null
                && $node->filterXPath('//history:joinTime')->getNode(0) != null
                && $node->filterXPath('//history:leaveTime')->getNode(0) != null
                && $node->filterXPath('//history:duration')->getNode(0) != null
            ) {
                $identifier = $node->filterXPath('//history:email')->text();
                if (array_key_exists($identifier, $createdAttendees)) {
                    $createdAttendees[$identifier] = [
                        'email' => $identifier,
                        'presenceDateStart' => min(\DateTimeImmutable::createFromFormat('m/d/Y H:i:s', $node->filterXPath('//history:joinTime')->text()), $createdAttendees[$identifier]['presenceDateStart']),
                        'presenceDateEnd' => max(\DateTimeImmutable::createFromFormat('m/d/Y H:i:s', $node->filterXPath('//history:leaveTime')->text()), $createdAttendees[$identifier]['presenceDateEnd']),
                        'duration' => $createdAttendees[$identifier]['duration'] + (int) $node->filterXPath('//history:duration')->text(),
                    ];
                } else {
                    $createdAttendees[$identifier] = [
                        'email' => $identifier,
                        'presenceDateStart' => \DateTimeImmutable::createFromFormat('m/d/Y H:i:s', $node->filterXPath('//history:joinTime')->text()),
                        'presenceDateEnd' => \DateTimeImmutable::createFromFormat('m/d/Y H:i:s', $node->filterXPath('//history:leaveTime')->text()),
                        'duration' => (int) $node->filterXPath('//history:duration')->text(),
                    ];
                }
            }
        });

        return $createdAttendees;
    }*/

    /*
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!createmeetingattendee
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    /*private function createMeetingAttendee(WebexConfigurationHolder $configurationHolder, ProviderParticipantSessionRole $participantSessionRole): bool
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_create_meeting_attendee.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_CREATE_MEETING_ATTENDEE,
            'participantSessionRole' => $participantSessionRole,
        ]);

        $response = $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
            'body' => $xml,
        ]);
        $this->providerLogger->info('Call WebexMeetingConnector createMeetingAttendee', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'content' => $xml,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);

        $this->handleResponse($response, self::ACTION_CREATE_MEETING_ATTENDEE);

        return true;
    }*/

    /*
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!delmeetingattendee/delmeetingattendee
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    /*private function deleteMeetingAttendee(WebexConfigurationHolder $configurationHolder, ProviderParticipantSessionRole $participantSessionRole): bool
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_delete_meeting_attendee.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_DELETE_MEETING_ATTENDEE,
            'participantSessionRole' => $participantSessionRole,
        ]);

        $response = $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
            'body' => $xml,
        ]);
        $this->providerLogger->info('Call WebexMeetingConnector deleteMeetingAttendee', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'content' => $xml,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);

        $this->handleResponse($response, self::ACTION_DELETE_MEETING_ATTENDEE);

        return true;
    }*/

    /*
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!getuser
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws UnexpectedProviderActionErrorException
     */
    /*private function getUser(WebexConfigurationHolder $configurationHolder, WebexParticipant $participant): WebexParticipant
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_get_user.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_GET_USER,
            'participant' => $participant,
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_GET_USER
        );

        $user = $participant->getUser();

        return $participant;
    }

    private function loginUser(WebexConfigurationHolder $configurationHolder): bool
    {
        $xml = $this->twig->render('connectors/cisco/webex_base.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_USER_LOGIN_TICKET,
        ]);

        $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_USER_LOGIN_TICKET
        );

        return true;
    }*/
}
