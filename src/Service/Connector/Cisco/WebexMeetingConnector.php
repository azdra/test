<?php

namespace App\Service\Connector\Cisco;

use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexMeetingTelephonyProfile;
use App\Entity\Cisco\WebexParticipant;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Exception\AbstractProviderException;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\ProviderGenericException;
use App\Exception\UnexpectedProviderActionErrorException;
use App\Model\ProviderResponse;
use App\Repository\WebexParticipantRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class WebexMeetingConnector extends AbstractWebexConnector
{
    public const ACTION_CREATE_MEETING = 'meeting.CreateMeeting';
    public const ACTION_TELEPHONY_PROFILE_INFO = 'meeting.GetMeeting';
    public const ACTION_SET_MEETING = 'meeting.SetMeeting';
    public const ACTION_GET_MEETING = 'meeting.GetMeeting';
    public const ACTION_DELETE_MEETING = 'meeting.DelMeeting';
    public const ACTION_GET_HOST_URL_MEETING = 'meeting.GethosturlMeeting';
    public const ACTION_GET_JOIN_URL_MEETING = 'meeting.GetjoinurlMeeting';
    public const ACTION_GET_CONNECTOR_JOIN_URL_MEETING = 'meeting.GetHostjoinurlMeeting';
    public const ACTION_LIST_MEETING_ATTENDEE = 'attendee.LstMeetingAttendee';
    public const ACTION_LIST_MEETING_ATTENDEE_HISTORY = 'history.LstmeetingattendeeHistory';
    public const ACTION_CREATE_MEETING_ATTENDEE = 'attendee.CreateMeetingAttendee';
    public const ACTION_DELETE_MEETING_ATTENDEE = 'attendee.DelMeetingAttendee';
    public const ACTION_GET_USER = 'user.GetUser';
    public const ACTION_USER_LOGIN_TICKET = 'user.GetLoginTicket';
    public const ACTION_GET_MEETING_LINK = 'meeting.GetMeetingLink';

    /**
     * WebexMeetingConnector constructor.
     */
    public function __construct(HttpClientInterface $httpClient, private Environment $twig, private WebexParticipantRepository $webexParticipantRepository, protected LoggerInterface $logger, private LoggerInterface $providerLogger)
    {
        parent::__construct($httpClient);
    }

    /**
     * @throws InvalidProviderConfigurationHandler
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    public function call(AbstractProviderConfigurationHolder $configurationHolder, string $action, mixed $parameters = null): ProviderResponse
    {
        if (!($configurationHolder instanceof WebexConfigurationHolder)) {
            throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
        }

        if (!$configurationHolder->isValid()) {
            throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
        }

        $connectorResponse = new ProviderResponse();
        try {
            $connectorResponse->setData(match ($action) {
                self::ACTION_CREATE_MEETING => $this->createMeeting($configurationHolder, $parameters),
                self::ACTION_TELEPHONY_PROFILE_INFO => $this->telephonyProfileInfo($configurationHolder, $parameters),
                self::ACTION_SET_MEETING => $this->setMeeting($configurationHolder, $parameters),
                self::ACTION_GET_MEETING => $this->getMeeting($configurationHolder, $parameters),
                self::ACTION_DELETE_MEETING => $this->deleteMeeting($configurationHolder, $parameters),
                self::ACTION_GET_HOST_URL_MEETING => $this->getHostUrlMeeting($configurationHolder, $parameters),
                self::ACTION_GET_JOIN_URL_MEETING => $this->getJoinUrlMeeting($configurationHolder, $parameters),
                self::ACTION_GET_CONNECTOR_JOIN_URL_MEETING => $this->getConnectorJoinUrlMeeting($configurationHolder, $parameters),
                self::ACTION_LIST_MEETING_ATTENDEE => $this->listMeetingAttendee($configurationHolder, $parameters),
                self::ACTION_LIST_MEETING_ATTENDEE_HISTORY => $this->listMeetingAttendeeHistory($configurationHolder, $parameters),
                self::ACTION_CREATE_MEETING_ATTENDEE => $this->createMeetingAttendee($configurationHolder, $parameters),
                self::ACTION_DELETE_MEETING_ATTENDEE => $this->deleteMeetingAttendee($configurationHolder, $parameters),
                self::ACTION_GET_USER => $this->getUser($configurationHolder, $parameters),
                self::ACTION_USER_LOGIN_TICKET => $this->loginUser($configurationHolder),
                self::ACTION_GET_MEETING_LINK => $this->getMeetingLink($configurationHolder, $parameters),
            });
        } catch (AbstractProviderException $exception) {
            $this->logger->error($exception);
            $connectorResponse->setData($exception->getMessage());
            $connectorResponse->setSuccess(false);
            $connectorResponse->setThrown($exception);
        }

        return $connectorResponse;
    }

    /**
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!createmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws UnexpectedProviderActionErrorException
     */
    private function createMeeting(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): WebexMeeting
    {
        $diff = $meeting->getDateEnd()?->diff($meeting->getDateStart());

        $xml = $this->twig->render('connectors/cisco/webex_meeting_set_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_CREATE_MEETING,
            'meeting' => $meeting,
            'meetingDuration' => $diff === null ?: $diff->i + $diff->h * 60,
        ]);

        $response = $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
            'body' => $xml,
        ]);

        $this->providerLogger->info('Call WebexMeetingConnector createMeeting', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'content' => $xml,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_CREATE_MEETING);

        $key = $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:meetingkey')->text();
        $meeting->setMeetingKey((int) $key);
        $password = $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:meetingPassword')->text();
        $meeting->setMeetingPassword($password);

        $meeting->setHostICalendarUrl(
            $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:host')->text()
        );

        $meeting->setAttendeeICalendarUrl(
            $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:attendee')->text()
        );

        return $meeting;
    }

    private function telephonyProfileInfo(WebexConfigurationHolder $configurationHolder, WebexMeeting $providerSession): WebexMeetingTelephonyProfile
    {
        $crawler = $this->getMeeting($configurationHolder, $providerSession);

        $webexMeetingTelephonyProfile = new WebexMeetingTelephonyProfile();
        switch ($providerSession->getConvocation()->getTypeAudio()) {
            case AbstractProviderAudio::AUDIO_CALLIN:
                $phoneNumber = $crawler->matches('serv|message serv|body serv|bodyContent meet|telephony meet|callInNum serv|tollNum')
                    ? $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:telephony/meet:callInNum/serv:tollNum')->text()
                    : null;
                $animatorCode = $crawler->matches('serv|message serv|body serv|bodyContent meet|hostKey')
                    ? $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:hostKey')->text()
                    : null;
                $participantCode = $crawler->matches('serv|message serv|body serv|bodyContent meet|meetingkey')
                    ? $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:meetingkey')->text()
                    : null;

                $webexMeetingTelephonyProfile
                    ->setWebexMeetingDefaultPhoneNumber($phoneNumber)
                    ->setCodeAnimator($animatorCode)
                    ->setCodeParticipant($participantCode)
                    ->setPhoneNumber($phoneNumber)
                    ->setConnectionType(AbstractProviderAudio::AUDIO_CALLIN);
                break;
        }

        return $webexMeetingTelephonyProfile;
    }

    /**
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!setmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws UnexpectedProviderActionErrorException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    private function setMeeting(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): WebexMeeting
    {
        $diff = $meeting->getDateEnd()?->diff($meeting->getDateStart());

        $xml = $this->twig->render('connectors/cisco/webex_meeting_set_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_SET_MEETING,
            'meeting' => $meeting,
            'meetingDuration' => $diff === null ?: $diff->i + $diff->h * 60,
        ]);

        $response = $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
            'body' => $xml,
        ]);
        $this->providerLogger->info('Call WebexMeetingConnector setMeeting', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'content' => $xml,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_SET_MEETING);

        $meeting->setHostICalendarUrl(
            $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:host')->text()
        );

        $meeting->setAttendeeICalendarUrl(
            $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:attendee')->text()
        );

        return $meeting;
    }

    /**
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!gethosturlmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws UnexpectedProviderActionErrorException
     */
    private function getHostUrlMeeting(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): string
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_get_host_url.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_GET_HOST_URL_MEETING,
            'meeting' => $meeting,
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_GET_HOST_URL_MEETING
        );

        return $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:hostMeetingURL')->text();
    }

    /**
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!getjoinurlmeeting/getjoinurlmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    private function getJoinUrlMeeting(WebexConfigurationHolder $configurationHolder, ProviderParticipantSessionRole $participantSessionRole): string
    {
        /** @var WebexMeeting $session */
        $session = $participantSessionRole->getSession();

        $xml = $this->twig->render('connectors/cisco/webex_meeting_get_join_url_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_GET_JOIN_URL_MEETING,
            'meetingKey' => $session->getMeetingKey(),
            'fullName' => $participantSessionRole->getParticipant()->getFullName(),
            'email' => $participantSessionRole->getEmail(),
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_GET_JOIN_URL_MEETING
        );

        return $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:joinMeetingURL')->text();
    }

    /**
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!getjoinurlmeeting/getjoinurlmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    private function getConnectorJoinUrlMeeting(WebexConfigurationHolder $configurationHolder, int $meetingKey): string
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_get_join_url_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_GET_JOIN_URL_MEETING,
            'meetingKey' => $meetingKey,
            'fullName' => $configurationHolder->getUsername(),
            'email' => $configurationHolder->getUsername(),
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_GET_JOIN_URL_MEETING
        );

        return $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:joinMeetingURL')->text();
    }

    /**
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!delmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws UnexpectedProviderActionErrorException
     */
    private function deleteMeeting(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): Crawler
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_delete_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_DELETE_MEETING,
            'meeting' => $meeting,
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_DELETE_MEETING
        );

        return $crawler;
    }

    private function getMeetingLink(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): WebexMeeting
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_get_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_GET_MEETING,
            'meeting' => $meeting,
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_GET_MEETING_LINK
        );

        $meetingLink = $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/meet:meetingLink')->text();
        $meeting->setMeetingLink($meetingLink);

        return $meeting;
    }

    /**
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!delmeeting
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws UnexpectedProviderActionErrorException
     */
    private function getMeeting(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): Crawler
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_get_meeting.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_GET_MEETING,
            'meeting' => $meeting,
        ]);

        $response = $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
            'body' => $xml,
        ]);
        $this->providerLogger->info('Call WebexMeetingConnector getMeeting', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'content' => $xml,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);
        $crawler = $this->handleResponse($response, self::ACTION_GET_MEETING);

        return $crawler;
    }

    /**
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!lstmeetingattendee
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    private function listMeetingAttendee(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): array
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_list_meeting_attendee.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_LIST_MEETING_ATTENDEE,
            'meeting' => $meeting,
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_LIST_MEETING_ATTENDEE
        );

        $createdAttendees = [];
        $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/att:attendee')->each(function (Crawler $node) use (&$createdAttendees) {
            $contactIdentifier = (int) $node->filterXPath('//att:contactID')->text();

            $createdAttendees[$contactIdentifier] = [
                'email' => $contactIdentifier,
            ];
        });

        return $createdAttendees;
    }

    /**
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!lstmeetingattendeehistory
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    private function listMeetingAttendeeHistory(WebexConfigurationHolder $configurationHolder, WebexMeeting $meeting): array
    {
        // FIXME: Create the following file. It's found currently
        $xml = $this->twig->render('connectors/cisco/webex_meeting_list_meeting_attendee_history.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_LIST_MEETING_ATTENDEE_HISTORY,
            'meeting' => $meeting,
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_LIST_MEETING_ATTENDEE_HISTORY
        );

        $createdAttendees = [];
        $crawler->filterXPath('//serv:message/serv:body/serv:bodyContent/history:meetingAttendeeHistory')->each(function (Crawler $node) use (&$createdAttendees) {
            if ($node->filterXPath('//history:email')->getNode(0) != null
                && $node->filterXPath('//history:joinTime')->getNode(0) != null
                && $node->filterXPath('//history:leaveTime')->getNode(0) != null
                && $node->filterXPath('//history:duration')->getNode(0) != null
            ) {
                $identifier = $node->filterXPath('//history:email')->text();
                if (array_key_exists($identifier, $createdAttendees)) {
                    $createdAttendees[$identifier] = [
                        'email' => $identifier,
                        'presenceDateStart' => min(\DateTimeImmutable::createFromFormat('m/d/Y H:i:s', $node->filterXPath('//history:joinTime')->text()), $createdAttendees[$identifier]['presenceDateStart']),
                        'presenceDateEnd' => max(\DateTimeImmutable::createFromFormat('m/d/Y H:i:s', $node->filterXPath('//history:leaveTime')->text()), $createdAttendees[$identifier]['presenceDateEnd']),
                        'duration' => $createdAttendees[$identifier]['duration'] + (int) $node->filterXPath('//history:duration')->text(),
                    ];
                } else {
                    $createdAttendees[$identifier] = [
                        'email' => $identifier,
                        'presenceDateStart' => \DateTimeImmutable::createFromFormat('m/d/Y H:i:s', $node->filterXPath('//history:joinTime')->text()),
                        'presenceDateEnd' => \DateTimeImmutable::createFromFormat('m/d/Y H:i:s', $node->filterXPath('//history:leaveTime')->text()),
                        'duration' => (int) $node->filterXPath('//history:duration')->text(),
                    ];
                }
            }
        });

        return $createdAttendees;
    }

    /**
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!createmeetingattendee
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    private function createMeetingAttendee(WebexConfigurationHolder $configurationHolder, ProviderParticipantSessionRole $participantSessionRole): bool
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_create_meeting_attendee.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_CREATE_MEETING_ATTENDEE,
            'participantSessionRole' => $participantSessionRole,
        ]);

        $response = $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
            'body' => $xml,
        ]);
        $this->providerLogger->info('Call WebexMeetingConnector createMeetingAttendee', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'content' => $xml,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);

        $this->handleResponse($response, self::ACTION_CREATE_MEETING_ATTENDEE);

        return true;
    }

    /**
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!delmeetingattendee/delmeetingattendee
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    private function deleteMeetingAttendee(WebexConfigurationHolder $configurationHolder, ProviderParticipantSessionRole $participantSessionRole): bool
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_delete_meeting_attendee.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_DELETE_MEETING_ATTENDEE,
            'participantSessionRole' => $participantSessionRole,
        ]);

        $response = $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
            'body' => $xml,
        ]);
        $this->providerLogger->info('Call WebexMeetingConnector deleteMeetingAttendee', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'content' => $xml,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);

        $this->handleResponse($response, self::ACTION_DELETE_MEETING_ATTENDEE);

        return true;
    }

    /**
     * @doc https://developer.cisco.com/docs/webex-xml-api-reference-guide/#!getuser
     *
     * @throws LoaderError
     * @throws ProviderGenericException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws UnexpectedProviderActionErrorException
     */
    private function getUser(WebexConfigurationHolder $configurationHolder, WebexParticipant $participant): WebexParticipant
    {
        $xml = $this->twig->render('connectors/cisco/webex_meeting_get_user.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_GET_USER,
            'participant' => $participant,
        ]);

        $crawler = $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_GET_USER
        );

        $user = $participant->getUser();

        return $participant;
    }

    private function loginUser(WebexConfigurationHolder $configurationHolder): bool
    {
        $xml = $this->twig->render('connectors/cisco/webex_base.xml.twig', [
            'configurationHolder' => $configurationHolder,
            'webexAction' => self::ACTION_USER_LOGIN_TICKET,
        ]);

        $this->handleResponse(
            $this->httpClient->request('POST', $configurationHolder->getApiUrl(), [
                'body' => $xml,
            ]),
            self::ACTION_USER_LOGIN_TICKET
        );

        return true;
    }
}
