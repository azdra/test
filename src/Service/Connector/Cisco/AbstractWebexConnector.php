<?php

namespace App\Service\Connector\Cisco;

use App\Exception\ProviderGenericException;
use App\Service\Connector\AbstractConnector;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Contracts\HttpClient\ResponseInterface;

abstract class AbstractWebexConnector extends AbstractConnector
{
    protected function handleResponse(ResponseInterface $response, $action): Crawler
    {
        $crawler = new Crawler($response->getContent());
        $code = $crawler->filterXPath('//serv:message/serv:header/serv:response/serv:result')->text();

        if ($code !== 'SUCCESS') {
            $reason =
                [
                    'serv:exceptionID' => (!empty($crawler->filterXPath('//serv:message/serv:header/serv:response/serv:exceptionID')->text())) ? $crawler->filterXPath('//serv:exceptionID')->text() : '',
                    'serv:reason' => (!empty($crawler->filterXPath('//serv:message/serv:header/serv:response/serv:reason')->text())) ? $crawler->filterXPath('//serv:message/serv:header/serv:response/serv:reason')->text() : '',
                ];

            throw new ProviderGenericException(self::class, $reason['serv:reason'], $reason['serv:exceptionID']);
        }

        return $crawler;
    }
}
