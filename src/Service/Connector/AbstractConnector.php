<?php

namespace App\Service\Connector;

use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

abstract class AbstractConnector implements ConnectorInterface
{
    private LoggerInterface $logger;

    public function __construct(protected HttpClientInterface $httpClient)
    {
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    abstract protected function handleResponse(ResponseInterface $response, $action): mixed;
}
