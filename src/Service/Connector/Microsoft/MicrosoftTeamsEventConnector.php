<?php

namespace App\Service\Connector\Microsoft;

use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\Microsoft\MicrosoftTeamsEventTelephonyProfile;
use App\Exception\AbstractProviderException;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\ProviderGenericException;
use App\Model\ProviderResponse;
use App\Serializer\MicrosoftTeamsEventSessionAPINormalizer;
use App\Service\Connector\AbstractConnector;
use Psr\Log\LoggerInterface;
use Redis;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class MicrosoftTeamsEventConnector extends AbstractConnector
{
    public const ACTION_GET_TOKEN = 'getToken';
    public const ACTION_SESSION_CREATE = 'event.create';
    public const ACTION_SESSION_UPDATE = 'event.update';
    public const ACTION_SESSION_GET_ONLINE_MEETING_FROM_JOINURL = 'event.getOnlineMeetingFromJoinUrl';
    public const ACTION_UPDATE_PARTICIPANTS = 'participants.updateAttendees';
    public const ACTION_SESSION_DELETE = 'session.delete';
    public const ACTION_CREATE_PARTICIPANT = 'attendee.CreateMeetingAttendee';
    public const ACTION_DELETE_PARTICIPANT = 'attendee.DelMeetingAttendee';
    public const ACTION_GET_USER = 'getUser';
    public const ACTION_GET_MEETING = 'getMeeting';
    public const ACTION_GET_MEETING_ATTENDANCE_REPORTS = 'getMeetingAttendanceReports';
    public const ACTION_GET_MEETING_ATTENDANCE_REPORT_ID = 'getMeetingAttendanceReportId';
    public const ACTION_CHECK_ORGANIZERS = 'checkUsersIsOrganizer';
    public const ACTION_TELEPHONY_PROFILE_INFO = 'telephonyProfileInfo';

    protected const REDIS_AUTH_TOKEN_PREFIX = 'microsoft_teams_token_';
    protected const REDIS_AUTH_TOKEN_TIMEOUT = 3600;

    protected Redis $redisClient;

    protected MicrosoftTeamsEventSessionAPINormalizer $microsoftTeamsEventSessionAPINormalizer;

    public function __construct(HttpClientInterface $httpClient, Redis $redisClient, MicrosoftTeamsEventSessionAPINormalizer $microsoftTeamsEventSessionAPINormalizer, protected LoggerInterface $logger, private LoggerInterface $providerLogger)
    {
        parent::__construct($httpClient);
        $this->redisClient = $redisClient;
        $this->microsoftTeamsEventSessionAPINormalizer = $microsoftTeamsEventSessionAPINormalizer;
    }

    protected function handleResponse(ResponseInterface $response, $action): mixed
    {
        $statusCode = $response->getStatusCode();
        if ($statusCode < 200 || $statusCode >= 300) {
            throw new ProviderGenericException(self::class, sprintf('Status code: %d.', $statusCode));
        }

        return json_decode($response->getContent(), true);
    }

    public function call(AbstractProviderConfigurationHolder $configurationHolder, string $action, mixed $parameters = null): ProviderResponse
    {
        if (!($configurationHolder instanceof MicrosoftTeamsEventConfigurationHolder)) {
            $exception = new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
            $this->logger->error($exception);
            throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
        }

        if (!$configurationHolder->isValid()) {
            $exception = new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
            $this->logger->error($exception);
            throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
        }

        $connectorResponse = new ProviderResponse();
        try {
            $connectorResponse->setData(match ($action) {
                self::ACTION_GET_TOKEN => $this->login($configurationHolder),
                self::ACTION_SESSION_CREATE => $this->createSession($configurationHolder, $parameters),
                self::ACTION_SESSION_GET_ONLINE_MEETING_FROM_JOINURL => $this->getOnlineMeetingFromJoinUrl($configurationHolder, $parameters),
                self::ACTION_SESSION_UPDATE => $this->updateSession($configurationHolder, $parameters),
                self::ACTION_UPDATE_PARTICIPANTS => $this->updateAttendees($configurationHolder, $parameters),
                self::ACTION_SESSION_DELETE => $this->deleteSession($configurationHolder, $parameters),
                self::ACTION_GET_USER => $this->getUser($configurationHolder, $parameters),
                self::ACTION_GET_MEETING => $this->getMeeting($configurationHolder, $parameters),
                self::ACTION_GET_MEETING_ATTENDANCE_REPORTS => $this->getMeetingAttendanceReports($configurationHolder, $parameters),
                self::ACTION_GET_MEETING_ATTENDANCE_REPORT_ID => $this->getMeetingAttendanceReportId($configurationHolder, $parameters),
                self::ACTION_CHECK_ORGANIZERS => $this->checkUsersIsOrganizers($configurationHolder),
                self::ACTION_TELEPHONY_PROFILE_INFO => $this->telephonyProfileInfo($configurationHolder, $parameters),
            });
        } catch (AbstractProviderException $exception) {
            $this->logger->error($exception);
            $connectorResponse->setSuccess(false);
            $connectorResponse->setThrown($exception);
        }

        return $connectorResponse;
    }

    private function login(MicrosoftTeamsEventConfigurationHolder $configurationHolder): ?string
    {
        /*if ($this->redisClient->exists(self::REDIS_AUTH_TOKEN_PREFIX.'default')) {
            return (string) $this->redisClient->get(self::REDIS_AUTH_TOKEN_PREFIX.'default');
        }*/
        $response = $this->handleResponse(
            $this->httpClient->request(
                'POST',
                'https://login.microsoftonline.com/'.$configurationHolder->getTenantId().'/oauth2/v2.0/token',
                [
                    'body' => [
                        'client_id' => $configurationHolder->getClientId(),
                        'client_secret' => $configurationHolder->getClientSecret(),
                        'scope' => $configurationHolder->getScope(),
                        'grant_type' => $configurationHolder->getGrantType(),
                    ],
                ]
            ),
            self::ACTION_GET_TOKEN
        );

        $this->redisClient->set(self::REDIS_AUTH_TOKEN_PREFIX.'default', $token = $response['access_token'], self::REDIS_AUTH_TOKEN_TIMEOUT);

        return $token;
    }

    private function createSession(MicrosoftTeamsEventConfigurationHolder $configurationHolder, MicrosoftTeamsEventSession $microsoftTeamsEventSession): MicrosoftTeamsEventSession
    {
        $user = $this->getUser($configurationHolder, $microsoftTeamsEventSession->getLicence());
        $query = [
            'headers' => [
                'Authorization' => $this->getAuthorizationBearer($configurationHolder),
            ],
            'json' => $this->microsoftTeamsEventSessionAPINormalizer->normalizeEvent($microsoftTeamsEventSession, context: ['groups' => 'microsoft_teams_event_session', 'include_participants' => false]),
        ];
        $response = $this->httpClient->request(
            'POST',
            $configurationHolder->getApiUrl().'/users/'.$user['id'].'/calendar/events',
            $query
        );
        $this->providerLogger->info('Call MicrosoftTeamsEventConnector createSession', ['apiUrl' => $configurationHolder->getApiUrl().'/users/'.$user['id'].'/calendar/events', 'query' => $query, 'response' => $response]);
        $response = $this->handleResponse($response, self::ACTION_SESSION_CREATE);

        $microsoftTeamsEventSession->setEventIdentifier($response['id']);
        $microsoftTeamsEventSession->setJoinUrl($response['onlineMeeting']['joinUrl']);
        $this->getOnlineMeetingFromJoinUrl($configurationHolder, $microsoftTeamsEventSession);
        //$this->updateAttendees($configurationHolder, $microsoftTeamsEventSession);

        return $microsoftTeamsEventSession;
    }

    private function getOnlineMeetingFromJoinUrl(MicrosoftTeamsEventConfigurationHolder $configurationHolder, MicrosoftTeamsEventSession $microsoftTeamsEventSession): array
    {
        $user = $this->getUser($configurationHolder, $microsoftTeamsEventSession->getLicence());

        $response = $this->httpClient->request(
            'GET',
            $configurationHolder->getApiUrl().'/users/'.$user['id'].'/onlineMeetings?$filter=JoinWebUrl%20eq%20\''.$microsoftTeamsEventSession->getJoinUrl().'\'',
            [
                'headers' => [
                    'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                ],
            ]
        );

        $meeting = $this->handleResponse(
            $response,
            self::ACTION_GET_MEETING
        );
        if (array_key_exists('value', $meeting) && !empty($meeting['value'])) {
            $microsoftTeamsEventSession->setOnlineMeetingIdentifier($meeting['value'][0]['id']);
        }

        return $meeting;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ProviderGenericException
     */
    private function updateSession(MicrosoftTeamsEventConfigurationHolder $configurationHolder, MicrosoftTeamsEventSession $microsoftTeamsEventSession): MicrosoftTeamsEventSession
    {
        $user = $this->getUser($configurationHolder, $microsoftTeamsEventSession->getLicence());

        $responseEvent = $this->handleResponse(
            $this->httpClient->request(
                'PATCH',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/calendar/events/'.$microsoftTeamsEventSession->getEventIdentifier(),
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                    'json' => $this->microsoftTeamsEventSessionAPINormalizer->normalizeEvent($microsoftTeamsEventSession, format: 'json', context: ['groups' => 'microsoft_teams_event_session', 'include_participants' => false]),
                ]
            ),
            self::ACTION_SESSION_UPDATE
        );

        $this->updateAttendees($configurationHolder, $microsoftTeamsEventSession);

        //TODO Faut-il faire un update sur onlineMeeting ?

        //$microsoftTeamsEventSession->setEventIdentifier($response['id']);
        //$microsoftTeamsEventSession->setJoinUrl($response['onlineMeeting']['joinUrl']);

        return $microsoftTeamsEventSession;
    }

    private function updateAttendees(MicrosoftTeamsEventConfigurationHolder $configurationHolder, MicrosoftTeamsEventSession $microsoftTeamsEventSession): MicrosoftTeamsEventSession
    {
        $user = $this->getUser($configurationHolder, $microsoftTeamsEventSession->getLicence());

        $participants = $this->httpClient->request(
            'PATCH',
            $configurationHolder->getApiUrl().'/users/'.$user['id'].'/onlineMeetings/'.$microsoftTeamsEventSession->getOnlineMeetingIdentifier(),
            [
                'headers' => [
                    'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                ],
                'json' => $this->microsoftTeamsEventSessionAPINormalizer->normalizeOnlineMeeting($microsoftTeamsEventSession, format: 'json', context: ['groups' => 'microsoft_teams_event_session', 'include_participants' => ($microsoftTeamsEventSession->getTraineesCount() + $microsoftTeamsEventSession->getAnimatorsCount() > 1), 'licence' => $microsoftTeamsEventSession->getLicence()]),
            ]
        );
        $response = $this->handleResponse(
            $participants,
            self::ACTION_UPDATE_PARTICIPANTS
        );

        return $microsoftTeamsEventSession;
    }

    private function deleteSession(MicrosoftTeamsEventConfigurationHolder $configurationHolder, MicrosoftTeamsEventSession $microsoftTeamsEventSession): MicrosoftTeamsEventSession
    {
        $user = $this->getUser($configurationHolder, $microsoftTeamsEventSession->getLicence());

        $response = $this->handleResponse(
            $this->httpClient->request(
                'DELETE',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/calendar/events/'.$microsoftTeamsEventSession->getEventIdentifier(),
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                ]
            ),
            self::ACTION_SESSION_DELETE
        );

        return $microsoftTeamsEventSession;
    }

    private function getAuthorizationBearer(MicrosoftTeamsEventConfigurationHolder $configurationHolder): string
    {
        return 'Bearer '.$this->login($configurationHolder);
    }

    private function getUser(MicrosoftTeamsEventConfigurationHolder $configurationHolder, string $emailUser): array
    {
        try {
            $user = $this->handleResponse(
                $this->httpClient->request(
                    'GET',
                    $configurationHolder->getApiUrl().'/users/'.$emailUser,
                    [
                        'headers' => [
                            'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                        ],
                    ]
                ),
                self::ACTION_GET_USER
            );
        } catch (\Exception|TransportExceptionInterface  $exception) {
            $user = [];
            $this->logger->error($exception, ['email' => $emailUser]);
        }

        return $user;
    }

    private function getMeeting(MicrosoftTeamsEventConfigurationHolder $configurationHolder, MicrosoftTeamsEventSession $microsoftTeamsEventSession): array
    {
        $user = $this->getUser($configurationHolder, $microsoftTeamsEventSession->getLicence());

        $meeting = $this->handleResponse(
            $this->httpClient->request(
                'GET',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/onlineMeetings/'.$microsoftTeamsEventSession->getOnlineMeetingIdentifier(),
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                ]
            ),
            self::ACTION_GET_MEETING
        );

        return $meeting;
    }

    private function getMeetingAttendanceReports(MicrosoftTeamsEventConfigurationHolder $configurationHolder, MicrosoftTeamsEventSession $microsoftTeamsEventSession): array
    {
        $user = $this->getUser($configurationHolder, $microsoftTeamsEventSession->getLicence());

        $reports = $this->handleResponse(
            $this->httpClient->request(
                'GET',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/onlineMeetings/'.$microsoftTeamsEventSession->getOnlineMeetingIdentifier().'/attendanceReports',
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                ]
            ),
            self::ACTION_GET_MEETING_ATTENDANCE_REPORTS
        );

        return $reports;
    }

    private function getMeetingAttendanceReportId(MicrosoftTeamsEventConfigurationHolder $configurationHolder, array $parameters): array
    {
        /** @var MicrosoftTeamsEventSession $microsoftTeamsEventSession */
        $microsoftTeamsEventSession = $parameters['providerSession'];
        $reportId = $parameters['reportId'];

        $user = $this->getUser($configurationHolder, $microsoftTeamsEventSession->getLicence());
        $report = $this->handleResponse(
            $this->httpClient->request(
                'GET',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/onlineMeetings/'.$microsoftTeamsEventSession->getOnlineMeetingIdentifier().'/attendanceReports/'.$reportId.'?$expand=attendanceRecords',
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                ]
            ),
            self::ACTION_GET_MEETING_ATTENDANCE_REPORT_ID
        );

        return $report;
    }

    private function telephonyProfileInfo(MicrosoftTeamsEventConfigurationHolder $configurationHolder, MicrosoftTeamsEventSession $microsoftTeamsEventSession): MicrosoftTeamsEventTelephonyProfile
    {
        $response = $this->getMeeting($configurationHolder, $microsoftTeamsEventSession);

        $microsoftTeamsEventTelephonyProfile = new MicrosoftTeamsEventTelephonyProfile();
        switch ($microsoftTeamsEventSession->getConvocation()->getTypeAudio()) {
            case AbstractProviderAudio::AUDIO_CALLIN:
                $audioConferencing = [
                    'tollNumber' => null,
                    'tollFreeNumber' => null,
                    'conferenceId' => null,
                    'dialinUrl' => null,
                ];
                if (!empty($response['audioConferencing']) && !is_null($response['audioConferencing'])) {
                    $audioConferencing['tollNumber'] = (empty($response['audioConferencing']['tollNumber'])) ? null : $response['audioConferencing']['tollNumber'];
                    $audioConferencing['tollFreeNumber'] = (empty($response['audioConferencing']['tollFreeNumber'])) ? null : $response['audioConferencing']['tollFreeNumber'];
                    $audioConferencing['conferenceId'] = (empty($response['audioConferencing']['conferenceId'])) ? null : $response['audioConferencing']['conferenceId'];
                    $audioConferencing['dialinUrl'] = (empty($response['audioConferencing']['dialinUrl'])) ? null : $response['audioConferencing']['dialinUrl'];
                }
                $phoneNumber = $audioConferencing['tollNumber'];
                $animatorCode = $audioConferencing['conferenceId'];
                $participantCode = $audioConferencing['conferenceId'];
                $microsoftTeamsEventTelephonyProfile
                    ->setMicrosoftTeamsEventDefaultPhoneNumber($phoneNumber)
                    ->setAudioConferencing($audioConferencing)
                    ->setCodeAnimator($animatorCode)
                    ->setCodeParticipant($participantCode)
                    ->setPhoneNumber($phoneNumber)
                    ->setConnectionType(AbstractProviderAudio::AUDIO_CALLIN);
                break;
        }

        return $microsoftTeamsEventTelephonyProfile;
    }

    private function checkUsersIsOrganizers(MicrosoftTeamsEventConfigurationHolder $configurationHolder): mixed
    {
        $usersOrganisers = $configurationHolder->getLicences();
        $participants = $configurationHolder->getClient()->getUsers();
        foreach ($participants as $user) {
            $userMsTeams = $this->getUser($configurationHolder, $user->getEmail());
            if (array_key_exists($user->getEmail(), $usersOrganisers)) {
                if (empty($userMsTeams)) {
                    unset($usersOrganisers[$user->getEmail()]);
                }
            } else {
                if (!empty($userMsTeams)) {
                    $usersOrganisers[$user->getEmail()] = [
                        'private' => true,
                        'shared' => false,
                        'attributed' => false,
                        'password' => '',
                        'licenceAttributed' => '',
                        'passwordAttributed' => '',
                        'user_id' => $user->getId(),
                    ];
                }
            }
        }

        $configurationHolder->setLicences($usersOrganisers);

        return $usersOrganisers;
    }
}
