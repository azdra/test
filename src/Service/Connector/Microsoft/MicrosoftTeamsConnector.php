<?php

namespace App\Service\Connector\Microsoft;

use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\Microsoft\MicrosoftTeamsTelephonyProfile;
use App\Exception\AbstractProviderException;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\ProviderGenericException;
use App\Model\ProviderResponse;
use App\Serializer\MicrosoftTeamsSessionAPINormalizer;
use App\Service\Connector\AbstractConnector;
use Psr\Log\LoggerInterface;
use Redis;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class MicrosoftTeamsConnector extends AbstractConnector
{
    public const ACTION_GET_TOKEN = 'getToken';
    public const ACTION_SESSION_CREATE = 'session.create';
    public const ACTION_SESSION_UPDATE = 'session.update';
    public const ACTION_SESSION_DELETE = 'session.delete';
    public const ACTION_CREATE_PARTICIPANT = 'attendee.CreateMeetingAttendee';
    public const ACTION_DELETE_PARTICIPANT = 'attendee.DelMeetingAttendee';
    public const ACTION_GET_USER = 'getUser';
    public const ACTION_GET_MEETING = 'getMeeting';
    public const ACTION_GET_MEETING_ATTENDANCE_REPORTS = 'getMeetingAttendanceReports';
    public const ACTION_GET_MEETING_ATTENDANCE_REPORT_ID = 'getMeetingAttendanceReportId';
    public const ACTION_CHECK_ORGANIZERS = 'checkUsersIsOrganizer';
    public const ACTION_TELEPHONY_PROFILE_INFO = 'telephonyProfileInfo';

    protected const REDIS_AUTH_TOKEN_PREFIX = 'microsoft_teams_token_';
    protected const REDIS_AUTH_TOKEN_TIMEOUT = 3600;

    protected Redis $redisClient;

    protected MicrosoftTeamsSessionAPINormalizer $microsoftTeamsSessionAPINormalizer;

    public function __construct(HttpClientInterface $httpClient, Redis $redisClient, MicrosoftTeamsSessionAPINormalizer $microsoftTeamsSessionAPINormalizer, protected LoggerInterface $logger, private LoggerInterface $providerLogger)
    {
        parent::__construct($httpClient);
        $this->redisClient = $redisClient;
        $this->microsoftTeamsSessionAPINormalizer = $microsoftTeamsSessionAPINormalizer;
    }

    protected function handleResponse(ResponseInterface $response, $action): mixed
    {
        $statusCode = $response->getStatusCode();
        if ($statusCode < 200 || $statusCode >= 300) {
            throw new ProviderGenericException(self::class, sprintf('Status code: %d.', $statusCode));
        }

        return json_decode($response->getContent(), true);
    }

    public function call(AbstractProviderConfigurationHolder $configurationHolder, string $action, mixed $parameters = null): ProviderResponse
    {
        if (!($configurationHolder instanceof MicrosoftTeamsConfigurationHolder)) {
            $exception = new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
            $this->logger->error($exception);
            throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
        }

        if (!$configurationHolder->isValid()) {
            $exception = new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
            $this->logger->error($exception);
            throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
        }

        $connectorResponse = new ProviderResponse();
        try {
            $connectorResponse->setData(match ($action) {
                self::ACTION_GET_TOKEN => $this->login($configurationHolder),
                self::ACTION_SESSION_CREATE => $this->createSession($configurationHolder, $parameters),
                self::ACTION_SESSION_UPDATE => $this->updateSession($configurationHolder, $parameters),
                self::ACTION_SESSION_DELETE => $this->deleteSession($configurationHolder, $parameters),
                self::ACTION_GET_USER => $this->getUser($configurationHolder, $parameters),
                self::ACTION_GET_MEETING => $this->getMeeting($configurationHolder, $parameters),
                self::ACTION_GET_MEETING_ATTENDANCE_REPORTS => $this->getMeetingAttendanceReports($configurationHolder, $parameters),
                self::ACTION_GET_MEETING_ATTENDANCE_REPORT_ID => $this->getMeetingAttendanceReportId($configurationHolder, $parameters),
                self::ACTION_CHECK_ORGANIZERS => $this->checkUsersIsOrganizers($configurationHolder),
                self::ACTION_TELEPHONY_PROFILE_INFO => $this->telephonyProfileInfo($configurationHolder, $parameters),
            });
        } catch (AbstractProviderException $exception) {
            $this->logger->error($exception);
            $connectorResponse->setSuccess(false);
            $connectorResponse->setThrown($exception);
        }

        return $connectorResponse;
    }

    private function login(MicrosoftTeamsConfigurationHolder $configurationHolder): ?string
    {
        /*if ($this->redisClient->exists(self::REDIS_AUTH_TOKEN_PREFIX.'default')) {
            return (string) $this->redisClient->get(self::REDIS_AUTH_TOKEN_PREFIX.'default');
        }*/
        $response = $this->handleResponse(
            $this->httpClient->request(
                'POST',
                'https://login.microsoftonline.com/'.$configurationHolder->getTenantId().'/oauth2/v2.0/token',
                [
                    'body' => [
                        'client_id' => $configurationHolder->getClientId(),
                        'client_secret' => $configurationHolder->getClientSecret(),
                        'scope' => $configurationHolder->getScope(),
                        'grant_type' => $configurationHolder->getGrantType(),
                    ],
                ]
            ),
            self::ACTION_GET_TOKEN
        );

        $this->redisClient->set(self::REDIS_AUTH_TOKEN_PREFIX.'default', $token = $response['access_token'], self::REDIS_AUTH_TOKEN_TIMEOUT);

        return $token;
    }

    private function createSession(MicrosoftTeamsConfigurationHolder $configurationHolder, MicrosoftTeamsSession $microsoftTeamsSession): MicrosoftTeamsSession
    {
        $user = $this->getUser($configurationHolder, $microsoftTeamsSession->getLicence());
        $query = [
            'headers' => [
                'Authorization' => $this->getAuthorizationBearer($configurationHolder),
            ],
            'json' => $this->microsoftTeamsSessionAPINormalizer->normalize($microsoftTeamsSession, context: ['groups' => 'microsoft_teams_session', 'include_participants' => true]),
        ];
        $response = $this->httpClient->request(
            'POST',
            $configurationHolder->getApiUrl().'/users/'.$user['id'].'/onlineMeetings',
            $query
        );
        $this->providerLogger->info('Call MicrosoftTeamsConnector createSession', ['apiUrl' => $configurationHolder->getApiUrl().'/users/'.$user['id'].'/onlineMeetings', 'query' => $query, 'response' => $response]);
        $response = $this->handleResponse($response, self::ACTION_SESSION_CREATE);

        $microsoftTeamsSession->setMeetingIdentifier($response['id']);
        $microsoftTeamsSession->setJoinWebUrl($response['joinWebUrl']);

        return $microsoftTeamsSession;
    }

    private function updateSession(MicrosoftTeamsConfigurationHolder $configurationHolder, MicrosoftTeamsSession $microsoftTeamsSession): MicrosoftTeamsSession
    {
        $user = $this->getUser($configurationHolder, $microsoftTeamsSession->getLicence());

        $response = $this->handleResponse(
            $this->httpClient->request(
                'PATCH',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/onlineMeetings/'.$microsoftTeamsSession->getMeetingIdentifier(),
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                    'json' => $this->microsoftTeamsSessionAPINormalizer->normalize($microsoftTeamsSession, format: 'json', context: ['groups' => 'microsoft_teams_session', 'include_participants' => true]),
                ]
            ),
            self::ACTION_SESSION_UPDATE
        );

        $microsoftTeamsSession->setMeetingIdentifier($response['id']);
        $microsoftTeamsSession->setJoinWebUrl($response['joinWebUrl']);

        return $microsoftTeamsSession;
    }

    private function deleteSession(MicrosoftTeamsConfigurationHolder $configurationHolder, MicrosoftTeamsSession $microsoftTeamsSession): MicrosoftTeamsSession
    {
        $user = $this->getUser($configurationHolder, $microsoftTeamsSession->getLicence());

        $response = $this->handleResponse(
            $this->httpClient->request(
                'DELETE',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/onlineMeetings/'.$microsoftTeamsSession->getMeetingIdentifier(),
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                ]
            ),
            self::ACTION_SESSION_DELETE
        );

        return $microsoftTeamsSession;
    }

    private function getAuthorizationBearer(MicrosoftTeamsConfigurationHolder $configurationHolder): string
    {
        return 'Bearer '.$this->login($configurationHolder);
    }

    private function getUser(MicrosoftTeamsConfigurationHolder $configurationHolder, string $emailUser): array
    {
        try {
            $user = $this->handleResponse(
                $this->httpClient->request(
                    'GET',
                    $configurationHolder->getApiUrl().'/users/'.$emailUser,
                    [
                        'headers' => [
                            'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                        ],
                    ]
                ),
                self::ACTION_GET_USER
            );
        } catch (\Exception  $exception) {
            $user = [];
            $this->logger->error($exception, ['email' => $emailUser]);
        }

        return $user;
    }

    private function getMeeting(MicrosoftTeamsConfigurationHolder $configurationHolder, MicrosoftTeamsSession $microsoftTeamsSession): array
    {
        $user = $this->getUser($configurationHolder, $microsoftTeamsSession->getLicence());

        $meeting = $this->handleResponse(
            $this->httpClient->request(
                'GET',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/onlineMeetings/'.$microsoftTeamsSession->getMeetingIdentifier(),
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                ]
            ),
            self::ACTION_GET_MEETING
        );

        return $meeting;
    }

    private function getMeetingAttendanceReports(MicrosoftTeamsConfigurationHolder $configurationHolder, MicrosoftTeamsSession $microsoftTeamsSession): array
    {
        $user = $this->getUser($configurationHolder, $microsoftTeamsSession->getLicence());

        $reports = $this->handleResponse(
            $this->httpClient->request(
                'GET',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/onlineMeetings/'.$microsoftTeamsSession->getMeetingIdentifier().'/attendanceReports',
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                ]
            ),
            self::ACTION_GET_MEETING_ATTENDANCE_REPORTS
        );

        return $reports;
    }

    private function getMeetingAttendanceReportId(MicrosoftTeamsConfigurationHolder $configurationHolder, array $parameters): array
    {
        /** @var MicrosoftTeamsSession $microsoftTeamsSession */
        $microsoftTeamsSession = $parameters['providerSession'];
        $reportId = $parameters['reportId'];

        $user = $this->getUser($configurationHolder, $microsoftTeamsSession->getLicence());
        $report = $this->handleResponse(
            $this->httpClient->request(
                'GET',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/onlineMeetings/'.$microsoftTeamsSession->getMeetingIdentifier().'/attendanceReports/'.$reportId.'?$expand=attendanceRecords',
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                ]
            ),
            self::ACTION_GET_MEETING_ATTENDANCE_REPORT_ID
        );

        return $report;
    }

    private function telephonyProfileInfo(MicrosoftTeamsConfigurationHolder $configurationHolder, MicrosoftTeamsSession $microsoftTeamsSession): MicrosoftTeamsTelephonyProfile
    {
        $response = $this->getMeeting($configurationHolder, $microsoftTeamsSession);

        $microsoftTeamsTelephonyProfile = new MicrosoftTeamsTelephonyProfile();
        switch ($microsoftTeamsSession->getConvocation()->getTypeAudio()) {
            case AbstractProviderAudio::AUDIO_CALLIN:
                $audioConferencing = [
                    'tollNumber' => null,
                    'tollFreeNumber' => null,
                    'conferenceId' => null,
                    'dialinUrl' => null,
                ];
                if (!empty($response['audioConferencing']) && !is_null($response['audioConferencing'])) {
                    $audioConferencing['tollNumber'] = (empty($response['audioConferencing']['tollNumber'])) ? null : $response['audioConferencing']['tollNumber'];
                    $audioConferencing['tollFreeNumber'] = (empty($response['audioConferencing']['tollFreeNumber'])) ? null : $response['audioConferencing']['tollFreeNumber'];
                    $audioConferencing['conferenceId'] = (empty($response['audioConferencing']['conferenceId'])) ? null : $response['audioConferencing']['conferenceId'];
                    $audioConferencing['dialinUrl'] = (empty($response['audioConferencing']['dialinUrl'])) ? null : $response['audioConferencing']['dialinUrl'];
                }
                $phoneNumber = $audioConferencing['tollNumber'];
                $animatorCode = $audioConferencing['conferenceId'];
                $participantCode = $audioConferencing['conferenceId'];
                $microsoftTeamsTelephonyProfile
                    ->setMicrosoftTeamsDefaultPhoneNumber($phoneNumber)
                    ->setAudioConferencing($audioConferencing)
                    ->setCodeAnimator($animatorCode)
                    ->setCodeParticipant($participantCode)
                    ->setPhoneNumber($phoneNumber)
                    ->setConnectionType(AbstractProviderAudio::AUDIO_CALLIN);
                break;
        }

        return $microsoftTeamsTelephonyProfile;
    }

    private function checkUsersIsOrganizers(MicrosoftTeamsConfigurationHolder $configurationHolder): mixed
    {
        $usersOrganisers = $configurationHolder->getLicences();
        $participants = $configurationHolder->getClient()->getUsers();
        foreach ($participants as $user) {
            $userMsTeams = $this->getUser($configurationHolder, $user->getEmail());
            if (array_key_exists($user->getEmail(), $usersOrganisers)) {
                if (empty($userMsTeams)) {
                    unset($usersOrganisers[$user->getEmail()]);
                }
            } else {
                if (!empty($userMsTeams)) {
                    $usersOrganisers[$user->getEmail()] = [
                        'private' => true,
                        'shared' => false,
                        'password' => '',
                        'user_id' => $user->getId(),
                    ];
                }
            }
        }

        $configurationHolder->setLicences($usersOrganisers);

        return $usersOrganisers;
    }
}
