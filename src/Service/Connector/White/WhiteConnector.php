<?php

namespace App\Service\Connector\White;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Entity\White\WhiteSession;
use App\Exception\AbstractProviderException;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\ProviderGenericException;
use App\Model\ProviderResponse;
use App\Serializer\WhiteSessionAPINormalizer;
use App\Service\Connector\AbstractConnector;
use Psr\Log\LoggerInterface;
use Redis;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class WhiteConnector extends AbstractConnector
{
    public const ACTION_GET_TOKEN = 'getToken';
    public const ACTION_SESSION_CREATE = 'session.create';
    public const ACTION_SESSION_UPDATE = 'session.update';
    public const ACTION_SESSION_DELETE = 'session.delete';
    //public const ACTION_CREATE_PARTICIPANT = 'attendee.CreateMeetingAttendee';
    //public const ACTION_DELETE_PARTICIPANT = 'attendee.DelMeetingAttendee';
    //public const ACTION_GET_USER = 'getUser';
    //public const ACTION_GET_MEETING = 'getMeeting';
    //public const ACTION_CHECK_ORGANIZERS = 'checkUsersIsOrganizer';

    protected const REDIS_AUTH_TOKEN_PREFIX = 'white_token_';
    protected const REDIS_AUTH_TOKEN_TIMEOUT = 3600;

    protected Redis $redisClient;

    protected WhiteSessionAPINormalizer $whiteSessionAPINormalizer;

    public function __construct(HttpClientInterface $httpClient, Redis $redisClient, WhiteSessionAPINormalizer $whiteSessionAPINormalizer, protected LoggerInterface $logger)
    {
        parent::__construct($httpClient);
        $this->redisClient = $redisClient;
        $this->whiteSessionAPINormalizer = $whiteSessionAPINormalizer;
    }

    protected function handleResponse(ResponseInterface $response, $action): mixed
    {
        $statusCode = $response->getStatusCode();
        if ($statusCode < 200 || $statusCode >= 300) {
            throw new ProviderGenericException(self::class, sprintf('Status code: %d.', $statusCode));
        }

        return json_decode($response->getContent(), true);
    }

    public function call(AbstractProviderConfigurationHolder $configurationHolder, string $action, mixed $parameters = null): ProviderResponse
    {
        if (!($configurationHolder instanceof WhiteConfigurationHolder)) {
            $exception = new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
            $this->logger->error($exception);
            throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
        }

        if (!$configurationHolder->isValid()) {
            $exception = new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
            $this->logger->error($exception);
            throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
        }

        $connectorResponse = new ProviderResponse();
        try {
            $connectorResponse->setData(match ($action) {
                self::ACTION_GET_TOKEN => $this->login($configurationHolder),
                self::ACTION_SESSION_CREATE => $this->createSession($configurationHolder, $parameters),
                self::ACTION_SESSION_UPDATE => $this->updateSession($configurationHolder, $parameters),
                self::ACTION_SESSION_DELETE => $this->deleteSession($configurationHolder, $parameters),
                //self::ACTION_GET_USER => $this->getUser($configurationHolder, $parameters),
                //self::ACTION_GET_MEETING => $this->getMeeting($configurationHolder, $parameters),
                //self::ACTION_CHECK_ORGANIZERS => $this->checkUsersIsOrganizers($configurationHolder),
            });
        } catch (AbstractProviderException $exception) {
            $this->logger->error($exception);
            $connectorResponse->setSuccess(false);
            $connectorResponse->setThrown($exception);
        }

        return $connectorResponse;
    }

    private function login(WhiteConfigurationHolder $configurationHolder): ?string
    {
        if ($this->redisClient->exists(self::REDIS_AUTH_TOKEN_PREFIX.'default')) {
            return (string) $this->redisClient->get(self::REDIS_AUTH_TOKEN_PREFIX.'default');
        }

        /*$response = $this->handleResponse(
            $this->httpClient->request(
                'POST',
                'https://login.whiteonline.com/'.$configurationHolder->getTenantId().'/oauth2/v2.0/token',
                [
                    'body' => [
                        'client_id' => $configurationHolder->getClientId(),
                        'client_secret' => $configurationHolder->getClientSecret(),
                        'scope' => $configurationHolder->getScope(),
                        'grant_type' => $configurationHolder->getGrantType(),
                    ],
                ]
            ),
            self::ACTION_GET_TOKEN
        );*/

        $lengthToken = 20;
        $response = [
            'access_token' => substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($lengthToken / strlen($x)))), 1, $lengthToken),
        ];
        $this->redisClient->set(self::REDIS_AUTH_TOKEN_PREFIX.'default', $token = $response['access_token'], self::REDIS_AUTH_TOKEN_TIMEOUT);

        return $token;
    }

    private function createSession(WhiteConfigurationHolder $configurationHolder, WhiteSession $whiteSession): WhiteSession
    {
        /*$user = $this->getUser($configurationHolder, $whiteSession->getLicence());

        $response = $this->handleResponse(
            $this->httpClient->request(
                'POST',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/onlineMeetings',
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                    'json' => $this->whiteSessionAPINormalizer->normalize($whiteSession, context: ['groups' => 'white_session', 'include_participants' => true]),
                ]
            ),
            self::ACTION_SESSION_CREATE
        );

        $whiteSession->setMeetingIdentifier($response['id']);
        $whiteSession->setJoinWebUrl($response['joinWebUrl']);*/

        return $whiteSession;
    }

    private function updateSession(WhiteConfigurationHolder $configurationHolder, WhiteSession $whiteSession): WhiteSession
    {
        /*$user = $this->getUser($configurationHolder, $whiteSession->getLicence());

        $response = $this->handleResponse(
            $this->httpClient->request(
                'PATCH',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/whiteConnect/'.$whiteSession->getMeetingIdentifier(),
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                    'json' => $this->whiteSessionAPINormalizer->normalize($whiteSession, format: 'json', context: ['groups' => 'white_session', 'include_participants' => true]),
                ]
            ),
            self::ACTION_SESSION_UPDATE
        );

        $whiteSession->setMeetingIdentifier($response['id']);
        $whiteSession->setJoinWebUrl($response['joinWebUrl']);*/

        return $whiteSession;
    }

    private function deleteSession(WhiteConfigurationHolder $configurationHolder, WhiteSession $whiteSession): WhiteSession
    {
        /*$user = $this->getUser($configurationHolder, $whiteSession->getLicence());

        $response = $this->handleResponse(
            $this->httpClient->request(
                'DELETE',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/whiteConnect/'.$whiteSession->getMeetingIdentifier(),
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                ]
            ),
            self::ACTION_SESSION_DELETE
        );*/

        return $whiteSession;
    }

    private function getAuthorizationBearer(WhiteConfigurationHolder $configurationHolder): string
    {
        return 'Bearer '.$this->login($configurationHolder);
    }

    private function getUser(WhiteConfigurationHolder $configurationHolder, string $emailUser): array
    {
        return [];
        /*try {
            $user = $this->handleResponse(
                $this->httpClient->request(
                    'GET',
                    $configurationHolder->getApiUrl().'/users/'.$emailUser,
                    [
                        'headers' => [
                            'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                        ],
                    ]
                ),
                self::ACTION_GET_USER
            );
        } catch (\Exception  $exception) {
            $user = [];
            $this->logger->error($exception, ['email' => $emailUser]);
        }

        return $user;*/
    }

    /*private function getMeeting(WhiteConfigurationHolder $configurationHolder, WhiteSession $whiteSession): array
    {
        $user = $this->getUser($configurationHolder, $whiteSession->getLicence());

        $meeting = $this->handleResponse(
            $this->httpClient->request(
                'GET',
                $configurationHolder->getApiUrl().'/users/'.$user['id'].'/whiteConnect/'.$whiteSession->getSessionIdentifier(),
                [
                    'headers' => [
                        'Authorization' => $this->getAuthorizationBearer($configurationHolder),
                    ],
                ]
            ),
            self::ACTION_GET_MEETING
        );

        return $meeting;
    }*/

    /*private function checkUsersIsOrganizers(WhiteConfigurationHolder $configurationHolder): mixed
    {
        $usersOrganisers = $configurationHolder->getLicences();
        $participants = $configurationHolder->getClient()->getUsers();
        foreach ($participants as $user) {
            $userWhite = $this->getUser($configurationHolder, $user->getEmail());
            if (array_key_exists($user->getEmail(), $usersOrganisers)) {
                if (empty($userWhite)) {
                    unset($usersOrganisers[$user->getEmail()]);
                }
            } else {
                if (!empty($userWhite)) {
                    $usersOrganisers[$user->getEmail()] = [
                        'private' => true,
                        'shared' => false,
                        'password' => '',
                        'user_id' => $user->getId(),
                    ];
                }
            }
        }

        $configurationHolder->setLicences($usersOrganisers);

        return $usersOrganisers;
    }*/
}
