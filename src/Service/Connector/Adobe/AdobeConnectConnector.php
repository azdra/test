<?php

namespace App\Service\Connector\Adobe;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectTelephonyProfile;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Exception\AbstractProviderException;
use App\Exception\InvalidArgumentException;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\ProviderGenericException;
use App\Exception\UnexpectedProviderActionErrorException;
use App\Model\Provider\AdobeConnect\AdobeConnectCommonInfo;
use App\Model\ProviderResponse;
use App\Repository\AdobeConnectMeetingAttendeeRepository;
use App\Repository\AdobeConnectPrincipalRepository;
use App\Repository\AdobeConnectSCORepository;
use App\Repository\AdobeConnectTelephonyProfileRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Service\Connector\AbstractConnector;
use App\Service\Factory\Adobe\AdobeConnectPrincipalFactory;
use DateTime;
use DateTimeImmutable;
use function in_array;
use Psr\Log\LoggerInterface;
use Redis;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class AdobeConnectConnector extends AbstractConnector
{
    public const ACTION_LOGIN = 'login';
    public const ACTION_COMMON_INFO = 'common-info';
    public const ACTION_AUTHENTICATE_PRINCIPAL = 'auth';
    public const ACTION_AUTHENTICATE_CONFIGURATION_HOLDER = 'auth-configuration-holder';
    public const ACTION_UPDATE_PRINCIPAL_PASSWORD = 'user-update-pwd';
    public const ACTION_SCO_CONTENTS = 'sco-contents';
    public const ACTION_SCO_SHORTCUTS = 'sco-shortcuts';
    public const ACTION_SCO_UPDATE = 'sco-update';
    public const ACTION_SCO_DELETE = 'sco-delete';
    public const ACTION_SCO_UPDATE_PERMISSION = 'sco-update-permission';
    public const ACTION_UPDATE_URL = 'update-sco-url';
    public const ACTION_PERMISSIONS_INFO = 'permissions-info';
    public const ACTION_PERMISSIONS_UPDATE = 'permissions-update';
    public const ACTION_PERMISSIONS_RESET = 'permissions-reset';
    public const ACTION_PRINCIPAL_UPDATE = 'principal-update';
    public const ACTION_PRINCIPAL_DELETE = 'principals-delete';
    public const ACTION_PRINCIPAL_LIST = 'principal-list';
    public const ACTION_TELEPHONY_PROFILE_LIST = 'telephony-profile-list';
    public const ACTION_TELEPHONY_PROFILE_INFO = 'telephony-profile-info';
    public const ACTION_REPORT_MEETING_ATTENDANCE = 'report-meeting-attendance';
    public const ACTION_SCO_ROOT_FOLDER = 'sco-root-folder';
    public const ACTION_SCO_NAME_LIST_FROM_PARENT_SCO = 'sco-content-names';
    public const ACTION_SCO_FOLDER_EXIST = 'sco-folder-exist';
    public const ACTION_SCO_CREATE_FOLDER = 'sco-create-folder';
    public const ACTION_SET_SCO_TELEPHONY_PROFILE = 'sco-set-telephony-profile';

    public const FUNCTION_GET_LS_TEMPLATE_FOLDER = 'find-ls-template-folder';
    public const FUNCTION_FIND_OR_CREATE_SCO = 'find-or-create-sco';
    public const LIVESESSION_DEFAULT_FOLDER = 'MyLiveSession';
    public const LIVESESSION_DEFAULT_TEMPLATE_FOLDER = 'Modeles';
    public const FUNCTION_GET_ALL_MODELS_TYPE_MEETING = 'find-all-models-type-meeting';

    public const PERMISSION_VERB_VIEW = 'view';
    public const PERMISSION_VERB_HOST = 'host';
    public const PERMISSION_VERB_MINI_HOST = 'mini-host';
    public const PERMISSION_VERB_REMOVE = 'remove';
    public const PERMISSION_VERB_PUBLISH = 'publish';
    public const PERMISSION_VERB_MANAGE = 'manage';
    public const PERMISSION_VERB_DENIED = 'denied';

    public const GRANT_ACCESS_USER_ONLY = 'denied';
    public const GRANT_ACCESS_VISITOR = 'remove';
    public const GRANT_ACCESS_ALL = 'view-hidden';

    public const SCO_TYPE_SHARED_MEETING_TEMPLATE = 'shared-meeting-templates';
    public const SCO_TYPE_MEETINGS = 'meetings';
    public const SCO_TYPE_SEMINARS = 'seminars';

    public const ROOT_PATH_SAVE_MEETING = [
        'MyLiveSessionV3',
        'Reunions',
    ];

    /**
     * Will be updated to be client dependant.
     */
    protected const REDIS_AUTH_COOKIE_PREFIX = 'adobe_connect_breeze_session_';
    protected const REDIS_AUTH_COOKIE_TIMEOUT = 300;

    public function __construct(
        HttpClientInterface $httpClient,
        protected Redis $redisClient,
        protected AdobeConnectSCORepository $adobeConnectSCORepository,
        protected AdobeConnectPrincipalFactory $adobeConnectPrincipalFactory,
        protected AdobeConnectPrincipalRepository $adobeConnectPrincipalRepository,
        protected AdobeConnectTelephonyProfileRepository $adobeConnectTelephonyProfileRepository,
        protected AdobeConnectMeetingAttendeeRepository $adobeConnectMeetingAttendeeRepository,
        protected ProviderParticipantSessionRoleRepository $participantSessionRoleRepository,
        protected LoggerInterface $logger,
        protected LoggerInterface $providerLogger
    ) {
        parent::__construct($httpClient);
    }

    protected function handleResponse(ResponseInterface $response, $action): Crawler
    {
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new UnexpectedProviderActionErrorException(__METHOD__);
        }

        $crawler = new Crawler($response->getContent());
        $code = $crawler->filterXPath('//results/status')->attr('code');
        $result = [];

        if ($code !== 'ok') {
            if (!empty($action)) {
                $crawler->filterXPath('//results/status')
                    ->each(function (Crawler $node) use (&$result) {
                        $invalidNodeExist = $node->matches('invalid');

                        $result =
                            [
                                '@code' => $node->attr('code'),
                                '@subcode' => $node->attr('subcode'),
                                'invalid' => [
                                    '@field' => $invalidNodeExist
                                        ? $node->filterXPath('//invalid')->attr('field')
                                        : null,
                                    '@subcode' => $invalidNodeExist
                                        ? $node->filterXPath('//invalid')->attr('subcode')
                                        : null,
                                ],
                            ];
                    });
                throw new ProviderGenericException(self::class, $this->errorHandleResponse($action, $result), $result['@code']);
            } else {
                throw new ProviderGenericException(self::class, $response->getContent());
            }
        }

        return $crawler;
    }

    private function errorHandleResponse($action = '', $result = []): string
    {
        if ($result['@code'] == 'internal-error') {
            return sprintf('Adobe Internal Error during the execution of "%s" action. Exception : %s',
                            $action,
                            (isset($result['exception']) ? $result['exception'] : 'No exception found')
                        );
        } elseif ($result['@code'] == 'no-data') {
            if ($action != 'principal-info') {
                return sprintf('No data found');
            }
        } elseif ($result['@code'] == 'invalid') {
            if (isset($result['invalid']['@field']) && isset($result['invalid']['@subcode'])) {
                if ($result['invalid']['@field'] == 'action') {
                    return sprintf('Action "%s" doesn\'t exist in Adobe Connect API', $action);
                } else {
                    return sprintf('Invalid field "%s" : %s', $result['invalid']['@field'], $result['invalid']['@subcode']);
                }
            } elseif (isset($result['@subcode'])) {
                if ($result['@subcode'] == 'user-suspended') {
                    return sprintf('Unable to login into AdobeConnect. Account suspended.');
                } else {
                    return sprintf('Invalid Exception during execution of "%s" action : %s', $action, $result['@subcode']);
                }
            } else {
                return sprintf('Invalid Exception during execution of "%s" action', $action);
            }
        } elseif ($result['@code'] == 'no-access') {
            if ($result['@subcode'] == 'denied') {
                return sprintf('Unauthorized access to execute "%s" action. Access denied.', $action);
            } elseif ($result['@subcode'] == 'not-logged-in') {
                return sprintf('Unauthorized access to execute "%s" action. You are not logged in.', $action);
            } elseif ($result['@subcode'] == 'account-expired') {
                return sprintf('Impossible to connect : Account Expired');
            }

            return sprintf('Unauthorized access to execute "%s" action. You are not logged in or you do not have to be the necessary rights to execute this action.', $action);
        } else {
            return sprintf('Couldn\'t perform the action: "%s". Status : %s', $action, $result['@code']);
        }

        return 'Unknow Error';
    }

    public function call(AbstractProviderConfigurationHolder $configurationHolder, string $action, mixed $parameters = null): ProviderResponse
    {
        $connectorResponse = new ProviderResponse();

        try {
            if (!($configurationHolder instanceof AdobeConnectConfigurationHolder)) {
                throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
            }

            if (!$configurationHolder->isValid()) {
                throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
            }

            $connectorResponse->setData(match ($action) {
                self::ACTION_LOGIN => $this->login($configurationHolder),
                self::ACTION_COMMON_INFO => $this->getConnectedUserdata($configurationHolder),
                self::ACTION_AUTHENTICATE_PRINCIPAL => $this->authenticatePrincipal($configurationHolder, ...$parameters),
                self::ACTION_AUTHENTICATE_CONFIGURATION_HOLDER => $this->authenticateConfigurationHolder($configurationHolder, $parameters),
                self::ACTION_UPDATE_PRINCIPAL_PASSWORD => $this->updatePrincipalPassword($configurationHolder, $parameters),
                self::ACTION_SCO_CONTENTS => $this->getSCOContents($configurationHolder, $parameters),
                self::ACTION_SCO_UPDATE => $this->updateSco($configurationHolder, $parameters),
                self::ACTION_SCO_UPDATE_PERMISSION => $this->updateScoDefaultPermissions($configurationHolder, $parameters),
                self::ACTION_SCO_DELETE => $this->deleteSco($configurationHolder, $parameters),
                self::ACTION_UPDATE_URL => $this->updateUrlPath($configurationHolder, ...$parameters),
                self::ACTION_PERMISSIONS_INFO => $this->permissionsInfo($configurationHolder, $parameters),
                self::ACTION_PERMISSIONS_UPDATE => $this->permissionsUpdate($configurationHolder, ...$parameters),
                self::ACTION_PERMISSIONS_RESET => $this->permissionsReset($configurationHolder, $parameters),
                self::ACTION_PRINCIPAL_UPDATE => $this->principalUpdate($configurationHolder, $parameters),
                self::ACTION_PRINCIPAL_DELETE => $this->principalDelete($configurationHolder, $parameters),
                self::ACTION_PRINCIPAL_LIST => $this->principalList($configurationHolder, ...$parameters),
                self::ACTION_TELEPHONY_PROFILE_LIST => $this->telephonyProfileList($configurationHolder, $parameters),
                self::ACTION_TELEPHONY_PROFILE_INFO => $this->telephonyProfileInfo($configurationHolder, $parameters),
                self::ACTION_REPORT_MEETING_ATTENDANCE => $this->reportMeetingAttendance($configurationHolder, $parameters),
                self::ACTION_SCO_ROOT_FOLDER => $this->getRootFolder($configurationHolder, $parameters),
                self::ACTION_SCO_NAME_LIST_FROM_PARENT_SCO => $this->getScoNameListFromParentSco($configurationHolder, $parameters),
                self::ACTION_SCO_FOLDER_EXIST => $this->isFolderExist($configurationHolder, ...$parameters),
                self::ACTION_SCO_CREATE_FOLDER => $this->createFolder($configurationHolder, ...$parameters),
                self::ACTION_SET_SCO_TELEPHONY_PROFILE => $this->setScoTelephonyProfile($configurationHolder, $parameters),
                self::FUNCTION_FIND_OR_CREATE_SCO => $this->findOrCreateSco($configurationHolder, ...$parameters),
                self::FUNCTION_GET_LS_TEMPLATE_FOLDER => $this->getLsTemplateFolder($configurationHolder),
                self::FUNCTION_GET_ALL_MODELS_TYPE_MEETING => $this->getAllModelsTypeMeeting($configurationHolder, $parameters),
            });
        } catch (AbstractProviderException $exception) {
            $this->logger->error($exception);
            $connectorResponse->setSuccess(false);
            $connectorResponse->setThrown($exception);
        }

        return $connectorResponse;
    }

    private function login(AdobeConnectConfigurationHolder $configurationHolder): string
    {
        $redisKey = self::REDIS_AUTH_COOKIE_PREFIX.'default'.$configurationHolder->getSubdomain().$configurationHolder->getUsername();

        if ($this->redisClient->exists($redisKey)) {
            return (string) $this->redisClient->get($redisKey);
        }

        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_LOGIN,
                'login' => $configurationHolder->getUsername(),
                'password' => $configurationHolder->getPassword(),
            ],
        ]);
        $this->handleResponse($response, self::ACTION_LOGIN);

        $breezeSessionCookie = current(explode(';', current($response->getHeaders()['set-cookie'])));
        $session = explode('=', $breezeSessionCookie)[1];
        $this->redisClient->set($redisKey, $session, self::REDIS_AUTH_COOKIE_TIMEOUT);

        return $session;
    }

    private function getConnectedUserdata(AdobeConnectConfigurationHolder $configurationHolder): AdobeConnectCommonInfo
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_COMMON_INFO,
                'session' => $this->login($configurationHolder),
            ],
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_COMMON_INFO);

        return new AdobeConnectCommonInfo(
            $crawler->filterXPath('//results/common/user')->attr('user-id'),
            $crawler->filterXPath('//results/common/user/login')->text()
        );
    }

    private function authenticatePrincipal(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectSCO $adobeConnectSCO, AdobeConnectPrincipal $adobeConnectPrincipal): string
    {
        $this->logger->info(' the URL Api is  GET => '.$configurationHolder->getApiUrl().' || action = '.self::ACTION_LOGIN.' || login = '.$adobeConnectPrincipal->getEmail().' || password  = '.$adobeConnectPrincipal->getPassword());
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_LOGIN,
                'login' => $adobeConnectPrincipal->getEmail(),
                'password' => $adobeConnectPrincipal->getPassword(),
            ],
        ]);
        $this->handleResponse($response, self::ACTION_LOGIN);

        $url = $this->buildRoomUrlFromResponse($response, $configurationHolder, $adobeConnectSCO);

        return $url;
    }

    private function authenticateConfigurationHolder(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectSCO $adobeConnectSCO): string
    {
        $redisKey = self::REDIS_AUTH_COOKIE_PREFIX.'login-configuration-holder'.$configurationHolder->getSubdomain().$configurationHolder->getUsername();

        if ($this->redisClient->exists($redisKey)) {
            return (string) $this->redisClient->get($redisKey);
        }

        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_LOGIN,
                'login' => $configurationHolder->getUsername(),
                'password' => $configurationHolder->getPassword(),
            ],
        ]);
        $this->handleResponse($response, self::ACTION_LOGIN);

        $url = $this->buildRoomUrlFromResponse($response, $configurationHolder, $adobeConnectSCO);

        $this->redisClient->set($redisKey, $url, self::REDIS_AUTH_COOKIE_TIMEOUT);

        return $url;
    }

    private function updatePrincipalPassword(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectPrincipal $adobeConnectPrincipal): void
    {
        $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_UPDATE_PRINCIPAL_PASSWORD,
                'user-id' => $adobeConnectPrincipal->getPrincipalIdentifier(),
                'password' => $adobeConnectPrincipal->getPassword(),
                'password-verify' => $adobeConnectPrincipal->getPassword(),
                'session' => $this->login($configurationHolder),
            ],
        ]);
    }

    /**
     * @doc https://helpx.adobe.com/adobe-connect/webservices/sco-shortcuts.html
     */
    private function getRootFolder(AdobeConnectConfigurationHolder $configurationHolder, string $rootFolderType): int
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_SCO_SHORTCUTS,
                'session' => $this->login($configurationHolder),
            ],
        ]);

        $rootScoId = null;

        $crawler = $this->handleResponse($response, self::ACTION_SCO_SHORTCUTS);
        $crawler->filterXPath('//results/shortcuts/sco')
                ->each(function (Crawler $node) use (&$rootFolderType, &$rootScoId) {
                    $scoType = $node->attr('type');

                    if ($scoType === $rootFolderType) {
                        $rootScoId = (int) $node->attr('sco-id');
                    }
                });

        if (is_null($rootScoId)) {
            $exception = new AbstractProviderException("Unable to find root folder named '$rootFolderType'");
            $this->logger->error($exception, ['rootFolderType' => $rootFolderType]);
            throw new AbstractProviderException("Unable to find root folder named '$rootFolderType'");
        }

        return $rootScoId;
    }

    /**
     * @doc https://helpx.adobe.com/adobe-connect/webservices/sco-contents.html
     */
    private function getScoNameListFromParentSco(AdobeConnectConfigurationHolder $configurationHolder, array $parameters): array
    {
        $parentScoId = $parameters['rootFolder'];
        $defaultQuery = [
            'action' => self::ACTION_SCO_CONTENTS,
            'sco-id' => $parentScoId,
            'session' => $this->login($configurationHolder),
        ];
        if (!empty($parameters['filter']) && $parameters['filter'] === 'meeting') {
            $defaultQuery['filter-type'] = 'meeting';
        } elseif (!empty($parameters['filter']) && $parameters['filter'] === 'folder') {
            $defaultQuery['filter-type'] = 'folder';
            /*} else {
                $defaultQuery['filter-type'] = 'meeting';*/
        }
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => $defaultQuery,
        ]);

        $scoFolders = [];

        $crawler = $this->handleResponse($response, self::ACTION_SCO_CONTENTS);
        $crawler->filterXPath('//results/scos/sco')
                ->each(function (Crawler $node) use (&$scoFolders) {
                    $scoId = $node->attr('sco-id');
                    $scoFolders[$scoId] = [
                            'uniq_identifier' => (int) $scoId,
                            'name' => $node->filterXPath('//name')->text(),
                        ];
                });

        asort($scoFolders);

        return $scoFolders;
    }

    private function isFolderExist(AdobeConnectConfigurationHolder $configurationHolder, int $folderSCOParentId, string $name): ?int
    {
        $query = [
            'action' => self::ACTION_SCO_CONTENTS,
            'sco-id' => $folderSCOParentId,
            'session' => $this->login($configurationHolder),
        ];

        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => $query,
        ]);
        $this->providerLogger->info('Call AdobeConnectConnector isFolderExist', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'query' => $query,
            'response' => ['status' => $response->getStatusCode()],
        ]);

        $fetchedSco = null;
        $crawler = $this->handleResponse($response, self::ACTION_SCO_CONTENTS);
        $crawler->filterXPath('//results/scos/sco')
                ->each(function (Crawler $node) use (&$rootSco, &$fetchedSco, &$name) {
                    if ($node->filterXPath('//name')->text() === $name) {
                        $fetchedSco = (int) $node->attr('sco-id');
                    }
                });

        return $fetchedSco;
    }

    private function createFolder(AdobeConnectConfigurationHolder $configurationHolder, int $folderSCOParentId, string $name): ?int
    {
        $query = [
            'action' => self::ACTION_SCO_UPDATE,
            'folder-id' => $folderSCOParentId,
            'type' => 'folder',
            'name' => $name,
            'session' => $this->login($configurationHolder),
        ];
        $result = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), ['query' => $query]);
        $this->providerLogger->info('Call AdobeConnectConnector createFolder', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'query' => $query,
            'response' => ['status' => $result->getStatusCode(), 'content' => $result->getContent()],
        ]);
        $fetchedScoId = null;
        $crawler = $this->handleResponse($result, self::ACTION_SCO_UPDATE);
        $crawler->filterXPath('//results/sco')
                ->each(function (Crawler $node) use (&$rootSco, &$fetchedScoId) {
                    $fetchedScoId = (int) $node->attr('sco-id');
                });

        return $fetchedScoId;
    }

    private function getSCOContents(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectSCO $rootSco): array
    {
        if ($rootSco->getUniqueScoIdentifier() === null) {
            throw new InvalidArgumentException('SCO identifier cannot be null.');
        }

        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_SCO_CONTENTS,
                'sco-id' => $rootSco->getUniqueScoIdentifier(),
                'session' => $this->login($configurationHolder),
            ],
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_SCO_CONTENTS);

        $fetchedSco = [];
        $crawler->filterXPath('//results/scos/sco')->each(function (Crawler $node) use (&$rootSco, &$fetchedSco, $configurationHolder) {
            $childScoIdentifier = (int) $node->attr('sco-id');

            if (null === $sco = $this->adobeConnectSCORepository->findOneBy(['uniqueScoIdentifier' => $childScoIdentifier])) {
                $sco = new AdobeConnectSCO($configurationHolder);
                $sco->setUniqueScoIdentifier($childScoIdentifier);
            }

            $sco->setType($node->attr('type'))
                ->setDuration((int) $node->attr('duration'))
                ->setName($node->filterXPath('//name')->text())
                ->setUrlPath($node->filterXPath('//url-path')->text())
                ->setIsSeminar($node->filterXPath('//is-seminar')->text() === 'true')
                ->setCreatedAt(new DateTime($node->filterXPath('//date-created')->text()))
                ->setUpdatedAt(new DateTime($node->filterXPath('//date-modified')->text()));

            if ($sco->getType() === AdobeConnectSCO::TYPE_MEETING) {
                $dateStartNode = $node->filterXPath('//date-begin');
                if (null !== $dateStart = $dateStartNode->count() ? $dateStartNode->text() : null) {
                    $sco->setDateStart(new DateTime($dateStart));
                }

                $dateEndNode = $node->filterXPath('//date-end');
                if (null !== $dateEnd = $dateEndNode->count() ? $dateEndNode->text() : null) {
                    $sco->setDateEnd(new DateTime($dateEnd));
                }
            }

            $fetchedSco[] = $sco;
        });

        return $fetchedSco;
    }

    private function updateSco(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectSCO $updatedSco): AdobeConnectSCO
    {
        // According to the Adobe connect restriction. @see: https://helpx.adobe.com/adobe-connect/webservices/common-xml-elements-attributes.html#lang
        $lang = (!empty($updatedSco->getLanguage()) && strlen($updatedSco->getLanguage()) >= 2)
            ? strtolower(substr($updatedSco->getLanguage(), 0, 2))
            : null;

        $query = [
            'action' => self::ACTION_SCO_UPDATE,
            'session' => $this->login($configurationHolder),
            'sco-id' => $updatedSco->getUniqueScoIdentifier(),
            'name' => $updatedSco->getName(),
            'date-begin' => $updatedSco->getDateStart()->format('c'),
            'date-end' => $updatedSco->getDateEnd()->format('c'),
            'url-path' => $updatedSco->getUrlPath(),
            'lang' => $lang,
            // In case of an update (unique SCO identifier already exists), don't send the folder identifier
            'folder-id' => $updatedSco->getUniqueScoIdentifier() ? null : $updatedSco->getParentScoIdentifier(),
            // In case of a create send source model uniq identifier
            'source-sco-id' => $updatedSco->getRoomModel(),
            'type' => $updatedSco->getType(),
        ];
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), ['query' => $query]);
        $this->providerLogger->info('Call AdobeConnectConnector updateSco', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'query' => $query,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);
        $crawler = $this->handleResponse($response, self::ACTION_SCO_UPDATE);

        if ($updatedSco->getUniqueScoIdentifier() === null) {
            $updatedSco->setUniqueScoIdentifier((int) $crawler->filterXPath('//results/sco')->attr('sco-id'));
            $updatedSco->setUrlPath($crawler->filterXPath('//results/sco/url-path')->text());
            $updatedSco->setCreatedAt(new DateTime($crawler->filterXPath('//results/sco/date-created')->text()));
            $updatedSco->setUpdatedAt(new DateTime($crawler->filterXPath('//results/sco/date-modified')->text()));
        }

        $updatedSco->setAbstractProviderConfigurationHolder($configurationHolder);

        return $updatedSco;
    }

    private function setScoTelephonyProfile(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectSCO $adobeConnectSCO): AdobeConnectSCO
    {
        $query = [
            'session' => $this->login($configurationHolder),
            'action' => 'acl-field-update',
            'acl-id' => $adobeConnectSCO->getUniqueScoIdentifier(),
            'field-id' => 'telephony-profile',
            'value' => $adobeConnectSCO->getProviderAudio()?->getProfileIdentifier(),
        ];
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), ['query' => $query]);
        $this->providerLogger->info('Call AdobeConnectConnector setScoTelephonyProfile', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'query' => $query,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);

        return $adobeConnectSCO;
    }

    private function updateScoDefaultPermissions(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectSCO $adobeConnectSCO): AdobeConnectSCO
    {
        // update distant permission
        // https://helpx.adobe.com/adobe-connect/webservices/permissions-update.html#permissions_update
        // https://helpx.adobe.com/adobe-connect/webservices/common-xml-elements-attributes.html#permission_id
        $query = [
            'action' => self::ACTION_PERMISSIONS_UPDATE,
            'session' => $this->login($configurationHolder),
            'acl-id' => $adobeConnectSCO->getUniqueScoIdentifier(),
            'principal-id' => 'public-access',
            'permission-id' => $adobeConnectSCO->getAdmittedSession(),
        ];
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), ['query' => $query]);
        $this->providerLogger->info('Call AdobeConnectConnector updateScoDefaultPermissions', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'query' => $query,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);

        return $adobeConnectSCO;
    }

    private function deleteSco(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectSCO $adobeConnectSCO): AdobeConnectSCO
    {
        $this->handleResponse(
            $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_SCO_DELETE,
                    'session' => $this->login($configurationHolder),
                    'sco-id' => $adobeConnectSCO->getParentScoIdentifier(),
                ],
            ]),
            self::ACTION_SCO_DELETE
        );

        return $adobeConnectSCO;
    }

    public function findOrCreateSco(
        AdobeConnectConfigurationHolder $configurationHolder,
        int $scoRootId,
        string $targetName
    ): ?int {
        $data = $this->getScoNameListFromParentSco($configurationHolder, ['rootFolder' => $scoRootId, 'filter' => 'all']);
        foreach ($data as $id => $sco) {
            if ($sco['name'] === $targetName) {
                return $id;
            }
        }

        return $this->createFolder($configurationHolder, $scoRootId, $targetName);
    }

    public function getAllModelsTypeMeeting(
        AdobeConnectConfigurationHolder $configurationHolder,
        ?array $parameters = []
    ): array {
        $modelsScoIds = [];
        $foldersScoIds = [];
        $parentScoId = $parameters['rootFolder'];
        $defaultQuery = [
            'action' => self::ACTION_SCO_CONTENTS,
            'sco-id' => $parentScoId,
            'session' => $this->login($configurationHolder),
        ];
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => $defaultQuery,
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_SCO_CONTENTS);
        $resultModelsScoIds = [];
        $crawler->filterXPath('//results/scos/sco')
            ->each(function (Crawler $node) use (&$scoFolders, &$resultModelsScoIds, &$parameters, &$configurationHolder) {
                $scoId = $node->attr('sco-id');
                if ($node->attr('type') == 'meeting') {
                    $modelsScoIds[$scoId] = [
                        'uniq_identifier' => (int) $scoId,
                        'name' => $node->filterXPath('//name')->text(),
                    ];
                    $resultModelsScoIds = array_merge($resultModelsScoIds, $modelsScoIds);
                } else { //folder
                    $parametersSubfolder = $parameters;
                    $parametersSubfolder['rootFolder'] = $scoId;
                    $resultModelsScoIds = array_merge($resultModelsScoIds, $this->getAllModelsTypeMeetingFromSubfolder($configurationHolder, $parametersSubfolder));
                }
                $scoFolders[$scoId] = [
                    'uniq_identifier' => (int) $scoId,
                    'name' => $node->filterXPath('//name')->text(),
                ];
            });

        return $resultModelsScoIds;
    }

    public function getAllModelsTypeMeetingFromSubfolder(
        AdobeConnectConfigurationHolder $configurationHolder,
        ?array $parameters = []
    ): array {
        $parentScoId = $parameters['rootFolder'];
        $defaultQuery = [
            'action' => self::ACTION_SCO_CONTENTS,
            'sco-id' => $parentScoId,
            'session' => $this->login($configurationHolder),
        ];

        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => $defaultQuery,
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_SCO_CONTENTS);

        $resultModelsScoIds = [];
        $crawler->filterXPath('//results/scos/sco')
            ->each(function (Crawler $node) use (&$scoFolders, &$resultModelsScoIds) {
                $scoId = $node->attr('sco-id');
                if ($node->attr('type') == 'meeting') {
                    $resultModelsScoIds[$scoId] = [
                        'uniq_identifier' => (int) $scoId,
                        'name' => $node->filterXPath('//name')->text(),
                    ];
                }
            });

        return $resultModelsScoIds;
    }

    public function getLsTemplateFolder(
        AdobeConnectConfigurationHolder $configurationHolder
    ): ?int {
        $meetingScoId = $this->getRootFolder($configurationHolder, self::SCO_TYPE_MEETINGS);
        $liveSessionScoId = $this->findOrCreateSco($configurationHolder, $meetingScoId, self::LIVESESSION_DEFAULT_FOLDER);
        $modelesScoId = $this->findOrCreateSco($configurationHolder, $liveSessionScoId, self::LIVESESSION_DEFAULT_TEMPLATE_FOLDER);

        return $modelesScoId;
    }

    private function updateUrlPath(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectSCO $adobeConnectSCO, string $newUrlPath): AdobeConnectSCO
    {
        $this->handleResponse(
            $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_UPDATE_URL,
                    'session' => $this->login($configurationHolder),
                    'sco-id' => $adobeConnectSCO->getUniqueScoIdentifier(),
                    'url-path' => $newUrlPath,
                ],
            ]),
            self::ACTION_UPDATE_URL
        );
        $adobeConnectSCO->setUrlPath($newUrlPath);

        return $adobeConnectSCO;
    }

    /**
     * @return AdobeConnectPrincipal[] Created principals. Updated principals are NOT returned. If you need them, you
     *                                 should use $adobeConnectSCO->getPrincipals().
     *
     * @throws AbstractProviderException
     * @throws TransportExceptionInterface
     */
    private function permissionsInfo(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectSCO $adobeConnectSCO): array
    {
        if ($adobeConnectSCO->getUniqueScoIdentifier() === null) {
            throw new InvalidArgumentException('SCO identifier cannot be null');
        }

        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_PERMISSIONS_INFO,
                'acl-id' => $adobeConnectSCO->getUniqueScoIdentifier(),
                'session' => $this->login($configurationHolder),
            ],
        ]);
        $crawler = $this->handleResponse($response, self::ACTION_PERMISSIONS_INFO);

        $createdPrincipals = $this->adobeConnectPrincipalFactory->createPrincipalsFromCrawler($configurationHolder, $crawler->filterXPath('//results/permissions/principal'));
        foreach ($createdPrincipals as $principal) {
            $adobeConnectSCO->addPrincipal($principal);
        }

        return $createdPrincipals;
    }

    private function permissionsUpdate(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectSCO $adobeConnectSCO, AdobeConnectPrincipal $adobeConnectPrincipal, string $permission): AdobeConnectPrincipal
    {
        if (!in_array($permission, $this->getAvailablePermissionIdentifiers())) {
            throw new InvalidArgumentException('Permission verb is not valid. Refer to https://helpx.adobe.com/adobe-connect/webservices/common-xml-elements-attributes.html#permission_id.');
        }
        $query = [
            'action' => self::ACTION_PERMISSIONS_UPDATE,
            'session' => $this->login($configurationHolder),
            'acl-id' => $adobeConnectSCO->getUniqueScoIdentifier(),
            'principal-id' => $adobeConnectPrincipal->getPrincipalIdentifier(),
            'permission-id' => $permission,
        ];
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), ['query' => $query]);
        $this->providerLogger->info('Call AdobeConnectConnector principalUpdate', [
            'apiUrl' => $configurationHolder->getApiUrl(),
            'query' => $query,
            'response' => ['status' => $response->getStatusCode(), 'content' => $response->getContent()],
        ]);
        $this->handleResponse($response, self::ACTION_PERMISSIONS_UPDATE);

        return $adobeConnectPrincipal;
    }

    private function permissionsReset(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectSCO $adobeConnectSCO): AdobeConnectSCO
    {
        if ($adobeConnectSCO->getUniqueScoIdentifier() === null) {
            throw new InvalidArgumentException('SCO identifier cannot be null');
        }

        $this->handleResponse(
            $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_PERMISSIONS_RESET,
                    'session' => $this->login($configurationHolder),
                    'acl-id' => $adobeConnectSCO->getUniqueScoIdentifier(),
                ],
            ]),
            self::ACTION_PERMISSIONS_RESET
        );

        return $adobeConnectSCO;
    }

    private function principalUpdate(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectPrincipal $adobeConnectPrincipal): AdobeConnectPrincipal
    {
        $user = $adobeConnectPrincipal->getUser();

        $crawler = $this->handleResponse(
            $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_PRINCIPAL_UPDATE,
                    'session' => $this->login($configurationHolder),
                    'principal-id' => $adobeConnectPrincipal->getPrincipalIdentifier(),
                    'login' => $user->getEmail(),
                    'first-name' => $user->getFirstName(),
                    'last-name' => $user->getLastName(),
                    // The password has to be set only when we're creating a new principal, not updating one
                    'password' => $adobeConnectPrincipal->getPrincipalIdentifier() ? null : $adobeConnectPrincipal->getPassword(),
                    // The type has to be set only when we're creating a new principal, not updating one
                    'type' => $adobeConnectPrincipal->getPrincipalIdentifier() ? null : 'user',
                    'has-children' => false,
                ],
            ]),
            self::ACTION_PRINCIPAL_UPDATE
        );

        if ($adobeConnectPrincipal->getPrincipalIdentifier() === null) {
            $node = $crawler->filterXPath('//results/principal');
            $adobeConnectPrincipal->setPrincipalIdentifier((int) $node->attr('principal-id'))
                ->setName($node->filterXPath('//name')->text());
        }

        $adobeConnectPrincipal->setName($user->getFirstName().' '.$user->getLastName());

        return $adobeConnectPrincipal;
    }

    private function principalDelete(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectPrincipal $adobeConnectPrincipal): AdobeConnectPrincipal
    {
        $this->handleResponse(
            $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_PRINCIPAL_DELETE,
                    'session' => $this->login($configurationHolder),
                    'principal-id' => $adobeConnectPrincipal->getPrincipalIdentifier(),
                ],
            ]),
            self::ACTION_PRINCIPAL_DELETE
        );

        return $adobeConnectPrincipal;
    }

    private function principalList(AdobeConnectConfigurationHolder $configurationHolder, bool $asAGroup = false, array $options = []): array
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => array_merge([
                'action' => self::ACTION_PRINCIPAL_LIST,
                'session' => $this->login($configurationHolder),
            ], $options),
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_PRINCIPAL_LIST);

        return (!$asAGroup)
            ? $this->adobeConnectPrincipalFactory->createPrincipalsFromCrawler($configurationHolder, $crawler->filterXPath('//results/principal-list/principal'))
            : $this->adobeConnectPrincipalFactory->createPrincipalsGroupFromCrawler($configurationHolder, $crawler->filterXPath('//results/principal-list/principal'));
    }

    /**
     * @return AdobeConnectTelephonyProfile[] Created profiles. Updated profiles are NOT returned. If you need them, you
     *                                        should use $adobeConnectPrincipal->getTelephonyProfiles().
     */
    private function telephonyProfileList(AdobeConnectConfigurationHolder $configurationHolder, ?AdobeConnectPrincipal $adobeConnectPrincipal = null): array
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_TELEPHONY_PROFILE_LIST,
                'session' => $this->login($configurationHolder),
                'principal-id' => $adobeConnectPrincipal?->getPrincipalIdentifier(),
            ],
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_TELEPHONY_PROFILE_LIST);
        $createdProfiles = [];

        $crawler->filterXPath('//results/telephony-profiles/profile')->each(function (Crawler $node) use (&$createdProfiles, $adobeConnectPrincipal, $configurationHolder) {
            $profileIdentifier = (int) $node->attr('profile-id');
            if (null === $telephonyProfile = $this->adobeConnectTelephonyProfileRepository
                    ->findOneBy([
                        'profileIdentifier' => $profileIdentifier,
                        'configurationHolder' => $configurationHolder,
                    ])
            ) {
                $telephonyProfile = new AdobeConnectTelephonyProfile();
                $telephonyProfile->setProfileIdentifier($profileIdentifier);
                $createdProfiles[] = $telephonyProfile;
            }

            $telephonyProfile
                ->setConnectionType(
                    str_replace(' ', ' ', $node->filterXPath('//name')->text())
                )
                ->setName($node->filterXPath('//profile-name')->text());

            $adobeConnectPrincipal?->addTelephonyProfile($telephonyProfile);
        });

        foreach ($createdProfiles as $telephonyProfile) {
            $this->telephonyProfileInfo($configurationHolder, $telephonyProfile);
        }

        return $createdProfiles;
    }

    private function telephonyProfileInfo(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectTelephonyProfile $adobeConnectTelephonyProfile): AdobeConnectTelephonyProfile
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_TELEPHONY_PROFILE_INFO,
                'session' => $this->login($configurationHolder),
                'profile-id' => $adobeConnectTelephonyProfile->getProfileIdentifier(),
            ],
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_TELEPHONY_PROFILE_INFO);

        $node = $crawler->filterXPath('//results/telephony-profile-fields');
        switch ($adobeConnectTelephonyProfile->getConnectionType()) {
            case AbstractProviderAudio::AUDIO_INTERCALL:
                $phoneNumber = $node->matches('telephony-profile-fields x-tel-intercall-uv-conference-number')
                    ? $node->filterXPath('//x-tel-intercall-uv-conference-number')->text()
                    : null;
                $animatorCode = $node->matches('telephony-profile-fields x-tel-intercall-leader-pin')
                    ? $node->filterXPath('//x-tel-intercall-leader-pin')->text()
                    : null;
                $participantCode = $node->matches('telephony-profile-fields x-tel-intercall-participant-code')
                    ? $node->filterXPath('//x-tel-intercall-participant-code')->text()
                    : null;
                $meetingOneConferenceId = null;
                break;
            case AbstractProviderAudio::AUDIO_PGI:
            case AbstractProviderAudio::AUDIO_PGI_EMEA:
                $phoneNumber = $node->matches('telephony-profile-fields x-tel-premiere-uv-conference-number')
                    ? $node->filterXPath('//x-tel-premiere-uv-conference-number')->text()
                    : null;
                $animatorCode = $node->matches('telephony-profile-fields x-tel-premiere-moderator-code')
                    ? $node->filterXPath('//x-tel-premiere-moderator-code')->text()
                    : null;
                $participantCode = $node->matches('telephony-profile-fields x-tel-premiere-participant-code')
                    ? $node->filterXPath('//x-tel-premiere-participant-code')->text()
                    : null;
                $meetingOneConferenceId = null;
                break;
            case AbstractProviderAudio::AUDIO_MEETINGONE_EMEA:
                $phoneNumber = null;
                $participantCode = null;
                $animatorCode = $node->matches('telephony-profile-fields x-tel-meetingone-host-pin')
                    ? $node->filterXPath('//x-tel-meetingone-host-pin')->text()
                    : null;
                $meetingOneConferenceId = $node->matches('telephony-profile-fields x-tel-meetingone-conference-id')
                    ? $node->filterXPath('//x-tel-meetingone-conference-id')->text()
                    : null;
                break;
            case AbstractProviderAudio::AUDIO_MEETINGONE_NA:
                $phoneNumber = null;
                $participantCode = null;
                $animatorCode = $node->matches('telephony-profile-fields x-tel-meetingone-host-pin')
                    ? $node->filterXPath('//x-tel-meetingone-host-pin')->text()
                    : null;
                $meetingOneConferenceId = $node->matches('telephony-profile-fields x-tel-meetingone-conference-id')
                    ? $node->filterXPath('//x-tel-meetingone-conference-id')->text()
                    : null;
                break;
            default:
                $phoneNumber = $animatorCode = $participantCode = $meetingOneConferenceId = null;
                break;
        }

        $adobeConnectTelephonyProfile
            ->setAdobeDefaultPhoneNumber($phoneNumber)
            ->setCodeAnimator($animatorCode)
            ->setCodeParticipant($participantCode)
            ->setMeetingOneConferenceId($meetingOneConferenceId);

        return $adobeConnectTelephonyProfile;
    }

    private function reportMeetingAttendance(AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectSCO $adobeConnectSCO): array
    {
        if ($adobeConnectSCO->getUniqueScoIdentifier() === null) {
            throw new InvalidArgumentException('SCO identifier cannot be null');
        }

        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_REPORT_MEETING_ATTENDANCE,
                'session' => $this->login($configurationHolder),
                'sco-id' => $adobeConnectSCO->getUniqueScoIdentifier(),
            ],
        ]);
        $crawler = $this->handleResponse($response, self::ACTION_REPORT_MEETING_ATTENDANCE);

        $valuesAttendees = [];
        $crawler->filterXPath('//results/report-meeting-attendance/row')->each(function (Crawler $node) use (&$valuesAttendees, $adobeConnectSCO) {
            $principalIdentifier = (int) $node->attr('principal-id');

            if (null !== $adobeConnectPrincipal = $this->adobeConnectPrincipalRepository->findOneBy(['principalIdentifier' => $principalIdentifier])) {
                $sessionStart = new DateTimeImmutable($node->filterXPath('//date-created')->text());
                $sessionEnd = new DateTimeImmutable($node->filterXPath('//date-end')->text());

                if ($this->participantSessionRoleRepository->isRegisteredOntoSession($adobeConnectSCO->getId(), $adobeConnectPrincipal->getId())) {
                    $valuesAttendees[] = [
                        'principal-id' => $principalIdentifier,
                        'sco-id' => (int) $node->attr('sco-id'),
                        'asset-id' => (int) $node->attr('asset-id'),
                        'login' => strtolower($node->filterXPath('//login')->text()),
                        'session-name' => $node->filterXPath('//session-name')->text(),
                        'sco-name' => $node->filterXPath('//sco-name')->text(),
                        'date-created' => $sessionStart,
                        'date-end' => $sessionEnd,
                        'participant-name' => $node->filterXPath('//participant-name')->text(),
                    ];
                }
            }
        });

        return $valuesAttendees;
    }

    private function getAvailablePermissionIdentifiers(): array
    {
        return [
            self::PERMISSION_VERB_VIEW,
            self::PERMISSION_VERB_HOST,
            self::PERMISSION_VERB_MINI_HOST,
            self::PERMISSION_VERB_REMOVE,
            self::PERMISSION_VERB_PUBLISH,
            self::PERMISSION_VERB_MANAGE,
            self::PERMISSION_VERB_DENIED,
        ];
    }

    private function buildRoomUrlFromResponse(ResponseInterface $response, AdobeConnectConfigurationHolder $configurationHolder, AdobeConnectSCO $adobeConnectSCO): string
    {
        $breezeSessionCookie = current(explode(';', current($response->getHeaders()['set-cookie'])));
        $session = explode('=', $breezeSessionCookie)[1];

        return "https://{$configurationHolder->getDomain()}{$adobeConnectSCO->getUrlPath()}?session=$session";
    }
}
