<?php

namespace App\Service\Connector\Adobe;

use App\Entity\Adobe\AdobeConnectWebinarPrincipal;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\Adobe\AdobeConnectWebinarTelephonyProfile;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Exception\AbstractProviderException;
use App\Exception\InvalidArgumentException;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\ProviderGenericException;
use App\Exception\UnexpectedProviderActionErrorException;
use App\Model\Provider\AdobeConnect\AdobeConnectWebinarCommonInfo;
use App\Model\ProviderResponse;
use App\Repository\AdobeConnectWebinarMeetingAttendeeRepository;
use App\Repository\AdobeConnectWebinarPrincipalRepository;
use App\Repository\AdobeConnectWebinarSCORepository;
use App\Repository\AdobeConnectWebinarTelephonyProfileRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Service\Connector\AbstractConnector;
use App\Service\Factory\Adobe\AdobeConnectWebinarPrincipalFactory;
use DateTime;
use DateTimeImmutable;
use function in_array;
use Psr\Log\LoggerInterface;
use Redis;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class AdobeConnectWebinarConnector extends AbstractConnector
{
    public const ACTION_LOGIN = 'login';
    public const ACTION_CREATE_ROOM_SCO_MEETING = 'create-room-sco-meeting';
    public const ACTION_SCO_CONTENTS = 'sco-contents';
    public const ACTION_CONVERT_ROOM_SCO_MEETING_TO_ROOM_SEMINAR = 'convert-room-sco-meeting-to-room-seminar';
    public const ACTION_CREATE_SESSION_SEMINAR = 'seminar-session-sco-update';
    public const ACTION_SCO_UPDATE = 'sco-update';
    public const ACTION_SET_NBR_MAX_USERS_TO_WEBINAR = 'set-nbr-max-users-to-webinar';
    public const ACTION_ACL_FIELD_UPDATE = 'acl-field-update';
    public const ACTION_CHANGE_AUDIO_PROFIL = 'change-audio-profile';
    public const ACTION_PERMISSIONS_UPDATE = 'permissions-update';
    public const ACTION_LINK_LICENCE_TO_EVENT = 'link-licence-to-event';
    public const ACTION_CHANGE_ACCESS_TYPE_TO_EVENT = 'change-access-type-to-event';
    public const ACTION_DELETE_WEBINAR = 'delete-webinar';
    public const ACTION_SCO_DELETE = 'sco-delete';
    public const ACTION_SEMINAR_SESSION_SCO_UPDATE = 'seminar-session-sco-update';
    public const ACTION_UPDATE_ROOM_WEBINAR = 'update-room-webinar';
    public const ACTION_UPDATE_SESSION_WEBINAR = 'update-session-webinar';
    public const ACTION_REPORT_MEETING_ATTENDANCE = 'report-meeting-attendance';
    public const ACTION_REPORT_MEETING_SESSION = 'report-meeting-session';
    public const ACTION_REPORT_EVENT_PARTICIPANTS_COMPLETE_INFORMATION = 'report-event-participants-complete-information';
    public const ACTION_PRINCIPAL_LIST = 'principal-list';
    public const ACTION_PRINCIPAL_UPDATE = 'principal-update';
    public const ACTION_ADD_USER_TO_ROOM_WEBINAR = 'add-user-to-room-webinar';
    public const ACTION_REMOVE_USER_TO_ROOM_WEBINAR = 'remove-user-to-room-webinar';
    public const ACTION_PRINCIPALS_DELETE = 'principals-delete';
    public const ACTION_UPDATE_PRINCIPAL_PASSWORD = 'user-update-pwd';
    public const ACTION_TELEPHONY_PROFILE_LIST = 'telephony-profile-list';
    public const ACTION_TELEPHONY_PROFILE_INFO = 'telephony-profile-info';
    public const ACTION_COMMON_INFO = 'common-info';
    public const ACTION_AUTHENTICATE_PRINCIPAL = 'auth';
    public const ACTION_AUTHENTICATE_LICENCE = 'auth-licence';
    public const ACTION_AUTHENTICATE_CONFIGURATION_HOLDER = 'auth-configuration-holder';
    public const ACTION_SCO_SHORTCUTS = 'sco-shortcuts';
    public const ACTION_SCO_UPDATE_PERMISSION = 'sco-update-permission';
    public const ACTION_UPDATE_URL = 'update-sco-url';
    public const ACTION_PERMISSIONS_INFO = 'permissions-info';
    public const ACTION_PERMISSIONS_RESET = 'permissions-reset';
    public const ACTION_SCO_ROOT_FOLDER = 'sco-root-folder';
    //public const ACTION_SCO_USER_WEBINAR_FOLDER = 'sco-user-webinar-folder';
    public const ACTION_SCO_NAME_LIST_FROM_PARENT_SCO = 'sco-content-names';

    public const ACTION_SCO_FOLDER_EXIST = 'sco-folder-exist';
    public const ACTION_SCO_CREATE_FOLDER = 'sco-create-folder';
    public const ACTION_SET_SCO_TELEPHONY_PROFILE = 'sco-set-telephony-profile';

    public const FUNCTION_GET_LS_TEMPLATE_FOLDER = 'find-ls-template-folder';
    public const FUNCTION_FIND_OR_CREATE_SCO = 'find-or-create-sco';
    public const LIVESESSION_DEFAULT_FOLDER = 'Mylivesession Manager V3';
    public const LIVESESSION_DEFAULT_TEMPLATE_FOLDER = 'Modeles de seminaires';

    //public const GRANT_ACCESS_USER_ONLY = 'denied';
    //public const GRANT_ACCESS_VISITOR = 'remove';
    //public const GRANT_ACCESS_ALL = 'view-hidden';

    public const SCO_TYPE_SHARED_MEETING_TEMPLATE = 'shared-meeting-templates';
    public const SCO_TYPE_MEETINGS = 'meetings';
    public const SCO_TYPE_SEMINARS = 'seminars';

    public const ROOT_PATH_SAVE_MEETING = [
        'Mylivesession Manager V3',
        'Sessions de seminaires',
    ];

    public const ROOT_PATH_TEMPLATE_FOLDER = [
        'Mylivesession Manager V3',
        'Modeles de seminaires',
    ];

    /**
     * Will be updated to be client dependant.
     */
    protected const REDIS_AUTH_COOKIE_PREFIX = 'adobe_connect_webinar_breeze_session_';
    protected const REDIS_AUTH_COOKIE_TIMEOUT = 300;

    public function __construct(
        HttpClientInterface $httpClient,
        protected Redis $redisClient,
        protected AdobeConnectWebinarSCORepository $adobeConnectWebinarSCORepository,
        protected AdobeConnectWebinarPrincipalFactory $adobeConnectWebinarPrincipalFactory,
        protected AdobeConnectWebinarPrincipalRepository $adobeConnectWebinarPrincipalRepository,
        protected AdobeConnectWebinarTelephonyProfileRepository $adobeConnectWebinarTelephonyProfileRepository,
        protected AdobeConnectWebinarMeetingAttendeeRepository $adobeConnectWebinarMeetingAttendeeRepository,
        protected ProviderParticipantSessionRoleRepository $participantSessionRoleRepository,
        protected LoggerInterface $logger
    ) {
        parent::__construct($httpClient);
    }

    protected function handleResponse(ResponseInterface $response, $action): Crawler
    {
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new UnexpectedProviderActionErrorException(__METHOD__);
        }

        $crawler = new Crawler($response->getContent());
        $code = $crawler->filterXPath('//results/status')->attr('code');
        $result = [];

        if ($code !== 'ok') {
            if (!empty($action)) {
                $crawler->filterXPath('//results/status')
                    ->each(function (Crawler $node) use (&$result) {
                        $invalidNodeExist = $node->matches('invalid');

                        $result =
                            [
                                '@code' => $node->attr('code'),
                                '@subcode' => $node->attr('subcode'),
                                'invalid' => [
                                    '@field' => $invalidNodeExist
                                        ? $node->filterXPath('//invalid')->attr('field')
                                        : null,
                                    '@subcode' => $invalidNodeExist
                                        ? $node->filterXPath('//invalid')->attr('subcode')
                                        : null,
                                ],
                            ];
                    });
                throw new ProviderGenericException(self::class, $this->errorHandleResponse($action, $result), $result['@code']);
            } else {
                throw new ProviderGenericException(self::class, $response->getContent());
            }
        }

        return $crawler;
    }

    private function errorHandleResponse($action = '', $result = []): string
    {
        if ($result['@code'] == 'internal-error') {
            return sprintf('Adobe Internal Error during the execution of "%s" action. Exception : %s',
                            $action,
                            (isset($result['exception']) ? $result['exception'] : 'No exception found')
                        );
        } elseif ($result['@code'] == 'no-data') {
            if ($action != 'principal-info') {
                return sprintf('No data found');
            }
        } elseif ($result['@code'] == 'invalid') {
            if (isset($result['invalid']['@field']) && isset($result['invalid']['@subcode'])) {
                if ($result['invalid']['@field'] == 'action') {
                    return sprintf('Action "%s" doesn\'t exist in Adobe Connect API', $action);
                } else {
                    return sprintf('Invalid field "%s" : %s', $result['invalid']['@field'], $result['invalid']['@subcode']);
                }
            } elseif (isset($result['@subcode'])) {
                if ($result['@subcode'] == 'user-suspended') {
                    return sprintf('Unable to login into AdobeConnect. Account suspended.');
                } else {
                    return sprintf('Invalid Exception during execution of "%s" action : %s', $action, $result['@subcode']);
                }
            } else {
                return sprintf('Invalid Exception during execution of "%s" action', $action);
            }
        } elseif ($result['@code'] == 'no-access') {
            if ($result['@subcode'] == 'denied') {
                return sprintf('Unauthorized access to execute "%s" action. Access denied.', $action);
            } elseif ($result['@subcode'] == 'not-logged-in') {
                return sprintf('Unauthorized access to execute "%s" action. You are not logged in.', $action);
            } elseif ($result['@subcode'] == 'account-expired') {
                return sprintf('Impossible to connect : Account Expired');
            }

            return sprintf('Unauthorized access to execute "%s" action. You are not logged in or you do not have to be the necessary rights to execute this action.', $action);
        } else {
            return sprintf('Couldn\'t perform the action: "%s". Status : %s', $action, $result['@code']);
        }

        return 'Unknow Error';
    }

    public function call(AbstractProviderConfigurationHolder $configurationHolder, string $action, mixed $parameters = null): ProviderResponse
    {
        $connectorResponse = new ProviderResponse();

        try {
            if (!($configurationHolder instanceof AdobeConnectWebinarConfigurationHolder)) {
                throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
            }

            if (!$configurationHolder->isValid()) {
                throw new InvalidProviderConfigurationHandler('provider configuration holder is invalid');
            }

            $connectorResponse->setData(match ($action) {
                self::ACTION_LOGIN => $this->login($configurationHolder),
                self::ACTION_CREATE_ROOM_SCO_MEETING => $this->createRoomScoMeeting($configurationHolder, $parameters),
                self::ACTION_SCO_CONTENTS => $this->getSCOContents($configurationHolder, $parameters),
                self::ACTION_CONVERT_ROOM_SCO_MEETING_TO_ROOM_SEMINAR => $this->convertRoomScoMeetingToRoomSeminar($configurationHolder, $parameters),
                self::ACTION_CREATE_SESSION_SEMINAR => $this->createSessionSeminar($configurationHolder, $parameters),
                self::ACTION_SET_NBR_MAX_USERS_TO_WEBINAR => $this->changeNbrMaxUsersToWebinar($configurationHolder, $parameters),
                self::ACTION_CHANGE_ACCESS_TYPE_TO_EVENT => $this->changeAccessTypeToEvent($configurationHolder, $parameters),
                self::ACTION_CHANGE_AUDIO_PROFIL => $this->changeProfilAudioToWebinar($configurationHolder, $parameters),
                self::ACTION_LINK_LICENCE_TO_EVENT => $this->linkLicenceToEvent($configurationHolder, $parameters),
                self::ACTION_DELETE_WEBINAR => $this->deleteWebinar($configurationHolder, $parameters),
                self::ACTION_SEMINAR_SESSION_SCO_UPDATE => $this->updateSessionWebinar($configurationHolder, $parameters),
                //self::ACTION_SCO_UPDATE => $this->updateSessionWebinar($configurationHolder, $parameters),
                self::ACTION_UPDATE_ROOM_WEBINAR => $this->updateRoomWebinar($configurationHolder, $parameters),
                //self::ACTION_UPDATE_SESSION_WEBINAR => $this->updateRoomWebinar($configurationHolder, $parameters),
                self::ACTION_REPORT_MEETING_SESSION => $this->reportMeetingSession($configurationHolder, $parameters),
                self::ACTION_REPORT_EVENT_PARTICIPANTS_COMPLETE_INFORMATION => $this->reportEventParticipantsCompleteInformation($configurationHolder, $parameters),
                self::ACTION_COMMON_INFO => $this->getConnectedUserdata($configurationHolder),
                self::ACTION_AUTHENTICATE_PRINCIPAL => $this->authenticatePrincipal($configurationHolder, ...$parameters),
                self::ACTION_AUTHENTICATE_LICENCE => $this->authenticateLicence($configurationHolder, ...$parameters),
                self::ACTION_AUTHENTICATE_CONFIGURATION_HOLDER => $this->authenticateConfigurationHolder($configurationHolder, $parameters),
                self::ACTION_UPDATE_PRINCIPAL_PASSWORD => $this->updatePrincipalPassword($configurationHolder, $parameters),
                self::ACTION_SCO_UPDATE_PERMISSION => $this->updateScoDefaultPermissions($configurationHolder, $parameters),
                self::ACTION_UPDATE_URL => $this->updateUrlPath($configurationHolder, ...$parameters),
                self::ACTION_PERMISSIONS_INFO => $this->permissionsInfo($configurationHolder, $parameters),
                self::ACTION_PERMISSIONS_UPDATE => $this->permissionsUpdate($configurationHolder, ...$parameters),
                self::ACTION_PERMISSIONS_RESET => $this->permissionsReset($configurationHolder, $parameters),
                self::ACTION_PRINCIPAL_UPDATE => $this->principalUpdate($configurationHolder, $parameters),
                self::ACTION_PRINCIPALS_DELETE => $this->principalsDelete($configurationHolder, $parameters),
                self::ACTION_PRINCIPAL_LIST => $this->principalList($configurationHolder, ...$parameters),
                self::ACTION_TELEPHONY_PROFILE_LIST => $this->telephonyProfileList($configurationHolder, $parameters),
                self::ACTION_TELEPHONY_PROFILE_INFO => $this->telephonyProfileInfo($configurationHolder, $parameters),
                self::ACTION_REPORT_MEETING_ATTENDANCE => $this->reportMeetingAttendance($configurationHolder, $parameters),
                self::ACTION_SCO_ROOT_FOLDER => $this->getRootFolder($configurationHolder, $parameters),
                //self::ACTION_SCO_USER_WEBINAR_FOLDER => $this->getUserWebinarFolder($configurationHolder, $parameters),
                self::ACTION_SCO_NAME_LIST_FROM_PARENT_SCO => $this->getScoNameListFromParentSco($configurationHolder, $parameters),

                self::ACTION_SCO_FOLDER_EXIST => $this->isFolderExist($configurationHolder, ...$parameters),
                self::ACTION_SCO_CREATE_FOLDER => $this->createFolder($configurationHolder, ...$parameters),
                self::ACTION_SET_SCO_TELEPHONY_PROFILE => $this->setScoTelephonyProfile($configurationHolder, $parameters),

                self::FUNCTION_FIND_OR_CREATE_SCO => $this->findOrCreateSco($configurationHolder, ...$parameters),
                self::FUNCTION_GET_LS_TEMPLATE_FOLDER => $this->getLsTemplateFolder($configurationHolder)
            });
        } catch (AbstractProviderException $exception) {
            $this->logger->error($exception);
            $connectorResponse->setSuccess(false);
            $connectorResponse->setThrown($exception);
        }

        return $connectorResponse;
    }

    private function login(AdobeConnectWebinarConfigurationHolder $configurationHolder): string
    {
        $redisKey = self::REDIS_AUTH_COOKIE_PREFIX.'default'.$configurationHolder->getSubdomain().$configurationHolder->getUsername();

        if ($this->redisClient->exists($redisKey)) {
            return (string) $this->redisClient->get($redisKey);
        }

        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_LOGIN,
                'login' => $configurationHolder->getUsername(),
                'password' => $configurationHolder->getPassword(),
            ],
        ]);
        $this->handleResponse($response, self::ACTION_LOGIN);

        $breezeSessionCookie = current(explode(';', current($response->getHeaders()['set-cookie'])));
        $session = explode('=', $breezeSessionCookie)[1];
        $this->redisClient->set($redisKey, $session, self::REDIS_AUTH_COOKIE_TIMEOUT);

        return $session;
    }

    private function createRoomScoMeeting(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        //dump('ACTION_CREATE_ROOM_SCO_MEETING');
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_SCO_UPDATE,
                    'folder-id' => $adobeConnectWebinarSCO->getParentScoIdentifier(), // $configurationHolder->getSavingMeetingFolderScoId(),
                    'type' => 'meeting',
                    'source-sco-id' => $adobeConnectWebinarSCO->getRoomModel(), //Récupérer ici le sco-id du modèle d'après lequel on souhaite construire la salle
                    'name' => $adobeConnectWebinarSCO->getName(),
                    'session' => $this->login($configurationHolder),
                ],
            ]);
        //$crawler = new Crawler($response->getContent());
        $crawler = $this->handleResponse($response, self::ACTION_CREATE_ROOM_SCO_MEETING);

        if ($adobeConnectWebinarSCO->getUniqueScoIdentifier() === null) {
            $adobeConnectWebinarSCO->setUniqueScoIdentifier((int) $crawler->filterXPath('//results/sco')->attr('sco-id'));
            $adobeConnectWebinarSCO->setUrlPath($crawler->filterXPath('//results/sco/url-path')->text());
            //$adobeConnectWebinarSCO->setCreatedAt(new DateTime($crawler->filterXPath('//results/sco/date-created')->text()));
            //$adobeConnectWebinarSCO->setUpdatedAt(new DateTime($crawler->filterXPath('//results/sco/date-modified')->text()));
            $adobeConnectWebinarSCO->setCreatedAt(new \DateTime());
            $adobeConnectWebinarSCO->setUpdatedAt(new \DateTime());
        }

        $adobeConnectWebinarSCO->setAbstractProviderConfigurationHolder($configurationHolder);

        return $adobeConnectWebinarSCO;
    }

    private function convertRoomScoMeetingToRoomSeminar(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        //dump('ACTION_CONVERT_ROOM_SCO_MEETING_TO_ROOM_SEMINAR');
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_SCO_UPDATE,
                    'folder-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                    'type' => 'seminarsession',
                    'name' => $adobeConnectWebinarSCO->getName(),
                    'session' => $this->login($configurationHolder),
                ],
            ]);

        //$crawler = new Crawler($response->getContent());
        $crawler = $this->handleResponse($response, self::ACTION_CONVERT_ROOM_SCO_MEETING_TO_ROOM_SEMINAR);

        $adobeConnectWebinarSCO->setSeminarScoIdentifier((int) $crawler->filterXPath('//results/sco')->attr('sco-id'));
        //$adobeConnectWebinarSCO->setIsSeminar($node->filterXPath('//is-seminar')->text() === 'true');

        $adobeConnectWebinarSCO->setAbstractProviderConfigurationHolder($configurationHolder);

        return $adobeConnectWebinarSCO;
    }

    private function createSessionSeminar(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        //dump('ACTION_CREATE_SESSION_SEMINAR');
        $dateStart = (clone $adobeConnectWebinarSCO->getDateStart())->setTimezone(new \DateTimeZone($adobeConnectWebinarSCO->getClient()->getTimezone()));
        $dateEnd = (clone $adobeConnectWebinarSCO->getDateEnd())->setTimezone(new \DateTimeZone($adobeConnectWebinarSCO->getClient()->getTimezone()));
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_CREATE_SESSION_SEMINAR,
                    'sco-id' => $adobeConnectWebinarSCO->getSeminarScoIdentifier(),
                    'source-sco-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                    'parent-acl-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                    'date-begin' => $dateStart->format('c'),
                    'date-end' => $dateEnd->format('c'),
                    'session' => $this->login($configurationHolder),
                ],
            ]);

        //$crawler = new Crawler($response->getContent());
        $crawler = $this->handleResponse($response, self::ACTION_CREATE_SESSION_SEMINAR);
        //dump($crawler->filterXPath('//results')->text());

        return $adobeConnectWebinarSCO;
    }

    private function changeNbrMaxUsersToWebinar(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        //dump('ACTION_SET_NBR_MAX_USERS_TO_WEBINAR');
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_ACL_FIELD_UPDATE,
                'acl-id' => $adobeConnectWebinarSCO->getSeminarScoIdentifier(),
                'field-id' => 'seminar-expected-load',
                'value' => $configurationHolder->getMaxParticipantsWebinar(),
                'session' => $this->login($configurationHolder),
            ],
        ]);

        //$crawler = new Crawler($response->getContent());
        $crawler = $this->handleResponse($response, self::ACTION_SET_NBR_MAX_USERS_TO_WEBINAR);
        //dump($crawler->filterXPath('//results')->text());

        return $adobeConnectWebinarSCO;
    }

    private function changeAccessTypeToEvent(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        //dump('ACTION_CHANGE_ACCESS_TYPE_TO_EVENT');
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_PERMISSIONS_UPDATE,
                'acl-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                'principal-id' => 'public-access',
                'permission-id' => AdobeConnectWebinarSCO::PERMISSION_VERB_VIEW_HIDDEN,
                'session' => $this->login($configurationHolder),
            ],
        ]);

        //$crawler = new Crawler($response->getContent());
        $crawler = $this->handleResponse($response, self::ACTION_CHANGE_ACCESS_TYPE_TO_EVENT);
        //dump($crawler->filterXPath('//results')->text());

        return $adobeConnectWebinarSCO;
    }

    private function changeProfilAudioToWebinar(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        //dump('ACTION_CHANGE_AUDIO_PROFIL');
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_ACL_FIELD_UPDATE,
                'sco-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                'field-id' => 'telephony-profil',
                'value' => 0,   //TODO Récupérer le sco-id du profil audio
                'session' => $this->login($configurationHolder),
            ],
        ]);

        //$crawler = new Crawler($response->getContent());
        $crawler = $this->handleResponse($response, self::ACTION_CHANGE_AUDIO_PROFIL);
        //dump($crawler->filterXPath('//results')->text());

        return $adobeConnectWebinarSCO;
    }

    private function linkLicenceToEvent(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        //dump('ACTION_LINK_LICENCE_TO_EVENT');
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_PERMISSIONS_UPDATE,
                'acl-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                'principal-id' => '', //TODO Trouver le sco-id de la licence
                'permission-id' => AdobeConnectWebinarSCO::PERMISSION_VERB_HOST,
                'session' => $this->login($configurationHolder),
            ],
        ]);

        //$crawler = new Crawler($response->getContent());
        $crawler = $this->handleResponse($response, self::ACTION_LINK_LICENCE_TO_EVENT);
        //dump($crawler->filterXPath('//results')->text());

        return $adobeConnectWebinarSCO;
    }

    private function deleteWebinar(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        //dump('ACTION_DELETE_WEBINAR');
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_SCO_DELETE,
                'sco-id' => $adobeConnectWebinarSCO->getParentScoIdentifier(),
                'session' => $this->login($configurationHolder),
            ],
        ]);

        //$crawler = new Crawler($response->getContent());
        $crawler = $this->handleResponse($response, self::ACTION_DELETE_WEBINAR);
        //dump($crawler->filterXPath('//results')->text());

        return $adobeConnectWebinarSCO;
    }

    /*private function updateRoomWebinar(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        dump('ACTION_SEMINAR_SESSION_SCO_UPDATE');
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_SEMINAR_SESSION_SCO_UPDATE,
                'sco-id' => $adobeConnectWebinarSCO->getSeminarScoIdentifier(),
                'source-sco-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                'parent-acl-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                //TODO Ajouter ici les champs que l'on souhaite modifier
                'session' => $this->login($configurationHolder),
            ],
        ]);

        //$crawler = new Crawler($response->getContent());
        $crawler = $this->handleResponse($response, self::ACTION_SEMINAR_SESSION_SCO_UPDATE);
        dump($crawler->filterXPath('//results')->text());

        return $adobeConnectWebinarSCO;
    }*/

    private function updateRoomWebinar(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        //dump('ACTION_UPDATE_ROOM_WEBINAR');
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_SCO_UPDATE,
                'sco-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                //TODO Ajouter ici les champs que l'on souhaite modifier par exemple "name"
                'session' => $this->login($configurationHolder),
            ],
        ]);

        //$crawler = new Crawler($response->getContent());
        $crawler = $this->handleResponse($response, self::ACTION_UPDATE_ROOM_WEBINAR);
        //dump($crawler->filterXPath('//results')->text());

        return $adobeConnectWebinarSCO;
    }

    private function updateSessionWebinar(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        //dump('ACTION_UPDATE_SESSION_WEBINAR');
        /*$response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_SCO_UPDATE,
                'sco-id' => $adobeConnectWebinarSCO->getSeminarScoIdentifier(),
                //TODO Ajouter ici les champs que l'on souhaite modifier par exemple "name", "date-begin", "date-end", "description", "lang"
                'session' => $this->login($configurationHolder),
            ],
        ]);

        //$crawler = new Crawler($response->getContent());
        $crawler = $this->handleResponse($response, self::ACTION_UPDATE_SESSION_WEBINAR);
        //dump($crawler->filterXPath('//results')->text());

        return $adobeConnectWebinarSCO;*/

        // According to the Adobe connect restriction. @see: https://helpx.adobe.com/adobe-connect/webservices/common-xml-elements-attributes.html#lang
        $lang = (!empty($adobeConnectWebinarSCO->getLanguage()) && strlen($adobeConnectWebinarSCO->getLanguage()) >= 2)
            ? strtolower(substr($adobeConnectWebinarSCO->getLanguage(), 0, 2))
            : null;

        $crawler = $this->handleResponse(
            $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_UPDATE_SESSION_WEBINAR,
                    'session' => $this->login($configurationHolder),
                    'sco-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                    'parent-acl-id' => $adobeConnectWebinarSCO->getSeminarScoIdentifier(),
                    'source-sco-id' => $adobeConnectWebinarSCO->getSeminarScoIdentifier(),
                    'name' => $adobeConnectWebinarSCO->getName(),
                    'date-begin' => $adobeConnectWebinarSCO->getDateStart()->format('c'),
                    'date-end' => $adobeConnectWebinarSCO->getDateEnd()->format('c'),
                    //'url-path' => $adobeConnectWebinarSCO->getUrlPath(),
                    //'lang' => $lang,
                    // In case of an update (unique SCO identifier already exists), don't send the folder identifier
                    //'folder-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier() ? null : $adobeConnectWebinarSCO->getParentScoIdentifier(),
                    // In case of a create send source model uniq identifier
                    //'source-sco-id' => $adobeConnectWebinarSCO->getRoomModel(),
                    //'type' => $adobeConnectWebinarSCO->getType(),
                ],
            ]),
            self::ACTION_UPDATE_SESSION_WEBINAR
        );

        if ($adobeConnectWebinarSCO->getUniqueScoIdentifier() === null) {
            $adobeConnectWebinarSCO->setUniqueScoIdentifier((int) $crawler->filterXPath('//results/sco')->attr('sco-id'));
            $adobeConnectWebinarSCO->setUrlPath($crawler->filterXPath('//results/sco/url-path')->text());
            $adobeConnectWebinarSCO->setCreatedAt(new DateTime($crawler->filterXPath('//results/sco/date-created')->text()));
            $adobeConnectWebinarSCO->setUpdatedAt(new DateTime($crawler->filterXPath('//results/sco/date-modified')->text()));
        }

        $adobeConnectWebinarSCO->setAbstractProviderConfigurationHolder($configurationHolder);

        return $adobeConnectWebinarSCO;
    }

    private function reportMeetingSession(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        //dump('ACTION_REPORT_MEETING_SESSION');
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_REPORT_MEETING_SESSION,
                'sco-id' => $adobeConnectWebinarSCO->getSeminarScoIdentifier(),
                'session' => $this->login($configurationHolder),
            ],
        ]);

        //$crawler = new Crawler($response->getContent());
        $crawler = $this->handleResponse($response, self::ACTION_REPORT_MEETING_SESSION);
        //dump($crawler->filterXPath('//results')->text());

        return $adobeConnectWebinarSCO;
    }

    private function reportEventParticipantsCompleteInformation(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        //dump('ACTION_REPORT_EVENT_PARTICIPANTS_COMPLETE_INFORMATION');
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_REPORT_EVENT_PARTICIPANTS_COMPLETE_INFORMATION,
                'sco-id' => $adobeConnectWebinarSCO->getSeminarScoIdentifier(),
                'session' => $this->login($configurationHolder),
            ],
        ]);

        //$crawler = new Crawler($response->getContent());
        $crawler = $this->handleResponse($response, self::ACTION_REPORT_EVENT_PARTICIPANTS_COMPLETE_INFORMATION);
        //dump($crawler->filterXPath('//results')->text());

        return $adobeConnectWebinarSCO;
    }

    private function getSCOContents(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $rootSco): array
    {
        if ($rootSco->getUniqueScoIdentifier() === null) {
            throw new InvalidArgumentException('SCO identifier cannot be null.');
        }

        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_SCO_CONTENTS,
                'sco-id' => $rootSco->getUniqueScoIdentifier(),
                'session' => $this->login($configurationHolder),
            ],
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_SCO_CONTENTS);

        $fetchedSco = [];
        $crawler->filterXPath('//results/scos/sco')->each(function (Crawler $node) use (&$rootSco, &$fetchedSco, $configurationHolder) {
            $childScoIdentifier = (int) $node->attr('sco-id');

            if (null === $sco = $this->adobeConnectWebinarSCORepository->findOneBy(['uniqueScoIdentifier' => $childScoIdentifier])) {
                $sco = new AdobeConnectWebinarSCO($configurationHolder);
                $sco->setUniqueScoIdentifier($childScoIdentifier);
            }

            $sco->setType($node->attr('type'))
                ->setDuration((int) $node->attr('duration'))
                ->setName($node->filterXPath('//name')->text())
                ->setUrlPath($node->filterXPath('//url-path')->text())
                ->setIsSeminar($node->filterXPath('//is-seminar')->text() === 'true')
                ->setCreatedAt(new DateTime($node->filterXPath('//date-created')->text()))
                ->setUpdatedAt(new DateTime($node->filterXPath('//date-modified')->text()));

            if ($sco->getType() === AdobeConnectWebinarSCO::TYPE_MEETING) {
                $dateStartNode = $node->filterXPath('//date-begin');
                if (null !== $dateStart = $dateStartNode->count() ? $dateStartNode->text() : null) {
                    $sco->setDateStart(new DateTime($dateStart));
                }

                $dateEndNode = $node->filterXPath('//date-end');
                if (null !== $dateEnd = $dateEndNode->count() ? $dateEndNode->text() : null) {
                    $sco->setDateEnd(new DateTime($dateEnd));
                }
            }

            $fetchedSco[] = $sco;
        });

        return $fetchedSco;
    }

    private function principalsDelete(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarPrincipal $adobeConnectWebinarPrincipal): AdobeConnectWebinarPrincipal
    {
        $this->handleResponse(
            $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_PRINCIPALS_DELETE,
                    'session' => $this->login($configurationHolder),
                    'principal-id' => $adobeConnectWebinarPrincipal->getPrincipalIdentifier(),
                ],
            ]),
            self::ACTION_PRINCIPALS_DELETE
        );

        return $adobeConnectWebinarPrincipal;
    }

    private function getConnectedUserdata(AdobeConnectWebinarConfigurationHolder $configurationHolder): AdobeConnectWebinarCommonInfo
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_COMMON_INFO,
                'session' => $this->login($configurationHolder),
            ],
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_COMMON_INFO);

        return new AdobeConnectWebinarCommonInfo(
            $crawler->filterXPath('//results/common/user')->attr('user-id'),
            $crawler->filterXPath('//results/common/user/login')->text()
        );
    }

    private function authenticatePrincipal(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO, AdobeConnectWebinarPrincipal $adobeConnectWebinarPrincipal): string
    {
        //dump($adobeConnectWebinarPrincipal->getId().' > '.$adobeConnectWebinarPrincipal->getEmail().' > '.$adobeConnectWebinarPrincipal->getPassword());
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_LOGIN,
                'login' => $adobeConnectWebinarPrincipal->getEmail(),
                'password' => $adobeConnectWebinarPrincipal->getPassword(),
            ],
        ]);
        $this->handleResponse($response, self::ACTION_LOGIN);

        $url = $this->buildRoomUrlFromResponse($response, $configurationHolder, $adobeConnectWebinarSCO);

        return $url;
    }

    private function authenticateLicence(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): string
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_LOGIN,
                'login' => $configurationHolder->getUsername(),
                'password' => $configurationHolder->getPassword(),
            ],
        ]);
        $this->handleResponse($response, self::ACTION_LOGIN);

        $url = $this->buildRoomUrlFromResponse($response, $configurationHolder, $adobeConnectWebinarSCO);

        return $url;
    }

    private function authenticateConfigurationHolder(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): string
    {
        $redisKey = self::REDIS_AUTH_COOKIE_PREFIX.'login-configuration-holder'.$configurationHolder->getSubdomain().$configurationHolder->getUsername();

        if ($this->redisClient->exists($redisKey)) {
            return (string) $this->redisClient->get($redisKey);
        }

        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_LOGIN,
                'login' => $configurationHolder->getUsername(),
                'password' => $configurationHolder->getPassword(),
            ],
        ]);
        $this->handleResponse($response, self::ACTION_LOGIN);

        $url = $this->buildRoomUrlFromResponse($response, $configurationHolder, $adobeConnectWebinarSCO);

        $this->redisClient->set($redisKey, $url, self::REDIS_AUTH_COOKIE_TIMEOUT);

        return $url;
    }

    private function updatePrincipalPassword(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarPrincipal $adobeConnectWebinarPrincipal): void
    {
        $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_UPDATE_PRINCIPAL_PASSWORD,
                'user-id' => $adobeConnectWebinarPrincipal->getPrincipalIdentifier(),
                'password' => $adobeConnectWebinarPrincipal->getPassword(),
                'password-verify' => $adobeConnectWebinarPrincipal->getPassword(),
                'session' => $this->login($configurationHolder),
            ],
        ]);
    }

    /*private function getUserWebinarFolder(AdobeConnectWebinarConfigurationHolder $configurationHolder, string $rootFolderId): int
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_SCO_SHORTCUTS,
                'sco-id' => $rootFolderId,
                'session' => $this->login($configurationHolder),
            ],
        ]);

        $userWebinarScoId = null;

        $crawler = $this->handleResponse($response, self::ACTION_SCO_USER_WEBINAR_FOLDER);
        $scos = $crawler->filterXPath('//results/scos');
            ->each(function (Crawler $node) use (&$userWebinarScoId) {
                $userWebinarScoId = $node->attr('sco-id');
            });

        if (is_null($userWebinarScoId)) {
            $exception = new AbstractProviderException("Unable to find user webinar folder in root sco-id '$rootFolderId'");
            $this->logger->error($exception, ['rootFolderId' => $rootFolderId]);
            throw new AbstractProviderException("Unable to find user webinar folder in root sco-id '$rootFolderId'");
        }

        return $userWebinarScoId;
    }*/

    private function getRootFolder(AdobeConnectWebinarConfigurationHolder $configurationHolder, string $rootFolderType): int
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_SCO_SHORTCUTS,
                'session' => $this->login($configurationHolder),
            ],
        ]);

        $rootScoId = null;

        $crawler = $this->handleResponse($response, self::ACTION_SCO_SHORTCUTS);
        $crawler->filterXPath('//results/shortcuts/sco')
                ->each(function (Crawler $node) use (&$rootFolderType, &$rootScoId) {
                    $scoType = $node->attr('type');

                    if ($scoType === $rootFolderType) {
                        $rootScoId = (int) $node->attr('sco-id');
                    }
                });

        if (is_null($rootScoId)) {
            $exception = new AbstractProviderException("Unable to find root folder named '$rootFolderType'");
            $this->logger->error($exception, ['rootFolderType' => $rootFolderType]);
            throw new AbstractProviderException("Unable to find root folder named '$rootFolderType'");
        }

        return $rootScoId;
    }

    private function getScoNameListFromParentSco(AdobeConnectWebinarConfigurationHolder $configurationHolder, int $parentScoId): array
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_SCO_CONTENTS,
                'sco-id' => $parentScoId,
                'session' => $this->login($configurationHolder),
            ],
        ]);

        $scoFolders = [];

        $crawler = $this->handleResponse($response, self::ACTION_SCO_CONTENTS);
        $crawler->filterXPath('//results/scos/sco')
                ->each(function (Crawler $node) use (&$scoFolders) {
                    $scoId = $node->attr('sco-id');
                    $scoFolders[$scoId] = [
                            'uniq_identifier' => (int) $scoId,
                            'name' => $node->filterXPath('//name')->text(),
                        ];
                });

        asort($scoFolders);

        return $scoFolders;
    }

    private function isFolderExist(AdobeConnectWebinarConfigurationHolder $configurationHolder, int $folderSCOParentId, string $name): ?int
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_SCO_CONTENTS,
                'sco-id' => $folderSCOParentId,
                'session' => $this->login($configurationHolder),
            ],
        ]);

        $fetchedSco = null;
        $crawler = $this->handleResponse($response, self::ACTION_SCO_CONTENTS);
        $crawler->filterXPath('//results/scos/sco')
                ->each(function (Crawler $node) use (&$rootSco, &$fetchedSco, &$name) {
                    if ($node->filterXPath('//name')->text() === $name) {
                        $fetchedSco = (int) $node->attr('sco-id');
                    }
                });

        return $fetchedSco;
    }

    private function createFolder(AdobeConnectWebinarConfigurationHolder $configurationHolder, int $folderSCOParentId, string $name): ?int
    {
        $result = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_SCO_UPDATE,
                'folder-id' => $folderSCOParentId,
                'type' => 'folder',
                'name' => $name,
                'session' => $this->login($configurationHolder),
            ],
        ]);
        $fetchedScoId = null;
        $crawler = $this->handleResponse($result, self::ACTION_SCO_UPDATE);
        $crawler->filterXPath('//results/sco')
                ->each(function (Crawler $node) use (&$rootSco, &$fetchedScoId) {
                    $fetchedScoId = (int) $node->attr('sco-id');
                });

        return $fetchedScoId;
    }

    private function setScoTelephonyProfile(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'session' => $this->login($configurationHolder),
                'action' => 'acl-field-update',
                'acl-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                'field-id' => 'telephony-profile',
                'value' => $adobeConnectWebinarSCO->getProviderAudio()?->getProfileIdentifier(),
            ],
        ]);

        return $adobeConnectWebinarSCO;
    }

    private function updateScoDefaultPermissions(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        // update distant permission
        // https://helpx.adobe.com/adobe-connect/webservices/permissions-update.html#permissions_update
        // https://helpx.adobe.com/adobe-connect/webservices/common-xml-elements-attributes.html#permission_id
        $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_PERMISSIONS_UPDATE,
                'session' => $this->login($configurationHolder),
                'acl-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                'principal-id' => 'public-access',
                'permission-id' => $adobeConnectWebinarSCO->getAdmittedSession(),
            ],
        ]);

        return $adobeConnectWebinarSCO;
    }

    /*private function deleteSco(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        $this->handleResponse(
            $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_SCO_DELETE,
                    'session' => $this->login($configurationHolder),
                    'sco-id' => $adobeConnectWebinarSCO->getParentScoIdentifier(),
                ],
            ]),
            self::ACTION_SCO_DELETE
        );

        return $adobeConnectWebinarSCO;
    }*/

    public function findOrCreateSco(
        AdobeConnectWebinarConfigurationHolder $configurationHolder,
        int $scoRootId,
        string $targetName
    ): ?int {
        $data = $this->getScoNameListFromParentSco($configurationHolder, $scoRootId);
        foreach ($data as $id => $sco) {
            if ($sco['name'] === $targetName) {
                return $id;
            }
        }

        return $this->createFolder($configurationHolder, $scoRootId, $targetName);
    }

    public function getLsTemplateFolder(
        AdobeConnectWebinarConfigurationHolder $configurationHolder
    ): ?int {
        $webinarScoId = $this->getRootFolder($configurationHolder, self::SCO_TYPE_SEMINARS);
        $liveSessionScoId = $this->findOrCreateSco($configurationHolder, $webinarScoId, self::LIVESESSION_DEFAULT_FOLDER);

        return $this->findOrCreateSco($configurationHolder, $liveSessionScoId, self::LIVESESSION_DEFAULT_TEMPLATE_FOLDER);
    }

    private function updateUrlPath(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO, string $newUrlPath): AdobeConnectWebinarSCO
    {
        $this->handleResponse(
            $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_UPDATE_URL,
                    'session' => $this->login($configurationHolder),
                    'sco-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                    'url-path' => $newUrlPath,
                ],
            ]),
            self::ACTION_UPDATE_URL
        );
        $adobeConnectWebinarSCO->setUrlPath($newUrlPath);

        return $adobeConnectWebinarSCO;
    }

    private function permissionsInfo(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): array
    {
        if ($adobeConnectWebinarSCO->getUniqueScoIdentifier() === null) {
            throw new InvalidArgumentException('SCO identifier cannot be null');
        }

        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_PERMISSIONS_INFO,
                'acl-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                'session' => $this->login($configurationHolder),
            ],
        ]);
        $crawler = $this->handleResponse($response, self::ACTION_PERMISSIONS_INFO);

        $createdPrincipals = $this->adobeConnectWebinarPrincipalFactory->createPrincipalsFromCrawler($configurationHolder, $crawler->filterXPath('//results/permissions/principal'));
        foreach ($createdPrincipals as $principal) {
            $adobeConnectWebinarSCO->addPrincipal($principal);
        }

        return $createdPrincipals;
    }

    private function permissionsUpdate(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO, AdobeConnectWebinarPrincipal $adobeConnectWebinarPrincipal, string $permission): AdobeConnectWebinarPrincipal
    {
        if (!in_array($permission, $this->getAvailablePermissionIdentifiers())) {
            throw new InvalidArgumentException('Permission verb is not valid. Refer to https://helpx.adobe.com/adobe-connect/webservices/common-xml-elements-attributes.html#permission_id.');
        }

        $test = [
            'query' => [
                'action' => self::ACTION_PERMISSIONS_UPDATE,
                'session' => $this->login($configurationHolder),
                'acl-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                'principal-id' => $adobeConnectWebinarPrincipal->getPrincipalIdentifier(),
                'permission-id' => $permission,
            ],
        ];
        //dump($test);

        $this->handleResponse(
            $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_PERMISSIONS_UPDATE,
                    'session' => $this->login($configurationHolder),
                    'acl-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                    'principal-id' => $adobeConnectWebinarPrincipal->getPrincipalIdentifier(),
                    'permission-id' => $permission,
                ],
            ]),
            self::ACTION_PERMISSIONS_UPDATE
        );

        return $adobeConnectWebinarPrincipal;
    }

    private function permissionsReset(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): AdobeConnectWebinarSCO
    {
        if ($adobeConnectWebinarSCO->getUniqueScoIdentifier() === null) {
            throw new InvalidArgumentException('SCO identifier cannot be null');
        }

        $this->handleResponse(
            $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_PERMISSIONS_RESET,
                    'session' => $this->login($configurationHolder),
                    'acl-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
                ],
            ]),
            self::ACTION_PERMISSIONS_RESET
        );

        return $adobeConnectWebinarSCO;
    }

    private function principalUpdate(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarPrincipal $adobeConnectWebinarPrincipal): AdobeConnectWebinarPrincipal
    {
        $user = $adobeConnectWebinarPrincipal->getUser();

        $crawler = $this->handleResponse(
            $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
                'query' => [
                    'action' => self::ACTION_PRINCIPAL_UPDATE,
                    'session' => $this->login($configurationHolder),
                    'principal-id' => $adobeConnectWebinarPrincipal->getPrincipalIdentifier(),
                    'login' => $user->getEmail(),
                    'first-name' => $user->getFirstName(),
                    'last-name' => $user->getLastName(),
                    // The password has to be set only when we're creating a new principal, not updating one
                    'password' => $adobeConnectWebinarPrincipal->getPrincipalIdentifier() ? null : $adobeConnectWebinarPrincipal->getPassword(),
                    // The type has to be set only when we're creating a new principal, not updating one
                    'type' => $adobeConnectWebinarPrincipal->getPrincipalIdentifier() ? null : 'user',
                    'has-children' => false,
                ],
            ]),
            self::ACTION_PRINCIPAL_UPDATE
        );

        if ($adobeConnectWebinarPrincipal->getPrincipalIdentifier() === null) {
            $node = $crawler->filterXPath('//results/principal');
            $adobeConnectWebinarPrincipal->setPrincipalIdentifier((int) $node->attr('principal-id'))
                ->setName($node->filterXPath('//name')->text());
        }

        $adobeConnectWebinarPrincipal->setName($user->getFirstName().' '.$user->getLastName());

        return $adobeConnectWebinarPrincipal;
    }

    private function principalList(AdobeConnectWebinarConfigurationHolder $configurationHolder, bool $asAGroup = false, array $options = []): array
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => array_merge([
                'action' => self::ACTION_PRINCIPAL_LIST,
                'session' => $this->login($configurationHolder),
            ], $options),
        ]);

        //var_dump($configurationHolder->getApiUrl().'?action='.self::ACTION_PRINCIPAL_LIST.'&'.key($options).'='.current($options));
        $crawler = $this->handleResponse($response, self::ACTION_PRINCIPAL_LIST);

        return (!$asAGroup)
            ? $this->adobeConnectWebinarPrincipalFactory->createPrincipalsFromCrawler($configurationHolder, $crawler->filterXPath('//results/principal-list/principal'))
            : $this->adobeConnectWebinarPrincipalFactory->createPrincipalsGroupFromCrawler($configurationHolder, $crawler->filterXPath('//results/principal-list/principal'));
    }

    private function telephonyProfileList(AdobeConnectWebinarConfigurationHolder $configurationHolder, ?AdobeConnectWebinarPrincipal $adobeConnectWebinarPrincipal = null): array
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_TELEPHONY_PROFILE_LIST,
                'session' => $this->login($configurationHolder),
                'principal-id' => $adobeConnectWebinarPrincipal?->getPrincipalIdentifier(),
            ],
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_TELEPHONY_PROFILE_LIST);
        $createdProfiles = [];

        $crawler->filterXPath('//results/telephony-profiles/profile')->each(function (Crawler $node) use (&$createdProfiles, $adobeConnectWebinarPrincipal, $configurationHolder) {
            $profileIdentifier = (int) $node->attr('profile-id');
            if (null === $telephonyProfile = $this->adobeConnectWebinarTelephonyProfileRepository
                    ->findOneBy([
                        'profileIdentifier' => $profileIdentifier,
                        'configurationHolder' => $configurationHolder,
                    ])
            ) {
                $telephonyProfile = new AdobeConnectWebinarTelephonyProfile();
                $telephonyProfile->setProfileIdentifier($profileIdentifier);
                $createdProfiles[] = $telephonyProfile;
            }

            $telephonyProfile
                ->setConnectionType(
                    str_replace(' ', ' ', $node->filterXPath('//name')->text())
                )
                ->setName($node->filterXPath('//profile-name')->text());

            $adobeConnectWebinarPrincipal?->addTelephonyProfile($telephonyProfile);
        });

        foreach ($createdProfiles as $telephonyProfile) {
            $this->telephonyProfileInfo($configurationHolder, $telephonyProfile);
        }

        return $createdProfiles;
    }

    private function telephonyProfileInfo(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarTelephonyProfile $adobeConnectWebinarTelephonyProfile): AdobeConnectWebinarTelephonyProfile
    {
        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_TELEPHONY_PROFILE_INFO,
                'session' => $this->login($configurationHolder),
                'profile-id' => $adobeConnectWebinarTelephonyProfile->getProfileIdentifier(),
            ],
        ]);

        $crawler = $this->handleResponse($response, self::ACTION_TELEPHONY_PROFILE_INFO);

        $node = $crawler->filterXPath('//results/telephony-profile-fields');
        switch ($adobeConnectWebinarTelephonyProfile->getConnectionType()) {
            case AbstractProviderAudio::AUDIO_INTERCALL:
                $phoneNumber = $node->matches('telephony-profile-fields x-tel-intercall-uv-conference-number')
                    ? $node->filterXPath('//x-tel-intercall-uv-conference-number')->text()
                    : null;
                $animatorCode = $node->matches('telephony-profile-fields x-tel-intercall-leader-pin')
                    ? $node->filterXPath('//x-tel-intercall-leader-pin')->text()
                    : null;
                $participantCode = $node->matches('telephony-profile-fields x-tel-intercall-participant-code')
                    ? $node->filterXPath('//x-tel-intercall-participant-code')->text()
                    : null;
                $meetingOneConferenceId = null;
                break;
            case AbstractProviderAudio::AUDIO_PGI:
            case AbstractProviderAudio::AUDIO_PGI_EMEA:
                $phoneNumber = $node->matches('telephony-profile-fields x-tel-premiere-uv-conference-number')
                    ? $node->filterXPath('//x-tel-premiere-uv-conference-number')->text()
                    : null;
                $animatorCode = $node->matches('telephony-profile-fields x-tel-premiere-moderator-code')
                    ? $node->filterXPath('//x-tel-premiere-moderator-code')->text()
                    : null;
                $participantCode = $node->matches('telephony-profile-fields x-tel-premiere-participant-code')
                    ? $node->filterXPath('//x-tel-premiere-participant-code')->text()
                    : null;
                $meetingOneConferenceId = null;
                break;
            case AbstractProviderAudio::AUDIO_MEETINGONE_EMEA:
                $phoneNumber = null;
                $participantCode = null;
                $animatorCode = $node->matches('telephony-profile-fields x-tel-meetingone-host-pin')
                    ? $node->filterXPath('//x-tel-meetingone-host-pin')->text()
                    : null;
                $meetingOneConferenceId = $node->matches('telephony-profile-fields x-tel-meetingone-conference-id')
                    ? $node->filterXPath('//x-tel-meetingone-conference-id')->text()
                    : null;
                break;
            case AbstractProviderAudio::AUDIO_MEETINGONE_NA:
                $phoneNumber = null;
                $participantCode = null;
                $animatorCode = $node->matches('telephony-profile-fields x-tel-meetingone-host-pin')
                    ? $node->filterXPath('//x-tel-meetingone-host-pin')->text()
                    : null;
                $meetingOneConferenceId = $node->matches('telephony-profile-fields x-tel-meetingone-conference-id')
                    ? $node->filterXPath('//x-tel-meetingone-conference-id')->text()
                    : null;
                break;
            default:
                $phoneNumber = $animatorCode = $participantCode = $meetingOneConferenceId = null;
                break;
        }

        $adobeConnectWebinarTelephonyProfile
            ->setAdobeDefaultPhoneNumber($phoneNumber)
            ->setCodeAnimator($animatorCode)
            ->setCodeParticipant($participantCode)
            ->setMeetingOneConferenceId($meetingOneConferenceId);

        return $adobeConnectWebinarTelephonyProfile;
    }

    private function reportMeetingAttendance(AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): array
    {
        if ($adobeConnectWebinarSCO->getUniqueScoIdentifier() === null) {
            throw new InvalidArgumentException('SCO identifier cannot be null');
        }

        $response = $this->httpClient->request('GET', $configurationHolder->getApiUrl(), [
            'query' => [
                'action' => self::ACTION_REPORT_MEETING_ATTENDANCE,
                'session' => $this->login($configurationHolder),
                'sco-id' => $adobeConnectWebinarSCO->getUniqueScoIdentifier(),
            ],
        ]);
        $crawler = $this->handleResponse($response, self::ACTION_REPORT_MEETING_ATTENDANCE);

        $valuesAttendees = [];
        $crawler->filterXPath('//results/report-meeting-attendance/row')->each(function (Crawler $node) use (&$valuesAttendees, $adobeConnectWebinarSCO, $configurationHolder) {
            $principalIdentifier = (int) $node->attr('principal-id');

            if (null !== $adobeConnectWebinarPrincipal = $this->adobeConnectWebinarPrincipalRepository->getPrincipalWithConfigurationHolder($principalIdentifier, $configurationHolder)) {
                $sessionStart = new DateTimeImmutable($node->filterXPath('//date-created')->text());
                $sessionEnd = new DateTimeImmutable($node->filterXPath('//date-end')->text());

                if ($this->participantSessionRoleRepository->isRegisteredOntoSession($adobeConnectWebinarSCO->getId(), $adobeConnectWebinarPrincipal->getId())) {
                    $valuesAttendees[] = [
                        'principal-id' => $principalIdentifier,
                        'sco-id' => (int) $node->attr('sco-id'),
                        'asset-id' => (int) $node->attr('asset-id'),
                        'login' => strtolower($node->filterXPath('//login')->text()),
                        'session-name' => $node->filterXPath('//session-name')->text(),
                        'sco-name' => $node->filterXPath('//sco-name')->text(),
                        'date-created' => $sessionStart,
                        'date-end' => $sessionEnd,
                        'participant-name' => $node->filterXPath('//participant-name')->text(),
                    ];
                }
            }
        });

        return $valuesAttendees;
    }

    private function getAvailablePermissionIdentifiers(): array
    {
        return [
            AdobeConnectWebinarSCO::PERMISSION_VERB_VIEW,
            AdobeConnectWebinarSCO::PERMISSION_VERB_HOST,
            AdobeConnectWebinarSCO::PERMISSION_VERB_MINI_HOST,
            AdobeConnectWebinarSCO::PERMISSION_VERB_REMOVE,
            AdobeConnectWebinarSCO::PERMISSION_VERB_PUBLISH,
            AdobeConnectWebinarSCO::PERMISSION_VERB_MANAGE,
            AdobeConnectWebinarSCO::PERMISSION_VERB_DENIED,
        ];
    }

    private function buildRoomUrlFromResponse(ResponseInterface $response, AdobeConnectWebinarConfigurationHolder $configurationHolder, AdobeConnectWebinarSCO $adobeConnectWebinarSCO): string
    {
        $breezeSessionCookie = current(explode(';', current($response->getHeaders()['set-cookie'])));
        $session = explode('=', $breezeSessionCookie)[1];

        return "https://{$configurationHolder->getDomain()}{$adobeConnectWebinarSCO->getUrlPath()}?session=$session";
    }
}
