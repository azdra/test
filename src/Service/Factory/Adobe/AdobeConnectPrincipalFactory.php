<?php

namespace App\Service\Factory\Adobe;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\User;
use App\Repository\AdobeConnectPrincipalRepository;
use Symfony\Component\DomCrawler\Crawler;

final class AdobeConnectPrincipalFactory
{
    private AdobeConnectPrincipalRepository $adobeConnectPrincipalRepository;

    public function __construct(AdobeConnectPrincipalRepository $adobeConnectPrincipalRepository)
    {
        $this->adobeConnectPrincipalRepository = $adobeConnectPrincipalRepository;
    }

    public function createPrincipalsFromCrawler(AdobeConnectConfigurationHolder $configurationHolder, Crawler $crawler): array
    {
        $principals = [];
        $crawler->each(function (Crawler $node) use (&$principals, $configurationHolder) {
            if ($node->attr('has-children') === 'true') {
                return;
            }

            $principalIdentifier = $node->attr('principal-id');
            if (!is_numeric($principalIdentifier)) {
                return;
            }

            if (null === $principal = $this->adobeConnectPrincipalRepository->findOneBy(['principalIdentifier' => $principalIdentifier])) {
                $principal = (new AdobeConnectPrincipal($configurationHolder))
                    ->setUser(new User());

            }

            $user = $principal->getUser();

            $principal
                ->setPrincipalIdentifier((int) $node->attr('principal-id'))
                ->setName($node->filterXPath('//name')->text());

            if (!empty($user)) {
                $user->setEmail($node->filterXPath('//login')->text());
            }

            $principals[] = $principal;
        });

        return $principals;
    }
    
    public function createPrincipalsGroupFromCrawler(AdobeConnectConfigurationHolder $configurationHolder, Crawler $crawler): array
    {
        $principals = [];
        $crawler->each(function (Crawler $node) use (&$principals, $configurationHolder) {
            if ($node->attr('has-children') === 'false') {
                return;
            }
            
            $principalIdentifier = $node->attr('principal-id');
            if (!is_numeric($principalIdentifier)) {
                return;
            }
            
            $principal = (new AdobeConnectPrincipal($configurationHolder))
                ->setPrincipalIdentifier((int) $principalIdentifier)
                ->setName($node->filterXPath('//name')->text());
            
            $principals[] = $principal;
        });
        
        return $principals;
    }
}
