<?php

namespace App\Service\Factory\Adobe;

use App\Entity\Adobe\AdobeConnectWebinarPrincipal;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\User;
use App\Repository\AdobeConnectWebinarPrincipalRepository;
use Symfony\Component\DomCrawler\Crawler;

final class AdobeConnectWebinarPrincipalFactory
{
    private AdobeConnectWebinarPrincipalRepository $adobeConnectWebinarPrincipalRepository;

    public function __construct(AdobeConnectWebinarPrincipalRepository $adobeConnectWebinarPrincipalRepository)
    {
        $this->adobeConnectWebinarPrincipalRepository = $adobeConnectWebinarPrincipalRepository;
    }

    public function createPrincipalsFromCrawler(AdobeConnectWebinarConfigurationHolder $configurationHolder, Crawler $crawler): array
    {
        $principals = [];
        $crawler->each(function (Crawler $node) use (&$principals, $configurationHolder) {
            if ($node->attr('has-children') === 'true') {
                return;
            }

            $principalIdentifier = $node->attr('principal-id');
            if (!is_numeric($principalIdentifier)) {
                return;
            }

            if (null === $principal = $this->adobeConnectWebinarPrincipalRepository->getPrincipalWithConfigurationHolder($principalIdentifier, $configurationHolder)) {
                $principal = (new AdobeConnectWebinarPrincipal($configurationHolder))
                    ->setUser(new User());

            }

            $user = $principal->getUser();

            $principal
                ->setPrincipalIdentifier((int) $node->attr('principal-id'))
                ->setName($node->filterXPath('//name')->text());

            if (!empty($user)) {
                $user->setEmail($node->filterXPath('//login')->text());
            }

            $principals[] = $principal;
        });

        return $principals;
    }
    
    public function createPrincipalsGroupFromCrawler(AdobeConnectWebinarConfigurationHolder $configurationHolder, Crawler $crawler): array
    {
        $principals = [];
        $crawler->each(function (Crawler $node) use (&$principals, $configurationHolder) {
            if ($node->attr('has-children') === 'false') {
                return;
            }
            
            $principalIdentifier = $node->attr('principal-id');
            if (!is_numeric($principalIdentifier)) {
                return;
            }
            
            $principal = (new AdobeConnectWebinarPrincipal($configurationHolder))
                ->setPrincipalIdentifier((int) $principalIdentifier)
                ->setName($node->filterXPath('//name')->text());
            
            $principals[] = $principal;
        });
        
        return $principals;
    }
}
