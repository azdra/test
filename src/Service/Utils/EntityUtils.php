<?php

namespace App\Service\Utils;

use Doctrine\ORM\EntityManagerInterface;

class EntityUtils
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function getEntityChanges(mixed $entity): array
    {
        $unitOfWork = $this->entityManager->getUnitOfWork();
        $unitOfWork->computeChangeSets();

        return $unitOfWork->getEntityChangeSet($entity);
    }
}
