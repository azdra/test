<?php

namespace App\Service\Utils;

abstract class ImageUtils
{
    public static function getImageSize(string $imagePath): array
    {
        try {
            $imageSize = getimagesize($imagePath);

            return [
                'width' => $imageSize[0],
                'height' => $imageSize[1],
            ];
        } catch (\Exception $e) {
        }

        return [
            'width' => 0,
            'height' => 0,
        ];
    }

    public static function isSquare(string $imagePath, int $tolerance = 20): bool
    {
        $imageSize = self::getImageSize($imagePath);

        $tolerance = max($imageSize['width'], $imageSize['height']) * ($tolerance / 100);

        return abs($imageSize['width'] - $imageSize['height']) <= $tolerance;
    }
}
