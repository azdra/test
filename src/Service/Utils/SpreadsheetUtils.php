<?php

namespace App\Service\Utils;

use App\Exception\UploadFile\UploadFileException;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class SpreadsheetUtils
{
    public static function numberToLetter(int $number): string
    {
        $numeric = ($number - 1) % 26;
        $letter = chr(65 + $numeric);
        $number2 = intval(($number - 1) / 26);

        return ($number2 > 0) ? self::numberToLetter($number2).$letter : $letter;
    }

    public static function parseExcelDate(mixed $rawValue, string $format, ?\DateTimeZone $timezone = null): ?string
    {
        if (is_null($rawValue)) {
            return null;
        }

        if (is_numeric($rawValue)) {
            return date($format, Date::excelToTimestamp($rawValue, $timezone));
        } else {
            $date = date_create_immutable_from_format($format, $rawValue, $timezone);

            if ($date === false) {
                throw new UploadFileException("Invalid date $rawValue");
            }

            return $date->format($format);
        }
    }
}
