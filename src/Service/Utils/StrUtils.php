<?php

namespace App\Service\Utils;

class StrUtils
{
    public static function senderFormated(string $sender): string
    {
        $sender = preg_replace('/\s+|[^a-zA-Z0-9]/', '', $sender);

        return substr($sender, 0, 11);
    }
}
