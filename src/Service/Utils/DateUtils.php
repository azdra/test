<?php

namespace App\Service\Utils;

class DateUtils
{
    public static function humanizeDuration(int $durationInMinutes, string $locale = 'fr'): string
    {
        $minutes = sprintf('%02d', $durationInMinutes % 60);
        $hours = sprintf('%02d', intval($durationInMinutes / 60));

        return $locale === 'en'
            ? "$hours:$minutes"
            : "{$hours}h{$minutes}m";
    }

    public static function humanizeDurationWithoutM(int $durationInMinutes, string $locale = 'fr'): string
    {
        $minutes = sprintf('%02d', $durationInMinutes % 60);
        $hours = sprintf('%02d', intval($durationInMinutes / 60));

        return $locale === 'en'
            ? "$hours:$minutes"
            : "{$hours}h$minutes";
    }
}
