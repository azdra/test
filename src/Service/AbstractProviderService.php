<?php

namespace App\Service;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Model\ProviderResponse;
use App\Service\Connector\ConnectorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractProviderService
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected ValidatorInterface $validator,
        protected ConnectorInterface $connector,
        protected LoggerInterface $logger
    ) {
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    abstract public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool;

    abstract public function createSession(ProviderSession $providerSession): ProviderResponse;

    abstract public function updateSession(ProviderSession $providerSession): ProviderResponse;

    abstract public function deleteSession(ProviderSession $providerSession): ProviderResponse;

    abstract public function createParticipant(ProviderParticipant $providerParticipant): ProviderResponse;

    abstract public function updateParticipant(ProviderParticipant $providerParticipant): ProviderResponse;

    abstract public function deleteParticipant(ProviderParticipant $providerParticipant): ProviderResponse;

    abstract public function updateParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse;

    abstract public function redirectParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse;
}
