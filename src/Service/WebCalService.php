<?php

namespace App\Service;

use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Entity\HelpTask;
use App\Entity\Client\Client;
use App\Entity\Client\ClientServices;
use App\Entity\ProviderSession;
use App\Entity\ServiceSchedule;
use Eluceo\iCal\Domain\Entity\Calendar;
use Eluceo\iCal\Domain\Entity\Event;
use Eluceo\iCal\Domain\ValueObject\DateTime as EluceoDateTime;
use Eluceo\iCal\Domain\ValueObject\TimeSpan;
use Eluceo\iCal\Domain\ValueObject\Uri;
use Eluceo\iCal\Presentation\Component;
use Eluceo\iCal\Presentation\Factory\CalendarFactory;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class WebCalService
{
    public const WEEKS_BEFORE = 1;
    public const WEEKS_AFTER = 29;

    public function __construct(
        private RouterInterface $router,
        private TranslatorInterface $translator
    ) {
    }

    public function generateWebCalFromSession(ProviderSession $session): Event
    {
        $summary = $session->getName();
        $description = $this->generateDescription($session);
        $event = new Event();
        $event->setSummary($summary);
        $event->setDescription($description);
        $event->setUrl(
            new Uri(
                $this->router->generate('_session_get', ['id' => $session->getId()], UrlGeneratorInterface::ABSOLUTE_URL)
            )
        );

        $event->setOccurrence(
            new TimeSpan(
                new EluceoDateTime($session->getDateStart(), $session->getClient()->getTimezone()),
                new EluceoDateTime($session->getDateEnd(), $session->getClient()->getTimezone())
            )
        );

        return $event;
    }

    public function generateWebCalFromHelpNeed(HelpNeed $helpNeed): Event
    {
        $description = $this->generateHelpNeedDescription($helpNeed);
        $event = new Event();
        $event->setSummary($this->getHelpNeedType($helpNeed));
        $event->setDescription($description);

        $event->setOccurrence(
            new TimeSpan(
                new EluceoDateTime($helpNeed->getStart(), $helpNeed->getClient()->getTimezone()),
                new EluceoDateTime($helpNeed->getEnd(), $helpNeed->getClient()->getTimezone())
            )
        );

        return $event;
    }

    public function generateWebCalFile(array $data): ?Component
    {
        $events = [];

        if (count($data) === 0) {
            return null;
        }

        foreach ($data as $eventData) {
            if ($eventData instanceof ProviderSession) {
                $event = $this->generateWebCalFromSession($eventData);
                $events[] = $event;
            } elseif ($eventData instanceof HelpTask) {
                $event = $this->generateWebCalFromHelpNeed($eventData->getHelpNeed());
                $events[] = $event;
            } else {
                continue;
            }
        }

        // 2. Create Calendar domain entity
        $calendar = new Calendar($events);

        // 3. Transform domain entity into an iCalendar component
        $componentFactory = new CalendarFactory();
        $calendarComponent = $componentFactory->createCalendar($calendar);

        return $calendarComponent;
    }

    public function generateHelpNeedDescription(HelpNeed $helpNeed): string
    {
        return $this->getHelpNeedType($helpNeed).' - '.$helpNeed->getClient()->getName();
    }

    public function generateDescription($eventData): string
    {
        $session = $eventData;

        $description = $this->router->generate('_session_get', ['id' => $session->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
        $description .= "\n";

        $description .= $this->translator->trans('Category').' : '.$this->translator->trans($session->getCategory()->label())." \n";
        $description .= "\n";

        $animators = $session->getAnimatorsOnly();
        $description .= $this->translator->trans('Animator.s').' : '.count($animators)."\n";
        if (count($animators) > 0) {
            foreach ($animators as $animator) {
                $description .= $animator->getParticipant()." \n";
            }
        }
        $description .= "\n";

        $description .= $this->translator->trans('Attendees count').' : '.count($session->getParticipantRoles())."\n";
        $description .= "\n";

        $description .= $this->translator->trans('Reference').' : '.$session->getRef()."\n";
        $description .= "\n";

        /** @var Client $client */
        $client = $session->getClient();
        $services = $client->getServices();

        $description .= $this->translator->trans('Additional services').' :';
        $description .= "\n";

        $description .= $this->translator->trans('Assistance').':'.$this->getAssistancesNames($session);
        $description .= "\n";
        $description .= $this->translator->trans($services->getReadableAssistanceType());
        $description .= "\n";
        $description .= $this->translator->trans('Duration of assistance (min)').' : '.$services->getAssistanceDuration();
        $description .= "\n";
        $description .= $this->translator->trans('Start of the assistance (min)').' : '.$services->getAssistanceStart();
        $description .= "\n";

        $support = $services->getSupportType();

        if ($support !== ClientServices::ENUM_SUPPORT_NO) {
            $description .= $this->translator->trans('Additional support').' : ';
            $description .= "\n";
            $description .= $this->translator->trans($services->getReadableSupportType()).' : ';
            $description .= "\n";

            $standardHours = $services->getStandardHours();
            if ($standardHours->isActive()) {
                $description .= $this->translator->trans('Standard hours').' : ';
                $description = $this->getReadableHours(
                    $standardHours,
                    $description
                );
            }

            $outsideStandardHours = $services->getOutsideStandardHours();
            if ($outsideStandardHours->isActive()) {
                $description .= $this->translator->trans('Outside standard hours').' : ';
                $description = $this->getReadableHours(
                    $outsideStandardHours,
                    $description
                );
            }
        }

        $description .= "\n";

        return $description;
    }

    private function getReadableHours(ServiceSchedule $serviceSchedule, string $description): string
    {
        $description .= $this->translator->trans('AM').' : '.$serviceSchedule->getStartTimeAM()->format('H:m').' '.$this->translator->trans('at').' '.$serviceSchedule->getEndTimeAM()->format('H:m')."\n";
        $description .= $this->translator->trans('PM').' : '.$serviceSchedule->getStartTimePM()->format('H:m').' '.$this->translator->trans('at').' '.$serviceSchedule->getEndTimePM()->format('H:m')."\n";
        $description .= $this->translator->trans('Lunch Pause').' : '.$serviceSchedule->getStartTimeLH()->format('H:m').' '.$this->translator->trans('at').' '.$serviceSchedule->getEndTimeLH()->format('H:m')."\n";

        return $description;
    }

    public function getAssistancesNames(ProviderSession $session): string
    {
        $assistance = '';
        foreach ($session->getHelpNeeds() as $helpNeed) {
            foreach ($helpNeed->getHelpTasks() as $helpTask) {
                $assistance = $assistance.'| '.$helpTask->getUser()->getFullName();
            }
        }

        return $assistance;
    }

    public function getHelpNeedType(HelpNeed $helpNeed): string
    {
        return match ($helpNeed->getType()) {
            HelpNeed::TYPE_DIRECT_NEED => $this->translator->trans('Services provided').' '.$helpNeed->getAdditionalInformation()['serviceType'],
            HelpNeed::TYPE_ASSISTANCE => $this->translator->trans('Assistance'),
            HelpNeed::TYPE_SUPPORT => $this->translator->trans('Support'),
            HelpNeed::TYPE_CUSTOMER_REQUIREMENT => $this->translator->trans('Customer requirement'),
        };
    }
}
