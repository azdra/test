<?php

namespace App\Service;

use App\Entity\SmsBounce;
use App\Event\BrevoSmsMessageEvent;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;

class SmsBounceService
{
    public function __construct(
        private UserRepository $userRepository,
        private ProviderSessionRepository $sessionRepository,
    ) {
    }

    public function createBounce(BrevoSmsMessageEvent $event): SmsBounce
    {
        $tag = $event->getTag();

        if (!empty($tag['user'])) {
            $user = $this->userRepository->find($tag['user']);
        }

        if (!empty($tag['session'])) {
            $session = $this->sessionRepository->find($tag['session']);
        }

        return (new SmsBounce())
            ->setCreatedAt(new \DateTime())
            ->setSession($session ?? null)
            ->setStatus($event->getName())
            ->setPhone($event->getPhone())
            ->setDateSend($event->getDate())
            ->setUser($user ?? null);
    }
}
