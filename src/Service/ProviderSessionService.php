<?php

namespace App\Service;

use App\CustomerService\Service\HelpNeedService;
use App\Entity\ActivityLog;
use App\Entity\CategorySessionType;
use App\Entity\Client\ClientEmailScheduling;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Entity\Evaluation;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Event\TransactionalEmailRequestEventInterface;
use App\Event\TransactionalSessionEmailRequestEvent;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\MiddlewareFailureException;
use App\Exception\ProviderServiceValidationException;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Model\ActivityLog\ActivityLogModel;
use App\RegistrationPage\Entity\RegistrationPageTemplate;
use App\Repository\AbstractProviderConfigurationHolderRepository;
use App\Repository\ConvocationRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\Provider\Adobe\AdobeConnectScoArrayHandler;
use App\Service\Provider\Adobe\AdobeConnectWebinarScoArrayHandler;
use App\Service\Provider\Microsoft\MicrosoftTeamsArrayHandler;
use App\Service\Provider\Microsoft\MicrosoftTeamsEventArrayHandler;
use App\Service\Provider\Model\SessionArrayHandler;
use App\Service\Provider\Webex\WebexMeetingArrayHandler;
use App\Service\Provider\Webex\WebexRestMeetingArrayHandler;
use App\Service\Provider\White\WhiteArrayHandler;
use App\Service\RessourceUploader\RessourceUploader;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProviderSessionService
{
    public function __construct(
        private MiddlewareService $middlewareService,
        private EntityManagerInterface $entityManager,
        private AdobeConnectScoArrayHandler $adobeConnectScoArrayHandler,
        private AdobeConnectWebinarScoArrayHandler $adobeConnectWebinarScoArrayHandler,
        private WebexMeetingArrayHandler $webexMeetingArrayHandler,
        private WebexRestMeetingArrayHandler $webexRestMeetingArrayHandler,
        private MicrosoftTeamsArrayHandler $microsoftTeamsArrayHandler,
        private MicrosoftTeamsEventArrayHandler $microsoftTeamsEventArrayHandler,
        private WhiteArrayHandler $whiteArrayHandler,
        private ActivityLogger $activityLogger,
        private RouterInterface $router,
        private EventDispatcherInterface $eventDispatcher,
        private UserRepository $userRepository,
        private MailerService $mailerService,
        private TranslatorInterface $translator,
        private LoggerInterface $logger,
        private HelpNeedService $helpNeedService,
        private AbstractProviderConfigurationHolderRepository $configurationHolderRepository,
        private ConvocationRepository $convocationRepository,
        private ProviderSessionRepository $providerSessionRepository,
        private RessourceUploader $ressourceUploader,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        private string $convocationAttachmentSavefilesDirectory
    ) {
    }

    /**
     * @throws Exception
     */
    public static function generateSessionReference(string $clientName): string
    {
        return strtoupper(substr($clientName, 0, 2))
               .(new DateTime())->format('ymdHis')
               .random_int(1000, 9999);
    }

    public function findLicenceUsedInSessions(string $licence): int
    {
        $nbrLicencesACScoUsedInSessions = $this->providerSessionRepository->findLicenceACScoUsedInSessions($licence);
        $nbrLicencesMSTeamsUsedInSessions = $this->providerSessionRepository->findLicenceMSTeamsUsedInSessions($licence);
        $nbrLicencesMSTeamsEventUsedInSessions = $this->providerSessionRepository->findLicenceMSTeamsEventUsedInSessions($licence);
        $nbrLicencesWebexMeetingUsedInSessions = $this->providerSessionRepository->findLicenceWebexMeetingUsedInSessions($licence);
        $nbrLicencesWebexRestMeetingUsedInSessions = $this->providerSessionRepository->findLicenceWebexRestMeetingUsedInSessions($licence);
        $nbrLicencesUsedAll = $nbrLicencesACScoUsedInSessions + $nbrLicencesMSTeamsUsedInSessions + $nbrLicencesMSTeamsEventUsedInSessions + $nbrLicencesWebexRestMeetingUsedInSessions + $nbrLicencesWebexRestMeetingUsedInSessions;

        return $nbrLicencesUsedAll;
    }

    public function applyRemindersAndConvocationPreference(ClientEmailScheduling $clientEmailScheduling, ProviderSession $session): ProviderSession
    {
        $session->setEnableSendRemindersToAnimators($clientEmailScheduling->isEnableSendRemindersToAnimators());

        $dates = $this->getConvocationAndRemindersDate($clientEmailScheduling, $session->getDateStart());

        if (null !== $dates['convocation']) {
            $session->setNotificationDate($dates['convocation']);
        }

        if (null !== $dates['reminders']) {
            $session->setDateOfSendingReminders($dates['reminders']);
        }

        return $session;
    }

    public function getConvocationAndRemindersDate(ClientEmailScheduling $clientEmailScheduling, DateTime $dateStart, ?string $format = null): array
    {
        return [
            'convocation' => $this->reportNotificationDateFromClientEmailScheduling($clientEmailScheduling, $dateStart, $format),
            'reminders' => $this->reportReminderDatesFromClientEmailScheduling($clientEmailScheduling, $dateStart, $format),
        ];
    }

    private function reportNotificationDateFromClientEmailScheduling(ClientEmailScheduling $clientEmailScheduling, DateTime $dateStart, ?string $format = null): DateTimeImmutable|string|null
    {
        $timeToSendConvocationBeforeSession = $clientEmailScheduling->getTimeToSendConvocationBeforeSession();
        if (empty($timeToSendConvocationBeforeSession)) {
            return null;
        }

        $notificationDate = clone $dateStart;
        date_sub($notificationDate, date_interval_create_from_date_string(
            $timeToSendConvocationBeforeSession['days'].' days '.
            $timeToSendConvocationBeforeSession['hours'].' hours '.
            $timeToSendConvocationBeforeSession['minutes'].' minutes'
        ));

        if (null !== $format) {
            if ($notificationDate < new DateTime()) {
                return (new DateTime())->format($format);
            } else {
                return $notificationDate->format($format);
            }
        }

        return \DateTimeImmutable::createFromMutable($notificationDate);
    }

    private function reportReminderDatesFromClientEmailScheduling(ClientEmailScheduling $clientEmailScheduling, DateTime $dateStart, ?string $format = null): ?array
    {
        $timesToSendReminderBeforeSession = $clientEmailScheduling->getTimesToSendReminderBeforeSession();
        if (empty($timesToSendReminderBeforeSession)) {
            return null;
        }

        $datesReminders = [];
        foreach ($timesToSendReminderBeforeSession as $timeReminderBeforeSession) {
            if ($timeReminderBeforeSession['days'] !== null && $timeReminderBeforeSession['hours'] !== null && $timeReminderBeforeSession['minutes'] !== null) {
                $interval = date_interval_create_from_date_string(
                    $timeReminderBeforeSession['days'].' days '.
                    $timeReminderBeforeSession['hours'].' hours '.
                    $timeReminderBeforeSession['minutes'].' minutes'
                );

                $date = clone $dateStart;
                $reminderDate = date_sub($date, $interval);

                if (null !== $format) {
                    $datesReminders[] = $reminderDate->format($format);
                } else {
                    $datesReminders[] = \DateTimeImmutable::createFromMutable($reminderDate);
                }
            }
        }

        return $datesReminders;
    }

    /**
     * @throws InvalidProviderConfigurationHandler
     * @throws MiddlewareFailureException
     */
    public function deleteSession(ProviderSession $providerSession): void
    {
        $middlewareResponse = $this->middlewareService->call(MiddlewareService::MIDDLEWARE_ACTION_DELETE_SESSION, $providerSession);

        if (!$middlewareResponse->isSuccess()) {
            throw new MiddlewareFailureException($middlewareResponse->getData()->getMessage());
        }

        /** @var ProviderParticipantSessionRole $participantRole */
        foreach ($providerSession->getParticipantRoles() as $participantRole) {
            $this->entityManager->remove($participantRole);
        }
        $this->entityManager->remove($providerSession);
    }

    public function deleteEvaluationsSessions(ProviderSession $providerSession): void
    {
        foreach ($providerSession->getEvaluationsProviderSession() as $evaluation) {
            $this->entityManager->remove($evaluation);
        }
    }

    /**
     * @throws InvalidProviderConfigurationHandler
     * @throws MiddlewareFailureException
     * @throws ProviderSessionRequestHandlerException
     */
    public function createSessionFromArray(AbstractProviderConfigurationHolder $configurationHolder, ?UserInterface $userOrigin, array &$data): ProviderSession
    {
        $providerSession = $this->getHandler($configurationHolder)->createSession($configurationHolder, $data, $userOrigin);

        return $this->createSession($providerSession);
    }

    /**
     * @throws MiddlewareFailureException
     */
    public function createSession(ProviderSession $session): ProviderSession
    {
        try {
            if (!$session->getClient()->categoryAvailable($session->getCategory())) {
                throw new MiddlewareFailureException('The session category is not available in your account!');
            }
            $middlewareResponse = $this->middlewareService->call(MiddlewareService::MIDDLEWARE_ACTION_CREATE_SESSION, $session);
            if (!$middlewareResponse->isSuccess()) {
                throw new MiddlewareFailureException($middlewareResponse->getException()->getMessage(), $middlewareResponse->getData());
            }
            $this->helpNeedService->synchronizeSessionHelpNeed($session);

            return $middlewareResponse->getData();
        } catch (ProviderSessionRequestHandlerException $exception) {
            $this->logger->error($exception);
            $errors = [];
            $errors[$exception->getKey()] = $exception->getMessage();

            $messageException = (!empty($exception->getMessage())) ? $exception->getMessage() : ProviderServiceValidationException::MESSAGE;

            throw new MiddlewareFailureException($this->translator->trans($messageException), $errors);
        } catch (\Throwable $exception) {
            if (!($exception instanceof MiddlewareFailureException)) {
                $this->logger->error($exception);
                throw new MiddlewareFailureException('Unknown error occurred', $exception);
            } else {
                $this->logger->error($exception);
                throw $exception;
            }
        }
    }

    /**
     * @throws InvalidProviderConfigurationHandler
     * @throws MiddlewareFailureException
     */
    public function updateSessionFromArray(AbstractProviderConfigurationHolder $configurationHolder, ProviderSession $providerSession, UserInterface $user, array &$data): void
    {
        $this->getHandler($configurationHolder)->updateSession($providerSession, $data, $user);

        if (!empty($data['updateFields'])) {
            $updateFields['updateFields'] = $data['updateFields'];
            $this->updateSession($providerSession, $updateFields);
        } else {
            $this->updateSession($providerSession);
        }
    }

    /**
     * @throws MiddlewareFailureException
     */
    public function updateSession(ProviderSession $session, ?array $params = []): ProviderSession
    {
        try {
            if (!$session->getClient()->categoryAvailable($session->getCategory())) {
                throw new MiddlewareFailureException('The session category is not available in your account!');
            }

            $updatedFieldsSession = $this->findUpdatedFields($session);

            // TODO: partie du code commenté pour celui qui y est retourné dans le cas de la définition des restrictions en relation avec la modification de session.
            /*if (!empty($session->getHelpNeeds()->getValues())){
                if (!array_key_exists('dateStart', $updatedFieldsSession)) {
                    if ($session->getDateStart() < (new DateTimeImmutable())->modify('+ 1 day') ){
                        if (array_key_exists('assistanceType', $updatedFieldsSession) ||
                            array_key_exists('supportType', $updatedFieldsSession) ){
                            throw new \Exception('You cannot modify your session in less than 24 hours and which contains a service request');
                        }
                    }
                } else {
                    if (new DateTimeImmutable($updatedFieldsSession['dateStart']['oldValue']) < (new DateTimeImmutable())->modify('+ 1 day')){
                        throw new \Exception('You cannot modify your session in less than 24 hours and which contains a service request');
                    }
                }
            }*/

            $middlewareResponse = $this->middlewareService->call(MiddlewareService::MIDDLEWARE_ACTION_UPDATE_SESSION, $session, $params);

            if (!$middlewareResponse->isSuccess()) {
                throw new MiddlewareFailureException($middlewareResponse->getException()->getMessage(), $middlewareResponse->getData());
            }
            if (!empty($updatedFieldsSession)) {
                $infos = [
                    'message' => ['key' => 'Editing the session <a href="%route%">%sessionName%</a> (ref: %ref%) session', 'params' => ['%route%' => $this->router->generate('_session_get', ['id' => $session->getId()]), '%sessionName%' => $session->getName(), '%ref%' => $session->getRef()]],
                    'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                    'updatedFields' => $updatedFieldsSession,
                    'links' => [['type' => 'component', 'name' => 'ModalSessionUpdateInfo']],
                ];
                $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SESSION_UPDATE, ActivityLog::SEVERITY_INFORMATION, $infos), true);
            }

            $this->helpNeedService->synchronizeSessionHelpNeed($session);

            return $middlewareResponse->getData();
        } catch (ProviderSessionRequestHandlerException $exception) {
            $errors = [];
            $errors[$exception->getKey()] = $exception->getMessage();
            $this->logger->error($exception);
            throw new MiddlewareFailureException($this->translator->trans(ProviderServiceValidationException::MESSAGE), $errors);
        } catch (\Throwable $exception) {
            if (!($exception instanceof MiddlewareFailureException)) {
                $this->logger->error($exception);
                throw new MiddlewareFailureException('Unknown error occurred', $exception);
            } else {
                $this->logger->error($exception);
                throw $exception;
            }
        }
    }

    public function findUpdatedFields(ProviderSession $session): array
    {
        $updatedFields = [];
        $uow = $this->entityManager->getUnitOfWork();
        if (!is_null($uow)) {
            $uow->computeChangeSets();
            $changesSet = $uow->getEntityChangeSet($session);
            foreach ($changesSet as $field => $changeSet) {
                $oldValue = $changeSet[0];
                $newValue = $changeSet[1];
                if ($oldValue != $newValue && !in_array($field, ['dateEnd', 'updatedBy', 'evaluations'])) {
                    $updatedFields[$field]['transKey'] = $this->getFieldNameTransKey($field);
                    switch ($field) {
                        case 'dateStart':
                        case 'notificationDate':
                            $updatedFields[$field]['oldValue'] = $oldValue ? $oldValue->format('Y-m-d H:i:s') : '';
                            $updatedFields[$field]['newValue'] = $newValue ? $newValue->format('Y-m-d H:i:s') : '';
                            break;
                        case 'duration':
                            $updatedFields[$field]['oldValue'] = $oldValue.' min';
                            $updatedFields[$field]['newValue'] = $newValue.' min';
                            break;
                        case 'convocation':
                            $updatedFields[$field]['oldValue'] = $oldValue->getName();
                            $updatedFields[$field]['newValue'] = $newValue->getName();
                            break;
                        case 'tags':
                            if ($oldValue == null) {
                                $oldValue = [];
                            }
                            $updatedFields[$field]['oldValue'] = implode(', ', $oldValue);
                            $updatedFields[$field]['newValue'] = implode(', ', $newValue);
                            break;
                        case 'dateOfSendingReminders':
                            $updatedFields[$field]['oldValue'] = join(' |', array_map(
                                fn (?\DateTimeImmutable $date) => $date ? $date->format('Y-m-d H:i:s') : '',
                                $oldValue
                            ));
                            $updatedFields[$field]['newValue'] = join(' |', array_map(
                                fn (?\DateTimeImmutable $date) => $date ? $date->format('Y-m-d H:i:s') : '',
                                $newValue
                            ));
                            break;
                        case 'category':
                            $updatedFields[$field]['oldValue'] = CategorySessionType::from($oldValue)->label();
                            $updatedFields[$field]['newValue'] = CategorySessionType::from($newValue)->label();
                            break;
                        default:
                            $updatedFields[$field]['oldValue'] = $oldValue;
                            $updatedFields[$field]['newValue'] = $newValue;
                            break;
                    }
                }
            }

            if (null !== $session->getOldEvaluations()) {
                $oldValue = join(' | ', array_map(
                    fn (Evaluation $evaluation) => $evaluation->getTitle(),
                    $session->getOldEvaluations()->toArray()
                ));
                $newValue = join(' | ', array_map(
                    fn (Evaluation $evaluation) => $evaluation->getTitle(),
                    $session->getEvaluations()->toArray()
                ));
                if ($oldValue !== $newValue) {
                    $field = 'evaluations';
                    $updatedFields[$field]['transKey'] = $this->getFieldNameTransKey($field);
                    $updatedFields[$field]['oldValue'] = $oldValue;
                    $updatedFields[$field]['newValue'] = $newValue;
                }
            }
        }

        return $updatedFields;
    }

    public function getFieldNameTransKey(string $fieldName): string
    {
        return match ($fieldName) {
            'name' => 'Name',
            'category' => 'Category',
            'dateStart' => 'Start date',
            'duration' => 'Duration',
            'businessNumber' => 'Business Number',
            'information' => 'Information',
            'tags' => 'Tags',
            'sessionTest' => 'Session test',
            'convocation' => 'Convocation',
            'admittedSession' => 'Admitted to the meeting',
            'notificationDate' => 'Date and hour of sending convocation',
            'dateOfSendingReminders' => 'Date and hour of sending reminder',
            'personalizedContent' => 'Custom content',
            'nature' => 'Nature',
            'providerAudio' => 'Audio Provider',
            'evaluations' => 'Evaluation',
            'assistanceType' => 'Type of Assistance',
            'objectives' => 'Objectives',
            'subscriptionMax' => 'Max number',
            'attachments' => 'Attachment',
            'alertLimit' => 'Limit of alert',
            'subject' => 'Subject',
            default => '',
        };
    }

    /**
     * @throws InvalidProviderConfigurationHandler
     * @throws MiddlewareFailureException
     */
    public function cancelSession(ProviderSession $providerSession): void
    {
        if ($providerSession->getDateStart() < (new DateTimeImmutable())->modify('+ 1 day') && !empty($providerSession->getHelpNeeds()->getValues())) {
            $providerSession->setSessionCanceledLessOneDay(true);
        }
        $this->eventDispatcher->dispatch(new TransactionalSessionEmailRequestEvent($providerSession), TransactionalEmailRequestEventInterface::CANCEL);
        $providerSession->setStatus(ProviderSession::STATUS_CANCELED);

        $infos = [
            'message' => ['key' => 'Cancelling the  session <a href="%route%">%sessionName%</a> (ref: %ref%)', 'params' => ['%route%' => $this->router->generate('_session_get', ['id' => $providerSession->getId()]), '%sessionName%' => $providerSession->getName(), '%ref%' => $providerSession->getRef()]],
            'session' => ['id' => $providerSession->getId(), 'name' => $providerSession->getName(), 'ref' => $providerSession->getRef()],
        ];

        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SESSION_CANCEL, ActivityLog::SEVERITY_WARNING, $infos), true);

        $userPreferenceEmailSessionCanceled = true;
        $userManagers = $this->userRepository->findManagerListByClient($providerSession->getClient());
        foreach ($userManagers as $user) {
            if (array_key_exists(User::EMAIL_SESSION_CANCELED, $user->getPreferences())) {
                $userPreferenceEmailSessionCanceled = $user->getPreferences()[User::EMAIL_SESSION_CANCELED];
            }
            if ($userPreferenceEmailSessionCanceled) {
                $sendGo = false;
                if ($user->isSubscribeToAlert(User::EMAIL_ONLY_FOR_MY_SESSIONS)) {
                    if ((!empty($providerSession->getCreatedBy()) && $providerSession->getCreatedBy() == $user) || (!empty($providerSession->getCreatedBy()) && $providerSession->getUpdatedBy() == $user)) {
                        $sendGo = true;
                    }
                } else {
                    $sendGo = true;
                }
                if ($sendGo) {
                    $this->mailerService->sendTemplatedMail(
                        $user->getEmail(),
                        $this->translator->trans('Session cancelled'),
                        'emails/manager_client/session_has_canceled.html.twig',
                        context: [
                            'session' => $providerSession,
                            'client' => $providerSession->getClient(),
                            'route' => $this->router->generate('_session_get', ['id' => $providerSession->getId()], 0),
                            'user' => $user,
                        ],
                        metadatas: [
                            'origin' => MailerService::ORIGIN_EMAIL_SEND_SESSION_CANCELED,
                            'user' => $user->getId(),
                        ],
                        senderSystem: true
                    );
                }
            }
        }
    }

    /**
     * @throws InvalidProviderConfigurationHandler
     * @throws MiddlewareFailureException
     */
    public function uncancelSession(ProviderSession $providerSession): void
    {
        $providerSession->setStatus(ProviderSession::STATUS_SCHEDULED);

        $infos = [
            'message' => ['key' => 'Uncancelling  the session  <a href="%route%">%sessionName%</a> (ref: %ref%)', 'params' => ['%route%' => $this->router->generate('_session_get', ['id' => $providerSession->getId()]), '%sessionName%' => $providerSession->getName(), '%ref%' => $providerSession->getRef()]],
            'session' => ['id' => $providerSession->getId(), 'name' => $providerSession->getName(), 'ref' => $providerSession->getRef()],
        ];

        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SESSION_UNCANCEL, ActivityLog::SEVERITY_WARNING, $infos), true);
        $this->eventDispatcher->dispatch(new TransactionalSessionEmailRequestEvent($providerSession), TransactionalEmailRequestEventInterface::UNCANCEL);

        $userPreferenceEmailSessionUncanceled = true;
        $userManagers = $this->userRepository->findManagerListByClient($providerSession->getClient());
        foreach ($userManagers as $user) {
            if (array_key_exists(User::EMAIL_SESSION_UNCANCELED, $user->getPreferences())) {
                $userPreferenceEmailSessionUncanceled = $user->getPreferences()[User::EMAIL_SESSION_UNCANCELED];
            }
            if ($userPreferenceEmailSessionUncanceled) {
                $sendGo = false;
                if ($user->isSubscribeToAlert(User::EMAIL_ONLY_FOR_MY_SESSIONS)) {
                    if ((!empty($providerSession->getCreatedBy()) && $providerSession->getCreatedBy() == $user) || (!empty($providerSession->getCreatedBy()) && $providerSession->getUpdatedBy() == $user)) {
                        $sendGo = true;
                    }
                } else {
                    $sendGo = true;
                }
                if ($sendGo) {
                    $this->mailerService->sendTemplatedMail(
                        $user->getEmail(),
                        $this->translator->trans('Session uncanceled'),
                        'emails/manager_client/session_has_uncanceled.html.twig',
                        context: [
                            'session' => $providerSession,
                            'client' => $providerSession->getClient(),
                            'route' => $this->router->generate('_session_get', ['id' => $providerSession->getId()], 0),
                            'user' => $user,
                        ],
                        metadatas: [
                            'origin' => MailerService::ORIGIN_EMAIL_SEND_SESSION_UNCANCELED,
                            'user' => $user->getId(),
                        ],
                        senderSystem: true
                    );
                }
            }
        }
    }

    public function sendEmailToParticipants(array $participants, ?UploadedFile $attachment = null): array
    {
        $response = [
            'filetarget' => '',
            'previewUri' => '',
        ];
        $attachments = [];
        if (!empty($attachment)) {
            $this->ressourceUploader->isFileUploadedValid($attachment, ProviderSession::AUTHORIZED_EXTENSIONS, ProviderSession::MAX_FILE_SIZE);
            $filename = \md5(\random_bytes(32)).'.'.$attachment->getClientOriginalExtension();
            (new Filesystem())->copy($attachment->getRealPath(), $this->convocationAttachmentSavefilesDirectory.'/'.$filename, true);
            $attachments[] = ['path' => $this->convocationAttachmentSavefilesDirectory.'/'.$filename, 'name' => $attachment->getClientOriginalName()];
            $response = [
                'filetarget' => $filename,
                'previewUri' => $this->convocationAttachmentSavefilesDirectory.'/'.$filename,
            ];
        }

        foreach ($participants['ppsr'] as $participantData) {
            if ($participantData['ppsrParticipantSend']) {
                /** @var ProviderParticipantSessionRole $participantRole */
                $participantRole = $this->providerParticipantSessionRoleRepository->find($participantData['ppsrId']);
                /** @var ProviderSession session */
                $session = $participantRole->getSession();
                $participant = $participantRole->getParticipant();
                $lang = $participant->getUser()->getPreferredLang();
                $this->mailerService->sendTemplatedMail(
                    $participantData['ppsrParticipantEmail'],
                    $participants['object'],
                    'emails/participant/session_message_participant.html.twig',
                    context: [
                        'session' => $session,
                        'lang' => $lang,
                        'participant' => $participant,
                        'message' => $participants['message'],
                        'client' => $session->getClient(),
                        'clientTimeZone' => $session->getClient()->getTimezone(),
                    ],
                    pathToAttach: $attachments,
                    metadatas: [
                        'origin' => MailerService::ORIGIN_EMAIL_SEND_SESSION_CANCELED,
                        'user' => $participant->getUser()->getId(),
                        'client' => $participant->getClient()->getId(),
                    ],
                    senderSystem: true
                );

                $infos = [
                    'message' => ['key' => 'The message is sent to the user <a href="%userPath%">%userName%</a> for the session <a href="%sessionPath%">%sessionName%</a> - Object: <a>%object%</a>', 'params' => ['userPath' => $this->router->generate('_participant_edit', ['id' => $participant->getUserId()]), 'userName' => $participant->getFullName(), 'sessionPath' => $this->router->generate('_session_get', ['id' => $session->getId()]), 'sessionName' => $session->getName(), 'object' => $participants['object']]],
                    'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                ];
                $this->activityLogger->initActivityLogContext($session->getClient(), null, ActivityLog::ORIGIN_USER_INTERFACE);
                $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SEND_EMAIL_TO_PARTICIPANT, ActivityLog::SEVERITY_NOTIFICATION, $infos));
                $this->activityLogger->flushActivityLogs();
            }
        }

        return $response;
    }

    public function getHandler(AbstractProviderConfigurationHolder $configurationHolder): SessionArrayHandler
    {
        return match (get_class($configurationHolder)) {
            AdobeConnectConfigurationHolder::class => $this->adobeConnectScoArrayHandler,
            AdobeConnectWebinarConfigurationHolder::class => $this->adobeConnectWebinarScoArrayHandler,
            WebexConfigurationHolder::class => $this->webexMeetingArrayHandler,
            WebexRestConfigurationHolder::class => $this->webexRestMeetingArrayHandler,
            MicrosoftTeamsConfigurationHolder::class => $this->microsoftTeamsArrayHandler,
            MicrosoftTeamsEventConfigurationHolder::class => $this->microsoftTeamsEventArrayHandler,
            WhiteConfigurationHolder::class => $this->whiteArrayHandler,
            default => throw new InvalidProviderConfigurationHandler()
        };
    }

    public function isValidConvocation(int $configurationHolderId, int $convocationId): bool
    {
        $configurationHolder = $this->configurationHolderRepository->findOneBy(['id' => $configurationHolderId]);
        $convocation = $this->convocationRepository->findOneBy(['id' => $convocationId]);
        $convocationsClient = $this->convocationRepository->findBy(['client' => $configurationHolder->getClient()]);

        return in_array($convocation, $convocationsClient) && $configurationHolder::getProvider() == $convocation->getConnectorType();
    }

    public function isUpdatedFields(array $updatedFields, ProviderSession $session): bool
    {
        if ($updatedFields['sessionName'] != $session->getName() || $updatedFields['sessionDateStart'] != $session->getDateStart()) {
            return true;
        }

        return false;
    }

    public function getFrenchTimezoneMapping(): array
    {
        return [
            'Africa/Abidjan' => ['en' => 'Africa/Abidjan', 'fr' => 'Afrique/Abidjan'],
            'Africa/Accra' => ['en' => 'Africa/Accra', 'fr' => 'Afrique/Accra'],
            'Africa/Addis_Ababa' => ['en' => 'Africa/Addis_Ababa', 'fr' => 'Afrique/Addis_Abeba'],
            'Africa/Algiers' => ['en' => 'Africa/Algiers', 'fr' => 'Afrique/Alger'],
            'Africa/Asmara' => ['en' => 'Africa/Asmara', 'fr' => 'Afrique/Asmara'],
            'Africa/Asmera' => ['en' => 'Africa/Asmera', 'fr' => 'Afrique/Asmara'],
            'Africa/Bamako' => ['en' => 'Africa/Bamako', 'fr' => 'Afrique/Bamako'],
            'Africa/Bangui' => ['en' => 'Africa/Bangui', 'fr' => 'Afrique/Bangui'],
            'Africa/Banjul' => ['en' => 'Africa/Banjul', 'fr' => 'Afrique/Banjul'],
            'Africa/Bissau' => ['en' => 'Africa/Bissau', 'fr' => 'Afrique/Bissau'],
            'Africa/Blantyre' => ['en' => 'Africa/Blantyre', 'fr' => 'Afrique/Blantyre'],
            'Africa/Brazzaville' => ['en' => 'Africa/Brazzaville', 'fr' => 'Afrique/Brazzaville'],
            'Africa/Bujumbura' => ['en' => 'Africa/Bujumbura', 'fr' => 'Afrique/Bujumbura'],
            'Africa/Cairo' => ['en' => 'Africa/Cairo', 'fr' => 'Afrique/Le_Caire'],
            'Africa/Casablanca' => ['en' => 'Africa/Casablanca', 'fr' => 'Afrique/Casablanca'],
            'Africa/Ceuta' => ['en' => 'Africa/Ceuta', 'fr' => 'Afrique/Ceuta'],
            'Africa/Conakry' => ['en' => 'Africa/Conakry', 'fr' => 'Afrique/Conakry'],
            'Africa/Dakar' => ['en' => 'Africa/Dakar', 'fr' => 'Afrique/Dakar'],
            'Africa/Dar_es_Salaam' => ['en' => 'Africa/Dar_es_Salaam', 'fr' => 'Afrique/Dar_es_Salaam'],
            'Africa/Djibouti' => ['en' => 'Africa/Djibouti', 'fr' => 'Afrique/Djibouti'],
            'Africa/Douala' => ['en' => 'Africa/Douala', 'fr' => 'Afrique/Douala'],
            'Africa/El_Aaiun' => ['en' => 'Africa/El_Aaiun', 'fr' => 'Afrique/El_Aaiun'],
            'Africa/Freetown' => ['en' => 'Africa/Freetown', 'fr' => 'Afrique/Freetown'],
            'Africa/Gaborone' => ['en' => 'Africa/Gaborone', 'fr' => 'Afrique/Gaborone'],
            'Africa/Harare' => ['en' => 'Africa/Harare', 'fr' => 'Afrique/Harare'],
            'Africa/Johannesburg' => ['en' => 'Africa/Johannesburg', 'fr' => 'Afrique/Johannesburg'],
            'Africa/Juba' => ['en' => 'Africa/Juba', 'fr' => 'Afrique/Juba'],
            'Africa/Kampala' => ['en' => 'Africa/Kampala', 'fr' => 'Afrique/Kampala'],
            'Africa/Khartoum' => ['en' => 'Africa/Khartoum', 'fr' => 'Afrique/Khartoum'],
            'Africa/Kigali' => ['en' => 'Africa/Kigali', 'fr' => 'Afrique/Kigali'],
            'Africa/Kinshasa' => ['en' => 'Africa/Kinshasa', 'fr' => 'Afrique/Kinshasa'],
            'Africa/Lagos' => ['en' => 'Africa/Lagos', 'fr' => 'Afrique/Lagos'],
            'Africa/Libreville' => ['en' => 'Africa/Libreville', 'fr' => 'Afrique/Libreville'],
            'Africa/Lome' => ['en' => 'Africa/Lome', 'fr' => 'Afrique/Lome'],
            'Africa/Luanda' => ['en' => 'Africa/Luanda', 'fr' => 'Afrique/Luanda'],
            'Africa/Lubumbashi' => ['en' => 'Africa/Lubumbashi', 'fr' => 'Afrique/Lubumbashi'],
            'Africa/Lusaka' => ['en' => 'Africa/Lusaka', 'fr' => 'Afrique/Lusaka'],
            'Africa/Malabo' => ['en' => 'Africa/Malabo', 'fr' => 'Afrique/Malabo'],
            'Africa/Maputo' => ['en' => 'Africa/Maputo', 'fr' => 'Afrique/Maputo'],
            'Africa/Maseru' => ['en' => 'Africa/Maseru', 'fr' => 'Afrique/Maseru'],
            'Africa/Mbabane' => ['en' => 'Africa/Mbabane', 'fr' => 'Afrique/Mbabane'],
            'Africa/Mogadishu' => ['en' => 'Africa/Mogadishu', 'fr' => 'Afrique/Mogadiscio'],
            'Africa/Monrovia' => ['en' => 'Africa/Monrovia', 'fr' => 'Afrique/Monrovia'],
            'Africa/Nairobi' => ['en' => 'Africa/Nairobi', 'fr' => 'Afrique/Nairobi'],
            'Africa/Ndjamena' => ['en' => 'Africa/Ndjamena', 'fr' => 'Afrique/Ndjamena'],
            'Africa/Niamey' => ['en' => 'Africa/Niamey', 'fr' => 'Afrique/Niamey'],
            'Africa/Nouakchott' => ['en' => 'Africa/Nouakchott', 'fr' => 'Afrique/Nouakchott'],
            'Africa/Ouagadougou' => ['en' => 'Africa/Ouagadougou', 'fr' => 'Afrique/Ouagadougou'],
            'Africa/Porto-Novo' => ['en' => 'Africa/Porto-Novo', 'fr' => 'Afrique/Porto-Novo'],
            'Africa/Sao_Tome' => ['en' => 'Africa/Sao_Tome', 'fr' => 'Afrique/Sao_Tome'],
            'Africa/Timbuktu' => ['en' => 'Africa/Timbuktu', 'fr' => 'Afrique/Timbuktu'],
            'Africa/Tripoli' => ['en' => 'Africa/Tripoli', 'fr' => 'Afrique/Tripoli'],
            'Africa/Tunis' => ['en' => 'Africa/Tunis', 'fr' => 'Afrique/Tunis'],
            'Africa/Windhoek' => ['en' => 'Africa/Windhoek', 'fr' => 'Afrique/Windhoek'],
            'America/Adak' => ['en' => 'America/Adak', 'fr' => 'Amérique/Adak'],
            'America/Anchorage' => ['en' => 'America/Anchorage', 'fr' => 'Amérique/Anchorage'],
            'America/Anguilla' => ['en' => 'America/Anguilla', 'fr' => 'Amérique/Anguilla'],
            'America/Antigua' => ['en' => 'America/Antigua', 'fr' => 'Amérique/Antigua'],
            'America/Araguaina' => ['en' => 'America/Araguaina', 'fr' => 'Amérique/Araguaina'],
            'America/Argentina/Buenos_Aires' => ['en' => 'America/Argentina/Buenos_Aires', 'fr' => 'Amérique/Argentine/Buenos_Aires'],
            'America/Argentina/Catamarca' => ['en' => 'America/Argentina/Catamarca', 'fr' => 'Amérique/Argentine/Catamarca'],
            'America/Argentina/ComodRivadavia' => ['en' => 'America/Argentina/ComodRivadavia', 'fr' => 'Amérique/Argentine/ComodRivadavia'],
            'America/Argentina/Cordoba' => ['en' => 'America/Argentina/Cordoba', 'fr' => 'Amérique/Argentine/Cordoba'],
            'America/Argentina/Jujuy' => ['en' => 'America/Argentina/Jujuy', 'fr' => 'Amérique/Argentine/Jujuy'],
            'America/Argentina/La_Rioja' => ['en' => 'America/Argentina/La_Rioja', 'fr' => 'Amérique/Argentine/La_Rioja'],
            'America/Argentina/Mendoza' => ['en' => 'America/Argentina/Mendoza', 'fr' => 'Amérique/Argentine/Mendoza'],
            'America/Argentina/Rio_Gallegos' => ['en' => 'America/Argentina/Rio_Gallegos', 'fr' => 'Amérique/Argentine/Rio_Gallegos'],
            'America/Argentina/Salta' => ['en' => 'America/Argentina/Salta', 'fr' => 'Amérique/Argentine/Salta'],
            'America/Argentina/San_Juan' => ['en' => 'America/Argentina/San_Juan', 'fr' => 'Amérique/Argentine/San_Juan'],
            'America/Argentina/San_Luis' => ['en' => 'America/Argentina/San_Luis', 'fr' => 'Amérique/Argentine/San_Luis'],
            'America/Argentina/Tucuman' => ['en' => 'America/Argentina/Tucuman', 'fr' => 'Amérique/Argentine/Tucuman'],
            'America/Argentina/Ushuaia' => ['en' => 'America/Argentina/Ushuaia', 'fr' => 'Amérique/Argentine/Ushuaia'],
            'America/Aruba' => ['en' => 'America/Aruba', 'fr' => 'Amérique/Aruba'],
            'America/Asuncion' => ['en' => 'America/Asuncion', 'fr' => 'Amérique/Asuncion'],
            'America/Atikokan' => ['en' => 'America/Atikokan', 'fr' => 'Amérique/Atikokan'],
            'America/Atka' => ['en' => 'America/Atka', 'fr' => 'Amérique/Atka'],
            'America/Bahia' => ['en' => 'America/Bahia', 'fr' => 'Amérique/Bahia'],
            'America/Bahia_Banderas' => ['en' => 'America/Bahia_Banderas', 'fr' => 'Amérique/Bahia_Banderas'],
            'America/Barbados' => ['en' => 'America/Barbados', 'fr' => 'Amérique/Barbados'],
            'America/Belem' => ['en' => 'America/Belem', 'fr' => 'Amérique/Belem'],
            'America/Belize' => ['en' => 'America/Belize', 'fr' => 'Amérique/Belize'],
            'America/Blanc-Sablon' => ['en' => 'America/Blanc-Sablon', 'fr' => 'Amérique/Blanc-Sablon'],
            'America/Boa_Vista' => ['en' => 'America/Boa_Vista', 'fr' => 'Amérique/Boa_Vista'],
            'America/Bogota' => ['en' => 'America/Bogota', 'fr' => 'Amérique/Bogota'],
            'America/Boise' => ['en' => 'America/Boise', 'fr' => 'Amérique/Boise'],
            'America/Buenos_Aires' => ['en' => 'America/Buenos_Aires', 'fr' => 'Amérique/Buenos_Aires'],
            'America/Cambridge_Bay' => ['en' => 'America/Cambridge_Bay', 'fr' => 'Amérique/Cambridge_Bay'],
            'America/Campo_Grande' => ['en' => 'America/Campo_Grande', 'fr' => 'Amérique/Campo_Grande'],
            'America/Cancun' => ['en' => 'America/Cancun', 'fr' => 'Amérique/Cancun'],
            'America/Caracas' => ['en' => 'America/Caracas', 'fr' => 'Amérique/Caracas'],
            'America/Catamarca' => ['en' => 'America/Catamarca', 'fr' => 'Amérique/Catamarca'],
            'America/Cayenne' => ['en' => 'America/Cayenne', 'fr' => 'Amérique/Cayenne'],
            'America/Cayman' => ['en' => 'America/Cayman', 'fr' => 'Amérique/Cayman'],
            'America/Chicago' => ['en' => 'America/Chicago', 'fr' => 'Amérique/Chicago'],
            'America/Chihuahua' => ['en' => 'America/Chihuahua', 'fr' => 'Amérique/Chihuahua'],
            'America/Coral_Harbour' => ['en' => 'America/Coral_Harbour', 'fr' => 'Amérique/Coral_Harbour'],
            'America/Cordoba' => ['en' => 'America/Cordoba', 'fr' => 'Amérique/Cordoba'],
            'America/Costa_Rica' => ['en' => 'America/Costa_Rica', 'fr' => 'Amérique/Costa_Rica'],
            'America/Creston' => ['en' => 'America/Creston', 'fr' => 'Amérique/Creston'],
            'America/Cuiaba' => ['en' => 'America/Cuiaba', 'fr' => 'Amérique/Cuiaba'],
            'America/Curacao' => ['en' => 'America/Curacao', 'fr' => 'Amérique/Curacao'],
            'America/Danmarkshavn' => ['en' => 'America/Danmarkshavn', 'fr' => 'Amérique/Danmarkshavn'],
            'America/Dawson' => ['en' => 'America/Dawson', 'fr' => 'Amérique/Dawson'],
            'America/Dawson_Creek' => ['en' => 'America/Dawson_Creek', 'fr' => 'Amérique/Dawson_Creek'],
            'America/Denver' => ['en' => 'America/Denver', 'fr' => 'Amérique/Denver'],
            'America/Detroit' => ['en' => 'America/Detroit', 'fr' => 'Amérique/Detroit'],
            'America/Dominica' => ['en' => 'America/Dominica', 'fr' => 'Amérique/Dominica'],
            'America/Edmonton' => ['en' => 'America/Edmonton', 'fr' => 'Amérique/Edmonton'],
            'America/Eirunepe' => ['en' => 'America/Eirunepe', 'fr' => 'Amérique/Eirunepe'],
            'America/El_Salvador' => ['en' => 'America/El_Salvador', 'fr' => 'Amérique/El_Salvador'],
            'America/Ensenada' => ['en' => 'America/Ensenada', 'fr' => 'Amérique/Ensenada'],
            'America/Fort_Nelson' => ['en' => 'America/Fort_Nelson', 'fr' => 'Amérique/Fort_Nelson'],
            'America/Fort_Wayne' => ['en' => 'America/Fort_Wayne', 'fr' => 'Amérique/Fort_Wayne'],
            'America/Fortaleza' => ['en' => 'America/Fortaleza', 'fr' => 'Amérique/Fortaleza'],
            'America/Glace_Bay' => ['en' => 'America/Glace_Bay', 'fr' => 'Amérique/Glace_Bay'],
            'America/Godthab' => ['en' => 'America/Godthab', 'fr' => 'Amérique/Godthab'],
            'America/Goose_Bay' => ['en' => 'America/Goose_Bay', 'fr' => 'Amérique/Goose_Bay'],
            'America/Grand_Turk' => ['en' => 'America/Grand_Turk', 'fr' => 'Amérique/Grand_Turk'],
            'America/Grenada' => ['en' => 'America/Grenada', 'fr' => 'Amérique/Grenada'],
            'America/Guadeloupe' => ['en' => 'America/Guadeloupe', 'fr' => 'Amérique/Guadeloupe'],
            'America/Guatemala' => ['en' => 'America/Guatemala', 'fr' => 'Amérique/Guatemala'],
            'America/Guayaquil' => ['en' => 'America/Guayaquil', 'fr' => 'Amérique/Guayaquil'],
            'America/Guyana' => ['en' => 'America/Guyana', 'fr' => 'Amérique/Guyane'],
            'America/Halifax' => ['en' => 'America/Halifax', 'fr' => 'Amérique/Halifax'],
            'America/Havana' => ['en' => 'America/Havana', 'fr' => 'Amérique/La_Havane'],
            'America/Hermosillo' => ['en' => 'America/Hermosillo', 'fr' => 'Amérique/Hermosillo'],
            'America/Indiana/Indianapolis' => ['en' => 'America/Indiana/Indianapolis', 'fr' => 'Amérique/Indiana/Indianapolis'],
            'America/Indiana/Knox' => ['en' => 'America/Indiana/Knox', 'fr' => 'Amérique/Indiana/Knox'],
            'America/Indiana/Marengo' => ['en' => 'America/Indiana/Marengo', 'fr' => 'Amérique/Indiana/Marengo'],
            'America/Indiana/Petersburg' => ['en' => 'America/Indiana/Petersburg', 'fr' => 'Amérique/Indiana/Petersburg'],
            'America/Indiana/Tell_City' => ['en' => 'America/Indiana/Tell_City', 'fr' => 'Amérique/Indiana/Tell_City'],
            'America/Indiana/Vevay' => ['en' => 'America/Indiana/Vevay', 'fr' => 'Amérique/Indiana/Vevay'],
            'America/Indiana/Vincennes' => ['en' => 'America/Indiana/Vincennes', 'fr' => 'Amérique/Indiana/Vincennes'],
            'America/Indiana/Winamac' => ['en' => 'America/Indiana/Winamac', 'fr' => 'Amérique/Indiana/Winamac'],
            'America/Indianapolis' => ['en' => 'America/Indianapolis', 'fr' => 'Amérique/Indianapolis'],
            'America/Inuvik' => ['en' => 'America/Inuvik', 'fr' => 'Amérique/Inuvik'],
            'America/Iqaluit' => ['en' => 'America/Iqaluit', 'fr' => 'Amérique/Iqaluit'],
            'America/Jamaica' => ['en' => 'America/Jamaica', 'fr' => 'Amérique/Jamaïque'],
            'America/Jujuy' => ['en' => 'America/Jujuy', 'fr' => 'Amérique/Jujuy'],
            'America/Juneau' => ['en' => 'America/Juneau', 'fr' => 'Amérique/Juneau'],
            'America/Kentucky/Louisville' => ['en' => 'America/Kentucky/Louisville', 'fr' => 'Amérique/Kentucky/Louisville'],
            'America/Kentucky/Monticello' => ['en' => 'America/Kentucky/Monticello', 'fr' => 'Amérique/Kentucky/Monticello'],
            'America/Knox_IN' => ['en' => 'America/Knox_IN', 'fr' => 'Amérique/Knox_IN'],
            'America/Kralendijk' => ['en' => 'America/Kralendijk', 'fr' => 'Amérique/Kralendijk'],
            'America/La_Paz' => ['en' => 'America/La_Paz', 'fr' => 'Amérique/La_Paz'],
            'America/Lima' => ['en' => 'America/Lima', 'fr' => 'Amérique/Lima'],
            'America/Los_Angeles' => ['en' => 'America/Los_Angeles', 'fr' => 'Amérique/Los_Angeles'],
            'America/Louisville' => ['en' => 'America/Louisville', 'fr' => 'Amérique/Louisville'],
            'America/Lower_Princes' => ['en' => 'America/Lower_Princes', 'fr' => 'Amérique/Lower_Princes'],
            'America/Maceio' => ['en' => 'America/Maceio', 'fr' => 'Amérique/Maceio'],
            'America/Managua' => ['en' => 'America/Managua', 'fr' => 'Amérique/Managua'],
            'America/Manaus' => ['en' => 'America/Manaus', 'fr' => 'Amérique/Manaus'],
            'America/Marigot' => ['en' => 'America/Marigot', 'fr' => 'Amérique/Marigot'],
            'America/Martinique' => ['en' => 'America/Martinique', 'fr' => 'Amérique/Martinique'],
            'America/Matamoros' => ['en' => 'America/Matamoros', 'fr' => 'Amérique/Matamoros'],
            'America/Mazatlan' => ['en' => 'America/Mazatlan', 'fr' => 'Amérique/Mazatlan'],
            'America/Mendoza' => ['en' => 'America/Mendoza', 'fr' => 'Amérique/Mendoza'],
            'America/Menominee' => ['en' => 'America/Menominee', 'fr' => 'Amérique/Menominee'],
            'America/Merida' => ['en' => 'America/Merida', 'fr' => 'Amérique/Merida'],
            'America/Metlakatla' => ['en' => 'America/Metlakatla', 'fr' => 'Amérique/Metlakatla'],
            'America/Mexico_City' => ['en' => 'America/Mexico_City', 'fr' => 'Amérique/Mexico_City'],
            'America/Miquelon' => ['en' => 'America/Miquelon', 'fr' => 'Amérique/Miquelon'],
            'America/Moncton' => ['en' => 'America/Moncton', 'fr' => 'Amérique/Moncton'],
            'America/Monterrey' => ['en' => 'America/Monterrey', 'fr' => 'Amérique/Monterrey'],
            'America/Montevideo' => ['en' => 'America/Montevideo', 'fr' => 'Amérique/Montevideo'],
            'America/Montreal' => ['en' => 'America/Montreal', 'fr' => 'Amérique/Montréal'],
            'America/Montserrat' => ['en' => 'America/Montserrat', 'fr' => 'Amérique/Montserrat'],
            'America/Nassau' => ['en' => 'America/Nassau', 'fr' => 'Amérique/Nassau'],
            'America/New_York' => ['en' => 'America/New_York', 'fr' => 'Amérique/New_York'],
            'America/Nipigon' => ['en' => 'America/Nipigon', 'fr' => 'Amérique/Nipigon'],
            'America/Nome' => ['en' => 'America/Nome', 'fr' => 'Amérique/Nome'],
            'America/Noronha' => ['en' => 'America/Noronha', 'fr' => 'Amérique/Noronha'],
            'America/North_Dakota/Beulah' => ['en' => 'America/North_Dakota/Beulah', 'fr' => 'Amérique/Dakota_du_Nord/Beulah'],
            'America/North_Dakota/Center' => ['en' => 'America/North_Dakota/Center', 'fr' => 'Amérique/Dakota_du_Nord/Center'],
            'America/North_Dakota/New_Salem' => ['en' => 'America/North_Dakota/New_Salem', 'fr' => 'Amérique/Dakota_du_Nord/New_Salem'],
            'America/Ojinaga' => ['en' => 'America/Ojinaga', 'fr' => 'Amérique/Ojinaga'],
            'America/Panama' => ['en' => 'America/Panama', 'fr' => 'Amérique/Panama'],
            'America/Pangnirtung' => ['en' => 'America/Pangnirtung', 'fr' => 'Amérique/Pangnirtung'],
            'America/Paramaribo' => ['en' => 'America/Paramaribo', 'fr' => 'Amérique/Paramaribo'],
            'America/Phoenix' => ['en' => 'America/Phoenix', 'fr' => 'Amérique/Phoenix'],
            'America/Port-au-Prince' => ['en' => 'America/Port-au-Prince', 'fr' => 'Amérique/Port-au-Prince'],
            'America/Port_of_Spain' => ['en' => 'America/Port_of_Spain', 'fr' => "Amérique/Port-d'Espagne"],
            'America/Porto_Acre' => ['en' => 'America/Porto_Acre', 'fr' => 'Amérique/Porto_Acre'],
            'America/Porto_Velho' => ['en' => 'America/Porto_Velho', 'fr' => 'Amérique/Porto_Velho'],
            'America/Puerto_Rico' => ['en' => 'America/Puerto_Rico', 'fr' => 'Amérique/Puerto_Rico'],
            'America/Punta_Arenas' => ['en' => 'America/Punta_Arenas', 'fr' => 'Amérique/Punta_Arenas'],
            'America/Rainy_River' => ['en' => 'America/Rainy_River', 'fr' => 'Amérique/Rainy_River'],
            'America/Rankin_Inlet' => ['en' => 'America/Rankin_Inlet', 'fr' => 'Amérique/Rankin_Inlet'],
            'America/Recife' => ['en' => 'America/Recife', 'fr' => 'Amérique/Recife'],
            'America/Regina' => ['en' => 'America/Regina', 'fr' => 'Amérique/Regina'],
            'America/Resolute' => ['en' => 'America/Resolute', 'fr' => 'Amérique/Resolute'],
            'America/Rio_Branco' => ['en' => 'America/Rio_Branco', 'fr' => 'Amérique/Rio_Branco'],
            'America/Rosario' => ['en' => 'America/Rosario', 'fr' => 'Amérique/Rosario'],
            'America/Santa_Isabel' => ['en' => 'America/Santa_Isabel', 'fr' => 'Amérique/Santa_Isabel'],
            'America/Santarem' => ['en' => 'America/Santarem', 'fr' => 'Amérique/Santarem'],
            'America/Santiago' => ['en' => 'America/Santiago', 'fr' => 'Amérique/Santiago'],
            'America/Santo_Domingo' => ['en' => 'America/Santo_Domingo', 'fr' => 'Amérique/Santo_Domingo'],
            'America/Sao_Paulo' => ['en' => 'America/Sao_Paulo', 'fr' => 'Amérique/Sao_Paulo'],
            'America/Scoresbysund' => ['en' => 'America/Scoresbysund', 'fr' => 'Amérique/Scoresbysund'],
            'America/Shiprock' => ['en' => 'America/Shiprock', 'fr' => 'Amérique/Shiprock'],
            'America/Sitka' => ['en' => 'America/Sitka', 'fr' => 'Amérique/Sitka'],
            'America/St_Barthelemy' => ['en' => 'America/St_Barthelemy', 'fr' => 'Amérique/St_Barthelemy'],
            'America/St_Johns' => ['en' => 'America/St_Johns', 'fr' => 'Amérique/St_Johns'],
            'America/St_Kitts' => ['en' => 'America/St_Kitts', 'fr' => 'Amérique/St_Kitts'],
            'America/St_Lucia' => ['en' => 'America/St_Lucia', 'fr' => 'Amérique/St_Lucia'],
            'America/St_Thomas' => ['en' => 'America/St_Thomas', 'fr' => 'Amérique/St_Thomas'],
            'America/St_Vincent' => ['en' => 'America/St_Vincent', 'fr' => 'Amérique/St_Vincent'],
            'America/Swift_Current' => ['en' => 'America/Swift_Current', 'fr' => 'Amérique/Swift_Current'],
            'America/Tegucigalpa' => ['en' => 'America/Tegucigalpa', 'fr' => 'Amérique/Tegucigalpa'],
            'America/Thule' => ['en' => 'America/Thule', 'fr' => 'Amérique/Thulé'],
            'America/Thunder_Bay' => ['en' => 'America/Thunder_Bay', 'fr' => 'Amérique/Thunder_Bay'],
            'America/Tijuana' => ['en' => 'America/Tijuana', 'fr' => 'Amérique/Tijuana'],
            'America/Toronto' => ['en' => 'America/Toronto', 'fr' => 'Amérique/Toronto'],
            'America/Tortola' => ['en' => 'America/Tortola', 'fr' => 'Amérique/Tortola'],
            'America/Vancouver' => ['en' => 'America/Vancouver', 'fr' => 'Amérique/Vancouver'],
            'America/Virgin' => ['en' => 'America/Virgin', 'fr' => 'Amérique/Virgin'],
            'America/Whitehorse' => ['en' => 'America/Whitehorse', 'fr' => 'Amérique/Whitehorse'],
            'America/Winnipeg' => ['en' => 'America/Winnipeg', 'fr' => 'Amérique/Winnipeg'],
            'America/Yakutat' => ['en' => 'America/Yakutat', 'fr' => 'Amérique/Yakutat'],
            'America/Yellowknife' => ['en' => 'America/Yellowknife', 'fr' => 'Amérique/Yellowknife'],
            'Antarctica/Casey' => ['en' => 'Antarctica/Casey', 'fr' => 'Antarctique/Casey'],
            'Antarctica/Davis' => ['en' => 'Antarctica/Davis', 'fr' => 'Antarctique/Davis'],
            'Antarctica/DumontDUrville' => ['en' => 'Antarctica/DumontDUrville', 'fr' => "Antarctique/Dumont-d'Urville"],
            'Antarctica/Macquarie' => ['en' => 'Antarctica/Macquarie', 'fr' => 'Antarctique/Macquarie'],
            'Antarctica/Mawson' => ['en' => 'Antarctica/Mawson', 'fr' => 'Antarctique/Mawson'],
            'Antarctica/McMurdo' => ['en' => 'Antarctica/McMurdo', 'fr' => 'Antarctique/McMurdo'],
            'Antarctica/Palmer' => ['en' => 'Antarctica/Palmer', 'fr' => 'Antarctique/Palmer'],
            'Antarctica/Rothera' => ['en' => 'Antarctica/Rothera', 'fr' => 'Antarctique/Rothera'],
            'Antarctica/South_Pole' => ['en' => 'Antarctica/South_Pole', 'fr' => 'Antarctique/Pôle_Sud'],
            'Antarctica/Syowa' => ['en' => 'Antarctica/Syowa', 'fr' => 'Antarctique/Syowa'],
            'Antarctica/Troll' => ['en' => 'Antarctica/Troll', 'fr' => 'Antarctique/Troll'],
            'Antarctica/Vostok' => ['en' => 'Antarctica/Vostok', 'fr' => 'Antarctique/Vostok'],
            'Arctic/Longyearbyen' => ['en' => 'Arctic/Longyearbyen', 'fr' => 'Arctique/Longyearbyen'],
            'Asia/Aden' => ['en' => 'Asia/Aden', 'fr' => 'Asie/Aden'],
            'Asia/Almaty' => ['en' => 'Asia/Almaty', 'fr' => 'Asie/Almaty'],
            'Asia/Amman' => ['en' => 'Asia/Amman', 'fr' => 'Asie/Amman'],
            'Asia/Anadyr' => ['en' => 'Asia/Anadyr', 'fr' => 'Asie/Anadyr'],
            'Asia/Aqtau' => ['en' => 'Asia/Aqtau', 'fr' => 'Asie/Aqtau'],
            'Asia/Aqtobe' => ['en' => 'Asia/Aqtobe', 'fr' => 'Asie/Aqtobe'],
            'Asia/Ashgabat' => ['en' => 'Asia/Ashgabat', 'fr' => 'Asie/Achgabat'],
            'Asia/Atyrau' => ['en' => 'Asia/Atyrau', 'fr' => 'Asie/Atyrau'],
            'Asia/Baghdad' => ['en' => 'Asia/Baghdad', 'fr' => 'Asie/Bagdad'],
            'Asia/Bahrain' => ['en' => 'Asia/Bahrain', 'fr' => 'Asie/Bahreïn'],
            'Asia/Baku' => ['en' => 'Asia/Baku', 'fr' => 'Asie/Bakou'],
            'Asia/Bangkok' => ['en' => 'Asia/Bangkok', 'fr' => 'Asie/Bangkok'],
            'Asia/Barnaul' => ['en' => 'Asia/Barnaul', 'fr' => 'Asie/Barnaoul'],
            'Asia/Beirut' => ['en' => 'Asia/Beirut', 'fr' => 'Asie/Beyrouth'],
            'Asia/Bishkek' => ['en' => 'Asia/Bishkek', 'fr' => 'Asie/Bichkek'],
            'Asia/Brunei' => ['en' => 'Asia/Brunei', 'fr' => 'Asie/Brunei'],
            'Asia/Calcutta' => ['en' => 'Asia/Calcutta', 'fr' => 'Asie/Calcutta'],
            'Asia/Chita' => ['en' => 'Asia/Chita', 'fr' => 'Asie/Tchita'],
            'Asia/Choibalsan' => ['en' => 'Asia/Choibalsan', 'fr' => 'Asie/Choibalsan'],
            'Asia/Colombo' => ['en' => 'Asia/Colombo', 'fr' => 'Asie/Colombo'],
            'Asia/Damascus' => ['en' => 'Asia/Damascus', 'fr' => 'Asie/Damas'],
            'Asia/Dhaka' => ['en' => 'Asia/Dhaka', 'fr' => 'Asie/Dacca'],
            'Asia/Dili' => ['en' => 'Asia/Dili', 'fr' => 'Asie/Dili'],
            'Asia/Dubai' => ['en' => 'Asia/Dubai', 'fr' => 'Asie/Dubaï'],
            'Asia/Dushanbe' => ['en' => 'Asia/Dushanbe', 'fr' => 'Asie/Douchanbé'],
            'Asia/Famagusta' => ['en' => 'Asia/Famagusta', 'fr' => 'Asie/Famagouste'],
            'Asia/Gaza' => ['en' => 'Asia/Gaza', 'fr' => 'Asie/Gaza'],
            'Asia/Hebron' => ['en' => 'Asia/Hebron', 'fr' => 'Asie/Hébron'],
            'Asia/Ho_Chi_Minh' => ['en' => 'Asia/Ho_Chi_Minh', 'fr' => 'Asie/Ho-Chi-Minh'],
            'Asia/Hong_Kong' => ['en' => 'Asia/Hong_Kong', 'fr' => 'Asie/Hong_Kong'],
            'Asia/Hovd' => ['en' => 'Asia/Hovd', 'fr' => 'Asie/Hovd'],
            'Asia/Irkutsk' => ['en' => 'Asia/Irkutsk', 'fr' => 'Asie/Irkoutsk'],
            'Asia/Istanbul' => ['en' => 'Asia/Istanbul', 'fr' => 'Asie/Istanbul'],
            'Asia/Jakarta' => ['en' => 'Asia/Jakarta', 'fr' => 'Asie/Djakarta'],
            'Asia/Jayapura' => ['en' => 'Asia/Jayapura', 'fr' => 'Asie/Jayapura'],
            'Asia/Jerusalem' => ['en' => 'Asia/Jerusalem', 'fr' => 'Asie/Jérusalem'],
            'Asia/Kabul' => ['en' => 'Asia/Kabul', 'fr' => 'Asie/Kaboul'],
            'Asia/Kamchatka' => ['en' => 'Asia/Kamchatka', 'fr' => 'Asie/Kamtchatka'],
            'Asia/Karachi' => ['en' => 'Asia/Karachi', 'fr' => 'Asie/Karachi'],
            'Asia/Katmandu' => ['en' => 'Asia/Katmandu', 'fr' => 'Asie/Katmandou'],
            'Asia/Khandyga' => ['en' => 'Asia/Khandyga', 'fr' => 'Asie/Khandyga'],
            'Asia/Kolkata' => ['en' => 'Asia/Kolkata', 'fr' => 'Asie/Calcutta'],
            'Asia/Krasnoyarsk' => ['en' => 'Asia/Krasnoyarsk', 'fr' => 'Asie/Krasnoïarsk'],
            'Asia/Kuala_Lumpur' => ['en' => 'Asia/Kuala_Lumpur', 'fr' => 'Asie/Kuala_Lumpur'],
            'Asia/Kuching' => ['en' => 'Asia/Kuching', 'fr' => 'Asie/Kuching'],
            'Asia/Kuwait' => ['en' => 'Asia/Kuwait', 'fr' => 'Asie/Koweït'],
            'Asia/Macau' => ['en' => 'Asia/Macau', 'fr' => 'Asie/Macao'],
            'Asia/Magadan' => ['en' => 'Asia/Magadan', 'fr' => 'Asie/Magadan'],
            'Asia/Makassar' => ['en' => 'Asia/Makassar', 'fr' => 'Asie/Makassar'],
            'Asia/Manila' => ['en' => 'Asia/Manila', 'fr' => 'Asie/Manille'],
            'Asia/Muscat' => ['en' => 'Asia/Muscat', 'fr' => 'Asie/Mascate'],
            'Asia/Nicosia' => ['en' => 'Asia/Nicosia', 'fr' => 'Asie/Nicosie'],
            'Asia/Novokuznetsk' => ['en' => 'Asia/Novokuznetsk', 'fr' => 'Asie/Novokouznetsk'],
            'Asia/Novosibirsk' => ['en' => 'Asia/Novosibirsk', 'fr' => 'Asie/Novossibirsk'],
            'Asia/Omsk' => ['en' => 'Asia/Omsk', 'fr' => 'Asie/Omsk'],
            'Asia/Oral' => ['en' => 'Asia/Oral', 'fr' => 'Asie/Oral'],
            'Asia/Phnom_Penh' => ['en' => 'Asia/Phnom_Penh', 'fr' => 'Asie/Phnom_Penh'],
            'Asia/Pontianak' => ['en' => 'Asia/Pontianak', 'fr' => 'Asie/Pontianak'],
            'Asia/Pyongyang' => ['en' => 'Asia/Pyongyang', 'fr' => 'Asie/Pyongyang'],
            'Asia/Qatar' => ['en' => 'Asia/Qatar', 'fr' => 'Asie/Qatar'],
            'Asia/Qostanay' => ['en' => 'Asia/Qostanay', 'fr' => 'Asie/Qostanaï'],
            'Asia/Qyzylorda' => ['en' => 'Asia/Qyzylorda', 'fr' => 'Asie/Qyzylorda'],
            'Asia/Riyadh' => ['en' => 'Asia/Riyadh', 'fr' => 'Asie/Riyad'],
            'Asia/Saigon' => ['en' => 'Asia/Saigon', 'fr' => 'Asie/Saïgon'],
            'Asia/Sakhalin' => ['en' => 'Asia/Sakhalin', 'fr' => 'Asie/Sakhaline'],
            'Asia/Samarkand' => ['en' => 'Asia/Samarkand', 'fr' => 'Asie/Samarcande'],
            'Asia/Seoul' => ['en' => 'Asia/Seoul', 'fr' => 'Asie/Séoul'],
            'Asia/Shanghai' => ['en' => 'Asia/Shanghai', 'fr' => 'Asie/Shanghai'],
            'Asia/Singapore' => ['en' => 'Asia/Singapore', 'fr' => 'Asie/Singapour'],
            'Asia/Srednekolymsk' => ['en' => 'Asia/Srednekolymsk', 'fr' => 'Asie/Srednekolymsk'],
            'Asia/Taipei' => ['en' => 'Asia/Taipei', 'fr' => 'Asie/Taipei'],
            'Asia/Tashkent' => ['en' => 'Asia/Tashkent', 'fr' => 'Asie/Tachkent'],
            'Asia/Tbilisi' => ['en' => 'Asia/Tbilisi', 'fr' => 'Asie/Tbilissi'],
            'Asia/Tehran' => ['en' => 'Asia/Tehran', 'fr' => 'Asie/Téhéran'],
            'Asia/Thimphu' => ['en' => 'Asia/Thimphu', 'fr' => 'Asie/Thimphou'],
            'Asia/Tokyo' => ['en' => 'Asia/Tokyo', 'fr' => 'Asie/Tokyo'],
            'Asia/Tomsk' => ['en' => 'Asia/Tomsk', 'fr' => 'Asie/Tomsk'],
            'Asia/Ulaanbaatar' => ['en' => 'Asia/Ulaanbaatar', 'fr' => 'Asie/Oulan-Bator'],
            'Asia/Urumqi' => ['en' => 'Asia/Urumqi', 'fr' => 'Asie/Ürümqi'],
            'Asia/Ust-Nera' => ['en' => 'Asia/Ust-Nera', 'fr' => 'Asie/Oust-Nera'],
            'Asia/Vientiane' => ['en' => 'Asia/Vientiane', 'fr' => 'Asie/Vientiane'],
            'Asia/Vladivostok' => ['en' => 'Asia/Vladivostok', 'fr' => 'Asie/Vladivostok'],
            'Asia/Yakutsk' => ['en' => 'Asia/Yakutsk', 'fr' => 'Asie/Iakoutsk'],
            'Asia/Yangon' => ['en' => 'Asia/Yangon', 'fr' => 'Asie/Yangon'],
            'Asia/Yekaterinburg' => ['en' => 'Asia/Yekaterinburg', 'fr' => 'Asie/Iekaterinbourg'],
            'Asia/Yerevan' => ['en' => 'Asia/Yerevan', 'fr' => 'Asie/Erevan'],
            'Atlantic/Azores' => ['en' => 'Atlantic/Azores', 'fr' => 'Atlantique/Açores'],
            'Atlantic/Bermuda' => ['en' => 'Atlantic/Bermuda', 'fr' => 'Atlantique/Bermudes'],
            'Atlantic/Canary' => ['en' => 'Atlantic/Canary', 'fr' => 'Atlantique/Canaries'],
            'Atlantic/Cape_Verde' => ['en' => 'Atlantic/Cape_Verde', 'fr' => 'Atlantique/Cap-Vert'],
            'Atlantic/Faeroe' => ['en' => 'Atlantic/Faeroe', 'fr' => 'Atlantique/Féroé'],
            'Atlantic/Faroe' => ['en' => 'Atlantic/Faroe', 'fr' => 'Atlantique/Féroé'],
            'Atlantic/Madeira' => ['en' => 'Atlantic/Madeira', 'fr' => 'Atlantique/Madeire'],
            'Atlantic/Reykjavik' => ['en' => 'Atlantic/Reykjavik', 'fr' => 'Atlantique/Reykjavik'],
            'Atlantic/South_Georgia' => ['en' => 'Atlantic/South_Georgia', 'fr' => 'Atlantique/Géorgie_du_Sud'],
            'Atlantic/St_Helena' => ['en' => 'Atlantic/St_Helena', 'fr' => 'Atlantique/Sainte-Hélène'],
            'Atlantic/Stanley' => ['en' => 'Atlantic/Stanley', 'fr' => 'Atlantique/Stanley'],
            'Australia/Adelaide' => ['en' => 'Australia/Adelaide', 'fr' => 'Australie/Adélaïde'],
            'Australia/Brisbane' => ['en' => 'Australia/Brisbane', 'fr' => 'Australie/Brisbane'],
            'Australia/Broken_Hill' => ['en' => 'Australia/Broken_Hill', 'fr' => 'Australie/Broken_Hill'],
            'Australia/Currie' => ['en' => 'Australia/Currie', 'fr' => 'Australie/Currie'],
            'Australia/Darwin' => ['en' => 'Australia/Darwin', 'fr' => 'Australie/Darwin'],
            'Australia/Eucla' => ['en' => 'Australia/Eucla', 'fr' => 'Australie/Eucla'],
            'Australia/Hobart' => ['en' => 'Australia/Hobart', 'fr' => 'Australie/Hobart'],
            'Australia/Lindeman' => ['en' => 'Australia/Lindeman', 'fr' => 'Australie/Lindeman'],
            'Australia/Lord_Howe' => ['en' => 'Australia/Lord_Howe', 'fr' => 'Australie/Lord_Howe'],
            'Australia/Melbourne' => ['en' => 'Australia/Melbourne', 'fr' => 'Australie/Melbourne'],
            'Australia/Perth' => ['en' => 'Australia/Perth', 'fr' => 'Australie/Perth'],
            'Australia/Sydney' => ['en' => 'Australia/Sydney', 'fr' => 'Australie/Sydney'],
            'CST6CDT' => ['en' => 'CST6CDT', 'fr' => 'CST6CDT'],
            'EST' => ['en' => 'EST', 'fr' => 'EST'],
            'EST5EDT' => ['en' => 'EST5EDT', 'fr' => 'EST5EDT'],
            'Etc/GMT' => ['en' => 'Etc/GMT', 'fr' => 'Etc/GMT'],
            'Etc/GMT+1' => ['en' => 'Etc/GMT+1', 'fr' => 'Etc/GMT+1'],
            'Etc/GMT+10' => ['en' => 'Etc/GMT+10', 'fr' => 'Etc/GMT+10'],
            'Etc/GMT+11' => ['en' => 'Etc/GMT+11', 'fr' => 'Etc/GMT+11'],
            'Etc/GMT+12' => ['en' => 'Etc/GMT+12', 'fr' => 'Etc/GMT+12'],
            'Etc/GMT+2' => ['en' => 'Etc/GMT+2', 'fr' => 'Etc/GMT+2'],
            'Etc/GMT+3' => ['en' => 'Etc/GMT+3', 'fr' => 'Etc/GMT+3'],
            'Etc/GMT+4' => ['en' => 'Etc/GMT+4', 'fr' => 'Etc/GMT+4'],
            'Etc/GMT+5' => ['en' => 'Etc/GMT+5', 'fr' => 'Etc/GMT+5'],
            'Etc/GMT+6' => ['en' => 'Etc/GMT+6', 'fr' => 'Etc/GMT+6'],
            'Etc/GMT+7' => ['en' => 'Etc/GMT+7', 'fr' => 'Etc/GMT+7'],
            'Etc/GMT+8' => ['en' => 'Etc/GMT+8', 'fr' => 'Etc/GMT+8'],
            'Etc/GMT+9' => ['en' => 'Etc/GMT+9', 'fr' => 'Etc/GMT+9'],
            'Etc/GMT-1' => ['en' => 'Etc/GMT-1', 'fr' => 'Etc/GMT-1'],
            'Etc/GMT-10' => ['en' => 'Etc/GMT-10', 'fr' => 'Etc/GMT-10'],
            'Etc/GMT-11' => ['en' => 'Etc/GMT-11', 'fr' => 'Etc/GMT-11'],
            'Etc/GMT-12' => ['en' => 'Etc/GMT-12', 'fr' => 'Etc/GMT-12'],
            'Etc/GMT-13' => ['en' => 'Etc/GMT-13', 'fr' => 'Etc/GMT-13'],
            'Etc/GMT-14' => ['en' => 'Etc/GMT-14', 'fr' => 'Etc/GMT-14'],
            'Etc/GMT-2' => ['en' => 'Etc/GMT-2', 'fr' => 'Etc/GMT-2'],
            'Etc/GMT-3' => ['en' => 'Etc/GMT-3', 'fr' => 'Etc/GMT-3'],
            'Etc/GMT-4' => ['en' => 'Etc/GMT-4', 'fr' => 'Etc/GMT-4'],
            'Etc/GMT-5' => ['en' => 'Etc/GMT-5', 'fr' => 'Etc/GMT-5'],
            'Etc/GMT-6' => ['en' => 'Etc/GMT-6', 'fr' => 'Etc/GMT-6'],
            'Etc/GMT-7' => ['en' => 'Etc/GMT-7', 'fr' => 'Etc/GMT-7'],
            'Etc/GMT-8' => ['en' => 'Etc/GMT-8', 'fr' => 'Etc/GMT-8'],
            'Etc/GMT-9' => ['en' => 'Etc/GMT-9', 'fr' => 'Etc/GMT-9'],
            'Etc/UTC' => ['en' => 'Etc/UTC', 'fr' => 'Etc/UTC'],
            'Europe/Amsterdam' => ['en' => 'Europe/Amsterdam', 'fr' => 'Europe/Amsterdam'],
            'Europe/Andorra' => ['en' => 'Europe/Andorra', 'fr' => 'Europe/Andorre'],
            'Europe/Astrakhan' => ['en' => 'Europe/Astrakhan', 'fr' => 'Europe/Astrakhan'],
            'Europe/Athens' => ['en' => 'Europe/Athens', 'fr' => 'Europe/Athènes'],
            'Europe/Belgrade' => ['en' => 'Europe/Belgrade', 'fr' => 'Europe/Belgrade'],
            'Europe/Berlin' => ['en' => 'Europe/Berlin', 'fr' => 'Europe/Berlin'],
            'Europe/Bratislava' => ['en' => 'Europe/Bratislava', 'fr' => 'Europe/Bratislava'],
            'Europe/Brussels' => ['en' => 'Europe/Brussels', 'fr' => 'Europe/Bruxelles'],
            'Europe/Bucharest' => ['en' => 'Europe/Bucharest', 'fr' => 'Europe/Bucarest'],
            'Europe/Budapest' => ['en' => 'Europe/Budapest', 'fr' => 'Europe/Budapest'],
            'Europe/Busingen' => ['en' => 'Europe/Busingen', 'fr' => 'Europe/Büsingen'],
            'Europe/Chisinau' => ['en' => 'Europe/Chisinau', 'fr' => 'Europe/Chisinau'],
            'Europe/Copenhagen' => ['en' => 'Europe/Copenhagen', 'fr' => 'Europe/Copenhague'],
            'Europe/Dublin' => ['en' => 'Europe/Dublin', 'fr' => 'Europe/Dublin'],
            'Europe/Gibraltar' => ['en' => 'Europe/Gibraltar', 'fr' => 'Europe/Gibraltar'],
            'Europe/Guernsey' => ['en' => 'Europe/Guernsey', 'fr' => 'Europe/Guernesey'],
            'Europe/Helsinki' => ['en' => 'Europe/Helsinki', 'fr' => 'Europe/Helsinki'],
            'Europe/Isle_of_Man' => ['en' => 'Europe/Isle_of_Man', 'fr' => 'Europe/Île_de_Man'],
            'Europe/Istanbul' => ['en' => 'Europe/Istanbul', 'fr' => 'Europe/Istanbul'],
            'Europe/Jersey' => ['en' => 'Europe/Jersey', 'fr' => 'Europe/Jersey'],
            'Europe/Kaliningrad' => ['en' => 'Europe/Kaliningrad', 'fr' => 'Europe/Kaliningrad'],
            'Europe/Kiev' => ['en' => 'Europe/Kiev', 'fr' => 'Europe/Kiev'],
            'Europe/Kirov' => ['en' => 'Europe/Kirov', 'fr' => 'Europe/Kirov'],
            'Europe/Lisbon' => ['en' => 'Europe/Lisbon', 'fr' => 'Europe/Lisbonne'],
            'Europe/Ljubljana' => ['en' => 'Europe/Ljubljana', 'fr' => 'Europe/Ljubljana'],
            'Europe/London' => ['en' => 'Europe/London', 'fr' => 'Europe/Londres'],
            'Europe/Luxembourg' => ['en' => 'Europe/Luxembourg', 'fr' => 'Europe/Luxembourg'],
            'Europe/Madrid' => ['en' => 'Europe/Madrid', 'fr' => 'Europe/Madrid'],
            'Europe/Malta' => ['en' => 'Europe/Malta', 'fr' => 'Europe/Malte'],
            'Europe/Mariehamn' => ['en' => 'Europe/Mariehamn', 'fr' => 'Europe/Mariehamn'],
            'Europe/Minsk' => ['en' => 'Europe/Minsk', 'fr' => 'Europe/Minsk'],
            'Europe/Monaco' => ['en' => 'Europe/Monaco', 'fr' => 'Europe/Monaco'],
            'Europe/Moscow' => ['en' => 'Europe/Moscow', 'fr' => 'Europe/Moscou'],
            'Europe/Oslo' => ['en' => 'Europe/Oslo', 'fr' => 'Europe/Oslo'],
            'Europe/Paris' => ['en' => 'Europe/Paris', 'fr' => 'Europe/Paris'],
            'Europe/Podgorica' => ['en' => 'Europe/Podgorica', 'fr' => 'Europe/Podgorica'],
            'Europe/Prague' => ['en' => 'Europe/Prague', 'fr' => 'Europe/Prague'],
            'Europe/Riga' => ['en' => 'Europe/Riga', 'fr' => 'Europe/Riga'],
            'Europe/Rome' => ['en' => 'Europe/Rome', 'fr' => 'Europe/Rome'],
            'Europe/Samara' => ['en' => 'Europe/Samara', 'fr' => 'Europe/Samara'],
            'Europe/San_Marino' => ['en' => 'Europe/San_Marino', 'fr' => 'Europe/Saint-Marin'],
            'Europe/Sarajevo' => ['en' => 'Europe/Sarajevo', 'fr' => 'Europe/Sarajevo'],
            'Europe/Saratov' => ['en' => 'Europe/Saratov', 'fr' => 'Europe/Saratov'],
            'Europe/Simferopol' => ['en' => 'Europe/Simferopol', 'fr' => 'Europe/Simféropol'],
            'Europe/Skopje' => ['en' => 'Europe/Skopje', 'fr' => 'Europe/Skopje'],
            'Europe/Sofia' => ['en' => 'Europe/Sofia', 'fr' => 'Europe/Sofia'],
            'Europe/Stockholm' => ['en' => 'Europe/Stockholm', 'fr' => 'Europe/Stockholm'],
            'Europe/Tallinn' => ['en' => 'Europe/Tallinn', 'fr' => 'Europe/Tallinn'],
            'Europe/Tirane' => ['en' => 'Europe/Tirane', 'fr' => 'Europe/Tirana'],
            'Europe/Ulyanovsk' => ['en' => 'Europe/Ulyanovsk', 'fr' => 'Europe/Oulianovsk'],
            'Europe/Uzhgorod' => ['en' => 'Europe/Uzhgorod', 'fr' => 'Europe/Oujgorod'],
            'Europe/Vaduz' => ['en' => 'Europe/Vaduz', 'fr' => 'Europe/Vaduz'],
            'Europe/Vatican' => ['en' => 'Europe/Vatican', 'fr' => 'Europe/Vatican'],
            'Europe/Vienna' => ['en' => 'Europe/Vienna', 'fr' => 'Europe/Vienne'],
            'Europe/Vilnius' => ['en' => 'Europe/Vilnius', 'fr' => 'Europe/Vilnius'],
            'Europe/Volgograd' => ['en' => 'Europe/Volgograd', 'fr' => 'Europe/Volgograd'],
            'Europe/Warsaw' => ['en' => 'Europe/Warsaw', 'fr' => 'Europe/Varsovie'],
            'Europe/Zagreb' => ['en' => 'Europe/Zagreb', 'fr' => 'Europe/Zagreb'],
            'Europe/Zaporozhye' => ['en' => 'Europe/Zaporozhye', 'fr' => 'Europe/Zaporojie'],
            'Europe/Zurich' => ['en' => 'Europe/Zurich', 'fr' => 'Europe/Zurich'],
            'Indian/Antananarivo' => ['en' => 'Indian/Antananarivo', 'fr' => 'Indien/Antananarivo'],
            'Indian/Chagos' => ['en' => 'Indian/Chagos', 'fr' => 'Indien/Chagos'],
            'Indian/Christmas' => ['en' => 'Indian/Christmas', 'fr' => 'Indien/Christmas'],
            'Indian/Cocos' => ['en' => 'Indian/Cocos', 'fr' => 'Indien/Cocos'],
            'Indian/Comoro' => ['en' => 'Indian/Comoro', 'fr' => 'Indien/Comores'],
            'Indian/Kerguelen' => ['en' => 'Indian/Kerguelen', 'fr' => 'Indien/Kerguelen'],
            'Indian/Mahe' => ['en' => 'Indian/Mahe', 'fr' => 'Indien/Mahe'],
            'Indian/Maldives' => ['en' => 'Indian/Maldives', 'fr' => 'Indien/Maldives'],
            'Indian/Mauritius' => ['en' => 'Indian/Mauritius', 'fr' => 'Indien/Maurice'],
            'Indian/Mayotte' => ['en' => 'Indian/Mayotte', 'fr' => 'Indien/Mayotte'],
            'Indian/Reunion' => ['en' => 'Indian/Reunion', 'fr' => 'Indien/Réunion'],
            'MST' => ['en' => 'MST', 'fr' => 'MST'],
            'MST7MDT' => ['en' => 'MST7MDT', 'fr' => 'MST7MDT'],
            'PST8PDT' => ['en' => 'PST8PDT', 'fr' => 'PST8PDT'],
            'Pacific/Apia' => ['en' => 'Pacific/Apia', 'fr' => 'Pacifique/Apia'],
            'Pacific/Auckland' => ['en' => 'Pacific/Auckland', 'fr' => 'Pacifique/Auckland'],
            'Pacific/Bougainville' => ['en' => 'Pacific/Bougainville', 'fr' => 'Pacifique/Bougainville'],
            'Pacific/Chatham' => ['en' => 'Pacific/Chatham', 'fr' => 'Pacifique/Chatham'],
            'Pacific/Chuuk' => ['en' => 'Pacific/Chuuk', 'fr' => 'Pacifique/Chuuk'],
            'Pacific/Easter' => ['en' => 'Pacific/Easter', 'fr' => 'Pacifique/Île_de_Pâques'],
            'Pacific/Efate' => ['en' => 'Pacific/Efate', 'fr' => 'Pacifique/Éfaté'],
            'Pacific/Enderbury' => ['en' => 'Pacific/Enderbury', 'fr' => 'Pacifique/Enderbury'],
            'Pacific/Fakaofo' => ['en' => 'Pacific/Fakaofo', 'fr' => 'Pacifique/Fakaofo'],
            'Pacific/Fiji' => ['en' => 'Pacific/Fiji', 'fr' => 'Pacifique/Fidji'],
            'Pacific/Funafuti' => ['en' => 'Pacific/Funafuti', 'fr' => 'Pacifique/Funafuti'],
            'Pacific/Galapagos' => ['en' => 'Pacific/Galapagos', 'fr' => 'Pacifique/Galápagos'],
            'Pacific/Gambier' => ['en' => 'Pacific/Gambier', 'fr' => 'Pacifique/Gambier'],
            'Pacific/Guadalcanal' => ['en' => 'Pacific/Guadalcanal', 'fr' => 'Pacifique/Guadalcanal'],
            'Pacific/Guam' => ['en' => 'Pacific/Guam', 'fr' => 'Pacifique/Guam'],
            'Pacific/Honolulu' => ['en' => 'Pacific/Honolulu', 'fr' => 'Pacifique/Honolulu'],
            'Pacific/Kiritimati' => ['en' => 'Pacific/Kiritimati', 'fr' => 'Pacifique/Kiritimati'],
            'Pacific/Kosrae' => ['en' => 'Pacific/Kosrae', 'fr' => 'Pacifique/Kosrae'],
            'Pacific/Kwajalein' => ['en' => 'Pacific/Kwajalein', 'fr' => 'Pacifique/Kwajalein'],
            'Pacific/Majuro' => ['en' => 'Pacific/Majuro', 'fr' => 'Pacifique/Majuro'],
            'Pacific/Marquesas' => ['en' => 'Pacific/Marquesas', 'fr' => 'Pacifique/Marquises'],
            'Pacific/Nauru' => ['en' => 'Pacific/Nauru', 'fr' => 'Pacifique/Nauru'],
            'Pacific/Niue' => ['en' => 'Pacific/Niue', 'fr' => 'Pacifique/Niue'],
            'Pacific/Norfolk' => ['en' => 'Pacific/Norfolk', 'fr' => 'Pacifique/Norfolk'],
            'Pacific/Noumea' => ['en' => 'Pacific/Noumea', 'fr' => 'Pacifique/Nouméa'],
            'Pacific/Pago_Pago' => ['en' => 'Pacific/Pago_Pago', 'fr' => 'Pacifique/Pago_Pago'],
            'Pacific/Palau' => ['en' => 'Pacific/Palau', 'fr' => 'Pacifique/Palaos'],
            'Pacific/Pitcairn' => ['en' => 'Pacific/Pitcairn', 'fr' => 'Pacifique/Pitcairn'],
            'Pacific/Pohnpei' => ['en' => 'Pacific/Pohnpei', 'fr' => 'Pacifique/Pohnpei'],
            'Pacific/Port_Moresby' => ['en' => 'Pacific/Port_Moresby', 'fr' => 'Pacifique/Port_Moresby'],
            'Pacific/Rarotonga' => ['en' => 'Pacific/Rarotonga', 'fr' => 'Pacifique/Rarotonga'],
            'Pacific/Saipan' => ['en' => 'Pacific/Saipan', 'fr' => 'Pacifique/Saipan'],
            'Pacific/Tahiti' => ['en' => 'Pacific/Tahiti', 'fr' => 'Pacifique/Tahiti'],
            'Pacific/Tarawa' => ['en' => 'Pacific/Tarawa', 'fr' => 'Pacifique/Tarawa'],
            'Pacific/Tongatapu' => ['en' => 'Pacific/Tongatapu', 'fr' => 'Pacifique/Tongatapu'],
            'Pacific/Wake' => ['en' => 'Pacific/Wake', 'fr' => 'Pacifique/Wake'],
            'Pacific/Wallis' => ['en' => 'Pacific/Wallis', 'fr' => 'Pacifique/Wallis'],
            'US/Alaska' => ['en' => 'US/Alaska', 'fr' => 'États-Unis/Alaska'],
            'US/Arizona' => ['en' => 'US/Arizona', 'fr' => 'États-Unis/Arizona'],
            'US/Central' => ['en' => 'US/Central', 'fr' => 'États-Unis/Central'],
            'US/Eastern' => ['en' => 'US/Eastern', 'fr' => 'États-Unis/Est'],
            'US/Hawaii' => ['en' => 'US/Hawaii', 'fr' => 'États-Unis/Hawaï'],
            'US/Mountain' => ['en' => 'US/Mountain', 'fr' => 'États-Unis/Montagnes'],
            'US/Pacific' => ['en' => 'US/Pacific', 'fr' => 'États-Unis/Pacifique'],
            'UTC' => ['en' => 'UTC', 'fr' => 'UTC'],
            'Universal' => ['en' => 'Universal', 'fr' => 'Universel'],
            'WET' => ['en' => 'WET', 'fr' => 'WET'],
            'Zulu' => ['en' => 'Zulu', 'fr' => 'Zoulou'],
        ];
    }

    public function getListTimezones(User $user): array
    {
        $timezones = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL_WITH_BC);
        $timezoneMapping = $this->getFrenchTimezoneMapping();

        $choices = [];
        foreach ($timezones as $timezone) {
            if ($user->getPreferredLang() === 'fr') {
                $displayName = $timezoneMapping[$timezone]['fr'] ?? $timezone;
            } else {
                $displayName = $timezoneMapping[$timezone]['en'] ?? $timezone;
            }
            $choices[$timezone] = preg_replace(['/[_]/', '/\//'], [' ', ', '], $displayName);
        }

        return $choices;
    }

    /**
     * Update session with registration page template information.
     *
     * @param providerSession          $session                  - The session to update
     * @param request                  $request                  - The request object containing the new data
     * @param registrationPageTemplate $registrationPageTemplate - The registration page template to set
     *
     * @return providerSession - The updated session
     */
    public function updateSessionWithRegistrationPageTemplate(ProviderSession $session, Request $request, RegistrationPageTemplate $registrationPageTemplate): ProviderSession
    {
        // Update the registration page template
        $session->setRegistrationPageTemplate($registrationPageTemplate);

        // Update the registration page banner
        $session->setRegistrationPageTemplateBanner($request->request->get('registrationPageTemplateBanner'));

        // Update the registration page illustration
        $session->setRegistrationPageTemplateIllustration($request->request->get('registrationPageTemplateIllustration'));

        // Update the registration page description2
        $session->getProviderSessionRegistrationPage()->setDescription2($request->request->get('providerSessionRegistrationPageDescription2'));

        // Update the registration page description
        $session->getProviderSessionRegistrationPage()->setDescription($request->request->get('providerSessionRegistrationPageDescription'));

        // Update the registration page places
        $session->getProviderSessionRegistrationPage()->setPlaces(intval($request->request->get('providerSessionRegistrationPagePlaces')));

        return $session;
    }

    /**
     * Calculates the minimum attendance duration required for a given session based on the session's duration and the required attendance percentage.
     *
     * @param ProviderSession $providerSession the provider session object containing the necessary data for calculation
     *
     * @return int The minimum required attendance duration in minutes. Returns 0 if the data does not allow for a valid calculation.
     *
     * @throws Exception
     */
    public function getDurationMinimumForPresence(ProviderSession $providerSession): int
    {
        // Retrieves the total duration of the session.
        $durationSession = $providerSession->getDuration();
        // Retrieves the required attendance percentage for the session.
        $percentage = $providerSession->getPercentageAttendance();

        // Checks if the session duration and percentage are logical values to prevent incorrect calculations.
        if ($durationSession <= 0 || $percentage <= 0 || $percentage > 100) {
            // Returns 0 to indicate a calculation error due to invalid data.
            throw new \Exception('Invalid data for duration or percentage of attendance for the session.');
        }

        // Calculates the minimum required attendance duration and returns the result.
        return (int) round($durationSession * $percentage / 100);
    }

    public function canUpdateSession(ProviderSession $session, User $user): bool
    {
        if ($user->isAnAdmin()) {
            return true;
        }

        if (!$user->isAManager() || $session->getReadableState() === ProviderSession::FINAL_STATUS_FINISHED) {
            return false;
        }

        return true;
    }
}
