<?php

namespace App\Service;

use App\Service\AsyncWorker\AsyncTaskService;
use App\Service\AsyncWorker\Tasks\ExportUserRGPDDataTask;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ExportUserService
{
    public function __construct(
        protected AsyncTaskService $asyncTaskService,
        protected Security $security
    ) {
    }

    public function createAsyncTasks(UserInterface $user, UserInterface $userOrigin): void
    {
        $this->asyncTaskService->publish(
            ExportUserRGPDDataTask::class,
            [
                'user' => $user->getId(),
                'userOrigin' => $userOrigin->getId(),
            ],
            userOrigin: $this->security->getUser()
        );
    }
}
