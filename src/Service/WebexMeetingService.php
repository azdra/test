<?php

namespace App\Service;

use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexParticipant;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Exception\ProviderServiceValidationException;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Model\ProviderResponse;
use App\Service\Connector\Cisco\WebexMeetingConnector;
use App\Service\Provider\Model\LicenceCheckerInterface;
use App\Service\Provider\Webex\WebexLicensingService;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class WebexMeetingService extends AbstractProviderService implements LicenceCheckerInterface
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected ValidatorInterface $validator,
        private WebexLicensingService $webexLicensingService,
        WebexMeetingConnector $webexMeetingConnector,
        LoggerInterface $logger
    ) {
        parent::__construct($entityManager, $validator, $webexMeetingConnector, $logger);
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof WebexConfigurationHolder;
    }

    /**
     * @throws ProviderServiceValidationException
     * @throws ProviderSessionRequestHandlerException
     */
    public function createSession(ProviderSession $providerSession): ProviderResponse
    {
        if (!($providerSession instanceof WebexMeeting)) {
            return $this->makeUnsupportedProviderSessionResponse($providerSession);
        }

        /* @var WebexMeeting $providerSession */
        $this->webexLicensingService->licensingASession($providerSession);

        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['create']))) {
            throw new ProviderServiceValidationException($list);
        }

        $response = $this->connector->call($providerSession->getAbstractProviderConfigurationHolder(),
            WebexMeetingConnector::ACTION_CREATE_MEETING,
            $providerSession);

        if ($response->isSuccess()) {
            $this->connector->call(
                $providerSession->getAbstractProviderConfigurationHolder(),
                WebexMeetingConnector::ACTION_GET_MEETING_LINK,
                $providerSession
            );

            $responseAudio = $this->connector->call(
                $providerSession->getAbstractProviderConfigurationHolder(),
                WebexMeetingConnector::ACTION_TELEPHONY_PROFILE_INFO,
                $providerSession
            );
            $providerSession->setProviderAudio($responseAudio->getData());
            if (!empty($providerSession->getLicenceParticipantRole())) {
                $responseParticipantRole = $this->createParticipantSessionRole($providerSession->getLicenceParticipantRole());
                if (!$responseParticipantRole->isSuccess()) {
                    return $responseParticipantRole;
                }
            }

            $this->entityManager->persist($providerSession);
        }

        return $response;
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     * @throws ProviderServiceValidationException
     */
    public function updateSession(ProviderSession $providerSession): ProviderResponse
    {
        if (!($providerSession instanceof WebexMeeting)) {
            return $this->makeUnsupportedProviderSessionResponse($providerSession);
        }

        /* @var WebexMeeting $providerSession */
        $this->webexLicensingService->licensingASession($providerSession, $providerSession->getId());

        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        $response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            WebexMeetingConnector::ACTION_SET_MEETING,
            $providerSession
        );

        if ($response->isSuccess()) {
            if (!empty($providerSession->getLicenceParticipantRole()) && !$this->entityManager->contains($providerSession->getLicenceParticipantRole())) {
                $responseParticipantRole = $this->updateParticipantSessionRole(
                    $providerSession->getLicenceParticipantRole()
                );
                if (!$responseParticipantRole->isSuccess()) {
                    return $responseParticipantRole;
                }
            }
        }

        return $response;
    }

    public function getSession(ProviderSession $providerSession): ProviderResponse
    {
        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            WebexMeetingConnector::ACTION_GET_MEETING,
            $providerSession
        );
    }

    /**
     * @throws ProviderServiceValidationException
     */
    public function deleteSession(ProviderSession $providerSession): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['delete']))) {
            throw new ProviderServiceValidationException($list);
        }

        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            WebexMeetingConnector::ACTION_DELETE_MEETING,
            $providerSession
        );
    }

    public function getParticipant(WebexParticipant $providerParticipant): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($providerParticipant, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        // We don't persist here because this action is intended to update already existing participants
        return $this->connector->call(
            $providerParticipant->getConfigurationHolder(),
            WebexMeetingConnector::ACTION_GET_USER,
            $providerParticipant
        );
    }

    public function fetchHostJoinUrl(WebexMeeting $meeting): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($meeting, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        return $this->connector->call(
            $meeting->getAbstractProviderConfigurationHolder(),
            WebexMeetingConnector::ACTION_GET_HOST_URL_MEETING,
            $meeting
        );
    }

    public function fetchGuestJoinUrl(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate([$participantSessionRole->getSession(), $participantSessionRole->getParticipant()], [new Valid()], ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        return $this->connector->call(
            $participantSessionRole->getAbstractProviderConfigurationHolder(),
            WebexMeetingConnector::ACTION_GET_JOIN_URL_MEETING,
            $participantSessionRole
        );
    }

    /**
     * @throws ProviderServiceValidationException
     */
    public function getConnectorJoinUrl(WebexMeeting $webexMeeting): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($webexMeeting, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        /** @var WebexConfigurationHolder $configurationHolder */
        $configurationHolder = $webexMeeting->getAbstractProviderConfigurationHolder();

        return $this->connector->call(
            $configurationHolder,
            WebexMeetingConnector::ACTION_GET_CONNECTOR_JOIN_URL_MEETING,
            $webexMeeting->getMeetingKey()
        );
    }

    public function listAttendee(WebexMeeting $meeting): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($meeting, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        return $this->connector->call(
            $meeting->getAbstractProviderConfigurationHolder(),
            WebexMeetingConnector::ACTION_LIST_MEETING_ATTENDEE,
            $meeting
        );
    }

    public function createParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return (new ProviderResponse())->setSuccess(true);
    }

    public function updateParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return (new ProviderResponse())->setSuccess(true);
    }

    public function deleteParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        $sessionRoles = $providerParticipant->getSessionRoles();

        /** @var ProviderParticipantSessionRole $sessionRole */
        foreach ($sessionRoles as $index => $sessionRole) {
            $this->connector->call(
                $sessionRole->getAbstractProviderConfigurationHolder(),
                WebexMeetingConnector::ACTION_DELETE_MEETING_ATTENDEE,
                $sessionRole->setRole(ProviderParticipantSessionRole::ROLE_REMOVE)
            );

            $this->entityManager->remove($sessionRole);
        }

        $this->entityManager->remove($providerParticipant);

        return (new ProviderResponse())->setSuccess(true);
    }

    public function createParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole
    ): ProviderResponse {
        if (0 !== count($list = $this->validator->validate([$participantSessionRole->getSession(), $participantSessionRole->getParticipant()], [new Valid()], ['update']))) {
            throw new ProviderServiceValidationException($list);
        }
        $role = $participantSessionRole->getRole();

        return $this->connector->call(
            $participantSessionRole->getAbstractProviderConfigurationHolder(),
            WebexMeetingConnector::ACTION_CREATE_MEETING_ATTENDEE,
            $participantSessionRole->setRole($role)
        );
    }

    public function updateParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole
    ): ProviderResponse {
        if (0 !== count($list = $this->validator->validate([$participantSessionRole->getSession(), $participantSessionRole->getParticipant()], [new Valid()], ['update']))) {
            throw new ProviderServiceValidationException($list);
        }
        if ($participantSessionRole->getRole() === ProviderParticipantSessionRole::ROLE_REMOVE) {
            return $this->connector->call(
                $participantSessionRole->getAbstractProviderConfigurationHolder(),
                WebexMeetingConnector::ACTION_DELETE_MEETING_ATTENDEE,
                $participantSessionRole
            );
        } else {
            $role = $participantSessionRole->getRole();

            $this->connector->call(
                $participantSessionRole->getAbstractProviderConfigurationHolder(),
                WebexMeetingConnector::ACTION_DELETE_MEETING_ATTENDEE,
                $participantSessionRole->setRole(ProviderParticipantSessionRole::ROLE_REMOVE)
            );

            return $this->connector->call(
                $participantSessionRole->getAbstractProviderConfigurationHolder(),
                WebexMeetingConnector::ACTION_CREATE_MEETING_ATTENDEE,
                $participantSessionRole->setRole($role)
            );
        }
    }

    public function redirectParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole
    ): ProviderResponse {
        $response = $this->connector->call(
            $participantSessionRole->getAbstractProviderConfigurationHolder(),
            WebexMeetingConnector::ACTION_GET_JOIN_URL_MEETING,
            $participantSessionRole
        );

        return $response;
    }

    public function loginUser(WebexConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            WebexMeetingConnector::ACTION_USER_LOGIN_TICKET
        );
    }

    public function reportMeetingAttendance(WebexMeeting $webexMeeting): ProviderResponse
    {
        $response = $this->connector->call(
            $webexMeeting->getAbstractProviderConfigurationHolder(),
            WebexMeetingConnector::ACTION_LIST_MEETING_ATTENDEE_HISTORY,
            $webexMeeting
        );

        return $response;
    }

    public function makeUnsupportedProviderSessionResponse(ProviderSession $providerSession): ProviderResponse
    {
        return (new ProviderResponse())
            ->setSuccess(false)
            ->setThrown(new InvalidArgumentException(sprintf('%s is not supported by %s.', get_class($providerSession), self::class)));
    }

    public function checkLicence(ProviderSession $providerSession): void
    {
        // TODO: Implement checkLicence() method.
    }

    public function getAvailableLicence(
        AbstractProviderConfigurationHolder $configurationHolder,
        \DateTime $dateStart,
        \DateTime $dateEnd,
        ?int $excludeSessionId = null
    ): array {
        // TODO: Implement getAvailableLicence() method.
        return [];
    }
}
