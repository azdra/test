<?php

namespace App\Service;

use App\Entity\Adobe\AdobeConnectWebinarPrincipal;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Exception\AbstractProviderException;
use App\Exception\InvalidArgumentException;
use App\Exception\ProviderServiceValidationException;
use App\Model\ProviderResponse;
use App\Repository\AdobeConnectWebinarSCORepository;
use App\Repository\AdobeConnectWebinarTelephonyProfileRepository;
use App\Repository\UserRepository;
use App\Service\Connector\Adobe\AdobeConnectWebinarConnector;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdobeConnectWebinarService extends AbstractProviderService
{
    public const REDIS_SHARED_WEBINARS_TEMPLATE_KEY = 'shared_webinar_template_';
    public const REDIS_TIMEOUT_IN_SECONDS = 300;

    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected ValidatorInterface $validator,
        private AdobeConnectWebinarSCORepository $adobeConnectWebinarSCORepository,
        AdobeConnectWebinarConnector $connector,
        private AdobeConnectWebinarTelephonyProfileRepository $adobeConnectWebinarTelephonyProfileRepository,
        private \Redis $redisClient,
        private SerializerInterface $serializer,
        private string $appEnvironment,
        LoggerInterface $logger,
        private UserRepository $userRepository
    ) {
        parent::__construct($entityManager, $validator, $connector, $logger);
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof AdobeConnectWebinarConfigurationHolder;
    }

    public function createSession(ProviderSession $providerSession): ProviderResponse
    {
        return $this->syncSession($providerSession);
    }

    public function updateSession(ProviderSession $providerSession): ProviderResponse
    {
        return $this->syncSession($providerSession);
    }

    public function syncSession(AdobeConnectWebinarSCO $adobeConnectWebinarSCO): ProviderResponse
    {
        /*if (0 !== count($list = $this->validator->validate($adobeConnectWebinarSCO, groups: $adobeConnectWebinarSCO->getUniqueScoIdentifier() ? 'update' : 'create'))) {
            throw new ProviderServiceValidationException($list);
        }*/

        /** @var AdobeConnectWebinarConfigurationHolder $adobeConnectWebinarConfigurationHolder */
        $adobeConnectWebinarConfigurationHolder = $adobeConnectWebinarSCO->getAbstractProviderConfigurationHolder();

        //try {
        $folderScoId = $this->getOrCreateScoFolder($adobeConnectWebinarConfigurationHolder, $adobeConnectWebinarConfigurationHolder->getSavingMeetingFolderScoId(), $adobeConnectWebinarSCO->getRef());
        $adobeConnectWebinarSCO->setParentScoIdentifier($folderScoId);

        //dump($adobeConnectWebinarSCO->getParentScoIdentifier().' | '.$adobeConnectWebinarSCO->getDateStart()->format('d/m/Y H:i').' | '.$adobeConnectWebinarSCO->getDateEnd()->format('d/m/Y H:i'));
        //Create room SCO
        if (empty($adobeConnectWebinarSCO->getUniqueScoIdentifier())) {
            $response = $this->connector->call(
                    $adobeConnectWebinarConfigurationHolder,
                    AdobeConnectWebinarConnector::ACTION_CREATE_ROOM_SCO_MEETING,
                    $adobeConnectWebinarSCO
                );
            //dump('Step 1 : '.$response->isSuccess().' | '.$adobeConnectWebinarSCO->getUniqueScoIdentifier());

            //if ($responseCreateRoomSco->isSuccess() && $this->entityManager->getRepository(AdobeConnectWebinarSCO::class)->findOneBy(['uniqueScoIdentifier' => $adobeConnectWebinarSCO->getUniqueScoIdentifier()]) === null) {
            if ($response->isSuccess() && $adobeConnectWebinarSCO->getUniqueScoIdentifier() != null) {
                //Convert room SCO to room seminar
                $response = $this->connector->call(
                        $adobeConnectWebinarConfigurationHolder,
                        AdobeConnectWebinarConnector::ACTION_CONVERT_ROOM_SCO_MEETING_TO_ROOM_SEMINAR,
                        $adobeConnectWebinarSCO
                    );
                //dump('Step 2 : '.$response->isSuccess().' | '.$adobeConnectWebinarSCO->getSeminarScoIdentifier());
                if ($response->isSuccess() && $adobeConnectWebinarSCO->getUniqueScoIdentifier() != null) {
                    //Create event for room seminar
                    $response = $this->connector->call(
                            $adobeConnectWebinarConfigurationHolder,
                            AdobeConnectWebinarConnector::ACTION_CREATE_SESSION_SEMINAR,
                            $adobeConnectWebinarSCO
                        );
                    //dump('Step 3 : '.$response->isSuccess().' | '.$adobeConnectWebinarSCO->getSeminarScoIdentifier());
                    if ($response->isSuccess() && $adobeConnectWebinarSCO->getSeminarScoIdentifier() != null) {
                        //dump($adobeConnectWebinarSCO);
                        $changeResponse = $this->connector->call(
                            $adobeConnectWebinarConfigurationHolder,
                            AdobeConnectWebinarConnector::ACTION_SET_NBR_MAX_USERS_TO_WEBINAR,
                            $adobeConnectWebinarSCO
                        );
                    }
                    //dump('Step 4 : '.$response->isSuccess().' | '.$adobeConnectWebinarSCO->getSeminarScoIdentifier());
                    if ($response->isSuccess() && $adobeConnectWebinarSCO->getUniqueScoIdentifier() != null) {
                        //dump($adobeConnectWebinarSCO);
                        //$this->setConnectorInSession($adobeConnectWebinarSCO);
                        $this->entityManager->persist($adobeConnectWebinarSCO);
                    }
                }
            }
        } else {
            try {
                $response = $this->connector->call(
                    $adobeConnectWebinarConfigurationHolder,
                    AdobeConnectWebinarConnector::ACTION_SEMINAR_SESSION_SCO_UPDATE,
                    $adobeConnectWebinarSCO
                );
                if ($response->isSuccess() && $this->entityManager->getRepository(AdobeConnectWebinarSCO::class)->findOneBy(['uniqueScoIdentifier' => $adobeConnectWebinarSCO->getUniqueScoIdentifier()]) === null) {
                    $accessResponse = $this->connector->call(
                        $adobeConnectWebinarConfigurationHolder,
                        AdobeConnectWebinarConnector::ACTION_SCO_UPDATE_PERMISSION,
                        $adobeConnectWebinarSCO
                    );
                    if (!$accessResponse->isSuccess()) {
                        $adobeConnectWebinarSCO->setAdmittedSession(AdobeConnectWebinarSCO::PERMISSION_VERB_REMOVE);
                    }

                    $this->setConnectorInSession($adobeConnectWebinarSCO);
                    $this->entityManager->persist($adobeConnectWebinarSCO);
                }

                $this->connector->call(
                    $adobeConnectWebinarConfigurationHolder,
                    AdobeConnectWebinarConnector::ACTION_SET_SCO_TELEPHONY_PROFILE,
                    $adobeConnectWebinarSCO
                );

                return $response;
            } catch (\Throwable $exception) {
                $this->logger->error($exception, [
                    'adobeConnectWebinarConfigurationHolder ID ' => $adobeConnectWebinarConfigurationHolder->getId(),
                ]);

                return (new ProviderResponse(false))
                    ->setThrown($exception);
            }
        }

        return $response;
        /*} catch (\Throwable $exception) {
            $this->logger->error($exception, [
                'AdobeConnectWebinarConfigurationHolder ID ' => $AdobeConnectWebinarConfigurationHolder->getId(),
            ]);

            return (new ProviderResponse(false))
                ->setThrown($exception);
        }*/
    }

    /*public function findParticipantByEmail(AdobeConnectWebinarSCO $adobeConnectWebinarSCO): ProviderResponse
    {
        // @var AdobeConnectWebinarConfigurationHolder $configurationHolder
        $configurationHolder = $adobeConnectWebinarSCO->getAbstractProviderConfigurationHolder();

        $response = $this->connector->call(
            $configurationHolder,
            AdobeConnectWebinarConnector::ACTION_PRINCIPAL_LIST,
            [
                true,
                ['filter-email' => 'participant@gmail.com'],
            ]
        );

        return $response;
    }*/

    public function setConnectorInSession(AdobeConnectWebinarSCO $adobeConnectWebinarSCO): ProviderResponse
    {
        /** @var AdobeConnectWebinarConfigurationHolder $configurationHolder */
        $configurationHolder = $adobeConnectWebinarSCO->getAbstractProviderConfigurationHolder();

        return $this->connector->call(
            $configurationHolder,
            AdobeConnectWebinarConnector::ACTION_PERMISSIONS_UPDATE,
            [
                $adobeConnectWebinarSCO,
                (new AdobeConnectWebinarPrincipal($configurationHolder))->setPrincipalIdentifier($configurationHolder->getUsernameScoId()),
                AdobeConnectWebinarSCO::PERMISSION_VERB_HOST,
            ]
        );
    }

    public function deleteSession(ProviderSession $providerSession): ProviderResponse
    {
        $response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            AdobeConnectWebinarConnector::ACTION_DELETE_WEBINAR,
            $providerSession
        );

        return $response;
    }

    public function createParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return $this->syncPrincipal($providerParticipant, 'create');
    }

    public function updateParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return $this->syncPrincipal($providerParticipant, 'update');
    }

    public function syncPrincipal(AdobeConnectWebinarPrincipal $adobeConnectWebinarPrincipal, string $context): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($adobeConnectWebinarPrincipal, groups: [$context]))) {
            throw new ProviderServiceValidationException($list);
        }

        $response = $this->connector->call(
            $adobeConnectWebinarPrincipal->getConfigurationHolder(),
            AdobeConnectWebinarConnector::ACTION_PRINCIPAL_UPDATE,
            $adobeConnectWebinarPrincipal
        );
        if ($response->isSuccess() && $this->entityManager->getRepository(AdobeConnectWebinarPrincipal::class)->findOneBy(['principalIdentifier' => $adobeConnectWebinarPrincipal->getPrincipalIdentifier()]) === null) {
            $this->entityManager->persist($adobeConnectWebinarPrincipal);
        }

        return $response;
    }

    public function deleteParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        $response = $this->connector->call(
            $providerParticipant->getConfigurationHolder(),
            AdobeConnectWebinarConnector::ACTION_PRINCIPALS_DELETE,
            $providerParticipant
        );

        if ($response->isSuccess()) {
            $this->entityManager->remove($providerParticipant);
        }

        return $response;
    }

    public function updateParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse
    {
        $session = $participantSessionRole->getSession();
        $participant = $participantSessionRole->getParticipant();
        $role = $this->getPermissionIdByRole($participantSessionRole->getRole());

        if (0 !== count($list = $this->validator->validate([$session, $participant], [new Valid()], groups: 'update'))) {
            throw new ProviderServiceValidationException($list);
        }

        return $this->connector->call(
            $session->getAbstractProviderConfigurationHolder(),
            AdobeConnectWebinarConnector::ACTION_PERMISSIONS_UPDATE,
            [$session, $participant, $role]
        );
    }

    private function getPermissionIdByRole(string $role): string
    {
        return match ($role) {
            ProviderParticipantSessionRole::ROLE_ANIMATOR => AdobeConnectWebinarSCO::PERMISSION_VERB_HOST,
            ProviderParticipantSessionRole::ROLE_TRAINEE => AdobeConnectWebinarSCO::PERMISSION_VERB_VIEW,
            ProviderParticipantSessionRole::ROLE_REMOVE => AdobeConnectWebinarSCO::PERMISSION_VERB_REMOVE,
            default => AdobeConnectWebinarSCO::PERMISSION_VERB_DENIED
        };
    }

    public function redirectParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse
    {
        $providerSession = $participantSessionRole->getSession();
        $providerParticipant = $participantSessionRole->getParticipant();

        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            AdobeConnectWebinarConnector::ACTION_AUTHENTICATE_PRINCIPAL,
            [$providerSession, $providerParticipant]);
    }

    public function redirectLicence(ProviderSession $providerSession): ProviderResponse
    {
        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            AdobeConnectWebinarConnector::ACTION_AUTHENTICATE_LICENCE,
            [$providerSession]
        );
    }

    public function redirectConnector(AdobeConnectWebinarSCO $adobeConnectWebinarSCO): ProviderResponse
    {
        /** @var AdobeConnectWebinarConfigurationHolder $configurationHolder */
        $configurationHolder = $adobeConnectWebinarSCO->getAbstractProviderConfigurationHolder();

        return $this->connector->call(
            $configurationHolder,
            AdobeConnectWebinarConnector::ACTION_AUTHENTICATE_CONFIGURATION_HOLDER,
            $adobeConnectWebinarSCO);
    }

    public function getHostGroupPrincipal(AdobeConnectWebinarConfigurationHolder $configurationHolder): ProviderResponse
    {
        $response = $this->connector->call(
            $configurationHolder,
            AdobeConnectWebinarConnector::ACTION_PRINCIPAL_LIST,
            [
            true,
            ['filter-type' => AdobeConnectWebinarPrincipal::TYPE_LIVE_ADMIN],
            ]
        );

        return $response;
    }

    public function getAdminsGroupPrincipal(AdobeConnectWebinarConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            AdobeConnectWebinarConnector::ACTION_PRINCIPAL_LIST,
            [
                true,
                ['filter-type' => AdobeConnectWebinarPrincipal::TYPE_ADMIN],
            ]
        );
    }

    public function getLiveAdminsGroupPrincipal(AdobeConnectWebinarConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            AdobeConnectWebinarConnector::ACTION_PRINCIPAL_LIST,
            [
                true,
                ['filter-type' => AdobeConnectWebinarPrincipal::TYPE_LIVE_ADMIN],
            ]
        );
    }

    public function getSeminarAdminsGroupPrincipal(AdobeConnectWebinarConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            AdobeConnectWebinarConnector::ACTION_PRINCIPAL_LIST,
            [
                true,
                ['filter-type' => AdobeConnectWebinarPrincipal::TYPE_SEMINAR_ADMINS],
            ]
        );
    }

    public function getConnectorInformation(AdobeConnectWebinarConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            AdobeConnectWebinarConnector::ACTION_COMMON_INFO
        );
    }

    public function getPrincipalInHostGroup(AdobeConnectWebinarConfigurationHolder $configurationHolder): ProviderResponse
    {
        if (empty($configurationHolder->getHostGroupScoId())) {
            throw new InvalidArgumentException('The principal ID of the host group wasn\'t find');
        }

        $response = $this->connector->call(
            $configurationHolder,
            AdobeConnectWebinarConnector::ACTION_PRINCIPAL_LIST, [
            false,
            [
                'group-id' => $configurationHolder->getHostGroupScoId(),
                'filter-is-member' => 'true',
            ],
        ]);

        $this->synchronizeLicence($configurationHolder, $response->getData());

        return $response;
    }

    public function synchronizeLicence(AdobeConnectWebinarConfigurationHolder $configurationHolder, array $licences): void
    {
        $oneAdded = false;
        /** @var AdobeConnectWebinarPrincipal $participant */
        foreach ($licences as $participant) {
            $user = $this->userRepository->findOneBy(['email' => $participant->getEmail(), 'client' => $configurationHolder->getClient()]);
            if (!empty($user) && !array_key_exists($user->getEmail(), $configurationHolder->getLicences())) {
                $configurationHolder->addLicence($user->getEmail(), [
                    'name' => $participant->getName(),
                    'password' => '',
                    'shared' => false,
                    'user_id' => $user->getId(),
                ]);
                $oneAdded = true;
            }
        }
        if ($oneAdded) {
            $this->entityManager->flush();
        }
    }

    public function getSharedMeetingsTemplate(AdobeConnectWebinarConfigurationHolder $configurationHolder): ProviderResponse
    {
        $redisKey = self::REDIS_SHARED_WEBINARS_TEMPLATE_KEY.$configurationHolder->getId();

        if ($this->redisClient->exists($redisKey)) {
            $response = $this->serializer->deserialize($this->redisClient->get($redisKey), ProviderResponse::class, 'json');
        } else {
            $parentScoId = $configurationHolder->getUsernameScoId();

            foreach (AdobeConnectWebinarConnector::ROOT_PATH_TEMPLATE_FOLDER as $folderName) {
                $currentScoId = $this->getOrCreateScoFolder($configurationHolder, $parentScoId, $folderName);
                $parentScoId = $currentScoId;
            }

            $response = $this->connector->call(
                $configurationHolder,
                AdobeConnectWebinarConnector::ACTION_SCO_NAME_LIST_FROM_PARENT_SCO,
                $parentScoId
            );

            return $response;

            if ($response->isSuccess()) {
                $this->redisClient->setex($redisKey, self::REDIS_TIMEOUT_IN_SECONDS, $this->serializer->serialize($response, 'json'));
            }
        }

        return $response;
    }

    public function deletePrincipal(AdobeConnectWebinarPrincipal $adobeConnectWebinarPrincipal): ProviderResponse
    {
        $response = $this->connector->call(
            $adobeConnectWebinarPrincipal->getConfigurationHolder(),
            AdobeConnectWebinarConnector::ACTION_PRINCIPALS_DELETE,
            $adobeConnectWebinarPrincipal
        );

        if ($response->isSuccess()) {
            $this->entityManager->remove($adobeConnectWebinarPrincipal);
        }

        return $response;
    }

    public function principalListByLogin(AdobeConnectWebinarConfigurationHolder $configurationHolder, string $email): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            AdobeConnectWebinarConnector::ACTION_PRINCIPAL_LIST,
            [
                false,
                ['filter-login' => $email],
            ]
        );
    }

    public function reportMeetingAttendance(AdobeConnectWebinarSCO $adobeConnectWebinarSCO): ProviderResponse
    {
        $response = $this->connector->call(
            $adobeConnectWebinarSCO->getAbstractProviderConfigurationHolder(),
            AdobeConnectWebinarConnector::ACTION_REPORT_MEETING_ATTENDANCE,
            $adobeConnectWebinarSCO
        );

        return $response;
    }

    public function reportActiveMeetings(): array
    {
        return $this->adobeConnectWebinarSCORepository->getActiveMeetings();
    }

    public function getAllAudiosToCreate(AdobeConnectWebinarConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call($configurationHolder, AdobeConnectWebinarConnector::ACTION_TELEPHONY_PROFILE_LIST);
    }

    public function checkProviderAudioAvailabilityForDate(AdobeConnectWebinarSCO $providerSession, $start, $end): bool
    {
        return $this->adobeConnectWebinarSCORepository->checkProviderAudioAvailabilityForDate($providerSession, $start, $end);
    }

    public function getAvailableAudios(DateTime $dateBegin, DateTime $dateEnd, string $type, string $lang, AdobeConnectWebinarConfigurationHolder $configurationHolder): array
    {
        $freeAudios = [];

        if ($type == AbstractProviderAudio::AUDIO_VOIP || $type == null) {
            return $freeAudios;
        }

        $audios = $this->adobeConnectWebinarTelephonyProfileRepository->findBy([
            'phoneLang' => $lang,
            'connectionType' => $type,
            'configurationHolder' => $configurationHolder,
        ]);

        $sessions = $this->adobeConnectWebinarSCORepository->getSCOBetweenDateForAudioFromAdobeConnect($dateBegin, $dateEnd, $type);

        foreach ($audios as $audio) {
            $free = true;
            $audioProfileIdentifier = $audio?->getProfileIdentifier();

            if (!is_null($audioProfileIdentifier)) {
                foreach ($sessions as $session) {
                    $sessionProfileIdentifier = $session?->getProviderAudio()?->getProfileIdentifier();
                    if (!is_null($sessionProfileIdentifier)) {
                        if ($audioProfileIdentifier == $sessionProfileIdentifier) {
                            $free = false;
                            break;
                        }
                    }
                }
            }
            if ($free) {
                $freeAudios[] = $audio;
            }
        }

        return $freeAudios;
    }

    /**
     * @throws \Exception
     */
    public function getSavingFolderScoId(AdobeConnectWebinarConfigurationHolder $configurationHolder): ProviderResponse
    {
        try {
            $parentScoId = $configurationHolder->getUsernameScoId();

            foreach (AdobeConnectWebinarConnector::ROOT_PATH_SAVE_MEETING as $folderName) {
                $currentScoId = $this->getOrCreateScoFolder($configurationHolder, $parentScoId, $folderName);
                $parentScoId = $currentScoId;
            }

            return new ProviderResponse(data: $this->getOrCreateScoFolder($configurationHolder, $parentScoId, $this->appEnvironment));
        } catch (AbstractProviderException $exception) {
            $this->logger->error($exception);

            return (new ProviderResponse(false))
                ->setThrown($exception);
        }
    }

    /**
     * @throws AbstractProviderException|\Exception
     */
    public function getOrCreateScoFolder(AdobeConnectWebinarConfigurationHolder $configurationHolder, int $parentFolderID, string $name): int
    {
        $response = $this->connector->call($configurationHolder, AdobeConnectWebinarConnector::ACTION_SCO_FOLDER_EXIST, [$parentFolderID, $name]);

        if (!$response->isSuccess()) {
            $exception = $response->getThrown() ?? new \Exception('Unknown error return while finding folder');
            $this->logger->error($exception);
            throw $response->getThrown() ?? new \Exception('Unknown error return while finding folder');
        }

        $folderFound = $response->getData();

        if (!is_null($folderFound)) {
            return $folderFound;
        }

        $response = $this->connector->call($configurationHolder, AdobeConnectWebinarConnector::ACTION_SCO_CREATE_FOLDER, [$parentFolderID, $name]);

        if (!$response->isSuccess()) {
            $exception = $response->getThrown() ?? new \Exception('Unknown error return while creating folder');
            $this->logger->error($exception);
            throw $response->getThrown() ?? new \Exception('Unknown error return while creating folder');
        }

        return $response->getData();
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getAllAdminsGroupsPrincipal(AdobeConnectWebinarConfigurationHolder $configurationHolder): array
    {
        $liveAdminsGroup = $this->getLiveAdminsGroupPrincipal($configurationHolder)->getData() ?? [];
        $adminsGroup = $this->getAdminsGroupPrincipal($configurationHolder)->getData() ?? [];
        $seminarAdminsGroup = $this->getSeminarAdminsGroupPrincipal($configurationHolder)->getData() ?? [];

        return array_merge($liveAdminsGroup, $adminsGroup, $seminarAdminsGroup);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function isAllowToUpdatePassword(AdobeConnectWebinarPrincipal $adobeConnectWebinarPrincipal, AdobeConnectWebinarConfigurationHolder $configurationHolder): bool
    {
        $liveAdminsGroup = $this->getLiveAdminsGroupPrincipal($configurationHolder)->getData() ?? [];
        $adminsGroup = $this->getAdminsGroupPrincipal($configurationHolder)->getData() ?? [];
        $seminarAdminsGroup = $this->getSeminarAdminsGroupPrincipal($configurationHolder)->getData() ?? [];

        return in_array($adobeConnectWebinarPrincipal, array_merge($liveAdminsGroup, $adminsGroup, $seminarAdminsGroup));
    }
}
