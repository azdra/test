<?php

namespace App\Service;

use App\Entity\ActivityLog;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Model\ActivityLog\ActivityLogModel;
use App\Service\Exporter\ExporterFactory;
use App\Service\Exporter\Pdf\PdfCertificateProviderParticipantSessionRoleExporter;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SendTrainingCertificatesService
{
    public function __construct(
        private MailerService $mailerService,
        private TranslatorInterface $translator,
        private ActivityLogger $activityLogger,
        private RouterInterface $router,
        private ExporterFactory $exporterFactory
    ) {
    }

    public function sendTrainingCertificateParticipant(ProviderSession $providerSession, User $sender = null): void
    {
        foreach ($providerSession->getTraineesOnly() as $participant) {
            if ($participant->isPresent()) {
                $attachments = [];
                $attachments[] = $this->exporterFactory
                    ->getExporter(PdfCertificateProviderParticipantSessionRoleExporter::EXPORT_TYPE, PdfCertificateProviderParticipantSessionRoleExporter::EXPORT_FORMAT)
                    ->createExportFile($participant);
                $context = ['session' => $providerSession, 'client' => $providerSession->getClient(), 'clientTimeZone' => $providerSession->getClient()->getTimezone()];
                if (!empty($sender)) {
                    $context['user'] = $sender;
                }
                $this->mailerService->sendTemplatedMail(
                    address: [$participant->getParticipant()->getUser()->getEmail()],
                    subject: $this->generateSubject($providerSession),
                    template: 'emails/manager_client/training_certificate.html.twig',
                    context: $context,
                    pathToAttach: $attachments,
                    senderSystem: true
                );
            }
        }
        $this->addLogForSendTrainingCertificate(providerSession: $providerSession, user: $sender);
    }

    public function addLogForSendTrainingCertificate(ProviderSession $providerSession, ?User $user = null): void
    {
        $infos = ['message' => ['key' => 'Training certificates have been emailed for the <a href="%route%">%sessionName%</a> session', 'params' => ['%route%' => $this->router->generate('_session_get', ['id' => $providerSession->getId()]), '%sessionName%' => $providerSession->getName()]], 'session' => ['id' => $providerSession->getId(), 'ref' => $providerSession->getRef(),  'name' => $providerSession->getName()]];
        $this->activityLogger->initActivityLogContext(
            $providerSession->getClient(),
            $user,
            ActivityLog::ORIGIN_CRON,
        );
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SEND_TRAINING_CERTIFICATE, ActivityLog::SEVERITY_INFORMATION, $infos), true);
        $this->activityLogger->flushActivityLogs();
    }

    private function generateSubject(ProviderSession $session): string
    {
        return $this->translator->trans('Training Certificate - %session% of %date% (%ref%)', [
            '%session%' => $session->getName(),
            '%date%' => $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i'),
            '%ref%' => $session->getRef(),
        ]);
    }
}
