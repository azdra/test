<?php

namespace App\Service;

use App\Entity\Client\Client;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Exception\InvalidArgumentException;
use http\Env\Response;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\Response\TraceableResponse;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Header\MetadataHeader;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class MailerService
{
    public string $mailerSenderSystem;
    public string $noReplyMailAddress;
    public string $managerSenderName;
    public const ORIGIN_EMAIL_SEND_CHOOSE_PASSWORD_REQUEST = 'Password initialization';
    public const ORIGIN_EMAIL_SEND_RESET_PASSWORD_REQUEST = 'Password reset';
    public const ORIGIN_EMAIL_SEND_INFO_USER_LINK_TO_NEW_CLIENT = 'Associate an existing user to account';
    public const ORIGIN_EMAIL_SEND_RESULT_IMPORT_SESSIONS = 'Import session';
    public const ORIGIN_EMAIL_SEND_RESULT_IMPORT_USERS = 'Import participant';
    public const ORIGIN_EMAIL_SEND_CONVOCATION = 'Convocation';
    public const ORIGIN_EMAIL_SEND_SESSION_CANCELED = 'Cancellation';
    public const ORIGIN_EMAIL_SEND_SESSION_UNCANCELED = 'Uncancellation';
    public const ORIGIN_EMAIL_SEND_SESSION_DELETED = 'Deletation';
    public const ORIGIN_EMAIL_SEND_SESSION_UNSUBSCRIBE = 'unsubscribe';
    public const ORIGIN_EMAIL_SEND_ATTENDANCE_REPORT = 'Attendance report';
    public const ORIGIN_EMAIL_SEND_SESSION_UPDATE = 'Updating of the session';
    public const ORIGIN_EMAIL_SEND_EVALUATION_LINK = 'Send the session evaluation link';
    public const ORIGIN_EMAIL_SEND_SIGNATURE_LINK = 'Send the signature link to attend the session';
    public const ORIGIN_EMAIL_SESSION_WITHOUT_ANIMATOR = 'Session without animator';
    public const ORIGIN_EMAIL_SESSION_WITHOUT_PARTICIPANT = 'Session without participant';
    public const ORIGIN_EMAIL_ANIMATOR_ON_SEVERAL_SESSIONS = 'Animator planned on several sessions in parallel';
    public const ORIGIN_EMAIL_TRAINEE_ON_SEVERAL_SESSIONS = 'Participant planned on several sessions in parallel';
    public const ORIGIN_EMAIL_NO_ANIMATOR_WITH_HOST_ON_SESSION = 'No animator with host privilege on the session';
    public const ORIGIN_EMAIL_SEVERAL_ANIMATORS_ON_SAME_SESSION = 'Several animators on the same session';
    public const ORIGIN_EMAIL_INVALID = 'Email invalid';
    public const ORIGIN_EMAIL_SESSION_NEW_ASSISTANCE_REQUEST = 'A new assistance request for a session';
    public const ORIGIN_EMAIL_SESSION_NEW_SUPPORT_REQUEST = 'A new support request for a session';
    public const ORIGIN_EMAIL_RGPD_EXTRACT_DATAS_AVAILABLE = 'Results of your extracted GDPR datas';
    public const ORIGIN_EMAIL_RGPD_EXTRACT_DATAS_FAILED = 'GDPR datas for user extract failed';
    public const ORIGIN_EMAIL_NO_MORE_CREDIT = 'Your credits are almost used up';
    public const ORIGIN_EMAIL_DEFERRED_ACTIONS_TASK = 'Your export has been successfully completed';
    public const ORIGIN_EMAIL_ALERT_NO_TREATED_RENAULT = 'Lines not treated for Renault today';
    public const ORIGIN_EMAIL_ALERT_IDFUSE_WEBHOOK = 'Alert when we received a webhook message';
    public const ORIGIN_EMAIL_ALERT_BREVO_WEBHOOK = 'Alert when we received a webhook Fredo message';

    public function __construct(
        private MailerInterface $mailer,
        private LoggerInterface $logger,
        private TranslatorInterface $translator,
        private Environment $twig,
        //private ActivityLogger $activityLogger,
        private HttpClientInterface $httpClient,
                                    $mailerSenderSystem,
                                    $noReplyMailAddress,
                                    $managerSenderName,
        private string $idfuseApiToken,
        private string $idfuseEndPointEnv,
        private string $brevoMailerApiKey,
        private KernelInterface $kernel,
        private string $emailReceiver,
    ) {
        $this->mailerSenderSystem = $mailerSenderSystem;
        $this->noReplyMailAddress = $noReplyMailAddress;
        $this->managerSenderName = $managerSenderName;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function sendTemplatedMail(Address|string|array $address, string $subject, string $template, array $context = [], null|array|string $pathToAttach = null, array $metadatas = null, Address|string|array $ccs = null, Address|string|array $bccs = null, $senderSystem = false, ?array $attachDataPart = []): void
    {
        if (empty($address)) {
            throw new InvalidArgumentException('The email address is invalid');
        }

        if (empty($template)) {
            throw new InvalidArgumentException('The email template is not valid');
        }

        if (array_key_exists('session', $context) && $context['session']->isSessionTest()) {
            $subject = $this->translator->trans('Test | ').$subject;
        }

        $templatedEmail = (new TemplatedEmail())
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($context);

        if (is_array($address)) {
            $templatedEmail->to(...$address);
        } else {
            $templatedEmail->to($address);
        }

        if (!empty($ccs)) {
            if (is_array($ccs)) {
                $templatedEmail->cc(...$ccs);
            } else {
                $templatedEmail->cc($ccs);
            }
        }

        if (!empty($bccs)) {
            if (is_array($bccs)) {
                $templatedEmail->bcc(...$bccs);
            } else {
                $templatedEmail->bcc($bccs);
            }
        }

        if (!empty($metadatas)) {
            foreach ($metadatas as $metadataKey => $metadataValue) {
                $templatedEmail->getHeaders()->add(new MetadataHeader($metadataKey, $metadataValue));
            }
        }

        if (!empty($attachDataPart)) {
            foreach ($attachDataPart as $attach) {
                $templatedEmail->attachPart($attach);
            }
        }

        if (!empty($pathToAttach)) {
            if (is_string($pathToAttach)) {
                $templatedEmail->attachFromPath($pathToAttach);
            } else {
                foreach ($pathToAttach as $path) {
                    if (is_string($path)) {
                        $templatedEmail->attachFromPath($path);
                    } elseif (is_array($path)) {
                        $templatedEmail->attachFromPath($path['path'], $path['name']);
                    }
                }
            }
        }

        $client = $this->findClientFromContext($context);
        $transporter = Client::TRANSPORTER_EMAIL_MANDRILL;
        if ($senderSystem) {
            $templatedEmail->from(new Address($this->mailerSenderSystem, $this->managerSenderName));
            $templatedEmail->replyTo($this->noReplyMailAddress);
        } else {
            if (!empty($client)) {
                if (!empty($client->getSenderEmail())) {
                    $templatedEmail->from(new Address($client->getSenderEmail(), empty($client->getSenderName()) ? '' : $client->getSenderName()));
                }

                if (!empty($client->getReplayToEmail())) {
                    $templatedEmail->replyTo($client->getReplayToEmail());
                }

                if (!empty($client->getSenderTransporter())) {
                    $transporter = $client->getSenderTransporter();
                }
            }
        }

        $this->send($templatedEmail, $transporter, $client);
    }

    private function findClientFromContext(array $context): ?Client
    {
        if (array_key_exists('client', $context) && $context['client'] instanceof Client) {
            return $context['client'];
        }

        if (array_key_exists('user', $context) && $context['user'] instanceof User) {
            return $context['user']->getClient();
        }

        if (array_key_exists('session', $context) && $context['session'] instanceof ProviderSession) {
            return $context['session']->getClient();
        }

        return null;
    }

    public function send(TemplatedEmail $email, string $transporter = Client::TRANSPORTER_EMAIL_MANDRILL, ?Client $client = null): void
    {
        try {
            $email->getHeaders()->addTextHeader('X-Transport', $transporter);
            if ($transporter == Client::TRANSPORTER_EMAIL_MANDRILL) {
                $this->mailer->send($email);
            } elseif ($transporter == CLient::TRANSPORTER_EMAIL_BREVO) {
                $this->sendEmailByBrevo($email, $client);
            } else {
                foreach ($email->getTo() as $to) {
                    $this->sendEmailByIDFuse($email, $to, $client);
                }
                foreach ($email->getCc() as $to) {
                    $this->sendEmailByIDFuse($email, $to, $client);
                }
                foreach ($email->getBcc() as $to) {
                    $this->sendEmailByIDFuse($email, $to, $client);
                }
            }
        } catch (TransportExceptionInterface $e) {
            $this->logger->error($e);
        }
    }

    public function renameReceiver($data): array
    {
        $environment = $this->kernel->getEnvironment();

        return array_map(
            function (Address $address) use ($environment) {
                $result = [
                    'email' => ($environment !== 'prod') ? $this->emailReceiver : $address->getAddress(),
                ];

                if ($address->getName() !== '') {
                    $result['name'] = $address->getName();
                }

                return $result;
            },
            $data
        );
    }

    public function sendEmailByBrevo(TemplatedEmail $email): void
    {
        $url = 'https://api.brevo.com/v3/smtp/email';
        $api_key = $this->brevoMailerApiKey;
        $from = $email->getFrom();

        $tags = [];
        $originHeader = (!empty($email->getHeaders()->get('x-metadata-origin'))) ? $email->getHeaders()->get('x-metadata-origin')->getBodyAsString() : '';
        $userHeader = (!empty($email->getHeaders()->get('x-metadata-user'))) ? $email->getHeaders()->get('x-metadata-user')->getBodyAsString() : '';
        $sessionHeader = (!empty($email->getHeaders()->get('x-metadata-session'))) ? $email->getHeaders()->get('x-metadata-session')->getBodyAsString() : '';
        array_push($tags, $originHeader);
        array_push($tags, $userHeader);
        array_push($tags, $sessionHeader);

        $headers = [
            'accept' => 'application/json',
            'api-key' => $api_key,
            'content-type' => 'application/json',
        ];

        $to = $this->renameReceiver($email->getTo());
        $cc = $this->renameReceiver($email->getCc());
        $bcc = $this->renameReceiver($email->getBcc());

        $complementContext = [
            'email' => [
                'to' => [
                    0 => [
                        'address' => $to[0]['email'],
//                        'name' => $to[0]['name'],
                    ],
                ],
            ],
        ];
        $fullContext = array_merge($email->getContext(), $complementContext);
        $html = $this->twig->render($email->getHtmlTemplate(), $fullContext);

        $attachments = [];
        foreach ($email->getAttachments() as $attachment) {
            // Vérifier si l'attachement est un DataPart
            if ($attachment instanceof DataPart) {
                $attachmentName = $attachment->getPreparedHeaders()->getHeaderParameter('Content-Disposition', 'filename');
                $attachmentBody = $attachment->getBody();
                $attachments[] = ['name' => $attachmentName, 'content' => base64_encode($attachmentBody)];
            }
        }

        $data = [
            'sender' => [
                'name' => $from[0]->getName(),
                'email' => $from[0]->getAddress(),
            ],
            'to' => $to,
            'subject' => $email->getSubject().' '.(new \DateTime())->getTimestamp(),
            'htmlContent' => $html,
            'replyTo' => [
                'email' => $this->noReplyMailAddress,
            ],
            'tags' => $tags,
        ];

        if (count($cc)) {
            $data['cc'] = $cc;
        }

        if (count($bcc)) {
            $data['bcc'] = $bcc;
        }

        if (count($attachments) > 0) {
            $data['attachment'] = $attachments;
        }

        try {
            $response = $this->httpClient->request('POST', $url, [
                'headers' => $headers,
                'json' => $data,
            ]);
        } catch (ClientException $e) {
            //Loguer les détails de l'erreur
            //dump($e->getMessage());
            $this->logger->error($e);
        }
    }

    public function sendEmailByIDFuse(TemplatedEmail $email, Address $to, ?Client $client = null): void
    {
        $from = $email->getFrom();
        $replyTo = $email->getReplyTo();

        $complementContext = [
            'email' => [
                'to' => [
                    0 => $to,
                ],
            ],
        ];
        $fullContext = array_merge($email->getContext(), $complementContext);
        $html = $this->twig->render($email->getHtmlTemplate(), $fullContext);
        $params = [];
        $post = [
            'email' => $to->getAddress(),
            'action' => 'send',
            'fields_data' => '{}',
            'type' => 'html',
            'message_id' => '-1',
            'campaign_id' => '6',
            'subject' => $email->getSubject(),
            'name' => ($to->getName() == '') ? '-' : $to->getName(), //Nom du destinataire
            'fromname' => $from[0]->getName(), //Nom de l'émetteur
            'fromemail' => $from[0]->getAddress(), //Email de l'émetteur
            'reply2' => $replyTo[0]->getAddress(),
            'html' => $html, //$email->getHtmlBody()
        ];

        if (count($email->getAttachments()) > 0) {
            $index = 0;
            foreach ($email->getAttachments() as $attachment) {
                $post['attachments['.$index.'][name]'] = $attachment->getPreparedHeaders()->getHeaderParameter('Content-Disposition', 'filename');
                $post['attachments['.$index.'][file]'] = $attachment->getBody();
                ++$index;
            }
        }
        $returnSend = $this->callIdFuseAPi($params, $post, 'api/transac/send_mail');
        if ($returnSend['result_code'] != 1) {
            $infos = [
                'message' => ['key' => 'The email to "%email%" (%subject%) could not be sent because the carrier detected the following error (%error_code%) "%error%".', 'params' => ['%email%' => $to->getAddress(), '%subject%' => $email->getSubject(), '%error_code%' => $returnSend['result_code'], '%error%' => $returnSend['result_message']]],
            ];
            /*$this->activityLogger->initActivityLogContext($client, null, ActivityLog::ORIGIN_CRON);
            $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_DELETE_SEND_EMAIL_IDFUSE, ActivityLog::SEVERITY_INFORMATION, $infos), true);
            $this->activityLogger->flushActivityLogs();*/
        }
    }

    public function callBrevoSendTest(array $params, array $post, string $url, ?string $type = 'POST'): array
    {
        $api_key = $this->brevoMailerApiKey;
        $url = 'https://api.brevo.com/v3/smtp/email';

        $data = [
            'sender' => [
                'name' => 'Sender Brevo Test',
                'email' => 'contact@live-session.fr',
            ],
            'to' => [
                [
                    'email' => $params['to'],
                    'name' => 'John Doe',
                ],
            ],
            'subject' => 'Test Brevo',
            'htmlContent' => '<html><head></head><body><p>Hello,</p>This is my first transactional email sent from Brevo.</p></body></html>',
            'tags' => [
                'Convocation', '14',
            ],
        ];

        $headers = [
            'accept' => 'application/json',
            'api-key' => $api_key,
            'content-type' => 'application/json',
        ];

        $response = $this->httpClient->request('POST', $url, [
            'headers' => $headers,
            'json' => $data,
        ]);

        //$traceableResponse = new TraceableResponse($response);
        /*$traceableResponse = new TraceableResponse($response);
        dump($traceableResponse);*/
        /*$httpResponse = $traceableResponse->getResponse();
        dump($httpResponse->getContent());*/

        //dump($response);
        /*echo $response->getContent();

        dump($response);
        $result = json_decode($response, true);*/

        return [];
    }

    public function callIdFuseAPi(array $params, array $post, string $url, ?string $type = 'POST'): array
    {
        $api_token = $this->idfuseApiToken; //'9a8d7a8faddb26cb31d877828e207f1a6d3e8ea5810b7594167dc8110725496c';
        $query = '?api_token='.$api_token.'&';
        foreach ($params as $key => $value) {
            $query .= $key.'='.urlencode($value).'&';
        }
        $query = rtrim($query, '& ');
        $data = '';
        foreach ($post as $key => $value) {
            $data .= $key.'='.urlencode($value).'&';
        }
        $data = rtrim($data, '& ');
        $url = rtrim($url, '/ ');
        //$api = 'https://app.idfuse.fr/'.$url.$query;
        $api = $this->idfuseEndPointEnv.$url.$query;
        $request = curl_init($api); // initiate curl object
        curl_setopt($request, CURLOPT_HTTPHEADER, ['Accept: application/json']); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        if ($type == 'POST') {
            curl_setopt($request, CURLOPT_POSTFIELDS, $data); // use HTTP POST to send form data
        }
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);
        $response = (string) curl_exec($request); // execute curl post and store results in $response
        curl_close($request); // close curl object
        if (!$response) {
            echo 'Nothing was returned.'."\n"."\n";
        }
        $result = json_decode($response, true);

        return $result;
    }
}
