<?php

namespace App\Service;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsEventParticipant;
use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\White\WhiteParticipant;
use App\Exception\ProviderServiceValidationException;
use App\Model\ProviderResponse;
use App\Service\AsyncWorker\AsyncTaskService;
use App\Service\AsyncWorker\Tasks\SyncSessionRoleToMicrosoftTeamsEventSessionTask;
use App\Service\Connector\Microsoft\MicrosoftTeamsEventConnector;
use App\Service\Provider\Microsoft\MicrosoftTeamsEventLicensingService;
use App\Service\Provider\Model\LicenceCheckerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MicrosoftTeamsEventService extends AbstractProviderService implements LicenceCheckerInterface
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected ValidatorInterface $validator,
        private MicrosoftTeamsEventLicensingService $microsoftTeamsEventLicensingService,
        MicrosoftTeamsEventConnector $microsoftTeamsEventConnector,
        protected AsyncTaskService $asyncTaskService,
        LoggerInterface $logger,
        private ConvocationService $convocationService
    ) {
        parent::__construct($entityManager, $validator, $microsoftTeamsEventConnector, $logger);
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof MicrosoftTeamsEventConfigurationHolder;
    }

    public function createSession(ProviderSession $providerSession): ProviderResponse
    {
        /** @var MicrosoftTeamsEventSession $providerSession */
        $sessionId = $this->entityManager->contains($providerSession) ? $providerSession->getId() : null;

        if (is_null($providerSession->getLicence())) {
            $this->microsoftTeamsEventLicensingService->licensingASession($providerSession, $sessionId);
        } else {
            /** @var MicrosoftTeamsEventConfigurationHolder $configurationHolder */
            $configurationHolder = $providerSession->getAbstractProviderConfigurationHolder();
            $this->microsoftTeamsEventLicensingService->addParticipantRole($configurationHolder, $providerSession);
        }
        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['create']))) {
            throw new ProviderServiceValidationException($list);
        }
        $response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsEventConnector::ACTION_SESSION_CREATE,
            $providerSession
        );

        $responseTelephonyProfile = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsEventConnector::ACTION_TELEPHONY_PROFILE_INFO,
            $providerSession
        );
        if ($responseTelephonyProfile->isSuccess()) {
            $providerSession->setProviderAudio($responseTelephonyProfile->getData());
        }

        if ($response->isSuccess()) {
            $this->entityManager->persist($providerSession);
        }

        return $response;
    }

    public function createHideSession(ProviderSession $providerSession): ProviderResponse
    {
        /* @var MicrosoftTeamsEventSession $providerSession */
        $sessionId = $this->entityManager->contains($providerSession) ? $providerSession->getId() : null;
        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['create']))) {
            throw new ProviderServiceValidationException($list);
        }
        $response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsEventConnector::ACTION_SESSION_CREATE,
            $providerSession
        );

        $responseTelephonyProfile = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsEventConnector::ACTION_TELEPHONY_PROFILE_INFO,
            $providerSession
        );
        if ($responseTelephonyProfile->isSuccess()) {
            $providerSession->setProviderAudio($responseTelephonyProfile->getData());
        }

        if ($response->isSuccess()) {
            $this->entityManager->persist($providerSession);
        }

        return $response;
    }

    public function changeSessionForNewLicence(MicrosoftTeamsEventSession $providerSession, string $newLicense): ProviderResponse
    {
        $response = $this->deleteSession($providerSession);

        if ($response->isSuccess()) {
            /** @var MicrosoftTeamsEventConfigurationHolder $configurationHolder */
            $configurationHolder = $providerSession->getAbstractProviderConfigurationHolder();
            $groupLicenses = $configurationHolder->getLicences();
            $retainedLicense = $newLicense;
            if (array_key_exists($newLicense, $groupLicenses)) {
                if ($groupLicenses[$newLicense]['attributed']) {
                    $retainedLicense = $groupLicenses[$newLicense]['licenceAttributed'];
                }
            }
            $providerSession->setLicence($retainedLicense);
            $providerSession->setUserLicence($newLicense);
            /*$providerSession->setLicence($targetLicense[1]);
            $providerSession->setUserLicence($targetLicense[0]);*/
            $response = $this->createHideSession($providerSession);

            if ($response->isSuccess()) {
                $this->entityManager->persist($providerSession);
            }
        }

        return $response;
    }

    public function sendEmailToInformAllAnimatorsForLicenceChange(ProviderParticipantSessionRole $participant): void
    {
        $providerSession = $participant->getSession();

        if (!$providerSession->isConvocationSent()) {
            return;
        }

        if ($providerSession instanceof MicrosoftTeamsEventSession) {
            foreach ($providerSession->getParticipantRoles() as $participantSessionRole) {
                if ($participantSessionRole->getRole() === ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                    $this->convocationService->sendUpdateSessionMail($participantSessionRole, $providerSession, [[
                        'type' => 'Licence',
                        'title' => 'Change of license',
                        'new' => "{$participant->getParticipant()->getFirstName()} {$participant->getParticipant()->getLastName()} ({$participant->getEmail()})",
                    ]]);
                }
            }
        }
    }

    public function findJoinLicenseAndUserLicense(MicrosoftTeamsEventConfigurationHolder $configurationHolder, string $userLicense): array
    {
        $groupLicenses = $configurationHolder->getLicences();
        $retainedLicense = $userLicense;
        if (array_key_exists($userLicense, $groupLicenses)) {
            if ($groupLicenses[$userLicense]['attributed']) {
                $retainedLicense = $groupLicenses[$userLicense]['licenceAttributed'];
            }
        }

        return [$userLicense, $retainedLicense];
    }

    public function deleteSession(ProviderSession $providerSession): ProviderResponse
    {
        /*if (0 !== count($list = $this->validator->validate($providerSession, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }*/

        $response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsEventConnector::ACTION_SESSION_DELETE,
            $providerSession
        );

        return $response;
    }

    public function createParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return (new ProviderResponse())->setSuccess(true);
    }

    public function updateParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return (new ProviderResponse())->setSuccess(true);
    }

    public function deleteParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return (new ProviderResponse())->setSuccess(true);
    }

    public function updateParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse
    {
        /* @var MicrosoftTeamsEventSession $session */
        $session = $participantSessionRole->getSession();

        if (!($session instanceof MicrosoftTeamsEventSession)) {
            return $this->makeUnsupportedProviderSessionResponse($session);
        }

        if (
            ($participantSessionRole->getRole() === ProviderParticipantSessionRole::ROLE_REMOVE && ($session->getLicence() == $participantSessionRole->getParticipant()->getEmail())) ||
            ($participantSessionRole->getRole() === ProviderParticipantSessionRole::ROLE_ANIMATOR)
        ) {
            $participants = [];
            foreach ($session->getParticipantRoles() as $key => $ppsr) {
                if ($ppsr->getRole() !== ProviderParticipantSessionRole::ROLE_REMOVE) {
                    $participants[] = [
                        'user_id' => $ppsr->getParticipant()->getUser()->getId(),
                        'role' => $ppsr->getRole(),
                        'situationMixte' => '1',
                    ];
                }
            }
            $session->setCandidates($participants);

            //TODO : check if licence is available
            //return $this->changeSessionForNewLicence($session);
            return $this->updateSession($session);
        } else {
            return $this->updateSession($session);
        }
    }

    public function getMeeting(ProviderSession $providerSession): ProviderResponse
    {
        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsEventConnector::ACTION_GET_MEETING,
            $providerSession
        );
    }

    public function getMeetingAttendanceReports(ProviderSession $providerSession): ProviderResponse
    {
        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsEventConnector::ACTION_GET_MEETING_ATTENDANCE_REPORTS,
            $providerSession
        );
    }

    public function getMeetingAttendanceReportId(ProviderSession $providerSession, string $reportId): ProviderResponse
    {
        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsEventConnector::ACTION_GET_MEETING_ATTENDANCE_REPORT_ID,
            [
                'providerSession' => $providerSession,
                'reportId' => $reportId,
            ]
        );
    }

    public function redirectParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse
    {
        /* @var MicrosoftTeamsEventSession $providerSession */
        $providerSession = $participantSessionRole->getSession();

        if (!($providerSession instanceof MicrosoftTeamsEventSession)) {
            return $this->makeUnsupportedProviderSessionResponse($providerSession);
        }

        $response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsEventConnector::ACTION_GET_MEETING,
            $providerSession
        );

        if ($response->isSuccess()) {
            $providerResponse = new ProviderResponse();
            $providerResponse
                    ->setData($providerSession->getJoinUrl())
                    ->setSuccess(true);

            return $providerResponse;
        } else {
            return $response;
        }
    }

    public function updateSession(ProviderSession $providerSession): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsEventConnector::ACTION_SESSION_UPDATE,
            $providerSession
        );
    }

    public function invite(MicrosoftTeamsEventSession $microsoftTeamsEventSession, Collection|WhiteParticipant $participants): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($microsoftTeamsEventSession, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        if ($participants instanceof MicrosoftTeamsEventParticipant) {
            $participants = new ArrayCollection([$participants]);
        }

        foreach ($participants as $participant) {
            if ($this->entityManager->getRepository(ProviderParticipantSessionRole::class)->findOneBy(['session' => $microsoftTeamsEventSession, 'participant' => $participant])) {
                continue;
            }

            if (0 !== count($list = $this->validator->validate($participant, groups: ['create']))) {
                throw new ProviderServiceValidationException($list);
            }

            $role = new ProviderParticipantSessionRole();
            $role->setRole(MicrosoftTeamsEventParticipant::ROLE_ATTENDEE)
                ->setParticipant($participant)
                ->setSession($microsoftTeamsEventSession);

            $microsoftTeamsEventSession->addParticipantRole($role);
            $participant->addSessionRoles($role);

            $this->entityManager->persist($role);
        }

        $flagOrigin = 'SessionRoleMSTeamsEvent_'.$this->asyncTaskService->randomFlag(20);
        $task = $this->asyncTaskService->publish(SyncSessionRoleToMicrosoftTeamsEventSessionTask::class, [
            'session_id' => $microsoftTeamsEventSession->getId(),
        ], $flagOrigin);

        return (new ProviderResponse())
            ->setSuccess(true)
            ->setData([
                'participants' => $participants,
                'async_task' => $task,
            ]);
    }

    public function getToken(MicrosoftTeamsEventConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            MicrosoftTeamsEventConnector::ACTION_GET_TOKEN
        );
    }

    public function getUser(MicrosoftTeamsEventConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            MicrosoftTeamsEventConnector::ACTION_GET_USER
        );
    }

    public function makeUnsupportedProviderSessionResponse(ProviderSession $providerSession): ProviderResponse
    {
        return (new ProviderResponse())
            ->setSuccess(false)
            ->setThrown(new InvalidArgumentException(sprintf('%s is not supported by %s.', get_class($providerSession), self::class)));
    }

    public function checkLicence(ProviderSession $providerSession): void
    {
        // TODO: Implement checkLicence() method.
    }

    public function getAvailableLicence(
        AbstractProviderConfigurationHolder $configurationHolder,
        \DateTime $dateStart,
        \DateTime $dateEnd,
        ?int $excludeSessionId = null
    ): array {
        // TODO: Implement getAvailableLicence() method.
        return [];
    }
}
