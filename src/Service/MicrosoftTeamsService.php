<?php

namespace App\Service;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\White\WhiteParticipant;
use App\Exception\ProviderServiceValidationException;
use App\Model\ProviderResponse;
use App\Service\AsyncWorker\AsyncTaskService;
use App\Service\AsyncWorker\Tasks\SyncSessionRoleToMicrosoftTeamsSessionTask;
use App\Service\Connector\Microsoft\MicrosoftTeamsConnector;
use App\Service\Provider\Microsoft\MicrosoftTeamsLicensingService;
use App\Service\Provider\Model\LicenceCheckerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MicrosoftTeamsService extends AbstractProviderService implements LicenceCheckerInterface
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected ValidatorInterface $validator,
        private MicrosoftTeamsLicensingService $microsoftTeamsLicensingService,
        MicrosoftTeamsConnector $microsoftTeamsConnector,
        protected AsyncTaskService $asyncTaskService,
        LoggerInterface $logger
    ) {
        parent::__construct($entityManager, $validator, $microsoftTeamsConnector, $logger);
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof MicrosoftTeamsConfigurationHolder;
    }

    public function createSession(ProviderSession $providerSession): ProviderResponse
    {
        /* @var MicrosoftTeamsSession $providerSession */
        $sessionId = $this->entityManager->contains($providerSession) ? $providerSession->getId() : null;
        $this->microsoftTeamsLicensingService->licensingASession($providerSession, $sessionId);

        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['create']))) {
            throw new ProviderServiceValidationException($list);
        }

        $response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsConnector::ACTION_SESSION_CREATE,
            $providerSession
        );

        $responseTelephonyProfile = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsConnector::ACTION_TELEPHONY_PROFILE_INFO,
            $providerSession
        );
        if ($responseTelephonyProfile->isSuccess()) {
            $providerSession->setProviderAudio($responseTelephonyProfile->getData());
        }

        if ($response->isSuccess()) {
            $this->entityManager->persist($providerSession);
        }

        return $response;
    }

    public function changeSessionForNewLicence(MicrosoftTeamsSession $providerSession): ProviderResponse
    {
        $response = $this->deleteSession($providerSession);

        if ($response->isSuccess()) {
            $providerSession->setLicence(null);
            $response = $this->createSession($providerSession);

            if ($response->isSuccess()) {
                $this->entityManager->persist($providerSession);
            }
        }

        return $response;
    }

    public function deleteSession(ProviderSession $providerSession): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        $response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsConnector::ACTION_SESSION_DELETE,
            $providerSession
        );

        return $response;
    }

    public function createParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return (new ProviderResponse())->setSuccess(true);
    }

    public function updateParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return (new ProviderResponse())->setSuccess(true);
    }

    public function deleteParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return (new ProviderResponse())->setSuccess(true);
    }

    public function updateParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse
    {
        /* @var MicrosoftTeamsSession $session */
        $session = $participantSessionRole->getSession();

        if (!($session instanceof MicrosoftTeamsSession)) {
            return $this->makeUnsupportedProviderSessionResponse($session);
        }

        if (
            ($participantSessionRole->getRole() === ProviderParticipantSessionRole::ROLE_REMOVE && ($session->getLicence() == $participantSessionRole->getParticipant()->getEmail())) ||
            ($participantSessionRole->getRole() === ProviderParticipantSessionRole::ROLE_ANIMATOR)
        ) {
            $participants = [];
            foreach ($session->getParticipantRoles() as $key => $ppsr) {
                if ($ppsr->getRole() !== ProviderParticipantSessionRole::ROLE_REMOVE) {
                    $participants[] = [
                        'user_id' => $ppsr->getParticipant()->getUser()->getId(),
                        'role' => $ppsr->getRole(),
                        'situationMixte' => '1',
                    ];
                }
            }
            $session->setCandidates($participants);

            return $this->changeSessionForNewLicence($session);
        } else {
            return $this->updateSession($session);
        }
    }

    public function getMeeting(ProviderSession $providerSession): ProviderResponse
    {
        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsConnector::ACTION_GET_MEETING,
            $providerSession
        );
    }

    public function getMeetingAttendanceReports(ProviderSession $providerSession): ProviderResponse
    {
        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsConnector::ACTION_GET_MEETING_ATTENDANCE_REPORTS,
            $providerSession
        );
    }

    public function getMeetingAttendanceReportId(ProviderSession $providerSession, string $reportId): ProviderResponse
    {
        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsConnector::ACTION_GET_MEETING_ATTENDANCE_REPORT_ID,
            [
                'providerSession' => $providerSession,
                'reportId' => $reportId,
            ]
        );
    }

    public function redirectParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse
    {
        /* @var MicrosoftTeamsSession $providerSession */
        $providerSession = $participantSessionRole->getSession();

        if (!($providerSession instanceof MicrosoftTeamsSession)) {
            return $this->makeUnsupportedProviderSessionResponse($providerSession);
        }

        $response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsConnector::ACTION_GET_MEETING,
            $providerSession
        );

        if ($response->isSuccess()) {
            $providerResponse = new ProviderResponse();
            $providerResponse
                    ->setData($providerSession->getJoinWebUrl())
                    ->setSuccess(true);

            return $providerResponse;
        } else {
            return $response;
        }
    }

    public function updateSession(ProviderSession $providerSession): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsConnector::ACTION_SESSION_UPDATE,
            $providerSession
        );
    }

    public function invite(MicrosoftTeamsSession $microsoftTeamsSession, Collection|WhiteParticipant $participants): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($microsoftTeamsSession, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        if ($participants instanceof WhiteParticipant) {
            $participants = new ArrayCollection([$participants]);
        }

        foreach ($participants as $participant) {
            if ($this->entityManager->getRepository(ProviderParticipantSessionRole::class)->findOneBy(['session' => $microsoftTeamsSession, 'participant' => $participant])) {
                continue;
            }

            if (0 !== count($list = $this->validator->validate($participant, groups: ['create']))) {
                throw new ProviderServiceValidationException($list);
            }

            $role = new ProviderParticipantSessionRole();
            $role->setRole(WhiteParticipant::ROLE_ATTENDEE)
                ->setParticipant($participant)
                ->setSession($microsoftTeamsSession);

            $microsoftTeamsSession->addParticipantRole($role);
            $participant->addSessionRoles($role);

            $this->entityManager->persist($role);
        }

        $flagOrigin = 'SessionRoleMSTeams_'.$this->asyncTaskService->randomFlag(20);
        $task = $this->asyncTaskService->publish(SyncSessionRoleToMicrosoftTeamsSessionTask::class, [
            'session_id' => $microsoftTeamsSession->getId(),
        ], $flagOrigin);

        return (new ProviderResponse())
            ->setSuccess(true)
            ->setData([
                'participants' => $participants,
                'async_task' => $task,
            ]);
    }

    public function getToken(MicrosoftTeamsConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            MicrosoftTeamsConnector::ACTION_GET_TOKEN
        );
    }

    public function getUser(MicrosoftTeamsConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            MicrosoftTeamsConnector::ACTION_GET_USER
        );
    }

    public function makeUnsupportedProviderSessionResponse(ProviderSession $providerSession): ProviderResponse
    {
        return (new ProviderResponse())
            ->setSuccess(false)
            ->setThrown(new InvalidArgumentException(sprintf('%s is not supported by %s.', get_class($providerSession), self::class)));
    }

    public function checkLicence(ProviderSession $providerSession): void
    {
        // TODO: Implement checkLicence() method.
    }

    public function getAvailableLicence(
        AbstractProviderConfigurationHolder $configurationHolder,
        \DateTime $dateStart,
        \DateTime $dateEnd,
        ?int $excludeSessionId = null
    ): array {
        // TODO: Implement getAvailableLicence() method.
        return [];
    }
}
