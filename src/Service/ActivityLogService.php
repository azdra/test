<?php

namespace App\Service;

use App\Entity\ActivityLog;
use App\Entity\AsyncTask;
use App\Entity\User;
use App\Model\ActivityLog\ActivityLogContext;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ActivityLogRepository;
use App\Repository\ClientRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ActivityLogService
{
    public const OLDER_INTERVAL_TARGET = 'P30D'; // DateInterval format

    public function __construct(
        private TranslatorInterface $translator,
        protected ActivityLogRepository $activityLogRepository,
        private string $importsSavefilesDirectory,
        private string $exportsSavefilesDirectory,
        private EntityManagerInterface $entityManager,
        private MailerService $mailerService,
        private LoggerInterface $logger,
        private ProviderSessionRepository $providerSessionRepository,
        private ClientRepository $clientRepository,
        private UserRepository $userRepository,
        private SerializerInterface $serializer,
    ) {
    }

    // FIXME: User origin may be null.
    public function prepareEmail(AsyncTask $task, string $target): array
    {
        $logActionToSend = match ($target) {
            ActivityLog::ACTION_IMPORT_SESSIONS => [ActivityLog::ACTION_SESSION_CREATION, ActivityLog::ACTION_SESSION_UPDATE, ActivityLog::ACTION_SESSION_DELETE, ActivityLog::ACTION_SESSION_CANCEL],
            ActivityLog::ACTION_IMPORT_USERS => [ActivityLog::ACTION_USER_ADD, ActivityLog::ACTION_USER_UPDATE, ActivityLog::ACTION_USER_DELETE, ActivityLog::ACTION_USER_ANONYMIZE],
            default => []
        };
        /** @var ActivityLog[] $activityLogs */
        $activityLogs = $this->activityLogRepository->searchResultImportForSendEmail($task->getFlagOrigin(), $logActionToSend);

        $currentContentByEmail = [
            'success' => [],
            'error' => [],
            'warning' => [],
        ];

        foreach ($activityLogs as $activityLog) {
            $result = match ($activityLog->getSeverity()) {
                ActivityLog::SEVERITY_ERROR => 'error',
                ActivityLog::SEVERITY_WARNING => 'warning',
                ActivityLog::SEVERITY_INFORMATION => 'success'
            };
            $currentContentByEmail[$result][] = $activityLog;
        }

        switch ($target) {
            case ActivityLog::ACTION_IMPORT_SESSIONS:
                $this->onSendResultImportSessionsRequest($task->getUserOrigin(), $task->getFileOrigin(), $task->getCreatedAt(), $currentContentByEmail);
                break;
            case ActivityLog::ACTION_IMPORT_USERS:
                $this->onSendResultImportUsersRequest($task->getUserOrigin(), $task->getFileOrigin(), $task->getCreatedAt(), $currentContentByEmail);
                break;
        }

        return $currentContentByEmail;
    }

    public function extractParamsForTranslator(array $dataInfos): array
    {
        $dataParamsNew = [];
        foreach ($dataInfos['params'] as $key => $value) {
            $keyNew = str_replace('%', '', $key);
            $dataParamsNew[$keyNew] = $value;
        }
        $dataInfos['params'] = $dataParamsNew;

        return $dataInfos;
    }

    public function extractResultImportDataForLog(array $resultsImport): array
    {
        $finalData = [];
        foreach ($resultsImport as $typeResult => $datasResultImport) {
            foreach ($datasResultImport as $dataResultImport) {
                $finalData[$typeResult][$dataResultImport->getInfos()['numlinefile']]['message'] = $this->extractParamsForTranslator($dataResultImport->getInfos()['message']);
                $finalData[$typeResult][$dataResultImport->getInfos()['numlinefile']]['numlinefile'] = $dataResultImport->getInfos()['numlinefile'];
            }
        }

        return $finalData;
    }

    public function onSendResultImportSessionsRequest(User $user, $fileOrigin, DateTime $taskCreatedAt, array $dataEmail = []): void
    {
        $taskCreatedAt = $taskCreatedAt->setTimezone(new \DateTimeZone($user->getClient()->getTimezone()));
        $this->mailerService->sendTemplatedMail(
            address: $user->getEmail(),
            subject: $this->translator->trans('Result of importing sessions from {DATE} to {HOUR}', [
                '{DATE}' => $taskCreatedAt->format('d/m/y'),
                '{HOUR}' => $taskCreatedAt->format('H:i'),
            ], locale: $user->getPreferredLang()),
            template: 'emails/manager_client/result_import_sessions.html.twig',
            context: [
                'dataEmail' => $dataEmail,
                'user' => $user,
                'client' => $user->getClient(),
            ],
            pathToAttach: $this->importsSavefilesDirectory.'/'.$fileOrigin,
            metadatas: [
                'origin' => MailerService::ORIGIN_EMAIL_SEND_RESULT_IMPORT_SESSIONS,
                'user' => $user->getId(),
            ],
            senderSystem: true
        );
    }

    public function onSendResultImportUsersRequest(User $user, $fileOrigin, DateTime $taskCreatedAt, array $dataEmail = []): void
    {
        $taskCreatedAt = $taskCreatedAt->setTimezone(new \DateTimeZone($user->getClient()->getTimezone()));
        $this->mailerService->sendTemplatedMail(
            address: $user->getEmail(),
            subject: $this->translator->trans('Result of importing users from {DATE} to {HOUR}', [
                '{DATE}' => $taskCreatedAt->format('d/m/y'),
                '{HOUR}' => $taskCreatedAt->format('H:i'),
            ], locale: $user->getPreferredLang()),
            template: 'emails/manager_client/result_import_users.html.twig',
            context: [
                'dataEmail' => $dataEmail,
                'user' => $user,
                'client' => $user->getClient(),
            ],
            pathToAttach: $this->importsSavefilesDirectory.'/'.$fileOrigin,
            metadatas: [
                'origin' => MailerService::ORIGIN_EMAIL_SEND_RESULT_IMPORT_USERS,
                'user' => $user->getId(),
            ],
            senderSystem: true
        );
    }

    public function generateActivityLogFromContext(ActivityLogContext $activityLogContext, ActivityLogModel $activityLogModel): void
    {
        $activityLog = ActivityLog::createFromContext($activityLogContext, $activityLogModel);
        $this->entityManager->persist($activityLog);
    }

    public function cleanGivenActivityLogs(array $activityLogs): void
    {
        /** @var ActivityLog $activityLog */
        foreach ($activityLogs as $activityLog) {
            if (!$activityLog instanceof ActivityLog || !in_array($activityLog->getAction(), ActivityLog::ALLOWED_ACTION_TO_BE_CLEANED)) {
                continue;
            }

            $filename = $this->findActivityLogFileName($activityLog);
            if (!is_null($filename) && file_exists($filename)) {
                unlink($filename);
            }

            $this->entityManager->remove($activityLog);
            $this->logger->info('Activity log cleaned-up', [
                'id' => $activityLog->getId(),
                'action' => $activityLog->getAction(),
                'filename' => $filename,
            ]);
        }
    }

    public function findActivityLogFileName(ActivityLog $activityLog): ?string
    {
        if (!in_array($activityLog->getAction(), ActivityLog::ALLOWED_ACTION_TO_BE_CLEANED)) {
            return null;
        }

        $infos = $activityLog->getInfos();

        if (!isset($infos['links'])) {
            return null;
        }

        $links = $infos['links'];
        if (!isset($links['routeKey'])) {
            return null;
        }

        switch ($links['routeKey']) {
            case 'download_import_file':
                $filename = $links['routeParam']['id'] ?? null;

                return (is_null($filename))
                    ? $filename
                    : "$this->importsSavefilesDirectory/$filename";

            case 'download_export_file':
                $filename = $links['routeParam']['filename'] ?? null;

                return (is_null($filename))
                    ? $filename
                    : "$this->exportsSavefilesDirectory/$filename";
        }

        return null;
    }

    public function cleanActivityLogSession(OutputInterface $output, int $nbrdays): int
    {
        $sessions = $this->providerSessionRepository->findOldSessionsByMinusDate($nbrdays);
        $output->writeln(count($sessions).' sessions concernées !');

        $treated = 0;
        foreach ($sessions as $session) {
            //$activities = $this->activityLogRepository->findAllBefore($session->getClient(), null, ['session.id' => strval($session->getId())]);

            //$output->writeln('Session ID : '.$session->getId().' ('.$session->getRef().')');
            $activities = $this->activityLogRepository->findAllBefore($session->getClient(), null, ['session.ref' => $session->getRef()]);
            foreach ($activities['results'] as $activity) {
                $output->writeln('Session ID : '.$session->getId().' ('.$session->getRef().') ('.$session->getClient()->getName().', '.$session->getClient()->getId().') | Activity ID : '.$activity->getId());
                $this->entityManager->remove($activity);

                ++$treated;
            }
        }
        $this->entityManager->flush();

        return $treated;
    }

    public function cleanActivityLogData(OutputInterface $output, int $minusdays, int $plusdays, ?int $activityLogId): int
    {
        $dateMin = ( new \DateTime() )->modify('-'.$minusdays.' days')->setTime(00, 00)->format('Y-m-d H:i:s');
        $dateMax = ( new \DateTime() )->modify('-'.$plusdays.' days')->setTime(23, 59)->format('Y-m-d H:i:s');
        $output->writeln($dateMin.' > '.$dateMax);
        if (empty($activityLogId)) {
            $aLogs = $this->activityLogRepository->findAllLogsBefore($dateMin, $dateMax);
        } else {
            $aLogs = $this->activityLogRepository->findBy(['id' => $activityLogId]);
        }
        $output->writeln(count($aLogs).' activity_log concernés !');

        $treated = 0;
        foreach ($aLogs as $al) {
            if (!empty($al->getInfos()['data'])) {
                $clearArray = $al->getInfos();
                unset($clearArray['data']);
                $output->writeln('Activity ID : '.$al->getId().' : key data[...] deleted in json!');
                $al->setInfos($clearArray);
                $this->entityManager->persist($al);
                $this->entityManager->flush();
                ++$treated;
            } else {
                $output->writeln('Activity ID : '.$al->getId().' : key data[...] does not exist in json!');
            }
        }

        return $treated;
    }

    public function cleanActivityLogClient(OutputInterface $output, int $nbrdays): int
    {
        $clients = $this->clientRepository->findAll();
        $output->writeln(count($clients).' clients concernées !');
        $dateMax = ( new \DateTime() )->modify('-'.$nbrdays.' days')->setTime(23, 59)->format('Y-m-d H:i:s');

        $treated = 0;
        foreach ($clients as $client) {
            $activities = $this->activityLogRepository->findAllBefore($client, $dateMax, ['client.id' => strval($client->getId())]);
            foreach ($activities['results'] as $activity) {
                //dump('Client ID : '.$client->getId().' ('.$client->getName().') | Activity ID : '.$activity->getId());
                $output->writeln('Client ID : '.$client->getId().' ('.$client->getName().') | Activity ID : '.$activity->getId());
                $this->entityManager->remove($activity);
                ++$treated;
            }
        }
        $this->entityManager->flush();

        return $treated;
    }

    public function cleanActivityLogUser(OutputInterface $output, int $nbrdays): int
    {
        $users = $this->userRepository->findAll();
        $output->writeln(count($users).' users concernées !');
        $dateMax = ( new \DateTime() )->modify('-'.$nbrdays.' days')->setTime(23, 59)->format('Y-m-d H:i:s');

        $treated = 0;
        foreach ($users as $user) {
            $activities = $this->activityLogRepository->findAllBefore($user->getClient(), $dateMax, ['user.id' => strval($user->getId())]);
            foreach ($activities['results'] as $activity) {
                /*dump('User ID : '.$user->getId().' ('.$user->getEmail().') | Client ID : '.$user->getClient()->getId().' ('.$user->getClient()->getName().') | Activity ID : '.$activity->getId());*/
                $output->writeln('User ID : '.$user->getId().' ('.$user->getEmail().') | Client ID : '.$user->getClient()->getId().' ('.$user->getClient()->getName().') | Activity ID : '.$activity->getId());
                $this->entityManager->remove($activity);
                ++$treated;
            }
        }
        $this->entityManager->flush();

        return $treated;
    }

    public function cleanActivityLogAll(OutputInterface $output, int $nombreDays): int
    {
        $dateMax = ( new \DateTime() )->modify('-'.$nombreDays.' days')->setTime(23, 59)->format('Y-m-d H:i:s');
        $output->writeln('We start to clean at  =>  : '.$dateMax);
        $treated = 0;
        $activities = $this->activityLogRepository->findAllUserLogToClean($dateMax);
        $output->writeln('We have  : '.count($activities).' activities to clean !');
        foreach ($activities as $activity) {
            $output->writeln('activity ID : '.$activity->getId());
            $this->entityManager->remove($activity);
            ++$treated;
        }

        $this->entityManager->flush();

        return $treated;
    }

    /**
     * @throws \Exception
     */
    public function cleanActivityLogAllWithSession(OutputInterface $output, int $nombreDays, int $firstDays): int
    {
        $dateMax = ( new \DateTime() )->modify('-'.$nombreDays.' days')->setTime(23, 59)->format('Y-m-d H:i:s');
        $dateFirstMax = ( new \DateTime() )->modify('-'.$firstDays.' days')->setTime(23, 59)->format('Y-m-d H:i:s');
        $output->writeln('We start to clean Between  =>  : '.$dateMax.' and '.$dateFirstMax);
        $treated = 0;
        $activities = $this->activityLogRepository->findAllLogWithSessionToClean($dateMax, $dateFirstMax);
        $output->writeln('We have  : '.count($activities).' activities to clean !');
        foreach ($activities as $activity) {
            if (array_key_exists('session', $activity->getInfos()) && array_key_exists('id', $activity->getInfos()['session'])) {
                $session = $this->providerSessionRepository->findOneBy(['id' => $activity->getInfos()['session']['id']]);
                if ($session && $session->getDateStart() > (new \DateTimeImmutable())->modify('-90 day')) {
                    $output->writeln('activity ID : '.$activity->getId().' | session ID : '.$activity->getInfos()['session']['id'].' =>  is not deleted because the start date of the session is in the future. !');
                } else {
                    $output->writeln('activity ID : '.$activity->getId().' | session ID : '.$activity->getInfos()['session']['id'].' ******** is deleted !');
                    $this->entityManager->remove($activity);
                    ++$treated;
                }
            } else {
                $output->writeln('activity ID : '.$activity->getId().' | session ID not defined ******** is deleted !');
                $this->entityManager->remove($activity);
                ++$treated;
            }
        }

        $this->entityManager->flush();

        return $treated;
    }
}
