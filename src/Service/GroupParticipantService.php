<?php

namespace App\Service;

use App\Entity\Client\Client;
use App\Entity\GroupParticipant;
use App\Entity\User;
use App\Exception\InvalidDataGroupParticipant;
use App\Repository\ClientRepository;
use App\Repository\GroupParticipantRepository;
use App\Repository\UserRepository;
use ErrorException;
use Exception;
use Psr\Log\LoggerInterface;

class GroupParticipantService
{
    public function __construct(
        private ClientRepository $clientRepository,
        private GroupParticipantRepository $groupParticipantRepository,
        private UserRepository $userRepository,
        private LoggerInterface $logger,
    ) {
    }

    public function updateGroupParticipant(GroupParticipant $groupParticipant, array $data): GroupParticipant
    {
        try {
            if (!$this->isDataValid($data)) {
                throw new InvalidDataGroupParticipant('Invalid data provided');
            }

            if (array_key_exists('name', $data)) {
                $groupParticipant->setName($data['name']);
            }
            if (array_key_exists('client', $data) && $groupParticipant->getClient()->getId() !== $data['client'] && $data['client'] !== null) {
                $groupParticipant->setClient($data['client']);
            }

            if (array_key_exists('active', $data)) {
                $groupParticipant->setActive($data['active']);
            }

            if (array_key_exists('parent', $data) && $groupParticipant->getId() !== $data['parent']) {
                $groupParticipant->setParent($data['parent']);
            }
            $groupParticipant->setLastUpdate(new \DateTime());
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
            throw new ErrorException($exception->getMessage());
        }

        return $groupParticipant;
    }

    public function handleDataTransformation(array &$data, GroupParticipant $groupParticipant): void
    {
        if (array_key_exists('client', $data)) {
            $data['client'] = $this->clientRepository->findOneBy(['id' => $data['client']]);
        }

        if (array_key_exists('parent', $data) && $groupParticipant->getId() !== $data['parent']) {
            $data['parent'] = $this->groupParticipantRepository->findOneBy(['id' => $data['parent']]);
        } else {
            $data['parent'] = null;
        }
    }

    public function isDataValid(array $data): bool
    {
        // Check if the 'client' key exists and if it corresponds to a valid Client
        if (!array_key_exists('client', $data) || !($data['client'] instanceof Client)) {
            return false;
        }

        // If 'parent' is present, check if it is null or a valid GroupParticipant
        if (array_key_exists('parent', $data) && $data['parent'] !== null && !($data['parent'] instanceof GroupParticipant)) {
            return false;
        }

        return true;
    }

    public function updateMemberGroupParticipant(GroupParticipant $groupParticipant, array $data): GroupParticipant
    {
        try {
            if (array_key_exists('members', $data)) {
                $currentUsers = $groupParticipant->getUsers();
                $this->addNewUsers($groupParticipant, $data['members']);
                $this->removeUsersNotInData($groupParticipant, $currentUsers, $data['members']);
            }
        } catch (Exception $exception) {
            $this->handleException($exception);
        }

        return $groupParticipant;
    }

    private function removeUsersNotInData(GroupParticipant $groupParticipant, mixed $currentUsers, array $membersData): void
    {
        foreach ($currentUsers as $currentUser) {
            $found = $this->isUserInData($currentUser, $membersData);

            if (!$found) {
                $currentUser->removeGroup($groupParticipant);
            }
        }
    }

    private function isUserInData(User $user, array $membersData): bool
    {
        foreach ($membersData as $userData) {
            if ($user->getEmail() === $userData['email']) {
                return true;
            }
        }

        return false;
    }

    private function addNewUsers(GroupParticipant $groupParticipant, array $membersData): void
    {
        foreach ($membersData as $userData) {
            $user = $this->userRepository->findOneBy(['email' => $userData['email'], 'client' => $groupParticipant->getClient()]);

            if ($user) {
                $user->addGroup($groupParticipant);
            }
        }
    }

    private function handleException(Exception $exception): void
    {
        $this->logger->error($exception->getMessage());
        throw new ErrorException($exception->getMessage());
    }

    public function extractDataGroups(array $allClientGroups): array
    {
        $fullDataGroups = [];

        foreach ($allClientGroups as $group) {
            $fullDataGroups[$group->getId()] = $this->fullDataGroup($group);
        }

        return $fullDataGroups;
    }

    private function fullDataGroup(GroupParticipant $groupParticipant): array
    {
        return [
            'id' => $groupParticipant->getId(),
            'name' => $groupParticipant->getName(),
            'active' => $groupParticipant->isActive(),
            'parent' => $groupParticipant->getParent()?->getId(),
            'client' => $groupParticipant->getClient()->getId(),
            'numberUsers' => $groupParticipant->getNumberUsers(),
            'emailsUsers' => $groupParticipant->getEmailsUsers(),
            'idsUsers' => $groupParticipant->getIdsUsers(),
        ];
    }
}
