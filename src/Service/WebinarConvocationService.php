<?php

namespace App\Service;

use App\Entity\ActivityLog;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectTelephonyProfile;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\Adobe\AdobeConnectWebinarTelephonyProfile;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexMeetingTelephonyProfile;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Cisco\WebexRestMeetingTelephonyProfile;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\Guest;
use App\Entity\GuestUnsubscribe;
use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\Microsoft\MicrosoftTeamsEventTelephonyProfile;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\Microsoft\MicrosoftTeamsTelephonyProfile;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\ProviderSessionAccessToken;
use App\Entity\SessionConvocationAttachment;
use App\Entity\User;
use App\Entity\WebinarConvocation;
use App\Entity\White\WhiteParticipant;
use App\Entity\White\WhiteSession;
use App\Exception\InvalidArgumentException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Model\GeneratorTrait;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\SmsBounceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;

class WebinarConvocationService
{
    use GeneratorTrait;

    public const SHORTS_CODES = [
        '__name__' => '{{ name }}',
        '__duration__' => '{{ duration | humanizeDuration(langue) }}',
        '__date_start__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateOnly(langue) }}',
        '__date_start_time_zone__' => '{{ dateStartTimeZone }}',
        '__date_start_year__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateYearOnly }}',
        '__date_start_month__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateMonthOnly }}',
        '__date_start_month_text__' => '{{ dateStart | applyTimezone(clientTimeZone) | humanizeMonth(langue) }}',
        '__date_start_day__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateDayOnly }}',
        '__date_start_day_text__' => '{{ dateStart | applyTimezone(clientTimeZone) | humanizeWeekDay(langue) }}',
        '__date_start_day_suffixed__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateDaySuffixedOnly(langue) }}',
        '__hour_start__' => '{{ dateStart | applyTimezone(clientTimeZone) | hourOnly }}',
        '__hour_start_12h__' => '{{ dateStart | applyTimezone(clientTimeZone) | hourH12Only(langue) }}',
        '__hour_end__' => '{{ dateEnd | applyTimezone(clientTimeZone) | hourOnly }}',
        '__hour_end_12h__' => '{{ dateEnd | applyTimezone(clientTimeZone) | hourH12Only(langue) }}',
        '__langue__' => '{{ langue | humanizeLang(langue) }}',
        '__login_url__' => '{{ login_url }}',
        '__content__' => '{{ personalizedContent | raw  }}',
        '__personalized_content__' => '{{ personalizedContent | raw }}',
        '__username__' => '{{ username }}',
        '__salle_url__' => '{{ salleUrl }}',
        '__lieu__' => '{{ lieu }}',
        '__objectifs__' => '{{ objectifs }}',
        '__room_url__' => '{{ ACRoomUrl }}',
        '__room_url_universal__' => '{{ joinSessionWebUrl }}',
        '__room_url_web_link__' => '{{ meetingRestWebLink }}',
        '__connection_information_universal__' => '{{ connectionInformation }}',
        '__audio_real_number__' => '{{ audioRealNumber }}',
        '__audio_shared_number__' => '{{ audioSharedNumber }}',
        '__audio_animator_code__' => '{{ audioAnimatorCode }}',
        '__audio_animator_code_concat__' => '{{ audioAnimatorCodeConcat }}',
        '__audio_participant_code__' => '{{ audioParticipantCode }}',
        '__audio_meetingone_na_conference_id__' => '{{ audioMeetingOneConferenceId }}',
        '__audio_meetingone_conference_id__' => '{{ audioMeetingOneConferenceId }}',
        '__if_is_animator__' => '{% if isAnAnimator %}',
        '__if_is_participant__' => '{% if isAParticipant %}',
        '__if_end__' => '{% endif %}',
        '__addevent_url__' => '{{ addEventUrl }}',
        '__if_eval_url_exist__' => '{% if evalUrl is not empty %}',
        '__eval_url__' => '{{ evalUrl }}',
        '__if_object_url_exist__' => '{% if objectUrl is not empty %}',
        '__object_url__' => '{{ objectUrl }}',
        '__acquis_link__' => '{{ acquisLink }}',
        '__animator_access_link__' => '{{ animatorAccessLink }}',
        '__objectifs_pres__' => '{{ objectifsPres }}',
        '__releve_pres__' => '{{ relevePres }}',
        '__eval_prest_pres__' => '{{ evalPrestPres }}',
        '__if_is_an_adobe_connect_session__' => '{% if isAnAdobeConnectSession %}',
        '__if_is_a_webex_meeting_session__' => '{% if isAWebexMeetingSession %}',
        '__if_is_a_microsoft_teams_session__' => '{% if isAMicrosoftTeamsSession %}',
        '__if_is_a_universal_connector_session__' => '{% if isAWhiteConnectorSession %}',
        '__nbr_days__' => '{{ nbrDays }}',
        '__rest_lunch__' => '{{ restLunch }}',
        '__location__' => '{{ location }}',
        '__if_is_presential_session__' => '{% if isAPresentialSession %}',
        '__if_is_mixte_session__' => '{% if isAMixteSession %}',
        '__if_is_distantial_participant__' => '{% if isADistantialParticipant %}',
        '__if_is_presential_participant__' => '{% if isAPresentialParticipant %}',
        '__licence_microsoft_teams_email__' => '{{ licenceMicrosoftTeamsEmail }}',
        '__licence_microsoft_teams_password__' => '{{ licenceMicrosoftTeamsPassword }}',
        '__if_is_a_microsoft_teams_shared_licence__' => '{% if isLicenceSharedMicrosoftTeams %}',
        '__if_is_an_adobe_connect_shared_licence__' => '{% if isLicenceSharedAdobeConnect %}',
        '__licence_adobe_connect_email__' => '{{ licenceAdobeConnectEmail }}',
        '__licence_adobe_connect_password__' => '{{ licenceAdobeConnectPassword }}',
        '__animator_list__' => '{{ animatorsList }}',
        '__host_opening_session__' => '{{ licenceMicrosoftTeamsHostOpening }}',
        '__host_opening_session_name__' => '{{ licenceMicrosoftTeamsHostOpeningName }}',
        '__host_opening_session_email__' => '{{ licenceMicrosoftTeamsHostOpeningEmail }}',
        '__host_opening_session_password__' => '{{ licenceMicrosoftTeamsHostOpeningPassword }}',
        '__session_replay__' => '{{ sessionReplay }}',
    ];

    public function __construct(
        private Environment $twig,
        private TranslatorInterface $translator,
        private ConvocationService $convocationService,
        private SMSService $smsService,
        private RouterInterface $router,
        private string $endPointDomain,
        private ActivityLogger $activityLogger,
        private MailerService $mailerService,
        private EntityManagerInterface $entityManager,
        private string $convocationAttachmentSavefilesDirectory,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        private ReferrerService $referrerService,
        private SmsBounceRepository $smsBounceRepository
    ) {
    }

    public function getTemplateParameters(Guest $guest): array
    {
        $providerSession = $guest->getWebinarConvocation()->getSession();

        if (empty($providerSession->getSubject())) {
            $name = $providerSession->getName();
        } else {
            $name = $providerSession->getSubject()->getName();
        }

        $dateformat = $providerSession->getLanguage() == 'en' ? 'h:i a (Y/m/d)' : 'H:i (d/m/Y';
        $parameters = [
            'clientTimeZone' => $providerSession->getClient()->getTimezone(),
            'name' => $name,
            'duration' => $providerSession->getDuration(),
            'dateStart' => $providerSession->getDateStart(),
            'dateStartTimeZone' => implode('<br>', array_map(
                fn (string $ppsr) => "• $ppsr (GMT ".date('P', $providerSession->getDateStart()->getTimestamp()).') : '.date($dateformat, $providerSession->getDateStart()->getTimestamp()),
                $providerSession->getAdditionalTimezones()
            )),
            'dateEnd' => $providerSession->getDateEnd(),
            'langue' => $providerSession->getLanguage(),
            'login_url' => '',
            'username' => "{$guest->getFirstName()} {$guest->getLastName()}",
            'salleUrl' => 'https://plop.fr',
            'lieu' => 'Paris',
            'objectifs' => implode('<br>', array_map(
                fn ($objective) => "• $objective",
                $providerSession->getObjectives() ?? []
            )),
            'isAnAnimator' => false,
            'isAParticipant' => false,
            'addEventUrl' => '',
            'evalUrl' => 'https://plop.fr',
            'objectUrl' => 'https://plop.fr',
            'acquisLink' => 'https://plop.fr',
            'animatorAccessLink' => '',
            'objectifsPres' => 'Hellow',
            'relevePres' => $this->endPointDomain.$this->router->generate('_session_signature_lobby', ['id' => $providerSession->getId()]),
            'evalPrestPres' => $this->endPointDomain.$this->router->generate('_session_evaluation_lobby', ['id' => $providerSession->getId()]),
            'nbrDays' => $providerSession->getNbrdays(),
            'personalizedContent' => $providerSession->getPersonalizedContent(),
            'restLunch' => $providerSession->getRestlunch(),
            'location' => $providerSession->getLocation(),
            'isAPresentialSession' => $providerSession->isSessionPresential(),
            'isAMixteSession' => $providerSession->isSessionMixte(),
            'isADistantialParticipant' => false,
            'isAPresentialParticipant' => false,
            'isAnAdobeConnectSession' => $providerSession instanceof AdobeConnectSCO,
            'isAWebexMeetingSession' => $providerSession instanceof WebexMeeting,
            'isAWebexRestMeetingSession' => $providerSession instanceof WebexRestMeeting,
            'isAMicrosoftTeamsSession' => $providerSession instanceof MicrosoftTeamsSession,
            'isAMicrosoftTeamsEventSession' => $providerSession instanceof MicrosoftTeamsEventSession,
            'isAWhiteConnectorSession' => $providerSession instanceof WhiteSession,
            'animatorsList' => implode('<br>', array_map(
                fn (ProviderParticipantSessionRole $ppsr) => "• {$ppsr->getParticipant()->getFullName()}",
                $providerSession->getAnimatorsOnly()
            )),
            'sessionReplay' => $this->endPointDomain.$this->router->generate('_weblink_access_replay', ['client' => $providerSession->getClient()->getSlug(), 'session' => $providerSession->getId(), 'token' => $guest->getToken()]),
        ];

        $providerSessionParameters = $this->getProviderSessionParameters($providerSession);

        return array_merge($parameters, $providerSessionParameters);
    }

    private function getProviderSessionParameters(ProviderSession $providerSession): array
    {
        switch (get_class($providerSession)) {
            case AdobeConnectSCO::class:
                /** @var AdobeConnectTelephonyProfile|null $providerAudio */
                $providerAudio = $providerSession->getProviderAudio();

                $sharedLicence = $providerSession->getLicenceShared();

                return [
                    'ACRoomUrl' => $providerSession->getFullUrlpath(),
                    'audioRealNumber' => $providerAudio?->getPhoneNumber() ?? $providerAudio?->getAdobeDefaultPhoneNumber(),
                    'audioSharedNumber' => $providerAudio?->getAdobeDefaultPhoneNumber() ?? '',
                    'audioAnimatorCode' => $providerAudio?->getCodeAnimator() ?? '',
                    'audioParticipantCode' => $providerAudio?->getCodeParticipant() ?? '',
                    'audioMeetingOneConferenceId' => $providerAudio?->getMeetingOneConferenceId() ?? '',
                    'isLicenceSharedAdobeConnect' => is_array($sharedLicence),
                    'licenceAdobeConnectEmail' => $sharedLicence['email'] ?? '',
                    'licenceAdobeConnectPassword' => $sharedLicence['password'] ?? '',
                ];

            case AdobeConnectWebinarSCO::class:
                /** @var AdobeConnectWebinarTelephonyProfile|null $providerAudio */
                $providerAudio = $providerSession->getProviderAudio();

                $sharedLicence = $providerSession->getLicenceShared();

                return [
                    'ACRoomUrl' => $providerSession->getFullUrlpath(),
                    'audioRealNumber' => $providerAudio?->getPhoneNumber() ?? $providerAudio?->getAdobeDefaultPhoneNumber(),
                    'audioSharedNumber' => $providerAudio?->getAdobeDefaultPhoneNumber() ?? '',
                    'audioAnimatorCode' => $providerAudio?->getCodeAnimator() ?? '',
                    'audioParticipantCode' => $providerAudio?->getCodeParticipant() ?? '',
                    'audioMeetingOneConferenceId' => $providerAudio?->getMeetingOneConferenceId() ?? '',
                    'isLicenceSharedAdobeConnect' => is_array($sharedLicence),
                    'licenceAdobeConnectEmail' => $sharedLicence['email'] ?? '',
                    'licenceAdobeConnectPassword' => $sharedLicence['password'] ?? '',
                ];

            case WebexMeeting::class:
                /** @var WebexMeetingTelephonyProfile|null $providerAudio */
                $providerAudio = $providerSession->getProviderAudio();

                return [
                    'audioRealNumber' => $providerAudio?->getPhoneNumber() ?? '',
                    'audioAnimatorCode' => $providerAudio?->getCodeAnimator() ?? '',
                    'audioAnimatorCodeConcat' => !empty($providerAudio?->getCodeAnimator())
                        ? str_replace(' ', '', $providerAudio->getCodeAnimator())
                        : '',
                    'audioParticipantCode' => $providerAudio?->getCodeParticipant() ?? '',
                ];

            case WebexRestMeeting::class:
                /** @var WebexRestMeetingTelephonyProfile|null $providerAudio */
                $providerAudio = $providerSession->getProviderAudio();

                /* @var WebexRestMeeting  $providerSession */
                return [
                    'audioRealNumber' => $providerAudio?->getPhoneNumber() ?? '',
                    'audioAnimatorCode' => $providerAudio?->getCodeAnimator() ?? '',
                    'audioAnimatorCodeConcat' => !empty($providerAudio?->getCodeAnimator())
                        ? str_replace(' ', '', $providerAudio->getCodeAnimator())
                        : '',
                    'audioParticipantCode' => $providerAudio?->getCodeParticipant() ?? '',
                    'meetingRestWebLink' => $providerSession->getMeetingRestWebLink() ?? '',
                ];

            case MicrosoftTeamsSession::class:
                /** @var MicrosoftTeamsTelephonyProfile|null $providerAudio */
                $providerAudio = $providerSession->getProviderAudio();

                /** @var MicrosoftTeamsConfigurationHolder $configurationHolder */
                $configurationHolder = $providerSession->getAbstractProviderConfigurationHolder();

                foreach ($providerSession->getParticipantRoles() as $ppsr) {
                    if ($ppsr->getRole() === ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                        $licenceMicrosoftTeamsEmail = $providerSession->getLicence();
                        $licenceMicrosoftTeamsPassword = $configurationHolder->getLicences()[$licenceMicrosoftTeamsEmail]['password'];
                        $isLicenceSharedMicrosoftTeams = $configurationHolder->getLicences()[$licenceMicrosoftTeamsEmail]['shared'];
                    }
                }

                return [
                    'audioRealNumber' => $providerAudio?->getPhoneNumber() ?? '',
                    'audioAnimatorCode' => $providerAudio?->getCodeAnimator() ?? '',
                    'audioAnimatorCodeConcat' => !empty($providerAudio?->getCodeAnimator())
                        ? str_replace(' ', '', $providerAudio->getCodeAnimator())
                        : '',
                    'audioParticipantCode' => $providerAudio?->getCodeParticipant() ?? '',
                    'microsoftTeamsDefaultPhoneNumber' => $providerAudio?->getMicrosoftTeamsDefaultPhoneNumber() ?? '',
                    'audioConferencingDailinUrl' => $providerAudio?->getAudioConferencing()['dailinUrl'] ?? '',
                    'audioConferencingTollNumber' => $providerAudio?->getAudioConferencing()['tollNumber'] ?? '',
                    'audioConferencingTollFreeNumber' => $providerAudio?->getAudioConferencing()['tollFreeNumber'] ?? '',
                    'audioConferencingConferenceId' => $providerAudio?->getAudioConferencing()['ConferenceId'] ?? '',
                    'licenceMicrosoftTeamsEmail' => (!empty($licenceMicrosoftTeamsEmail)) ? $licenceMicrosoftTeamsEmail : '',
                    'licenceMicrosoftTeamsPassword' => (!empty($licenceMicrosoftTeamsPassword)) ? $licenceMicrosoftTeamsPassword : '',
                    'isLicenceSharedMicrosoftTeams' => (!empty($isLicenceSharedMicrosoftTeams)) ? $isLicenceSharedMicrosoftTeams : '',
                ];

            case MicrosoftTeamsEventSession::class:
                /** @var MicrosoftTeamsEventTelephonyProfile|null $providerAudio */
                $providerAudio = $providerSession->getProviderAudio();

                /** @var MicrosoftTeamsEventConfigurationHolder $configurationHolder */
                $configurationHolder = $providerSession->getAbstractProviderConfigurationHolder();

                foreach ($providerSession->getParticipantRoles() as $ppsr) {
                    if ($ppsr->getRole() === ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                        $licenceMicrosoftTeamsEventEmail = $providerSession->getLicence();
                        $licenceMicrosoftTeamsEventPassword = $configurationHolder->getLicences()[$licenceMicrosoftTeamsEventEmail]['password'];
                        $isLicenceSharedMicrosoftTeamsEvent = $configurationHolder->getLicences()[$licenceMicrosoftTeamsEventEmail]['shared'];
                    }
                }

                /** @var ProviderParticipantSessionRole $animatorLicence */
                $animatorLicence = $providerSession->getParticipantRoles()->filter(fn (ProviderParticipantSessionRole $ppsr) => $ppsr->getParticipant()->getEmail() === $providerSession->getLicence())->first();
                $email = (!empty($licenceMicrosoftTeamsEventEmail)) ? $licenceMicrosoftTeamsEventEmail : '';

                return [
                    'audioRealNumber' => $providerAudio?->getPhoneNumber() ?? '',
                    'audioAnimatorCode' => $providerAudio?->getCodeAnimator() ?? '',
                    'audioAnimatorCodeConcat' => !empty($providerAudio?->getCodeAnimator())
                        ? str_replace(' ', '', $providerAudio->getCodeAnimator())
                        : '',
                    'audioParticipantCode' => $providerAudio?->getCodeParticipant() ?? '',
                    'microsoftTeamsDefaultPhoneNumber' => $providerAudio?->getMicrosoftTeamsEventDefaultPhoneNumber() ?? '',
                    'audioConferencingDailinUrl' => $providerAudio?->getAudioConferencing()['dailinUrl'] ?? '',
                    'audioConferencingTollNumber' => $providerAudio?->getAudioConferencing()['tollNumber'] ?? '',
                    'audioConferencingTollFreeNumber' => $providerAudio?->getAudioConferencing()['tollFreeNumber'] ?? '',
                    'audioConferencingConferenceId' => $providerAudio?->getAudioConferencing()['ConferenceId'] ?? '',
                    'licenceMicrosoftTeamsEmail' => $email,
                    'licenceMicrosoftTeamsPassword' => (!empty($licenceMicrosoftTeamsEventPassword)) ? $licenceMicrosoftTeamsEventPassword : '',
                    'isLicenceSharedMicrosoftTeams' => (!empty($isLicenceSharedMicrosoftTeamsEvent)) ? $isLicenceSharedMicrosoftTeamsEvent : '',
                    'licenceMicrosoftTeamsHostOpening' => sprintf('%s (%s)', $animatorLicence->getFullName() ?? '', $email),
                    'licenceMicrosoftTeamsHostOpeningName' => "{$animatorLicence->getFirstName()} {$animatorLicence->getLastName()}",
                    'licenceMicrosoftTeamsHostOpeningEmail' => $email,
                    'licenceMicrosoftTeamsHostOpeningPassword' => 'uwuw',
                ];

            case WhiteSession::class:
                /* @var WhiteSession $providerSession */
                return [
                    'joinSessionWebUrl' => $providerSession->getJoinSessionWebUrl(),
                    'connectionInformation' => $providerSession->getConnectionInformation(),
                ];

            default:
                return [];
        }
    }

    /**
     * @throws LoaderError
     * @throws SyntaxError
     */
    public function transformShortsCodes(
        string $rawConvocationContent,
        Guest $guest
    ): string {
        $shortsCodes = [];

        foreach (self::SHORTS_CODES as $k => $shortsCode) {
            $shortsCodes["/$k/"] = $shortsCode;
        }

        $cleanConvocationContent = preg_replace(array_keys($shortsCodes), $shortsCodes, $rawConvocationContent);

        $templateTwig = $this->twig->createTemplate($cleanConvocationContent);

        return html_entity_decode($templateTwig->render($this->getTemplateParameters($guest)));
    }

    /**
     * @throws LoaderError
     * @throws SyntaxError
     * @throws InvalidArgumentException
     */
    public function sendInvitation(WebinarConvocation $invitation): void
    {
        $session = $invitation->getSession();
        $attachments = [];
        /** @var SessionConvocationAttachment $attachment */
        foreach ($invitation->getAttachments() as $attachment) {
            $attachments[] = ['path' => $this->convocationAttachmentSavefilesDirectory.'/'.$attachment->getFileName(), 'name' => $attachment->getOriginalName()];
        }

        foreach ($invitation->getGuests() as $guest) {
            if (!$this->isEmailValid($guest->getEmail())) {
                continue;
            }
            $guestUnsubscribe = $this->entityManager->getRepository(GuestUnsubscribe::class)->findBy(['email' => $guest->getEmail(), 'client' => $guest->getClient()]);
            if ($guestUnsubscribe !== []) {
                continue;
            }
            $subject = $this->transformShortsCodes($invitation->getSubjectMail(), $guest);
            $content = $this->transformShortsCodes($invitation->getContent(), $guest);

            $this->activityLogger->initActivityLogContext(
                $session->getClient(),
                null,
                ActivityLog::ORIGIN_CRON,
                [
                    'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                ]
            );
            $address = new Address($guest->getEmail(), "{$guest->getFirstName()} {$guest->getLastName()}");

            $this->mailerService->sendTemplatedMail(
                $address,
                $subject,
                'emails/participant/invitation.html.twig',
                [
                    'content' => $content,
                    'client' => $session->getClient()->getName(),
                    'clientService' => $session->getClient(),
                    'session' => $session,
                    'token' => $guest->getToken(),
                ],
                pathToAttach: $attachments,
                metadatas: [
                    'origin' => MailerService::ORIGIN_EMAIL_SEND_CONVOCATION,
                    'session' => $session->getId(),
                    'guest_id' => $guest->getId(),
                    'type' => WebinarConvocation::TYPE_INVITATION,
                ]
            );
            $guest->setEmailSent(true);
            $message = [
                'key' => 'The invitation for <a>%userName%</a> has been successfully sent for the <a href="%sessionRoute%">%sessionName%</a>.',
                'params' => [
                    '%userName%' => $guest->getFullName().'('.$guest->getEmail().')',
                    '%sessionName%' => $session->getName(),
                    '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                ],
            ];
            $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SEND_CONVOCATION, ActivityLog::SEVERITY_INFORMATION, ['message' => $message]));
        }
        $invitation->setSent(true);
    }

    /**
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function sendSms(WebinarConvocation $smsCommunication): void
    {
        $session = $smsCommunication->getSession();
        /** @var ProviderParticipantSessionRole $participant */
        foreach ($session->getTraineesOnly() as $participant) {
            if ($participant->getParticipant()->getUser()->getPhone() === null) {
                continue;
            }
            $content = $this->convocationService->transformShortsCodes($smsCommunication->getContent(), $participant);
            $params = [
                'sender' => $smsCommunication->getSender(),
                'recipient' => $participant->getParticipant()->getUser()->getPhone(),
                'content' => $content,
                'tag' => json_encode([
                    'type' => 'sendSms',
                    'session' => $smsCommunication->getSession()->getId(),
                    'user' => $participant->getParticipant()->getUser()->getId(),
                ]),
                'client' => $smsCommunication->getSession()->getClient(),
            ];

            $responses = $this->smsService->sendSmsBrevo($params);

            $this->activityLogger->initActivityLogContext(
                $session->getClient(),
                null,
                ActivityLog::ORIGIN_CRON,
                [
                    'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                ]
            );
            if (!isset($responses['error'])) {
                $message = [
                    'key' => 'The sms for <a>%userName%</a> has been successfully sent for the <a href="%sessionRoute%">%sessionName%</a>.',
                    'params' => [
                        '%userName%' => $participant->getFullName().'('.$participant->getParticipant()->getUser()->getPhone().')',
                        '%sessionName%' => $session->getName(),
                        '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                    ],
                ];
                $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SEND_CONVOCATION, ActivityLog::SEVERITY_INFORMATION, ['message' => $message]));
            } else {
                $message = [
                    'key' => 'The sms for <a>%userName%</a> has not been sent for the <a href="%sessionRoute%">%sessionName%</a>.',
                    'params' => [
                        '%userName%' => $participant->getFullName().'('.$participant->getParticipant()->getUser()->getPhone().')',
                        '%sessionName%' => $session->getName(),
                        '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                    ],
                ];
                $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SEND_CONVOCATION, ActivityLog::SEVERITY_ERROR, ['message' => $message]));
            }
        }
        $smsCommunication->setSent(true);
    }

    public function unsubscribeEmailGuest(Guest $guest): void
    {
        $guest->setUnsubscribe(true);
        $guestUnsubscribe = (new GuestUnsubscribe())
            ->setEmail($guest->getEmail())
            ->setClient($guest->getClient());
        $this->entityManager->persist($guestUnsubscribe);
    }

    public function statisticsInvitationGuests(ProviderSession $providerSession): array
    {
        $statistics = [];
        foreach ($providerSession->getWebinarConvocation() as $communication) {
            if ($communication->getConvocationType() === WebinarConvocation::TYPE_INVITATION) {
                $statistics[$communication->getId()] = $this->getStatisticsInvitationGuests($communication);
            }
        }

        return $statistics;
    }

    public function getStatisticsInvitationGuests(WebinarConvocation $invitation): array
    {
        $statisticsInvitation = [
            'numberRegistered' => 0,
            'numberDelivered' => 0,
            'numberUnsubscribes' => 0,
            'numberOpened' => 0,
            'numberClicked' => 0,
            'numberBounces' => 0,
        ];

        foreach ($invitation->getGuests() as $guest) {
            $statisticsInvitation['numberRegistered'] += $this->isInscritInSession($guest) ? 1 : 0;
            $statisticsInvitation['numberDelivered'] += $guest->isEmailSent() ? 1 : 0;
            $statisticsInvitation['numberUnsubscribes'] += $guest->isUnsubscribe() ? 1 : 0;
            $statisticsInvitation['numberOpened'] += $guest->getOpened();
            $statisticsInvitation['numberClicked'] += $guest->getClicked();
            $statisticsInvitation['numberBounces'] += $guest->isBounce() ? 1 : 0;
        }

        return $statisticsInvitation;
    }

    public function getSmsStatistics(ProviderSession $session): int
    {
        return $this->smsBounceRepository->countBouncesBySession($session);
    }

    public function isInscritInSession(Guest $guest): bool
    {
        return (bool) $this->providerParticipantSessionRoleRepository->getParticipantWithSessionAndEmail(
            $guest->getWebinarConvocation()->getSession(),
            $guest->getEmail()
        );
    }

    public function isEmailValid(string $email): mixed
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @throws SyntaxError
     * @throws LoaderError
     * @throws InvalidArgumentException
     */
    public function testSendSms(WebinarConvocation $smsCommunication, string $recipient): array
    {
        $user = (new User())->setFirstName('Jean')->setLastName('fsfsfq');

        $accessToken = (new ProviderSessionAccessToken())
            ->setToken('MyToken');

        $participant = (new WhiteParticipant($smsCommunication->getSession()->getAbstractProviderConfigurationHolder()))
            ->setUser($user);

        $ppsr = (new ProviderParticipantSessionRole())
            ->setSession($smsCommunication->getSession())
            ->setProviderSessionAccessToken($accessToken);
        $ppsr->setParticipant($participant);
        $content = $this->convocationService->transformShortsCodes($smsCommunication->getContent(), $ppsr);

        $params = [
            'sender' => $smsCommunication->getSender(),
            'recipient' => $recipient,
            'content' => $content,
            'tag' => json_encode([
                'type' => 'testSendSms',
//                'session' => $smsCommunication->getSession()->getId()
            ]),
            'client' => $smsCommunication->getSession()->getClient(),
        ];

        $responses = $this->smsService->sendSmsBrevo($params);

        if (!isset($responses['error'])) {
            return ['success' => 'The sms has been successfully sent.'];
        } else {
            return $responses;
        }
    }
}
