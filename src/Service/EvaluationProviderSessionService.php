<?php

namespace App\Service;

use App\Entity\ActivityLog;
use App\Entity\Client\Client;
use App\Entity\Client\EvaluationModalMail;
use App\Entity\Evaluation;
use App\Entity\EvaluationProviderSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\SessionConvocationAttachment;
use App\Entity\User;
use App\Exception\InvalidArgumentException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Model\GeneratorTrait;
use App\Repository\ProviderParticipantSessionRoleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;

class EvaluationProviderSessionService
{
    use GeneratorTrait;

    public const SHORTS_CODES = [
        '__name__' => '{{ name }}',
        '__duration__' => '{{ duration | humanizeDuration(langue) }}',
        '__date_start__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateOnly(langue) }}',
        '__date_start_time_zone__' => '{{ dateStartTimeZone }}',
        '__date_start_year__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateYearOnly }}',
        '__date_start_month__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateMonthOnly }}',
        '__date_start_month_text__' => '{{ dateStart | applyTimezone(clientTimeZone) | humanizeMonth(langue) }}',
        '__date_start_day__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateDayOnly }}',
        '__date_start_day_text__' => '{{ dateStart | applyTimezone(clientTimeZone) | humanizeWeekDay(langue) }}',
        '__date_start_day_suffixed__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateDaySuffixedOnly(langue) }}',
        '__hour_start__' => '{{ dateStart | applyTimezone(clientTimeZone) | hourOnly }}',
        '__hour_start_12h__' => '{{ dateStart | applyTimezone(clientTimeZone) | hourH12Only(langue) }}',
        '__hour_end__' => '{{ dateEnd | applyTimezone(clientTimeZone) | hourOnly }}',
        '__hour_end_12h__' => '{{ dateEnd | applyTimezone(clientTimeZone) | hourH12Only(langue) }}',
        '__langue__' => '{{ langue | humanizeLang(langue) }}',
        '__username__' => '{{ username }}',
        '__objectifs__' => '{{ objectifs }}',
        '__if_is_animator__' => '{% if isAnAnimator %}',
        '__if_is_participant__' => '{% if isAParticipant %}',
        '__if_end__' => '{% endif %}',
        '__evaluation_link__' => '{{ evaluationLink }}',
        '__evaluation_link_personally__' => '{{ evaluationLinkPersonally }}',
        '__nbr_days__' => '{{ nbrDays }}',
        '__rest_lunch__' => '{{ restLunch }}',
        '__location__' => '{{ location }}',
        '__if_is_presential_session__' => '{% if isAPresentialSession %}',
        '__if_is_mixte_session__' => '{% if isAMixteSession %}',
        '__if_is_distantial_participant__' => '{% if isADistantialParticipant %}',
        '__if_is_presential_participant__' => '{% if isAPresentialParticipant %}',
        '__animator_list__' => '{{ animatorsList }}',
    ];

    public function __construct(
        private ProviderParticipantSessionRoleRepository $participantSessionRoleRepository,
        private Environment $twig,
        private TranslatorInterface $translator,
        private RouterInterface $router,
        private string $endPointDomain,
        private ActivityLogger $activityLogger,
        private MailerService $mailerService,
        private EntityManagerInterface $entityManager,
        private string $convocationAttachmentSavefilesDirectory,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        private ReferrerService $referrerService
    ) {
    }

    public function createNewEvaluationProviderSession(Evaluation $evaluation, ProviderSession $session, string $type): EvaluationProviderSession
    {
        $evaluationProviderSession = new EvaluationProviderSession($evaluation, $session);
        $modalMail = $this->getModalEvaluationForClient($session->getClient(), $type);
        if ($modalMail) {
            $evaluationProviderSession->setSubjectMail($modalMail->getSubjectMail())
                ->setContent($modalMail->getContent())
                ->setAttachments($modalMail->getAttachments())
                ->setEvaluationType($type);
            $this->addAttachmentsModalToNewEvaluation($evaluationProviderSession, $modalMail);
        } else {
            $isEnglish = $session->getLanguage() == 'en';
            $getSubjectMailMethod = $isEnglish ? 'getSubjectMailProposalEnglish' : 'getSubjectMailProposalFrench';
            $getContentMailMethod = $isEnglish ? 'getContentMailProposalEnglish' : 'getContentMailProposalFrench';

            $evaluationProviderSession->setSubjectMail($this->$getSubjectMailMethod($type))
                ->setContent($this->$getContentMailMethod($type))
                ->setEvaluationType($type);
        }

        return $evaluationProviderSession;
    }

    public function getModalEvaluationForClient(Client $client, string $type): ?EvaluationModalMail
    {
        $allEvaluationModalMail = $client->getEvaluationsModalMail();
        foreach ($allEvaluationModalMail as $evaluationModalMail) {
            if ($evaluationModalMail->getEvaluationType() === $type) {
                return $evaluationModalMail;
            }
        }

        return null;
    }

    public function getSubjectMailProposalFrench(string $type): string
    {
        return match ($type) {
            EvaluationProviderSession::EVALUATION_COLD => 'Évaluation d\'impression initiale de __name__ le __date_start__ de __hour_start__ à __hour_end__',
            EvaluationProviderSession::EVALUATION_ACQUIRED => 'Évaluation des compétences acquises pour __name__ - __date_start__ de __hour_start__ à __hour_end__',
            EvaluationProviderSession::EVALUATION_HOT => 'Demande de retour immédiat pour la session __name__ le __date_start__ de __hour_start__ à __hour_end__',
            EvaluationProviderSession::EVALUATION_POSITIONING => 'Évaluation de positionnement pour __name__ - __date_start__ de __hour_start__ à __hour_end__',
            EvaluationProviderSession::EVALUATION_KNOWLEDGE => 'Vérification des connaissances pour la session __name__ - __date_start__ de __hour_start__ à __hour_end__',
            EvaluationProviderSession::EVALUATION_OTHER => 'Évaluation spéciale pour __name__ - __date_start__ de __hour_start__ à __hour_end__',
            default => 'Évaluation générale de la session __name__ - __date_start__ de __hour_start__ à __hour_end__',
        };
    }

    public function getSubjectMailProposalEnglish(string $type): string
    {
        return match ($type) {
            EvaluationProviderSession::EVALUATION_COLD => 'Initial Impression Evaluation of __name__ on __date_start__ from __hour_start__ to __hour_end__',
            EvaluationProviderSession::EVALUATION_ACQUIRED => 'Acquired Skills Assessment for __name__ - __date_start__ from __hour_start__ to __hour_end__',
            EvaluationProviderSession::EVALUATION_HOT => 'Immediate Feedback Request for __name__ Session on __date_start__ from __hour_start__ to __hour_end__',
            EvaluationProviderSession::EVALUATION_POSITIONING => 'Positioning Evaluation for __name__ - __date_start__ from __hour_start__ to __hour_end__',
            EvaluationProviderSession::EVALUATION_KNOWLEDGE => 'Knowledge Check for __name__ Session - __date_start__ from __hour_start__ to __hour_end__',
            EvaluationProviderSession::EVALUATION_OTHER => 'Special Evaluation for __name__ - __date_start__ from __hour_start__ to __hour_end__',
            default => 'General Session Evaluation for __name__ - __date_start__ from __hour_start__ to __hour_end__',
        };
    }

    public function getContentMailProposalFrench(string $type): string
    {
        return match ($type) {
            EvaluationProviderSession::EVALUATION_COLD => '<p>Bonjour,</p><p>Votre impression initiale de la session __name__ est importante pour nous. Aidez-nous à évaluer votre première expérience le __date_start_day__ __date_start_month_text__ __date_start_year__.</p><p>Merci de remplir notre enquête d\'évaluation "à froid".</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
            EvaluationProviderSession::EVALUATION_ACQUIRED => '<p>Bonjour,</p><p>Nous espérons que vous avez trouvé la session __name__ enrichissante. Votre avis sur les compétences et connaissances acquises nous intéresse.</p><p>Merci de prendre un moment pour évaluer les compétences acquises lors de cette session.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
            EvaluationProviderSession::EVALUATION_HOT => '<p>Bonjour,</p><p>Merci d\'avoir participé à notre session dynamique __name__. Vos retours immédiats sont cruciaux pour nous.</p><p>Partagez votre expérience et vos impressions immédiates via notre enquête "à chaud".</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
            EvaluationProviderSession::EVALUATION_POSITIONING => '<p>Bonjour,</p><p>Votre positionnement dans la session __name__ est un élément clé pour nous. Aidez-nous à comprendre où vous vous situez.</p><p>Merci de remplir l\'enquête de positionnement liée à cette session.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
            EvaluationProviderSession::EVALUATION_KNOWLEDGE => '<p>Bonjour,</p><p>Après avoir participé à la session __name__, nous sommes curieux de connaître votre niveau de compréhension et de rétention.</p><p>Veuillez évaluer votre assimilation des connaissances dans notre enquête dédiée.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
            EvaluationProviderSession::EVALUATION_OTHER => '<p>Bonjour,</p><p>Votre participation à la session __name__ a été appréciée. Pour les besoins spécifiques de cette session, nous avons une enquête sur mesure pour vous.</p><p>Merci de prendre le temps de compléter cette enquête spécifique.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
            default => '<p>Bonjour,</p><p>Merci d\'avoir participé à la session __name__ le __date_start_day__ __date_start_month_text__ __date_start_year__. Nous apprécierions vos commentaires pour améliorer nos sessions.</p><p>Veuillez prendre un moment pour remplir notre enquête d\'évaluation.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>Si le lien est cass&eacute;, copiez/collez l\'URL ci-dessous dabs votre navigateur :&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>Nous vous remercions sinc&egrave;rement pour votre participation et vos pr&eacute;cieux commentaires. Votre avis nous est essentiel pour offrir des exp&eacute;riences toujours meilleures.</p><p>N\'h&eacute;sitez pas &agrave; nous contacter pour toute question ou commentaire suppl&eacute;mentaire.</p><p>Cordialement.</p>',
        };
    }

    public function getContentMailProposalEnglish(string $type): string
    {
        return match ($type) {
            EvaluationProviderSession::EVALUATION_COLD => '<p>Hello,</p><p>Your initial impression of the __name__ session is important to us. Help us evaluate your first experience on __date_start_day__ __date_start_month_text__ __date_start_year__.</p><p>Please fill out our "cold" evaluation survey.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments</p><p>Best regards.</p>',
            EvaluationProviderSession::EVALUATION_ACQUIRED => '<p>Hello,</p><p>We hope you found the __name__ session enriching. Your feedback on the skills and knowledge acquired is of interest to us.</p><p>Please take a moment to evaluate the skills acquired during this session.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments.</p><p>Best regards.</p>',
            EvaluationProviderSession::EVALUATION_HOT => '<p>Hello,</p><p>Thank you for participating in our dynamic __name__ session. Your immediate feedback is crucial to us.</p><p>Share your experience and immediate impressions via our "hot" survey.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44"/></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments.</p><p>Best regards.</p>',
            EvaluationProviderSession::EVALUATION_POSITIONING => '<p>Hello,</p><p>Your positioning in the __name__ session is a key element for us. Help us understand where you are.</p><p>Please fill out the positioning survey linked to this session.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments.</p><p>Best regards.</p>',
            EvaluationProviderSession::EVALUATION_KNOWLEDGE => '<p>Hello,</p><p>After participating in the __name__ session, we are curious to know your level of understanding and retention.</p><p>Please evaluate your assimilation of knowledge in our dedicated survey.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments.</p><p>Best regards.</p>',
            EvaluationProviderSession::EVALUATION_OTHER => '<p>Hello,</p><p>Your participation in the __name__ session was appreciated. For the specific needs of this session, we have a custom survey for you.</p><p>Please take the time to complete this specific survey.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44" /></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments.</p><p>Best regards.</p>',
            default => '<p>Hello,</p><p>Thank you for participating in the __name__ session on __date_start_day__ __date_start_month_text__ __date_start_year__. We would appreciate your feedback to improve our sessions.</p><p>Please take a moment to fill out our evaluation survey.</p><p><a href="__evaluation_link__" target="_blank" rel="noopener"><br><img style="display: block; margin-left: auto; margin-right: auto;" src="https://app.mylivesession.com/uploads/client_convocations_images/mceu-31877263811704875438701-659e55af0d471.png" width="294" height="44"/></a></p><p>If the link is broken, copy/paste the URL below into your browser:&nbsp;</p><p style="text-align: center;">__evaluation_link__</p><p>We sincerely thank you for your participation and your valuable feedback. Your feedback is essential to us to offer ever better experiences.</p><p>Do not hesitate to contact us for any additional questions or comments.</p><p>Best regards.</p>',
        };
    }

    /**
     * @throws LoaderError
     * @throws SyntaxError
     * @throws InvalidArgumentException
     */
    public function sendEvaluationMailToSelectedParticipant(EvaluationProviderSession $evaluationProviderSession, array $participants): void
    {
        $this->activityLogger->initActivityLogContext($evaluationProviderSession->getProviderSession()->getClient(), null, ActivityLog::ORIGIN_CRON,
            [
                'session' => [
                    'id' => $evaluationProviderSession->getProviderSession()->getId(),
                    'name' => $evaluationProviderSession->getProviderSession()->getName(),
                    'ref' => $evaluationProviderSession->getProviderSession()->getRef(), ],
            ]
        );

        $successSendEmails = [];
        foreach ($participants as $IdParticipant) {
            /** @var ProviderParticipantSessionRole $participantSessionRole */
            $participantSessionRole = $this->participantSessionRoleRepository->findOneBy(['id' => $IdParticipant]);
            if ($participantSessionRole && !$this->isEmailValid($participantSessionRole->getEmail())) {
                continue;
            }
            $successSendEmails[] = $this->sendEvaluationMailToParticipant($evaluationProviderSession, $participantSessionRole);
        }

        $infos = [
            'message' => [
                'key' => 'Evaluation emails for "%evaluationName%" have been sent to "%participantCount%" participant.s successfully for session <a href="%sessionRoute%">%sessionName%</a>.',
                'params' => [
                    '%evaluationName%' => $evaluationProviderSession->getEvaluation()->getTitle(),
                    '%participantCount%' => count($successSendEmails),
                    '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $evaluationProviderSession->getProviderSession()->getId()]),
                    '%sessionName%' => $evaluationProviderSession->getProviderSession()->getName(),
                ],
            ],
            'participants' => $successSendEmails,
            'links' => [['type' => 'component', 'name' => 'ModalEmailSendToParticipants']],
        ];
        $this->activityLogger->addActivityLog(
            new ActivityLogModel(ActivityLog::ACTION_SEND_EVALUATION_PARTICIPANT, ActivityLog::SEVERITY_INFORMATION, $infos)
        );

        $evaluationProviderSession
            ->setSendAt(new \DateTime())
            ->setSent(true);
    }

    /**
     * @throws SyntaxError
     * @throws InvalidArgumentException
     * @throws LoaderError
     */
    public function sendEvaluationMailToParticipant(EvaluationProviderSession $evaluationProviderSession, ProviderParticipantSessionRole $providerParticipantSessionRole, ?User $userSender = null): string|null
    {
        if (!$this->isEmailValid($providerParticipantSessionRole->getEmail())) {
            return null;
        }

        $attachments = [];
        /** @var SessionConvocationAttachment $attachment */
        foreach ($evaluationProviderSession->getAttachments() as $attachment) {
            $attachments[] = ['path' => $this->convocationAttachmentSavefilesDirectory.'/'.$attachment->getFileName(), 'name' => $attachment->getOriginalName()];
        }

        $subject = $this->transformShortsCodes(rawConvocationContent: $evaluationProviderSession->getSubjectMail(), participantSessionRole: $providerParticipantSessionRole, evaluationProviderSession: $evaluationProviderSession);
        $content = $this->transformShortsCodes(rawConvocationContent: $evaluationProviderSession->getContent(), participantSessionRole: $providerParticipantSessionRole, evaluationProviderSession: $evaluationProviderSession);

        $address = new Address($providerParticipantSessionRole->getEmail(), "{$providerParticipantSessionRole->getFirstName()} {$providerParticipantSessionRole->getLastName()}");

        $this->mailerService->sendTemplatedMail(
            $address,
            $subject,
            'emails/participant/evaluation_session_link_mail.html.twig',
            [
                'content' => $content,
                'client' => $evaluationProviderSession->getProviderSession()->getClient()->getName(),
                'clientService' => $evaluationProviderSession->getProviderSession()->getClient(),
                'session' => $evaluationProviderSession->getProviderSession(),
                'isAnimator' => $providerParticipantSessionRole->isAnAnimator(),
            ],
            pathToAttach: $attachments,
            metadatas: [
                'origin' => MailerService::ORIGIN_EMAIL_SEND_CONVOCATION,
                'user' => $providerParticipantSessionRole->getParticipant()->getUser()->getId(),
                'session' => $evaluationProviderSession->getProviderSession()->getId(),
            ],
        );

        return $providerParticipantSessionRole->getFullName().'('.$providerParticipantSessionRole->getEmail().')';
    }

    /**
     * @throws SyntaxError
     * @throws LoaderError
     * @throws InvalidArgumentException
     */
    public function sendMailEvaluationToAllParticipant(EvaluationProviderSession $evaluationProviderSession, ?User $userSender = null): void
    {
        $participants = $evaluationProviderSession->getProviderSession()->getTraineesOnly();
        foreach ($participants as $providerParticipantSessionRole) {
            $this->activityLogger->initActivityLogContext(
                $evaluationProviderSession->getProviderSession()->getClient(),
                $userSender,
                ($userSender !== null) ? ActivityLog::ORIGIN_USER_INTERFACE : ActivityLog::ORIGIN_CRON,
                [
                    'session' => [
                        'id' => $evaluationProviderSession->getProviderSession()->getId(),
                        'name' => $evaluationProviderSession->getProviderSession()->getName(),
                        'ref' => $evaluationProviderSession->getProviderSession()->getRef(), ],
                ]
            );

            $this->sendEvaluationMailToParticipant(evaluationProviderSession: $evaluationProviderSession, providerParticipantSessionRole: $providerParticipantSessionRole, userSender: $userSender);

            $message = [
                'key' => 'The evaluation %evaluationName% for <a>%userName%</a> has been successfully sent for the <a href="%sessionRoute%">%sessionName%</a>.',
                'params' => [
                    '%userName%' => $providerParticipantSessionRole->getFullName().'('.$providerParticipantSessionRole->getEmail().')',
                    '%sessionName%' => $evaluationProviderSession->getProviderSession()->getName(),
                    '%evaluationName%' => $evaluationProviderSession->getEvaluation()->getTitle(),
                    '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $evaluationProviderSession->getProviderSession()->getId()]),
                ],
            ];
            $this->activityLogger->addActivityLog(
                new ActivityLogModel(
                    ActivityLog::ACTION_SEND_EVALUATION_PARTICIPANT, ActivityLog::SEVERITY_INFORMATION, ['message' => $message]
                )
            );
        }
    }

    /**
     * @throws LoaderError
     * @throws SyntaxError
     */
    public function transformShortsCodes(
        string $rawConvocationContent,
        ProviderParticipantSessionRole $participantSessionRole,
        EvaluationProviderSession $evaluationProviderSession
    ): string {
        $shortsCodes = [];

        foreach (self::SHORTS_CODES as $k => $shortsCode) {
            $shortsCodes["/$k/"] = $shortsCode;
        }

        $cleanConvocationContent = preg_replace(array_keys($shortsCodes), $shortsCodes, $rawConvocationContent);

        $templateTwig = $this->twig->createTemplate($cleanConvocationContent);

        return html_entity_decode($templateTwig->render($this->getTemplateParameters(providerParticipantSessionRole: $participantSessionRole, evaluationProviderSession: $evaluationProviderSession)));
    }

    public function getTemplateParameters(ProviderParticipantSessionRole $providerParticipantSessionRole, EvaluationProviderSession $evaluationProviderSession): array
    {
        $providerSession = $evaluationProviderSession->getProviderSession();

        if (empty($providerSession->getSubject())) {
            $name = $providerSession->getName();
        } else {
            $name = $providerSession->getSubject()->getName();
        }

        $dateformat = $providerSession->getLanguage() == 'en' ? 'h:i a (Y/m/d)' : 'H:i (d/m/Y';
        $parameters = [
            'clientTimeZone' => $providerSession->getClient()->getTimezone(),
            'name' => $name,
            'duration' => $providerSession->getDuration(),
            'dateStart' => $providerSession->getDateStart(),
            'dateStartTimeZone' => implode('<br>', array_map(
                fn (string $providerParticipantSessionRole) => "• $providerParticipantSessionRole (GMT ".date('P', $providerSession->getDateStart()->getTimestamp()).') : '.date($dateformat, $providerSession->getDateStart()->getTimestamp()),
                $providerSession->getAdditionalTimezones()
            )),
            'dateEnd' => $providerSession->getDateEnd(),
            'langue' => $providerSession->getLanguage(),
            'username' => "{$providerParticipantSessionRole->getFirstName()} {$providerParticipantSessionRole->getLastName()}",
            'objectifs' => implode('<br>', array_map(
                fn ($objective) => "• $objective",
                $providerSession->getObjectives() ?? []
            )),
            'isAnAnimator' => $providerParticipantSessionRole->isAnAnimator(),
            'isAParticipant' => $providerParticipantSessionRole->isATrainee(),
            'evaluationLink' => $this->endPointDomain.$this->router->generate('_evaluation_session_lobby', ['id' => $evaluationProviderSession->getId()]),
            'evaluationLinkPersonally' => $this->endPointDomain.$this->router->generate('_participant_evaluation_session_answer', ['id' => $evaluationProviderSession->getId(), 'token' => $providerParticipantSessionRole->getProviderSessionAccessToken()->getToken()]),
            'nbrDays' => $providerSession->getNbrdays(),
            'restLunch' => $providerSession->getRestlunch(),
            'location' => $providerSession->getLocation(),
            'isAPresentialSession' => $providerSession->isSessionPresential(),
            'isAMixteSession' => $providerSession->isSessionMixte(),
            'isADistantialParticipant' => $providerParticipantSessionRole->getSituationMixte() === $providerParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL,
            'isAPresentialParticipant' => $providerParticipantSessionRole->getSituationMixte() === $providerParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL,
            'animatorsList' => implode('<br>', array_map(
                fn (ProviderParticipantSessionRole $providerParticipantSessionRole) => "• {$providerParticipantSessionRole->getFullName()}",
                $providerSession->getAnimatorsOnly()
            )),
        ];

        return $parameters;
    }

    public function isEmailValid(string $email): mixed
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    private function addAttachmentsModalToNewEvaluation(EvaluationProviderSession $evaluationProviderSession, EvaluationModalMail $modalMail): void
    {
        foreach ($modalMail->getAttachments() as $attachment) {
            $newAttachment = new SessionConvocationAttachment();
            $newAttachment->setOriginalName($attachment->getOriginalName());
            $newAttachment->setFileName($attachment->getFileName());
            $newAttachment->setEvaluationProviderSession($evaluationProviderSession);
            $this->entityManager->persist($newAttachment);
        }
    }
}
