<?php

namespace App\Service;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Repository\ProviderParticipantSessionRoleRepository;
use Psr\Log\LoggerInterface;
use SforceEnterpriseClient;
use SObject;

class ReferrerService
{
    private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository;
    private LoggerInterface $logger;
    private string $wsdlFirstFinance;

    public function __construct(ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository, LoggerInterface $logger, $wsdlFirstFinance)
    {
        $this->providerParticipantSessionRoleRepository = $providerParticipantSessionRoleRepository;
        $this->logger = $logger;
        $this->wsdlFirstFinance = $wsdlFirstFinance;
    }

    public function getReferrerFromUser(ProviderParticipantSessionRole $providerParticipantSessionRole): array
    {
        $ccs = [];
        if ($providerParticipantSessionRole->getParticipant()->getClient()->isEnableSendToReferrer()) {
            if (!empty($providerParticipantSessionRole->getParticipant()->getUser()->getEmailReferrer())) {
                $ccs[] = $providerParticipantSessionRole->getParticipant()->getUser()->getEmailReferrer();
            }
        }

        return $ccs;
    }

    public function updateReferrerUser(User $user): void
    {
        //$user->setEmailReferrer('testReferrerEmail@first-finance.com');
        //dump($user->getEmail().' < '.$user->getEmailReferrer().' >');

        //By dependance
        $emailReferrer = $this->SFDCConnection($user->getEmail());
        if (!empty($emailReferrer)) {
            $user->setEmailReferrer($emailReferrer);
        }
    }

    public function SFDCConnection(string $emailParticipant): ?string
    {
        $emailReferrer = null;
        ini_set('soap.wsdl_cache_enabled', '0');
        ini_set('soap.wsdl_cache_ttl', 0);

        $sfdcUsername = 'blog@first-finance.net';
        $sfdcPassword = 'phpconnector2013';
        $sfdcToken = 'GHk7THstp0oVp08FUXXqCjRc';

        $options = [
            'features' => 1,  //SOAP_SINGLE_ELEMENT_ARRAYS
            'compression' => 32 | 0, //SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP
            'exceptions' => true,
        ];

        try {
            //Login
            $sfdc = new SforceEnterpriseClient();
            $sfdc->SforceEnterpriseClient();

            // Instanciation $SObject, used for the conversion of the fields send by SOAP
            $sObject = new SObject();
            $this->logger->info('Get wsdl path: '.$this->wsdlFirstFinance);
            // Create the connection by SOAP
            $soapClient = $sfdc->createConnection($this->wsdlFirstFinance, null, $options);
            //dump($soapClient);
            // Authentification with username, password and security token
            $login = $sfdc->login($sfdcUsername, $sfdcPassword.$sfdcToken);
            //dump('Get session ID of SforceEnterpriseClient: '.$sfdc->getSessionId());
            $this->logger->info('Get session ID of SforceEnterpriseClient: '.$sfdc->getSessionId());

            // Search contact
            // TODO effectuer la bascule une fois la vérification web effectuée | CLI OK
            //$query = "SELECT Id, LastName, FirstName, Email, IP__c, RF_Centralise__c, RF__c FROM Contact WHERE Email = 'steeven.zouari@first-finance.fr'";
            $query = "SELECT Id, LastName, FirstName, Email, IP__c, RF_Centralise__c, RF__c FROM Contact WHERE Email = '".$emailParticipant."'";
            //print_r('Get email participant: '.$emailParticipant);
            $this->logger->info('Get email participant: '.$emailParticipant);

            $items = $sfdc->query($query);
            //dump($items->records);
            //print_r($sfdc->describeSObject('Lead'));
            if ($items->records) {
                foreach ($items->records as $item) {
                    if (!empty($item->RF__c)) {
                        $referrerId = $item->RF__c;
                        $this->logger->info('Get referrer ID: '.$referrerId);
                        //print_r($item->Id.' | '.$item->RF__c);
                        $queryReferrer = "SELECT Id, LastName, FirstName, Email, IP__c, RF_Centralise__c, RF__c FROM Contact WHERE Id = '".$referrerId."'";
                        $itemsReferrer = $sfdc->query($queryReferrer);
                        if ($itemsReferrer->records) {
                            foreach ($itemsReferrer->records as $itemReferrer) {
                                if (!empty($itemReferrer->Email)) {
                                    $emailReferrer = $itemReferrer->Email;
                                }
                            }
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception);
        }
        $this->logger->info('Get email referrer: '.$emailParticipant);

        return $emailReferrer;
    }
}
