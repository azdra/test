<?php

namespace App\Service;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\White\WhiteParticipant;
use App\Entity\White\WhiteSession;
use App\Exception\ProviderServiceValidationException;
use App\Model\ProviderResponse;
use App\Service\AsyncWorker\AsyncTaskService;
use App\Service\AsyncWorker\Tasks\SyncSessionRoleToMicrosoftTeamsSessionTask;
use App\Service\Connector\White\WhiteConnector;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class WhiteConnectorService extends AbstractProviderService
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected ValidatorInterface $validator,
        WhiteConnector $whiteConnector,
        protected AsyncTaskService $asyncTaskService,
        LoggerInterface $logger
    ) {
        parent::__construct($entityManager, $validator, $whiteConnector, $logger);
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof WhiteConfigurationHolder;
    }

    public function createSession(ProviderSession $providerSession): ProviderResponse
    {
        /* @var WhiteSession $providerSession */
        $sessionId = $this->entityManager->contains($providerSession) ? $providerSession->getId() : null;
        //$this->whiteLicensingService->licensingASession($providerSession, $sessionId);

        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['create']))) {
            throw new ProviderServiceValidationException($list);
        }

        $response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            WhiteConnector::ACTION_SESSION_CREATE,
            $providerSession
        );

        if ($response->isSuccess()) {
            $this->entityManager->persist($providerSession);
        }

        return $response;
    }

    /*public function changeSessionForNewLicence(WhiteSession $providerSession): ProviderResponse
    {
        $response = $this->deleteSession($providerSession);

        if ($response->isSuccess()) {
            $providerSession->setLicence(null);
            $response = $this->createSession($providerSession);

            if ($response->isSuccess()) {
                $this->entityManager->persist($providerSession);
            }
        }

        return $response;
    }*/

    public function deleteSession(ProviderSession $providerSession): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        $response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            WhiteConnector::ACTION_SESSION_DELETE,
            $providerSession
        );

        return $response;
    }

    public function createParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return (new ProviderResponse())->setSuccess(true);
    }

    public function updateParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return (new ProviderResponse())->setSuccess(true);
    }

    public function deleteParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return (new ProviderResponse())->setSuccess(true);
    }

    public function updateParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse
    {
        /* @var WhiteSession $session */
        $session = $participantSessionRole->getSession();

        if (!($session instanceof WhiteSession)) {
            return $this->makeUnsupportedProviderSessionResponse($session);
        }

        /*if (
            ($participantSessionRole->getRole() === ProviderParticipantSessionRole::ROLE_REMOVE && ($session->getLicence() == $participantSessionRole->getParticipant()->getEmail())) ||
            ($participantSessionRole->getRole() === ProviderParticipantSessionRole::ROLE_ANIMATOR)
        ) {
            $participants = [];
            foreach ($session->getParticipantRoles() as $key => $ppsr) {
                if ($ppsr->getRole() !== ProviderParticipantSessionRole::ROLE_REMOVE) {
                    $participants[] = [
                        'user_id' => $ppsr->getParticipant()->getUser()->getId(),
                        'role' => $ppsr->getRole(),
                        'situationMixte' => '1',
                    ];
                }
            }
            $session->setCandidates($participants);

            return $this->changeSessionForNewLicence($session);
        } else {*/
        return $this->updateSession($session);
        //}
    }

    /*public function getMeeting(ProviderSession $providerSession): ProviderResponse
    {
        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            WhiteConnector::ACTION_GET_MEETING,
            $providerSession
        );
    }*/

    public function redirectParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse
    {
        /* @var WhiteSession $providerSession */
        $providerSession = $participantSessionRole->getSession();

        if (!($providerSession instanceof WhiteSession)) {
            return $this->makeUnsupportedProviderSessionResponse($providerSession);
        }

        /*$response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            WhiteConnector::ACTION_GET_MEETING,
            $providerSession
        );*/

        //if ($response->isSuccess()) {
        $providerResponse = new ProviderResponse();
        $providerResponse
                    ->setData($providerSession->getJoinSessionWebUrl())
                    ->setSuccess(true);

        return $providerResponse;
        /*} else {
            return $response;
        }*/
    }

    public function updateSession(ProviderSession $providerSession): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($providerSession, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            WhiteConnector::ACTION_SESSION_UPDATE,
            $providerSession
        );
    }

    public function invite(WhiteSession $whiteSession, Collection|WhiteParticipant $participants): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($whiteSession, groups: ['update']))) {
            throw new ProviderServiceValidationException($list);
        }

        if ($participants instanceof WhiteParticipant) {
            $participants = new ArrayCollection([$participants]);
        }

        foreach ($participants as $participant) {
            if ($this->entityManager->getRepository(ProviderParticipantSessionRole::class)->findOneBy(['session' => $whiteSession, 'participant' => $participant])) {
                continue;
            }

            if (0 !== count($list = $this->validator->validate($participant, groups: ['create']))) {
                throw new ProviderServiceValidationException($list);
            }

            $role = new ProviderParticipantSessionRole();
            $role->setRole(WhiteParticipant::ROLE_ATTENDEE)
                ->setParticipant($participant)
                ->setSession($whiteSession);

            $whiteSession->addParticipantRole($role);
            $participant->addSessionRoles($role);

            $this->entityManager->persist($role);
        }

        $flagOrigin = 'SessionRoleMSTeams_'.$this->asyncTaskService->randomFlag(20);
        $task = $this->asyncTaskService->publish(SyncSessionRoleToMicrosoftTeamsSessionTask::class, [
            'session_id' => $whiteSession->getId(),
        ], $flagOrigin);

        return (new ProviderResponse())
            ->setSuccess(true)
            ->setData([
                'participants' => $participants,
                'async_task' => $task,
            ]);
    }

    /*public function getToken(WhiteConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            WhiteConnector::ACTION_GET_TOKEN
        );
    }*/

    /*public function getUser(WhiteConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            WhiteConnector::ACTION_GET_USER
        );
    }*/

    public function makeUnsupportedProviderSessionResponse(ProviderSession $providerSession): ProviderResponse
    {
        return (new ProviderResponse())
            ->setSuccess(false)
            ->setThrown(new InvalidArgumentException(sprintf('%s is not supported by %s.', get_class($providerSession), self::class)));
    }

    /*public function checkLicence(ProviderSession $providerSession): void
    {
        // TODO: Implement checkLicence() method.
    }

    public function getAvailableLicence(
        AbstractProviderConfigurationHolder $configurationHolder,
        \DateTime $dateStart,
        \DateTime $dateEnd,
        ?int $excludeSessionId = null
    ): array {
        // TODO: Implement getAvailableLicence() method.
        return [];
    }*/
}
