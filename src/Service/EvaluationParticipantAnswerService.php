<?php

namespace App\Service;

use App\Entity\Evaluation;
use App\Entity\EvaluationParticipantAnswer;
use App\Entity\EvaluationProviderSession;
use App\Entity\EvaluationSessionParticipantAnswer;
use App\Entity\ProviderParticipantSessionRole;
use App\Repository\EvaluationParticipantAnswerRepository;
use App\Repository\EvaluationSessionParticipantAnswerRepository;
use Doctrine\ORM\EntityManagerInterface;

class EvaluationParticipantAnswerService
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private EvaluationParticipantAnswerRepository $evaluationParticipantAnswerRepository,
        private EvaluationSessionParticipantAnswerRepository $evaluationSessionParticipantAnswerRepository
    ) {
    }

    public function saveAnswer(Evaluation $evaluation, ProviderParticipantSessionRole $providerParticipantSessionRole, array $answer): void
    {
        $participantAnswer = $this->evaluationParticipantAnswerRepository
            ->findOneBy(['evaluation' => $evaluation, 'providerParticipantSessionRole' => $providerParticipantSessionRole]);

        if (empty($participantAnswer)) {
            $participantAnswer = new EvaluationParticipantAnswer($evaluation, $providerParticipantSessionRole);
            $this->entityManager->persist($participantAnswer);
        }

        $participantAnswer->setAnswer($answer);
    }

    public function saveAnswerEvaluationSession(EvaluationProviderSession $evaluationProviderSession, ProviderParticipantSessionRole $providerParticipantSessionRole, array $answer): void
    {
        $participantAnswer = $this->evaluationSessionParticipantAnswerRepository
            ->findOneBy(['evaluationProviderSession' => $evaluationProviderSession, 'providerParticipantSessionRole' => $providerParticipantSessionRole]);

        if (empty($participantAnswer)) {
            $participantAnswer = new EvaluationSessionParticipantAnswer($evaluationProviderSession, $providerParticipantSessionRole);
            $this->entityManager->persist($participantAnswer);
        }

        $participantAnswer->setAnswer($answer);
    }
}
