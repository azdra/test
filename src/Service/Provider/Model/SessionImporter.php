<?php

namespace App\Service\Provider\Model;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\ProviderConfigurationHolderInterface;

interface SessionImporter
{
    public function extractData(array $values, Client $client, AbstractProviderConfigurationHolder $configurationHolder): array;

    public function validateData(array $data, ProviderConfigurationHolderInterface $configurationHolder): void;

    public function checkWarnings(array &$data): void;
}
