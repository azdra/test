<?php

namespace App\Service\Provider\Model;

use App\Entity\Client\Client;
use App\Exception\InvalidTemplateUploadedForImportException;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

interface ImportDataReader
{
    public const SESSION_CATEG_VIRTUAL_CLASSROOM = 1;
    public const SESSION_CATEG_BLENDED_TRAINING = 2;
    public const SESSION_CATEG_FACE_TO_FACE = 3;
    public const SESSION_CATEG_MEETING = 4;
    public const SESSION_CATEG_WEBINAR = 5;

    public function getSheet(Spreadsheet $spreadsheet): Worksheet;

    /**
     * @throws InvalidTemplateUploadedForImportException
     */
    public function validateTemplateLoadedIsValid(Worksheet $sheet): bool;

    public function readData(Worksheet $sheet, Client $client): array;
}
