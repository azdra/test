<?php

namespace App\Service\Provider\Model;

use App\Entity\ProviderSession;

interface SessionPresenceReportSynchronize
{
    public function synchronizeReportPresence(ProviderSession $providerSession): array;
}
