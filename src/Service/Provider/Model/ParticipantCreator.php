<?php

namespace App\Service\Provider\Model;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Entity\User;

interface ParticipantCreator
{
    public function createProviderParticipant(AbstractProviderConfigurationHolder $configurationHolder, User $user): ProviderParticipant;
}
