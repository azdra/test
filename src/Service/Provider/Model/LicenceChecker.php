<?php

namespace App\Service\Provider\Model;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\ProviderSession;
use App\Exception\UnknownProviderConfigurationHolderException;
use App\Service\MicrosoftTeamsEventService;
use App\Service\MicrosoftTeamsService;
use App\Service\Provider\Adobe\AdobeLicenceChecker;
use App\Service\Provider\Adobe\AdobeWebinarLicenceChecker;
use App\Service\WebexMeetingService;
use App\Service\WebexRestMeetingService;

class LicenceChecker
{
    public function __construct(
        private AdobeLicenceChecker $adobeLicenceChecker,
        private AdobeWebinarLicenceChecker $adobeWebinarLicenceChecker,
        private WebexMeetingService $webexMeetingService,
        private WebexRestMeetingService $webexRestMeetingService,
        private MicrosoftTeamsService $microsoftTeamsService,
        private MicrosoftTeamsEventService $microsoftTeamsEventService,
    ) {
    }

    public function checkLicence(ProviderSession $session): void
    {
        $this->getLicenceChecker($session->getAbstractProviderConfigurationHolder())
            ->checkLicence($session);
    }

    public function getLicenceAvailable(
        AbstractProviderConfigurationHolder $configurationHolder,
        \DateTime $dateStart,
        \DateTime $dateEnd,
        int $excludeSessionId = null
    ): array {
        return $this->getLicenceChecker($configurationHolder)
            ->getAvailableLicence(
                $configurationHolder,
                $dateStart,
                $dateEnd,
                $excludeSessionId
            );
    }

    private function getLicenceChecker(AbstractProviderConfigurationHolder $configurationHolder): LicenceCheckerInterface
    {
        return match (get_class($configurationHolder)) {
            AdobeConnectConfigurationHolder::class => $this->adobeLicenceChecker,
            AdobeConnectWebinarConfigurationHolder::class => $this->adobeWebinarLicenceChecker,
            WebexConfigurationHolder::class => $this->webexMeetingService,
            WebexRestConfigurationHolder::class => $this->webexRestMeetingService,
            MicrosoftTeamsConfigurationHolder::class => $this->microsoftTeamsService,
            MicrosoftTeamsEventConfigurationHolder::class => $this->microsoftTeamsEventService,
            default => throw new UnknownProviderConfigurationHolderException(get_class($configurationHolder).' is not supported by middleware.')
        };
    }
}
