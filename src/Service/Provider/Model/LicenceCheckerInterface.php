<?php

namespace App\Service\Provider\Model;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ProviderSession;

interface LicenceCheckerInterface
{
    public function checkLicence(ProviderSession $providerSession): void;

    public function getAvailableLicence(
        AbstractProviderConfigurationHolder $configurationHolder,
        \DateTime $dateStart,
        \DateTime $dateEnd,
        ?int $excludeSessionId = null
    ): array;
}
