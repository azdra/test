<?php

namespace App\Service\Provider\Model;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ProviderSession;
use Symfony\Component\Security\Core\User\UserInterface;

interface SessionArrayHandler
{
    public function createSession(AbstractProviderConfigurationHolder $configurationHolder, array &$data, ?UserInterface $user = null): ProviderSession;

    public function updateSession(ProviderSession $session, array &$data, ?UserInterface $user = null): void;
}
