<?php

namespace App\Service\Provider\ClientsImporter\IFCAM;

use App\Entity\ActivityLog;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\CategorySessionType;
use App\Entity\Client\Client;
use App\Entity\Client\ClientServices;
use App\Entity\Client\TrainingActionNatureEnum;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ConfigurationHolder\ProviderConfigurationHolderInterface;
use App\Entity\ProviderSession;
use App\Exception\InvalidImportDataException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ConvocationRepository;
use App\Repository\EvaluationRepository;
use App\Repository\ProviderSessionRepository;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\ActivityLogger;
use App\Service\AdobeConnectWebinarService;
use App\Service\AsyncWorker\Tasks\ActionsFromImportSessionTask;
use App\Service\Provider\Common\AbstractSessionImporter;
use App\Service\ProviderSessionService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Contracts\Translation\TranslatorInterface;

class IfcamAdobeWebinarSessionImporter extends AbstractSessionImporter
{
    public const NAME_INDEX = 1;    //A
    public const DATE_START_INDEX = 2;    //B
    public const DURATION_INDEX = 3;    //C
    public const CONVOCATION_NAME_INDEX = 4;    //D
    public const SESSION_TYPE_INDEX = 5;    //E
    public const LANG_INDEX = 6;    //F
    public const REFERENCE_INDEX = 7;    //G
    public const ACTION_INDEX = 8;    //H
    //public const URL_INDEX = 9;
    public const ACCESS_TYPES_INDEX = 9;    //I
    public const MODEL_INDEX = 10;    //J
    public const CONVOCATION_SESSION_CONTENT_INDEX = 11;    //K
    public const BUSINESS_INDEX = 12;    //L
    public const TAGS_INDEX = 13;    //M
    public const INFORMATION_INDEX = 14;    //N
    public const EVALUATION_INDEX = 15;    //O
    public const SESSION_TEST = 16;    //P
    public const OBJECTIVES = 17;    //Q
    public const LOCATION = 18;    //R
    public const REST_LUNCH = 19;    //S
    public const NBR_DAYS = 20;    //T
    public const SUBJECT_IMPORT_CODE = 21;    //U
    public const SUBSCRIPTION_MAX = 22;    //V
    public const ALERT_LIMIT = 23;    //W
    public const IMMEDIATE_SEND = 24;    //X

    public function __construct(
        ProviderSessionService $providerSessionService,
        ProviderSessionRepository $providerSessionRepository,
        ConvocationRepository $convocationRepository,
        EvaluationRepository $evaluationRepository,
        SubjectRepository $subjectRepository,
        TranslatorInterface $translator,
        private AdobeConnectWebinarService $adobeConnectWebinarService,
        ActivityLogger $activityLogger
    ) {
        parent::__construct($providerSessionRepository, $convocationRepository, $translator, $providerSessionService, $evaluationRepository, $subjectRepository, $activityLogger);
    }

    public function readData(array $values): array
    {
        return [
            'title' => trim($values[self::NAME_INDEX] ?? ''),
            'dateStart' => (isset($values[self::DATE_START_INDEX]) ? \DateTime::createFromFormat('d/m/Y H:i', trim($values[2])) : ''),
            'duration' => (isset($values[self::DURATION_INDEX]) ? \DateTime::createFromFormat('H:i', trim($values[3])) : ''),
            'convocation' => trim($values[self::CONVOCATION_NAME_INDEX] ?? ''),
            'category' => trim($values[self::SESSION_TYPE_INDEX] ?? ''),
            'lang' => trim($values[self::LANG_INDEX] ?? ''),
            'reference' => trim($values[self::REFERENCE_INDEX] ?? ''),
            'action' => trim($values[self::ACTION_INDEX] ?? ''),
            //'url' => trim($values[self::URL_INDEX] ?? ''),
            'accessType' => (int) trim($values[self::ACCESS_TYPES_INDEX] ?? ''),
            'businessNumber' => trim($values[self::BUSINESS_INDEX] ?? ''),
            'information' => trim($values[self::INFORMATION_INDEX] ?? ''),
            'tags' => trim(trim($values[self::TAGS_INDEX] ?? ''), ';'),
            'personalizedContent' => trim($values[self::CONVOCATION_SESSION_CONTENT_INDEX] ?? ''),
            'model' => trim($values[self::MODEL_INDEX] ?? ''),
            'evaluation' => trim($values[self::EVALUATION_INDEX] ?? ''),
            'sessionTest' => boolval($values[self::SESSION_TEST] ?? 0),
            'objectives' => trim(trim($values[self::OBJECTIVES] ?? ''), ';'),
            'location' => trim($values[self::LOCATION] ?? ''),
            'restlunch' => intval($values[self::REST_LUNCH] ?? 0),
            'nbrdays' => intval($values[self::NBR_DAYS] ?? 0),
            'subjectImportCode' => trim($values[self::SUBJECT_IMPORT_CODE] ?? ''),
            'subscriptionMax' => intval($values[self::SUBSCRIPTION_MAX] ?? 0),
            'alertLimit' => intval($values[self::ALERT_LIMIT] ?? 0),
            'immediateSend' => intval($values[self::IMMEDIATE_SEND] ?? 0),
            'warnings' => [],
        ];
    }

    public function fulfillData(array &$data, Client $client, AbstractProviderConfigurationHolder|AdobeConnectWebinarConfigurationHolder $configurationHolder): void
    {
        $data['providerSession'] = !empty($data['reference']) ? $this->providerSessionRepository->findOneBy(['ref' => $data['reference'], 'abstractProviderConfigurationHolder' => $configurationHolder]) : null;
        if (empty($data['convocation']) && $configurationHolder->getDefaultConvocation() != null) {
            $data['convocationObj'] = $this->convocationRepository->find(id: $configurationHolder->getDefaultConvocation());
        } else {
            $data['convocationObj'] = $this->convocationRepository->findConvocation(client: $client, convocationName: $data['convocation'], configurationHolderType: $configurationHolder::getProvider());
        }
        $data['category'] = empty($data['category']) ? CategorySessionType::CATEGORY_SESSION_FORMATION : intval($data['category']);
        $data['action'] = empty($data['action']) ? ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE : (int) $data['action'];
        $data['reference'] = empty($data['reference']) ? ProviderSessionService::generateSessionReference($client->getName()) : $data['reference'];
        $data['nature'] = !empty($data['nature']) ? intval($data['nature']) : null;
        $data['subject'] = !empty($data['subjectImportCode']) ? $this->subjectRepository->findOneBy(['importCode' => $data['subjectImportCode']]) : null;
        $data['subscriptionMax'] = empty($data['subscriptionMax']) ? 0 : intval($data['subscriptionMax']);
        $data['alertLimit'] = intval($data['alertLimit']);
        $data['immediateSend'] = intval($data['immediateSend']);

        if (!empty($data['dateStart']) && !empty($data['duration'])) {
            $data['dateEnd'] = clone $data['dateStart'];
            $data['dateEnd']->modify('+ '.$data['duration']->format('H').' hours')->modify('+ '.$data['duration']->format('i').' min');
        }

        $data['tags'] = empty($data['tags']) ? null : explode(';', $data['tags']);
        $data['objectives'] = empty($data['objectives']) ? null : explode(';', $data['objectives']);
        $data['automaticSendingTrainingCertificates'] = $client->getEmailOptions()->isSendAnAttestationAutomatically();

        if ($data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE && empty($data['providerSession']) && !empty($data['dateStart'])) {
            $dates = $this->providerSessionService->getConvocationAndRemindersDate($client->getEmailScheduling(), $data['dateStart']);
            if ($data['immediateSend'] == 1) {
                $data['notificationDate'] = new \DateTimeImmutable();
                $data['lastConvocationSent'] = new \DateTimeImmutable();
            } else {
                $data['notificationDate'] = $dates['convocation'];
            }

            $data['dateOfSendingReminders'] = $dates['reminders'];
        }

        $data['logAction'] = match ($data['action']) {
            ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE => empty($data['providerSession']) ? ActivityLog::ACTION_SESSION_CREATION : ActivityLog::ACTION_SESSION_UPDATE,
            ActionsFromImportSessionTask::ACTION_SESSION_DELETE => ActivityLog::ACTION_SESSION_DELETE,
            ActionsFromImportSessionTask::ACTION_SESSION_CANCEL => ActivityLog::ACTION_SESSION_CANCEL,
            default => ActivityLog::ACTION_IMPORT_SESSIONS
        };

        if (!empty($data['evaluation'])) {
            $evaluation = $this->evaluationRepository->findOneBy([
                                                                     'title' => $data['evaluation'],
                                                                     'client' => $client,
                                                                 ]);

            $data['evaluationsObj'] = ($evaluation !== null ? new ArrayCollection([$evaluation]) : -1);
        }

        $services = $client->getServices();
        $data['assistanceType'] = $services->getAssistanceType() === ClientServices::ENUM_FULL ? ProviderSession::ENUM_LAUNCH : ProviderSession::ENUM_NO;
        $data['supportType'] = $services->getSupportType() === ClientServices::ENUM_SUPPORT_FULL ? ProviderSession::ENUM_SUPPORT_TOTAL : ProviderSession::ENUM_NO;
        $data['standardHoursActive'] = $services->getStandardHours()->isActive();
        $data['outsideStandardHoursActive'] = $services->getOutsideStandardHours()->isActive();

        $data['accessType'] = match ($data['accessType']) {
            ActionsFromImportSessionTask::ACCESS_USER_ONLY => AdobeConnectWebinarSCO::PERMISSION_VERB_DENIED,
            ActionsFromImportSessionTask::ACCESS_VISITOR => AdobeConnectWebinarSCO::PERMISSION_VERB_REMOVE,
            ActionsFromImportSessionTask::ACCESS_ALL => AdobeConnectWebinarSCO::PERMISSION_VERB_VIEW_HIDDEN,
            default => $configurationHolder->getDefaultConnexionType()
        };

        $data['template'] = $this->getMeetingTemplate(empty($data['model']) ? '' : $data['model'], $configurationHolder, $configurationHolder->getDefaultRoom());
        $data['defaultModelId'] = array_key_exists('uniq_identifier', $data['template']) ? $data['template']['uniq_identifier'] : null;
    }

    private function getMeetingTemplate(string $model, AdobeConnectWebinarConfigurationHolder $configurationHolder, ?int $defaultModel): array
    {
        $response = $this->adobeConnectWebinarService->getSharedMeetingsTemplate($configurationHolder);
        /** @var array $templates */
        $templates = $response->isSuccess() ? $response->getData() : [];

        $availableTemplates = [];

        if (!empty($model)) {
            $availableTemplates = array_filter($templates, function (array $template) use ($model) {
                return $template['name'] === $model;
            });
        }

        if (empty($availableTemplates) && !empty($defaultModel) && array_key_exists($defaultModel, $templates)) {
            return $templates[$defaultModel];
        }

        return empty($availableTemplates) ? [] : array_values($availableTemplates)[0];
    }

    public function validateData(array $data, ProviderConfigurationHolderInterface $configurationHolder): void
    {
        if (empty($data['title'])) {
            throw new InvalidImportDataException('The session\'s name is empty!', [], $this->translator->trans('The session\'s name is empty!'));
        }

        if (empty($data['dateStart'])) {
            throw new InvalidImportDataException('The session date "%title%" is empty!', ['%title%' => $data['title']], $this->translator->trans('The session date "%title%" is empty!', ['%title%' => $data['title']]));
        }

        if (empty($data['duration'])) {
            throw new InvalidImportDataException('The duration for the "%title%" session is not correct!', ['%title%' => $data['title']], $this->translator->trans('The duration for the "%title%" session is not correct!', ['%title%' => $data['title']]));
        }

        if (empty($data['convocation']) && $configurationHolder->getDefaultConvocation() == null) {
            throw new InvalidImportDataException('The "%title%" session convocation is empty!', ['%title%' => $data['title']], $this->translator->trans('The "%title%" session convocation is empty!', ['%title%' => $data['title']]));
        }

        if (empty($data['convocationObj'])) {
            throw new InvalidImportDataException('The convocation does not exist!', [], $this->translator->trans('The convocation does not exist!', []));
        }

        if (!is_null($data['category']) && is_null(CategorySessionType::tryFrom($data['category']))) {
            throw new InvalidImportDataException('The "%title%" session category is not correct!', ['%title%' => $data['title']], $this->translator->trans('The "%title%" session category is not correct!', ['%title%' => $data['title']]));
        }

        if (!in_array($data['action'], [ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE, ActionsFromImportSessionTask::ACTION_SESSION_DELETE, ActionsFromImportSessionTask::ACTION_SESSION_CANCEL])) {
            throw new InvalidImportDataException('The requested action "%action%" for the "%title%" session is not correct!', ['%title%' => $data['title'], '%action%' => $data['action']], $this->translator->trans('The requested action "%action%" for the "%title%" session is not correct!', ['%title%' => $data['title'], '%action%' => $data['action']]));
        }

        if ($data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_DELETE && $data['providerSession'] === null) {
            throw new InvalidImportDataException('You cannot delete a session "%title%" that does not exist!', ['%title%' => $data['title']], $this->translator->trans('You cannot delete a session "%title%" that does not exist!', ['%title%' => $data['title']]));
        }

        if ($data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_CANCEL && $data['providerSession'] === null) {
            throw new InvalidImportDataException('You cannot cancel a session "%title%" that does not exist!', ['%title%' => $data['title']], $this->translator->trans('You cannot cancel a session "%title%" that does not exist!', ['%title%' => $data['title']]));
        }

        if (!is_null($data['nature']) && is_null(TrainingActionNatureEnum::tryFrom($data['nature']))) {
            throw new InvalidImportDataException('The nature of the session "%title%" is not correct!', ['%title%' => $data['title']], $this->translator->trans('The nature of the session "%title%" is not correct!', ['%title%' => $data['title']]));
        }

        if (intval($data['restlunch']) > 180) {
            throw new InvalidImportDataException('the lunch break of the session must be less than or equal to 180 minutes', ['%session%' => $data['title']], $this->translator->trans('the lunch break of the session must be less than or equal to 180 minutes', ['%session%' => $data['title']]));
        }

        if (intval($data['nbrdays']) > 5) {
            throw new InvalidImportDataException('The days number of session "%session%" must be less than or equal to 5 days!', ['%session%' => $data['title']], $this->translator->trans('The days number of session "%session%" must be less than or equal to 5 days!', ['%session%' => $data['title']]));
        }

        if (!$configurationHolder->getClient()->categoryAvailable(CategorySessionType::from($data['category']))) {
            throw new InvalidImportDataException('The "%title%" session category is not available in your account!', ['%title%' => $data['title']], $this->translator->trans('The "%title%" session category is not available in your account!', ['%title%' => $data['title']]));
        }

        if (isset($data['evaluationsObj']) && $data['evaluationsObj'] === -1) {
            throw new InvalidImportDataException('The evaluation form "%evaluation%" is not available in your account!', ['%evaluation%' => $data['evaluation']], $this->translator->trans('The evaluation form "%evaluation%" is not available in your account!', ['%evaluation%' => $data['evaluation']]));
        }

        if (empty($data['subject']) && !empty($data['subjectImportCode'])) {
            throw new InvalidImportDataException('The subject is not found', [], $this->translator->trans('The subject is not found'));
        }

        /*if (!empty($data['url']) && preg_match('/[^A-Za-z0-9\-]/', $data['url'])) {
            throw new InvalidImportDataException('The url given for the session "%session%" is not valid!', ['%title%' => $data['title']], $this->translator->trans('The url given for the session "%session%" is not valid!', ['%title%' => $data['title']]));
        }

        if (!empty($data['url']) && $data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE && $data['providerSession'] !== null) {
            throw new InvalidImportDataException('You do not need to enter a url when you update the "%title%" session!', ['%title%' => $data['title']], $this->translator->trans('You do not need to enter a url when you update the "%title%" session!', ['%title%' => $data['title']]));
        }*/

        if (empty($data['accessType'])) {
            throw new InvalidImportDataException('The type of access entered for the "%session%" session is not valid!', ['%title%' => $data['title']], $this->translator->trans('The type of access entered for the "%session%" session is not valid!', ['%title%' => $data['title']]));
        }
    }

    public function checkWarnings(array &$data): void
    {
        parent::checkWarnings($data);
        if (empty($data['model']) && $data['action'] == ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE) {
            if (empty($data['providerSession'])) {
                $this->activityLogger->addActivityLog(new ActivityLogModel(
                    $data['logAction'],
                    ActivityLog::SEVERITY_WARNING,
                    ['message' => ['key' => 'The default room template was applied to the session (%ref%)', 'params' => ['%ref%' => $data['reference']]]]
                ));
                $data['warnings'][] = ['The default room template was applied to the session (%ref%)', ['%ref%' => $data['reference']]];
            }
        }
    }
}
