<?php

namespace App\Service\Provider\ClientsImporter\IFCAM;

use App\Entity\CategorySessionType;
use App\Entity\Client\Client;
use App\Exception\InvalidTemplateUploadedForImportException;
use App\Service\Provider\Model\ImportDataReader;
use App\Service\Utils\SpreadsheetUtils;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class IfcamMicrosoftTeamsSessionImportDataReader implements ImportDataReader
{
    public const HIGHEST_COLUMN = 'W';

    public function getSheet(Spreadsheet $spreadsheet): Worksheet
    {
        return $spreadsheet->getSheet(2);
    }

    /**
     * {@inheritDoc}
     */
    public function validateTemplateLoadedIsValid(Worksheet $sheet): bool
    {
        if (
            $sheet->getCell('A1')->getValue() !== 'Titre de la session' ||
            $sheet->getCell('B1')->getValue() !== 'Date et horaire de la session' ||
            $sheet->getCell('C1')->getValue() !== 'Durée de la session' ||
            $sheet->getCell('D1')->getValue() !== 'Modèle de convocation' ||
            $sheet->getCell('E1')->getValue() !== 'Licence organisateur' ||
            $sheet->getCell('F1')->getValue() !== 'Catégorie de session' ||
            $sheet->getCell('G1')->getValue() !== 'Langue' ||
            $sheet->getCell('H1')->getValue() !== 'Référence session' ||
            $sheet->getCell('I1')->getValue() !== 'Action' ||
            $sheet->getCell('J1')->getValue() !== 'Contenu personnalisé' ||
            $sheet->getCell('K1')->getValue() !== 'Affaire' ||
            $sheet->getCell('L1')->getValue() !== 'Tags' ||
            $sheet->getCell('M1')->getValue() !== 'Informations' ||
            $sheet->getCell('N1')->getValue() !== 'Formulaire d’évaluation' ||
            $sheet->getCell('O1')->getValue() !== 'Session de test' ||
            $sheet->getCell('P1')->getValue() !== 'Objectifs' ||
            $sheet->getCell('Q1')->getValue() !== 'Lieu' ||
            $sheet->getCell('R1')->getValue() !== 'Pause' ||
            $sheet->getCell('S1')->getValue() !== 'Nombre de jours' ||
            $sheet->getCell('T1')->getValue() !== 'Sujet' ||
            $sheet->getCell('U1')->getValue() !== 'Seuil Max' ||
            $sheet->getCell('V1')->getValue() !== 'Seuil Submax' ||
            $sheet->getCell('W1')->getValue() !== 'Envoi immédiat des convocations'
        ) {
            throw new InvalidTemplateUploadedForImportException('The file uploaded for the session import is not a valid session import file.');
        }

        return true;
    }

    public function readData(Worksheet $sheet, Client $client): array
    {
        $rowsData = [];
        $rowNum = 1;

        foreach ($sheet->getRowIterator(5) as $row) {
            ++$rowNum;
            $colNum = 0;

            foreach ($row->getCellIterator('A', self::HIGHEST_COLUMN) as $cell) {
                ++$colNum;
                $value = $cell->getValue();
                $letterColumn = SpreadsheetUtils::numberToLetter($colNum);
                if ($letterColumn == 'B') {
                    if (is_string($value)) {
                        $value2 = (\DateTime::createFromFormat('d/m/Y H:i', $value, new \DateTimeZone($client->getTimezone())))->setTimezone(new \DateTimeZone('UTC'));
                        $value = SpreadsheetUtils::parseExcelDate($value2->format('d/m/Y H:i'), 'd/m/Y H:i', new \DateTimeZone($client->getTimezone()));
                    } else {
                        $value = SpreadsheetUtils::parseExcelDate($value, 'd/m/Y H:i', new \DateTimeZone($client->getTimezone()));
                    }
                } elseif ($letterColumn == 'C') {
                    $value = SpreadsheetUtils::parseExcelDate($value, 'H:i');
                } elseif ($letterColumn == 'F') {
                    if (empty($cell->getValue())) {
                        $value = CategorySessionType::CATEGORY_SESSION_FORMATION;
                    }

                    switch ($cell->getValue()) {
                        case ImportDataReader::SESSION_CATEG_VIRTUAL_CLASSROOM:
                            $value = CategorySessionType::CATEGORY_SESSION_FORMATION;
                            break;
                        case ImportDataReader::SESSION_CATEG_MEETING:
                            $value = CategorySessionType::CATEGORY_SESSION_REUNION;
                            break;
                        case ImportDataReader::SESSION_CATEG_FACE_TO_FACE:
                            $value = CategorySessionType::CATEGORY_SESSION_PRESENTIAL;
                            break;
                        case ImportDataReader::SESSION_CATEG_BLENDED_TRAINING:
                            $value = CategorySessionType::CATEGORY_SESSION_MIXTE;
                            break;
                        case ImportDataReader::SESSION_CATEG_WEBINAR:
                            $value = CategorySessionType::CATEGORY_SESSION_WEBINAR;
                            break;
                    }
                } elseif ($letterColumn == 'G') {
                    if (empty($cell->getValue())) {
                        $value = $client->getDefaultLang();
                    } else {
                        if ($cell->getValue() == 'FR' || $cell->getValue() == 'EN') {
                            $value = strtolower($cell->getValue());
                        } else {
                            $value = $client->getDefaultLang();
                        }
                    }
                }
                $rowsData[$rowNum][$colNum] = $value;
            }
            $rowsData[$rowNum][$colNum + 1] = $client->getId();
            $rowsData[$rowNum]['numlinefile'] = $rowNum + 3;
        }

        return $rowsData;
    }
}
