<?php

namespace App\Service\Provider\ClientsImporter\IFCAM;

use App\Entity\ActivityLog;
use App\Entity\CategorySessionType;
use App\Entity\Client\Client;
use App\Entity\Client\ClientServices;
use App\Entity\Client\TrainingActionNatureEnum;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\ProviderConfigurationHolderInterface;
use App\Entity\ProviderSession;
use App\Exception\InvalidImportDataException;
use App\Repository\ConvocationRepository;
use App\Repository\EvaluationRepository;
use App\Repository\ProviderSessionRepository;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\ActivityLogger;
use App\Service\AsyncWorker\Tasks\ActionsFromImportSessionTask;
use App\Service\Provider\Common\AbstractSessionImporter;
use App\Service\ProviderSessionService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Contracts\Translation\TranslatorInterface;

class IfcamWebexSessionImporter extends AbstractSessionImporter
{
    public const NAME_INDEX = 1;
    public const DATE_START_INDEX = 2;
    public const DURATION_INDEX = 3;
    public const CONVOCATION_NAME_INDEX = 4;
    public const LICENCE_INDEX = 5;
    public const SESSION_TYPE_INDEX = 6;
    public const LANG_INDEX = 7;
    public const REFERENCE_INDEX = 8;
    public const ACTION_INDEX = 9;
    public const CONVOCATION_SESSION_CONTENT_INDEX = 10;
    public const BUSINESS_INDEX = 11;
    public const TAGS_INDEX = 12;
    public const INFORMATION_INDEX = 13;
    public const EVALUATION_INDEX = 14;
    public const SESSION_TEST = 15;
    public const OBJECTIF = 16;
    public const LOCATION = 17;
    public const REST_LUNCH = 18;
    public const NBR_DAYS = 19;
    public const SUBJECT_IMPORT_CODE = 20;
    public const SUBSCRIPTION_MAX = 21;
    public const ALERT_LIMIT = 22;
    public const IMMEDIATE_SEND = 23;

    public function __construct(
        ProviderSessionService $providerSessionService,
        ProviderSessionRepository $providerSessionRepository,
        ConvocationRepository $convocationRepository,
        EvaluationRepository $evaluationRepository,
        SubjectRepository $subjectRepository,
        TranslatorInterface $translator,
        ActivityLogger $activityLogger
    ) {
        parent::__construct($providerSessionRepository, $convocationRepository, $translator, $providerSessionService, $evaluationRepository, $subjectRepository, $activityLogger);
    }

    protected function readData(array $values): array
    {
        return [
            'title' => trim($values[self::NAME_INDEX] ?? ''),
            'dateStart' => (isset($values[self::DATE_START_INDEX]) ? \DateTime::createFromFormat('d/m/Y H:i', trim($values[2])) : null),
            'duration' => (isset($values[self::DURATION_INDEX]) ? \DateTime::createFromFormat('H:i', trim($values[3])) : null),
            'convocation' => trim($values[self::CONVOCATION_NAME_INDEX] ?? ''),
            'category' => trim($values[self::SESSION_TYPE_INDEX] ?? ''),
            'lang' => trim($values[self::LANG_INDEX] ?? ''),
            'reference' => trim($values[self::REFERENCE_INDEX] ?? ''),
            'action' => trim($values[self::ACTION_INDEX] ?? ''),
            'licence' => trim($values[self::LICENCE_INDEX] ?? ''),
            'personalizedContent' => trim($values[self::CONVOCATION_SESSION_CONTENT_INDEX] ?? ''),
            'businessNumber' => trim($values[self::BUSINESS_INDEX] ?? ''),
            'tags' => trim(trim($values[self::TAGS_INDEX] ?? ''), ';'),
            'information' => trim($values[self::INFORMATION_INDEX] ?? ''),
            'evaluation' => trim($values[self::EVALUATION_INDEX] ?? ''),
            'sessionTest' => boolval($values[self::SESSION_TEST] ?? 0),
            'objectives' => trim(trim($values[self::OBJECTIF] ?? ''), ';'),
            'location' => trim($values[self::LOCATION] ?? ''),
            'restlunch' => intval($values[self::REST_LUNCH] ?? 0),
            'nbrdays' => intval($values[self::NBR_DAYS] ?? 0),
            'subjectImportCode' => trim($values[self::SUBJECT_IMPORT_CODE] ?? ''),
            'subscriptionMax' => intval($values[self::SUBSCRIPTION_MAX] ?? 0),
            'alertLimit' => intval($values[self::ALERT_LIMIT] ?? 0),
            'immediateSend' => intval($values[self::IMMEDIATE_SEND] ?? 0),
            'warnings' => [],
        ];
    }

    public function fulfillData(array &$data, Client $client, AbstractProviderConfigurationHolder|WebexConfigurationHolder $configurationHolder): void
    {
        $data['providerSession'] = !empty($data['reference']) ? $this->providerSessionRepository->findOneBy(['ref' => $data['reference'], 'abstractProviderConfigurationHolder' => $configurationHolder]) : null;
        if (empty($data['convocation']) && $configurationHolder->getDefaultConvocation() != null) {
            $data['convocationObj'] = $this->convocationRepository->find(id: $configurationHolder->getDefaultConvocation());
        } else {
            $data['convocationObj'] = $this->convocationRepository->findConvocation(client: $client, convocationName: $data['convocation'], configurationHolderType: $configurationHolder::getProvider());
        }
        $data['category'] = empty($data['category']) ? CategorySessionType::CATEGORY_SESSION_FORMATION : intval($data['category']);
        $data['action'] = empty($data['action']) ? ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE : (int) $data['action'];
        $data['reference'] = empty($data['reference']) ? ProviderSessionService::generateSessionReference($client->getName()) : $data['reference'];
        $data['nature'] = !empty($data['nature']) ? intval($data['nature']) : null;
        $data['subject'] = !empty($data['subjectImportCode']) ? $this->subjectRepository->findOneBy(['importCode' => $data['subjectImportCode']]) : null;
        $data['subscriptionMax'] = empty($data['subscriptionMax']) ? 0 : intval($data['subscriptionMax']);
        $data['alertLimit'] = intval($data['alertLimit']);
        $data['immediateSend'] = intval($data['immediateSend']);

        if (!empty($data['dateStart']) && !empty($data['duration'])) {
            $data['dateEnd'] = clone $data['dateStart'];
            $data['dateEnd']->modify('+ '.$data['duration']->format('H').' hours')->modify('+ '.$data['duration']->format('i').' min');
        }

        $data['tags'] = empty($data['tags']) ? null : explode(';', $data['tags']);
        $data['objectives'] = empty($data['objectives']) ? null : explode(';', $data['objectives']);
        $data['automaticSendingTrainingCertificates'] = $client->getEmailOptions()->isSendAnAttestationAutomatically();

        if ($data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE && empty($data['providerSession']) && !empty($data['dateStart'])) {
            $dates = $this->providerSessionService->getConvocationAndRemindersDate($client->getEmailScheduling(), $data['dateStart']);
            if ($data['immediateSend'] == 1) {
                $data['notificationDate'] = new \DateTimeImmutable();
                $data['lastConvocationSent'] = new \DateTimeImmutable();
            } else {
                $data['notificationDate'] = $dates['convocation'];
            }

            $data['dateOfSendingReminders'] = $dates['reminders'];
        }

        $data['logAction'] = match ($data['action']) {
            ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE => empty($data['providerSession']) ? ActivityLog::ACTION_SESSION_CREATION : ActivityLog::ACTION_SESSION_UPDATE,
            ActionsFromImportSessionTask::ACTION_SESSION_DELETE => ActivityLog::ACTION_SESSION_DELETE,
            ActionsFromImportSessionTask::ACTION_SESSION_CANCEL => ActivityLog::ACTION_SESSION_CANCEL,
            default => ActivityLog::ACTION_IMPORT_SESSIONS
        };

        if (!empty($data['evaluation'])) {
            $evaluation = $this->evaluationRepository->findOneBy([
                                                                     'title' => $data['evaluation'],
                                                                     'client' => $client,
                                                                 ]);

            $data['evaluationsObj'] = ($evaluation !== null ? new ArrayCollection([$evaluation]) : -1);
        }

        $services = $client->getServices();
        $data['assistanceType'] = $services->getAssistanceType() === ClientServices::ENUM_FULL ? ProviderSession::ENUM_LAUNCH : ProviderSession::ENUM_NO;
        $data['supportType'] = $services->getSupportType() === ClientServices::ENUM_SUPPORT_FULL ? ProviderSession::ENUM_SUPPORT_TOTAL : ProviderSession::ENUM_NO;
        $data['standardHoursActive'] = $services->getStandardHours()->isActive();
        $data['outsideStandardHoursActive'] = $services->getOutsideStandardHours()->isActive();
    }

    /**
     * @throws InvalidImportDataException
     */
    public function validateData(array $data, ProviderConfigurationHolderInterface $configurationHolder): void
    {
        if (empty($data['title'])) {
            throw new InvalidImportDataException('The session\'s name is empty!', [], $this->translator->trans('The session\'s name is empty!'));
        }

        if (empty($data['dateStart'])) {
            throw new InvalidImportDataException('The session date "%title%" is empty!', ['%title%' => $data['title']], $this->translator->trans('The session date "%title%" is empty!', ['%title%' => $data['title']]));
        }

        $dateStart = $data['dateStart'];
        $endStart = new \DateTime(Client::TIPPING_DATE_CLIENT_IFCAM);
        $intervalDateStart = $dateStart->diff($endStart);
        if ($intervalDateStart->invert == 1) {
            throw new InvalidImportDataException('A session whose start date is after 12/31/2023 11:59:59 PM cannot be taken into account by this connector which will become obsolete!', [], $this->translator->trans('A session whose start date is after 12/31/2023 11:59:59 PM cannot be taken into account by this connector which will become obsolete!'));
        }

        if (empty($data['duration'])) {
            throw new InvalidImportDataException('The duration for the "%title%" session is not correct!', ['%title%' => $data['title']], $this->translator->trans('The duration for the "%title%" session is not correct!', ['%title%' => $data['title']]));
        }

        if (empty($data['convocation']) && $configurationHolder->getDefaultConvocation() == null) {
            throw new InvalidImportDataException('The "%title%" session convocation is empty!', ['%title%' => $data['title']], $this->translator->trans('The "%title%" session convocation is empty!', ['%title%' => $data['title']]));
        }

        if (empty($data['convocationObj'])) {
            throw new InvalidImportDataException('The convocation does not exist!', [], $this->translator->trans('The convocation does not exist!', []));
        }

        if (!is_null($data['category']) && is_null(CategorySessionType::tryFrom($data['category']))) {
            throw new InvalidImportDataException('The "%title%" session category is not correct!', ['%title%' => $data['title']], $this->translator->trans('The "%title%" session category is not correct!', ['%title%' => $data['title']]));
        }

        if (!in_array($data['action'], [ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE, ActionsFromImportSessionTask::ACTION_SESSION_DELETE, ActionsFromImportSessionTask::ACTION_SESSION_CANCEL])) {
            throw new InvalidImportDataException('The requested action "%action%" for the "%title%" session is not correct!', ['%title%' => $data['title'], '%action%' => $data['action']], $this->translator->trans('The requested action "%action%" for the "%title%" session is not correct!', ['%title%' => $data['title'], '%action%' => $data['action']]));
        }

        if ($data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_DELETE && $data['providerSession'] === null) {
            throw new InvalidImportDataException('You cannot delete a session "%title%" that does not exist!', ['%title%' => $data['title']], $this->translator->trans('You cannot delete a session "%title%" that does not exist!', ['%title%' => $data['title']]));
        }

        if ($data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_CANCEL && $data['providerSession'] === null) {
            throw new InvalidImportDataException('You cannot cancel a session "%title%" that does not exist!', ['%title%' => $data['title']], $this->translator->trans('You cannot cancel a session "%title%" that does not exist!', ['%title%' => $data['title']]));
        }

        if (!is_null($data['nature']) && is_null(TrainingActionNatureEnum::tryFrom($data['nature']))) {
            throw new InvalidImportDataException('The nature of the session "%title%" is not correct!', ['%title%' => $data['title']], $this->translator->trans('The nature of the session "%title%" is not correct!', ['%title%' => $data['title']]));
        }

        if (intval($data['restlunch']) > 180) {
            throw new InvalidImportDataException('the lunch break of the session must be less than or equal to 180 minutes', ['%session%' => $data['title']], $this->translator->trans('the lunch break of the session must be less than or equal to 180 minutes', ['%session%' => $data['title']]));
        }

        if (intval($data['nbrdays']) > 5) {
            throw new InvalidImportDataException('The days number of session "%session%" must be less than or equal to 5 days!', ['%session%' => $data['title']], $this->translator->trans('The days number of session "%session%" must be less than or equal to 5 days!', ['%session%' => $data['title']]));
        }

        if (!$configurationHolder->getClient()->categoryAvailable(CategorySessionType::from($data['category']))) {
            throw new InvalidImportDataException('The "%title%" session category is not available in your account!', ['%title%' => $data['title']], $this->translator->trans('The "%title%" session category is not available in your account!', ['%title%' => $data['title']]));
        }

        if (isset($data['evaluationsObj']) && $data['evaluationsObj'] === -1) {
            throw new InvalidImportDataException('The evaluation form "%evaluation%" is not available in your account!', ['%evaluation%' => $data['evaluation']], $this->translator->trans('The evaluation form "%evaluation%" is not available in your account!', ['%evaluation%' => $data['evaluation']]));
        }

        if (empty($data['subject']) && !empty($data['subjectImportCode'])) {
            throw new InvalidImportDataException('The subject is not found', [], $this->translator->trans('The subject is not found'));
        }
    }
}
