<?php

namespace App\Service\Provider\ClientsImporter\IFCAM;

use App\Entity\Client\Client;
use App\Exception\InvalidTemplateUploadedForImportException;
use App\Service\Provider\Model\ImportDataReader;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class IfcamUserImportDataReader implements ImportDataReader
{
    public const HIGHEST_COLUMN = 'L';

    public function getSheet(Spreadsheet $spreadsheet): Worksheet
    {
        return $spreadsheet->getSheet(0);
    }

    /**
     * {@inheritDoc}
     */
    public function validateTemplateLoadedIsValid(Worksheet $sheet): bool
    {
        if ($sheet->getCell('A1')->getValue() !== 'Prénom' ||
            $sheet->getCell('B1')->getValue() !== 'Nom' ||
            $sheet->getCell('C1')->getValue() !== 'Email' ||
            $sheet->getCell('D1')->getValue() !== 'Référence participant' ||
            $sheet->getCell('E1')->getValue() !== 'Action' ||
            $sheet->getCell('F1')->getValue() !== 'Téléphone' ||
            $sheet->getCell('G1')->getValue() !== 'Entreprise' ||
            $sheet->getCell('H1')->getValue() !== 'Information' ||
            $sheet->getCell('I1')->getValue() !== 'Email Référent' ||
            $sheet->getCell('J1')->getValue() !== 'Référence session' ||
            $sheet->getCell('K1')->getValue() !== 'Rôle dans la session' ||
            $sheet->getCell('L1')->getValue() !== 'Formation mixte'
        ) {
            throw new InvalidTemplateUploadedForImportException('The file uploaded for the user import is not a valid user import file.');
        }

        return true;
    }

    public function readData(Worksheet $sheet, Client $client): array
    {
        $rowsData = [];
        $rowNum = 1;

        foreach ($sheet->getRowIterator(4) as $row) {
            ++$rowNum;

            $colNum = 0;
            foreach ($row->getCellIterator('A', self::HIGHEST_COLUMN) as $cell) {
                ++$colNum;
                $rowsData[$rowNum][$colNum] = $cell->getValue();
            }
            $rowsData[$rowNum][$colNum + 1] = $client->getId();
            $rowsData[$rowNum]['numlinefile'] = $rowNum + 2;
        }

        return $rowsData;
    }
}
