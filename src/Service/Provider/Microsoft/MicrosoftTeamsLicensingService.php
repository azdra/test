<?php

namespace App\Service\Provider\Microsoft;

use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Repository\MicrosoftTeamsRepository;
use App\Repository\UserRepository;
use DateTime;

class MicrosoftTeamsLicensingService
{
    public function __construct(
        private MicrosoftTeamsRepository $microsoftTeamsRepository,
        private UserRepository $userRepository,
        private MicrosoftTeamsParticipantCreator $microsoftTeamsParticipantCreator,
    ) {
    }

    public function getAvailableLicences(
        MicrosoftTeamsConfigurationHolder $configurationHolder,
        DateTime $from, DateTime $to, int $safetyTime, ?int $sessionIdToExclude = null
    ): array {
        $usedLicences = $this->microsoftTeamsRepository->getUsedLicencesOnSlot(
            $configurationHolder,
            $from,
            $to,
            $safetyTime,
            $sessionIdToExclude
        );

        if (!empty($usedLicences)) {
            $usedLicences = array_values(array_unique(array_column($usedLicences, 'licence')));
        }

        return array_diff_key($configurationHolder->getLicences(), array_flip($usedLicences));
    }

    public function getNextAvailableLicence(
        MicrosoftTeamsConfigurationHolder $configurationHolder,
        DateTime $from, DateTime $to, int $safetyTime, ?int $sessionIdToExclude = null
    ): ?string {
        $availableLicense = $this->getAvailableLicences($configurationHolder, $from,
            $to,$safetyTime,
            $sessionIdToExclude);

        $firstLicence = null;
        foreach ($availableLicense as $key => $values) {
            if ($values['shared']) {
                $firstLicence = $key;
                break;
            }
        }

        return $firstLicence;
    }

    public function licensingASession(MicrosoftTeamsSession $session, ?int $sourceSessionId = null): ?string
    {
        /** @var MicrosoftTeamsConfigurationHolder $configurationHolder */
        $configurationHolder = $session->getAbstractProviderConfigurationHolder();

        if ($configurationHolder->isAutomaticLicensing()) {
            $safetyTime = $configurationHolder->getSafetyTime();

            $availableLicences = $this->getAvailableLicences($configurationHolder, $session->getDateStart(), $session->getDateEnd(), $safetyTime, $sourceSessionId);
            if (empty($availableLicences)) {
                throw new ProviderSessionRequestHandlerException('licence', [], false, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');
            }

            $availableLicencesUserId = array_flip(array_map(fn (array $licence) => $licence['user_id'], $availableLicences));

            $firstLicencePrivate = null;
            foreach ($session->getCandidates() as $candidate) {
                if ($candidate['role'] == 'animator') {
                    if (array_key_exists($candidate['user_id'], $availableLicencesUserId)) {
                        $licenceMail = $availableLicencesUserId[$candidate['user_id']];
                        $licenceInfo = $availableLicences[$licenceMail];
                        if ($licenceInfo['shared']) {
                            $session->setLicence($licenceMail);
                            break;
                        } elseif ($firstLicencePrivate === null) {
                            $firstLicencePrivate = $licenceMail;
                        }
                    }
                }
            }

            if (!is_null($firstLicencePrivate) && is_null($session->getLicence())) {
                $session->setLicence($firstLicencePrivate);
            }

            $nextAvailableLicence = $this->getNextAvailableLicence($configurationHolder, $session->getDateStart(), $session->getDateEnd(), $safetyTime, $sourceSessionId);

            if (is_null($nextAvailableLicence) && is_null($session->getLicence())) {
                throw new ProviderSessionRequestHandlerException('licence', [], false, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');
            }

            if (is_null($session->getLicence())) {
                $session->setLicence($nextAvailableLicence);
            }

            if (!$this->isLicencePresentParticipantRole($session)) {
                $this->addParticipantRole($configurationHolder, $session);
            }
        }

        return $session->getLicence();
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     */
    public function addParticipantRole(MicrosoftTeamsConfigurationHolder $configurationHolder, MicrosoftTeamsSession $session): void
    {
        $user = $this->userRepository->findOneBy([
            'email' => $session->getLicence(),
            'client' => $configurationHolder->getClient(),
        ]);

        if (empty($user)) {
            $session->setLicence(null);
            throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');
        }

        $licenceParticipantRole = $session->getLicenceParticipantRole();

        if (is_null($licenceParticipantRole)) {
            $participantSessionRole = $this->createAnimatorParticipantRole($configurationHolder, $session, $user);
            $session->addParticipantRole($participantSessionRole);
        } elseif ($licenceParticipantRole->getParticipant()->getEmail() !== $session->getLicence()) {
            $session->removeParticipantRole($session->getLicenceParticipantRole());
            $participantSessionRole = $this->createAnimatorParticipantRole($configurationHolder, $session, $user);
            $session->addParticipantRole($participantSessionRole);
        }
    }

    private function createAnimatorParticipantRole(MicrosoftTeamsConfigurationHolder $configurationHolder, MicrosoftTeamsSession $session, User $user): ProviderParticipantSessionRole
    {
        $participant = $this->microsoftTeamsParticipantCreator->createProviderParticipant($configurationHolder, $user);

        return (new ProviderParticipantSessionRole())
            ->setSession($session)
            ->setParticipant($participant)
            ->setRole(ProviderParticipantSessionRole::ROLE_ANIMATOR);
    }

    private function isLicencePresentParticipantRole(MicrosoftTeamsSession $session): bool
    {
        foreach ($session->getParticipantRoles() as $ppsr) {
            if ($session->getLicence() === $ppsr->getParticipant()->getUser()->getEmail() && $ppsr->getRole() != ProviderParticipantSessionRole::ROLE_REMOVE) {
                return true;
            }
        }

        return false;
    }
}
