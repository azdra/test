<?php

namespace App\Service\Provider\Microsoft;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsEventParticipant;
use App\Entity\ProviderParticipant;
use App\Entity\User;
use App\Service\Provider\Model\ParticipantCreator;

class MicrosoftTeamsEventParticipantCreator implements ParticipantCreator
{
    public function createProviderParticipant(AbstractProviderConfigurationHolder $configurationHolder, User $user): ProviderParticipant
    {
        return (new MicrosoftTeamsEventParticipant($configurationHolder))
            ->setUser($user);
    }
}
