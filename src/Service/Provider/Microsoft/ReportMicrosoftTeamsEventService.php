<?php

namespace App\Service\Provider\Microsoft;

use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\ProviderSession;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\MiddlewareFailureException;
use App\Model\ProviderResponse;
use App\Service\MicrosoftTeamsEventService;
use App\Service\Provider\Model\SessionPresenceReportSynchronize;
use App\Service\ProviderSessionService;
use Doctrine\ORM\EntityManagerInterface;

class ReportMicrosoftTeamsEventService implements SessionPresenceReportSynchronize
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private MicrosoftTeamsEventService $microsoftTeamsEventService,
        private ProviderSessionService $providerSessionService
    ) {
    }

    public function synchronizeReportPresence(ProviderSession $providerSession): array
    {
        if (!($providerSession instanceof MicrosoftTeamsEventSession)) {
            throw new InvalidProviderConfigurationHandler();
        }

        $response = $this->getReportMicrosoftTeamsEventSession($providerSession, true);

        return $response->getData();
    }

    private function getFakeAttendanceReports(): ProviderResponse
    {
        $data = [
            '@odata.context' => "https://graph.microsoft.com/v1.0/metadata#users('24cde4f2-6439-45d4-b4ad-3c3285b1dd92')/onlineMeetings('MSoyNGNkZTRmMi02NDM5LTQ1ZDQtYjRhZC0zYzMyODViMWRkOTIqMCoqMTk6bWVldGluZ19Oems1TmpZNU1XRXRNVGMyWmkwME0yUXhMV0ZqTmpjdE16Y3pNekkzTWpCaE0yRmhAdGhyZWFkLnYy')/attendanceReports",
            'value' => [
                0 => [
                    'id' => '646dab2c-6f86-4ddf-9883-8dae7d175b8f1',
                    'totalParticipantCount' => 4,
                    'meetingStartDateTime' => '2023-05-10T14:00:25.397Z',
                    'meetingEndDateTime' => '2023-05-10T14:42:49.725Z',
                ],
                1 => [
                    'id' => '646dab2c-6f86-4ddf-9883-8dae7d175b8f2',
                    'totalParticipantCount' => 6,
                    'meetingStartDateTime' => '2023-05-10T15:00:25.397Z',
                    'meetingEndDateTime' => '2023-05-10T15:42:49.725Z',
                ],
            ],
        ];
        $providerResponse = (new ProviderResponse())->setSuccess(true)->setData($data);

        return $providerResponse;
    }

    private function getFakeAttendanceReportId(): ProviderResponse
    {
        $data = [
            '@odata.context' => "https://graph.microsoft.com/v1.0/metadata#users('16664f75-11dc-4870-bec6-38c1aaa81431')/onlineMeetings('MSpkYzE3Njc0Yy04MWQ5LTRhZGItYmZ')/attendanceReports('c9b6db1c-d5eb-427d-a5c0-20088d9b22d7')",
            'id' => 'c9b6db1c-d5eb-427d-a5c0-20088d9b22d7',
            'totalParticipantCount' => 4,
            'meetingStartDateTime' => '2021-10-05T04:38:23.945Z',
            'meetingEndDateTime' => '2021-10-05T04:43:49.77Z',
            'attendanceRecords' => [
                0 => [
                    'emailAddress' => 'ingenierie@76tszc.onmicrosoft.com',
                    'totalAttendanceInSeconds' => 1152,
                    'role' => 'Presenter',
                    'identity' => [
                        'id' => 'dc17674c-81d9-4adb-bfb2-8f6a442e4623',
                        'displayName' => 'Frederick Cormier',
                        'tenantId' => null,
                    ],
                    'attendanceIntervals' => [
                        0 => [
                            'joinDateTime' => '2021-03-16T18:59:52.2782182Z',
                            'leaveDateTime' => '2021-03-16T19:06:47.7218491Z',
                            'durationInSeconds' => 415,
                        ],
                        1 => [
                            'joinDateTime' => '2021-03-16T19:09:23.9834702Z',
                            'leaveDateTime' => '2021-03-16T19:16:31.1381195Z',
                            'durationInSeconds' => 427,
                        ],
                        2 => [
                            'joinDateTime' => '2021-03-16T19:20:27.7094382Z',
                            'leaveDateTime' => '2021-03-16T19:25:37.7121956Z',
                            'durationInSeconds' => 310,
                        ],
                    ],
                ],
            ],
        ];
        $providerResponse = (new ProviderResponse())->setSuccess(true)->setData($data);

        return $providerResponse;
    }

    public function getReportMicrosoftTeamsEventSession(MicrosoftTeamsEventSession $microsoftTeamsEventSession, bool $throwOnError = false): ProviderResponse
    {
        $reportAttendees = [];

        $presencesReports = $this->microsoftTeamsEventService->getMeetingAttendanceReports($microsoftTeamsEventSession);
        //*** TEST
        //$presencesReports = $this->getFakeAttendanceReports();
        //***
        if (!$presencesReports->isSuccess() && $throwOnError) {
            throw new MiddlewareFailureException('Error fetching data : '.$presencesReports->getData());
        } else {
            $providerResponse = (new ProviderResponse())->setSuccess(true)->setData($presencesReports->getData());

            foreach ($presencesReports->getData()['value'] as $report) {
                $presences = $this->microsoftTeamsEventService->getMeetingAttendanceReportId($microsoftTeamsEventSession, $report['id']);
                //*** TEST
                //$presences = $this->getFakeAttendanceReportId();
                //***
                if (!$presences->isSuccess() && $throwOnError) {
                    throw new MiddlewareFailureException('Error fetching data : '.$presences->getData());
                } else {
                    $providerResponse = (new ProviderResponse())->setSuccess(true)->setData($presences->getData());

                    foreach ($presences->getData()['attendanceRecords'] as $record) {
                        $emailAddress = $record['emailAddress'];
                        $totalAttendanceInSeconds = $record['totalAttendanceInSeconds'];
                        $reportAttendees[$emailAddress] = [];
                        foreach ($record['attendanceIntervals'] as $slot) {
                            $joinDateTime = new \DateTimeImmutable($slot['joinDateTime']);
                            $leaveDateTime = new \DateTimeImmutable($slot['leaveDateTime']);
                            $reportAttendees[$emailAddress]['totalAttendanceInSeconds'] = $totalAttendanceInSeconds;
                            $reportAttendees[$emailAddress]['totalAttendanceInMinutes'] = $totalAttendanceInSeconds / 60;

                            if (empty($reportAttendees[$emailAddress]['date-from'])) {
                                $reportAttendees[$emailAddress]['date-from'] = $joinDateTime;
                                $reportAttendees[$emailAddress]['date-to'] = $leaveDateTime;
                            } else {
                                $reportAttendees[$emailAddress]['date-from'] = ($reportAttendees[$emailAddress]['date-from'] > $joinDateTime) ? $joinDateTime : $reportAttendees[$emailAddress]['date-from'];
                                $reportAttendees[$emailAddress]['date-to'] = ($reportAttendees[$emailAddress]['date-to'] < $leaveDateTime) ? $leaveDateTime : $reportAttendees[$emailAddress]['date-to'];
                            }
                        }
                    }
                }
            }
        }

        foreach ($reportAttendees as $attendee => $values) {
            if ($values['totalAttendanceInMinutes'] >= $this->providerSessionService->getDurationMinimumForPresence($microsoftTeamsEventSession)) {
                foreach ($microsoftTeamsEventSession->getParticipantRoles() as $ppsr) {
                    if (strtolower($ppsr->getParticipant()->getEmail()) == strtolower($attendee)) {
                        $ppsr
                            ->setPresenceDateStart($values['date-from'])
                            ->setPresenceDateEnd($values['date-to'])
                            ->setSlotPresenceDuration($values['totalAttendanceInMinutes'])
                            ->setRemotePresenceStatus(1)
                            ->setPresenceStatus(1);
                        $this->entityManager->persist($ppsr);
                    }
                }
            } else {
                foreach ($microsoftTeamsEventSession->getParticipantRoles() as $ppsr) {
                    if (strtolower($ppsr->getParticipant()->getEmail()) == strtolower($attendee)) {
                        $ppsr
                            ->setPresenceDateStart($values['date-from'])
                            ->setPresenceDateEnd($values['date-to'])
                            ->setSlotPresenceDuration($values['totalAttendanceInMinutes'])
                            ->setRemotePresenceStatus(0)
                            ->setPresenceStatus(0);
                        $this->entityManager->persist($ppsr);
                    }
                }
            }
        }

        return $providerResponse;
    }
}
