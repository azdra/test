<?php

namespace App\Service\Provider\Microsoft;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsParticipant;
use App\Entity\ProviderParticipant;
use App\Entity\User;
use App\Service\Provider\Model\ParticipantCreator;

class MicrosoftTeamsParticipantCreator implements ParticipantCreator
{
    public function createProviderParticipant(AbstractProviderConfigurationHolder $configurationHolder, User $user): ProviderParticipant
    {
        return (new MicrosoftTeamsParticipant($configurationHolder))
            ->setUser($user);
    }
}
