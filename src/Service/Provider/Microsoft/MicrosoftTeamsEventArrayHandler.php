<?php

namespace App\Service\Provider\Microsoft;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\UserRepository;
use App\Service\MicrosoftTeamsEventService;
use App\Service\Provider\Common\AbstractSessionArrayHandler;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use Symfony\Component\Security\Core\User\UserInterface;

class MicrosoftTeamsEventArrayHandler extends AbstractSessionArrayHandler
{
    public function __construct(
        private MicrosoftTeamsEventLicensingService $microsoftTeamsEventLicensingService,
        private UserRepository $userRepository,
        private MicrosoftTeamsEventService $microsoftTeamsEventService,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        private ParticipantSessionSubscriber $participantSessionSubscriber
    ) {
    }

    protected function createRealSession(AbstractProviderConfigurationHolder $configurationHolder, array $data): ProviderSession
    {
        return new MicrosoftTeamsEventSession($configurationHolder);
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     */
    public function updateSession(ProviderSession|MicrosoftTeamsEventSession $session, array &$data, ?UserInterface $user = null): void
    {
        if (!array_key_exists('participants', $data)) {
            $data['participants'] = [];
        }
        $session->setCandidates($data['participants']);

        if (!empty($data['licence'])) {
            if ($data['licence'] != $session->getLicence() && $session->getLicence() != null) {
                $resultPresenceLicenceInAnimators = $this->providerParticipantSessionRoleRepository->getParticipantWithSessionAndEmailAndAnimator($session, $data['licence']);
                if (empty($resultPresenceLicenceInAnimators)) {
                    $userLicenceEmail = $data['licence'];
                    if (array_key_exists('userLicence', $data) && !empty($data['userLicence'])) {
                        if ($session instanceof MicrosoftTeamsEventSession) {
                            $userLicenceEmail = $data['userLicence'];
                        }
                    }
                    $userLicence = $this->userRepository->findOneBy(['email' => $userLicenceEmail, 'client' => $session->getClient()]);
                    if (!empty($userLicence)) {
                        $this->participantSessionSubscriber->subscribe($session, $userLicence, ProviderParticipantSessionRole::ROLE_ANIMATOR);
                    }
                }
                $this->microsoftTeamsEventService->changeSessionForNewLicence($session, $data['licence']);
            }
        }

        parent::updateSession($session, $data, $user);
    }
}
