<?php

namespace App\Service\Provider\Microsoft;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\ProviderConfigurationHolderInterface;
use App\Exception\InvalidImportDataException;
use App\Repository\ConvocationRepository;
use App\Repository\EvaluationRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\ActivityLogger;
use App\Service\Provider\Common\AbstractSessionImporter;
use App\Service\ProviderSessionService;
use Symfony\Contracts\Translation\TranslatorInterface;

class MicrosoftTeamsSessionImporter extends AbstractSessionImporter
{
    public const NAME_INDEX = 1;
    public const DATE_START_INDEX = 2;
    public const DURATION_INDEX = 3;
    public const CONVOCATION_NAME_INDEX = 4;
    public const LICENCE_INDEX = 5;
    public const SESSION_TYPE_INDEX = 6;
    public const LANG_INDEX = 7;
    public const REFERENCE_INDEX = 8;
    public const ACTION_INDEX = 9;
    public const CONVOCATION_SESSION_CONTENT_INDEX = 10;
    public const BUSINESS_INDEX = 11;
    public const TAGS_INDEX = 12;
    public const INFORMATION_INDEX = 13;
    public const EVALUATION_INDEX = 14;
    public const SESSION_TEST = 15;
    public const OBJECTIF = 16;
    public const LOCATION = 17;
    public const REST_LUNCH = 18;
    public const NBR_DAYS = 19;
    public const SUBJECT_IMPORT_CODE = 20;
    public const SUBSCRIPTION_MAX = 21;
    public const ALERT_LIMIT = 22;
    public const IMMEDIATE_SEND = 23;

    public function __construct(
        ProviderSessionService $providerSessionService,
        ProviderSessionRepository $providerSessionRepository,
        ConvocationRepository $convocationRepository,
        EvaluationRepository $evaluationRepository,
        SubjectRepository $subjectRepository,
        TranslatorInterface $translator,
        private UserRepository $userRepository,
        ActivityLogger $activityLogger
    ) {
        parent::__construct($providerSessionRepository, $convocationRepository, $translator, $providerSessionService, $evaluationRepository, $subjectRepository, $activityLogger);
    }

    protected function readData(array $values): array
    {
        return [
            'title' => trim($values[self::NAME_INDEX] ?? ''),
            'dateStart' => (isset($values[self::DATE_START_INDEX]) ? \DateTime::createFromFormat('d/m/Y H:i', trim($values[2])) : ''),
            'duration' => (isset($values[self::DURATION_INDEX]) ? \DateTime::createFromFormat('H:i', trim($values[3])) : ''),
            'convocation' => trim($values[self::CONVOCATION_NAME_INDEX] ?? ''),
            'category' => trim($values[self::SESSION_TYPE_INDEX] ?? ''),
            'lang' => trim($values[self::LANG_INDEX] ?? ''),
            'reference' => trim($values[self::REFERENCE_INDEX] ?? ''),
            'action' => trim($values[self::ACTION_INDEX] ?? ''),
            'licence' => trim($values[self::LICENCE_INDEX] ?? ''),
            'personalizedContent' => trim($values[self::CONVOCATION_SESSION_CONTENT_INDEX] ?? ''),
            'businessNumber' => trim($values[self::BUSINESS_INDEX] ?? ''),
            'tags' => trim(trim($values[self::TAGS_INDEX] ?? ''), ';'),
            'information' => trim($values[self::INFORMATION_INDEX] ?? ''),
            'evaluation' => trim($values[self::EVALUATION_INDEX] ?? ''),
            'sessionTest' => boolval($values[self::SESSION_TEST] ?? 0),
            'objectives' => trim(trim($values[self::OBJECTIF] ?? ''), ';'),
            'location' => trim($values[self::LOCATION] ?? ''),
            'restlunch' => intval($values[self::REST_LUNCH] ?? 0),
            'nbrdays' => intval($values[self::NBR_DAYS] ?? 0),
            'subjectImportCode' => trim($values[self::SUBJECT_IMPORT_CODE] ?? ''),
            'subscriptionMax' => intval($values[self::SUBSCRIPTION_MAX] ?? 0),
            'alertLimit' => intval($values[self::ALERT_LIMIT] ?? 0),
            'immediateSend' => intval($values[self::IMMEDIATE_SEND] ?? 0),
            'warnings' => [],
        ];
    }

    public function fulfillData(array &$data, Client $client, AbstractProviderConfigurationHolder|MicrosoftTeamsConfigurationHolder $configurationHolder): void
    {
        parent::fulfillData($data, $client, $configurationHolder);

        if (!$configurationHolder->isAutomaticLicensing() || !empty($data['licence'])) {
            $data['isLicenceValid'] = array_key_exists($data['licence'], $configurationHolder->getLicences());
            $data['userLicence'] = $this->userRepository->findOneBy(['email' => $data['licence'],
                                                                          'client' => $client,
            ]);
        }
    }

    /**
     * @throws InvalidImportDataException
     */
    public function validateData(array $data, ProviderConfigurationHolderInterface $configurationHolder): void
    {
        parent::validateData($data, $configurationHolder);

        if ($configurationHolder instanceof MicrosoftTeamsConfigurationHolder) {
            if (!$configurationHolder->isAutomaticLicensing()) {
                if (empty($data['licence'])) {
                    throw new InvalidImportDataException('No licence given for the session %title% !', ['%title%' => $data['title']], $this->translator->trans('No licence given for the session %title% !', ['%title%' => $data['title']]));
                }

                if (empty($data['isLicenceValid'])) {
                    throw new InvalidImportDataException('The licence %email% for the session %title% is not a valid licence !', ['%title%' => $data['title']], $this->translator->trans('The licence %licence% for the session %title% is not a valid licence !', ['%title%' => $data['title'], '%licence%' => $data['licence']]));
                }

                if (empty($data['userLicence'])) {
                    throw new InvalidImportDataException('The licence %email% for the session %title% is not linked to any user !', ['%title%' => $data['title']], $this->translator->trans('The licence %licence% for the session %title% is not linked to any user !', ['%title%' => $data['title'], '%licence%' => $data['licence']]));
                }
            }
        }

        if (intval($data['restlunch']) > 180) {
            throw new InvalidImportDataException('the lunch break of the session must be less than or equal to 180 minutes', ['%session%' => $data['title']], $this->translator->trans('the lunch break of the session must be less than or equal to 180 minutes', ['%session%' => $data['title']]));
        }

        if (intval($data['nbrdays']) > 5) {
            throw new InvalidImportDataException('The days number of session "%session%" must be less than or equal to 5 days!', ['%session%' => $data['title']], $this->translator->trans('The days number of session "%session%" must be less than or equal to 5 days!', ['%session%' => $data['title']]));
        }
    }
}
