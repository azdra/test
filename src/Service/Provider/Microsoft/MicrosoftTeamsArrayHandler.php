<?php

namespace App\Service\Provider\Microsoft;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\ProviderSession;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Repository\UserRepository;
use App\Service\Provider\Common\AbstractSessionArrayHandler;
use Symfony\Component\Security\Core\User\UserInterface;

class MicrosoftTeamsArrayHandler extends AbstractSessionArrayHandler
{
    public function __construct(
        private MicrosoftTeamsLicensingService $microsoftTeamsLicensingService,
        private UserRepository $userRepository,
    ) {
    }

    protected function createRealSession(AbstractProviderConfigurationHolder $configurationHolder, array $data): ProviderSession
    {
        return new MicrosoftTeamsSession($configurationHolder);
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     */
    public function updateSession(ProviderSession|MicrosoftTeamsSession $session, array &$data, ?UserInterface $user = null): void
    {
        parent::updateSession($session, $data, $user);

        if (!array_key_exists('participants', $data)) {
            $data['participants'] = [];
        }
        $session->setCandidates($data['participants']);

        /** @var MicrosoftTeamsConfigurationHolder $configurationHolder */
        $configurationHolder = $session->getAbstractProviderConfigurationHolder();

        if (!$configurationHolder->isAutomaticLicensing() || !empty($data['licence'])) {
            $session->setLicence($data['licence']);
        } else {
            $session->setCandidates($data['participants']);
        }
    }
}
