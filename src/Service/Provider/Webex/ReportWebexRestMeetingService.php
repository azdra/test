<?php

namespace App\Service\Provider\Webex;

use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\ProviderSession;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\MiddlewareFailureException;
use App\Model\ProviderResponse;
use App\Service\Provider\Model\SessionPresenceReportSynchronize;
use App\Service\ProviderSessionService;
use App\Service\WebexRestMeetingService;
use Doctrine\ORM\EntityManagerInterface;

class ReportWebexRestMeetingService implements SessionPresenceReportSynchronize
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private WebexRestMeetingService $webexRestMeetingService,
        private ProviderSessionService $providerSessionService
    ) {
        $this->entityManager = $entityManager;
        $this->webexRestMeetingService = $webexRestMeetingService;
    }

    public function synchronizeReportPresence(ProviderSession $providerSession): array
    {
        if (!($providerSession instanceof WebexRestMeeting)) {
            throw new InvalidProviderConfigurationHandler();
        }

        $response = $this->getReportWebexRestMeeting($providerSession, true);

        return $response->getData();
    }

    public function getReportWebexRestMeeting(WebexRestMeeting $webexRestMeeting, bool $throwOnError = false): ProviderResponse
    {
        $presences = $this->webexRestMeetingService->reportMeetingAttendance($webexRestMeeting);
        if (!$presences->isSuccess() && $throwOnError) {
            throw new MiddlewareFailureException('Error fetching data : '.$presences->getData());
        }

        if (!empty($presences->getData())) {
            foreach ($presences->getData() as $key => $value) {
                foreach ($webexRestMeeting->getParticipantRoles()->getValues() as $ppsr) {
                    if ($value['email'] == $ppsr->getParticipant()->getUser()->getEmail()) {
                        if ($value['duration'] >= $this->providerSessionService->getDurationMinimumForPresence($webexRestMeeting)) {
                            $ppsr
                                ->setPresenceDateStart($value['presenceDateStart'])
                                ->setPresenceDateEnd($value['presenceDateEnd'])
                                ->setSlotPresenceDuration($value['duration'])
                                ->setRemotePresenceStatus(1)
                                ->setPresenceStatus(1);
                            $this->entityManager->persist($ppsr);
                        } else {
                            $ppsr
                                ->setPresenceDateStart($value['presenceDateStart'])
                                ->setPresenceDateEnd($value['presenceDateEnd'])
                                ->setSlotPresenceDuration($value['duration'])
                                ->setRemotePresenceStatus(1)
                                ->setPresenceStatus(0);
                            $this->entityManager->persist($ppsr);
                        }
                    }
                }
            }
        }

        return $presences;
    }
}
