<?php

namespace App\Service\Provider\Webex;

use App\Entity\Cisco\WebexMeeting;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ProviderSession;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Service\Provider\Common\AbstractSessionArrayHandler;
use Symfony\Component\Security\Core\User\UserInterface;

class WebexMeetingArrayHandler extends AbstractSessionArrayHandler
{
    protected function createRealSession(AbstractProviderConfigurationHolder $configurationHolder, array $data): ProviderSession
    {
        return new WebexMeeting($configurationHolder);
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     */
    public function updateSession(ProviderSession|WebexMeeting $session, array &$data, ?UserInterface $user = null): void
    {
        parent::updateSession($session, $data, $user);

        /** @var WebexConfigurationHolder $configurationHolder */
        $configurationHolder = $session->getAbstractProviderConfigurationHolder();

        if (!$configurationHolder->isAutomaticLicensing()) {
            $session->setLicence($data['licence']);
        }
    }
}
