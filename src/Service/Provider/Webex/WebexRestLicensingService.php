<?php

namespace App\Service\Provider\Webex;

use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\UserRepository;
use App\Repository\WebexRestMeetingRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class WebexRestLicensingService
{
    public function __construct(
        private WebexRestMeetingRepository $webexRestMeetingRepository,
        private UserRepository $userRepository,
        private WebexRestParticipantCreator $webexRestParticipantCreator,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function getAvailableLicences(
        WebexRestConfigurationHolder $configurationHolder,
        DateTime $from, DateTime $to, int $safetyTime, ?int $sessionIdToExclude = null
    ): array {
        $usedLicences = $this->webexRestMeetingRepository->getUsedLicencesOnSlot(
            $configurationHolder,
            $from,
            $to,
            $safetyTime,
            $sessionIdToExclude
        );

        if (!empty($usedLicences)) {
            $usedLicences = array_values(array_unique(array_column($usedLicences, 'licence')));
        }

        $diffLicences = array_diff($configurationHolder->getLicences(), $usedLicences);

        return $diffLicences;
    }

    public function getNextAvailableLicence(
        WebexRestConfigurationHolder $configurationHolder,
        DateTime $from, DateTime $to, int $safetyTime, ?int $sessionIdToExclude = null
    ): ?string {
        $availableLicense = $this->getAvailableLicences($configurationHolder, $from,
            $to,$safetyTime,
            $sessionIdToExclude);
        $firstLicence = reset($availableLicense);

        return $firstLicence !== false
            ? $firstLicence
            : null;
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     */
    public function licensingASession(WebexRestMeeting &$session, ?int $sourceSessionId = null): ?string
    {
        /** @var WebexRestConfigurationHolder $configurationHolder */
        $configurationHolder = $session->getAbstractProviderConfigurationHolder();
        $safetyTime = $configurationHolder->getSafetyTime();
        $oldLicence = $session->getLicence();

        $availableLicences = $this->getAvailableLicences($configurationHolder, $session->getDateStart(), $session->getDateEnd(), $safetyTime, $sourceSessionId);
        if (empty($availableLicences)) {
            throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');
        }

        if (empty($session->getLicence())) {
            $session->setLicence(reset($availableLicences));
        } else {
            //Si la licence indiquée pour la session n'est pas dans les licences disponibles, on la remplace par la première licence disponible
            if (!in_array($session->getLicence(), $availableLicences)) {
                $session->setLicence(reset($availableLicences));
            }
        }

        if (empty($session->getMeetingRestHostEmail())) {  //Mode création
            $session->setMeetingRestHostEmail($session->getLicence());
        } //Dans le cas contraire la gestion est faite du côté de WebexRestMeetingService

        //*************STOP ICI*************
        //throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot.');

        $this->addParticipantRole($configurationHolder, $session);

        if (!empty($oldLicence) && $oldLicence != $session->getLicence()) {
            $ppsrOldLicence = $this->providerParticipantSessionRoleRepository->getLicenceFromSessionAndEmail($session, $oldLicence);
            $this->entityManager->remove($ppsrOldLicence);
        }

        $this->entityManager->persist($session);
        //if (!empty($sourceSessionId)) {
        $this->entityManager->flush();
        // FLUSH
        //}

        return $session->getLicence();
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     */
    public function addParticipantRole(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $session): void
    {
        $user = $this->userRepository->findOneBy([
            'email' => $session->getLicence(),
            'client' => $configurationHolder->getClient(),
        ]);
        if (empty($user)) {
            $session->setLicence(null);
            throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');
        }
        $licenceParticipantRole = $session->getLicenceParticipantRole();
        if (is_null($licenceParticipantRole)) {
            $participantSessionRole = $this->createAnimatorParticipantRole($configurationHolder, $session, $user);
            $session->addParticipantRole($participantSessionRole);
        } elseif ($licenceParticipantRole->getParticipant()->getEmail() !== $session->getLicence()) {
            $session->removeParticipantRole($session->getLicenceParticipantRole());
            $participantSessionRole = $this->createAnimatorParticipantRole($configurationHolder, $session, $user);
            $session->addParticipantRole($participantSessionRole);
        }
    }

    private function createAnimatorParticipantRole(WebexRestConfigurationHolder $configurationHolder, WebexRestMeeting $session, User $user): ProviderParticipantSessionRole
    {
        $participant = $this->webexRestParticipantCreator->createProviderParticipant($configurationHolder, $user);

        return (new ProviderParticipantSessionRole())
            ->setSession($session)
            ->setParticipant($participant)
            ->setRole(ProviderParticipantSessionRole::ROLE_ANIMATOR);
    }
}
