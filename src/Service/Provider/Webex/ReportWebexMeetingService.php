<?php

namespace App\Service\Provider\Webex;

use App\Entity\Cisco\WebexMeeting;
use App\Entity\ProviderSession;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\MiddlewareFailureException;
use App\Model\ProviderResponse;
use App\Service\Provider\Model\SessionPresenceReportSynchronize;
use App\Service\ProviderSessionService;
use App\Service\WebexMeetingService;
use Doctrine\ORM\EntityManagerInterface;

class ReportWebexMeetingService implements SessionPresenceReportSynchronize
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private WebexMeetingService $webexMeetingService,
        private ProviderSessionService $providerSessionService
    ) {
    }

    public function synchronizeReportPresence(ProviderSession $providerSession): array
    {
        if (!($providerSession instanceof WebexMeeting)) {
            throw new InvalidProviderConfigurationHandler();
        }

        $response = $this->getReportWebexMeeting($providerSession, true);

        return $response->getData();
    }

    public function getReportWebexMeeting(WebexMeeting $webexMeeting, bool $throwOnError = false): ProviderResponse
    {
        $presences = $this->webexMeetingService->reportMeetingAttendance($webexMeeting);
        if (!$presences->isSuccess() && $throwOnError) {
            throw new MiddlewareFailureException('Error fetching data : '.$presences->getData());
        }

        if (!empty($presences->getData())) {
            foreach ($presences->getData() as $key => $value) {
                foreach ($webexMeeting->getParticipantRoles()->getValues() as $ppsr) {
                    if ($value['email'] == $ppsr->getParticipant()->getUser()->getEmail()) {
                        $duration = $value['duration'];

                        if ($duration >= $this->providerSessionService->getDurationMinimumForPresence($webexMeeting)) {
                            $ppsr
                                ->setPresenceDateStart($value['presenceDateStart'])
                                ->setPresenceDateEnd($value['presenceDateEnd'])
                                ->setSlotPresenceDuration($duration)
                                ->setRemotePresenceStatus(1)
                                ->setPresenceStatus(1);
                            $this->entityManager->persist($ppsr);
                        } else {
                            unset($presences->getData()[$key]);
                        }
                    }
                }
            }
        }

        return $presences;
    }
}
