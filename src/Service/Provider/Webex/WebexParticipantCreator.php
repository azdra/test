<?php

namespace App\Service\Provider\Webex;

use App\Entity\Cisco\WebexParticipant;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Entity\User;
use App\Service\Provider\Model\ParticipantCreator;

class WebexParticipantCreator implements ParticipantCreator
{
    public function createProviderParticipant(AbstractProviderConfigurationHolder $configurationHolder, User $user): ProviderParticipant
    {
        return (new WebexParticipant($configurationHolder))
            ->setUser($user);
    }
}
