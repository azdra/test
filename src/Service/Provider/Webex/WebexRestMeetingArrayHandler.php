<?php

namespace App\Service\Provider\Webex;

use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ProviderSession;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Service\Provider\Common\AbstractSessionArrayHandler;
use Symfony\Component\Security\Core\User\UserInterface;

class WebexRestMeetingArrayHandler extends AbstractSessionArrayHandler
{
    protected function createRealSession(AbstractProviderConfigurationHolder $configurationHolder, array $data): ProviderSession
    {
        return new WebexRestMeeting($configurationHolder);
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     */
    public function updateSession(ProviderSession|WebexRestMeeting $session, array &$data, ?UserInterface $user = null): void
    {
        parent::updateSession($session, $data, $user);

        /** @var WebexRestConfigurationHolder $configurationHolder */
        $configurationHolder = $session->getAbstractProviderConfigurationHolder();
        //Not automatic licensing attributed >>> manually selected
        if (!$configurationHolder->isAutomaticLicensing()) {
            /*dump($session->getLicence());
            if( empty($session->getLicence()) && !empty($data['licence']) ) { //if nothing attributed licence
                dump('MANU : Licence session is null | Licence data is not null');
                dump('The licence is null');
                $session->setLicence($data['licence']);
            }*/
            //$session->setLicence($data['licence']);
        } else {  //Automatic licensing attributed
            /*if( empty($session->getLicence()) && empty($data['licence']) ) {
                dump('AUTO : Licence session is null | Licence data is null');
            }*/
        }
    }
}
