<?php

namespace App\Service\Provider\Webex;

use App\Entity\Cisco\WebexMeeting;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\User;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\UserRepository;
use App\Repository\WebexMeetingRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class WebexLicensingService
{
    public function __construct(
        private WebexMeetingRepository $webexMeetingRepository,
        private UserRepository $userRepository,
        private WebexParticipantCreator $webexParticipantCreator,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function getAvailableLicences(
        WebexConfigurationHolder $configurationHolder,
        DateTime $from, DateTime $to, int $safetyTime, ?int $sessionIdToExclude = null
    ): array {
        $usedLicences = $this->webexMeetingRepository->getUsedLicencesOnSlot(
            $configurationHolder,
            $from,
            $to,
            $safetyTime,
            $sessionIdToExclude
        );

        if (!empty($usedLicences)) {
            $usedLicences = array_values(array_unique(array_column($usedLicences, 'licence')));
        }

        $diffLicences = array_diff($configurationHolder->getLicences(), $usedLicences);

        return $diffLicences;
    }

    public function getNextAvailableLicence(
        WebexConfigurationHolder $configurationHolder,
        DateTime $from, DateTime $to, int $safetyTime, ?int $sessionIdToExclude = null
    ): ?string {
        $availableLicense = $this->getAvailableLicences($configurationHolder, $from,
            $to,$safetyTime,
            $sessionIdToExclude);
        $firstLicence = reset($availableLicense);

        return $firstLicence !== false
            ? $firstLicence
            : null;
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     */
    public function licensingASession(WebexMeeting &$session, ?int $sourceSessionId = null): ?string
    {
        /** @var WebexConfigurationHolder $configurationHolder */
        $configurationHolder = $session->getAbstractProviderConfigurationHolder();

        if ($configurationHolder->isAutomaticLicensing()) {
            $safetyTime = $configurationHolder->getSafetyTime();

            $nextAvailableLicence = $this->getNextAvailableLicence($configurationHolder, $session->getDateStart(), $session->getDateEnd(), $safetyTime, $sourceSessionId);
            $oldLicence = $session->getLicence();

            if (is_null($nextAvailableLicence)) {
                throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');
            }

            $session->setLicence($nextAvailableLicence);
        }

        $this->addParticipantRole($configurationHolder, $session);

        if (!empty($oldLicence) && $oldLicence != $nextAvailableLicence) {
            $ppsrOldLicence = $this->providerParticipantSessionRoleRepository->getLicenceFromSessionAndEmail($session, $oldLicence);
            $this->entityManager->remove($ppsrOldLicence);
        }

        return $session->getLicence();
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     */
    public function addParticipantRole(WebexConfigurationHolder $configurationHolder, WebexMeeting $session): void
    {
        $user = $this->userRepository->findOneBy([
            'email' => $session->getLicence(),
            'client' => $configurationHolder->getClient(),
        ]);

        if (empty($user)) {
            $session->setLicence(null);
            throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');
        }

        $licenceParticipantRole = $session->getLicenceParticipantRole();

        if (is_null($licenceParticipantRole)) {
            $participantSessionRole = $this->createAnimatorParticipantRole($configurationHolder, $session, $user);
            $session->addParticipantRole($participantSessionRole);
        } elseif ($licenceParticipantRole->getParticipant()->getEmail() !== $session->getLicence()) {
            $session->removeParticipantRole($session->getLicenceParticipantRole());
            $participantSessionRole = $this->createAnimatorParticipantRole($configurationHolder, $session, $user);
            $session->addParticipantRole($participantSessionRole);
        }
    }

    private function createAnimatorParticipantRole(WebexConfigurationHolder $configurationHolder, WebexMeeting $session, User $user): ProviderParticipantSessionRole
    {
        $participant = $this->webexParticipantCreator->createProviderParticipant($configurationHolder, $user);

        return (new ProviderParticipantSessionRole())
            ->setSession($session)
            ->setParticipant($participant)
            ->setRole(ProviderParticipantSessionRole::ROLE_ANIMATOR);
    }
}
