<?php

namespace App\Service\Provider\Adobe;

use App\Entity\ActivityLog;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ConfigurationHolder\ProviderConfigurationHolderInterface;
use App\Exception\InvalidImportDataException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ConvocationRepository;
use App\Repository\EvaluationRepository;
use App\Repository\ProviderSessionRepository;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\ActivityLogger;
use App\Service\AdobeConnectWebinarService;
use App\Service\AsyncWorker\Tasks\ActionsFromImportSessionTask;
use App\Service\Provider\Common\AbstractSessionImporter;
use App\Service\ProviderSessionService;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdobeWebinarSessionImporter extends AbstractSessionImporter
{
    public const NAME_INDEX = 1;    //A
    public const DATE_START_INDEX = 2;    //B
    public const DURATION_INDEX = 3;    //C
    public const CONVOCATION_NAME_INDEX = 4;    //D
    public const SESSION_TYPE_INDEX = 5;    //E
    public const LANG_INDEX = 6;    //F
    public const REFERENCE_INDEX = 7;    //G
    public const ACTION_INDEX = 8;    //H
    //public const URL_INDEX = 9;
    public const ACCESS_TYPES_INDEX = 9;    //I
    public const MODEL_INDEX = 10;    //J
    public const CONVOCATION_SESSION_CONTENT_INDEX = 11;    //K
    public const BUSINESS_INDEX = 12;    //L
    public const TAGS_INDEX = 13;    //M
    public const INFORMATION_INDEX = 14;    //N
    public const EVALUATION_INDEX = 15;    //O
    public const SESSION_TEST = 16;    //P
    public const OBJECTIVES = 17;    //Q
    public const LOCATION = 18;    //R
    public const REST_LUNCH = 19;    //S
    public const NBR_DAYS = 20;    //T
    public const SUBJECT_IMPORT_CODE = 21;    //U
    public const SUBSCRIPTION_MAX = 22;    //V
    public const ALERT_LIMIT = 23;    //W
    public const IMMEDIATE_SEND = 24;    //X

    public function __construct(
        ProviderSessionService $providerSessionService,
        ProviderSessionRepository $providerSessionRepository,
        ConvocationRepository $convocationRepository,
        EvaluationRepository $evaluationRepository,
        SubjectRepository $subjectRepository,
        TranslatorInterface $translator,
        private AdobeConnectWebinarService $adobeConnectWebinarService,
        ActivityLogger $activityLogger
    ) {
        parent::__construct($providerSessionRepository, $convocationRepository, $translator, $providerSessionService, $evaluationRepository, $subjectRepository, $activityLogger);
    }

    public function readData(array $values): array
    {
        return [
            'title' => trim($values[self::NAME_INDEX] ?? ''),
            'dateStart' => (isset($values[self::DATE_START_INDEX]) ? \DateTime::createFromFormat('d/m/Y H:i', trim($values[2])) : ''),
            'duration' => (isset($values[self::DURATION_INDEX]) ? \DateTime::createFromFormat('H:i', trim($values[3])) : ''),
            'convocation' => trim($values[self::CONVOCATION_NAME_INDEX] ?? ''),
            'category' => trim($values[self::SESSION_TYPE_INDEX] ?? ''),
            'lang' => trim($values[self::LANG_INDEX] ?? ''),
            'reference' => trim($values[self::REFERENCE_INDEX] ?? ''),
            'action' => trim($values[self::ACTION_INDEX] ?? ''),
            //'url' => trim($values[self::URL_INDEX] ?? ''),
            'accessType' => (int) trim($values[self::ACCESS_TYPES_INDEX] ?? ''),
            'businessNumber' => trim($values[self::BUSINESS_INDEX] ?? ''),
            'information' => trim($values[self::INFORMATION_INDEX] ?? ''),
            'tags' => trim(trim($values[self::TAGS_INDEX] ?? ''), ';'),
            'personalizedContent' => trim($values[self::CONVOCATION_SESSION_CONTENT_INDEX] ?? ''),
            'model' => trim($values[self::MODEL_INDEX] ?? ''),
            'evaluation' => trim($values[self::EVALUATION_INDEX] ?? ''),
            'sessionTest' => boolval($values[self::SESSION_TEST] ?? 0),
            'objectives' => trim(trim($values[self::OBJECTIVES] ?? ''), ';'),
            'location' => trim($values[self::LOCATION] ?? ''),
            'restlunch' => intval($values[self::REST_LUNCH] ?? 0),
            'nbrdays' => intval($values[self::NBR_DAYS] ?? 0),
            'subjectImportCode' => trim($values[self::SUBJECT_IMPORT_CODE] ?? ''),
            'subscriptionMax' => intval($values[self::SUBSCRIPTION_MAX] ?? 0),
            'alertLimit' => intval($values[self::ALERT_LIMIT] ?? 0),
            'immediateSend' => intval($values[self::IMMEDIATE_SEND] ?? 0),
            'warnings' => [],
        ];
    }

    public function fulfillData(array &$data, Client $client, AbstractProviderConfigurationHolder|AdobeConnectWebinarConfigurationHolder $configurationHolder): void
    {
        parent::fulfillData($data, $client, $configurationHolder);

        $data['accessType'] = match ($data['accessType']) {
            ActionsFromImportSessionTask::ACCESS_USER_ONLY => AdobeConnectWebinarSCO::PERMISSION_VERB_DENIED,
            ActionsFromImportSessionTask::ACCESS_VISITOR => AdobeConnectWebinarSCO::PERMISSION_VERB_REMOVE,
            ActionsFromImportSessionTask::ACCESS_ALL => AdobeConnectWebinarSCO::PERMISSION_VERB_VIEW_HIDDEN,
            default => $configurationHolder->getDefaultConnexionType()
        };

        $data['template'] = $this->getMeetingTemplate(empty($data['model']) ? '' : $data['model'], $configurationHolder, $configurationHolder->getDefaultRoom());
        $data['defaultModelId'] = array_key_exists('uniq_identifier', $data['template']) ? $data['template']['uniq_identifier'] : null;
    }

    private function getMeetingTemplate(string $model, AdobeConnectWebinarConfigurationHolder $configurationHolder, ?int $defaultModel): array
    {
        $response = $this->adobeConnectWebinarService->getSharedMeetingsTemplate($configurationHolder);
        /** @var array $templates */
        $templates = $response->isSuccess() ? $response->getData() : [];

        $availableTemplates = [];

        if (!empty($model)) {
            $availableTemplates = array_filter($templates, function (array $template) use ($model) {
                return $template['name'] === $model;
            });
        }

        if (empty($availableTemplates) && !empty($defaultModel) && array_key_exists($defaultModel, $templates)) {
            return $templates[$defaultModel];
        }

        return empty($availableTemplates) ? [] : array_values($availableTemplates)[0];
    }

    public function validateData(array $data, ProviderConfigurationHolderInterface $configurationHolder): void
    {
        parent::validateData($data, $configurationHolder);

        /*if (!empty($data['url']) && preg_match('/[^A-Za-z0-9\-]/', $data['url'])) {
            throw new InvalidImportDataException('The url given for the session "%session%" is not valid!', ['%title%' => $data['title']], $this->translator->trans('The url given for the session "%session%" is not valid!', ['%title%' => $data['title']]));
        }

        if (!empty($data['url']) && $data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE && $data['providerSession'] !== null) {
            throw new InvalidImportDataException('You do not need to enter a url when you update the "%title%" session!', ['%title%' => $data['title']], $this->translator->trans('You do not need to enter a url when you update the "%title%" session!', ['%title%' => $data['title']]));
        }*/

        if (empty($data['accessType'])) {
            throw new InvalidImportDataException('The type of access entered for the "%session%" session is not valid!', ['%title%' => $data['title']], $this->translator->trans('The type of access entered for the "%session%" session is not valid!', ['%title%' => $data['title']]));
        }
    }

    public function checkWarnings(array &$data): void
    {
        parent::checkWarnings($data);
        if (empty($data['model']) && $data['action'] == ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE) {
            if (empty($data['providerSession'])) {
                $this->activityLogger->addActivityLog(new ActivityLogModel(
                    $data['logAction'],
                    ActivityLog::SEVERITY_WARNING,
                    ['message' => ['key' => 'The default room template was applied to the session (%ref%)', 'params' => ['%ref%' => $data['reference']]]]
                ));
                $data['warnings'][] = ['The default room template was applied to the session (%ref%)', ['%ref%' => $data['reference']]];
            }
        }
    }
}
