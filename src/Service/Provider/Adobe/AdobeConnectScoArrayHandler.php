<?php

namespace App\Service\Provider\Adobe;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ProviderSession;
use App\Service\AdobeConnectService;
use App\Service\Provider\Common\AbstractSessionArrayHandler;
use Symfony\Component\Security\Core\User\UserInterface;

class AdobeConnectScoArrayHandler extends AbstractSessionArrayHandler
{
    public function __construct(private AdobeConnectService $adobeConnectService)
    {
    }

    protected function createRealSession(AbstractProviderConfigurationHolder $configurationHolder, array $data): ProviderSession
    {
        $session = new AdobeConnectSCO($configurationHolder);

        $session->setUrlPath($data['url'])
            ->setType(AdobeConnectSCO::TYPE_MEETING);

        if (array_key_exists('defaultModelId', $data) && $data['defaultModelId'] !== null) {
            $session->setRoomModel($data['defaultModelId']);
        }

        return $session;
    }

    public function updateSession(ProviderSession|AdobeConnectSCO $session, array &$data, ?UserInterface $user = null): void
    {
        parent::updateSession($session, $data, $user);

        $session->setAdmittedSession($data['accessType']);

        $this->checkProviderAudioAvailableOrAssignOne($session);
    }

    private function checkProviderAudioAvailableOrAssignOne(AdobeConnectSCO $session): void
    {
        $convocation = $session->getConvocation();
        $dateBeginAudio = ( new \DateTime($session->getDateStart()->format('Y-m-d H:i:s')) )->modify('-30 minutes');
        $dateEndAudio = ( new \DateTime($session->getDateEnd()->format('Y-m-d H:i:s')) )->modify('+30 minutes');
        if (!empty($convocation)) {
            $currentProviderAudio = $session->getProviderAudio();
            if (!empty($currentProviderAudio) &&
                $currentProviderAudio->getConnectionType() === $convocation->getTypeAudio() &&
                $currentProviderAudio->getPhoneLang() === $convocation->getLanguages() &&
                $this->adobeConnectService->checkProviderAudioAvailabilityForDate($session, $dateBeginAudio, $dateEndAudio)
            ) {
                return;
            }

            $availableAudios = ($convocation->getTypeAudio() != null) ? $this->adobeConnectService->getAvailableAudios($dateBeginAudio, $dateEndAudio, $convocation->getTypeAudio(), $convocation->getLanguages(), $session->getAbstractProviderConfigurationHolder()) : [];
            $session->setProviderAudio(empty($availableAudios) ? null : $availableAudios[0]);
        }
    }
}
