<?php

namespace App\Service\Provider\Adobe;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Exception\LicenceCheckException;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Repository\AdobeConnectSCORepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\UserRepository;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\Provider\Model\LicenceCheckerInterface;
use Doctrine\ORM\EntityManagerInterface;

class AdobeLicenceChecker implements LicenceCheckerInterface
{
    public function __construct(
        private AdobeConnectSCORepository $adobeConnectSCORepository,
        private UserRepository $userRepository,
        private ParticipantSessionSubscriber $participantSessionSubscriber,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        private EntityManagerInterface $entityManager
    ) {
    }

    public function getAvailableLicence(
        AbstractProviderConfigurationHolder $configurationHolder,
        \DateTime $dateStart,
        \DateTime $dateEnd,
        ?int $excludeSessionId = null
    ): array {
        /** @var AdobeConnectConfigurationHolder $configurationHolder */
        if (!$configurationHolder->isAutomaticLicensing()) {
            return [];
        }

        $licences = array_keys(array_filter($configurationHolder->getLicences(), function ($licence) {
            return $licence['shared'] === true;
        }));
        $safetyTime = 30;

        $usedLicence = $this->adobeConnectSCORepository->getUsedLicenceOnSLot(
            $configurationHolder,
            $licences,
            \DateTimeImmutable::createFromMutable($dateStart)->sub(new \DateInterval("PT{$safetyTime}M")),
            \DateTimeImmutable::createFromMutable($dateEnd)->add(new \DateInterval("PT{$safetyTime}M")),
            $excludeSessionId
        );

        $availableLicences = array_values(array_diff($licences, $usedLicence));

        if (count($availableLicences) === 0) {
            throw new LicenceCheckException('You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot', [], 'No licence available for time slot');
        }

        return $availableLicences;
    }

    public function checkLicence(AdobeConnectSCO|ProviderSession $providerSession): void
    {
        /** @var AdobeConnectConfigurationHolder $configurationHolder */
        $configurationHolder = $providerSession->getAbstractProviderConfigurationHolder();
        if ($configurationHolder->isAutomaticLicensing() && $providerSession->getAnimatorsCount() === 0) {
            $availableLicences = $this->getAvailableLicence(
                $configurationHolder,
                $providerSession->getDateStart(),
                $providerSession->getDateEnd(),
                $providerSession->getId()
            );

            $licence = $availableLicences[0];
            $userLicence = $this->userRepository->findOneBy([
                    'email' => $licence,
                    'client' => $providerSession->getClient(),
            ]);

            $this->participantSessionSubscriber->subscribe(
                $providerSession,
                $userLicence,
                ProviderParticipantSessionRole::ROLE_ANIMATOR,
                $providerSession->isSessionPresential() ? ProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL : ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL
            );

            /* @var AdobeConnectSCO $providerSession */
            $providerSession->setLicence($licence);
        }
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     */
    public function licensingASession(AdobeConnectSCO &$session, \DateTime $dateStart, \DateTime $dateEnd): ?string
    {
        /** @var AdobeConnectConfigurationHolder $configurationHolder */
        $configurationHolder = $session->getAbstractProviderConfigurationHolder();
        if (!$configurationHolder->isAutomaticLicensing()) {
            return '';
        }
        $nextAvailableLicence = $this->getAvailableLicence($configurationHolder, $dateStart, $dateEnd, $session->getId());
        $oldLicence = $session->getLicence();

        if ($nextAvailableLicence == []) {
            throw new ProviderSessionRequestHandlerException('licence', [], true, 'You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot');
        }

        if (empty($oldLicence) || !in_array($oldLicence, $nextAvailableLicence)) {
            $session->setLicence($nextAvailableLicence[0]);

            $userLicence = $this->userRepository->findOneBy([
                'email' => $nextAvailableLicence[0],
                'client' => $session->getClient(),
            ]);
            $this->participantSessionSubscriber->subscribe(
                $session,
                $userLicence,
                ProviderParticipantSessionRole::ROLE_ANIMATOR,
                $session->isSessionPresential() ? ProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL : ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL
            );

            if (!empty($oldLicence)) {
                $ppsrOldLicence = $this->providerParticipantSessionRoleRepository->getLicenceFromSessionAndEmail($session, $oldLicence);
                $this->entityManager->remove($ppsrOldLicence);
            }
        }

        return $session->getLicence();
    }
}
