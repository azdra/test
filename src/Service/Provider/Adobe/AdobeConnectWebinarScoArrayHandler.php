<?php

namespace App\Service\Provider\Adobe;

use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ProviderSession;
use App\Service\AdobeConnectWebinarService;
use App\Service\Provider\Common\AbstractSessionArrayHandler;
use Symfony\Component\Security\Core\User\UserInterface;

class AdobeConnectWebinarScoArrayHandler extends AbstractSessionArrayHandler
{
    public function __construct(private AdobeConnectWebinarService $adobeConnectWebinarService)
    {
    }

    protected function createRealSession(AbstractProviderConfigurationHolder $configurationHolder, array $data): ProviderSession
    {
        $session = new AdobeConnectWebinarSCO($configurationHolder);

        //$session->setUrlPath($data['url']);
        $session->setType(AdobeConnectWebinarSCO::TYPE_WEBINAR);

        if ($data['defaultModelId'] !== null) {
            $session->setRoomModel($data['defaultModelId']);
            //TODO: Persister le sco-id du modèle
            //$session->setRoomModel(5352936956);
        }

        return $session;
    }

    public function updateSession(ProviderSession|AdobeConnectWebinarSCO $session, array &$data, ?UserInterface $user = null): void
    {
        parent::updateSession($session, $data, $user);

        $session->setAdmittedSession($data['accessType']);

        $this->checkProviderAudioAvailableOrAssignOne($session);
    }

    private function checkProviderAudioAvailableOrAssignOne(AdobeConnectWebinarSCO $session): void
    {
        $convocation = $session->getConvocation();
        $dateBeginAudio = ( new \DateTime($session->getDateStart()->format('Y-m-d H:i:s')) )->modify('-30 minutes');
        $dateEndAudio = ( new \DateTime($session->getDateEnd()->format('Y-m-d H:i:s')) )->modify('+30 minutes');
        if (!empty($convocation)) {
            $currentProviderAudio = $session->getProviderAudio();
            if (!empty($currentProviderAudio) &&
                $currentProviderAudio->getConnectionType() === $convocation->getTypeAudio() &&
                $currentProviderAudio->getPhoneLang() === $convocation->getLanguages() &&
                $this->adobeConnectWebinarService->checkProviderAudioAvailabilityForDate($session, $dateBeginAudio, $dateEndAudio)
            ) {
                return;
            }

            $availableAudios = $this->adobeConnectWebinarService->getAvailableAudios($dateBeginAudio, $dateEndAudio, $convocation->getTypeAudio(), $convocation->getLanguages(), $session->getAbstractProviderConfigurationHolder());
            $session->setProviderAudio(empty($availableAudios) ? null : $availableAudios[0]);
        }
    }
}
