<?php

namespace App\Service\Provider\Adobe;

use App\Entity\ActivityLog;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\ProviderConfigurationHolderInterface;
use App\Exception\InvalidImportDataException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ConvocationRepository;
use App\Repository\EvaluationRepository;
use App\Repository\ProviderSessionRepository;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\ActivityLogger;
use App\Service\AdobeConnectService;
use App\Service\AsyncWorker\Tasks\ActionsFromImportSessionTask;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use App\Service\Provider\Common\AbstractSessionImporter;
use App\Service\ProviderSessionService;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdobeSessionImporter extends AbstractSessionImporter
{
    public const NAME_INDEX = 1;
    public const DATE_START_INDEX = 2;
    public const DURATION_INDEX = 3;
    public const CONVOCATION_NAME_INDEX = 4;
    public const SESSION_TYPE_INDEX = 5;
    public const LANG_INDEX = 6;
    public const REFERENCE_INDEX = 7;
    public const ACTION_INDEX = 8;
    public const URL_INDEX = 9;
    public const ACCESS_TYPES_INDEX = 10;
    public const MODEL_INDEX = 11;
    public const CONVOCATION_SESSION_CONTENT_INDEX = 12;
    public const BUSINESS_INDEX = 13;
    public const TAGS_INDEX = 14;
    public const INFORMATION_INDEX = 15;
    public const EVALUATION_INDEX = 16;
    public const SESSION_TEST = 17;
    public const OBJECTIVES = 18;
    public const LOCATION = 19;
    public const REST_LUNCH = 20;
    public const NBR_DAYS = 21;
    public const SUBJECT_IMPORT_CODE = 22;
    public const SUBSCRIPTION_MAX = 23;
    public const ALERT_LIMIT = 24;
    public const IMMEDIATE_SEND = 25;

    public function __construct(
        ProviderSessionService $providerSessionService,
        ProviderSessionRepository $providerSessionRepository,
        ConvocationRepository $convocationRepository,
        EvaluationRepository $evaluationRepository,
        SubjectRepository $subjectRepository,
        TranslatorInterface $translator,
        private AdobeConnectService $adobeConnectService,
        ActivityLogger $activityLogger
    ) {
        parent::__construct($providerSessionRepository, $convocationRepository, $translator, $providerSessionService, $evaluationRepository, $subjectRepository, $activityLogger);
    }

    public function readData(array $values): array
    {
        return [
            'title' => trim($values[self::NAME_INDEX] ?? ''),
            'dateStart' => (isset($values[self::DATE_START_INDEX]) ? \DateTime::createFromFormat('d/m/Y H:i', trim($values[2])) : ''),
            'duration' => (isset($values[self::DURATION_INDEX]) ? \DateTime::createFromFormat('H:i', trim($values[3])) : ''),
            'convocation' => trim($values[self::CONVOCATION_NAME_INDEX] ?? ''),
            'category' => trim($values[self::SESSION_TYPE_INDEX] ?? ''),
            'lang' => trim($values[self::LANG_INDEX] ?? ''),
            'reference' => trim($values[self::REFERENCE_INDEX] ?? ''),
            'action' => trim($values[self::ACTION_INDEX] ?? ''),
            'url' => trim($values[self::URL_INDEX] ?? ''),
            'accessType' => (int) trim($values[self::ACCESS_TYPES_INDEX] ?? ''),
            'businessNumber' => trim($values[self::BUSINESS_INDEX] ?? ''),
            'information' => trim($values[self::INFORMATION_INDEX] ?? ''),
            'tags' => trim(trim($values[self::TAGS_INDEX] ?? ''), ';'),
            'personalizedContent' => trim($values[self::CONVOCATION_SESSION_CONTENT_INDEX] ?? ''),
            'model' => trim($values[self::MODEL_INDEX] ?? ''),
            'evaluation' => trim($values[self::EVALUATION_INDEX] ?? ''),
            'sessionTest' => boolval($values[self::SESSION_TEST] ?? 0),
            'objectives' => trim(trim($values[self::OBJECTIVES] ?? ''), ';'),
            'location' => trim($values[self::LOCATION] ?? ''),
            'restlunch' => intval($values[self::REST_LUNCH] ?? 0),
            'nbrdays' => intval($values[self::NBR_DAYS] ?? 0),
            'subjectImportCode' => trim($values[self::SUBJECT_IMPORT_CODE] ?? ''),
            'subscriptionMax' => intval($values[self::SUBSCRIPTION_MAX] ?? 0),
            'alertLimit' => intval($values[self::ALERT_LIMIT] ?? 0),
            'immediateSend' => intval($values[self::IMMEDIATE_SEND] ?? 0),
            'warnings' => [],
        ];
    }

    public function fulfillData(array &$data, Client $client, AbstractProviderConfigurationHolder|AdobeConnectConfigurationHolder $configurationHolder): void
    {
        parent::fulfillData($data, $client, $configurationHolder);

        $data['accessType'] = match ($data['accessType']) {
            ActionsFromImportSessionTask::ACCESS_USER_ONLY => AdobeConnectConnector::GRANT_ACCESS_USER_ONLY,
            ActionsFromImportSessionTask::ACCESS_VISITOR => AdobeConnectConnector::GRANT_ACCESS_VISITOR,
            ActionsFromImportSessionTask::ACCESS_ALL => AdobeConnectConnector::GRANT_ACCESS_ALL,
            default => $configurationHolder->getDefaultConnexionType()
        };

        $data['template'] = $this->getMeetingTemplate(empty($data['model']) ? '' : $data['model'], $configurationHolder, $configurationHolder->getDefaultRoom());
        $data['defaultModelId'] = array_key_exists('uniq_identifier', $data['template']) ? $data['template']['uniq_identifier'] : null;
    }

    private function getMeetingTemplate(string $model, AdobeConnectConfigurationHolder $configurationHolder, ?int $defaultModel): array
    {
        //$response = $this->adobeConnectService->getSharedMeetingsTemplate($configurationHolder);
        $response = $this->adobeConnectService->findAllModelsRoom($configurationHolder);
        /** @var array $templates */
        $templates = $response->isSuccess() ? $response->getData() : [];

        $availableTemplates = [];

        if (!empty($model)) {
            $availableTemplates = array_filter($templates, function (array $template) use ($model) {
                return $template['name'] === $model;
            });
        }

        if (empty($availableTemplates) && !empty($defaultModel) && array_key_exists($defaultModel, $templates)) {
            return $templates[$defaultModel];
        }

        return empty($availableTemplates) ? [] : array_values($availableTemplates)[0];
    }

    public function validateData(array $data, ProviderConfigurationHolderInterface $configurationHolder): void
    {
        parent::validateData($data, $configurationHolder);

        if (!empty($data['url']) && preg_match('/[^A-Za-z0-9\-]/', $data['url'])) {
            throw new InvalidImportDataException('The url given for the session "%session%" is not valid!', ['%title%' => $data['title']], $this->translator->trans('The url given for the session "%session%" is not valid!', ['%title%' => $data['title']]));
        }

        if (!empty($data['url']) && $data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE && $data['providerSession'] !== null) {
            throw new InvalidImportDataException('You do not need to enter a url when you update the "%title%" session!', ['%title%' => $data['title']], $this->translator->trans('You do not need to enter a url when you update the "%title%" session!', ['%title%' => $data['title']]));
        }

        if (empty($data['accessType'])) {
            throw new InvalidImportDataException('The type of access entered for the "%session%" session is not valid!', ['%title%' => $data['title']], $this->translator->trans('The type of access entered for the "%session%" session is not valid!', ['%title%' => $data['title']]));
        }
    }

    public function checkWarnings(array &$data): void
    {
        parent::checkWarnings($data);
        if (empty($data['model']) && $data['action'] == ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE) {
            if (empty($data['providerSession'])) {
                $this->activityLogger->addActivityLog(new ActivityLogModel(
                    $data['logAction'],
                    ActivityLog::SEVERITY_WARNING,
                    ['message' => ['key' => 'The default room template was applied to the session (%ref%)', 'params' => ['%ref%' => $data['reference']]]]
                ));
                $data['warnings'][] = ['The default room template was applied to the session (%ref%)', ['%ref%' => $data['reference']]];
            }
        }
    }
}
