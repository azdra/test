<?php

namespace App\Service\Provider\Adobe;

use App\Entity\Adobe\AdobeConnectWebinarPrincipal;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Entity\User;
use App\Service\AdobeConnectWebinarService;
use App\Service\Provider\Model\ParticipantCreator;

class AdobeWebinarParticipantCreator implements ParticipantCreator
{
    public function __construct(private AdobeConnectWebinarService $adobeConnectWebinarService)
    {
    }

    public function createProviderParticipant(AbstractProviderConfigurationHolder $configurationHolder, User $user): ProviderParticipant
    {
        $response = $this->adobeConnectWebinarService->principalListByLogin($configurationHolder, $user->getEmail());

        $scoId = null;
        if ($response->isSuccess()) {
            $participants = $response->getData();
            $firstKey = array_key_first($participants);

            if (null !== $firstKey) {
                /** @var AdobeConnectWebinarPrincipal $participant */
                // Le syncParticipant nous retourne une liste vide ou contenant un participant car le filtre est sur un champs unique (login).
                $participant = $participants[$firstKey];
                $scoId = $participant->getPrincipalIdentifier();
            }
        }

        return (new AdobeConnectWebinarPrincipal($configurationHolder))
            ->setName($user->getFullName())
            ->setPrincipalIdentifier($scoId)
            ->setUser($user);
    }
}
