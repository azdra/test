<?php

namespace App\Service\Provider\Adobe;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Entity\User;
use App\Service\AdobeConnectService;
use App\Service\Provider\Model\ParticipantCreator;

class AdobeParticipantCreator implements ParticipantCreator
{
    public function __construct(private AdobeConnectService $adobeConnectService)
    {
    }

    public function createProviderParticipant(AbstractProviderConfigurationHolder $configurationHolder, User $user): ProviderParticipant
    {
        $response = $this->adobeConnectService->principalListByLogin($configurationHolder, $user->getEmail());

        $scoId = null;
        if ($response->isSuccess()) {
            $participants = $response->getData();
            $firstKey = array_key_first($participants);

            if (null !== $firstKey) {
                /** @var AdobeConnectPrincipal $participant */
                // Le syncParticipant nous retourne une liste vide ou contenant un participant car le filtre est sur un champs unique (login).
                $participant = $participants[$firstKey];
                $scoId = $participant->getPrincipalIdentifier();
            }
        }

        return (new AdobeConnectPrincipal($configurationHolder))
            ->setName($user->getFullName())
            ->setPrincipalIdentifier($scoId)
            ->setUser($user);
    }
}
