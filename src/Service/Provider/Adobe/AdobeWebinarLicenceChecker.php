<?php

namespace App\Service\Provider\Adobe;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Exception\LicenceCheckException;
use App\Repository\AdobeConnectWebinarSCORepository;
use App\Repository\UserRepository;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\Provider\Model\LicenceCheckerInterface;

class AdobeWebinarLicenceChecker implements LicenceCheckerInterface
{
    public function __construct(
        private AdobeConnectWebinarSCORepository $adobeConnectWebinarSCORepository,
        private UserRepository $userRepository,
        private ParticipantSessionSubscriber $participantSessionSubscriber
    ) {
    }

    public function getAvailableLicence(
        AbstractProviderConfigurationHolder $configurationHolder,
        \DateTime $dateStart,
        \DateTime $dateEnd,
        ?int $excludeSessionId = null
    ): array {
        /** @var AdobeConnectWebinarConfigurationHolder $configurationHolder */
        if (!$configurationHolder->isAutomaticLicensing()) {
            return [];
        }

        $licences = array_keys(array_filter($configurationHolder->getLicences(), function ($licence) {
            return $licence['shared'] === true;
        }));
        $safetyTime = 30;

        $usedLicence = $this->adobeConnectWebinarSCORepository->getUsedLicenceOnSLot(
            $configurationHolder,
            $licences,
            \DateTimeImmutable::createFromMutable($dateStart)->sub(new \DateInterval("PT{$safetyTime}M")),
            \DateTimeImmutable::createFromMutable($dateEnd)->add(new \DateInterval("PT{$safetyTime}M")),
            $excludeSessionId
        );

        $availableLicences = array_values(array_diff($licences, $usedLicence));

        if (count($availableLicences) === 0) {
            throw new LicenceCheckException('You cannot create or modify a session due to lack of available organizer license. Schedule a session on another time slot', [], 'No licence available for time slot');
        }

        return $availableLicences;
    }

    public function checkLicence(ProviderSession $providerSession): void
    {
        /** @var AdobeConnectWebinarConfigurationHolder $configurationHolder */
        $configurationHolder = $providerSession->getAbstractProviderConfigurationHolder();
        if ($configurationHolder->isAutomaticLicensing() && $providerSession->getAnimatorsCount() === 0) {
            $availableLicences = $this->getAvailableLicence(
                $configurationHolder,
                $providerSession->getDateStart(),
                $providerSession->getDateEnd(),
                $providerSession->getId()
            );

            $licence = $availableLicences[0];
            $userLicence = $this->userRepository->findOneBy([
                    'email' => $licence,
                    'client' => $providerSession->getClient(),
            ]);

            $this->participantSessionSubscriber->subscribe(
                $providerSession,
                $userLicence,
                ProviderParticipantSessionRole::ROLE_ANIMATOR,
                $providerSession->isSessionPresential() ? ProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL : ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL
            );
        }
    }
}
