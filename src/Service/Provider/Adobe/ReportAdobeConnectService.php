<?php

namespace App\Service\Provider\Adobe;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\ProviderSession;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\MiddlewareFailureException;
use App\Model\ProviderResponse;
use App\Service\AdobeConnectService;
use App\Service\Provider\Model\SessionPresenceReportSynchronize;
use App\Service\ProviderSessionService;
use Doctrine\ORM\EntityManagerInterface;

class ReportAdobeConnectService implements SessionPresenceReportSynchronize
{
    public function __construct(private EntityManagerInterface $entityManager,
                                private AdobeConnectService $adobeConnectService,
                                private ProviderSessionService $providerSessionService
    ) {
    }

    public function synchronizeReportPresence(ProviderSession $providerSession): array
    {
        if (!($providerSession instanceof AdobeConnectSCO)) {
            throw new InvalidProviderConfigurationHandler();
        }

        return $this->getReportAdobeConnect($providerSession, true);
    }

    public function getReportAdobeConnect(AdobeConnectSCO $adobeConnectSCO, bool $throwOnError = false): array
    {
        /** @var ProviderResponse $response */
        $ProvResponse = $this->adobeConnectService->reportMeetingAttendance($adobeConnectSCO);
        if ($ProvResponse->isSuccess()) {
            $response = $ProvResponse->getData();
        } else {
            if ($throwOnError) {
                throw new MiddlewareFailureException('Error fetching data : '.$ProvResponse->getData());
            } else {
                return [];
            }
        }

        $reportAttendees = $this->prepareAttendees($response, $adobeConnectSCO);

        foreach ($reportAttendees as $key => $value) {
            foreach ($adobeConnectSCO->getParticipantRoles()->getValues() as $ppsr) {
                if ($value['principal-id'] == $ppsr->getParticipant()->getPrincipalIdentifier()) {
                    $duration = $value['final-duration'];
                    $ppsr
                        ->setPresenceDateStart($value['date-from'])
                        ->setPresenceDateEnd($value['date-to'])
                        ->setSlotPresenceDuration($duration);
                    if ($duration >= $this->providerSessionService->getDurationMinimumForPresence($adobeConnectSCO)) {
                        $ppsr->setRemotePresenceStatus(1);
                        $ppsr->setPresenceStatus(1);
                    } else {
                        $ppsr->setPresenceStatus(0);
                    }
                    $this->entityManager->persist($ppsr);
                }
            }
        }

        return $reportAttendees;
    }

    public function prepareAttendees(array $rawAttendees, AdobeConnectSCO $adobeConnectSCO): array
    {
        $dStartSlot = \DateTimeImmutable::createFromMutable((clone $adobeConnectSCO->getDateStart())->setTimezone(new \DateTimeZone($adobeConnectSCO->getClient()->getTimezone()))->modify('-1 hours'));
        $dEndSlot = \DateTimeImmutable::createFromMutable((clone $adobeConnectSCO->getDateEnd())->setTimezone(new \DateTimeZone($adobeConnectSCO->getClient()->getTimezone()))->modify('+1 hours'));

        $reportAttendees = $this->arrangeAttendees($rawAttendees, $dStartSlot, $dEndSlot);

        foreach ($reportAttendees as $key => $value) {
            [$reportAttendees[$key]['final-duration'], $reportAttendees[$key]['date-from'], $reportAttendees[$key]['date-to']] = $this->evaluateDateAndDurationFromSlots($value['slots-duration']);
        }

        return $reportAttendees;
    }

    public function arrangeAttendees(array $rawAttendees, \DateTimeImmutable $dStartSlot, \DateTimeImmutable $dEndSlot): array
    {
        $reportAttendees = [];
        foreach ($rawAttendees as $rawAttendee) {
            if (isset($rawAttendee['date-created']) && isset($rawAttendee['date-end']) && $rawAttendee['date-created'] < $dEndSlot && $rawAttendee['date-end'] > $dStartSlot) {
                $username = $rawAttendee['login'];
                if (!isset($reportAttendees[$username])) {
                    $reportAttendees[$username] = [
                        'principal-id' => $rawAttendee['principal-id'],
                        'slots-duration' => [],
                    ];
                }
                $reportAttendees[$username]['slots-duration'][] = [
                    'date-from' => ($rawAttendee['date-created'] < $dStartSlot) ? $dStartSlot : $rawAttendee['date-created'],
                    'date-to' => ($rawAttendee['date-end'] > $dEndSlot) ? $dEndSlot : $rawAttendee['date-end'],
                ];
            }
        }

        return $reportAttendees;
    }

    public function evaluateDateAndDurationFromSlots(array $rawSlots): array
    {
        usort($rawSlots, static function (array $a, array $b) {
            $aTS = $a['date-from']->getTimestamp();
            $bTS = $b['date-from']->getTimestamp();

            return $aTS - $bTS;
        });

        $duration = 0;
        $dateStart = $rawSlots[0]['date-from'];
        $dateEnd = $dateStart;
        foreach ($rawSlots as $rawSlot) {
            //CASES TREATED
            //$dateStart|$dateEnd                               |-------------------|
            //$rawSlot['date-from']|$rawSlot['date-to']               |----------|
            //$final-duration                                   |----------------|

            //$dateStart|$dateEnd                               |----------------|
            //$rawSlot['date-from']|$rawSlot['date-to']                       |----------|
            //$final-duration                                   |----------------------|

            //$dateStart|$dateEnd                               |----------------|
            //$rawSlot['date-from']|$rawSlot['date-to']                               |----------|
            //$final-duration                                   |----------------|  |----------|
            if ($rawSlot['date-to'] > $dateEnd) {
                $duration += $this->calculateDuration($dateEnd, $rawSlot['date-from'], $rawSlot['date-to']);
                $dateEnd = $rawSlot['date-to'];
            }
        }

        return [$duration, $dateStart, $dateEnd];
    }

    private function calculateDuration(\DateTimeImmutable $toOld, \DateTimeImmutable $from, \DateTimeImmutable $to): int
    {
        if ($toOld < $from) {
            return $this->dateDiffIntervalMin($from, $to);
        } else {
            return $this->dateDiffIntervalMin($toOld, $to);
        }
    }

    private function dateDiffIntervalMin(\DateTimeImmutable $from, \DateTimeImmutable $to): int
    {
        $since_start = $to->diff($from);
        $minutes = $since_start->days * 24 * 60;
        $minutes += $since_start->h * 60;
        $minutes += $since_start->i;

        return $minutes;
    }
}
