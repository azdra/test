<?php

namespace App\Service\Provider\RequestHandler;

use App\Entity\ActivityLog;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\CategorySessionType;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Entity\EvaluationProviderSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\ProviderSessionRegistrationPage;
use App\Entity\SessionConvocationAttachment;
use App\Entity\User;
use App\Entity\WebinarConvocation;
use App\Exception\AsyncWorker\AsyncTaskException;
use App\Exception\InvalidImportDataException;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\LicenceCheckException;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Exception\UnknownProviderConfigurationHolderException;
use App\Model\ActivityLog\ActivityLogModel;
use App\RegistrationPage\Repository\RegistrationPageTemplateRepository;
use App\Repository\ClientRepository;
use App\Repository\ConvocationRepository;
use App\Repository\EvaluationRepository;
use App\Repository\GroupParticipantRepository;
use App\Repository\ProviderConfigurationHolderRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\SelfSubscription\Entity\Subject;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\ActivityLogger;
use App\Service\Provider\Adobe\AdobeLicenceChecker;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\Provider\Model\LicenceChecker;
use App\Service\ProviderSessionService;
use App\Service\Utils\StrUtils;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SessionRequestHandler
{
    public function __construct(
        private ParticipantSessionSubscriber $participantSessionSubscriber,
        private AuthorizationCheckerInterface $authorizationChecker,
        private EntityManagerInterface $entityManager,
        private TranslatorInterface $translator,
        private ClientRepository $clientRepository,
        private ConvocationRepository $convocationRepository,
        private ProviderConfigurationHolderRepository $configurationHolderRepository,
        private ProviderSessionRepository $sessionRepository,
        private UserRepository $userRepository,
        private ActivityLogger $activityLogger,
        private LoggerInterface $logger,
        private ProviderSessionService $providerSessionService,
        private EvaluationRepository $evaluationRepository,
        private SubjectRepository $subjectRepository,
        private LicenceChecker $licenceChecker,
        private AdobeLicenceChecker $adobeLicenceChecker,
        private RegistrationPageTemplateRepository $registrationPageTemplateRepository,
        private LoggerInterface $providerLogger,
        private GroupParticipantRepository $groupParticipantRepository
    ) {
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     * @throws InvalidProviderConfigurationHandler
     */
    public function handleRequest(array &$data, UserInterface $user): ProviderSession
    {
        $client = $this->clientRepository->find($data['clientId']);
        $configurationHolder = $data['configurationHolder'] = $this->configurationHolderRepository->find($data['configurationHolder']);

        //$this->activityLogger->initActivityLogContext($client, $user, ActivityLog::ORIGIN_USER_INTERFACE, ['data' => $data]);
        $this->activityLogger->initActivityLogContext($client, $user, ActivityLog::ORIGIN_USER_INTERFACE, []);
        $action = array_key_exists('id', $data) ? ActivityLog::ACTION_SESSION_UPDATE : ActivityLog::ACTION_SESSION_CREATION;
        try {
            $this->fulfillData($data);
            $this->validData($client, $user, $configurationHolder, $data);
            $this->providerLogger->info('Data fulfilled & validated', ['data' => $data]);
            if (array_key_exists('id', $data)) {
                $session = $data['session'];
                $oldSession = $this->sessionRepository->find($data['id']);
                $data['updateFields'] = [
                    'sessionName' => $oldSession->getName(),
                    'sessionDateStart' => $oldSession->getDateStart(),
                    'sessionDuration' => $oldSession->getDuration(),
                    'sessionLanguage' => $oldSession->getLanguage(),
                    'sessionPersonalizedContent' => $oldSession->getPersonalizedContent(),
                    'sessionSubject' => $oldSession->getSubject()?->getId(),
                ];
                if (array_key_exists('sendEmailForEdit', $data)) {
                    $data['updateFields']['sendEmailForEdit'] = $data['sendEmailForEdit'];
                }
                if (array_key_exists('joinSessionWebUrl', $data)) {
                    $data['updateFields']['sessionJoinSessionWebUrl'] = $oldSession->getJoinSessionWebUrl();
                }
                if (array_key_exists('connectionInformation', $data)) {
                    $data['updateFields']['sessionConnectionInformation'] = $oldSession->getConnectionInformation();
                }
                if ($session instanceof AdobeConnectSCO) {
                    $this->adobeLicenceChecker->licensingASession($session, $data['dateStart'], $data['dateEnd']);
                }
                $this->providerSessionService->updateSessionFromArray($configurationHolder, $session, $user, $data);
                if ($session instanceof WebexRestMeeting) {
                    $session->setLicence($data['licence']);
                }
            } else {
                if (!$configurationHolder instanceof WhiteConfigurationHolder && !$configurationHolder instanceof WebexRestConfigurationHolder) {
                    $this->licenceChecker->getLicenceAvailable($configurationHolder, $data['dateStart'], $data['dateEnd']);
                }
                $session = $this->providerSessionService->createSessionFromArray($configurationHolder, $user, $data);
            }
            $this->entityManager->flush();
        } catch (LicenceCheckException $exception) {
            throw $exception;
        } catch (ProviderSessionRequestHandlerException $exception) {
            $this->logger->error($exception);
            if (!$exception->isDisplayable()) {
                $exception->setKey(array_key_exists('id', $data)
                    ? 'An error occurred while creating a session'
                    : 'An error occurred while updating a session');
                $exception->setParams([]);
            }
            $infos = ['message' => ['key' => $exception->getKey(), 'params' => $exception->getParams()]];
            $this->activityLogger->addActivityLog(new ActivityLogModel($action, ActivityLog::SEVERITY_ERROR, $infos), true);

            throw $exception;
        }

        if ($configurationHolder instanceof AdobeConnectConfigurationHolder && empty($session->getProviderAudio()) && $session->getConvocation()->getCategoryTypeAudio() != AbstractProviderAudio::AUDIO_CATEGORY_COMPUTER && $session->getConvocation()->getCategoryTypeAudio() != AbstractProviderAudio::AUDIO_CATEGORY_PRESENTIAL && $action == ActivityLog::ACTION_SESSION_CREATION) {
            $warning = [
                'message' => ['key' => 'Session \'%sessionName%\' (ref: %sessionRef%) is stuck in draft status because there is no audio profile available', 'params' => ['%sessionName%' => $session->getName(), '%sessionRef%' => $session->getRef()]],
                'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
            ];
            $this->activityLogger->addActivityLog(new ActivityLogModel($action, ActivityLog::SEVERITY_WARNING, $warning), true);
        }

        return $session;
    }

    public function fulfillData(&$data): void
    {
        $data['session'] = array_key_exists('id', $data) ? $this->sessionRepository->find($data['id']) : null;

        $data['convocationObj'] = $this->convocationRepository->find($data['convocation']);

        if (str_contains($data['duration'], ':')) {
            $data['duration'] = str_replace(':', 'h', $data['duration']);
        }
        $data['duration'] = DateTime::createFromFormat('H\hi', $data['duration']);
        $data['dateStart'] = new DateTime($data['dateStart']);
        $data['dateEnd'] = (clone $data['dateStart'])->modify($data['duration']->format('\+ H \h\o\u\r\s \+ i \m\i\n'));

        $data['notificationDate'] = DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s.vp', $data['notificationDate']);
        $dateOfSendingReminders = [];
        foreach ($data['dateOfSendingReminders'] as $key => $date) {
            $reminders = DateTimeImmutable::createFromFormat('Y-m-d\TH:i:s.vp', $data['dateOfSendingReminders'][$key]);
            if ($reminders instanceof DateTimeImmutable) {
                $dateOfSendingReminders[] = $reminders;
            }
        }
        $data['dateOfSendingReminders'] = $dateOfSendingReminders;

        $data['evaluationsObj'] = (array_key_exists('evaluations', $data) && !empty($data['evaluations'])) ? $this->evaluationRepository->findBy(['id' => $data['evaluations']]) : [];
        $data['evaluationsObj'] = new ArrayCollection($data['evaluationsObj']);
        $data['nature'] = !empty($data['nature']) ? intval($data['nature']) : null;
        $data['assistanceType'] = empty($data['assistanceType']) ? ProviderSession::ENUM_NO : intval($data['assistanceType']);
        $data['supportType'] = empty($data['supportType']) ? ProviderSession::ENUM_SUPPORT_NO : intval($data['supportType']);
        $data['participants'] ??= [];
        $data['situationMixte'] ??= ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL;
        $data['subject'] = !empty($data['subject']) ? $this->subjectRepository->find($data['subject']) : null;
        $data['subscriptionMax'] ??= !empty($data['subscriptionMax']) ? intval($data['subscriptionMax']) : 0;
        $data['alertLimit'] ??= !empty($data['alertLimit']) ? intval($data['alertLimit']) : 0;
        $data['publishedReplay'] = !empty($data['publishedReplay']) && !empty($data['replayUrlKey']);
        $data['accessParticipantReplay'] = !empty($data['accessParticipantReplay']) && !empty($data['replayUrlKey']);

        $providerSessionRegistrationPage = new ProviderSessionRegistrationPage();
        if (array_key_exists('providerSessionRegistrationPage', $data)) {
            if (array_key_exists('places', $data['providerSessionRegistrationPage'])) {
                $providerSessionRegistrationPage->setPlaces($data['providerSessionRegistrationPage']['places']);
            }
            if (array_key_exists('description', $data['providerSessionRegistrationPage'])) {
                $providerSessionRegistrationPage->setDescription($data['providerSessionRegistrationPage']['description']);
            }
            if (array_key_exists('description2', $data['providerSessionRegistrationPage'])) {
                $providerSessionRegistrationPage->setDescription2($data['providerSessionRegistrationPage']['description2']);
            }
        }

        $data['providerSessionRegistrationPage'] = $providerSessionRegistrationPage;
        $data['registrationPageTemplate'] = array_key_exists('registrationPageTemplate', $data) ? $this->registrationPageTemplateRepository->findOneBy(['id' => $data['registrationPageTemplate']]) : null;

        if (array_key_exists('attachments', $data) && !empty($data['attachments'])) {
            $data['attachmentsObj'] = new ArrayCollection($this->entityManager->getRepository(SessionConvocationAttachment::class)->findBy(['id' => $data['attachments']]));
        }

        if (array_key_exists('webinarCommunication', $data) && !empty($data['webinarCommunication'])) {
            $data['webinarCommunicationObject'] = $this->updateWebinarConvocation($data['webinarCommunication']);
        }

        if (array_key_exists('evaluationsProviderSession', $data) && !empty($data['evaluationsProviderSession'])) {
            $data['evaluationsProviderSessionObject'] = $this->updateEvaluationsProviderSession($data['evaluationsProviderSession']);
        }

        if (array_key_exists('licenceEmailUserTeams', $data) && !empty($data['licenceEmailUserTeams'])) {
            $data['userLicence'] = $data['licenceEmailUserTeams'];
        }

        if (array_key_exists('disableSubjectReplayOnSave', $data) && $data['disableSubjectReplayOnSave']) {
            /** @var Subject $subject */
            $subject = $this->entityManager->getRepository(Subject::class)->findOneBy(['id' => $data['subject']]);
            if ($subject->getVideoReplay() && $subject->getVideoReplay()->getPublished()) {
                $subject->getVideoReplay()->setPublished(false);
            }
        }
    }

    public function updateWebinarConvocation(array $webinarCommunication): array
    {
        $webinarCommunicationObject = [];
        foreach ($webinarCommunication as $communication) {
            $convocation = $this->entityManager->getRepository(WebinarConvocation::class)->findOneBy(['id' => $communication['id']]);
            $convocation->setSubjectMail($communication['subjectMail'])
                        ->setConvocationDate(new DateTimeImmutable($communication['convocationDate']))
                        ->setContent($communication['content'])
                        ->setSendCommunicationTo($communication['sendCommunicationTo'])
                        ->setConvocationType($communication['convocationType'])
                        ->setIcs($communication['ics'])
                        ->setConvocationType($communication['convocationType']);

            if ($communication['sender']) {
                $convocation->setSender(StrUtils::senderFormated($communication['sender']));
            }

            foreach ($communication['attachments'] as $attachmentFile) {
                ($this->entityManager->getRepository(SessionConvocationAttachment::class)->findOneBy(['id' => $attachmentFile['id']]))->setWebinarConvocation($convocation);
            }
            $webinarCommunicationObject[] = $convocation;
        }

        return $webinarCommunicationObject;
    }

    public function updateEvaluationsProviderSession(array $evaluationsProviderSession): array
    {
        $evaluationsProviderSessionObject = [];
        foreach ($evaluationsProviderSession as $evaluation) {
            $evaluationProviderSession = $this->entityManager->getRepository(EvaluationProviderSession::class)->findOneBy(['id' => $evaluation['id']]);
            if (array_key_exists('sendAt', $evaluation) && !empty($evaluation['sendAt'])) {
                $evaluationProviderSession->setSendAt(new DateTime($evaluation['sendAt']));
            }
            $evaluationProviderSession->setSubjectMail($evaluation['subjectMail'])
                                      ->setContent($evaluation['content']);
            foreach ($evaluation['attachments'] as $attachmentFile) {
                ($this->entityManager->getRepository(SessionConvocationAttachment::class)->findOneBy(['id' => $attachmentFile['id']]))->setEvaluationProviderSession($evaluationProviderSession);
            }
            $evaluationsProviderSessionObject[] = $evaluationProviderSession;
        }

        return $evaluationsProviderSessionObject;
    }

    public function handleUpdateParticipantListRequest(
        Request $request,
        ProviderSession $session,
    ): ProviderSession {
        $data = json_decode($request->getContent(), true);
        if (!is_array($data) || empty($data)) {
            throw new ProviderSessionRequestHandlerException('Content not provided', [], false, $this->translator->trans('Content not provided'));
        }
        $this->subscribeParticipants($session, $data);
        if (array_key_exists('groups', $data)) {
            $this->subscribeGroups($session, $data['groups']);
        }

        $this->entityManager->flush();

        return $session;
    }

    private function validData(
        ?Client $client,
        UserInterface $user,
        ?AbstractProviderConfigurationHolder $configurationHolder,
        array $data
    ): void {
        if (empty($client)) {
            throw new ProviderSessionRequestHandlerException('The account cannot be found', [], false, $this->translator->trans('The account cannot be found'));
        }

        if ($client->getId() !== $user->getClient()->getId() && !$this->authorizationChecker->isGranted(User::ROLE_ADMIN)) {
            throw new ProviderSessionRequestHandlerException('The user is not allowed to', [], false, $this->translator->trans('The user is not allowed to'));
        }

        if (empty($configurationHolder)) {
            throw new ProviderSessionRequestHandlerException('The connector cannot be found', [], false, $this->translator->trans('The connector cannot be found'));
        }

        if ($client->getId() !== $configurationHolder->getClient()->getId()) {
            throw new ProviderSessionRequestHandlerException('Connector does not belong to the account', [], false, $this->translator->trans('Connector does not belong to the account'));
        }

        if (empty($data['title'])) {
            throw new ProviderSessionRequestHandlerException('The session\'s name is empty!', [], true, $this->translator->trans('The session\'s name is empty!'));
        }

        if (empty($data['reference'])) {
            throw new ProviderSessionRequestHandlerException('The session reference "%title%" is empty!', ['%title%' => $data['title']], true, $this->translator->trans('The session reference "%title%" is empty!', ['%title%' => $data['title']]));
        }

        if ($data['configurationHolder']->getClient()->getId() == Client::ID_CLIENT_IFCAM) {
            $dateStart = $data['dateStart'];
            $endStart = new \DateTime(Client::TIPPING_DATE_CLIENT_IFCAM);
            $intervalDateStart = $dateStart->diff($endStart);
            if ($data['configurationHolder'] instanceof WebexConfigurationHolder) {
                if ($intervalDateStart->invert == 1) {
                    //dump('La date de départ est après le 31 décembre 2023 à 23:59:59');
                    throw new ProviderSessionRequestHandlerException('A session whose start date is after 12/31/2023 11:59:59 PM cannot be taken into account by this connector which will become obsolete!', [], true, $this->translator->trans('A session whose start date is after 12/31/2023 11:59:59 PM cannot be taken into account by this connector which will become obsolete!'));
                }
            }

            if ($data['configurationHolder'] instanceof WebexRestConfigurationHolder) {
                if ($intervalDateStart->invert != 1) {
                    //dump('La date de départ est avant le 31 décembre 2023 à 23:59:59');
                    throw new ProviderSessionRequestHandlerException('A session whose start date is before 12/31/2023 11:59:59 PM cannot be taken into account by this connector which will not be activated!', [], true, $this->translator->trans('A session whose start date is before 12/31/2023 11:59:59 PM cannot be taken into account by this connector which will not be activated!'));
                }
            }
        }

        if (array_key_exists('id', $data) && empty($data['session'])) {
            throw new ProviderSessionRequestHandlerException('the session %title% does not exist', ['%title%' => $data['title']], true, $this->translator->trans('the session %title% does not exist', ['%title%' => $data['title']]));
        }

        if (array_key_exists('id', $data) && $data['session']->getRef() !== $data['reference']) {
            throw new ProviderSessionRequestHandlerException('The session reference %ref% cannot be updated', ['%title%' => $data['title']], true, $this->translator->trans('The session reference %ref% cannot be updated', ['%title%' => $data['title']]));
        }

        if (array_key_exists('id', $data) && $client->getId() !== $data['session']->getClient()->getId()) {
            throw new ProviderSessionRequestHandlerException('Session %title% to update does not belong to the same account', ['%title%' => $data['title']], false, $this->translator->trans('Session %title% to update does not belong to the same account', ['%title%' => $data['title']]));
        }

        if (!array_key_exists('id', $data) && !empty($this->sessionRepository->findOneBy(['ref' => $data['reference'], 'client' => $configurationHolder->getClient()]))) {
            throw new ProviderSessionRequestHandlerException('A session with the reference %ref% already exists', ['%ref%' => $data['reference']], true, $this->translator->trans('A session with the reference %ref% already exists', ['%ref%' => $data['reference']]));
        }

        if (!is_null($data['category']) && is_null(CategorySessionType::from($data['category']))) {
            throw new ProviderSessionRequestHandlerException('The session category %title% is invalid !', ['%title%' => $data['title']], true, $this->translator->trans('The session category %title% is invalid !', ['%title%' => $data['title']]));
        }

        if (empty($data['lang']) || !in_array($data['lang'], ['fr', 'en'])) {
            throw new ProviderSessionRequestHandlerException('The session %title% language is invalid!', ['%title%' => $data['title']], true, $this->translator->trans('The session %title% language is invalid!', ['%title%' => $data['title']]));
        }

        if (empty($data['convocationObj'])) {
            throw new ProviderSessionRequestHandlerException('The session %title% language is invalid!', ['%title%' => $data['title']], true, $this->translator->trans('The session %title% convocation does not exist!', ['%title%' => $data['title']]));
        }

        if (!$configurationHolder->getClient()->categoryAvailable(CategorySessionType::from($data['category']))) {
            throw new InvalidImportDataException('The "%title%" session category is not available in your account!', ['%title%' => $data['title']], $this->translator->trans('The "%title%" session category is not available in your account!', ['%title%' => $data['title']]));
        }

        if ($data['category'] !== (CategorySessionType::CATEGORY_SESSION_WEBINAR)->value) {
            if (array_key_exists('registrationPageTemplate', $data) && !is_null($data['registrationPageTemplate'])) {
                throw new ProviderSessionRequestHandlerException('Only webinar categories can create registration pages', [], true, $this->translator->trans('Only webinar categories can create registration pages'));
            }
        }

        /** @var Subject $subject */
        $subject = $data['subject'];
        if (array_key_exists('disableSubjectReplayOnSave', $data) && !$data['disableSubjectReplayOnSave']) {
            if ($subject && $subject->getVideoReplay() && $subject->getVideoReplay()->getPublished()) {
                throw new ProviderSessionRequestHandlerException('The subject has a replay published', [], true, $this->translator->trans('The subject has a replay published'));
            }
        }
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     */
    private function subscribeParticipants(
        ProviderSession $session,
        array $data
    ): void {
        if (!empty($data)) {
            try {
                $this->participantSessionSubscriber->massSubscribe($session, $data['participants'], catchError: false);
                $this->participantSessionSubscriber->massUnsubscribe($session, $data['participants'], catchError: false);
            } catch (AsyncTaskException|UnknownProviderConfigurationHolderException $exception) {
                $this->logger->error($exception);
                throw new ProviderSessionRequestHandlerException($exception->getMessage(), [], false, $exception->getMessage());
            }
        }
    }

    public function subscribeGroups(ProviderSession $session, array $data): array
    {
        try {
            // Retrieve groups to be modified (subscribed or unsubscribed)
            $groupsToModify = $this->getGroupsToModify($session->getGroupsParticipants(), $data);

            // Process groups to subscribe
            foreach ($groupsToModify['toSubscribe'] as $groupParticipantDataToSubscribe) {
                $groupParticipant = $this->groupParticipantRepository->findOneBy(['id' => $groupParticipantDataToSubscribe['id']]);
                if ($groupParticipant) {
                    // Subscribe the group participant to the session
                    $warnings = $this->participantSessionSubscriber->groupSubscribe($session, $groupParticipant);
                }
            }

            // Process groups to unsubscribe
            foreach ($groupsToModify['toUnsubscribe'] as $groupParticipantDataToUnsubscribe) {
                $groupParticipant = $this->groupParticipantRepository->findOneBy(['id' => $groupParticipantDataToUnsubscribe['id']]);
                if ($groupParticipant) {
                    // Unsubscribe the group participant from the session
                    $warnings = $this->participantSessionSubscriber->groupUnSubscribe($session, $groupParticipant);
                }
            }

            // Process groups to verify (it's the same with the groups to subscribe, but we need clarification for the code)
            foreach ($groupsToModify['toVerify'] as $groupParticipantDataToVerify) {
                $groupParticipant = $this->groupParticipantRepository->findOneBy(['id' => $groupParticipantDataToVerify['id']]);
                if ($groupParticipant) {
                    // verify the participant registered in the session
                    $warnings = $this->participantSessionSubscriber->groupSubscribe($session, $groupParticipant);
                }
            }

            // Update the session with the new group participants
            $session->setGroupsParticipants($data);

            return $warnings ?? [];
        } catch (AsyncTaskException|UnknownProviderConfigurationHolderException $exception) {
            // Log the error and throw a custom exception
            $this->logger->error($exception);
            throw new ProviderSessionRequestHandlerException($exception->getMessage(), [], false, $exception->getMessage());
        }
    }

    private function getGroupsToModify(array $groupExisting, array $groupFinal): array
    {
        $groupsToSubscribe = [];
        $groupsToUnsubscribe = [];
        $groupsToVerify = [];

        if (!empty($groupExisting)) {
            // Create an associative array for quick lookup of existing groups
            $existingGroups = array_column($groupExisting, null, 'id');
        }

        // Find groups to subscribe (present in $groupFinal but not in $groupExisting)
        foreach ($groupFinal as $group) {
            if (!isset($existingGroups[$group['id']])) {
                $groupsToSubscribe[] = $group;
            }
        }

        // Find groups to unsubscribe (present in $groupExisting but not in $groupFinal)
        foreach ($groupExisting as $group) {
            if (!in_array($group['id'], array_column($groupFinal, 'id'))) {
                $groupsToUnsubscribe[] = $group;
            }
        }

        // Find groups to verify (present in both $groupFinal and $groupExisting but not in $groupsToUnsubscribe)
        foreach ($groupFinal as $group) {
            if (isset($existingGroups[$group['id']]) && !in_array($group, $groupsToUnsubscribe, true)) {
                $groupsToVerify[] = $group;
            }
        }

        return [
            'toSubscribe' => $groupsToSubscribe,
            'toUnsubscribe' => $groupsToUnsubscribe,
            'toVerify' => $groupsToVerify,
        ];
    }
}
