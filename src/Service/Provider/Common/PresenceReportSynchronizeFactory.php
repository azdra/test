<?php

namespace App\Service\Provider\Common;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\ProviderSession;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Service\Provider\Adobe\ReportAdobeConnectService;
use App\Service\Provider\Adobe\ReportAdobeConnectWebinarService;
use App\Service\Provider\Microsoft\ReportMicrosoftTeamsEventService;
use App\Service\Provider\Microsoft\ReportMicrosoftTeamsService;
use App\Service\Provider\Model\SessionPresenceReportSynchronize;
use App\Service\Provider\Webex\ReportWebexMeetingService;
use App\Service\Provider\Webex\ReportWebexRestMeetingService;

class PresenceReportSynchronizeFactory
{
    public function __construct(
        private ReportAdobeConnectService $reportAdobeConnectService,
        private ReportAdobeConnectWebinarService $reportAdobeConnectWebinarService,
        private ReportWebexMeetingService $reportWebexMeetingService,
        private ReportWebexRestMeetingService $reportWebexRestMeetingService,
        private ReportMicrosoftTeamsService $reportMicrosoftTeamsService,
        private ReportMicrosoftTeamsEventService $reportMicrosoftTeamsEventService,
    ) {
    }

    public function getService(AbstractProviderConfigurationHolder $configurationHolder): SessionPresenceReportSynchronize
    {
        return match (get_class($configurationHolder)) {
            AdobeConnectConfigurationHolder::class => $this->reportAdobeConnectService,
            AdobeConnectWebinarConfigurationHolder::class => $this->reportAdobeConnectWebinarService,
            WebexConfigurationHolder::class => $this->reportWebexMeetingService,
            WebexRestConfigurationHolder::class => $this->reportWebexRestMeetingService,
            MicrosoftTeamsConfigurationHolder::class => $this->reportMicrosoftTeamsService,
            MicrosoftTeamsEventConfigurationHolder::class => $this->reportMicrosoftTeamsEventService,
            default => throw new InvalidProviderConfigurationHandler()
        };
    }

    public function synchronizePresenceReport(ProviderSession $providerSession): array
    {
        $presenceReport = $this->getService($providerSession->getAbstractProviderConfigurationHolder())
            ->synchronizeReportPresence($providerSession);

        $providerSession->setLastPresenceStatusSynchronization(new \DateTimeImmutable());

        return $presenceReport;
    }
}
