<?php

namespace App\Service\Provider\Common;

use App\Entity\CategorySessionType;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Client\TrainingActionNatureEnum;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\ProviderSession;
use App\Entity\White\WhiteSession;
use App\Service\Provider\Model\SessionArrayHandler;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class AbstractSessionArrayHandler implements SessionArrayHandler
{
    abstract protected function createRealSession(AbstractProviderConfigurationHolder $configurationHolder, array $data): ProviderSession;

    public function createSession(AbstractProviderConfigurationHolder $configurationHolder, array &$data, ?UserInterface $user = null): ProviderSession
    {
        $session = $this->createRealSession($configurationHolder, $data);
        $session->setRef($data['reference'])
            ->setStatus(ProviderSession::STATUS_SCHEDULED)
            ->setLanguage($data['lang'])
            ->setPercentageAttendance($configurationHolder->getPercentageAttendance())
            ->setCreatedBy($user);

        $this->updateSession($session, $data, $user);

        if ($configurationHolder instanceof AdobeConnectConfigurationHolder && empty($session->getProviderAudio()) && $session->getConvocation()->getCategoryTypeAudio() != AbstractProviderAudio::AUDIO_CATEGORY_COMPUTER) {
            $session->setStatus(ProviderSession::STATUS_DRAFT);
            $session->getClient()->decrementCreditClient($session->getTraineesCount());
        }

        if ($configurationHolder instanceof WhiteConfigurationHolder) {
            if ($session->getCategory()->value !== CategorySessionType::CATEGORY_SESSION_PRESENTIAL->value) {
                if (empty($data['joinSessionWebUrl']) || $data['joinSessionWebUrl'] == '') {
                    $session->setStatus(ProviderSession::STATUS_DRAFT);
                    $session->getClient()->decrementCreditClient($session->getTraineesCount());
                }
            }
        }

        return $session;
    }

    public function updateSession(ProviderSession $session, array &$data, ?UserInterface $user = null): void
    {
        $session->setName($data['title'])
            ->setDateStart($data['dateStart'])
            ->setDateEnd($data['dateEnd'])
            ->setBusinessNumber($data['businessNumber'])
            ->setInformation($data['information'])
            ->setTags($data['tags'])
            ->setPersonalizedContent($data['personalizedContent'])
            ->setCategory(!is_null($data['category']) ? CategorySessionType::from($data['category']) : null)
            ->setUpdatedBy($user)
            ->setAutomaticSendingTrainingCertificates($data['automaticSendingTrainingCertificates'])
            ->setSessionTest($data['sessionTest'])
            ->setConvocation($data['convocationObj'])
            ->setNature(!is_null($data['nature']) ? TrainingActionNatureEnum::tryFrom($data['nature']) : null)
            ->setObjectives($data['objectives']);

        if (array_key_exists('nbrdays', $data) && !empty($data['nbrdays'])) {
            $session->setNbrdays(intval($data['nbrdays']));
        }

        if (array_key_exists('restlunch', $data) && !empty($data['restlunch'])) {
            $session->setRestlunch(intval($data['restlunch']));
        }

        if (array_key_exists('location', $data) && !empty($data['location'])) {
            $session->setLocation(trim($data['location']));
        }

        if (array_key_exists('joinSessionWebUrl', $data) && !empty($data['joinSessionWebUrl'])) {
            if ($session instanceof WhiteSession && $session->getCategory()->value !== CategorySessionType::CATEGORY_SESSION_PRESENTIAL->value) {
                $session->setJoinSessionWebUrl(trim($data['joinSessionWebUrl']));
                if ($data['joinSessionWebUrl'] != '' && $session->getStatus() == ProviderSession::STATUS_DRAFT) {
                    $session->setStatus(ProviderSession::STATUS_SCHEDULED);
                }
            }
        }

        if (array_key_exists('licence', $data) && !empty($data['licence'])) {
            if ($session instanceof WebexRestMeeting || $session instanceof MicrosoftTeamsEventSession) {
                $session->setLicence(trim($data['licence']));
            }
        }

        if (array_key_exists('userLicence', $data) && !empty($data['userLicence'])) {
            if ($session instanceof MicrosoftTeamsEventSession) {
                $session->setUserLicence($data['userLicence']);
            }
        }

        if (array_key_exists('location', $data) && !empty($data['location'])) {
            if ($session instanceof WhiteSession && $session->getCategory()->value === CategorySessionType::CATEGORY_SESSION_PRESENTIAL->value) {
                $session->setLocation(trim($data['location']));
                if ($data['location'] != '' && $session->getStatus() == ProviderSession::STATUS_DRAFT) {
                    $session->setStatus(ProviderSession::STATUS_SCHEDULED);
                }
            }
        }

        if (array_key_exists('connectionInformation', $data) && !empty($data['connectionInformation'])) {
            if ($session instanceof WhiteSession) {
                $session->setConnectionInformation($data['connectionInformation']);
            }
        }

        if (array_key_exists('subject', $data) && (!empty($data['subject']) || is_null($data['subject']))) {
            if (($data['subject'] && array_key_exists('updateFields', $data) && $data['subject']->getId() !== $data['updateFields']['sessionSubject'])) {
                $data['notificationDate'] = new \DateTimeImmutable();
            }
            $session->setSubject($data['subject']);
        }

        if (array_key_exists('subscriptionMax', $data) && !empty($data['subscriptionMax'])) {
            $session->setSubscriptionMax($data['subscriptionMax']);
        }

        if (array_key_exists('alertLimit', $data) && !empty($data['alertLimit'])) {
            $session->setAlertLimit($data['alertLimit']);
        }

        if (array_key_exists('registrationPageTemplateBanner', $data)) {
            $session->setRegistrationPageTemplateBanner($data['registrationPageTemplateBanner']);
        }

        if (array_key_exists('registrationPageTemplateIllustration', $data)) {
            $session->setRegistrationPageTemplateIllustration($data['registrationPageTemplateIllustration']);
        }

        if (array_key_exists('assistanceType', $data)) {
            $session->setAssistanceType(intval($data['assistanceType']));
        }

        if (array_key_exists('supportType', $data)) {
            $session->setSupportType(intval($data['supportType']));
        }

        if (array_key_exists('standardHoursActive', $data) && !empty($data['standardHoursActive'])) {
            $session->setStandardHours(boolval($data['standardHoursActive']));
        }

        if (array_key_exists('outsideStandardHoursActive', $data) && !empty($data['outsideStandardHoursActive'])) {
            $session->setOutsideStandardHours(boolval($data['outsideStandardHoursActive']));
        }

        if (array_key_exists('notificationDate', $data) && $data['notificationDate'] instanceof \DateTimeImmutable) {
            $session->setNotificationDate($data['notificationDate']);
        }

        if (array_key_exists('lastConvocationSent', $data) && $data['lastConvocationSent'] instanceof \DateTimeImmutable) {
            $session->setLastConvocationSent($data['lastConvocationSent']);
        }

        if (array_key_exists('dateOfSendingReminders', $data) && !empty($data['dateOfSendingReminders'])) {
            $session->setDateOfSendingReminders($data['dateOfSendingReminders']);
        }

        if (array_key_exists('evaluationsObj', $data) && !empty($data['evaluationsObj'])) {
            $session->setEvaluations($data['evaluationsObj']);
        }

        if (array_key_exists('attachmentsObj', $data) && !empty($data['attachmentsObj'])) {
            $session->setAttachments($data['attachmentsObj']);
        }

        if (array_key_exists('lang', $data) && !empty($data['lang'])) {
            $session->setLanguage($data['lang']);
        }

        if (array_key_exists('description', $data) && !empty($data['description'])) {
            $session->setDescription($data['description']);
        }

        if (array_key_exists('description2', $data) && !empty($data['description2'])) {
            $session->setDescription2($data['description2']);
        }

        if (array_key_exists('webinarCommunicationObject', $data) && !empty($data['webinarCommunicationObject'])) {
            $session->updateWebinarConvocation($data['webinarCommunicationObject']);
        }

        if (array_key_exists('evaluationsProviderSessionObject', $data) && !empty($data['evaluationsProviderSessionObject'])) {
            $session->updateEvaluationsProviderSession($data['evaluationsProviderSessionObject']);
        }

        if (array_key_exists('replayVideoPlatform', $data) && !empty($data['replayVideoPlatform'])) {
            $session->setReplayVideoPlatform($data['replayVideoPlatform']);
        }

        if (array_key_exists('replayUrlKey', $data)) {
            $session->setReplayUrlKey($data['replayUrlKey']);
        }

        if (array_key_exists('publishedReplay', $data)) {
            $session->setPublishedReplay($data['publishedReplay']);
        }

        if (array_key_exists('accessParticipantReplay', $data)) {
            $session->setAccessParticipantReplay($data['accessParticipantReplay']);
        }

        if (array_key_exists('replayLayoutPlayButton', $data)) {
            $session->setReplayLayoutPlayButton($data['replayLayoutPlayButton']);
        }

        if (array_key_exists('providerSessionRegistrationPage', $data) && (!empty($data['providerSessionRegistrationPage']) || is_null($data['providerSessionRegistrationPage']))) {
            $session->setProviderSessionRegistrationPage($data['providerSessionRegistrationPage']);
        }

        if (array_key_exists('registrationPageTemplate', $data) && (!empty($data['registrationPageTemplate']) || is_null($data['registrationPageTemplate']))) {
            $session->setRegistrationPageTemplate($data['registrationPageTemplate']);
        }
    }
}
