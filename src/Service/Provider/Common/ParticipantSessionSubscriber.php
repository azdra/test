<?php

namespace App\Service\Provider\Common;

use App\Entity\ActivityLog;
use App\Entity\CategorySessionType;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Entity\GroupParticipant;
use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Entity\White\WhiteSession;
use App\Exception\AsyncWorker\AsyncTaskException;
use App\Exception\UnknownProviderConfigurationHolderException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ProviderParticipantRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\MiddlewareService;
use App\Service\Provider\Adobe\AdobeParticipantCreator;
use App\Service\Provider\Adobe\AdobeWebinarParticipantCreator;
use App\Service\Provider\Microsoft\MicrosoftTeamsEventParticipantCreator;
use App\Service\Provider\Microsoft\MicrosoftTeamsParticipantCreator;
use App\Service\Provider\Model\ParticipantCreator;
use App\Service\Provider\Webex\WebexParticipantCreator;
use App\Service\Provider\Webex\WebexRestParticipantCreator;
use App\Service\Provider\White\WhiteParticipantCreator;
use App\Service\ReferrerService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class ParticipantSessionSubscriber
{
    public const ANIMATOR = 'animator';
    public const TRAINEE = 'trainee';

    public function __construct(
        private AdobeParticipantCreator $adobeParticipantCreator,
        private AdobeWebinarParticipantCreator $adobeWebinarParticipantCreator,
        private WebexParticipantCreator $webexParticipantCreator,
        private WebexRestParticipantCreator $webexRestParticipantCreator,
        private MicrosoftTeamsParticipantCreator $microsoftTeamsParticipantCreator,
        private MicrosoftTeamsEventParticipantCreator $microsoftTeamsEventParticipantCreator,
        private WhiteParticipantCreator $whiteParticipantCreator,
        private MiddlewareService $middlewareService,
        private EntityManagerInterface $entityManager,
        private ProviderParticipantRepository $participantRepository,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        private TranslatorInterface $translator,
        private ActivityLogger $activityLogger,
        private RouterInterface $router,
        private EventDispatcherInterface $eventDispatcher,
        private UserRepository $userRepository,
        private ReferrerService $referrerService,
        private LoggerInterface $logger,
        private LoggerInterface $providerLogger,
        private Security $security
    ) {
    }

    /**
     * @throws UnknownProviderConfigurationHolderException
     */
    private function getParticipantImporter(AbstractProviderConfigurationHolder $configurationHolder): ParticipantCreator
    {
        return match (get_class($configurationHolder)) {
            AdobeConnectConfigurationHolder::class => $this->adobeParticipantCreator,
            AdobeConnectWebinarConfigurationHolder::class => $this->adobeWebinarParticipantCreator,
            WebexConfigurationHolder::class => $this->webexParticipantCreator,
            WebexRestConfigurationHolder::class => $this->webexRestParticipantCreator,
            MicrosoftTeamsConfigurationHolder::class => $this->microsoftTeamsParticipantCreator,
            MicrosoftTeamsEventConfigurationHolder::class => $this->microsoftTeamsEventParticipantCreator,
            WhiteConfigurationHolder::class => $this->whiteParticipantCreator,
            default => throw new UnknownProviderConfigurationHolderException(get_class($configurationHolder).' is not supported by middleware.')
        };
    }

    public function massUnsubscribe(ProviderSession $session, array $rawParticipants, string $userIdKey = 'user_id', bool $catchError = true): array
    {
        $warnings = [];
        $userIdsToKeep = array_column($rawParticipants, $userIdKey);

        /** @var ProviderParticipantSessionRole $participantRole */
        foreach ($session->getParticipantRoles() as $participantRole) {
            if (!in_array($participantRole->getParticipant()->getUser()->getId(), $userIdsToKeep)) {
                try {
                    $this->unsubscribe($participantRole);
                } catch (AsyncTaskException|UnknownProviderConfigurationHolderException $exception) {
                    $this->providerLogger->error($exception);
                    if (!$catchError) {
                        $this->logger->error($exception);
                        throw $exception;
                    }

                    $this->logger->error($exception);

                    $warnings[] = $this->translator->trans(
                        'An error occurred while unsubscribing the participant %user%',
                        ['%user%' => $participantRole->getParticipant()->getFullName()]
                    );
                    continue;
                }
            }
        }

        return $warnings;
    }

    public function massSubscribe(ProviderSession $session, array $rawParticipants, string $userIdKey = 'user_id', bool $catchError = true): array
    {
        $warnings = [];

        $users = $this->userRepository->findByIdIndexById(array_column($rawParticipants, $userIdKey));
        foreach ($rawParticipants as $rawParticipant) {
            if ($session instanceof WebexMeeting && $session->getLicenceUser()?->getId() === $rawParticipant[$userIdKey]) {
                continue;
            }

            if ($session instanceof WebexRestMeeting && $session->getLicenceUser()?->getId() === $rawParticipant[$userIdKey]) {
                continue;
            }

            if ($session instanceof MicrosoftTeamsSession && $session->getLicenceUser()?->getId() === $rawParticipant[$userIdKey]) {
                continue;
            }

            if ($session instanceof MicrosoftTeamsEventSession && $session->getLicenceUser()?->getId() === $rawParticipant[$userIdKey]) {
                continue;
            }

            /*if ($session instanceof WhiteSession && $session->getLicenceUser()?->getId() === $rawParticipant[$userIdKey]) {
                continue;
            }*/

            $user = $users[$rawParticipant[$userIdKey]];
            try {
                if (!$this->isRegisteredWithRole($user->getEmail(), $session, $rawParticipant['role'], $rawParticipant['situationMixte'] ?? ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL)) {
                    $this->subscribe($session, $user, $rawParticipant['role'] ?? ProviderParticipantSessionRole::ROLE_TRAINEE, $rawParticipant['situationMixte'] ?? ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL);
                }
            } catch (AsyncTaskException|UnknownProviderConfigurationHolderException $exception) {
                $this->providerLogger->error($exception);
                if (!$catchError) {
                    $this->logger->error($exception);
                    throw $exception;
                }

                $this->logger->error($exception);

                $warnings[] = $this->translator->trans(
                    'An error occurred during the registration of the participant %user%',
                    ['%user%' => $user->getFullName()]
                );
                continue;
            }
        }

        return $warnings;
    }

    public function groupParticipantExisteInProviderParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole, GroupParticipant $groupParticipant): bool
    {
        foreach ($participantSessionRole->getGroupsParticipants() as $groupParticipantToSave) {
            if ($groupParticipantToSave['id'] == $groupParticipant->getId()) {
                return true;
            }
        }

        return false;
    }

    public function providerParticipantSessionRoleHaveAnotherGroupParticipant(ProviderParticipantSessionRole $participantSessionRole, GroupParticipant $groupParticipant): bool
    {
        foreach ($participantSessionRole->getGroupsParticipants() as $groupParticipantToSave) {
            if ($groupParticipantToSave['id'] != $groupParticipant->getId()) {
                return true;
            }
        }

        return false;
    }

    public function groupSubscribe(ProviderSession $session, GroupParticipant $groupParticipant, bool $catchError = true): array
    {
        $warnings = [];
        // Iterate over each user in the group participant
        foreach ($groupParticipant->getUsers() as $user) {
            $email = $user->getEmail(); // Store the user email for reuse
            $groupParticipantData = ['id' => $groupParticipant->getId(), 'name' => $groupParticipant->getName()]; // Store group participant data

            try {
                // Check if the user is already registered with the required role
                if (!$this->isRegisteredWithRole($email, $session, ProviderParticipantSessionRole::ROLE_TRAINEE, ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL)) {
                    // Subscribe the user and set group participants if not registered
                    $providerParticipantSessionRole = $this->subscribe($session, $user, ProviderParticipantSessionRole::ROLE_TRAINEE);
                    $providerParticipantSessionRole->setGroupsParticipants([$groupParticipantData]);
                } else {
                    // Get the existing participant
                    $existingParticipant = $this->getProviderParticipantSessionRoleWithEmail($email, $session);
                    // Check if the group participant does not already exist in the participant
                    if (!$this->groupParticipantExisteInProviderParticipantSessionRole($existingParticipant, $groupParticipant)) {
                        // Update the participant with the new group participant
                        $existingParticipant->setGroupsParticipants(array_merge($existingParticipant->getGroupsParticipants(), [$groupParticipantData]));
                    }
                }
            } catch (AsyncTaskException|UnknownProviderConfigurationHolderException $exception) {
                // Log the exception
                $this->providerLogger->error($exception);

                if (!$catchError) {
                    // If catchError is false, log and rethrow the exception
                    throw $exception;
                }

                // Add a warning for the user if an exception occurs
                $warnings[] = $this->translator->trans(
                    'An error occurred during the registration of the participant %user%',
                    ['%user%' => $user->getFullName()]
                );
            }
        }

        return $warnings;
    }

    public function groupUnSubscribe(ProviderSession $session, GroupParticipant $groupParticipant, bool $catchError = true): array
    {
        $warnings = [];
        // Iterate over each user in the group participant
        foreach ($groupParticipant->getUsers() as $user) {
            $email = $user->getEmail(); // Store the user email for reuse
            $groupId = $groupParticipant->getId(); // Store the group participant ID for reuse

            try {
                // Get the session role for the user
                $providerParticipantSessionRole = $this->getProviderParticipantSessionRoleWithEmail($email, $session);
                if ($providerParticipantSessionRole && $this->groupParticipantExisteInProviderParticipantSessionRole($providerParticipantSessionRole, $groupParticipant)) {
                    // Check if the user belongs to another group
                    if ($this->providerParticipantSessionRoleHaveAnotherGroupParticipant($providerParticipantSessionRole, $groupParticipant)) {
                        // Remove the user from the current group participant
                        $providerParticipantSessionRole->setGroupsParticipants(array_filter(
                                                                                   $providerParticipantSessionRole->getGroupsParticipants(),
                                                                                   function ($groupParticipantToSave) use ($groupId) {
                                                                                       return $groupParticipantToSave['id'] != $groupId;
                                                                                   }
                                                                               ));
                    } else {
                        // Unsubscribe the user if they do not belong to any other group
                        $this->unsubscribe($providerParticipantSessionRole);
                    }
                }
            } catch (AsyncTaskException|UnknownProviderConfigurationHolderException $exception) {
                // Log the exception
                $this->providerLogger->error($exception);

                if (!$catchError) {
                    // If catchError is false, log and rethrow the exception
                    throw $exception;
                }

                // Add a warning for the user if an exception occurs
                $warnings[] = $this->translator->trans(
                    'An error occurred during the unsubscription of the participant %user%',
                    ['%user%' => $user->getFullName()]
                );
            }
        }

        return $warnings;
    }

    public function isRegisteredWithRole(
        string $email,
        ProviderSession $session,
        string $role,
        int $situationMixte
    ): bool {
        /** @var ProviderParticipantSessionRole $participantRole */
        foreach ($session->getParticipantRoles() as $participantRole) {
            if ($email == $participantRole->getEmail() && ($participantRole->getRole() == $role && $participantRole->getSituationMixte() == $situationMixte)) {
                return true;
            }
        }

        return false;
    }

    public function getProviderParticipantSessionRoleWithEmail(
        string $email,
        ProviderSession $session,
    ): ?ProviderParticipantSessionRole {
        /** @var ProviderParticipantSessionRole $participantRole */
        foreach ($session->getParticipantRoles() as $participantRole) {
            if ($email == $participantRole->getEmail()) {
                return $participantRole;
            }
        }

        return null;
    }

    /**
     * @throws AsyncTaskException
     * @throws UnknownProviderConfigurationHolderException
     */
    public function subscribe(ProviderSession $session, User $user, string $role, ?int $situation = ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL): ProviderParticipantSessionRole
    {
        $configurationHolder = $session->getAbstractProviderConfigurationHolder();

        $participant = $this->participantRepository->findOneBy(['user' => $user, 'configurationHolder' => $configurationHolder]);

        if (empty($participant)) {
            $participant = $this->createParticipant($user, $configurationHolder);
            if ($session->getClient()->getSpecialCustomisation()->isEnabledSalesForce()) {
                $this->referrerService->updateReferrerUser($user);
            }
        }

        /** @var ProviderParticipantSessionRole $participantSessionRole */
        $participantSessionRole = $this->providerParticipantSessionRoleRepository->findOneBy([
            'session' => $session,
            'participant' => $participant,
        ]);

        if (!in_array($situation, [ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL, ProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL])) {
            $situation = $session->getCategory() === CategorySessionType::CATEGORY_SESSION_PRESENTIAL ? ProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL : ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL;
        }

        if (empty($participantSessionRole)) {
            $participantSessionRole = $this->createParticipantSessionRole($session, $participant, $role, $situation);
        } else {
            $this->updateParticipantSessionRoleRole($participantSessionRole, $role, $situation);
        }

        return $participantSessionRole;
    }

    /**
     * @throws AsyncTaskException
     */
    public function unsubscribe(ProviderParticipantSessionRole $participantSessionRole): void
    {
        $this->updateParticipantSessionRoleRole($participantSessionRole, ProviderParticipantSessionRole::ROLE_REMOVE);
    }

    /**
     * @throws UnknownProviderConfigurationHolderException
     * @throws AsyncTaskException
     */
    private function createParticipant(User $user, AbstractProviderConfigurationHolder $configurationHolder): ProviderParticipant
    {
        $participant = $this->getParticipantImporter($configurationHolder)->createProviderParticipant($configurationHolder, $user);

        $response = $this->middlewareService->call(MiddlewareService::MIDDLEWARE_ACTION_CREATE_PARTICIPANT, $participant);

        if ($response->isSuccess()) {
            $this->entityManager->persist($participant);
            $user->addParticipant($participant);
        } else {
            throw new AsyncTaskException($this->translator->trans('The user with the email %email%, already exists in the distant service.', ['%email%' => $user->getEmail()]));
        }

        return $participant;
    }

    /**
     * @throws AsyncTaskException
     */
    private function createParticipantSessionRole(ProviderSession $session, ProviderParticipant $participant, string $role, int $situation): ProviderParticipantSessionRole
    {
        $participantSessionRole = new ProviderParticipantSessionRole();

        $participantSessionRole->setSession($session)
            ->setParticipant($participant)
            ->setRole($role);

        $session->addParticipantRole($participantSessionRole);
        $participant->addSessionRoles($participantSessionRole);

        if ($session->getCategory() === CategorySessionType::CATEGORY_SESSION_MIXTE) {
            $participantSessionRole->setSituationMixte($situation);
        }

        if ($session->getCategory() === CategorySessionType::CATEGORY_SESSION_PRESENTIAL) {
            $participantSessionRole->setSituationMixte(ProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL);
        }

        $response = $this->middlewareService->call(MiddlewareService::MIDDLEWARE_ACTION_UPDATE_PARTICIPANT_SESSION_ROLE, $participantSessionRole);

        if (!$response->isSuccess()) {
            $this->addLogErrorOnSubscribing($participantSessionRole, ProviderParticipantSessionRole::ROLE_TRAINEE, $role);
            throw new AsyncTaskException($this->translator->trans('An error occurred when registering the participant with the email address "%email%" to the session %session%.', ['%email%' => $participantSessionRole->getParticipant()->getUser()->getEmail(), '%session%' => $participantSessionRole->getSession()->getRef()]));
        }

        $this->entityManager->persist($participantSessionRole);

        $this->addLogSuccessOnSubscribing($participantSessionRole, '', $role);

        return $participantSessionRole;
    }

    /**
     * @throws AsyncTaskException
     */
    public function updateParticipantSessionRoleRole(ProviderParticipantSessionRole $participantSessionRole, string $role, ?int $situation = ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL): void
    {
        if ($role === $participantSessionRole->getRole() && $situation === $participantSessionRole->getSituationMixte()) {
            $this->addLogAlreadyHaveRole($participantSessionRole);

            return;
        }
        $oldRole = $participantSessionRole->getRole();
        $oldSituation = $participantSessionRole->getSituationMixte();

        $participantSessionRole
            ->setRole($role);

        if ($participantSessionRole->getSession()->getCategory() === CategorySessionType::CATEGORY_SESSION_MIXTE) {
            $participantSessionRole->setSituationMixte($situation);
        }

        if ($participantSessionRole->getSession()->getCategory() === CategorySessionType::CATEGORY_SESSION_PRESENTIAL) {
            $participantSessionRole->setSituationMixte(ProviderParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL);
        }

        $response = $this->middlewareService->call(
            MiddlewareService::MIDDLEWARE_ACTION_UPDATE_PARTICIPANT_SESSION_ROLE,
            $participantSessionRole
        );

        if (!$response->isSuccess()) {
            $this->addLogErrorOnSubscribing($participantSessionRole, $oldRole, $role);
            throw new AsyncTaskException($this->translator->trans('An error occurred when registering the participant with the email address "%email%" to the session %session%.', ['%email%' => $participantSessionRole->getParticipant()->getUser()->getEmail(), '%session%' => $participantSessionRole->getSession()->getRef()]));
        }

        if ($role === ProviderParticipantSessionRole::ROLE_REMOVE) {
            $this->entityManager->remove($participantSessionRole);
        }

        if ($oldRole !== $role) {
            $this->addLogSuccessOnSubscribing($participantSessionRole, $oldRole, $role);
        }

        if ($oldSituation !== $situation) {
            $this->addLogSuccessUpdateSituation($participantSessionRole, $oldSituation, $situation);
        }
    }

    private function addLogAlreadyHaveRole(ProviderParticipantSessionRole $providerParticipantSessionRole): void
    {
        $params = $this->generateParamsForLog($providerParticipantSessionRole);
        $transKey = match ($providerParticipantSessionRole->getRole()) {
            ProviderParticipantSessionRole::ROLE_TRAINEE => 'Participant <a href="%userRoute%">%userName%</a> is already participant of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)',
            ProviderParticipantSessionRole::ROLE_ANIMATOR => 'Participant <a href="%userRoute%">%userName%</a> is already animator of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)',
            ProviderParticipantSessionRole::ROLE_REMOVE => 'Participant <a href="%userRoute%">%userName%</a> is already unsubscribed from the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)'
        };
        $infos = [
            'message' => ['key' => $transKey, 'params' => $params],
            'user' => ['id' => $providerParticipantSessionRole->getParticipant()->getUserId(), 'name' => $providerParticipantSessionRole->getParticipant()->getFullName()],
            'session' => ['id' => $providerParticipantSessionRole->getSession()->getId(), 'name' => $providerParticipantSessionRole->getSession()->getName(), 'ref' => $providerParticipantSessionRole->getSession()->getRef()],
        ];
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_PARTICIPANT_UPDATE_ROLE, ActivityLog::SEVERITY_WARNING, $infos));
    }

    private function addLogErrorOnSubscribing(ProviderParticipantSessionRole $providerParticipantSessionRole, string $oldRole, string $newRole): void
    {
        $params = $this->generateParamsForLog($providerParticipantSessionRole);
        $transKey = match ($newRole) {
            ProviderParticipantSessionRole::ROLE_TRAINEE => 'Error while adding <a href="%userRoute%">%userName%</a> as participant of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)',
            ProviderParticipantSessionRole::ROLE_ANIMATOR => 'Error while adding <a href="%userRoute%">%userName%</a> as animator of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)',
            ProviderParticipantSessionRole::ROLE_REMOVE => match ($oldRole) {
                ProviderParticipantSessionRole::ROLE_TRAINEE => 'Error while removing <a href="%userRoute%">%userName%</a> as participant of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)',
                ProviderParticipantSessionRole::ROLE_ANIMATOR => 'Error while removing <a href="%userRoute%">%userName%</a> as animator of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)'
            }
        };
        $infos = [
            'message' => ['key' => $transKey, 'params' => $params],
            'user' => ['id' => $providerParticipantSessionRole->getParticipant()->getUserId(), 'name' => $providerParticipantSessionRole->getParticipant()->getFullName()],
            'session' => ['id' => $providerParticipantSessionRole->getSession()->getId(), 'name' => $providerParticipantSessionRole->getSession()->getName(), 'ref' => $providerParticipantSessionRole->getSession()->getRef()],
        ];
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_PARTICIPANT_UPDATE_ROLE, ActivityLog::SEVERITY_ERROR, $infos));
    }

    private function addLogSuccessOnSubscribing(ProviderParticipantSessionRole $providerParticipantSessionRole, string $oldRole, string $newRole): void
    {
//        $params = $this->generateParamsForLog($providerParticipantSessionRole);
//        $transKey = match ($newRole) {
//            ProviderParticipantSessionRole::ROLE_TRAINEE => 'Participant <a href="%userRoute%">%userName%</a> is participant of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)',
//            ProviderParticipantSessionRole::ROLE_ANIMATOR => 'Participant <a href="%userRoute%">%userName%</a> is animator of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)',
//            ProviderParticipantSessionRole::ROLE_REMOVE => match ($oldRole) {
//                ProviderParticipantSessionRole::ROLE_TRAINEE => ($providerParticipantSessionRole->getParticipant()->isAllowToSendCanceledMail()) ? 'Participant <a href="%userRoute%">%userName%</a> is no more participant of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)' : 'Participant email <a href="%userRoute%">%userName%</a> has been changed',
//                ProviderParticipantSessionRole::ROLE_ANIMATOR => ($providerParticipantSessionRole->getParticipant()->isAllowToSendCanceledMail()) ? 'Participant <a href="%userRoute%">%userName%</a> is no more animator of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)' : 'Animator email <a href="%userRoute%">%userName%</a> has been changed'
//            }
//        };

        $user = $this->security->getUser();

        $params = $this->generateParamsForLog($providerParticipantSessionRole);

        if ($user) {
            $params = array_merge($params, [
                '%clientRoute%' => $this->router->generate('_participant_edit', ['id' => $this->security->getUser()->getId()]),
                '%client%' => $this->security->getUser()->getFullName(),
            ]);
        }

        $addTrainee = $user ? 'Participant <a href="%userRoute%">%userName%</a> has been added to the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%) by <a href="%clientRoute%">%client%</a>' : 'Participant <a href="%userRoute%">%userName%</a> is participant of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)';
        $addAnimator = $user ? "The Animator <a href='%userRoute%'>%userName%</a> has been added to the session <a href='%sessionRoute%'>%sessionName%</a> (ref: %sessionRef%) by <a href='%clientRoute%'>%client%</a>" : 'Participant <a href="%userRoute%">%userName%</a> is animator of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)';

        $removeTrainee = $user ? 'Participant <a href="%userRoute%">%userName%</a> has been removed to the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%) by <a href="%clientRoute%">%client%</a>' : 'Participant <a href="%userRoute%">%userName%</a> is no more participant of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)';
        $removeAnimator = $user ? "The Animator <a href='%userRoute%'>%userName%</a> has been removed to the session <a href='%sessionRoute%'>%sessionName%</a> (ref: %sessionRef%) by <a href='%clientRoute%'>%client%</a>" : 'Participant <a href="%userRoute%">%userName%</a> is no more animator of the session <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%)';

        $transKey = match ($newRole) {
            ProviderParticipantSessionRole::ROLE_TRAINEE => $addTrainee,
            ProviderParticipantSessionRole::ROLE_ANIMATOR => $addAnimator,
            ProviderParticipantSessionRole::ROLE_REMOVE => match ($oldRole) {
                ProviderParticipantSessionRole::ROLE_TRAINEE => ($providerParticipantSessionRole->getParticipant()->isAllowToSendCanceledMail()) ? $removeTrainee : 'Participant email <a href="%userRoute%">%userName%</a> has been changed',
                ProviderParticipantSessionRole::ROLE_ANIMATOR => ($providerParticipantSessionRole->getParticipant()->isAllowToSendCanceledMail()) ? $removeAnimator : 'Animator email <a href="%userRoute%">%userName%</a> has been changed'
            }
        };

        $infos = [
            'message' => ['key' => $transKey, 'params' => $params],
            'user' => ['id' => $providerParticipantSessionRole->getParticipant()->getUserId(), 'name' => $providerParticipantSessionRole->getParticipant()->getFullName()],
            'session' => ['id' => $providerParticipantSessionRole->getSession()->getId(), 'name' => $providerParticipantSessionRole->getSession()->getName(), 'ref' => $providerParticipantSessionRole->getSession()->getRef()],
        ];
        $this->activityLogger->addActivityLog(new ActivityLogModel($user ? ActivityLog::ACTION_ADD_PARTICIPANT_TO_SESSION : ActivityLog::ACTION_PARTICIPANT_UPDATE_ROLE, ActivityLog::SEVERITY_INFORMATION, $infos));
    }

    private function addLogSuccessUpdateSituation(ProviderParticipantSessionRole $providerParticipantSessionRole, string $oldSituation, string $newSituation): void
    {
        $params = array_merge($this->generateParamsForLog($providerParticipantSessionRole), [
            '%oldSituation%' => $this->translator->trans(ProviderParticipantSessionRole::getSituationMixteHumanized($oldSituation)),
            '%newSituation%' => $this->translator->trans(ProviderParticipantSessionRole::getSituationMixteHumanized($newSituation)),
        ]);
        $transKey = 'Participant <a href="%userRoute%">%userName%</a> has been updated situation in <a href="%sessionRoute%">%sessionName%</a> (ref: %sessionRef%) from %oldSituation% to %newSituation%';
        $infos = [
            'message' => ['key' => $transKey, 'params' => $params],
            'user' => ['id' => $providerParticipantSessionRole->getParticipant()->getUserId(), 'name' => $providerParticipantSessionRole->getParticipant()->getFullName()],
            'session' => ['id' => $providerParticipantSessionRole->getSession()->getId(), 'name' => $providerParticipantSessionRole->getSession()->getName(), 'ref' => $providerParticipantSessionRole->getSession()->getRef()],
        ];
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_PARTICIPANT_UPDATE_SITUATION, ActivityLog::SEVERITY_INFORMATION, $infos));
    }

    private function generateParamsForLog(ProviderParticipantSessionRole $providerParticipantSessionRole): array
    {
        return [
            '%userRoute%' => $this->router->generate('_participant_edit', ['id' => $providerParticipantSessionRole->getParticipant()->getUserId()]),
            '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $providerParticipantSessionRole->getSession()->getId()]),
            '%sessionName%' => $providerParticipantSessionRole->getSession()->getName(),
            '%userName%' => $providerParticipantSessionRole->getParticipant()->getFullName(),
            '%sessionRef%' => $providerParticipantSessionRole->getSession()->getRef(),
        ];
    }
}
