<?php

namespace App\Service\Provider\Common;

use App\Exception\InvalidTemplateUploadedForImportException;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class GuestImportDataReader
{
    public const HIGHEST_COLUMN = 'D';

    /**
     * @throws Exception
     */
    public function getSheet(Spreadsheet $spreadsheet): Worksheet
    {
        return $spreadsheet->getSheet(0);
    }

    public function validateTemplateLoadedIsValid(Worksheet $sheet): bool
    {
        if ($sheet->getCell('A1')->getValue() !== 'Email' ||
            $sheet->getCell('B1')->getValue() !== 'Prénom' ||
            $sheet->getCell('C1')->getValue() !== 'Nom' ||
            $sheet->getCell('D1')->getValue() !== 'Action'
        ) {
            throw new InvalidTemplateUploadedForImportException('The file uploaded for the user import is not a valid user import file.');
        }

        return true;
    }

    public function readData(Worksheet $sheet): array
    {
        $rowsData = [];
        $rowNum = 1;
        foreach ($sheet->getRowIterator(4) as $row) {
            ++$rowNum;

            $colNum = 0;
            foreach ($row->getCellIterator('A', self::HIGHEST_COLUMN) as $cell) {
                ++$colNum;
                $rowsData[$rowNum][$colNum] = $cell->getValue();
            }
            $rowsData[$rowNum]['numlinefile'] = $rowNum + 2;
        }

        return $rowsData;
    }
}
