<?php

namespace App\Service\Provider\Common;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\ProviderSession;
use App\Exception\MiddlewareFailureException;
use App\Model\ProviderSessionInitialClientData;

class ProviderSessionFactory
{
    /**
     * @throws MiddlewareFailureException
     */
    public static function createProviderSession(string $type, AbstractProviderConfigurationHolder $abstractProviderConfigurationHolder): ProviderSession
    {
        $session = match ($type) {
            'cisco_webex_meeting' => new WebexMeeting($abstractProviderConfigurationHolder),
            'cisco_webex_rest_meeting' => new WebexRestMeeting($abstractProviderConfigurationHolder),
            'adobe_connect_sco' => new AdobeConnectSCO($abstractProviderConfigurationHolder),
            'microsoft_teams_session' => new MicrosoftTeamsSession($abstractProviderConfigurationHolder),
            default => throw new MiddlewareFailureException('Unable to generate this session. No matching provider with the given configuration holder', ['provider' => $abstractProviderConfigurationHolder::getProvider(), 'configurationHolderId' => $abstractProviderConfigurationHolder->getId(), 'clientId' => $abstractProviderConfigurationHolder->getClient()->getId()])
        };

        ProviderSessionInitialClientData::fromClient($abstractProviderConfigurationHolder->getClient())
                                        ->apply($session);

        return $session;
    }
}
