<?php

namespace App\Service\Provider\Common;

use App\Entity\ActivityLog;
use App\Entity\CategorySessionType;
use App\Entity\Client\Client;
use App\Entity\Client\ClientServices;
use App\Entity\Client\TrainingActionNatureEnum;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\ProviderConfigurationHolderInterface;
use App\Entity\ProviderSession;
use App\Exception\InvalidImportDataException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ConvocationRepository;
use App\Repository\EvaluationRepository;
use App\Repository\ProviderSessionRepository;
use App\SelfSubscription\Entity\Subject;
use App\SelfSubscription\Repository\SubjectRepository;
use App\Service\ActivityLogger;
use App\Service\AsyncWorker\Tasks\ActionsFromImportSessionTask;
use App\Service\Provider\Model\SessionImporter;
use App\Service\ProviderSessionService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSessionImporter implements SessionImporter
{
    public function __construct(
        protected ProviderSessionRepository $providerSessionRepository,
        protected ConvocationRepository $convocationRepository,
        protected TranslatorInterface $translator,
        protected ProviderSessionService $providerSessionService,
        protected EvaluationRepository $evaluationRepository,
        protected SubjectRepository $subjectRepository,
        protected ActivityLogger $activityLogger
    ) {
    }

    abstract protected function readData(array $values): array;

    protected function fulfillData(array &$data, Client $client, AbstractProviderConfigurationHolder $configurationHolder): void
    {
        $data['providerSession'] = !empty($data['reference']) ? $this->providerSessionRepository->findOneBy(['ref' => $data['reference'], 'abstractProviderConfigurationHolder' => $configurationHolder]) : null;
        if (empty($data['convocation']) && $configurationHolder->getDefaultConvocation() != null) {
            $data['convocationObj'] = match ($data['lang']) {
                'en' => $this->convocationRepository->find(id: $configurationHolder->getDefaultConvocationEnglish()),
                default => $this->convocationRepository->find(id: $configurationHolder->getDefaultConvocation()),
            };
        } else {
            $data['convocationObj'] = $this->convocationRepository->findConvocation(client: $client, convocationName: $data['convocation'], configurationHolderType: $configurationHolder::getProvider());
        }
        $data['category'] = empty($data['category']) ? CategorySessionType::CATEGORY_SESSION_FORMATION : intval($data['category']);
        $data['action'] = empty($data['action']) ? ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE : (int) $data['action'];
        $data['reference'] = empty($data['reference']) ? ProviderSessionService::generateSessionReference($client->getName()) : $data['reference'];
        $data['nature'] = !empty($data['nature']) ? intval($data['nature']) : null;
        $data['subject'] = !empty($data['subjectImportCode']) ? $this->subjectRepository->findOneBy(['importCode' => $data['subjectImportCode']]) : null;
        $data['subscriptionMax'] = empty($data['subscriptionMax']) ? 0 : intval($data['subscriptionMax']);
        $data['alertLimit'] = intval($data['alertLimit']);
        $data['immediateSend'] = intval($data['immediateSend']);

        if (!empty($data['dateStart']) && !empty($data['duration'])) {
            $data['dateEnd'] = clone $data['dateStart'];
            $data['dateEnd']->modify('+ '.$data['duration']->format('H').' hours')->modify('+ '.$data['duration']->format('i').' min');
        }

        $data['tags'] = empty($data['tags']) ? null : explode(';', $data['tags']);
        $data['objectives'] = empty($data['objectives']) ? null : explode(';', $data['objectives']);
        $data['automaticSendingTrainingCertificates'] = $client->getEmailOptions()->isSendAnAttestationAutomatically();

        if ($data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE && empty($data['providerSession']) && !empty($data['dateStart'])) {
            $dates = $this->providerSessionService->getConvocationAndRemindersDate($client->getEmailScheduling(), $data['dateStart']);
            if ($data['immediateSend'] == 1) {
                $data['notificationDate'] = new \DateTimeImmutable();
                $data['lastConvocationSent'] = new \DateTimeImmutable();
            } else {
                $data['notificationDate'] = $dates['convocation'];
            }

            $data['dateOfSendingReminders'] = $dates['reminders'];
        }

        $data['logAction'] = match ($data['action']) {
            ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE => empty($data['providerSession']) ? ActivityLog::ACTION_SESSION_CREATION : ActivityLog::ACTION_SESSION_UPDATE,
            ActionsFromImportSessionTask::ACTION_SESSION_DELETE => ActivityLog::ACTION_SESSION_DELETE,
            ActionsFromImportSessionTask::ACTION_SESSION_CANCEL => ActivityLog::ACTION_SESSION_CANCEL,
            default => ActivityLog::ACTION_IMPORT_SESSIONS
        };

        if (!empty($data['evaluation'])) {
            $evaluation = $this->evaluationRepository->findOneBy([
                'title' => $data['evaluation'],
                'client' => $client,
            ]);

            $data['evaluationsObj'] = ($evaluation !== null ? new ArrayCollection([$evaluation]) : -1);
        }

        $services = $client->getServices();
        $data['assistanceType'] = $services->getAssistanceType() === ClientServices::ENUM_FULL ? ProviderSession::ENUM_LAUNCH : ProviderSession::ENUM_NO;

        $data['supportType'] = match ($services->getSupportType()) {
            ClientServices::ENUM_SUPPORT_FULL => $data['supportType'] = ProviderSession::ENUM_SUPPORT_TOTAL,
            ClientServices::ENUM_SUPPORT_LAUNCH => $data['supportType'] = ProviderSession::ENUM_SUPPORT_LAUNCH,
            default => $data['supportType'] = ProviderSession::ENUM_SUPPORT_NO,
        };

        $data['standardHoursActive'] = $services->getStandardHours()->isActive();
        $data['outsideStandardHoursActive'] = $services->getOutsideStandardHours()->isActive();
    }

    public function extractData(array $values, Client $client, AbstractProviderConfigurationHolder $configurationHolder): array
    {
        $data = $this->readData($values);
        $this->fulfillData($data, $client, $configurationHolder);

        return $data;
    }

    /**
     * @throws InvalidImportDataException
     */
    public function validateData(array $data, ProviderConfigurationHolderInterface $configurationHolder): void
    {
        if (empty($data['title'])) {
            throw new InvalidImportDataException('The session\'s name is empty!', [], $this->translator->trans('The session\'s name is empty!'));
        }

        if (empty($data['dateStart'])) {
            throw new InvalidImportDataException('The session date "%title%" is empty!', ['%title%' => $data['title']], $this->translator->trans('The session date "%title%" is empty!', ['%title%' => $data['title']]));
        }

        if (empty($data['duration'])) {
            throw new InvalidImportDataException('The duration for the "%title%" session is not correct!', ['%title%' => $data['title']], $this->translator->trans('The duration for the "%title%" session is not correct!', ['%title%' => $data['title']]));
        }

        if (empty($data['convocation']) && $configurationHolder->getDefaultConvocation() == null) {
            throw new InvalidImportDataException('The "%title%" session convocation is empty!', ['%title%' => $data['title']], $this->translator->trans('The "%title%" session convocation is empty!', ['%title%' => $data['title']]));
        }

        if (empty($data['convocationObj'])) {
            throw new InvalidImportDataException('The convocation does not exist!', [], $this->translator->trans('The convocation does not exist!', []));
        }

        if (!is_null($data['category']) && is_null(CategorySessionType::tryFrom($data['category']))) {
            throw new InvalidImportDataException('The "%title%" session category is not correct!', ['%title%' => $data['title']], $this->translator->trans('The "%title%" session category is not correct!', ['%title%' => $data['title']]));
        }

        if (!in_array($data['action'], [ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE, ActionsFromImportSessionTask::ACTION_SESSION_DELETE, ActionsFromImportSessionTask::ACTION_SESSION_CANCEL])) {
            throw new InvalidImportDataException('The requested action "%action%" for the "%title%" session is not correct!', ['%title%' => $data['title'], '%action%' => $data['action']], $this->translator->trans('The requested action "%action%" for the "%title%" session is not correct!', ['%title%' => $data['title'], '%action%' => $data['action']]));
        }

        if ($data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_DELETE && $data['providerSession'] === null) {
            throw new InvalidImportDataException('You cannot delete a session "%title%" that does not exist!', ['%title%' => $data['title']], $this->translator->trans('You cannot delete a session "%title%" that does not exist!', ['%title%' => $data['title']]));
        }

        if ($data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_CANCEL && $data['providerSession'] === null) {
            throw new InvalidImportDataException('You cannot cancel a session "%title%" that does not exist!', ['%title%' => $data['title']], $this->translator->trans('You cannot cancel a session "%title%" that does not exist!', ['%title%' => $data['title']]));
        }

        if (!is_null($data['nature']) && is_null(TrainingActionNatureEnum::tryFrom($data['nature']))) {
            throw new InvalidImportDataException('The nature of the session "%title%" is not correct!', ['%title%' => $data['title']], $this->translator->trans('The nature of the session "%title%" is not correct!', ['%title%' => $data['title']]));
        }

        if (intval($data['restlunch']) > 180) {
            throw new InvalidImportDataException('the lunch break of the session must be less than or equal to 180 minutes', ['%session%' => $data['title']], $this->translator->trans('the lunch break of the session must be less than or equal to 180 minutes', ['%session%' => $data['title']]));
        }

        if (intval($data['nbrdays']) > 5) {
            throw new InvalidImportDataException('The days number of session "%session%" must be less than or equal to 5 days!', ['%session%' => $data['title']], $this->translator->trans('The days number of session "%session%" must be less than or equal to 5 days!', ['%session%' => $data['title']]));
        }

        if (!$configurationHolder->getClient()->categoryAvailable(CategorySessionType::from($data['category']))) {
            throw new InvalidImportDataException('The "%title%" session category is not available in your account!', ['%title%' => $data['title']], $this->translator->trans('The "%title%" session category is not available in your account!', ['%title%' => $data['title']]));
        }

        if (isset($data['evaluationsObj']) && $data['evaluationsObj'] === -1) {
            throw new InvalidImportDataException('The evaluation form "%evaluation%" is not available in your account!', ['%evaluation%' => $data['evaluation']], $this->translator->trans('The evaluation form "%evaluation%" is not available in your account!', ['%evaluation%' => $data['evaluation']]));
        }

        if (empty($data['subject']) && !empty($data['subjectImportCode'])) {
            throw new InvalidImportDataException('The subject is not found', [], $this->translator->trans('The subject is not found'));
        }
    }

    public function checkWarnings(array &$data): void
    {
        if ($data['action'] == ActionsFromImportSessionTask::ACTION_SESSION_ADD_OR_UPDATE) {
            /** @var Subject $subject */
            $subject = $data['subject'];
            if ($subject && $subject->getVideoReplay()) {
                $this->activityLogger->addActivityLog(new ActivityLogModel(
                    $data['logAction'],
                    ActivityLog::SEVERITY_WARNING,
                    ['message' => ['key' => 'The session (%ref%) was not attached to the topic because it has a published replay', 'params' => ['%ref%' => $data['reference']]]]
                ));
                $data['warnings'][] = ['The session (%ref%) was not attached to the topic because it has a published replay', ['%ref%' => $data['reference']]];
                $data['subject'] = null;
            }
        }
    }
}
