<?php

namespace App\Service\Provider\White;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Entity\ProviderSession;
use App\Entity\White\WhiteSession;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Repository\UserRepository;
use App\Service\Provider\Common\AbstractSessionArrayHandler;
use Symfony\Component\Security\Core\User\UserInterface;

class WhiteArrayHandler extends AbstractSessionArrayHandler
{
    public function __construct(
        private UserRepository $userRepository,
    ) {
    }

    protected function createRealSession(AbstractProviderConfigurationHolder $configurationHolder, array $data): ProviderSession
    {
        return new WhiteSession($configurationHolder);
    }

    /**
     * @throws ProviderSessionRequestHandlerException
     */
    public function updateSession(ProviderSession|WhiteSession $session, array &$data, ?UserInterface $user = null): void
    {
        parent::updateSession($session, $data, $user);

        if (!array_key_exists('participants', $data)) {
            $data['participants'] = [];
        }
        $session->setCandidates($data['participants']);

        /** @var WhiteConfigurationHolder $configurationHolder */
        $configurationHolder = $session->getAbstractProviderConfigurationHolder();

        /*if (!$configurationHolder->isAutomaticLicensing() || !empty($data['licence'])) {
            $session->setLicence($data['licence']);
        } else {*/
        $session->setCandidates($data['participants']);
        //}
    }
}
