<?php

namespace App\Service\Provider\White;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Entity\User;
use App\Entity\White\WhiteParticipant;
use App\Service\Provider\Model\ParticipantCreator;

class WhiteParticipantCreator implements ParticipantCreator
{
    public function createProviderParticipant(AbstractProviderConfigurationHolder $configurationHolder, User $user): ProviderParticipant
    {
        return (new WhiteParticipant($configurationHolder))
            ->setUser($user);
    }
}
