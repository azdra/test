<?php

namespace App\Service;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\MiddlewareManagedEntityInterface;
use App\Entity\ProviderSession;
use App\Entity\White\WhiteSession;
use App\Event\TransactionalEmailRequestEventInterface;
use App\Event\TransactionnalNotifyParticipantsSessionIsUpdateEmailRequestEvent;
use App\Exception\MiddlewareException;
use App\Exception\ProviderServiceValidationException;
use App\Exception\UnknownMethodException;
use App\Exception\UnknownProviderConfigurationHolderException;
use App\Model\MiddlewareResponse;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;

class MiddlewareService
{
    public const SERVICE_TYPE_ADOBE_CONNECT = 'adobe-connect';
    public const SERVICE_TYPE_CISCO_WEBEX = 'cisco-webex';
    public const SERVICE_TYPE_CISCO_WEBEX_REST = 'cisco-webex-rest';
    public const SERVICE_TYPE_MICROSOFT_TEAMS = 'microsoft-teams';
    public const SERVICE_TYPE_MICROSOFT_TEAMS_EVENT = 'microsoft-teams-event';
    public const SERVICE_TYPE_ADOBE_CONNECT_WEBINAR = 'adobe-connect-webinar';
    public const SERVICE_TYPE_WHITE = 'white-connector';

    public const MODEL_TYPE_SESSION = 'session';
    public const MODEL_TYPE_PARTICIPANT = 'participant';

    public const MIDDLEWARE_ACTION_CREATE_SESSION = 'createSession';
    public const MIDDLEWARE_ACTION_UPDATE_SESSION = 'updateSession';
    public const MIDDLEWARE_ACTION_DELETE_SESSION = 'deleteSession';
    public const MIDDLEWARE_ACTION_CANCEL_SESSION = 'cancelSession';
    public const MIDDLEWARE_ACTION_CREATE_PARTICIPANT = 'createParticipant';
    public const MIDDLEWARE_ACTION_UPDATE_PARTICIPANT = 'updateParticipant';
    public const MIDDLEWARE_ACTION_DELETE_PARTICIPANT = 'deleteParticipant';
    public const MIDDLEWARE_ACTION_UPDATE_PARTICIPANT_SESSION_ROLE = 'updateParticipantSessionRole';
    public const MIDDLEWARE_ACTION_REDIRECT_PARTICIPANT_SESSION_ROLE = 'redirectParticipantSessionRole';
    public const MIDDLEWARE_ACTION_GET_MEETING = 'getMeeting';
    public const MIDDLEWARE_ACTION_CHECK_ORGANIZERS = 'checkUsersIsOrganizers';

    public function __construct(
        private iterable $providerServices,
        private LoggerInterface $logger,
        private EventDispatcherInterface $eventDispatcher
    ) {
    }

    public function getProviderServiceForConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): AbstractProviderService
    {
        /** @var AbstractProviderService $providerService */
        foreach ($this->providerServices as $providerService) {
            if ($providerService->supportConfigurationHolder($configurationHolder)) {
                return $providerService;
            }
        }
        $exception = new UnknownProviderConfigurationHolderException(sprintf('ConfigurationHolder %s is not handled', get_class($configurationHolder)));
        $this->logger->error($exception);
        throw new UnknownProviderConfigurationHolderException(sprintf('ConfigurationHolder %s is not handled', get_class($configurationHolder)));
    }

    public function call(string $method, MiddlewareManagedEntityInterface $middlewareManagedEntity, ?array $params = []): MiddlewareResponse
    {
        try {
            if (!$this->isMethodSupported($method)) {
                throw new UnknownMethodException(sprintf('Asked method %s is not supported', $method));
            }
            $providerService = $this->getProviderServiceForConfigurationHolder($middlewareManagedEntity->getAbstractProviderConfigurationHolder());

            $serviceResponse = call_user_func_array([$providerService, $method], [$middlewareManagedEntity]);
            if ($method == self::MIDDLEWARE_ACTION_UPDATE_SESSION && $serviceResponse->isSuccess() && !empty($params['updateFields'])) {
                $this->notifyParticipantIfSignificantChange($serviceResponse->getData(), $params['updateFields']);
            }

            return MiddlewareResponse::fromProvider($serviceResponse);
        } catch (ProviderServiceValidationException $exception) {
            $this->logger->error($exception);

            return MiddlewareResponse::fromValidationException($exception);
        } catch (MiddlewareException $exception) {
            $this->logger->error($exception);

            return MiddlewareResponse::fromMiddlewareException($exception);
        }
    }

    private function isMethodSupported(string $method): bool
    {
        return in_array($method,
            [
                self::MIDDLEWARE_ACTION_CREATE_SESSION,
                self::MIDDLEWARE_ACTION_UPDATE_SESSION,
                self::MIDDLEWARE_ACTION_DELETE_SESSION,
                self::MIDDLEWARE_ACTION_CANCEL_SESSION,
                self::MIDDLEWARE_ACTION_CREATE_PARTICIPANT,
                self::MIDDLEWARE_ACTION_UPDATE_PARTICIPANT,
                self::MIDDLEWARE_ACTION_DELETE_PARTICIPANT,
                self::MIDDLEWARE_ACTION_UPDATE_PARTICIPANT_SESSION_ROLE,
                self::MIDDLEWARE_ACTION_REDIRECT_PARTICIPANT_SESSION_ROLE,
                self::MIDDLEWARE_ACTION_GET_MEETING,
                self::MIDDLEWARE_ACTION_CHECK_ORGANIZERS,
            ]
        );
    }

    public function notifyParticipantIfSignificantChange(ProviderSession $providerSession, array $updateFields = []): void
    {
        if ($providerSession->isScheduled() && $providerSession->isConvocationSent()) {
            $changes = [];
            if (array_key_exists('sessionName', $updateFields)) {
                if ($updateFields['sessionName'] !== $providerSession->getName()) {
                    $changes[] = [
                        'type' => 'Name',
                        'title' => 'Title of the session',
                        'previous' => $updateFields['sessionName'],
                        'new' => $providerSession->getName(),
                    ];
                }
            }

            if (array_key_exists('sessionDateStart', $updateFields)) {
                if ($updateFields['sessionDateStart']->format('d/m/Y H:i') !== $providerSession->getDateStart()->format('d/m/Y H:i')) {
                    $changes[] = [
                        'type' => 'Date',
                        'title' => 'Session Date',
                        'previous' => $updateFields['sessionDateStart'],
                        'new' => $providerSession->getDateStart(),
                    ];
                }
            }

            if (array_key_exists('sessionDuration', $updateFields)) {
                if ($updateFields['sessionDuration'] !== $providerSession->getDuration()) {
                    $changes[] = [
                        'type' => 'duration',
                        'title' => 'Duration',
                        'previous' => $updateFields['sessionDuration'],
                        'new' => $providerSession->getDuration(),
                    ];
                }
            }

            if (array_key_exists('sessionJoinSessionWebUrl', $updateFields) && $providerSession instanceof WhiteSession) {
                /** @var WhiteSession $providerSession */
                if ($updateFields['sessionJoinSessionWebUrl'] !== $providerSession->getJoinSessionWebUrl()) {
                    $changes[] = [
                        'type' => 'joinSessionWebUrl',
                        'title' => 'Url virtual room',
                        'previous' => $updateFields['sessionJoinSessionWebUrl'],
                        'new' => $providerSession->getJoinSessionWebUrl(),
                    ];
                }
            }

            if (array_key_exists('sessionLanguage', $updateFields)) {
                if ($updateFields['sessionLanguage'] !== $providerSession->getLanguage()) {
                    $changes[] = [
                        'type' => 'language',
                        'title' => 'Lang',
                        'previous' => $updateFields['sessionLanguage'],
                        'new' => $providerSession->getLanguage(),
                    ];
                }
            }

            if (array_key_exists('sessionPersonalizedContent', $updateFields)) {
                if ($updateFields['sessionPersonalizedContent'] !== $providerSession->getPersonalizedContent()) {
                    $changes[] = [
                        'type' => 'personalizedContent',
                        'title' => 'Custom content',
                        'previous' => $updateFields['sessionPersonalizedContent'],
                        'new' => $providerSession->getPersonalizedContent(),
                    ];
                }
            }

            if (array_key_exists('sessionConnectionInformation', $updateFields)) {
                /** @var WhiteSession $providerSession */
                if ($updateFields['sessionConnectionInformation'] !== $providerSession->getConnectionInformation()) {
                    $changes[] = [
                        'type' => 'connectionInformation',
                        'title' => 'Connection informations for the animator',
                        'previous' => $updateFields['sessionConnectionInformation'],
                        'new' => $providerSession->getConnectionInformation(),
                    ];
                }
            }

            if (array_key_exists('sendEmailForEdit', $updateFields)) {
                $sendEmailForEdit = $updateFields['sendEmailForEdit'];
            } else {
                $sendEmailForEdit = true;
            }

            if (!empty($changes) && $sendEmailForEdit) {
                $this->eventDispatcher->dispatch(
                    new TransactionnalNotifyParticipantsSessionIsUpdateEmailRequestEvent(
                        $providerSession,
                        null,
                        false,
                        $changes
                    ),
                    TransactionalEmailRequestEventInterface::NOTIFY_PARTICIPANTS_SESSION_IS_UPDATED
                );
            }
        }
    }
}
