<?php

namespace App\Service\ConfigurationHolderFormHandler;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Exception\ConfigurationHolderViolationsException;
use App\Exception\ProviderGenericException;
use App\Service\WebexRestMeetingService;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CiscoWebexRestConfigurationJsonHandler implements ProviderConfigurationJsonHandlerInterface
{
    public function __construct(
        private WebexRestMeetingService $webexRestMeetingService,
        private TranslatorInterface $translator,
        private LoggerInterface $logger
    ) {
    }

    public function checkViolations(array $configuration): array
    {
        $violations = [];

        if (!array_key_exists('id', $configuration)) {
            $violations['id'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => 'id'], 'validators');
        }

        if (!array_key_exists('provider', $configuration) || empty($configuration['provider'])) {
            $violations['provider'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => 'provider'], 'validators');
        }

        if (!array_key_exists('username', $configuration) || empty($configuration['username'])) {
            $field = $this->translator->trans('Connector ID');
            $violations['username'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        if (!array_key_exists('password', $configuration) || empty($configuration['password'])) {
            $field = $this->translator->trans('Password');
            $violations['password'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        if (!array_key_exists('siteName', $configuration) || empty($configuration['siteName'])) {
            $field = $this->translator->trans('Subdomain');
            $violations['siteName'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        return $violations;
    }

    public function createConfigurationHolder(array $configuration): AbstractProviderConfigurationHolder
    {
        return new WebexRestConfigurationHolder();
    }

    public function updateConfigurationHolder(AbstractProviderConfigurationHolder|WebexRestConfigurationHolder $configurationHolder, array $configuration): void
    {
        $configurationHolder->setUsername($configuration['username'])
            ->setPassword($configuration['password'])
            ->setName($configuration['name']);
        $configurationHolder->setSiteName($configuration['siteName'])
            ->setDefaultOptions([
                'optionJoinTeleconfBeforeHost' => $configuration['optionJoinTeleconfBeforeHost'],
                'optionOpenTime' => $configuration['optionOpenTime'],
            ]);
        $configurationHolder->setAutomaticLicensing(boolval($configuration['automaticLicensing']));
        $configurationHolder->setSafetyTime(intval($configuration['safetyTime']));
        if (array_key_exists('defaultConvocation', $configuration)) {
            $configurationHolder->setDefaultConvocation($configuration['defaultConvocation']);
        }

        if (array_key_exists('defaultConvocationEnglish', $configuration)) {
            $configurationHolder->setDefaultConvocationEnglish($configuration['defaultConvocationEnglish']);
        }

        if ($configurationHolder->getClientAccessToken() !== $configuration['clientAccessToken']) {
            $configurationHolder->setClientAccessToken($configuration['clientAccessToken']);
            $configurationHolder->setExpireAtToken(new \DateTime('+ 14 days'));
        }

        if ($configurationHolder->getClientId() !== $configuration['clientId']) {
            $configurationHolder->setClientId($configuration['clientId']);
        }

        if ($configurationHolder->getClientSecret() !== $configuration['clientSecret']) {
            $configurationHolder->setClientSecret($configuration['clientSecret']);
        }

        if ($configurationHolder->getClientRefreshToken() !== $configuration['clientRefreshToken']) {
            $configurationHolder->setClientRefreshToken($configuration['clientRefreshToken']);
        }
    }

    public function testConnexionAndGetDefaultData(AbstractProviderConfigurationHolder|WebexRestConfigurationHolder $configurationHolder): void
    {
        try {
            $response = $this->webexRestMeetingService->loginUser($configurationHolder);

            if (!$response->isSuccess()) {
                $exception = new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to connect to this provider', domain: 'validators')]);
                $this->logger->error($exception);
                throw $exception;
            }
        } catch (ProviderGenericException  $exception) {
            $this->logger->error($exception);
            throw new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to connect to this provider', domain: 'validators')]);
        }
    }
}
