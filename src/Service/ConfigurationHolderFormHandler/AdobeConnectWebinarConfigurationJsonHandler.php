<?php

namespace App\Service\ConfigurationHolderFormHandler;

use App\Entity\Adobe\AdobeConnectWebinarPrincipal;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Exception\ConfigurationHolderViolationsException;
use App\Exception\ProviderGenericException;
use App\Model\Provider\AdobeConnect\AdobeConnectWebinarCommonInfo;
use App\Service\AdobeConnectWebinarService;
use App\Service\Connector\Adobe\AdobeConnectWebinarConnector;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdobeConnectWebinarConfigurationJsonHandler implements ProviderConfigurationJsonHandlerInterface
{
    public function __construct(
        private AdobeConnectWebinarConnector $adobeConnectWebinarConnector,
        private AdobeConnectWebinarService $adobeConnectWebinarService,
        private TranslatorInterface $translator,
        private LoggerInterface $logger
    ) {
    }

    public function checkViolations(array $configuration): array
    {
        $violations = [];

        if (!array_key_exists('id', $configuration)) {
            $violations['id'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => 'id'], 'validators');
        }

        if (!array_key_exists('provider', $configuration) || empty($configuration['provider'])) {
            $violations['provider'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => 'provider'], 'validators');
        }

        if (!array_key_exists('username', $configuration) || empty($configuration['username'])) {
            $field = $this->translator->trans('Connector ID');
            $violations['username'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        if (!array_key_exists('password', $configuration) || empty($configuration['password'])) {
            $field = $this->translator->trans('Password');
            $violations['password'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        if (!array_key_exists('subdomain', $configuration) || empty($configuration['subdomain'])) {
            $field = $this->translator->trans('Subdomain');
            $violations['subdomain'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        if (!array_key_exists('defaultConnexionType', $configuration) || empty($configuration['defaultConnexionType'])) {
            $field = $this->translator->trans('Default connection type');
            $violations['defaultConnexionType'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        if (!array_key_exists('licenceType', $configuration) || empty($configuration['licenceType'])) {
            $field = $this->translator->trans('License type');
            $violations['licenceType'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        return $violations;
    }

    public function createConfigurationHolder(array $configuration): AbstractProviderConfigurationHolder
    {
        return new AdobeConnectWebinarConfigurationHolder();
    }

    public function updateConfigurationHolder(AbstractProviderConfigurationHolder|AdobeConnectWebinarConfigurationHolder $configurationHolder, array $configuration): void
    {
        $configurationHolder->setUsername($configuration['username'])
            ->setPassword($configuration['password'])
            ->setName($configuration['name']);
        $configurationHolder->setSubdomain($configuration['subdomain'])
            ->setDefaultConnexionType($configuration['defaultConnexionType'])
            ->setLicenceType((int) $configuration['licenceType'])
            ->setBirthday(new \DateTime($configuration['birthday']))
            ->setStandardViewOnOpeningRoom($configuration['standardViewOnOpeningRoom'])
            ->setLicences($configuration['licences'])
            ->setUsernameScoId($configuration['usernameScoId']);

        if (array_key_exists('automaticLicensing', $configuration)) {
            $configurationHolder->setAutomaticLicensing($configuration['automaticLicensing']);
        }

        if (array_key_exists('defaultRoom', $configuration)) {
            $configurationHolder->setDefaultRoom((int) $configuration['defaultRoom']);
        }

        if (array_key_exists('maxParticipantsWebinar', $configuration)) {
            $configurationHolder->setMaxParticipantsWebinar((int) $configuration['maxParticipantsWebinar']);
        }

        if (array_key_exists('defaultConvocation', $configuration)) {
            $configurationHolder->setDefaultConvocation($configuration['defaultConvocation']);
        }

        if (array_key_exists('defaultConvocationEnglish', $configuration)) {
            $configurationHolder->setDefaultConvocationEnglish($configuration['defaultConvocationEnglish']);
        }
    }

    /**
     * @throws ConfigurationHolderViolationsException
     */
    public function tryToLogin(AbstractProviderConfigurationHolder|AdobeConnectWebinarConfigurationHolder $configurationHolder): void
    {
        try {
            // <editor-folder> Connexion
            $response = $this->adobeConnectWebinarConnector->call($configurationHolder, AdobeConnectWebinarConnector::ACTION_LOGIN);

            if (!$response->isSuccess()) {
                $exception = new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to connect to this provider', domain: 'validators')]);
                $this->logger->error($exception);
                throw $exception;
            }
            // </editor-folder> Connexion

            // <editor-folder> Get host group
            $response = $this->adobeConnectWebinarService->getHostGroupPrincipal($configurationHolder);

            if (!$response->isSuccess()) {
                $exception = new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to find the host group', domain: 'validators')]);
                $this->logger->error($exception);
                throw $exception;
            }

            $principalsHostGroup = $response->getData();
            /** @var AdobeConnectWebinarPrincipal $principalHostGroup */
            $principalHostGroup = reset($principalsHostGroup);

            $configurationHolder->setHostGroupScoId($principalHostGroup?->getPrincipalIdentifier());
            // </editor-folder> Get host group

            // <editor-folder> Get admins group
            //$response = $this->adobeConnectWebinarService->getAdminsGroupPrincipal($configurationHolder);
            $response = $this->adobeConnectWebinarService->getAllAdminsGroupsPrincipal($configurationHolder);

            /*if (!$response->isSuccess()) {
                $exception = new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to find the admins group', domain: 'validators')]);
                $this->logger->error($exception);
                throw $exception;
            }

            $principals = $response->getData();*/
            $principals = $response;
            //var_dump(count($principals));
            if (count($principals) > 0) {
                if (is_null($configurationHolder->getAdminGroupScoIds())) {
                    $configurationHolder->setAdminGroupScoIds([]);
                }
            }
            foreach ($principals as $principal) {
                if (!array_key_exists($principal->getPrincipalIdentifier(), $configurationHolder->getAdminGroupScoIds())) {
                    //var_dump($principal->getPrincipalIdentifier());
                    $configurationHolder->addAdminGroupScoId($principal->getPrincipalIdentifier());
                }
            }
            //var_dump($configurationHolder->getAdminGroupScoIds());

            //$principalsAdminsGroup = $response->getData();

            // </editor-folder> Get admins troup
        } catch (ProviderGenericException  $exception) {
            $this->logger->error($exception);
            throw new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to connect to this provider', domain: 'validators')]);
        }
    }

    /**
     * @throws ConfigurationHolderViolationsException
     * @throws \Exception
     */
    public function getDefaultData(AbstractProviderConfigurationHolder|AdobeConnectWebinarConfigurationHolder $configurationHolder): void
    {
        try {
            // <editor-folder> Get saving sco folder id
            $response = $this->adobeConnectWebinarService->getSavingFolderScoId($configurationHolder);

            if (!$response->isSuccess()) {
                $exception = new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to find the place to store sessions', domain: 'validators')]);
                $this->logger->error($exception);
                throw $exception;
            }
            $savingFolderScoId = $response->getData();
            $configurationHolder->setSavingMeetingFolderScoId($savingFolderScoId);
            // </editor-folder> Get saving sco folder id

            // <editor-folder> Connector information
            $response = $this->adobeConnectWebinarService->getConnectorInformation($configurationHolder);

            if (!$response->isSuccess()) {
                $exception = new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to find information of connecteur connector', domain: 'validators')]);
                $this->logger->error($exception);
                throw new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to find information of connecteur connector', domain: 'validators')]);
            }

            /** @var AdobeConnectWebinarCommonInfo $adobeConnectWebinarCommonInfo */
            $adobeConnectWebinarCommonInfo = $response->getData();
            //$configurationHolder->setUsernameScoId($adobeConnectWebinarCommonInfo->getUserId());
            // </editor-folder> Connector information
        } catch (ProviderGenericException  $exception) {
            $exception = new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to connect to this provider', domain: 'validators')]);
            $this->logger->error($exception);
            throw new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to connect to this provider', domain: 'validators')]);
        }
    }

    /**
     * @throws ConfigurationHolderViolationsException
     */
    public function testConnexionAndGetDefaultData(AbstractProviderConfigurationHolder|AdobeConnectWebinarConfigurationHolder $configurationHolder): void
    {
        $this->tryToLogin($configurationHolder);
        $this->getDefaultData($configurationHolder);
    }
}
