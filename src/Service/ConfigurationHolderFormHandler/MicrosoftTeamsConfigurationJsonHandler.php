<?php

namespace App\Service\ConfigurationHolderFormHandler;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Exception\ConfigurationHolderViolationsException;
use App\Exception\ProviderGenericException;
use App\Service\MicrosoftTeamsService;
use Symfony\Contracts\Translation\TranslatorInterface;

class MicrosoftTeamsConfigurationJsonHandler implements ProviderConfigurationJsonHandlerInterface
{
    public function __construct(
        private MicrosoftTeamsService $microsoftTeamsService,
        private TranslatorInterface $translator
    ) {
    }

    public function checkViolations(array $configuration): array
    {
        $violations = [];

        if (!array_key_exists('id', $configuration)) {
            $violations['id'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => 'id'], 'validators');
        }

        if (!array_key_exists('provider', $configuration) || empty($configuration['provider'])) {
            $violations['provider'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => 'provider'], 'validators');
        }

        if (!array_key_exists('username', $configuration) || empty($configuration['username'])) {
            $field = $this->translator->trans('Connector ID');
            $violations['username'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        if (!array_key_exists('password', $configuration) || empty($configuration['password'])) {
            $field = $this->translator->trans('Password');
            $violations['password'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        /*if (!array_key_exists('siteName', $configuration) || empty($configuration['siteName'])) {
            $field = $this->translator->trans('Subdomain');
            $violations['siteName'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }*/

        return $violations;
    }

    public function createConfigurationHolder(array $configuration): AbstractProviderConfigurationHolder
    {
        return new MicrosoftTeamsConfigurationHolder();
    }

    public function updateConfigurationHolder(AbstractProviderConfigurationHolder|MicrosoftTeamsConfigurationHolder $configurationHolder, array $configuration): void
    {
        $configurationHolder->setUsername($configuration['username'])
            ->setPassword($configuration['password'])
            ->setName($configuration['name']);

        $configurationHolder
            ->setDomain($configuration['domain']);

        $configurationHolder
            ->setOrganizerEmail($configuration['organizerEmail'])
            ->setTokenEndpoint($configuration['tokenEndPoint'])
            ->setGrantType($configuration['grantType'])
            ->setOrganizerUniqueIdentifier($configuration['organizerUniqueIdentifier'])
            ->setScope($configuration['scope'])
            ->setTenantId($configuration['tenantId'])
            ->setClientId($configuration['clientId'])
            ->setClientSecret($configuration['clientSecret']);

        $configurationHolder->setAutomaticLicensing(boolval($configuration['automaticLicensing']));
        if (array_key_exists('defaultConvocation', $configuration)) {
            $configurationHolder->setDefaultConvocation($configuration['defaultConvocation']);
        }

        if (array_key_exists('defaultConvocationEnglish', $configuration)) {
            $configurationHolder->setDefaultConvocationEnglish($configuration['defaultConvocationEnglish']);
        }
    }

    public function testConnexionAndGetDefaultData(AbstractProviderConfigurationHolder|MicrosoftTeamsConfigurationHolder $configurationHolder): void
    {
        try {
            /*$response = $this->microsoftTeamsService->getToken($configurationHolder);

            if (!$response->isSuccess()) {
                throw new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to connect to this provider', domain: 'validators')]);
            }*/
        } catch (ProviderGenericException  $exception) {
            throw new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to connect to this provider', domain: 'validators')]);
        }
    }
}
