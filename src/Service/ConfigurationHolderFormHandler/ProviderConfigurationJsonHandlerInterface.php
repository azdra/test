<?php

namespace App\Service\ConfigurationHolderFormHandler;

use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;

interface ProviderConfigurationJsonHandlerInterface
{
    public function checkViolations(array $configuration): array;

    public function createConfigurationHolder(array $configuration): AbstractProviderConfigurationHolder;

    public function updateConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder, array $configuration): void;

    public function testConnexionAndGetDefaultData(AbstractProviderConfigurationHolder $configurationHolder): void;
}
