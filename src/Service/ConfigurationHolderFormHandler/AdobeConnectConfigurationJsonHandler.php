<?php

namespace App\Service\ConfigurationHolderFormHandler;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Exception\ConfigurationHolderViolationsException;
use App\Exception\ProviderGenericException;
use App\Model\Provider\AdobeConnect\AdobeConnectCommonInfo;
use App\Service\AdobeConnectService;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdobeConnectConfigurationJsonHandler implements ProviderConfigurationJsonHandlerInterface
{
    public function __construct(
        private AdobeConnectConnector $adobeConnectConnector,
        private AdobeConnectService $adobeConnectService,
        private TranslatorInterface $translator,
        private LoggerInterface $logger
    ) {
    }

    public function checkViolations(array $configuration): array
    {
        $violations = [];

        if (!array_key_exists('id', $configuration)) {
            $violations['id'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => 'id'], 'validators');
        }

        if (!array_key_exists('provider', $configuration) || empty($configuration['provider'])) {
            $violations['provider'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => 'provider'], 'validators');
        }

        if (!array_key_exists('username', $configuration) || empty($configuration['username'])) {
            $field = $this->translator->trans('Connector ID');
            $violations['username'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        if (!array_key_exists('password', $configuration) || empty($configuration['password'])) {
            $field = $this->translator->trans('Password');
            $violations['password'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        if (!array_key_exists('subdomain', $configuration) || empty($configuration['subdomain'])) {
            $field = $this->translator->trans('Subdomain');
            $violations['subdomain'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        if (!array_key_exists('defaultConnexionType', $configuration) || empty($configuration['defaultConnexionType'])) {
            $field = $this->translator->trans('Default connection type');
            $violations['defaultConnexionType'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        if (!array_key_exists('licenceType', $configuration) || empty($configuration['licenceType'])) {
            $field = $this->translator->trans('License type');
            $violations['licenceType'] = $this->translator->trans("Field \"%field%\" canno't be empty", ['%field%' => $field], 'validators');
        }

        return $violations;
    }

    public function createConfigurationHolder(array $configuration): AbstractProviderConfigurationHolder
    {
        return new AdobeConnectConfigurationHolder();
    }

    public function updateConfigurationHolder(AbstractProviderConfigurationHolder|AdobeConnectConfigurationHolder $configurationHolder, array $configuration): void
    {
        $configurationHolder->setUsername($configuration['username'])
            ->setPassword($configuration['password'])
            ->setName($configuration['name']);
        $configurationHolder->setSubdomain($configuration['subdomain'])
            ->setDefaultConnexionType($configuration['defaultConnexionType'])
            ->setLicenceType((int) $configuration['licenceType'])
            ->setBirthday(new \DateTime($configuration['birthday']))
            ->setStandardViewOnOpeningRoom($configuration['standardViewOnOpeningRoom'])
            ->setLicences($configuration['licences'])
            ->setAutomaticLicensing($configuration['automaticLicensing'] ?? false);

        if (array_key_exists('defaultRoom', $configuration)) {
            $configurationHolder->setDefaultRoom((int) $configuration['defaultRoom']);
        }
        if (array_key_exists('folderDefaultRoom', $configuration)) {
            $configurationHolder->setFolderDefaultRoom((int) $configuration['folderDefaultRoom']);
        }
        if (array_key_exists('defaultConvocation', $configuration)) {
            $configurationHolder->setDefaultConvocation($configuration['defaultConvocation']);
        }

        if (array_key_exists('defaultConvocationEnglish', $configuration)) {
            $configurationHolder->setDefaultConvocationEnglish($configuration['defaultConvocationEnglish']);
        }
    }

    /**
     * @throws ConfigurationHolderViolationsException
     */
    public function tryToLogin(AbstractProviderConfigurationHolder|AdobeConnectConfigurationHolder $configurationHolder): void
    {
        try {
            // <editor-folder> Connexion
            $response = $this->adobeConnectConnector->call($configurationHolder, AdobeConnectConnector::ACTION_LOGIN);

            if (!$response->isSuccess()) {
                $exception = new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to connect to this provider', domain: 'validators')]);
                $this->logger->error($exception);
                throw $exception;
            }
            // </editor-folder> Connexion

            // <editor-folder> Get host group
            $response = $this->adobeConnectService->getHostGroupPrincipal($configurationHolder);

            if (!$response->isSuccess()) {
                $exception = new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to find the host group', domain: 'validators')]);
                $this->logger->error($exception);
                throw $exception;
            }

            $principalsHostGroup = $response->getData();
            /** @var AdobeConnectPrincipal $principalHostGroup */
            $principalHostGroup = reset($principalsHostGroup);

            $configurationHolder->setHostGroupScoId($principalHostGroup?->getPrincipalIdentifier());
            // </editor-folder> Get host group

            // <editor-folder> Get admins group
            $response = $this->adobeConnectService->getAdminsGroupPrincipal($configurationHolder);

            if (!$response->isSuccess()) {
                $exception = new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to find the admins group', domain: 'validators')]);
                $this->logger->error($exception);
                throw $exception;
            }

            $principalsAdminsGroup = $response->getData();
            /** @var AdobeConnectPrincipal $principalAdminGroup */
            $principalAdminGroup = reset($principalsAdminsGroup);

            $configurationHolder->setAdminGroupScoId($principalAdminGroup?->getPrincipalIdentifier());
            // </editor-folder> Get admins group
        } catch (ProviderGenericException  $exception) {
            $this->logger->error($exception);
            throw new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to connect to this provider', domain: 'validators')]);
        }
    }

    /**
     * @throws ConfigurationHolderViolationsException
     * @throws \Exception
     */
    public function getDefaultData(AbstractProviderConfigurationHolder|AdobeConnectConfigurationHolder $configurationHolder): void
    {
        try {
            // <editor-folder> Get saving sco folder id
            $response = $this->adobeConnectService->getSavingFolderScoId($configurationHolder);

            if (!$response->isSuccess()) {
                $exception = new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to find the place to store sessions', domain: 'validators')]);
                $this->logger->error($exception);
                throw $exception;
            }
            $savingFolderScoId = $response->getData();
            $configurationHolder->setSavingMeetingFolderScoId($savingFolderScoId);
            // </editor-folder> Get saving sco folder id

            // <editor-folder> Connector information
            $response = $this->adobeConnectService->getConnectorInformation($configurationHolder);

            if (!$response->isSuccess()) {
                $exception = new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to find information of connecteur connector', domain: 'validators')]);
                $this->logger->error($exception);
                throw new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to find information of connecteur connector', domain: 'validators')]);
            }

            /** @var AdobeConnectCommonInfo $adobeConnectCommonInfo */
            $adobeConnectCommonInfo = $response->getData();
            $configurationHolder->setUsernameScoId($adobeConnectCommonInfo->getUserId());
            // </editor-folder> Connector information
        } catch (ProviderGenericException  $exception) {
            $exception = new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to connect to this provider', domain: 'validators')]);
            $this->logger->error($exception);
            throw new ConfigurationHolderViolationsException(['global' => $this->translator->trans('Unable to connect to this provider', domain: 'validators')]);
        }
    }

    /**
     * @throws ConfigurationHolderViolationsException
     */
    public function testConnexionAndGetDefaultData(AbstractProviderConfigurationHolder|AdobeConnectConfigurationHolder $configurationHolder): void
    {
        $this->tryToLogin($configurationHolder);
        $this->getDefaultData($configurationHolder);
    }
}
