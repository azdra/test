<?php

namespace App\Service\ConfigurationHolderFormHandler;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\ProviderConfigurationHolderInterface;
use App\Exception\ConfigurationHolderViolationsException;
use App\Exception\UnknownProviderConfigurationHolderException;
use App\Repository\ProviderConfigurationHolderRepository;
use App\Service\MiddlewareService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConfigurationHolderRequestHandler
{
    public function __construct(
        private AdobeConnectConfigurationJsonHandler $adobeConnectConfigurationJsonHandler,
        private AdobeConnectWebinarConfigurationJsonHandler $adobeConnectWebinarConfigurationJsonHandler,
        private CiscoWebexConfigurationJsonHandler $ciscoWebexConfigurationJsonHandler,
        private CiscoWebexRestConfigurationJsonHandler $ciscoWebexRestConfigurationJsonHandler,
        private MicrosoftTeamsConfigurationJsonHandler $microsoftTeamsConfigurationJsonHandler,
        private MicrosoftTeamsEventConfigurationJsonHandler $microsoftTeamsEventConfigurationJsonHandler,
        private WhiteConfigurationJsonHandler $whiteConfigurationJsonHandler,
        private ProviderConfigurationHolderRepository $providerConfigurationHolderRepository,
        private EntityManagerInterface $entityManager,
        private TranslatorInterface $translator,
        private LoggerInterface $logger
    ) {
    }

    public function providerConfigurationJsonHandlerFactory(string $provider): ProviderConfigurationJsonHandlerInterface
    {
        return match ($provider) {
            MiddlewareService::SERVICE_TYPE_ADOBE_CONNECT => $this->adobeConnectConfigurationJsonHandler,
            MiddlewareService::SERVICE_TYPE_CISCO_WEBEX => $this->ciscoWebexConfigurationJsonHandler,
            MiddlewareService::SERVICE_TYPE_CISCO_WEBEX_REST => $this->ciscoWebexRestConfigurationJsonHandler,
            MiddlewareService::SERVICE_TYPE_MICROSOFT_TEAMS => $this->microsoftTeamsConfigurationJsonHandler,
            MiddlewareService::SERVICE_TYPE_MICROSOFT_TEAMS_EVENT => $this->microsoftTeamsEventConfigurationJsonHandler,
            MiddlewareService::SERVICE_TYPE_ADOBE_CONNECT_WEBINAR => $this->adobeConnectWebinarConfigurationJsonHandler,
            MiddlewareService::SERVICE_TYPE_WHITE => $this->whiteConfigurationJsonHandler,
            default => throw new UnknownProviderConfigurationHolderException()
        };
    }

    public function handleRequest(Client $client, Request $request): void
    {
        $configurations = json_decode($request->getContent(), true);

        if (!is_array($configurations) || empty($configurations)) {
            throw new BadRequestException('No rows detected');
        }

        $configurationHolders = [];
        $configurationHolderViolations = [];
        foreach ($configurations as $configuration) {
            if (!$configuration['archived']) {
                try {
                    $handler = $this->providerConfigurationJsonHandlerFactory($configuration['provider']);

                    $violations = $handler->checkViolations($configuration);

                    if (!empty($violations)) {
                        throw new ConfigurationHolderViolationsException($violations);
                    }

                    $configurationHolder = $this->providerConfigurationHolderRepository->find($configuration['id'] ?? 0);
                    if (empty($configurationHolder)) {
                        $configurationHolder = $handler->createConfigurationHolder($configuration);
                        $configurationHolder->setClient($client);
                        $this->entityManager->persist($configurationHolder);
                    }

                    $handler->updateConfigurationHolder($configurationHolder, $configuration);

                    $handler->testConnexionAndGetDefaultData($configurationHolder);

                    $configurationHolders[$configuration['id'] ?? $configuration['identifier']] = $configurationHolder;
                } catch (UnknownProviderConfigurationHolderException $exception) {
                    $this->logger->error($exception);
                    $configurationHolderViolations[$configuration['identifier']] = ['global' => $this->translator->trans('The provider configuration was wrong.', domain: 'validators')];
                } catch (ConfigurationHolderViolationsException $exception) {
                    $this->logger->error($exception);
                    $configurationHolderViolations[$configuration['identifier']] = $exception->violations;
                } catch (\Exception $exception) {
                    $this->logger->error($exception, [
                        'clientId' => $client->getId(),
                        'provider' => $configuration['provider'] ?? null,
                        'configurationHolderId' => $configuration['id'] ?? null,
                        'errorClass' => get_class($exception),
                        ]);
                    $configurationHolderViolations[$configuration['identifier']] = ['global' => $this->translator->trans('An unknown error occurred while configuring the connector.')];
                }
            }
        }

        if (!empty($configurationHolderViolations)) {
            throw new ConfigurationHolderViolationsException($configurationHolderViolations);
        }

        $this->purgeProviderConfigurationHolderForClient($client, $configurationHolders);
        $this->entityManager->flush();
    }

    public function purgeProviderConfigurationHolderForClient(Client $client, array $configurationHolderToKeep): void
    {
        /** @var ProviderConfigurationHolderInterface $configurationHolder */
        foreach ($client->getConfigurationHolders() as $configurationHolder) {
            if (!array_key_exists($configurationHolder->getId(), $configurationHolderToKeep) && !$configurationHolder->isArchived()) {
                $this->entityManager->remove($configurationHolder);
                $client->getConfigurationHolders()->removeElement($configurationHolder);
            }
        }
    }
}
