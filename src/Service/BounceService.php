<?php

namespace App\Service;

use App\Entity\Bounce;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Event\BrevoMessageEvents;
use App\Event\IDFuseMessageEvents;
use App\Event\MandrillMessageEvents;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Contracts\Translation\TranslatorInterface;

class BounceService
{
    public const REASON_REJECTED = 'rejected';
    public const REASON_DEFERRED = 'deferred';
    public const REASON_SENT = 'sent';
    public const REASON_GENERAL = 'general';
    public const REASON_BAD_MAILBOX = 'bad_mailbox';
    public const REASON_INVALID_DOMAIN = 'invalid_domain';
    public const REASON_MAILBOX_FULL = 'mailbox_full';

    public const IDFUSE_REASON_REJECTED = 'rejected';

    public const BREVO_REASON_REJECTED = 'rejected';

    public function __construct(
        private UserRepository $userRepository,
        private ProviderSessionRepository $sessionRepository,
        private MailerService $mailerService,
        private TranslatorInterface $translator,
        private LoggerInterface $logger,
        private string $idfuseApiToken,
        private string $idfuseEndPointEnv,
        private string $brevoMailerApiKey,
    ) {
    }

    public function createBounce(string $name, array $messageDatas): Bounce
    {
        //dd($messageDatas);
        if (!empty($messageDatas['metadata']) &&
            !empty($messageDatas['metadata']['user'])
        ) {
            $user = $this->userRepository->find($messageDatas['metadata']['user']);
        }

        if (empty($user)) {
            throw new UserNotFoundException('User return by Mandrill Webhook was not found. No bounce created.');
        }

        if (!empty($messageDatas['metadata']['session'])) {
            $session = $this->sessionRepository->find($messageDatas['metadata']['session']);
        }

        $bounce = new Bounce();

        switch ($name) {
            case MandrillMessageEvents::HARD_BOUNCE:
            case MandrillMessageEvents::SOFT_BOUNCE:
                $subject = $messageDatas['diag'] ?? '';
                $reason = $messageDatas['bounce_description'] ?? '';
                break;
            case MandrillMessageEvents::SPAM:
                $subject = '';
                $reason = $messageDatas['state'] ?? '__spam';
                break;
            case MandrillMessageEvents::DELAYED:
                $subject = end($messageDatas['smtp_events'])['diag'];
                $reason = $messageDatas['state'] ?? '__deffered';
                break;
            case MandrillMessageEvents::REJECTED:
                $subject = '';
                $reason = $messageDatas['state'] ?? '__rejected';
                break;
            case MandrillMessageEvents::WHITELIST:
                $subject = '';
                $reason = $messageDatas['state'] ?? '__whitelist';
                break;
            case MandrillMessageEvents::BLACKLIST:
                $subject = '';
                $reason = $messageDatas['state'] ?? '__blaklist';
                break;
            case IDFuseMessageEvents::HARD_BOUNCE:
                $subject = $messageDatas['diag'] ?? '';
                $reason = $messageDatas['reason'] ?? $this::IDFUSE_REASON_REJECTED;
                break;
            case IDFuseMessageEvents::SOFT_BOUNCE:
                $subject = $messageDatas['diag'] ?? '';
                $reason = $messageDatas['reason'] ?? $this::IDFUSE_REASON_REJECTED;
                break;
            case BrevoMessageEvents::HARD_BOUNCE:
                $subject = $messageDatas['subject'] ?? '';
                $reason = $messageDatas['reason'] ?? $this::BREVO_REASON_REJECTED;
                break;
            case BrevoMessageEvents::SOFT_BOUNCE:
                $subject = $messageDatas['subject'] ?? '';
                $reason = $messageDatas['reason'] ?? $this::BREVO_REASON_REJECTED;
                break;
            default:
                $subject = '';
                $reason = $messageDatas['state'] ?? '';
                break;
        }

        $bounce
            ->setCreatedAt(new \DateTime())
            ->setDateStatus(new \DateTime())
            ->setEmail($messageDatas['email'] ?? '')
            ->setSubject($subject)
            ->setSession($session ?? null)
            ->setReason(
                $this->transcriptReason($reason)
            )
            ->setMandrillEvent($name)
            ->setMailOrigin($messageDatas['metadata']['origin'] ?? '');

        if (!empty($user)) {
            $bounce->setUser($user);
        }
        if (array_key_exists('ts', $messageDatas)) {
            $bounce->setDateSend((new \DateTimeImmutable())->setTimestamp($messageDatas['ts']));
        } else {
            $bounce->setDateSend($messageDatas['date_send']);
        }
        //dd($bounce);
        if (!empty($session) && $messageDatas['email'] !== null) {
            $this->sendEmailForInvalidMail($session, $subject, $messageDatas['email'], $user);
        }
        $this->logger->info('Bounce added for : '.($messageDatas['email'] ?? '').' reason : '.$reason);

        return $bounce;
    }

    public function sendEmailForInvalidMail(ProviderSession $session, string $subject, string $emailInvalid, User $user): void
    {
        $usersManager = $session->getClient()->getManagerUsers();
        foreach ($usersManager as $userManager) {
            if ($userManager->getEmail() == $emailInvalid) {
                return;
            }
        }

        foreach ($usersManager as $userManager) {
            if (isset($userManager->getPreferences()['EmailInvalidNotification']) && $userManager->getPreferences()['EmailInvalidNotification'] && $userManager->getEmail() !== $emailInvalid) {
                $this->mailerService->sendTemplatedMail(
                    $userManager->getEmail(),
                    $this->translator->trans('The email "{EMAIL}" is not valid', [
                        '{EMAIL}' => $emailInvalid,
                    ]),
                    'emails/alert/email_invalid_bounce.html.twig',
                    context: [
                        'session' => $session,
                        'user' => $userManager,
                        'emailInvalid' => $emailInvalid,
                        'objectInvalid' => $subject,
                        'client' => $session->getClient(),
                        'userBounce' => $user,
                    ],
                    metadatas: [
                        'origin' => MailerService::ORIGIN_EMAIL_INVALID,
                        'user' => $userManager->getId(),
                        'session' => $session->getId(),
                    ],
                    senderSystem: true);
            }
        }
    }

    public function transcriptReason(string $reason): string
    {
        return match ($reason) {
            self::REASON_REJECTED => 'Rejected',
            self::REASON_DEFERRED, self::REASON_SENT => 'Deferred',
            self::REASON_GENERAL => 'Temporary error',
            self::REASON_BAD_MAILBOX => 'Email error',
            self::REASON_INVALID_DOMAIN => 'Invalid domain',
            self::REASON_MAILBOX_FULL => 'Box full',
            self::IDFUSE_REASON_REJECTED => 'Rejected',
            self::BREVO_REASON_REJECTED => 'Rejected',
            default => $reason,
        };
    }

    public function callBrevoAPICampaignReportBounceList(array $params): ?array
    {
        /*$api_token = $this->idfuseApiToken; //'9a8d7a8faddb26cb31d877828e207f1a6d3e8ea5810b7594167dc8110725496c';
        $url = 'api/campaign/report/bounce/list/'.$params['campaign_id'].'?api_token='.$api_token;

        return $this->callIDFuseAPI([], $url, 'GET');*/
        return [];
    }

    public function callIDFuseAPITotals(array $params): ?array
    {
        //$url = 'api/report/subscribers/action?api_token={{api_token}}&date_start={{date_start}}&date_end={{date_end}}&campaign_ids={{?campaign_ids}}&message_ids={{?message_ids}}&list_ids={{?list_ids}}&subscriber_id={{?subsriber_id}}&actions%5B{{action}}%5D={{action}}';
        $api_token = $this->idfuseApiToken; //'9a8d7a8faddb26cb31d877828e207f1a6d3e8ea5810b7594167dc8110725496c';
        $url = 'api/report/subscribers/action?api_token='.$api_token;
        $campaign_ids_query = http_build_query(['campaign_ids' => $params['campaign_ids']]);
        $date_start = $campaign_ids_query = http_build_query(['date_start' => $params['date_start']]);
        $date_end = $campaign_ids_query = http_build_query(['date_end' => $params['date_end']]);
        $url .= '&'.$campaign_ids_query.'&'.$date_start.'&'.$date_end;

        return $this->callIDFuseAPI($params, $url, 'GET');
    }

    public function callIDFuseAPICampaignReportBouceList(array $params): ?array
    {
        $api_token = $this->idfuseApiToken; //'9a8d7a8faddb26cb31d877828e207f1a6d3e8ea5810b7594167dc8110725496c';
        $url = 'api/campaign/report/bounce/list/'.$params['campaign_id'].'?api_token='.$api_token;

        return $this->callIDFuseAPI([], $url, 'GET');
    }

    public function callIDFuseAPIOpen(array $params): ?array
    {
        $api_token = $this->idfuseApiToken; //'9a8d7a8faddb26cb31d877828e207f1a6d3e8ea5810b7594167dc8110725496c';
        $url = 'api/campaign/report/open/total/'.$params['campaign_id'].'?api_token='.$api_token;

        return $this->callIDFuseAPI([], $url, 'GET');
    }

    public function callIDFuseAPIUnopen(array $params): ?array
    {
        $api_token = $this->idfuseApiToken; //'9a8d7a8faddb26cb31d877828e207f1a6d3e8ea5810b7594167dc8110725496c';
        $url = 'api/campaign/report/unopen/list/'.$params['campaign_id'].'?api_token='.$api_token;

        return $this->callIDFuseAPI([], $url, 'GET');
    }

    public function callIDFuseAPIBounces(array $params): ?array
    {
        $api_token = $this->idfuseApiToken; //'9a8d7a8faddb26cb31d877828e207f1a6d3e8ea5810b7594167dc8110725496c';
        $url = 'api/campaign/report/bounce/total/'.$params['campaign_id'].'?api_token='.$api_token;

        return $this->callIDFuseAPI([], $url, 'GET');
    }

    public function callIDFuseAPI(array $params, string $url, ?string $type = 'GET'): ?array
    {
        /*$api_token = $this->idfuseApiToken; //'9a8d7a8faddb26cb31d877828e207f1a6d3e8ea5810b7594167dc8110725496c';
        dump($api_token);
        $query = '?api_token='.$api_token.'&';
        foreach ($params as $key => $value) {
            $query .= $key.'='.urlencode($value).'&';
        }
        $query = rtrim($query, '& ');
        $url = rtrim($url, '/ ');
        //$api = 'https://app.idfuse.fr/'.$url.$query;
        $api = $this->idfuseEndPointEnv.$url.$query;*/
        $api = $this->idfuseEndPointEnv.$url;
        //dump($api);
        $request = curl_init($api); // initiate curl object
        //curl_setopt($request, CURLOPT_HTTPHEADER, ['Accept: application/json']); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_ENCODING, '');
        curl_setopt($request, CURLOPT_MAXREDIRS, 10);
        curl_setopt($request, CURLOPT_TIMEOUT, 0);
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        if ($type == 'GET') {
            curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'GET');
        }
        $response = (string) curl_exec($request); // execute curl post and store results in $response
        curl_close($request); // close curl object
        if (!$response) {
            echo 'Nothing was returned.'."\n"."\n";
        }
        $result = json_decode($response, true);

        return $result;
    }
}
