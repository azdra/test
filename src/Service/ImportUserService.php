<?php

namespace App\Service;

use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Repository\AbstractProviderConfigurationHolderRepository;
use App\Repository\ClientRepository;
use App\Service\AsyncWorker\AsyncTaskService;
use App\Service\AsyncWorker\Tasks\ActionsFromImportUserTask;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamUserImportDataReader;
use App\Service\Provider\Common\UserImportDataReader;
use App\Service\Provider\Model\ImportDataReader;
use App\Service\RessourceUploader\RessourceUploader;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class ImportUserService extends AbstractImportService
{
    public const AUTHORIZED_EXTENSION = ['xls', 'xlsx', 'csv', 'ods'];
    public const MAX_FILE_SIZE = 4000000;
    public const ALLOWED_MAX_ROW = 2000;

    public function __construct(
        AsyncTaskService $asyncTaskService,
        string $importsSavefilesDirectory,
        Security $security,
        ClientRepository $clientRepository,
        AbstractProviderConfigurationHolderRepository $configurationHolderRepository,
        TranslatorInterface $translator,
        RessourceUploader $ressourceUploader,
        private UserImportDataReader $userImportDataReader,
        private IfcamUserImportDataReader $ifcamUserImportDataReader
    ) {
        parent::__construct($asyncTaskService, $importsSavefilesDirectory, $security, $clientRepository, $configurationHolderRepository, $translator, $ressourceUploader);
    }

    protected function getDataReader(AbstractProviderConfigurationHolder $configurationHolder): ImportDataReader
    {
        if ($configurationHolder->getClient()->getId() == Client::ID_CLIENT_IFCAM) {
            return $this->ifcamUserImportDataReader;
        }

        return $this->userImportDataReader;
    }

    protected function getAllowedExtensions(): array
    {
        return self::AUTHORIZED_EXTENSION;
    }

    protected function getMaxFileSize(): int
    {
        return self::MAX_FILE_SIZE;
    }

    protected function getRowLimit(): int
    {
        return self::ALLOWED_MAX_ROW;
    }

    protected function getFlagOrigin(): string
    {
        return 'ImportUsers_'.$this->asyncTaskService->randomFlag(20);
    }

    protected function getAsyncTaskType(): string
    {
        return ActionsFromImportUserTask::class;
    }
}
