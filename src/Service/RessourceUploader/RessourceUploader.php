<?php

namespace App\Service\RessourceUploader;

use App\Exception\UploadFile\UploadFileNoFoundInRequestException;
use App\Exception\UploadFile\UploadFileNotAllowedExtException;
use App\Exception\UploadFile\UploadFileOverSizeException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RessourceUploader
{
    private SluggerInterface $slugger;

    private TranslatorInterface $translator;

    private string $clientConvocationsImagesDirectory;

    private string $clientConvocationsImagesPathweb;

    public function __construct(SluggerInterface $slugger, TranslatorInterface $translator, $clientConvocationsImagesDirectory, $clientConvocationsImagesPathweb)
    {
        $this->slugger = $slugger;
        $this->translator = $translator;
        $this->clientConvocationsImagesDirectory = $clientConvocationsImagesDirectory;
        $this->clientConvocationsImagesPathweb = $clientConvocationsImagesPathweb;
    }

    /**
     * @throws UploadFileNotAllowedExtException
     * @throws UploadFileOverSizeException|UploadFileNoFoundInRequestException
     */
    public function isFileUploadedValid(?UploadedFile $file, array $fileExtensionsAllowed, int $fileMaxsize): string
    {
        if (empty($file)) {
            throw new UploadFileNoFoundInRequestException($this->translator->trans('The file does not respect the expected format. Allowed extension : %allowed%. Maximum size : %size%.', ['%allowed%' => implode(', ', $fileExtensionsAllowed), '%size%' => self::humanReadableFileSize($fileMaxsize)]));
        }

        $fileExtension = strtolower($file->getClientOriginalExtension());
        if (!in_array($fileExtension, $fileExtensionsAllowed)) {
            throw new UploadFileNotAllowedExtException($this->translator->trans('This file has an incorrect extension: %current% (allowed: %allowed%)', ['%current%' => $fileExtension, '%allowed%' => implode(', ', $fileExtensionsAllowed)]));
        }

        if ($file->getSize() > $fileMaxsize) {
            throw new UploadFileOverSizeException($this->translator->trans('The file exceeds the maximum size allowed (%size%)', ['%size%' => self::humanReadableFileSize($fileMaxsize)]));
        }

        return $fileExtension;
    }

    /**
     * @throws FileException
     */
    public function upload(UploadedFile $file, string $target): ?string
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $newFilename = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        $file->move(
            $target,
            $newFilename
        );

        return $newFilename;
    }

    public static function humanReadableFileSize(int $bytes): string
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        $power = $bytes > 0 ? (int) floor(log($bytes, 1000)) : 0;

        return number_format($bytes / pow(1000, $power), 2, '.', ',').' '.$units[$power];
    }

    public function uploadImage(UploadedFile $uploadedFile): File
    {
        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();
        $destinationFolder = $this->clientConvocationsImagesDirectory;
        $path = $uploadedFile->move($destinationFolder, $fileName);

        return $path;
    }

    public function listImagesConvocation(): array
    {
        $list = [];

        $finder = new Finder();
        $finder->files()->in($this->clientConvocationsImagesDirectory);

        foreach ($finder as $file) {
            $fileNameWithExtension = $file->getBasename();
            $list[] = [
                'title' => $fileNameWithExtension,
                'value' => $this->clientConvocationsImagesPathweb.$fileNameWithExtension,
            ];
        }

        return $list;
    }
}
