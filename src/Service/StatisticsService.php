<?php

namespace App\Service;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\ReplayCategory;
use App\Entity\VideoReplay;
use App\Entity\VideoReplayViewer;
use Doctrine\ORM\EntityManagerInterface;

class StatisticsService
{
    public const EVOLUTION_MODE_BY_MONTH = 0;
    public const EVOLUTION_MODE_BY_DAY = 1;

    public const CATALOG_SOURCE = 'catalog';
    public const REGISTRATION_PAGE_SOURCE = 'registrationPage';

    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function getStatisticsSessionsForPeriod(\DateTime $dateStart, \DateTime $dateEnd, array $sessions): array
    {
        /** @var ProviderSession[] $sessions */
        $sessionStatistics = $this->initSessionStatistics();
        $participantOnFinishedSessionStatistics = $this->initParticipantStatistics();
        $evolutionMode = $dateEnd->diff($dateStart)->days > 31 ? self::EVOLUTION_MODE_BY_MONTH : self::EVOLUTION_MODE_BY_DAY;
        $evolutionStatistics = $this->initEvolutionStatistics($evolutionMode, clone $dateStart, $dateEnd);

        $sessionStatistics['total']['count'] = count($sessions);
        foreach ($sessions as $session) {
            $this->calculateSessionStatistics($sessionStatistics, $session);
            $this->calculateStatisticsForParticipantsOnFinishedSession($participantOnFinishedSessionStatistics, $session);
            $this->calculateEvolutionStatistics($evolutionMode, $evolutionStatistics, $session);
        }

        return ['sessions' => $sessionStatistics, 'participants' => $participantOnFinishedSessionStatistics, 'evolution' => $evolutionStatistics];
    }

    /**
     * @throws \Exception
     */
    public function getStatisticsReplayForPeriod(array $sessions, array $videoReplays): array
    {
        $allStatisticsResult = [
            'allViewer' => 0,
            'allViewerUnique' => 0,
            'replayCategory' => [],
            'videos' => [],
        ];

        foreach ($sessions as $session) {
            if ($session->isPublishedReplay()) {
                $infosVideo = [
                    'titreVideo' => $session->getName(),
                    'dateVideo' => ($session->getDateStart())->setTimezone(new \DateTimeZone($session->getClient()->getTimeZone()))->format('d/m/Y H:i:s'),
                    'source' => $this::REGISTRATION_PAGE_SOURCE,
                    'modalName' => $session->getRegistrationPageTemplate()->getName(),
                    'id' => $session->getId(),
                    'clientSlug' => $session->getClient()->getSlug(),
                    'domain' => '',
                ];
                $allStatisticsResult['videos'] = $this->updateVideosStatistics(videos: $allStatisticsResult['videos'], keyVideo: 'registration_page_'.$session->getId(), infosVideo: $infosVideo, viewerSeen: 0);
                foreach ($session->getReplays() as $viewerReplaySession) {
                    $allStatisticsResult['videos'] = $this->updateVideosStatistics(videos: $allStatisticsResult['videos'], keyVideo: 'registration_page_'.$session->getId(), infosVideo: $infosVideo, viewerSeen: $viewerReplaySession->getSeen());
                    $allStatisticsResult['allViewer'] += $viewerReplaySession->getSeen();
                    ++$allStatisticsResult['allViewerUnique'];
                }
            }
        }

        foreach ($videoReplays as $videoReplay) {
            $infosVideo = [
                'titreVideo' => $videoReplay->getTitle(),
                'dateVideo' => ($videoReplay->getPublishedDate())->setTimezone(new \DateTimeZone($videoReplay->getModule()->getClient()->getTimeZone()))->format('d/m/Y H:i:s'),
                'source' => $this::CATALOG_SOURCE,
                'modalName' => $videoReplay->getModule()->getName(),
                'id' => $videoReplay->getId(),
                'clientSlug' => $videoReplay->getModule()->getClient()->getSlug(),
                'domain' => $videoReplay->getModule()->getDomain(),
            ];
            $allStatisticsResult['videos'] = $this->updateVideosStatistics(videos: $allStatisticsResult['videos'], keyVideo: 'catalog_'.$videoReplay->getId(), infosVideo: $infosVideo, viewerSeen: 0);
            foreach ($videoReplay->getVideosReplaysViewers() as $viewer) {
                $allStatisticsResult['videos'] = $this->updateVideosStatistics(videos: $allStatisticsResult['videos'], keyVideo: 'catalog_'.$videoReplay->getId(), infosVideo: $infosVideo, viewerSeen: $viewer->getSeen());
                $allStatisticsResult['replayCategory'] = $this->updateReplayCategoryStatistics($allStatisticsResult['replayCategory'], $videoReplay, $viewer->getSeen());
                $allStatisticsResult['allViewer'] += $viewer->getSeen();
                ++$allStatisticsResult['allViewerUnique'];
            }
        }

        return $allStatisticsResult;
    }

    private function calculateSessionStatistics(array &$sessionStatistics, ProviderSession $session): void
    {
        $sessionStatistics['total']['duration'] += $session->getDuration();
        ++$sessionStatistics['byState'][$session->getReadableState()];

        if (empty($session->getCategory())) {
            ++$sessionStatistics['byCategory']['Unkonwn'];
        } else {
            ++$sessionStatistics['byCategory'][$session->getCategory()->label()];
        }
    }

    private function calculateStatisticsForParticipantsOnFinishedSession(array &$participantOnFinishedSessionStatistics, ProviderSession $session): void
    {
        if ($session->getReadableState() !== ProviderSession::FINAL_STATUS_FINISHED) {
            return;
        }

        $currentCategory = $session->getCategory()->label();
        if (!array_key_exists($currentCategory, $participantOnFinishedSessionStatistics['byCategory'])) {
            $participantOnFinishedSessionStatistics['byCategory'][$currentCategory] = ['count' => 0, 'test' => 0, 'attendees' => 0, 'absences' => 0, 'max' => 0, 'finishedSession' => 0];
        }

        if ($session->isFinished()) {
            ++$participantOnFinishedSessionStatistics['byCategory'][$currentCategory]['finishedSession'];
        }

        $countParticipant = $session->getParticipantRoles()->count();

        $participantOnFinishedSessionStatistics['total']['count'] += $countParticipant;
        if ($countParticipant > $participantOnFinishedSessionStatistics['total']['max']) {
            $participantOnFinishedSessionStatistics['total']['max'] = $countParticipant;
        }

        $participantOnFinishedSessionStatistics['byCategory'][$currentCategory]['count'] += $countParticipant;
        if ($countParticipant > $participantOnFinishedSessionStatistics['byCategory'][$currentCategory]['max']) {
            $participantOnFinishedSessionStatistics['byCategory'][$currentCategory]['max'] = $countParticipant;
        }

        /** @var ProviderParticipantSessionRole $participantRole */
        foreach ($session->getParticipantRoles() as $participantRole) {
            if ($participantRole->getPresenceStatus()) {
                ++$participantOnFinishedSessionStatistics['total']['attendees'];
                ++$participantOnFinishedSessionStatistics['byCategory'][$currentCategory]['attendees'];
            } else {
                ++$participantOnFinishedSessionStatistics['total']['absences'];
                ++$participantOnFinishedSessionStatistics['byCategory'][$currentCategory]['absences'];
            }
        }
    }

    private function calculateEvolutionStatistics(int $mode, array &$evolutionStatistics, ProviderSession $session): void
    {
        if ($mode === self::EVOLUTION_MODE_BY_MONTH) {
            $label = $session->getDateStart()->format('m/y');
        } else {
            $label = $session->getDateStart()->format('d/m');
        }
        if (!array_key_exists($label, $evolutionStatistics['participants']) || !array_key_exists($label, $evolutionStatistics['sessions'])) {
            $this->initialEvaluationStatistics($evolutionStatistics, $label);
        }
        ++$evolutionStatistics['sessions'][$label];
        $evolutionStatistics['participants'][$label] += $session->getParticipantRoles()->count();
    }

    private function initSessionStatistics(): array
    {
        return [
            'total' => ['duration' => 0, 'count' => 0],
            'byState' => [
                ProviderSession::FINAL_STATUS_SCHEDULED => 0,
                ProviderSession::FINAL_STATUS_SENT => 0,
                ProviderSession::FINAL_STATUS_RUNNING => 0,
                ProviderSession::FINAL_STATUS_FINISHED => 0,
                ProviderSession::FINAL_STATUS_DRAFT => 0,
                ProviderSession::FINAL_STATUS_CANCELED => 0,
            ],
            'byCategory' => [
                ProviderSession::FINAL_CATEGORY_FORMATION => 0,
                ProviderSession::FINAL_CATEGORY_MEETING => 0,
                ProviderSession::FINAL_CATEGORY_FACE_TO_FACE => 0,
                ProviderSession::FINAL_CATEGORY_MIXTE => 0,
                ProviderSession::FINAL_CATEGORY_WEBINAR => 0,
                ProviderSession::FINAL_CATEGORY_TEST => 0,
                ProviderSession::FINAL_CATEGORY_UNKNOWN => 0,
            ],
        ];
    }

    private function initParticipantStatistics(): array
    {
        return [
            'total' => ['count' => 0, 'test' => 0, 'attendees' => 0, 'absences' => 0, 'max' => 0],
            'byCategory' => [],
        ];
    }

    private function initEvolutionStatistics(int $mode, \DateTime $dateStart, \DateTime $dateEnd): array
    {
        $evolutionStatistics = [
            'participants' => [],
            'sessions' => [],
        ];

        $clonedDateStart = clone $dateStart;
        if ($mode === self::EVOLUTION_MODE_BY_MONTH) {
            $this->fillEvolutionStatistics($evolutionStatistics, $clonedDateStart, $dateEnd, 'm/y', '+1 month');
        } else {
            $this->fillEvolutionStatistics($evolutionStatistics, $clonedDateStart, $dateEnd, 'd/m', '+1 day');
        }

        return $evolutionStatistics;
    }

    private function fillEvolutionStatistics(array &$evolutionStatistics, \DateTime $initialDate, \DateTime $dateEnd, string $format, string $modify): void
    {
        if ($initialDate->format('d') == '31') {
            $initialDate->modify('-1 day');
        }

        while ($initialDate < $dateEnd) {
            $label = $initialDate->format($format);
            $this->initialEvaluationStatistics($evolutionStatistics, $label);
            $initialDate->modify($modify);
        }

        /*
         * initialDate = 2022-06-09
         * dateEnd = 2023-06-04
         *
         * si le jours de initialDate est supérieur au jours de dateEnd et que le mois est le même alors on ajoute une ligne car sinon pas pris en compte
         */
        if ($format != 'd/m' && $initialDate->format('d/m') > $dateEnd->format('d/m')) {
            $label = $dateEnd->format($format);
            $this->initialEvaluationStatistics($evolutionStatistics, $label);
        }
    }

    private function initialEvaluationStatistics(array &$evolutionStatistics, string $label): void
    {
        $evolutionStatistics['participants'][$label] = 0;
        $evolutionStatistics['sessions'][$label] = 0;
    }

    public function isRecentView(VideoReplayViewer $viewerReplay): bool
    {
        $lastShowReplay = $viewerReplay->getLastShowReplay();

        return $lastShowReplay !== null && $lastShowReplay->diff(new \DateTimeImmutable())->i >= 1;
    }

    private function updateReplayCategoryStatistics(array $replayCategory, VideoReplay $videoReplay, int $viewerSeen): array
    {
        foreach ($videoReplay->getCatalogsReplay() as $category) {
            if (!array_key_exists($category, $replayCategory)) {
                $replay = $this->entityManager->getRepository(ReplayCategory::class)->findOneBy(['id' => $category]);
                if ($replay === null) {
                    $replayCategory[$category]['name'] = '';
                } else {
                    $replayCategory[$category]['name'] = $replay->getCategoryName();
                }
                $replayCategory[$category]['viewer'] = 0;
                $replayCategory[$category]['viewerUnique'] = 0;
            }
            $replayCategory[$category]['viewer'] += $viewerSeen;
            ++$replayCategory[$category]['viewerUnique'];
        }

        return $replayCategory;
    }

    private function updateVideosStatistics(array $videos, string $keyVideo, array $infosVideo, int $viewerSeen): array
    {
        if (!array_key_exists($keyVideo, $videos)) {
            $videos[$keyVideo]['name'] = $infosVideo['titreVideo'];
            $videos[$keyVideo]['date'] = $infosVideo['dateVideo'];
            $videos[$keyVideo]['source'] = $infosVideo['source'];
            $videos[$keyVideo]['modalName'] = $infosVideo['modalName'];
            $videos[$keyVideo]['id'] = $infosVideo['id'];
            $videos[$keyVideo]['clientSlug'] = $infosVideo['clientSlug'];
            $videos[$keyVideo]['domain'] = $infosVideo['domain'];
            $videos[$keyVideo]['viewer'] = 0;
            $videos[$keyVideo]['viewerUnique'] = 0;
        } else {
            $videos[$keyVideo]['viewer'] += $viewerSeen;
            ++$videos[$keyVideo]['viewerUnique'];
        }

        return $videos;
    }
}
