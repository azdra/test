<?php

namespace App\Service;

use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderParticipant;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Exception\AbstractProviderException;
use App\Exception\InvalidArgumentException;
use App\Exception\ProviderServiceValidationException;
use App\Model\ProviderResponse;
use App\Repository\AdobeConnectSCORepository;
use App\Repository\AdobeConnectTelephonyProfileRepository;
use App\Repository\UserRepository;
use App\Service\Connector\Adobe\AdobeConnectConnector;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdobeConnectService extends AbstractProviderService
{
    public const REDIS_SHARED_MEETINGS_TEMPLATE_KEY = 'shared_meeting_template_';
    public const REDIS_TIMEOUT_IN_SECONDS = 3600;

    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected ValidatorInterface $validator,
        private AdobeConnectSCORepository $adobeConnectSCORepository,
        AdobeConnectConnector $connector,
        private AdobeConnectTelephonyProfileRepository $adobeConnectTelephonyProfileRepository,
        private \Redis $redisClient,
        private SerializerInterface $serializer,
        private string $appEnvironment,
        LoggerInterface $logger,
        private UserRepository $userRepository
    ) {
        parent::__construct($entityManager, $validator, $connector, $logger);
    }

    public function supportConfigurationHolder(AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        return $configurationHolder instanceof AdobeConnectConfigurationHolder;
    }

    public function createSession(ProviderSession $providerSession): ProviderResponse
    {
        return $this->syncSession($providerSession);
    }

    public function updateSession(ProviderSession $providerSession): ProviderResponse
    {
        return $this->syncSession($providerSession);
    }

    public function syncSession(AdobeConnectSCO $adobeConnectSCO): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($adobeConnectSCO, groups: $adobeConnectSCO->getUniqueScoIdentifier() ? 'update' : 'create'))) {
            throw new ProviderServiceValidationException($list);
        }

        /** @var AdobeConnectConfigurationHolder $adobeConnectConfigurationHolder */
        $adobeConnectConfigurationHolder = $adobeConnectSCO->getAbstractProviderConfigurationHolder();

        try {
            $folderScoId = $this->getOrCreateScoFolder($adobeConnectConfigurationHolder, $adobeConnectConfigurationHolder->getSavingMeetingFolderScoId(), $adobeConnectSCO->getRef());

            $adobeConnectSCO->setParentScoIdentifier($folderScoId);

            $response = $this->connector->call(
                $adobeConnectConfigurationHolder,
                AdobeConnectConnector::ACTION_SCO_UPDATE,
                $adobeConnectSCO
            );
            if ($response->isSuccess() && $this->entityManager->getRepository(AdobeConnectSCO::class)->findOneBy(['uniqueScoIdentifier' => $adobeConnectSCO->getUniqueScoIdentifier()]) === null) {
                $accessResponse = $this->connector->call(
                    $adobeConnectConfigurationHolder,
                    AdobeConnectConnector::ACTION_SCO_UPDATE_PERMISSION,
                    $adobeConnectSCO
                );
                if (!$accessResponse->isSuccess()) {
                    $adobeConnectSCO->setAdmittedSession(AdobeConnectConnector::GRANT_ACCESS_VISITOR);
                }

                $this->setConnectorInSession($adobeConnectSCO);
                $this->entityManager->persist($adobeConnectSCO);
            }

            $this->connector->call(
                $adobeConnectConfigurationHolder,
                AdobeConnectConnector::ACTION_SET_SCO_TELEPHONY_PROFILE,
                $adobeConnectSCO
            );

            return $response;
        } catch (\Throwable $exception) {
            $this->logger->error($exception, [
                'adobeConnectConfigurationHolder ID ' => $adobeConnectConfigurationHolder->getId(),
            ]);

            return (new ProviderResponse(false))
                ->setThrown($exception);
        }
    }

    public function setConnectorInSession(AdobeConnectSCO $adobeConnectSCO): ProviderResponse
    {
        /** @var AdobeConnectConfigurationHolder $configurationHolder */
        $configurationHolder = $adobeConnectSCO->getAbstractProviderConfigurationHolder();

        return $this->connector->call(
            $configurationHolder,
            AdobeConnectConnector::ACTION_PERMISSIONS_UPDATE,
            [
                $adobeConnectSCO,
                (new AdobeConnectPrincipal($configurationHolder))->setPrincipalIdentifier($configurationHolder->getUsernameScoId()),
                AdobeConnectConnector::PERMISSION_VERB_HOST,
            ]
        );
    }

    public function deleteSession(ProviderSession $providerSession): ProviderResponse
    {
        $response = $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            AdobeConnectConnector::ACTION_SCO_DELETE,
            $providerSession
        );

        return $response;
    }

    public function createParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return $this->syncPrincipal($providerParticipant, 'create');
    }

    public function updateParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        return $this->syncPrincipal($providerParticipant, 'update');
    }

    public function syncPrincipal(AdobeConnectPrincipal $adobeConnectPrincipal, string $context): ProviderResponse
    {
        if (0 !== count($list = $this->validator->validate($adobeConnectPrincipal, groups: [$context]))) {
            throw new ProviderServiceValidationException($list);
        }

        $response = $this->connector->call(
            $adobeConnectPrincipal->getConfigurationHolder(),
            AdobeConnectConnector::ACTION_PRINCIPAL_UPDATE,
            $adobeConnectPrincipal
        );
        if ($response->isSuccess() && $this->entityManager->getRepository(AdobeConnectPrincipal::class)->findOneBy(['principalIdentifier' => $adobeConnectPrincipal->getPrincipalIdentifier()]) === null) {
            $this->entityManager->persist($adobeConnectPrincipal);
        }

        return $response;
    }

    public function deleteParticipant(ProviderParticipant $providerParticipant): ProviderResponse
    {
        $response = $this->connector->call(
            $providerParticipant->getConfigurationHolder(),
            AdobeConnectConnector::ACTION_PRINCIPAL_DELETE,
            $providerParticipant
        );

        if ($response->isSuccess()) {
            $this->entityManager->remove($providerParticipant);
        }

        return $response;
    }

    public function updateParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse
    {
        $session = $participantSessionRole->getSession();
        $participant = $participantSessionRole->getParticipant();
        $role = $this->getPermissionIdByRole($participantSessionRole->getRole());

        if (0 !== count($list = $this->validator->validate([$session, $participant], [new Valid()], groups: 'update'))) {
            throw new ProviderServiceValidationException($list);
        }

        return $this->connector->call(
            $session->getAbstractProviderConfigurationHolder(),
            AdobeConnectConnector::ACTION_PERMISSIONS_UPDATE,
            [$session, $participant, $role]
        );
    }

    private function getPermissionIdByRole(string $role): string
    {
        return match ($role) {
            ProviderParticipantSessionRole::ROLE_ANIMATOR => AdobeConnectConnector::PERMISSION_VERB_HOST,
            ProviderParticipantSessionRole::ROLE_TRAINEE => AdobeConnectConnector::PERMISSION_VERB_VIEW,
            ProviderParticipantSessionRole::ROLE_REMOVE => AdobeConnectConnector::PERMISSION_VERB_REMOVE,
            default => AdobeConnectConnector::PERMISSION_VERB_DENIED
        };
    }

    public function redirectParticipantSessionRole(ProviderParticipantSessionRole $participantSessionRole): ProviderResponse
    {
        $providerSession = $participantSessionRole->getSession();
        $providerParticipant = $participantSessionRole->getParticipant();

        return $this->connector->call(
            $providerSession->getAbstractProviderConfigurationHolder(),
            AdobeConnectConnector::ACTION_AUTHENTICATE_PRINCIPAL,
            [$providerSession, $providerParticipant]);
    }

    public function redirectConnector(AdobeConnectSCO $adobeConnectSCO): ProviderResponse
    {
        /** @var AdobeConnectConfigurationHolder $configurationHolder */
        $configurationHolder = $adobeConnectSCO->getAbstractProviderConfigurationHolder();

        return $this->connector->call(
            $configurationHolder,
            AdobeConnectConnector::ACTION_AUTHENTICATE_CONFIGURATION_HOLDER,
            $adobeConnectSCO);
    }

    public function getHostGroupPrincipal(AdobeConnectConfigurationHolder $configurationHolder): ProviderResponse
    {
        $response = $this->connector->call(
            $configurationHolder,
            AdobeConnectConnector::ACTION_PRINCIPAL_LIST,
            [
            true,
            ['filter-type' => AdobeConnectPrincipal::TYPE_LIVE_ADMIN],
            ]
        );

        return $response;
    }

    public function getAdminsGroupPrincipal(AdobeConnectConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            AdobeConnectConnector::ACTION_PRINCIPAL_LIST,
            [
                true,
                ['filter-type' => AdobeConnectPrincipal::TYPE_ADMIN],
            ]
        );
    }

    public function getConnectorInformation(AdobeConnectConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            AdobeConnectConnector::ACTION_COMMON_INFO
        );
    }

    public function getPrincipalInHostGroup(AdobeConnectConfigurationHolder $configurationHolder): ProviderResponse
    {
        if (empty($configurationHolder->getHostGroupScoId())) {
            throw new InvalidArgumentException('The principal ID of the host group wasn\'t find');
        }

        $response = $this->connector->call(
            $configurationHolder,
            AdobeConnectConnector::ACTION_PRINCIPAL_LIST, [
            false,
            [
                'group-id' => $configurationHolder->getHostGroupScoId(),
                'filter-is-member' => 'true',
            ],
        ]);

        $this->synchronizeLicence($configurationHolder, $response->getData());

        return $response;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getPrincipalInAdminsGroup(AdobeConnectConfigurationHolder $configurationHolder): ProviderResponse
    {
        if (empty($configurationHolder->getHostGroupScoId())) {
            throw new InvalidArgumentException('The principal ID of the host group wasn\'t find');
        }

        $response = $this->connector->call(
            $configurationHolder,
            AdobeConnectConnector::ACTION_PRINCIPAL_LIST, [
                false,
                [
                    'group-id' => $configurationHolder->getAdminGroupScoId(),
                    'filter-is-member' => 'true',
                ],
            ]);
        $this->synchronizeLicence($configurationHolder, $response->getData() ?? []);

        return $response;
    }

    public function synchronizeLicence(AdobeConnectConfigurationHolder $configurationHolder, array $licences): void
    {
        $oneAdded = false;
        /** @var AdobeConnectPrincipal $participant */
        foreach ($licences as $participant) {
            $user = $this->userRepository->findOneBy(['email' => $participant->getEmail(), 'client' => $configurationHolder->getClient()]);
            if (!empty($user) && !array_key_exists($user->getEmail(), $configurationHolder->getLicences())) {
                $configurationHolder->addLicence($user->getEmail(), [
                    'name' => $participant->getName(),
                    'password' => '',
                    'shared' => false,
                    'user_id' => $user->getId(),
                ]);
                $oneAdded = true;
            }
        }
        if ($oneAdded) {
            $this->entityManager->flush();
        }
    }

    public function getAnimatorsRoomModel(AdobeConnectConfigurationHolder $configurationHolder): ProviderResponse
    {
        $response = $this->connector->call(
            $configurationHolder,
            AdobeConnectConnector::FUNCTION_GET_LS_TEMPLATE_FOLDER,
        );
        if ($response->isSuccess()) {
            $parameters = [
                'rootFolder' => $response->getData(),
                'filter' => 'folder',
            ];

            $response = $this->connector->call(
                $configurationHolder,
                AdobeConnectConnector::ACTION_SCO_NAME_LIST_FROM_PARENT_SCO,
                $parameters
            );
        }

        return $response;
    }

    public function getRoomModelsFromAnimator(AdobeConnectConfigurationHolder $configurationHolder, ?string $scoidFolder = null): ProviderResponse
    {
        /*$response = $this->connector->call(
            $configurationHolder,
            AdobeConnectConnector::FUNCTION_GET_LS_TEMPLATE_FOLDER,
        );
        if ($response->isSuccess()) {*/
        $parameters = [
                'rootFolder' => $scoidFolder,
                'filter' => 'meeting',
            ];

        $response = $this->connector->call(
                $configurationHolder,
                AdobeConnectConnector::ACTION_SCO_NAME_LIST_FROM_PARENT_SCO,
                $parameters
            );
        //}

        return $response;
    }

    public function findAllModelsRoom(AdobeConnectConfigurationHolder $configurationHolder, ?string $filter = 'meeting'): ProviderResponse
    {
        $response = $this->connector->call(
            $configurationHolder,
            AdobeConnectConnector::FUNCTION_GET_LS_TEMPLATE_FOLDER,
        );
        if ($response->isSuccess()) {
            $parameters = [
                'rootFolder' => $response->getData(),
                'filter' => 'all',
            ];
            $response = $this->connector->call(
                $configurationHolder,

                AdobeConnectConnector::FUNCTION_GET_ALL_MODELS_TYPE_MEETING,
                $parameters
            );

            if ($response->isSuccess()) {
            }
            //dump($response);
        }

        return $response;
    }

    public function getSharedMeetingsTemplate(AdobeConnectConfigurationHolder $configurationHolder, ?string $filter = 'meeting'): ProviderResponse
    {
        $redisKey = self::REDIS_SHARED_MEETINGS_TEMPLATE_KEY.$configurationHolder->getId();

        if ($this->redisClient->exists($redisKey)) {
            $response = $this->serializer->deserialize($this->redisClient->get($redisKey), ProviderResponse::class, 'json');
        } else {
            $response = $this->connector->call(
                $configurationHolder,
                AdobeConnectConnector::FUNCTION_GET_LS_TEMPLATE_FOLDER,
            );
            if ($response->isSuccess()) {
                $parameters = [
                        'rootFolder' => $response->getData(),
                        'filter' => $filter,
                    ];

                $response = $this->connector->call(
                        $configurationHolder,
                        AdobeConnectConnector::ACTION_SCO_NAME_LIST_FROM_PARENT_SCO,
                        $parameters
                    );

                if ($response->isSuccess()) {
                    $this->redisClient->setex($redisKey, self::REDIS_TIMEOUT_IN_SECONDS, $this->serializer->serialize($response, 'json'));
                }
            }
        }

        return $response;
    }

    public function deletePrincipal(AdobeConnectPrincipal $adobeConnectPrincipal): ProviderResponse
    {
        $response = $this->connector->call(
            $adobeConnectPrincipal->getConfigurationHolder(),
            AdobeConnectConnector::ACTION_PRINCIPAL_DELETE,
            $adobeConnectPrincipal
        );

        if ($response->isSuccess()) {
            $this->entityManager->remove($adobeConnectPrincipal);
        }

        return $response;
    }

    public function principalListByLogin(AdobeConnectConfigurationHolder $configurationHolder, string $email): ProviderResponse
    {
        return $this->connector->call(
            $configurationHolder,
            AdobeConnectConnector::ACTION_PRINCIPAL_LIST,
            [
                false,
                ['filter-login' => $email],
            ]
        );
    }

    public function reportMeetingAttendance(AdobeConnectSCO $adobeConnectSCO): ProviderResponse
    {
        $response = $this->connector->call(
            $adobeConnectSCO->getAbstractProviderConfigurationHolder(),
            AdobeConnectConnector::ACTION_REPORT_MEETING_ATTENDANCE,
            $adobeConnectSCO
        );

        return $response;
    }

    public function reportActiveMeetings(): array
    {
        return $this->adobeConnectSCORepository->getActiveMeetings();
    }

    public function getAllAudiosToCreate(AdobeConnectConfigurationHolder $configurationHolder): ProviderResponse
    {
        return $this->connector->call($configurationHolder, AdobeConnectConnector::ACTION_TELEPHONY_PROFILE_LIST);
    }

    public function checkProviderAudioAvailabilityForDate(AdobeConnectSCO $providerSession, $start, $end): bool
    {
        return $this->adobeConnectSCORepository->checkProviderAudioAvailabilityForDate($providerSession, $start, $end);
    }

    public function getAvailableAudios(DateTime $dateBegin, DateTime $dateEnd, string $type, string $lang, AdobeConnectConfigurationHolder $configurationHolder): array
    {
        $freeAudios = [];

        if ($type == AbstractProviderAudio::AUDIO_VOIP || $type == null) {
            return $freeAudios;
        }

        $audios = $this->adobeConnectTelephonyProfileRepository->findBy([
            'phoneLang' => $lang,
            'connectionType' => $type,
            'configurationHolder' => $configurationHolder,
        ]);

        $sessions = $this->adobeConnectSCORepository->getSCOBetweenDateForAudioFromAdobeConnect($dateBegin, $dateEnd, $type);

        foreach ($audios as $audio) {
            $free = true;
            $audioProfileIdentifier = $audio?->getProfileIdentifier();

            if (!is_null($audioProfileIdentifier)) {
                foreach ($sessions as $session) {
                    $sessionProfileIdentifier = $session?->getProviderAudio()?->getProfileIdentifier();
                    if (!is_null($sessionProfileIdentifier)) {
                        if ($audioProfileIdentifier == $sessionProfileIdentifier) {
                            $free = false;
                            break;
                        }
                    }
                }
            }
            if ($free) {
                $freeAudios[] = $audio;
            }
        }

        return $freeAudios;
    }

    /**
     * @throws \Exception
     */
    public function getSavingFolderScoId(AdobeConnectConfigurationHolder $configurationHolder): ProviderResponse
    {
        try {
            $response = $this->connector->call(
                $configurationHolder,
                AdobeConnectConnector::ACTION_SCO_ROOT_FOLDER,
                AdobeConnectConnector::SCO_TYPE_MEETINGS
            );

            if (!$response->isSuccess()) {
                $exception = $response->getThrown() ?? new \Exception('Unknown error return while saving folder sco id');
                $this->logger->error($exception);
                throw $response->getThrown() ?? new \Exception('Unknown error return while saving folder sco id');
            }

            $rootScoId = $response->getData();
            $parentScoId = $rootScoId;

            foreach (AdobeConnectConnector::ROOT_PATH_SAVE_MEETING as $folderName) {
                $currentScoId = $this->getOrCreateScoFolder($configurationHolder, $parentScoId, $folderName);
                $parentScoId = $currentScoId;
            }

            return new ProviderResponse(data: $this->getOrCreateScoFolder($configurationHolder, $parentScoId, $this->appEnvironment));
        } catch (AbstractProviderException $exception) {
            $this->logger->error($exception);

            return (new ProviderResponse(false))
                ->setThrown($exception);
        }
    }

    /**
     * @throws AbstractProviderException|\Exception
     */
    public function getOrCreateScoFolder(AdobeConnectConfigurationHolder $configurationHolder, int $parentFolderID, string $name): int
    {
        $response = $this->connector->call($configurationHolder, AdobeConnectConnector::ACTION_SCO_FOLDER_EXIST, [$parentFolderID, $name]);

        if (!$response->isSuccess()) {
            $exception = $response->getThrown() ?? new \Exception('Unknown error return while finding folder');
            $this->logger->error($exception);
            throw $response->getThrown() ?? new \Exception('Unknown error return while finding folder');
        }

        $folderFound = $response->getData();

        if (!is_null($folderFound)) {
            return $folderFound;
        }

        $response = $this->connector->call($configurationHolder, AdobeConnectConnector::ACTION_SCO_CREATE_FOLDER, [$parentFolderID, $name]);

        if (!$response->isSuccess()) {
            $exception = $response->getThrown() ?? new \Exception('Unknown error return while creating folder');
            $this->logger->error($exception);
            throw $response->getThrown() ?? new \Exception('Unknown error return while creating folder');
        }

        return $response->getData();
    }

    /**
     * @throws InvalidArgumentException
     */
    public function isAllowToUpdatePassword(AdobeConnectPrincipal $adobeConnectPrincipal, AdobeConnectConfigurationHolder $configurationHolder): bool
    {
        $hostGroup = $this->getPrincipalInHostGroup($configurationHolder)->getData() ?? [];
        $adminsGroup = $this->getPrincipalInAdminsGroup($configurationHolder)->getData() ?? [];

        return in_array($adobeConnectPrincipal, array_merge($hostGroup, $adminsGroup));
    }
}
