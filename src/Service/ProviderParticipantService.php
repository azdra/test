<?php

namespace App\Service;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSessionAccessToken;
use App\Model\GeneratorSessionTrait;

class ProviderParticipantService
{
    use GeneratorSessionTrait;

    private string $appSecret;

    public function __construct(string $appSecret)
    {
        $this->appSecret = $appSecret;
    }

    public function setProviderSessionAccessToken(ProviderParticipantSessionRole $participantSessionRole): ProviderSessionAccessToken
    {
        return $this->encryptSessionAccessToken($this->appSecret, $participantSessionRole);
    }
}
