<?php

namespace App\Service;

use App\Entity\Client\Client;
use App\Entity\DeferredActionsTask;
use App\Model\GeneratorTrait;
use App\Service\AsyncWorker\TaskInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final class DeferredActionsTaskService
{
    use GeneratorTrait;

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function publish(): DeferredActionsTask
    {
        $data = [];
        $infos = [];
        $task = (new DeferredActionsTask())
            ->setClassName('Evaluation')
            ->setData($data)
            ->setStatus(DeferredActionsTask::STATUS_READY)
            ->setInfos($infos);

        return $task;
    }

    /*public function publish(string $taskClassName, array $data = [], ?string $flagOrigin = null, ?Client $clientOrigin = null, ?string $fileOrigin = null, ?UserInterface $userOrigin = null, array $infos = []): AsyncTask
    {
        $task = (new AsyncTask())
            ->setClassName($taskClassName)
            ->setData($data)
            ->setStatus(TaskInterface::STATUS_READY)
            ->setInfos($infos);

        if (!empty($flagOrigin)) {
            $task->setFlagOrigin($flagOrigin);
        }

        if (!empty($clientOrigin)) {
            $task->setClientOrigin($clientOrigin);
        }

        if (!empty($fileOrigin)) {
            $task->setFileOrigin($fileOrigin);
        }

        if (!empty($userOrigin)) {
            $task->setUserOrigin($userOrigin);
        }

        $this->entityManager->persist($task);

        return $task;
    }*/
}
