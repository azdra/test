<?php

namespace App\Service;

use App\Entity\Client\Client;
use App\Entity\User;
use App\Model\ActivityLog\ActivityLogContext;
use App\Model\ActivityLog\ActivityLogModel;
use Doctrine\ORM\EntityManagerInterface;

class ActivityLogger
{
    private ActivityLogContext $activityLogContext;

    public function __construct(
        private ActivityLogService $activityLogService,
        private EntityManagerInterface $entityManager,
        private array $activityLogModels = []
    ) {
    }

    public function isActivityLogContextInitiated(): bool
    {
        return isset($this->activityLogContext);
    }

    public function initActivityLogContext(Client $client, ?User $user, string $origin, array $infos = []): void
    {
        $this->activityLogContext = new ActivityLogContext($client, $user, $origin, $infos);
    }

    public function addDataToActivityLogContext(string $key, mixed $data): void
    {
        $this->activityLogContext->infos[$key] = $data;
    }

    public function clearLogModels(): void
    {
        $this->activityLogModels = [];
    }

    public function addActivityLog(ActivityLogModel $activityLogModel, bool $asFirst = false): void
    {
        if ($asFirst) {
            $activityLogModelsCopy = $this->activityLogModels;
            array_unshift($activityLogModelsCopy, $activityLogModel);
            $this->activityLogModels = $activityLogModelsCopy;
        } else {
            $this->activityLogModels[] = $activityLogModel;
        }
    }

    public function flushActivityLogs(): void
    {
        foreach ($this->activityLogModels as $activityLog) {
            $this->activityLogService->generateActivityLogFromContext($this->activityLogContext, $activityLog);
        }
        $this->entityManager->flush();
        $this->activityLogModels = [];
    }

    public function getActivityLogModels(): array
    {
        return $this->activityLogModels;
    }

    public function getActivityLogContext(): ActivityLogContext
    {
        return $this->activityLogContext;
    }
}
