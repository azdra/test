<?php

namespace App\Service;

use App\Entity\ActivityLog;
use App\Entity\Adobe\AdobeConnectPrincipal;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Adobe\AdobeConnectTelephonyProfile;
use App\Entity\Adobe\AdobeConnectWebinarPrincipal;
use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\Adobe\AdobeConnectWebinarTelephonyProfile;
use App\Entity\CategorySessionType;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexMeetingTelephonyProfile;
use App\Entity\Cisco\WebexParticipant;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Cisco\WebexRestMeetingTelephonyProfile;
use App\Entity\Cisco\WebexRestParticipant;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\Convocation;
use App\Entity\Microsoft\MicrosoftTeamsEventParticipant;
use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\Microsoft\MicrosoftTeamsEventTelephonyProfile;
use App\Entity\Microsoft\MicrosoftTeamsParticipant;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\Microsoft\MicrosoftTeamsTelephonyProfile;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\ProviderSessionAccessToken;
use App\Entity\SessionConvocationAttachment;
use App\Entity\User;
use App\Entity\WebinarConvocation;
use App\Entity\White\WhiteParticipant;
use App\Entity\White\WhiteSession;
use App\Exception\InvalidArgumentException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Model\ConvocationSendingHistory;
use App\Model\GeneratorTrait;
use App\Model\SendMailFormModel;
use App\Service\Utils\StrUtils;
use DateTime;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;

class ConvocationService
{
    use GeneratorTrait;

    public const SHORTS_CODES = [
        '__name__' => '{{ name }}',
        '__duration__' => '{{ duration | humanizeDuration(langue) }}',
        '__date_start__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateOnly(langue) }}',
        '__date_start_time_zone__' => '{{ dateStartTimeZone }}',
        '__date_start_year__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateYearOnly }}',
        '__date_start_month__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateMonthOnly }}',
        '__date_start_month_text__' => '{{ dateStart | applyTimezone(clientTimeZone) | humanizeMonth(langue) }}',
        '__date_start_day__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateDayOnly }}',
        '__date_start_day_text__' => '{{ dateStart | applyTimezone(clientTimeZone) | humanizeWeekDay(langue) }}',
        '__date_start_day_suffixed__' => '{{ dateStart | applyTimezone(clientTimeZone) | dateDaySuffixedOnly(langue) }}',
        '__hour_start__' => '{{ dateStart | applyTimezone(clientTimeZone) | hourOnly }}',
        '__hour_start_12h__' => '{{ dateStart | applyTimezone(clientTimeZone) | hourH12Only(langue) }}',
        '__hour_end__' => '{{ dateEnd | applyTimezone(clientTimeZone) | hourOnly }}',
        '__hour_end_12h__' => '{{ dateEnd | applyTimezone(clientTimeZone) | hourH12Only(langue) }}',
        '__langue__' => '{{ langue | humanizeLang(langue) }}',
        '__login_url__' => '{{ login_url }}',
        '__content__' => '{{ personalizedContent | raw  }}',
        '__personalized_content__' => '{{ personalizedContent | raw }}',
        '__username__' => '{{ username }}',
        '__salle_url__' => '{{ salleUrl }}',
        '__lieu__' => '{{ lieu }}',
        '__objectifs__' => '{{ objectifs }}',
        '__room_url__' => '{{ ACRoomUrl }}',
        '__room_url_universal__' => '{{ joinSessionWebUrl }}',
        '__room_url_web_link__' => '{{ meetingRestWebLink }}',
        '__connection_information_universal__' => '{{ connectionInformation }}',
        '__audio_real_number__' => '{{ audioRealNumber }}',
        '__audio_shared_number__' => '{{ audioSharedNumber }}',
        '__audio_animator_code__' => '{{ audioAnimatorCode }}',
        '__audio_animator_code_concat__' => '{{ audioAnimatorCodeConcat }}',
        '__audio_participant_code__' => '{{ audioParticipantCode }}',
        '__audio_meetingone_na_conference_id__' => '{{ audioMeetingOneConferenceId }}',
        '__audio_meetingone_conference_id__' => '{{ audioMeetingOneConferenceId }}',
        '__if_is_animator__' => '{% if isAnAnimator %}',
        '__if_is_participant__' => '{% if isAParticipant %}',
        '__if_end__' => '{% endif %}',
        '__addevent_url__' => '{{ addEventUrl }}',
        '__if_eval_url_exist__' => '{% if evalUrl is not empty %}',
        '__eval_url__' => '{{ evalUrl }}',
        '__if_object_url_exist__' => '{% if objectUrl is not empty %}',
        '__object_url__' => '{{ objectUrl }}',
        '__acquis_link__' => '{{ acquisLink }}',
        '__animator_access_link__' => '{{ animatorAccessLink }}',
        '__objectifs_pres__' => '{{ objectifsPres }}',
        '__releve_pres__' => '{{ relevePres }}',
        '__eval_prest_pres__' => '{{ evalPrestPres }}',
        '__if_is_an_adobe_connect_session__' => '{% if isAnAdobeConnectSession %}',
        '__if_is_a_webex_meeting_session__' => '{% if isAWebexMeetingSession %}',
        '__if_is_a_microsoft_teams_session__' => '{% if isAMicrosoftTeamsSession %}',
        '__if_is_a_universal_connector_session__' => '{% if isAWhiteConnectorSession %}',
        '__nbr_days__' => '{{ nbrDays }}',
        '__rest_lunch__' => '{{ restLunch }}',
        '__location__' => '{{ location }}',
        '__if_is_presential_session__' => '{% if isAPresentialSession %}',
        '__if_is_mixte_session__' => '{% if isAMixteSession %}',
        '__if_is_distantial_participant__' => '{% if isADistantialParticipant %}',
        '__if_is_presential_participant__' => '{% if isAPresentialParticipant %}',
        '__licence_microsoft_teams_email__' => '{{ licenceMicrosoftTeamsEmail }}',
        '__licence_microsoft_teams_password__' => '{{ licenceMicrosoftTeamsPassword }}',
        '__if_is_a_microsoft_teams_shared_licence__' => '{% if isLicenceSharedMicrosoftTeams %}',
        '__if_is_an_adobe_connect_shared_licence__' => '{% if isLicenceSharedAdobeConnect %}',
        '__licence_adobe_connect_email__' => '{{ licenceAdobeConnectEmail }}',
        '__licence_adobe_connect_password__' => '{{ licenceAdobeConnectPassword }}',
        '__animator_list__' => '{{ animatorsList }}',
        '__host_opening_session__' => '{{ licenceMicrosoftTeamsHostOpening }}',
        '__host_opening_session_name__' => '{{ licenceMicrosoftTeamsHostOpeningName }}',
        '__host_opening_session_email__' => '{{ licenceMicrosoftTeamsHostOpeningEmail }}',
        '__host_opening_session_password__' => '{{ licenceMicrosoftTeamsHostOpeningPassword }}',
        '__session_replay__' => '{{ sessionReplay }}',
    ];

    public function __construct(
        private Environment $twig,
        private TranslatorInterface $translator,
        private RouterInterface $router,
        private string $endPointDomain,
        private ActivityLogger $activityLogger,
        private MailerService $mailerService,
        private string $convocationAttachmentSavefilesDirectory,
        private ReferrerService $referrerService
    ) {
    }

    public function getTemplateParameters(ProviderParticipantSessionRole $providerParticipantSessionRole): array
    {
        $providerSession = $providerParticipantSessionRole->getSession();
        $providerParticipant = $providerParticipantSessionRole->getParticipant();

        if (empty($providerSession->getSubject())) {
            $name = $providerSession->getName();
        } else {
            $name = $providerSession->getSubject()->getName();
        }

        $dateformat = $providerSession->getLanguage() == 'en' ? 'h:i a (Y/m/d)' : 'H:i (d/m/Y';
        $parameters = [
            'clientTimeZone' => $providerSession->getClient()->getTimezone(),
            'name' => $name,
            'duration' => $providerSession->getDuration(),
            'dateStart' => $providerSession->getDateStart(),
            'dateStartTimeZone' => implode('<br>', array_map(
                fn (string $ppsr) => "• $ppsr (GMT ".date('P', $providerSession->getDateStart()->getTimestamp()).') : '.date($dateformat, $providerSession->getDateStart()->getTimestamp()),
                $providerSession->getAdditionalTimezones()
            )),
            'dateEnd' => $providerSession->getDateEnd(),
            'langue' => $providerSession->getLanguage(),
            'login_url' => $this->endPointDomain.$this->router->generate('_session_access_plateforme', ['token' => $providerParticipantSessionRole->getProviderSessionAccessToken()->getToken()]),
            'username' => "{$providerParticipant->getFirstName()} {$providerParticipant->getLastName()}",
            'salleUrl' => 'https://plop.fr',
            'lieu' => 'Paris',
            'objectifs' => implode('<br>', array_map(
                fn ($objective) => "• $objective",
                $providerSession->getObjectives() ?? []
            )),
            'isAnAnimator' => $providerParticipantSessionRole->isAnAnimator(),
            'isAParticipant' => $providerParticipantSessionRole->isATrainee(),
            'addEventUrl' => $this->endPointDomain.$this->router->generate('_session_add_to_calendar', ['token' => $providerParticipantSessionRole->getProviderSessionAccessToken()->getToken()]),
            'evalUrl' => 'https://plop.fr',
            'objectUrl' => 'https://plop.fr',
            'acquisLink' => 'https://plop.fr',
            'animatorAccessLink' => $this->endPointDomain.$this->router->generate('_session_get', ['id' => $providerSession->getId(), 'token' => $providerParticipantSessionRole->getProviderSessionAccessToken()->getToken()]),
            'objectifsPres' => 'Hellow',
            'relevePres' => $this->endPointDomain.$this->router->generate('_session_signature_lobby', ['id' => $providerSession->getId()]),
            'evalPrestPres' => $this->endPointDomain.$this->router->generate('_session_evaluation_lobby', ['id' => $providerSession->getId()]),
            'nbrDays' => $providerSession->getNbrdays(),
            'personalizedContent' => $providerSession->getPersonalizedContent(),
            'restLunch' => $providerSession->getRestlunch(),
            'location' => $providerSession->getLocation(),
            'isAPresentialSession' => $providerSession->isSessionPresential(),
            'isAMixteSession' => $providerSession->isSessionMixte(),
            'isADistantialParticipant' => $providerParticipantSessionRole->getSituationMixte() === $providerParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL,
            'isAPresentialParticipant' => $providerParticipantSessionRole->getSituationMixte() === $providerParticipantSessionRole::SITUATION_MIXTE_PRESENTIAL,
            'isAnAdobeConnectSession' => $providerSession instanceof AdobeConnectSCO,
            'isAWebexMeetingSession' => $providerSession instanceof WebexMeeting,
            'isAWebexRestMeetingSession' => $providerSession instanceof WebexRestMeeting,
            'isAMicrosoftTeamsSession' => $providerSession instanceof MicrosoftTeamsSession,
            'isAMicrosoftTeamsEventSession' => $providerSession instanceof MicrosoftTeamsEventSession,
            'isAWhiteConnectorSession' => $providerSession instanceof WhiteSession,
            'animatorsList' => implode('<br>', array_map(
                fn (ProviderParticipantSessionRole $ppsr) => "• {$ppsr->getParticipant()->getFullName()}",
                $providerSession->getAnimatorsOnly()
            )),
            'sessionReplay' => $this->endPointDomain.$this->router->generate('_weblink_access_replay', ['client' => $providerSession->getClient()->getSlug(), 'session' => $providerSession->getId(), 'token' => $providerParticipantSessionRole->getProviderSessionAccessToken()->getToken()]),
        ];

        $providerSessionParameters = $this->getProviderSessionParameters($providerSession);

        return array_merge($parameters, $providerSessionParameters);
    }

    private function getProviderSessionParameters(ProviderSession $providerSession): array
    {
        switch (get_class($providerSession)) {
            case AdobeConnectSCO::class:
                /** @var AdobeConnectTelephonyProfile|null $providerAudio */
                $providerAudio = $providerSession->getProviderAudio();

                $sharedLicence = $providerSession->getLicenceShared();

                return [
                    'ACRoomUrl' => $providerSession->getFullUrlpath(),
                    'audioRealNumber' => $providerAudio?->getPhoneNumber() ?? $providerAudio?->getAdobeDefaultPhoneNumber(),
                    'audioSharedNumber' => $providerAudio?->getAdobeDefaultPhoneNumber() ?? '',
                    'audioAnimatorCode' => $providerAudio?->getCodeAnimator() ?? '',
                    'audioParticipantCode' => $providerAudio?->getCodeParticipant() ?? '',
                    'audioMeetingOneConferenceId' => $providerAudio?->getMeetingOneConferenceId() ?? '',
                    'isLicenceSharedAdobeConnect' => is_array($sharedLicence),
                    'licenceAdobeConnectEmail' => $sharedLicence['email'] ?? '',
                    'licenceAdobeConnectPassword' => $sharedLicence['password'] ?? '',
                ];

            case AdobeConnectWebinarSCO::class:
                /** @var AdobeConnectWebinarTelephonyProfile|null $providerAudio */
                $providerAudio = $providerSession->getProviderAudio();

                $sharedLicence = $providerSession->getLicenceShared();

                return [
                    'ACRoomUrl' => $providerSession->getFullUrlpath(),
                    'audioRealNumber' => $providerAudio?->getPhoneNumber() ?? $providerAudio?->getAdobeDefaultPhoneNumber(),
                    'audioSharedNumber' => $providerAudio?->getAdobeDefaultPhoneNumber() ?? '',
                    'audioAnimatorCode' => $providerAudio?->getCodeAnimator() ?? '',
                    'audioParticipantCode' => $providerAudio?->getCodeParticipant() ?? '',
                    'audioMeetingOneConferenceId' => $providerAudio?->getMeetingOneConferenceId() ?? '',
                    'isLicenceSharedAdobeConnect' => is_array($sharedLicence),
                    'licenceAdobeConnectEmail' => $sharedLicence['email'] ?? '',
                    'licenceAdobeConnectPassword' => $sharedLicence['password'] ?? '',
                ];

            case WebexMeeting::class:
                /** @var WebexMeetingTelephonyProfile|null $providerAudio */
                $providerAudio = $providerSession->getProviderAudio();

                return [
                    'audioRealNumber' => $providerAudio?->getPhoneNumber() ?? '',
                    'audioAnimatorCode' => $providerAudio?->getCodeAnimator() ?? '',
                    'audioAnimatorCodeConcat' => !empty($providerAudio?->getCodeAnimator())
                        ? str_replace(' ', '', $providerAudio->getCodeAnimator())
                        : '',
                    'audioParticipantCode' => $providerAudio?->getCodeParticipant() ?? '',
                ];

            case WebexRestMeeting::class:
                /** @var WebexRestMeetingTelephonyProfile|null $providerAudio */
                $providerAudio = $providerSession->getProviderAudio();

                /* @var WebexRestMeeting $providerSession */
                return [
                    'audioRealNumber' => $providerAudio?->getPhoneNumber() ?? '',
                    'audioAnimatorCode' => $providerAudio?->getCodeAnimator() ?? '',
                    'audioAnimatorCodeConcat' => !empty($providerAudio?->getCodeAnimator())
                        ? str_replace(' ', '', $providerAudio->getCodeAnimator())
                        : '',
                    'audioParticipantCode' => $providerAudio?->getCodeParticipant() ?? '',
                    'meetingRestWebLink' => $providerSession->getMeetingRestWebLink() ?? '',
                ];

            case MicrosoftTeamsSession::class:
                /** @var MicrosoftTeamsTelephonyProfile|null $providerAudio */
                $providerAudio = $providerSession->getProviderAudio();

                /** @var MicrosoftTeamsConfigurationHolder $configurationHolder */
                $configurationHolder = $providerSession->getAbstractProviderConfigurationHolder();

                foreach ($providerSession->getParticipantRoles() as $ppsr) {
                    if ($ppsr->getRole() === ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                        $licenceMicrosoftTeamsEmail = $providerSession->getLicence();
                        $licenceMicrosoftTeamsPassword = $configurationHolder->getLicences()[$licenceMicrosoftTeamsEmail]['password'];
                        $isLicenceSharedMicrosoftTeams = $configurationHolder->getLicences()[$licenceMicrosoftTeamsEmail]['shared'];
                    }
                }

                /** @var ProviderParticipantSessionRole $animatorLicence */
                $animatorLicence = $providerSession->getParticipantRoles()->filter(fn (ProviderParticipantSessionRole $ppsr) => $ppsr->getParticipant()->getEmail() === $providerSession->getLicence())->first();
                $email = (!empty($licenceMicrosoftTeamsEmail)) ? $licenceMicrosoftTeamsEmail : '';

                return [
                    'audioRealNumber' => $providerAudio?->getPhoneNumber() ?? '',
                    'audioAnimatorCode' => $providerAudio?->getCodeAnimator() ?? '',
                    'audioAnimatorCodeConcat' => !empty($providerAudio?->getCodeAnimator())
                        ? str_replace(' ', '', $providerAudio->getCodeAnimator())
                        : '',
                    'audioParticipantCode' => $providerAudio?->getCodeParticipant() ?? '',
                    'microsoftTeamsDefaultPhoneNumber' => $providerAudio?->getMicrosoftTeamsDefaultPhoneNumber() ?? '',
                    'audioConferencingDailinUrl' => $providerAudio?->getAudioConferencing()['dailinUrl'] ?? '',
                    'audioConferencingTollNumber' => $providerAudio?->getAudioConferencing()['tollNumber'] ?? '',
                    'audioConferencingTollFreeNumber' => $providerAudio?->getAudioConferencing()['tollFreeNumber'] ?? '',
                    'audioConferencingConferenceId' => $providerAudio?->getAudioConferencing()['ConferenceId'] ?? '',
                    'licenceMicrosoftTeamsEmail' => $email,
                    'licenceMicrosoftTeamsPassword' => (!empty($licenceMicrosoftTeamsPassword)) ? $licenceMicrosoftTeamsPassword : '',
                    'isLicenceSharedMicrosoftTeams' => (!empty($isLicenceSharedMicrosoftTeams)) ? $isLicenceSharedMicrosoftTeams : '',
                    'licenceMicrosoftTeamsHostOpening' => sprintf('%s (%s)', $animatorLicence->getFullName() ?? '', $email),
                    'licenceMicrosoftTeamsHostOpeningName' => "{$animatorLicence->getFirstName()} {$animatorLicence->getLastName()}",
                    'licenceMicrosoftTeamsHostOpeningEmail' => $email,
                    'licenceMicrosoftTeamsHostOpeningPassword' => $configurationHolder->getPassword(),
                ];

            case MicrosoftTeamsEventSession::class:
                /** @var MicrosoftTeamsEventSession $providerSession */
                /** @var MicrosoftTeamsEventTelephonyProfile|null $providerAudio */
                $providerAudio = $providerSession->getProviderAudio();

                /** @var MicrosoftTeamsEventConfigurationHolder $configurationHolder */
                $configurationHolder = $providerSession->getAbstractProviderConfigurationHolder();

                /*foreach ($providerSession->getParticipantRoles() as $ppsr) {
                    if ($ppsr->getRole() === ProviderParticipantSessionRole::ROLE_ANIMATOR) {
                        $licenceMicrosoftTeamsEventEmail = $providerSession->getLicence();
                        if(array_key_exists($licenceMicrosoftTeamsEventEmail, $configurationHolder->getLicences())) { //Not attributed
                            $licenceMicrosoftTeamsEventPassword = $configurationHolder->getLicences()[$licenceMicrosoftTeamsEventEmail]['password'];
                            $isLicenceSharedMicrosoftTeamsEvent = $configurationHolder->getLicences()[$licenceMicrosoftTeamsEventEmail]['shared'];
                        } else {
                            $isLicenceAttributedMicrosoftTeamsEvent = $configurationHolder->getLicences()[$providerSession->getUserLicence()]['attributed'];
                        }
                    }
                }*/

                $licenceMicrosoftTeamsEventEmail = $providerSession->getLicence();
                $licenceMicrosoftTeamsEventPassword = (array_key_exists($licenceMicrosoftTeamsEventEmail, $configurationHolder->getLicences())) ? $configurationHolder->getLicences()[$licenceMicrosoftTeamsEventEmail]['password'] : '';
                $isLicenceSharedMicrosoftTeamsEvent = (array_key_exists($licenceMicrosoftTeamsEventEmail, $configurationHolder->getLicences())) ? $configurationHolder->getLicences()[$licenceMicrosoftTeamsEventEmail]['shared'] : false;
                $isLicenceAttributedMicrosoftTeamsEvent = (!array_key_exists($licenceMicrosoftTeamsEventEmail, $configurationHolder->getLicences())) ? $configurationHolder->getLicences()[$providerSession->getUserLicence()]['attributed'] : false;

                $animatorChoiceLicence = $providerSession->getAnimatorChoiceLicence();
                /** @var ProviderParticipantSessionRole $animatorLicence */
                $animatorLicence = $providerSession->getParticipantRoles()->filter(fn (ProviderParticipantSessionRole $ppsr) => $ppsr->getParticipant()->getEmail() === $animatorChoiceLicence)->first();
                $email = (!empty($licenceMicrosoftTeamsEventEmail)) ? $licenceMicrosoftTeamsEventEmail : '';

                $password = $isLicenceSharedMicrosoftTeamsEvent ? $configurationHolder->getPassword() : $configurationHolder->getLicences()[$animatorChoiceLicence]['passwordAttributed'];

                return [
                    'audioRealNumber' => $providerAudio?->getPhoneNumber() ?? '',
                    'audioAnimatorCode' => $providerAudio?->getCodeAnimator() ?? '',
                    'audioAnimatorCodeConcat' => !empty($providerAudio?->getCodeAnimator())
                        ? str_replace(' ', '', $providerAudio->getCodeAnimator())
                        : '',
                    'audioParticipantCode' => $providerAudio?->getCodeParticipant() ?? '',
                    'microsoftTeamsDefaultPhoneNumber' => $providerAudio?->getMicrosoftTeamsEventDefaultPhoneNumber() ?? '',
                    'audioConferencingDailinUrl' => $providerAudio?->getAudioConferencing()['dailinUrl'] ?? '',
                    'audioConferencingTollNumber' => $providerAudio?->getAudioConferencing()['tollNumber'] ?? '',
                    'audioConferencingTollFreeNumber' => $providerAudio?->getAudioConferencing()['tollFreeNumber'] ?? '',
                    'audioConferencingConferenceId' => $providerAudio?->getAudioConferencing()['ConferenceId'] ?? '',
                    'licenceMicrosoftTeamsEmail' => $email,
                    'licenceMicrosoftTeamsPassword' => (!empty($licenceMicrosoftTeamsEventPassword)) ? $licenceMicrosoftTeamsEventPassword : '',
                    'isLicenceSharedMicrosoftTeams' => (!empty($isLicenceSharedMicrosoftTeamsEvent)) ? $isLicenceSharedMicrosoftTeamsEvent : '',
                    'isLicenceAttributedMicrosoftTeams' => (!empty($isLicenceAttributedMicrosoftTeamsEvent)) ? $isLicenceAttributedMicrosoftTeamsEvent : '',
                    'licenceMicrosoftTeamsHostOpening' => sprintf('%s (%s)', $animatorLicence->getFullName() ?? '', $email),
                    'licenceMicrosoftTeamsHostOpeningName' => "{$animatorLicence->getFirstName()} {$animatorLicence->getLastName()}",
                    'licenceMicrosoftTeamsHostOpeningEmail' => $email,
                    'licenceMicrosoftTeamsHostOpeningPassword' => $password,
                ];

            case WhiteSession::class:
                /* @var WhiteSession $providerSession */
                return [
                    'joinSessionWebUrl' => $providerSession->getJoinSessionWebUrl(),
                    'connectionInformation' => $providerSession->getConnectionInformation(),
                ];

            default:
                return [];
        }
    }

    /**
     * @throws LoaderError
     * @throws SyntaxError
     */
    public function transformShortsCodes(
        string $rawConvocationContent,
        ProviderParticipantSessionRole $participantSessionRole
    ): string {
        $shortsCodes = [];

        foreach (self::SHORTS_CODES as $k => $shortsCode) {
            $shortsCodes["/$k/"] = $shortsCode;
        }

        $cleanConvocationContent = preg_replace(array_keys($shortsCodes), $shortsCodes, $rawConvocationContent);

        $templateTwig = $this->twig->createTemplate($cleanConvocationContent);

        return html_entity_decode($templateTwig->render($this->getTemplateParameters($participantSessionRole)));
    }

    /**
     * @throws LoaderError
     * @throws SyntaxError
     */
    public function checkShortsCodesValid(
        string $rawConvocationContent
    ): array {
        $shortsCodes = [];
        $shortsCodesConvocationContent = [];

        foreach (self::SHORTS_CODES as $k => $shortsCode) {
            $shortsCodes["/$k/"] = $shortsCode;
        }

        preg_match_all(
            '/__\w*__/',
            $rawConvocationContent,
            $matches,
            PREG_PATTERN_ORDER
        );

        foreach ($matches[0] as $match) {
            if (!array_key_exists('/'.$match.'/', $shortsCodes)) {
                $shortsCodesConvocationContent[] = $match;
            }
        }

        if (count($shortsCodesConvocationContent) > 0) {
            return $shortsCodesConvocationContent;
        }

        $cleanConvocationContent = preg_replace(array_keys($shortsCodes), $shortsCodes, $rawConvocationContent);
        $templateTwig = $this->twig->createTemplate($cleanConvocationContent);

        return $shortsCodesConvocationContent;
    }

    /**
     * @throws LoaderError
     * @throws SyntaxError
     */
    public function transformWithFakeData(string $rawConvocationContent, string $lang = 'fr'): string
    {
        $client = (new Client())
            ->setName('My client')
            ->setTimezone('Europe/Paris');

        $configurationHolder = (new AdobeConnectConfigurationHolder())
            ->setClient($client);

        $session = (new AdobeConnectSCO($configurationHolder))
            ->setName('My name')
            ->setDuration(65)
            ->setDateStart(new DateTime('2021-08-14 09:10:00'))
            ->setDateEnd(new DateTime('2021-08-14 19:05:00'))
            ->setLanguage($lang)
            ->setPersonalizedContent('My awesome content !')
            ->setId(42);

        $user = (new User())
            ->setFirstName('Firstname')
            ->setLastName('LASTNAME')
            ->setClient($client);

        $participant = (new AdobeConnectPrincipal($configurationHolder))
            ->setUser($user);

        $accessToken = (new ProviderSessionAccessToken())
            ->setToken('MyToken');

        $participantSessionRole = (new ProviderParticipantSessionRole())
            ->setSession($session)
            ->setParticipant($participant)
            ->setProviderSessionAccessToken($accessToken)
            ->setRole(ProviderParticipantSessionRole::ROLE_TRAINEE);

        return $this->transformShortsCodes($rawConvocationContent, $participantSessionRole);
    }

    /**
     * @throws LoaderError
     * @throws SyntaxError
     * @throws InvalidArgumentException
     */
    public function sendConvocation(ProviderParticipantSessionRole $participantRole, bool $sendAsReminder = false, ?User $origin = null, ?array $ccs = null): void
    {
        $session = $participantRole->getSession();
        $convocation = $session->getConvocation();
        $participant = $participantRole->getParticipant();
        if ($session->getCategory() != CategorySessionType::CATEGORY_SESSION_WEBINAR) {
            $subjectPrefix = ($sendAsReminder ? $this->translator->trans('Reminder').' - ' : '');
            $subject = $subjectPrefix.$this->transformShortsCodes($convocation->getSubjectMail(), $participantRole);
            $content = $this->transformShortsCodes($convocation->getContent(), $participantRole);

            $address = new Address($participant->getEmail(), "{$participant->getFirstName()} {$participant->getLastName()}");

            if (!$this->activityLogger->isActivityLogContextInitiated()) {
                $this->activityLogger->initActivityLogContext(
                    $session->getClient(),
                    $origin,
                    (null === $origin ? ActivityLog::ORIGIN_CRON : ActivityLog::ORIGIN_USER_INTERFACE),
                    [
                        'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                        'user' => ['id' => $participant->getUserId(), 'name' => $participant->getFullName()],
                    ]
                );
            }

            $attachments = [];
            /** @var SessionConvocationAttachment $attachment */
            foreach ($session->getAttachments() as $attachment) {
                $attachments[] = ['path' => $this->convocationAttachmentSavefilesDirectory.'/'.$attachment->getFileName(), 'name' => $attachment->getOriginalName()];
            }
            $ics = $this->linkWithICS($session, $participantRole);
            if (!empty($ics)) {
                $icsDataPart = new DataPart($ics, 'invitation.ics', 'text/calendar');
            }
            $this->mailerService->sendTemplatedMail(
                $address,
                $subject,
                'emails/participant/convocation.html.twig',
                [
                    'content' => $content,
                    'client' => $session->getClient()->getName(),
                    'clientService' => $session->getClient(),
                    'session' => $session,
                    'isAnimator' => $participantRole->isAnAnimator(),
                ],
                pathToAttach: $attachments,
                metadatas: [
                    'origin' => MailerService::ORIGIN_EMAIL_SEND_CONVOCATION,
                    'user' => $participant->getUser()->getId(),
                    'session' => $session->getId(),
                ],
                ccs: $ccs,
                attachDataPart: (!empty($ics)) ? [$icsDataPart] : []
            );

            if (!$sendAsReminder) {
                $participantRole->addADateOfSendingConvocation(ConvocationSendingHistory::TYPE_CONVOCATION, $origin);

                $message = [
                    'key' => 'The participants <a href="%userRoute%">%userName%</a> convocation for the <a href="%sessionRoute%">%sessionName%</a> has been sent.',
                    'params' => [
                        '%userName%' => $participant->getFullName(),
                        '%sessionName%' => $session->getName(),
                        '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                        '%userRoute%' => $this->router->generate('_participant_edit', ['id' => $participant->getUser()->getId()]),
                    ],
                ];
            } else {
                $participantRole->addADateOfSendingConvocation(ConvocationSendingHistory::TYPE_REMINDER, $origin);

                $message = [
                    'key' => 'The <a href="%userRoute%">%userName%</a> participant convocation reminder for the <a href="%sessionRoute%">%sessionName%</a> session has been sent.',
                    'params' => [
                        '%userName%' => $participant->getFullName(),
                        '%sessionName%' => $session->getName(),
                        '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                        '%userRoute%' => $this->router->generate('_participant_edit', ['id' => $participant->getUser()->getId()]),
                    ],
                ];
            }
            $this->activityLogger->addActivityLog(new ActivityLogModel($sendAsReminder ? ActivityLog::ACTION_SEND_REMINDER : ActivityLog::ACTION_SEND_CONVOCATION, ActivityLog::SEVERITY_INFORMATION, ['message' => $message]));

            if (!empty($ccs)) {
                $messageReferrer = [
                    'key' => 'The participants <a href="%userRoute%">%userName%</a> convocation for the <a href="%sessionRoute%">%sessionName%</a> has been sent to referrer <a href="%userRoute%">%userReferrer%</a>',
                    'params' => [
                        '%userName%' => $participant->getFullName(),
                        '%sessionName%' => $session->getName(),
                        '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                        '%userRoute%' => $this->router->generate('_participant_edit', ['id' => $participant->getUser()->getId()]),
                        '%userReferrer%' => $participantRole->getParticipant()->getUser()->getEmailReferrer(),
                    ],
                ];
                $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_SEND_CONVOCATION, ActivityLog::SEVERITY_INFORMATION, ['message' => $messageReferrer]));
            }
        }
    }

    /**
     * @throws LoaderError
     * @throws SyntaxError
     * @throws InvalidArgumentException
     */
    public function sendCommunication(ProviderParticipantSessionRole $participantRole, WebinarConvocation $communication, bool $sendAsReminder = false, ?User $origin = null): void
    {
        $session = $participantRole->getSession();
        $participant = $participantRole->getParticipant();
        if ($this->isEmailValid($participant->getEmail())) {
            $subject = $this->transformShortsCodes($communication->getSubjectMail(), $participantRole);
            $content = $this->transformShortsCodes($communication->getContent(), $participantRole);

            $address = new Address($participant->getEmail(), "{$participant->getFirstName()} {$participant->getLastName()}");

            if (!$this->activityLogger->isActivityLogContextInitiated()) {
                $this->activityLogger->initActivityLogContext(
                    $session->getClient(),
                    $origin,
                    (null === $origin ? ActivityLog::ORIGIN_CRON : ActivityLog::ORIGIN_USER_INTERFACE),
                    [
                        'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
                        'user' => ['id' => $participant->getUserId(), 'name' => $participant->getFullName()],
                    ]
                );
            }

            $attachments = [];
            /** @var SessionConvocationAttachment $attachment */
            foreach ($communication->getAttachments() as $attachment) {
                $attachments[] = ['path' => $this->convocationAttachmentSavefilesDirectory.'/'.$attachment->getFileName(), 'name' => $attachment->getOriginalName()];
            }
            $ics = $this->linkWithICS($session, $participantRole);
            if (!empty($ics)) {
                $icsDataPart = new DataPart($ics, 'invitation.ics', 'text/calendar');
            }
            $this->mailerService->sendTemplatedMail(
                $address,
                $subject,
                'emails/participant/convocation.html.twig',
                [
                    'content' => $content,
                    'client' => $session->getClient()->getName(),
                    'clientService' => $session->getClient(),
                    'session' => $session,
                    'isAnimator' => $participantRole->isAnAnimator(),
                ],
                pathToAttach: $attachments,
                metadatas: [
                    'origin' => MailerService::ORIGIN_EMAIL_SEND_CONVOCATION,
                    'user' => $participant->getUser()->getId(),
                    'session' => $session->getId(),
                ],
                attachDataPart: (!empty($ics)) ? [$icsDataPart] : []
            );
        }

        switch ($communication->getConvocationType()) {
            case WebinarConvocation::TYPE_CONFIRMATION:
                $participantRole->setLastCommunicationSent($communication->getConvocationDate());
                $session->setLastConvocationSent(new \DateTimeImmutable());
                $message = [
                    'key' => 'The participants <a href="%userRoute%">%userName%</a> confirmation for the <a href="%sessionRoute%">%sessionName%</a> has been sent.',
                    'params' => [
                        '%userName%' => $participant->getFullName(),
                        '%sessionName%' => $session->getName(),
                        '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                        '%userRoute%' => $this->router->generate('_participant_edit', ['id' => $participant->getUser()->getId()]),
                    ],
                ];
                break;
            case WebinarConvocation::TYPE_REMINDER:
                $participantRole->setLastReminderCommunicationSent(new \DateTimeImmutable());

                $message = [
                    'key' => 'the <a href="%userRoute%">%userName%</a> participant communication reminder for the  <a href="%sessionRoute%">%sessionName%</a> session has been sent',
                    'params' => [
                        '%userName%' => $participant->getFullName(),
                        '%sessionName%' => $session->getName(),
                        '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                        '%userRoute%' => $this->router->generate('_participant_edit', ['id' => $participant->getUser()->getId()]),
                    ],
                ];
                break;
            case WebinarConvocation::TYPE_THANK_YOU:
                $participantRole->setLastThanksCommunicationSent(new \DateTimeImmutable());

                $message = [
                    'key' => 'thank you for the participant <a href="%userRoute%">%userName%</a> for the  <a href="%sessionRoute%">%sessionName%</a> session has been sent',
                    'params' => [
                        '%userName%' => $participant->getFullName(),
                        '%sessionName%' => $session->getName(),
                        '%sessionRoute%' => $this->router->generate('_session_get', ['id' => $session->getId()]),
                        '%userRoute%' => $this->router->generate('_participant_edit', ['id' => $participant->getUser()->getId()]),
                    ],
                ];
                break;
        }
        $this->activityLogger->addActivityLog(new ActivityLogModel($sendAsReminder ? ActivityLog::ACTION_SEND_REMINDER : ActivityLog::ACTION_SEND_CONVOCATION, ActivityLog::SEVERITY_INFORMATION, ['message' => $message]));
    }

    /**
     * @throws SyntaxError
     * @throws InvalidArgumentException
     * @throws LoaderError
     */
    public function sendConvocationTestEmailRequest(
        ProviderSession $providerSession,
        SendMailFormModel $mails,
        User $sender
    ): void {
        $this->sendConvocationTestEmail($providerSession, $mails->getRecipients()['tags'], ProviderParticipantSessionRole::ROLE_ANIMATOR, $mails->getCc()['tags'], $mails->getBcc()['tags'], $sender);
        $this->sendConvocationTestEmail($providerSession, $mails->getRecipients()['tags'], ProviderParticipantSessionRole::ROLE_TRAINEE, $mails->getCc()['tags'], $mails->getBcc()['tags'], $sender);
    }

    /**
     * @throws SyntaxError
     * @throws InvalidArgumentException
     * @throws LoaderError
     */
    public function sendConvocationTestEmail(ProviderSession $providerSession, array $recipients, string $role, array $ccs = [], array $bccs = [], ?User $sender = null): void
    {
        $convocation = $providerSession->getConvocation();

        $participant = match (get_class($providerSession)) {
            WebexMeeting::class => (new WebexParticipant($providerSession->getAbstractProviderConfigurationHolder()))
                ->setUser($sender),
            WebexRestMeeting::class => (new WebexRestParticipant($providerSession->getAbstractProviderConfigurationHolder()))
                ->setUser($sender),
            AdobeConnectSCO::class => (new AdobeConnectPrincipal($providerSession->getAbstractProviderConfigurationHolder()))
                ->setName($sender->getFullName())
                ->setPrincipalIdentifier(null)
                ->setUser($sender),
            AdobeConnectWebinarSCO::class => (new AdobeConnectWebinarPrincipal($providerSession->getAbstractProviderConfigurationHolder()))
                ->setName($sender->getFullName())
                ->setPrincipalIdentifier(null)
                ->setUser($sender),
            MicrosoftTeamsSession::class => (new MicrosoftTeamsParticipant($providerSession->getAbstractProviderConfigurationHolder()))
                ->setUser($sender),
            MicrosoftTeamsEventSession::class => (new MicrosoftTeamsEventParticipant($providerSession->getAbstractProviderConfigurationHolder()))
                ->setUser($sender),
            WhiteSession::class => (new WhiteParticipant($providerSession->getAbstractProviderConfigurationHolder()))
                ->setUser($sender),
            default => null
        };

        if (is_null($participant)) {
            return;
        }
        $participantRole = (new ProviderParticipantSessionRole())
            ->setSession($providerSession)
            ->setParticipant($participant)
            ->setProviderSessionAccessToken((new ProviderSessionAccessToken())
                ->setToken($participant->getUser()->getToken()))
            ->setRole($role);

        $convocations = [];

        if ($providerSession->getCategory() === CategorySessionType::CATEGORY_SESSION_WEBINAR) {
            foreach ($providerSession->getWebinarConvocation() as $webinarConvocation) {
                $attachments = [];
                if (!$providerSession->isSessionTest()) {
                    $subject = 'Test '.$this->transformShortsCodes($webinarConvocation->getSubjectMail() ?? $convocation->getSubjectMail(), $participantRole);
                } else {
                    $subject = $this->transformShortsCodes($webinarConvocation->getSubjectMail() ?? $convocation->getSubjectMail(), $participantRole);
                }

                /** @var SessionConvocationAttachment $attachment */
                foreach ($webinarConvocation->getAttachments() as $attachment) {
                    $attachments[] = ['path' => $this->convocationAttachmentSavefilesDirectory.'/'.$attachment->getFileName(), 'name' => $attachment->getOriginalName()];
                }

                $convocations[] = [
                    'subject' => $subject,
                    'content' => $webinarConvocation?->getContent() ?? $convocation->getContent(),
                    'attachments' => $attachments,
                ];
            }
        } else {
            if (!$providerSession->isSessionTest()) {
                $subject = 'Test '.$this->transformShortsCodes($convocation->getSubjectMail(), $participantRole);
            } else {
                $subject = $this->transformShortsCodes($convocation->getSubjectMail(), $participantRole);
            }
            $attachments = [];
            /** @var SessionConvocationAttachment $attachment */
            foreach ($providerSession->getAttachments() as $attachment) {
                $attachments[] = ['path' => $this->convocationAttachmentSavefilesDirectory.'/'.$attachment->getFileName(), 'name' => $attachment->getOriginalName()];
            }

            $convocations[] = [
                'subject' => $subject,
                'content' => $convocation->getContent(),
                'attachments' => $attachments,
            ];
        }

        foreach ($convocations as $convocation) {
            $content = $this->transformShortsCodes($convocation['content'], $participantRole);
            $this->mailerService->sendTemplatedMail(
                $recipients,
                $convocation['subject'],
                'emails/participant/send_test_convocation.html.twig',
                [
                    'content' => $content,
                    'client' => $providerSession->getClient()->getName(),
                    'clientService' => $providerSession->getClient(),
                    'session' => $providerSession,
                    'isAnimator' => $participantRole->isAnAnimator(),
                ],
                pathToAttach: $convocation['attachments'],
                metadatas: [
                    'origin' => MailerService::ORIGIN_EMAIL_SEND_CONVOCATION,
                    'user' => $sender->getId(),
                    'session' => $providerSession->getId(),
                ],
                ccs: $ccs,
                bccs: $bccs
            );
        }
    }

    public function addNewCommunicationWebinar(Convocation $convocation, string $communicationType, ProviderSession $session, User $user): WebinarConvocation
    {
        $communicationWebinar = new WebinarConvocation($convocation, $session);
        $communicationWebinar->setConvocationType($communicationType)
            ->setContent($convocation->getContent())
            ->setSendCommunicationTo(WebinarConvocation::SEND_TO_ALL)
            ->setConvocationDate((new \DateTimeImmutable())->modify('+2 hours'));

        if ($communicationWebinar->getConvocationType() === WebinarConvocation::TYPE_CONFIRMATION) {
            $communicationWebinar->setIcs($convocation->getIcs());
        }

        $infos = [
            'message' => ['key' => 'A %type% webinar communication has been added', 'params' => ['%type%' => $communicationType]],
            'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
        ];
        $this->activityLogger->initActivityLogContext($session->getClient(), $user, ActivityLog::ORIGIN_USER_INTERFACE);
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_ADD_NEW_COMMUNICATION_WEBINAR, ActivityLog::SEVERITY_INFORMATION, $infos), true);
        $this->activityLogger->flushActivityLogs();

        return $communicationWebinar;
    }

    public function addNewCommunicationSMSWebinar(string $communicationType, ProviderSession $session, User $user): WebinarConvocation
    {
        $communicationWebinar = new WebinarConvocation(null, $session);

        $communicationWebinar->setConvocationType($communicationType)
            ->setContent('')
            ->setSendCommunicationTo(WebinarConvocation::SEND_TO_ALL)
            ->setConvocationDate((new \DateTimeImmutable())->modify('+2 hours'))
            ->setSender(StrUtils::senderFormated($session->getClient()->getName()));

        $infos = [
            'message' => ['key' => 'A %type% webinar communication has been added', 'params' => ['%type%' => $communicationType]],
            'session' => ['id' => $session->getId(), 'name' => $session->getName(), 'ref' => $session->getRef()],
        ];
        $this->activityLogger->initActivityLogContext($session->getClient(), $user, ActivityLog::ORIGIN_USER_INTERFACE);
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_ADD_NEW_COMMUNICATION_WEBINAR, ActivityLog::SEVERITY_INFORMATION, $infos), true);
        $this->activityLogger->flushActivityLogs();

        return $communicationWebinar;
    }

    public function existeCommunicationWebinarConfirmationType(ProviderSession $session): bool
    {
        foreach ($session->getWebinarConvocation() as $communication) {
            if ($communication->getConvocationType() == WebinarConvocation::TYPE_CONFIRMATION) {
                return true;
            }
        }

        return false;
    }

    public function linkWithICS(ProviderSession $session, ProviderParticipantSessionRole $ppsr): ?string
    {
        $ics = null;

        $convocationHaveIcs = false;
        if ($session->getCategory() !== CategorySessionType::CATEGORY_SESSION_WEBINAR) {
            if ($session->getConvocation()->getIcs()) {
                $convocationHaveIcs = true;
            }
        } else {
            /** @var WebinarConvocation $webinarConvocation */
            foreach ($session->getWebinarConvocation() as $webinarConvocation) {
                if ($webinarConvocation->getConvocationType() === WebinarConvocation::TYPE_CONFIRMATION && $webinarConvocation->getIcs()) {
                    $convocationHaveIcs = true;
                    break;
                }
            }
        }
        if ($convocationHaveIcs) {
            $loginUrl = $this->endPointDomain.$this->router->generate('_session_access_plateforme', ['token' => $ppsr->getProviderSessionAccessToken()->getToken()]);

            if ($session->getCategory() === CategorySessionType::CATEGORY_SESSION_WEBINAR) {
                $description = $this->translator->trans("Hello %ppsr_fullname%,<br/><br/>Thank you for registering for the webinar: \"%title%\" - On %date_start% at %time_start%.<br/><br/>To access the webinar on the day, log in to the event's virtual room and activate the sound:<br/>%link%<br/><br/>Got a problem? Have a question? A doubt? Don't hesitate to contact us, and one of our advisors will be happy to help: serviceclient@live-session.fr / 0970 407 907.<br/><br/>We look forward to welcoming you to this webinar,<br/><br/>The %client_name% team", [
                    '%ppsr_fullname%' => "{$ppsr->getParticipant()->getFirstName()} {$ppsr->getParticipant()->getLastName()}", // $ppsr->getParticipant()->getFullName(),
                    '%title%' => $session->getName(),
                    '%date_start%' => $session->getDateStart()->format('d/m/Y'),
                    '%time_start%' => $session->getDateStart()->format('H:i'),
                    '%link%' => $loginUrl,
                    '%client_name%' => $session->getClient()->getName(),
                ]);
            } else {
                $convocationUrl = $this->endPointDomain.$this->router->generate('_trainee_convocation_show', ['token' => $ppsr->getProviderSessionAccessToken()->getToken()]);
                $description = '- Rejoignez la session : '.$loginUrl.'<br>- Consultez la convocation : '.$convocationUrl.'<br><br>';
                $description .= $this->translator->trans('Would you like to change your registration? Contact your training manager directly to inform him/her. Responses to this "meeting" will be ignored.');
            }

            $location = $loginUrl;
            if ($session->isSessionPresential() && $session->getLocation()) {
                $location = $session->getLocation();
            } elseif ($session->isSessionMixte()) {
                $location = sprintf('%s - %s', $session->getAbstractProviderConfigurationHolder()->getName(), $session->getLocation());
            }

            $params = [
                'receiverName' => $ppsr->getParticipant()->getFullName(),
                'receiverEmail' => $ppsr->getParticipant()->getEmail(),
                'organizerName' => $session->getClient()->getSenderName(),
                'organizerEmail' => $session->getClient()->getSenderEmail(),
                'dateStart' => $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone())),
                'dateEnd' => $session->getDateEnd()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone())),
                'location' => $location,
                'uid' => 'a658e3c9-e011-4b50-9b8c-0f7568c06024-'.uniqid(),
                'description' => $description,
                'summary' => $session->getName().' ('.$session->getRef().')',
            ];
            $ics = $this->generateIcal($params);
        }

        return $ics;
    }

    public function isEmailValid(string $email): mixed
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public function generateIcal(array $params): string
    {
        $ical = 'BEGIN:VCALENDAR'."\n".
            'PRODID:-//Microsoft Corporation//Outlook 10.0 MIMEDIR//EN'."\n".
            'VERSION:2.0'."\n".
            'METHOD:REQUEST'."\n".
            'BEGIN:VTIMEZONE'."\n".
            'TZID:Europe/Paris'."\n".
            'LAST-MODIFIED:20221105T024526Z'."\n".
            'TZURL:https://www.tzurl.org/zoneinfo-outlook/Europe/Paris'."\n".
            'X-LIC-LOCATION:Europe/Paris'."\n".
            'BEGIN:DAYLIGHT'."\n".
            'TZNAME:CEST'."\n".
            'TZOFFSETFROM:+0100'."\n".
            'TZOFFSETTO:+0200'."\n".
            'DTSTART:19700329T020000'."\n".
            'RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU'."\n".
            'END:DAYLIGHT'."\n".
            'BEGIN:STANDARD'."\n".
            'TZNAME:CET'."\n".
            'TZOFFSETFROM:+0200'."\n".
            'TZOFFSETTO:+0100'."\n".
            'DTSTART:19701025T030000'."\n".
            'RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU'."\n".
            'END:STANDARD'."\n".
            'END:VTIMEZONE'."\n".
            'BEGIN:VEVENT'."\n".
            'DTSTAMP:'.(new \DateTime())->format("Ymd\THis\Z")."\n".
            'ATTENDEE;CN="'.$params['receiverName'].'";ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:'.$params['receiverEmail']."\n".
            'ORGANIZER;CN="'.$params['organizerName'].'":MAILTO:'.$params['organizerEmail']."\n".
            'DTSTART;TZID=Europe/Paris:'.$params['dateStart']->format("Ymd\THis")."\n".
            'DTEND;TZID=Europe/Paris:'.$params['dateEnd']->format("Ymd\THis")."\n".
            'LOCATION:'.$params['location']."\n".
            'TRANSP:OPAQUE'."\n".
            'SEQUENCE:0'."\n".
            'UID:'.$params['uid']."\n".
            'DESCRIPTION:'.$params['description']."\n".
            'X-ALT-DESC;FMTTYPE=text/html:'.str_replace(["\n"], ["\t"], $params['description'])."\n".
            'SUMMARY:'.$params['summary']."\n".
            'PRIORITY:5'."\n".
            'CLASS:PUBLIC'."\n".
            'END:VEVENT'."\n".
            'END:VCALENDAR'."\n";

        return $ical;
    }

    public function sendUpdateSessionMail(ProviderParticipantSessionRole $participantRole, ProviderSession $session, array $changes = []): void
    {
        $convocation = $session->getConvocation();
        $participant = $participantRole->getParticipant();
        $lang = $participant->getUser()->getPreferredLang();

        $address = new Address($participant->getEmail(), "{$participant->getFirstName()} {$participant->getLastName()}");

        $subject = $this->translator->trans('Important: update session "%title%" from %date% to %time%.', [
            '%title%' => $session->getName(),
            '%date%' => (clone $session->getDateStart())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format($lang === 'en' ? 'Y/m/d' : 'd/m/Y'),
            '%time%' => (clone $session->getDateStart())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('H:i'),
        ]);

        $content = $this->transformShortsCodes($convocation->getContent(), $participantRole);

        $this->mailerService->sendTemplatedMail(
            $address,
            $subject,
            'emails/participant/session_updated.html.twig',
            [
                'lang' => $lang,
                'listChanges' => $changes,
                'content' => $content,
                'session' => $session,
                'client' => $session->getClient()->getName(),
                'clientService' => $session->getClient(),
                'clientTimeZone' => $session->getClient()->getTimezone(),
            ],
            metadatas: [
                'origin' => MailerService::ORIGIN_EMAIL_SEND_SESSION_UPDATE,
                'user' => $participant->getUser()->getId(),
                'session' => $session->getId(),
            ],
            ccs: $this->referrerService->getReferrerFromUser($participantRole)
        );
    }
}
