<?php

namespace App\Service;

use App\Entity\ActivityLog;
use App\Entity\Client\Client;
use App\Entity\Client\EvaluationModalMail;
use App\Entity\EvaluationProviderSession;
use App\Entity\User;
use App\Exception\ClientNoMoreCreditAvailableException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ClientService
{
    public function __construct(
        private UserService $userService,
        private ActivityLogger $activityLogger,
        private ClientRepository $clientRepository,
        private TranslatorInterface $translator,
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function reportActiveClients(): array
    {
        return $this->clientRepository->findBy(['active' => true]);
    }

    public function getAllConfigurationHolder(array $clients): array
    {
        $configurationsHolder = [];
        foreach ($clients as $client) {
            if ($client->getConfigurationHoldersActive()->getValues() != []) {
                $configurationsHolder = array_merge($configurationsHolder, $client->getConfigurationHoldersActive()->getValues());
            }
        }

        return $configurationsHolder;
    }

    public function clientUseCredit(Client $client): void
    {
        if (
            $client->getBillingMode() === Client::BILLING_MODE_CREDIT
            && $client->getRemainingCredit() <= 0
        ) {
            throw new ClientNoMoreCreditAvailableException();
        }

        $client->incrementSessionCount();

        if ($client->getBillingMode() === Client::BILLING_MODE_CREDIT && $client->getRemainingCredit() <= $client->getOptionFromAlert(Client::NOTIFICATION_NO_MORE_CREDIT_OPTION_LIMIT)) {
            if ($client->getRemainingCredit() === 0) {
                $infos = [
                    'message' => ['key' => 'Your account has no credit left. Contact your Live Session advisor to add credit.', 'params' => ['%remaining_credit%' => $client->getRemainingCredit()]],
                    'client' => ['id' => $client->getId(), 'name' => $client->getName()],
                ];
                $activityLog = new ActivityLogModel(ActivityLog::ACTION_SESSION_CREATION, ActivityLog::SEVERITY_ERROR, $infos);
                $this->activityLogger->addActivityLog($activityLog, true);
            } elseif (
                $client->isSubscribeToAlert(Client::NOTIFICATION_NO_MORE_CREDIT) &&
                $client->getRemainingCredit() === (int) $client->getOptionFromAlert(Client::NOTIFICATION_NO_MORE_CREDIT_OPTION_LIMIT)
            ) {
                $this->userService->sendMailToClientUsersSubscribeToEmail(
                    $client,
                    User::EMAIL_NO_MORE_CREDIT,
                    $this->translator->trans('Your credits are almost used up'),
                    'emails/alert/credit_almost_exhausted.html.twig',
                    context: ['client' => $client],
                    metadatas: ['origin' => MailerService::ORIGIN_EMAIL_NO_MORE_CREDIT],
                );

                $infos = [
                    'message' => ['key' => 'Your account only has %remaining_credit%. You can contact your Live Session advisor  to add more.', 'params' => ['%remaining_credit%' => $client->getRemainingCredit()]],
                    'client' => ['id' => $client->getId(), 'name' => $client->getName()],
                ];
                $activityLog = new ActivityLogModel(ActivityLog::ACTION_SESSION_CREATION, ActivityLog::SEVERITY_WARNING, $infos);
                $this->activityLogger->addActivityLog($activityLog, true);
            }
        }
    }

    public function clientUseCreditParticipants(Client $client): void
    {
        if (
            $client->getBillingMode() === Client::BILLING_MODE_PARTICIPANT
            && $client->getRemainingCreditParticipants() <= 0
        ) {
            throw new ClientNoMoreCreditAvailableException();
        }

        $client->incrementParticipantCount();
        $this->notificationClientUseCredit($client);
    }

    public function clientUseCreditParticipantsWithQuantity(Client $client, int $quantity): void
    {
        if (
            $client->getBillingMode() === Client::BILLING_MODE_PARTICIPANT
            && $client->getRemainingCreditParticipants() < $quantity
        ) {
            throw new ClientNoMoreCreditAvailableException();
        }

        $client->incrementParticipantCountWithQuantity($quantity);
        $this->notificationClientUseCredit($client);
    }

    public function notificationClientUseCredit(Client $client): void
    {
        if ($client->getBillingMode() === Client::BILLING_MODE_PARTICIPANT && $client->getRemainingCreditParticipants() <= $client->getOptionFromAlert(Client::NOTIFICATION_NO_MORE_CREDIT_OPTION_LIMIT)) {
            if ($client->getRemainingCreditParticipants() === 0) {
                $infos = [
                    'message' => ['key' => 'Your account has no credit left. Contact your Live Session advisor to add credit.', 'params' => ['%remaining_credit%' => $client->getRemainingCreditParticipants()]],
                    'client' => ['id' => $client->getId(), 'name' => $client->getName()],
                ];
                $activityLog = new ActivityLogModel(ActivityLog::ACTION_PARTICIPANT_ADD, ActivityLog::SEVERITY_ERROR, $infos);
                $this->activityLogger->addActivityLog($activityLog, true);
            } elseif (
                $client->isSubscribeToAlert(Client::NOTIFICATION_NO_MORE_CREDIT) &&
                $client->getRemainingCreditParticipants() === (int) $client->getOptionFromAlert(Client::NOTIFICATION_NO_MORE_CREDIT_OPTION_LIMIT)
            ) {
                $this->userService->sendMailToClientUsersSubscribeToEmail(
                               $client,
                               User::EMAIL_NO_MORE_CREDIT,
                               $this->translator->trans('Your credits are almost used up'),
                               'emails/alert/credit_almost_exhausted.html.twig',
                    context: ['client' => $client],
                    metadatas: ['origin' => MailerService::ORIGIN_EMAIL_NO_MORE_CREDIT],
                );

                $infos = [
                    'message' => ['key' => 'Your account only has %remaining_credit%. You can contact your Live Session advisor  to add more.', 'params' => ['%remaining_credit%' => $client->getRemainingCreditParticipants()]],
                    'client' => ['id' => $client->getId(), 'name' => $client->getName()],
                ];
                $activityLog = new ActivityLogModel(ActivityLog::ACTION_PARTICIPANT_ADD, ActivityLog::SEVERITY_WARNING, $infos);
                $this->activityLogger->addActivityLog($activityLog, true);
            }
        }
    }

    /**
     * @throws ClientNoMoreCreditAvailableException
     */
    public function useCreditClient(Client $client, int $quantity): void
    {
        switch ($client->getBillingMode()) {
            case Client::BILLING_MODE_CREDIT:
                $this->clientUseCredit($client);
                break;
            case Client::BILLING_MODE_PARTICIPANT:
                $this->clientUseCreditParticipantsWithQuantity($client, $quantity);
                break;
        }
    }

    public function createEvaluationModal(Client $client): void
    {
        $evaluationModalMailKnowledge = new EvaluationModalMail($client, EvaluationProviderSession::EVALUATION_KNOWLEDGE, $client->getDefaultLang());
        $evaluationModalMailAcquired = new EvaluationModalMail($client, EvaluationProviderSession::EVALUATION_ACQUIRED, $client->getDefaultLang());
        $evaluationModalMailCold = new EvaluationModalMail($client, EvaluationProviderSession::EVALUATION_COLD, $client->getDefaultLang());
        $evaluationModalMailHot = new EvaluationModalMail($client, EvaluationProviderSession::EVALUATION_HOT, $client->getDefaultLang());
        $evaluationModalMailPositioning = new EvaluationModalMail($client, EvaluationProviderSession::EVALUATION_POSITIONING, $client->getDefaultLang());

        $this->entityManager->persist($evaluationModalMailKnowledge);
        $this->entityManager->persist($evaluationModalMailAcquired);
        $this->entityManager->persist($evaluationModalMailCold);
        $this->entityManager->persist($evaluationModalMailHot);
        $this->entityManager->persist($evaluationModalMailPositioning);
    }
}
