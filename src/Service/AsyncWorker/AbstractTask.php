<?php

namespace App\Service\AsyncWorker;

use App\Entity\ActivityLog;
use App\Entity\AsyncTask;
use App\Service\ActivityLogger;
use App\Service\ActivityLogService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractTask implements TaskInterface, LoggerAwareInterface
{
    protected LoggerInterface $logger;

    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected ActivityLogService $activityLogService,
        protected ActivityLogger $activityLogger
    ) {
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    public function setEntityManager(EntityManagerInterface $entityManager): AbstractTask
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    public function getActivityLogService(): ActivityLogService
    {
        return $this->activityLogService;
    }

    public function setActivityLogService(ActivityLogService $activityLogService): AbstractTask
    {
        $this->activityLogService = $activityLogService;

        return $this;
    }

    public function generateLogContext(AsyncTask $task, array $importData): void
    {
        $this->activityLogger->initActivityLogContext(
            $task->getClientOrigin(),
            $task->getUserOrigin(),
            ActivityLog::ORIGIN_IMPORT,
            [
                'flagOrigin' => $task->getFlagOrigin(),
                'fileOrigin' => $task->getFileOrigin(),
                'rawData' => $importData,
                'numlinefile' => $importData['numlinefile'],
            ]
        );
    }

    public function areAllAsyncTaskDoneForFlagOrigin(string $flagOrigin): bool
    {
        return $this->entityManager->getRepository(AsyncTask::class)
            ->count(['flagOrigin' => $flagOrigin, 'status' => [TaskInterface::STATUS_READY]]) === 0;
    }
}
