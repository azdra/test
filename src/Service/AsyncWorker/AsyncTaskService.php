<?php

namespace App\Service\AsyncWorker;

use App\Entity\AsyncTask;
use App\Entity\Client\Client;
use App\Model\GeneratorTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final class AsyncTaskService
{
    use GeneratorTrait;

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function publish(string $taskClassName, array $data = [], ?string $flagOrigin = null, ?Client $clientOrigin = null, ?string $fileOrigin = null, ?UserInterface $userOrigin = null, array $infos = []): AsyncTask
    {
        $task = (new AsyncTask())
            ->setClassName($taskClassName)
            ->setData($data)
            ->setStatus(TaskInterface::STATUS_READY)
            ->setInfos($infos);

        if (!empty($flagOrigin)) {
            $task->setFlagOrigin($flagOrigin);
        }

        if (!empty($clientOrigin)) {
            $task->setClientOrigin($clientOrigin);
        }

        if (!empty($fileOrigin)) {
            $task->setFileOrigin($fileOrigin);
        }

        if (!empty($userOrigin)) {
            $task->setUserOrigin($userOrigin);
        }

        $this->entityManager->persist($task);

        return $task;
    }

    public function randomFlag(int $length = 15): string
    {
        return $this->randomString($length);
    }
}
