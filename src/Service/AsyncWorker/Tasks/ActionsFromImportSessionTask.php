<?php

namespace App\Service\AsyncWorker\Tasks;

use App\Entity\ActivityLog;
use App\Entity\AsyncTask;
use App\Entity\CategorySessionType;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\ConfigurationHolder\AbstractProviderConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexConfigurationHolder;
use App\Entity\ConfigurationHolder\Cisco\WebexRestConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsConfigurationHolder;
use App\Entity\ConfigurationHolder\Microsoft\MicrosoftTeamsEventConfigurationHolder;
use App\Entity\ConfigurationHolder\White\WhiteConfigurationHolder;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Exception\AsyncWorker\AsyncTaskException;
use App\Exception\InvalidImportDataException;
use App\Exception\InvalidProviderConfigurationHandler;
use App\Exception\MiddlewareFailureException;
use App\Exception\ProviderSessionRequestHandlerException;
use App\Exception\UnknownProviderConfigurationHolderException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Model\ValidatorTrait;
use App\Repository\AbstractProviderConfigurationHolderRepository;
use App\Repository\ConvocationRepository;
use App\Repository\ProviderConfigurationHolderRepository;
use App\Repository\ProviderSessionRepository;
use App\Service\ActivityLogger;
use App\Service\ActivityLogService;
use App\Service\AdobeConnectService;
use App\Service\AsyncWorker\AbstractTask;
use App\Service\MiddlewareService;
use App\Service\Provider\Adobe\AdobeConnectScoArrayHandler;
use App\Service\Provider\Adobe\AdobeConnectWebinarScoArrayHandler;
use App\Service\Provider\Adobe\AdobeSessionImporter;
use App\Service\Provider\Adobe\AdobeWebinarSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamAdobeSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamAdobeWebinarSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamMicrosoftTeamsEventSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamMicrosoftTeamsSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamWebexRestSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamWebexSessionImporter;
use App\Service\Provider\ClientsImporter\IFCAM\IfcamWhiteSessionImporter;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\Provider\Microsoft\MicrosoftTeamsArrayHandler;
use App\Service\Provider\Microsoft\MicrosoftTeamsEventArrayHandler;
use App\Service\Provider\Microsoft\MicrosoftTeamsEventSessionImporter;
use App\Service\Provider\Microsoft\MicrosoftTeamsSessionImporter;
use App\Service\Provider\Model\LicenceChecker;
use App\Service\Provider\Model\SessionImporter;
use App\Service\Provider\Webex\WebexMeetingArrayHandler;
use App\Service\Provider\Webex\WebexRestMeetingArrayHandler;
use App\Service\Provider\Webex\WebexRestSessionImporter;
use App\Service\Provider\Webex\WebexSessionImporter;
use App\Service\Provider\White\WhiteArrayHandler;
use App\Service\Provider\White\WhiteSessionImporter;
use App\Service\ProviderSessionService;
use function array_key_exists;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ActionsFromImportSessionTask extends AbstractTask
{
    use ValidatorTrait;

    public const ACTION_SESSION_ADD_OR_UPDATE = 1;
    public const ACTION_SESSION_DELETE = 2;
    public const ACTION_SESSION_CANCEL = 3;

    public const ACCESS_USER_ONLY = 1;
    public const ACCESS_VISITOR = 2;
    public const ACCESS_ALL = 3;

    public function __construct(
        EntityManagerInterface $entityManager,
        ActivityLogService $activityLogService,
        private ProviderSessionRepository $providerSessionRepository,
        private ConvocationRepository $convocationRepository,
        private MiddlewareService $middlewareService,
        private TranslatorInterface $translator,
        private EventDispatcherInterface $eventDispatcher,
        private ProviderConfigurationHolderRepository $providerConfigurationHolderRepository,
        private AdobeSessionImporter $adobeSessionImporter,
        private AdobeWebinarSessionImporter $adobeWebinarSessionImporter,
        private WebexSessionImporter $webexSessionImporter,
        private WebexRestSessionImporter $webexRestSessionImporter,
        private MicrosoftTeamsSessionImporter $microsoftTeamsSessionImporter,
        private MicrosoftTeamsEventSessionImporter $microsoftTeamsEventSessionImporter,
        private WhiteSessionImporter $whiteSessionImporter,
        private AdobeConnectScoArrayHandler $adobeConnectScoArrayHandler,
        private AdobeConnectWebinarScoArrayHandler $adobeConnectWebinarScoArrayHandler,
        private WebexMeetingArrayHandler $webexMeetingArrayHandler,
        private WebexRestMeetingArrayHandler $webexRestMeetingArrayHandler,
        private MicrosoftTeamsArrayHandler $microsoftTeamsArrayHandler,
        private MicrosoftTeamsEventArrayHandler $microsoftTeamsEventArrayHandler,
        private WhiteArrayHandler $whiteArrayHandler,
        private ProviderSessionService $providerSessionService,
        ActivityLogger $activityLogger,
        private ParticipantSessionSubscriber $participantSessionSubscriber,
        private RoleHierarchyInterface $roleHierarchy,
        private AbstractProviderConfigurationHolderRepository $configurationHolderRepository,
        private LicenceChecker $licenceChecker,
        private AdobeConnectService $adobeConnectService,
        private IfcamAdobeSessionImporter $ifcamAdobeSessionImporter,
        private IfcamAdobeWebinarSessionImporter $ifcamAdobeWebinarSessionImporter,
        private IfcamWebexSessionImporter $ifcamWebexSessionImporter,
        private IfcamWebexRestSessionImporter $ifcamWebexRestSessionImporter,
        private IfcamMicrosoftTeamsSessionImporter $ifcamMicrosoftTeamsSessionImporter,
        private IfcamMicrosoftTeamsEventSessionImporter $ifcamMicrosoftTeamsEventSessionImporter,
        private IfcamWhiteSessionImporter $ifcamWhiteSessionImporter,
    ) {
        parent::__construct($entityManager, $activityLogService, $activityLogger);
    }

    public function execute(AsyncTask $task): void
    {
        if (!array_key_exists('rows', $task->getData())) {
            throw new AsyncTaskException($this->translator->trans('The data does not contain the "row" key'));
        }

        if (empty($task->getClientOrigin())) {
            throw new AsyncTaskException($this->translator->trans('Account is not specified'));
        }

        if (empty($task->getUserOrigin())) {
            throw new AsyncTaskException($this->translator->trans('The user was not found'));
        }

        $client = $task->getClientOrigin();
        if (!empty($task->getInfos()['configurationHolder'])) {
            $configurationHolder = $this->configurationHolderRepository->find($task->getInfos()['configurationHolder']);
        } else {
            $configurationHolder = $client->getConfigurationHolders()->first();
        }

        $importer = $this->getImporter($configurationHolder);

        $arrResults = ['rows' => []];

        foreach ($task->getData()['rows'] as $values) {
            $this->generateLogContext($task, $values);
            $data = [];
            try {
                $data = $importer->extractData($values, $client, $configurationHolder);
                $importer->validateData($data, $configurationHolder);
                $this->validateAction($client, $task->getUserOrigin(), $data);
                if ($data['action'] == self::ACTION_SESSION_ADD_OR_UPDATE) {
                    if ($this->haveAudioAvailable($data, $configurationHolder)) {
                        $importer->checkWarnings($data);
                        $data['message_info'] = $this->createOrUpdateAction($data, $configurationHolder, $task->getUserOrigin());
                        if (!array_key_exists('success', $data)) {
                            $data['success'] = true;
                        }
                    } else {
                        $data['success'] = false;
                        $data['message_info'] = [
                            'key' => 'Error while importing session \'%sessionName%\': All audios are already used during this period',
                            'params' => ['%sessionName%' => $data['title']],
                        ];
                    }
                } else {
                    $data['message_info'] = match ($data['action']) {
                        self::ACTION_SESSION_DELETE => $this->deleteAction($data),
                        self::ACTION_SESSION_CANCEL => $this->cancelAction($data),
                        default => throw new AsyncTaskException($this->translator->trans('An error has occurred'))
                    };
                    $data['success'] = true;
                }
            } catch (ProviderSessionRequestHandlerException $e) {
                $data['success'] = false;
                $data['message_info'] = ['key' => $e->getMessage()];
                $this->logger->error($e, [
                    'asyncTaskId' => $task->getId(),
                    'sessionId' => $this->getProviderSessionIdFromData($data),
                ]);
            } catch (InvalidImportDataException $e) {
                $data['success'] = false;
                $data['message_info'] = ['key' => $e->getKey(), 'params' => $e->getParams()];
                $this->logger->error($e, [
                    'asyncTaskId' => $task->getId(),
                    'sessionId' => $this->getProviderSessionIdFromData($data),
                ]);
            } catch (MiddlewareFailureException $e) {
                $data['success'] = false;
                $data['message_info'] = ['key' => $e->getMessage().$this->translator->trans(' for the session %name%'), 'params' => ['%name%' => $data['title']]];
                $data['additionalData'] = $e->getData();
                $this->logger->error($e, [
                    'asyncTaskId' => $task->getId(),
                    'sessionId' => $this->getProviderSessionIdFromData($data),
                    'fieldErrors' => $e->getData(),
                ]);
            } catch (AsyncTaskException|\Exception $e) {
                $data['success'] = false;
                $data['message_info'] = ['key' => 'An error occurred while importing the session %name%', 'params' => ['%name%' => $data['title']]];
                $this->logger->error($e, [
                    'asyncTaskId' => $task->getId(),
                    'sessionId' => $this->getProviderSessionIdFromData($data),
                ]);
            }

            $arrResults['rows'][] = [
                'action' => $data['action'],
                'title' => $data['title'] ?? null,
                'dateStart' => !empty($data['dateStart']) ? $data['dateStart']->format('d/m/y H:i') : null,
                'dateEnd' => !empty($data['dateEnd']) ? $data['dateEnd']->format('d/m/Y H:i') : null,
                'duration' => !empty($data['duration']) ? $data['duration']->format('H:i') : null,
                'convocation' => $data['convocation'] ?? null,
                'category' => $data['category'] ?? null,
                'type' => $data['success'] ? 'success' : 'error',
                'message' => $this->translator->trans($data['message_info']['key'], $data['message_info']['params'] ?? []),
                'otherData' => $data['additionalData'] ?? [],
                'warnings' => [],
                'date' => ( new DateTime() )->format('d/m/Y H:m'),
                'idsession' => '',
            ];

            $task->setResult($arrResults);
            $this->getEntityManager()->flush();

            if ($this->activityLogger->isActivityLogContextInitiated() && !array_key_exists('flagOrigin', $this->activityLogger->getActivityLogContext()->infos)) {
                $this->generateLogContext($task, $values);
            }
            $this->addActivityLog($data);
        }

        if ($this->areAllAsyncTaskDoneForFlagOrigin($task->getFlagOrigin())) {
            $resultsImport = $this->activityLogService->prepareEmail($task, $this->getOriginTask());
            $this->addActivityLogForFullImport(resultsImport: $resultsImport, task: $task);
        }
    }

    /**
     * @throws InvalidImportDataException
     */
    public function validateAction(Client $client, User $user, array $data): void
    {
        $roles = $this->roleHierarchy->getReachableRoleNames($user->getRoles());
        if (array_search('ROLE_ADMIN', $roles)) {
            return;
        }

        if (false === array_search('ROLE_MANAGER', $roles)) {
            throw new InvalidImportDataException('You do not have the role needed to import data!', [], $this->translator->trans('You do not have the role needed to import data!'));
        }

        /** @var ProviderSession $session */
        $session = $data['providerSession'];
        if (!empty($session)) {
            if ($session->isFinished() || $session->isRunning()) {
                if ($data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_DELETE) {
                    throw new InvalidImportDataException('You can not delete a session "%title%" which is finished or running!', ['%title%' => $data['title']], $this->translator->trans('You can not delete a session "%title%" which is finished or running!', ['%title%' => $data['title']]));
                } elseif ($data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_CANCEL) {
                    throw new InvalidImportDataException('You can not cancel a session "%title%" which is finished or running!', ['%title%' => $data['title']], $this->translator->trans('You can not cancel a session "%title%" which is finished or running!', ['%title%' => $data['title']]));
                } else {
                    throw new InvalidImportDataException('You can not change the "%title%" session that is completed or in progress!', ['%title%' => $data['title']], $this->translator->trans('You can not change the "%title%" session that is completed or in progress!', ['%title%' => $data['title']]));
                }
            }
            if ($session->isADraft() && $data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_CANCEL) {
                throw new InvalidImportDataException('You can not cancel a session "%title%" which is a draft!', ['%title%' => $data['title']], $this->translator->trans('You can not cancel a session "%title%" which is a draft!', ['%title%' => $data['title']]));
            }
        } elseif ($client->getBillingMode() === Client::BILLING_MODE_CREDIT && $client->getRemainingCredit() <= 0) {
            throw new InvalidImportDataException('Your account has no credit left to create the "%title%" session! Contact your Live Session Advisor to add credits', ['%title%' => $data['title']], $this->translator->trans('Your account has no credit left to create the "%title%" session! Contact your Live Session Advisor to add credits', ['%title%' => $data['title']]));
        }
    }

    public function addActivityLog(array $data): void
    {
        $this->activityLogger->addDataToActivityLogContext('success', $data['success']);
        $this->activityLogger->addDataToActivityLogContext('session', $this->extractSessionDataForLog($data));
        if (!$data['success']) {
            $this->activityLogger->addActivityLog(new ActivityLogModel($data['logAction'], ActivityLog::SEVERITY_ERROR, ['message' => $data['message_info']]), true);
        }
        $this->activityLogger->flushActivityLogs();
    }

    public function haveAudioAvailable(array $data, AbstractProviderConfigurationHolder $configurationHolder): bool
    {
        if ($configurationHolder instanceof AdobeConnectConfigurationHolder && $data['convocationObj']->getCategoryTypeAudio() != AbstractProviderAudio::AUDIO_CATEGORY_COMPUTER) {
            if (($this->adobeConnectService->getAvailableAudios($data['dateStart'], $data['dateEnd'], $data['convocationObj']->getTypeAudio(), $data['convocationObj']->getLanguages(), $configurationHolder)) == []) {
                return false;
            }
        }

        return true;
    }

    public function addActivityLogForFullImport(array $resultsImport, AsyncTask $task): void
    {
        $extractResultImport = $this->activityLogService->extractResultImportDataForLog($resultsImport);
        $this->activityLogger->initActivityLogContext(
            $task->getClientOrigin(),
            $task->getUserOrigin(),
            ActivityLog::ORIGIN_IMPORT,
            [
                'flagOrigin' => $task->getFlagOrigin(),
                'fileOrigin' => $task->getFileOrigin(),
                'taskId' => $task->getId(),
            ]
        );
        $infos = [
            'message' => ['key' => 'Importing sessions and sending the import report on %date% to %hours%.', 'params' => ['%date%' => (new \DateTime())->format('d/m/Y'), '%hours%' => (new \DateTime())->setTimezone(new \DateTimeZone($task->getClientOrigin()->getTimezone()))->format('H:i')]],
            'infosResult' => $extractResultImport,
            'typeImport' => ActivityLog::ACTION_IMPORT_SESSIONS,
            'links' => [
                ['type' => 'component', 'name' => 'ModalImportInfo'],
                ['type' => 'link', 'routeKey' => 'download_import_file', 'routeParam' => ['id' => $task->getId()], 'transKey' => 'Download File'],
            ],
        ];
        $this->activityLogger->addDataToActivityLogContext('infosResult', $extractResultImport);
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_IMPORT_SESSIONS, ActivityLog::SEVERITY_INFORMATION, $infos), true);
        $this->activityLogger->flushActivityLogs();
    }

    public function extractSessionDataForLog(array $data): array
    {
        if (empty($data['providerSession']) || $data['action'] === ActionsFromImportSessionTask::ACTION_SESSION_DELETE) {
            return ['name' => $data['reference'], 'ref' => $data['reference']];
        }

        return ['name' => $data['providerSession']->getName(), 'id' => $data['providerSession']->getId(), 'ref' => $data['providerSession']->getRef()];
    }

    public function getOriginTask(): string
    {
        return ActivityLog::ACTION_IMPORT_SESSIONS;
    }

    public function getImporter(AbstractProviderConfigurationHolder $configurationHolder): SessionImporter
    {
        if ($configurationHolder->getClient()->getId() == Client::ID_CLIENT_IFCAM) {
            return match (get_class($configurationHolder)) {
                AdobeConnectConfigurationHolder::class => $this->ifcamAdobeSessionImporter,
                AdobeConnectWebinarConfigurationHolder::class => $this->ifcamAdobeWebinarSessionImporter,
                WebexConfigurationHolder::class => $this->ifcamWebexSessionImporter,
                WebexRestConfigurationHolder::class => $this->ifcamWebexRestSessionImporter,
                MicrosoftTeamsConfigurationHolder::class => $this->ifcamMicrosoftTeamsSessionImporter,
                MicrosoftTeamsEventConfigurationHolder::class => $this->ifcamMicrosoftTeamsEventSessionImporter,
                WhiteConfigurationHolder::class => $this->ifcamWhiteSessionImporter,
                default => throw new InvalidProviderConfigurationHandler()
            };
        } else {
            return match (get_class($configurationHolder)) {
                AdobeConnectConfigurationHolder::class => $this->adobeSessionImporter,
                AdobeConnectWebinarConfigurationHolder::class => $this->adobeWebinarSessionImporter,
                WebexConfigurationHolder::class => $this->webexSessionImporter,
                WebexRestConfigurationHolder::class => $this->webexRestSessionImporter,
                MicrosoftTeamsConfigurationHolder::class => $this->microsoftTeamsSessionImporter,
                MicrosoftTeamsEventConfigurationHolder::class => $this->microsoftTeamsEventSessionImporter,
                WhiteConfigurationHolder::class => $this->whiteSessionImporter,
                default => throw new InvalidProviderConfigurationHandler()
            };
        }
    }

    /**
     * @throws AsyncTaskException
     * @throws InvalidProviderConfigurationHandler
     * @throws MiddlewareFailureException
     * @throws UnknownProviderConfigurationHolderException
     */
    private function createOrUpdateAction(array &$data, AbstractProviderConfigurationHolder $configurationHolder, User $userOrigin): array
    {
        try {
            if (empty($data['providerSession'])) {
                $data['providerSession'] = $this->providerSessionService->createSessionFromArray(
                    $configurationHolder, $userOrigin, $data
                );

                $message_key = 'The session (%ref%) has been successfully added';
                if ($data['providerSession']->getCategory()->value === CategorySessionType::CATEGORY_SESSION_WEBINAR->value && !$data['providerSession']->hasACommunicationConfirmation()) {
                    $data['providerSession']->setStatus(ProviderSession::STATUS_DRAFT);
                    $message_key = 'The session (%ref%) is locked in the draft state until you add a communication scheme';
                }

                $this->entityManager->flush();

                if (!$configurationHolder instanceof WhiteConfigurationHolder && !$configurationHolder instanceof WebexRestConfigurationHolder) {
                    $this->licenceChecker->checkLicence($data['providerSession']);
                }

                return [
                    'key' => $message_key,
                    'params' => ['%ref%' => $data['reference']], ];
            }
            if ($configurationHolder instanceof WebexRestConfigurationHolder && $data['providerSession']->getCategory()->value !== $data['category']) {
                $message_key = 'The session (%ref%) has not been modified because the category can not be changed in Webex Rest connector.';
                $data['success'] = false;

                return [
                    'key' => $message_key,
                    'params' => ['%ref%' => $data['reference']], ];
            }
            $this->providerSessionService->updateSessionFromArray(
                $configurationHolder, $data['providerSession'], $userOrigin, $data
            );

            return [
                'key' => 'The session (%ref%) has been successfully modified',
                'params' => ['%ref%' => $data['reference']], ];
        } catch (ProviderSessionRequestHandlerException $exception) {
            $this->logger->error($exception, [
                'key' => $exception->getMessage(),
                'params' => $exception->getParams(),
            ]);

            return [
                'key' => $exception->getMessage(),
                'params' => $exception->getParams(), ];
        }
    }

    private function deleteAction(array $data): array
    {
        $this->providerSessionService->deleteSession($data['providerSession']);

        return ['key' => 'The session (%ref%) has been successfully  deleted', 'params' => ['%ref%' => $data['reference']]];
    }

    private function cancelAction(array $data): array
    {
        $this->providerSessionService->cancelSession($data['providerSession']);

        return ['key' => 'The session (%ref%) has been  successfully cancelled', 'params' => ['%ref%' => $data['reference']]];
    }

    private function getProviderSessionIdFromData(array $data): ?int
    {
        if (empty($data['providerSession'])) {
            return null;
        }

        return $this->entityManager->contains($data['providerSession'])
            ? $data['providerSession']->getId()
            : null;
    }
}
