<?php

namespace App\Service\AsyncWorker\Tasks;

use App\Entity\AsyncTask;
use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Exception\AsyncWorker\AsyncTaskException;
use App\Service\AsyncWorker\AbstractTask;
use App\Service\Connector\Microsoft\MicrosoftTeamsEventConnector;
use Doctrine\ORM\EntityManagerInterface;

final class SyncSessionRoleToMicrosoftTeamsEventSessionTask extends AbstractTask
{
    protected EntityManagerInterface $entityManager;

    private MicrosoftTeamsEventConnector $microsoftTeamsEventConnector;

    public function __construct(EntityManagerInterface $entityManager, MicrosoftTeamsEventConnector $microsoftTeamsEventConnector)
    {
        $this->entityManager = $entityManager;
        $this->microsoftTeamsEventConnector = $microsoftTeamsEventConnector;
    }

    public function execute(AsyncTask $task): void
    {
        if (!\array_key_exists('session_id', $task->getData())) {
            throw new AsyncTaskException('Data does not contains "session_id" key.');
        }

        /** @var MicrosoftTeamsEventSession $session */
        $session = $this->entityManager->getRepository(MicrosoftTeamsEventSession::class)->find($task->getData()['session_id']);

        if (!$session) {
            throw new AsyncTaskException(sprintf('Session with id #%d could not be find.', $task->getData()['session_id']));
        }

        $response = $this->microsoftTeamsEventConnector->call(
            $session->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsEventConnector::ACTION_SESSION_UPDATE,
            $session
        );

        if (!$response->isSuccess()) {
            $exception = new AsyncTaskException((string) $response->getThrown());
            $this->logger->error($exception);
            throw new AsyncTaskException((string) $response->getThrown());
        }

        /** @var ProviderParticipantSessionRole $participantRole */
        foreach ($session->getParticipantRoles() as $participantRole) {
            $participantRole->setSyncedWithProvider(true);
        }

        $this->entityManager->flush();
    }

    public function getOriginTask(): string
    {
        return 'SyncSessionRoleMicrosoftTeamsEvent';
    }
}
