<?php

namespace App\Service\AsyncWorker\Tasks;

use App\Entity\AsyncTask;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\ActivityLogService;
use App\Service\AsyncWorker\AbstractTask;
use App\Service\Exporter\ExporterService;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetUserRGPDDatasExporter;
use App\Service\MailerService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

class ExportUserRGPDDataTask extends AbstractTask
{
    public function __construct(
        EntityManagerInterface $entityManager,
        ActivityLogService $activityLogService,
        ActivityLogger $activityLogger,
        protected LoggerInterface $logger,
        private ExporterService $exporterService,
        private UserRepository $userRepository,
        private MailerService $mailerService,
        private TranslatorInterface $translator,
        protected string $exportsSavefilesDirectory
    ) {
        parent::__construct($entityManager, $activityLogService, $activityLogger);
    }

    public function execute(AsyncTask $task): void
    {
        $datas = $task->getData();

        if (empty($datas['user']) || empty($datas['userOrigin'])) {
            throw new \Exception('Empty value for user or user origin, export cannot be done');
        }

        $user = $this->userRepository->find($datas['user']);
        $userOrigin = $this->userRepository->find($datas['userOrigin']);

        try {
            $response = $this->exporterService->generateArchive(
                AbstractSpreadsheetUserRGPDDatasExporter::EXPORT_TYPE,
                ExportFormat::XLSX,
                [
                    'user' => $user,
                    'userOrigin' => $userOrigin,
                ],
                false
            );

            $responseContent = json_decode($response->getContent());

            if ($response->getStatusCode() == Response::HTTP_OK) {
                $success = true;
                $message_info = '';
            } else {
                $success = false;
                $this->logger->error(new \Exception('An error has occurred while exporting RGDP datas for user :'), [
                    'User fullName' => $user->getFullName(),
                    'id Task' => $task->getId(),
                ]);
                $message_info = ['key' => 'An error has occurred while exporting RGDP datas for user : %user%', 'params' => ['%user%' => $user->getFullName()]];
            }
        } catch (Throwable $exception) {
            $this->logger->error($exception, [
                'id Task' => $task->getId(),
            ]);
            $success = false;
            $message_info = ['key' => $exception->getMessage(), 'params' => []];
        }

        $resultData = [
            'success' => $success,
            'date' => (new DateTime())->format('d/m/Y H:m'),
            'message' => !empty($message_info) ? $this->translator->trans($message_info['key'], $message_info['params']) : '',
        ];

        $task->setResult($resultData);
        $this->getEntityManager()->flush();

        if ($success) {
            $this->sendExtractAvalableMail($userOrigin, $responseContent);
        } else {
            $this->sendExtractGDPRFailedMail($userOrigin, $user);
        }
    }

    public function getOriginTask(): string
    {
        return 'ExportUserRGPDDataTask';
    }

    private function sendExtractAvalableMail(?UserInterface $userOrigin, mixed $responseContent): void
    {
        $this->mailerService->sendTemplatedMail(
            $userOrigin->getEmail(),
            $this->translator->trans('Extracted GDPR datas available', locale: $userOrigin->getPreferredLang()),
            'emails/export/result_export_gdpr_available.html.twig',
            [
                'user' => $userOrigin,
                'tokenFile' => $responseContent->message,
            ],
            null,
            [
                'origin' => MailerService::ORIGIN_EMAIL_RGPD_EXTRACT_DATAS_AVAILABLE,
                'user' => $userOrigin->getId(),
            ],
        );
    }

    private function sendExtractGDPRFailedMail(?UserInterface $userOrigin, UserInterface $user): void
    {
        $this->mailerService->sendTemplatedMail(
            $userOrigin->getEmail(),
            $this->translator->trans('GDPR datas for user extract failed', locale: $userOrigin->getPreferredLang()),
            'emails/export/failed_export_gdpr_available.html.twig',
            [
                'user' => $userOrigin,
                'userConcerned' => $user,
            ],
            null,
            [
                'origin' => MailerService::ORIGIN_EMAIL_RGPD_EXTRACT_DATAS_FAILED,
                'user' => $userOrigin->getId(),
            ],
        );
    }
}
