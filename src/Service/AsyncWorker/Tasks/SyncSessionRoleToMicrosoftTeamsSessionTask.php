<?php

namespace App\Service\AsyncWorker\Tasks;

use App\Entity\AsyncTask;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\ProviderParticipantSessionRole;
use App\Exception\AsyncWorker\AsyncTaskException;
use App\Service\AsyncWorker\AbstractTask;
use App\Service\Connector\Microsoft\MicrosoftTeamsConnector;
use Doctrine\ORM\EntityManagerInterface;

final class SyncSessionRoleToMicrosoftTeamsSessionTask extends AbstractTask
{
    protected EntityManagerInterface $entityManager;

    private MicrosoftTeamsConnector $microsoftTeamsConnector;

    public function __construct(EntityManagerInterface $entityManager, MicrosoftTeamsConnector $microsoftTeamsConnector)
    {
        $this->entityManager = $entityManager;
        $this->microsoftTeamsConnector = $microsoftTeamsConnector;
    }

    public function execute(AsyncTask $task): void
    {
        if (!\array_key_exists('session_id', $task->getData())) {
            throw new AsyncTaskException('Data does not contains "session_id" key.');
        }

        /** @var MicrosoftTeamsSession $session */
        $session = $this->entityManager->getRepository(MicrosoftTeamsSession::class)->find($task->getData()['session_id']);

        if (!$session) {
            throw new AsyncTaskException(sprintf('Session with id #%d could not be find.', $task->getData()['session_id']));
        }

        $response = $this->microsoftTeamsConnector->call(
            $session->getAbstractProviderConfigurationHolder(),
            MicrosoftTeamsConnector::ACTION_SESSION_UPDATE,
            $session
        );

        if (!$response->isSuccess()) {
            $exception = new AsyncTaskException((string) $response->getThrown());
            $this->logger->error($exception);
            throw new AsyncTaskException((string) $response->getThrown());
        }

        /** @var ProviderParticipantSessionRole $participantRole */
        foreach ($session->getParticipantRoles() as $participantRole) {
            $participantRole->setSyncedWithProvider(true);
        }

        $this->entityManager->flush();
    }

    public function getOriginTask(): string
    {
        return 'SyncSessionRoleMicrosoftTeams';
    }
}
