<?php

namespace App\Service\AsyncWorker\Tasks;

use App\Entity\ActivityLog;
use App\Entity\AsyncTask;
use App\Entity\Client\Client;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Exception\AsyncWorker\AsyncTaskException;
use App\Exception\InvalidImportDataException;
use App\Exception\UnknownProviderConfigurationHolderException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Model\ValidatorTrait;
use App\Repository\ClientRepository;
use App\Repository\ProviderSessionRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\ActivityLogService;
use App\Service\AsyncWorker\AbstractTask;
use App\Service\MiddlewareService;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final class ActionsFromImportUserTask extends AbstractTask
{
    use ValidatorTrait;

    public const ACTION_USER_ADD_OR_UPDATE = 1;
    public const ACTION_USER_REMOVE = 2;
    public const ACTION_USER_ANONYMISATION = 3;

    public const ROLE_PARTICIPANT_ANIMATOR = 1;
    public const ROLE_PARTICIPANT_TRAINEE = 2;
    public const ROLE_PARTICIPANT_REMOVE = 3;

    public const LANG_FR = 'FR';
    public const LANG_EN = 'EN';

    public function __construct(
        EntityManagerInterface $entityManager,
        ActivityLogService $activityLogService,
        private UserPasswordHasherInterface $passwordHasher,
        private UserService $userService,
        private UserRepository $userRepository,
        private ClientRepository $clientRepository,
        private TranslatorInterface $translator,
        private ProviderSessionRepository $providerSessionRepository,
        private MiddlewareService $middlewareService,
        private ValidatorInterface $validator,
        private ParticipantSessionSubscriber $participantSessionSubscriber,
        ActivityLogger $activityLogger
    ) {
        parent::__construct($entityManager, $activityLogService, $activityLogger);
    }

    public function execute(AsyncTask $task): void
    {
        if (!\array_key_exists('rows', $task->getData())) {
            throw new AsyncTaskException($this->translator->trans('The data does not contain the "row" key'));
        }

        if (empty($task->getClientOrigin())) {
            throw new AsyncTaskException($this->translator->trans('Account is not specified'));
        }

        $client = $task->getClientOrigin();

        $arrResults = ['rows' => []];

        foreach ($task->getData()['rows'] as $row) {
            $this->generateLogContext($task, $row);
            try {
                $data = $this->getDataFromRow($row);
                $this->fulfillData($data, $client);
                $this->validateData($data);

                $data['message_info'] = match ($data['action']) {
                    self::ACTION_USER_ADD_OR_UPDATE => $this->createOrUpdateUser($data, $client),
                    self::ACTION_USER_ANONYMISATION => $this->anonymization($data),
                    self::ACTION_USER_REMOVE => $this->removeUser($data),
                    default => throw new InvalidImportDataException('This action "%action%" is not correct!', ['%action%' => $data['action']], $this->translator->trans('This action "%action%" is not correct!', ['%action%' => $data['action']]))
                };
                $data['success'] = true;
            } catch (InvalidImportDataException $e) {
                $data['success'] = false;
                $data['message_info'] = ['key' => $e->getKey(), 'params' => $e->getParams()];
                $this->logger->error($e);
            } catch (AsyncTaskException|UnknownProviderConfigurationHolderException $ae) {
                $data['message_info'] = ['key' => 'An error was detected on the line including the email "%email%"!', 'params' => ['%email%' => $data['email']]];
                $data['success'] = false;
                $this->logger->error($ae, [
                    'task' => $this->getOriginTask(),
                    'email' => $data['email'],
                ]);
            }

            $arrResults['rows'][] = [
                'email' => $data['email'],
                'type' => $data['success'] ? 'success' : 'error',
                'message' => $this->translator->trans($data['message_info']['key'], $data['message_info']['params']),
                'date' => ( new \DateTime() )->format('d/m/Y H:m'),
            ];

            $task->setResult($arrResults);
            $this->getEntityManager()->flush();

            $this->addActivityLog($data);
        }

        if ($this->areAllAsyncTaskDoneForFlagOrigin($task->getFlagOrigin())) {
            $resultsImport = $this->activityLogService->prepareEmail($task, $this->getOriginTask());
            $this->addActivityLogForFullImport(resultsImport: $resultsImport, task: $task);
        }
    }

    public function addActivityLogForFullImport(array $resultsImport, AsyncTask $task): void
    {
        $extractResultImport = $this->activityLogService->extractResultImportDataForLog($resultsImport);
        $this->activityLogger->initActivityLogContext(
            $task->getClientOrigin(),
            $task->getUserOrigin(),
            ActivityLog::ORIGIN_IMPORT,
            [
                'flagOrigin' => $task->getFlagOrigin(),
                'fileOrigin' => $task->getFileOrigin(),
                'taskId' => $task->getId(),
            ]
        );
        $infos = [
            'message' => ['key' => 'Importing users and sending the import report on %date% to %hours%.', 'params' => ['%date%' => (new \DateTime())->format('d/m/Y'), '%hours%' => (new \DateTime())->setTimezone(new \DateTimeZone($task->getClientOrigin()->getTimezone()))->format('H:i')]],
            'infosResult' => $extractResultImport,
            'typeImport' => ActivityLog::ACTION_IMPORT_USERS,
            'links' => [
                ['type' => 'component', 'name' => 'ModalImportInfo'],
                ['type' => 'link', 'routeKey' => 'download_import_file', 'routeParam' => ['id' => $task->getId()], 'transKey' => 'Download File'],
            ],
        ];
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_IMPORT_USERS, ActivityLog::SEVERITY_INFORMATION, $infos), true);
        $this->activityLogger->flushActivityLogs();
    }

    public function addActivityLog(array $data): void
    {
        $this->activityLogger->addDataToActivityLogContext('success', $data['success']);
        $this->activityLogger->addDataToActivityLogContext('user', ['name' => empty($data['user']) ? ($data['firstName'].' '.$data['lastName']) : $data['user']->getFullName(), 'id' => (empty($data['user']) || $data['action'] === self::ACTION_USER_REMOVE) ? null : $data['user']->getId()]);
        if (array_key_exists('session', $data) && $data['session'] instanceof ProviderSession) {
            $this->activityLogger->addDataToActivityLogContext('session', ['name' => $data['session']->getName(), 'id' => $data['session']->getId()]);
        }
        if (!$data['success']) {
            $this->activityLogger->addActivityLog(new ActivityLogModel($data['logAction'], ActivityLog::SEVERITY_ERROR, ['message' => $data['message_info']]), true);
        }
        $this->activityLogger->flushActivityLogs();
    }

    /**
     * @throws UnknownProviderConfigurationHolderException
     * @throws AsyncTaskException
     */
    public function createOrUpdateUser(array &$importData, Client $client): array
    {
        if (!empty($importData['user'])) {
            $message = ['key' => 'The user "%email%" has been updated successfully!', 'params' => ['%email%' => $importData['email']]];
        } else {
            $importData['user'] = new User();

            $importData['user']
                ->setEmail($importData['email'])
                ->setRoles([User::ROLE_TRAINEE])
                ->setClearPassword(uniqid())
                ->setClient($client)
                ->setStatus(User::STATUS_ACTIVE)
                ->setPreferredLang($client->getDefaultLang());

            $message = ['key' => 'The user %email%" has been successfully added!', 'params' => ['%email%' => $importData['email']]];
        }

        $importData['user']
            ->setReference($importData['reference'])
            ->setFirstName($importData['firstName'])
            ->setLastName($importData['lastName'])
            ->setPhone($importData['phoneNumber'])
            ->setCompany($importData['company'])
            ->setInformation($importData['information'])
            ->setUpdatedAt();

        if (array_key_exists('emailReferent', $importData) && !empty($importData['emailReferent'])) {
            $importData['user']->setEmailReferrer($importData['emailReferent']);
        }

        $violations = $this->validator->validate($importData['user']);

        if ($violations->count() > 0) {
            throw new InvalidImportDataException('An error was detected on the line including the email "%email%"!', ['%email%' => $importData['email']], $this->translator->trans('An error was detected on the line including the email "%email%"!', ['%email%' => $importData['email']]));
        }

        $this->getEntityManager()->persist($importData['user']);
        $this->getEntityManager()->flush();

        if (!empty($importData['session'])) {
            $this->participantSessionSubscriber->subscribe(
                $importData['session'],
                $importData['user'],
                $this->getRoleByExcelValue($importData['roleSession']),
                $importData['situationMixte'] ?? ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL
            );
        }

        return $message;
    }

    /**
     * @throws AsyncTaskException
     */
    public function anonymization(array $data): array
    {
        if (empty($data['user'])) {
            throw new InvalidImportDataException('This user cannot be anonymized because the email "%email%" does not exist!', ['%email%' => $data['email']], $this->translator->trans('This user cannot be anonymized because the email "%email%" does not exist!', ['%email%' => $data['email']]));
        }

        $this->userService->anonymize($data['user']);

        return ['key' => 'The user "%email%" has been successfully anonymized!', 'params' => ['%email%' => $data['email']]];
    }

    /**
     * @throws AsyncTaskException
     */
    public function removeUser(array $data): array
    {
        if (empty($data['user'])) {
            throw new InvalidImportDataException('This user could not be deleted because the email "%email%" does not exist!', ['%email%' => $data['email']], $this->translator->trans('This user could not be deleted because the email "%email%" does not exist!', ['%email%' => $data['email']]));
        }

        $data['user']->setStatus(User::STATUS_ARCHIVE);

        if (count($data['user']->getParticipants()) >= 1) {
            foreach ($data['user']->getParticipants() as $participant) {
                $this->middlewareService->call(MiddlewareService::MIDDLEWARE_ACTION_DELETE_PARTICIPANT, $participant);
            }
        }

        $this->getEntityManager()->remove($data['user']);

        return ['key' => 'The user "%email%" has been successfully deleted!', 'params' => ['%email%' => $data['email']]];
    }

    public function getOriginTask(): string
    {
        return ActivityLog::ACTION_IMPORT_USERS;
    }

    protected function getCellValueFromDataLine(mixed $values, int $index, mixed $defaultValue = null): mixed
    {
        return (empty($values[$index])) ? $defaultValue : trim($values[$index]);
    }

    protected function getRoleByExcelValue(?int $roleSession = null): string
    {
        return match ($roleSession) {
            self::ROLE_PARTICIPANT_ANIMATOR => ProviderParticipantSessionRole::ROLE_ANIMATOR,
            self::ROLE_PARTICIPANT_REMOVE => ProviderParticipantSessionRole::ROLE_REMOVE,
            default => ProviderParticipantSessionRole::ROLE_TRAINEE
        };
    }

    private function getDataFromRow(array $row): array
    {
        return [
            'firstName' => $this->getCellValueFromDataLine($row, 1),
            'lastName' => $this->getCellValueFromDataLine($row, 2),
            'email' => $this->getCellValueFromDataLine($row, 3),
            'reference' => $this->getCellValueFromDataLine($row, 4),
            'action' => (int) $this->getCellValueFromDataLine($row, 5, self::ACTION_USER_ADD_OR_UPDATE),
            'phoneNumber' => $this->getCellValueFromDataLine($row, 6),
            'company' => $this->getCellValueFromDataLine($row, 7),
            'information' => $this->getCellValueFromDataLine($row, 8),
            'emailReferent' => $this->getCellValueFromDataLine($row, 9),
            'referenceSession' => $this->getCellValueFromDataLine($row, 10),
            'roleSession' => (int) $this->getCellValueFromDataLine($row, 11),
            'situationMixte' => (int) $this->getCellValueFromDataLine($row, 12),
        ];
    }

    /**
     * @throws AsyncTaskException
     */
    private function validateData(array $data): void
    {
        if (empty($data['action']) ||
            !in_array($data['action'], [
                self::ACTION_USER_ADD_OR_UPDATE,
                self::ACTION_USER_ANONYMISATION,
                self::ACTION_USER_REMOVE,
            ])
        ) {
            throw new InvalidImportDataException('This action "%action%" is not correct!', ['%action%' => $data['action']], $this->translator->trans('This action "%action%" is not correct!', ['%action%' => $data['action']]));
        }

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            throw new InvalidImportDataException('This email "%email%" is not correct!', ['%email%' => $data['email']], $this->translator->trans('This email "%email%" is not correct!', ['%email%' => $data['email']]));
        }

        if (empty($data['firstName'])) {
            throw new InvalidImportDataException('The first name of this user is empty "%email%"!', ['%email%' => $data['email']], $this->translator->trans('The first name of this user is empty "%email%"!', ['%email%' => $data['email']]));
        }

        if (empty($data['lastName'])) {
            throw new InvalidImportDataException('The last name of this user is empty "%email%"!', ['%email%' => $data['email']], $this->translator->trans('The last name of this user is empty "%email%"!', ['%email%' => $data['email']]));
        }
    }

    private function fulfillData(array &$data, Client $client): void
    {
        $data['user'] = $this->userRepository->findOneBy(['email' => $data['email'], 'client' => $client]);
        $data['logAction'] = match ($data['action']) {
            self::ACTION_USER_ADD_OR_UPDATE => (empty($data['user']) ? ActivityLog::ACTION_USER_ADD : ActivityLog::ACTION_USER_UPDATE),
            self::ACTION_USER_ANONYMISATION => ActivityLog::ACTION_USER_ANONYMIZE,
            self::ACTION_USER_REMOVE => ActivityLog::ACTION_USER_DELETE,
            default => ActivityLog::ACTION_IMPORT_USERS
        };
        $data['session'] = $this->getSession($data);
    }

    public function getSession(array $data): ?ProviderSession
    {
        if (!empty($data['referenceSession'])) {
            /** @var ProviderSession $session */
            $session = $this->providerSessionRepository->findOneBy([
                'ref' => $data['referenceSession'],
            ]);

            if (empty($session)) {
                throw new InvalidImportDataException('An error occurred, the session %session% does not exist.', ['%session%' => $data['referenceSession']], $this->translator->trans('An error occurred, the session %session% does not exist.', ['%session%' => $data['referenceSession']]));
            }

            return $session;
        }

        return null;
    }
}
