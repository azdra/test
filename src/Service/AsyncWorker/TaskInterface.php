<?php

namespace App\Service\AsyncWorker;

use App\Entity\AsyncTask;
use App\Exception\AsyncWorker\AsyncTaskException;

interface TaskInterface
{
    public const STATUS_READY = 'ready';
    public const STATUS_PROCESSING = 'processing';
    public const STATUS_DONE = 'done';
    public const STATUS_FAILED = 'failed';

    /**
     * Instructions to execute by the asynchronous worker. Errors management must be done
     * using {@see \App\Exception\AsyncWorker\AsyncTaskException}.
     *
     * @return void Execute must not return any value. It won't be used.
     *
     * @throws AsyncTaskException
     */
    public function execute(AsyncTask $task): void;

    /**
     * Force herited class to define an Origin for the ActivityLog.
     */
    public function getOriginTask(): string;
}
