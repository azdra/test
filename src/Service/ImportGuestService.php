<?php

namespace App\Service;

use App\Entity\Client\Client;
use App\Entity\Guest;
use App\Entity\WebinarConvocation;
use App\Exception\InvalidTemplateUploadedForImportException;
use App\Exception\MissingClientParameter;
use App\Exception\MissingCommunicationParameter;
use App\Exception\UploadFile\UploadFileNoFoundInRequestException;
use App\Exception\UploadFile\UploadFileNotAllowedExtException;
use App\Exception\UploadFile\UploadFileOverSizeException;
use App\Exception\UploadFile\UploadFileTooManyRowException;
use App\Repository\AbstractProviderConfigurationHolderRepository;
use App\Repository\ClientRepository;
use App\Service\AsyncWorker\AsyncTaskService;
use App\Service\Provider\Common\GuestImportDataReader;
use App\Service\RessourceUploader\RessourceUploader;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Ods;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class ImportGuestService
{
    public const AUTHORIZED_EXTENSION = ['xls', 'xlsx', 'csv'];
    public const MAX_FILE_SIZE = 4000000;
    public const ALLOWED_MAX_ROW = 2000;

    public function __construct(
        private AsyncTaskService $asyncTaskService,
        protected string $importsSavefilesDirectory,
        Security $security,
        private ClientRepository $clientRepository,
        private EntityManagerInterface $entityManager,
        private RessourceUploader $ressourceUploader,
        AbstractProviderConfigurationHolderRepository $configurationHolderRepository,
        private TranslatorInterface $translator,
        private GuestImportDataReader $guestImportDataReader,
    ) {
    }

    /**
     * @throws UploadFileOverSizeException
     * @throws UploadFileNotAllowedExtException
     * @throws MissingClientParameter
     * @throws UploadFileNoFoundInRequestException
     * @throws UploadFileTooManyRowException
     * @throws InvalidTemplateUploadedForImportException
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws MissingCommunicationParameter
     */
    public function importGuestsFromRequest(Request $request): void
    {
        /* @var Client $client */
        $client = $this->clientRepository->find($request->request->get('client'));
        $webinarConvocation = $this->entityManager->getRepository(WebinarConvocation::class)->findOneBy(['id' => $request->request->get('webinarConvocation')]);

        $this->validateRequest($request);
        $file = $request->files->get('file');
        $this->validateFile($file);
        $sheet = $this->guestImportDataReader->getSheet($this->getSpreadsheet($file));
        $this->guestImportDataReader->validateTemplateLoadedIsValid($sheet);
        $this->validateData($sheet);
        $this->saveFile($file, $this->getFlagOrigin());
        $rowsData = $this->guestImportDataReader->readData($sheet);
        $this->guestHandler($rowsData, $client, $webinarConvocation);
    }

    /**
     * @throws MissingClientParameter
     * @throws UploadFileNoFoundInRequestException|MissingCommunicationParameter
     */
    private function validateRequest(Request $request): void
    {
        if (empty($request->request->get('client'))) {
            throw new MissingClientParameter('No account is associated with the import');
        }

        if (empty($request->request->get('webinarConvocation'))) {
            throw new MissingCommunicationParameter('No communication is associated with the import');
        }

        if (empty($request->files->get('file')) || empty($request->files->get('file')->getClientOriginalExtension())) {
            throw new UploadFileNoFoundInRequestException($this->translator->trans('Not file is present'));
        }
    }

    /**
     * @throws UploadFileNotAllowedExtException
     * @throws UploadFileOverSizeException
     * @throws UploadFileNoFoundInRequestException
     */
    public function validateFile(UploadedFile $file): void
    {
        $this->ressourceUploader->isFileUploadedValid($file, $this->getAllowedExtensions(), $this->getMaxFileSize());
    }

    /**
     * @throws Exception
     */
    private function getSpreadsheet(UploadedFile $file): Spreadsheet
    {
        $reader = match ($file->getClientOriginalExtension()) {
            'xls' => new Xls(),
            'xlsx' => new Xlsx(),
            'csv' => new Csv(),
            'ods' => new Ods()
        };
        $reader->setReadDataOnly(true);
        $reader->setReadEmptyCells(false);

        return $reader->load($file->getRealPath());
    }

    /**
     * @throws UploadFileTooManyRowException
     */
    public function validateData(Worksheet $sheet): void
    {
        if ($this->getRowLimit() > 0 && $sheet->getHighestRow() > $this->getRowLimit()) {
            throw new UploadFileTooManyRowException($this->translator->trans('The file contains too many lines, the maximum allowed is %limit%', ['%limit%' => $this->getRowLimit()]));
        }
    }

    private function saveFile(UploadedFile $file, string $newBasename): void
    {
        ( new Filesystem() )->copy(
            $file->getRealPath(),
            $this->importsSavefilesDirectory.'/'.$newBasename.'.'.$file->getClientOriginalExtension(),
            true
        );
    }

    protected function getAllowedExtensions(): array
    {
        return self::AUTHORIZED_EXTENSION;
    }

    protected function getMaxFileSize(): int
    {
        return self::MAX_FILE_SIZE;
    }

    protected function getRowLimit(): int
    {
        return self::ALLOWED_MAX_ROW;
    }

    protected function getFlagOrigin(): string
    {
        return 'ImportUsers_'.$this->asyncTaskService->randomFlag(20);
    }

    public function guestHandler(array $rowsData, Client $client, WebinarConvocation $webinarConvocation): void
    {
        foreach ($rowsData as $dataGuest) {
            switch ($dataGuest['4']) {
                case null:
                case '1':
                    $this->addOrUpdateGuest($dataGuest, $client, $webinarConvocation);
                    break;
                case '2':
                    $this->deleteGuest($dataGuest, $webinarConvocation);
                    break;
                default:
                    break;
            }
        }
    }

    public function addOrUpdateGuest(array $dataGuest, Client $client, WebinarConvocation $webinarConvocation): void
    {
        if (empty($dataGuest['1'])) {
            return;
        }
        $guest = $webinarConvocation->getGuestByEmail($dataGuest['1']);
        if ($guest !== null) {
            $guest->setEmail($dataGuest['1'])
                  ->setFirstName($dataGuest['2'])
                  ->setLastName($dataGuest['3']);
        } else {
            $guest = new Guest();
            $guest->setEmail($dataGuest['1'])
                  ->setFirstName($dataGuest['2'])
                  ->setLastName($dataGuest['3'])
                  ->setWebinarConvocation($webinarConvocation)
                  ->setClient($client);

            $this->entityManager->persist($guest);
        }
    }

    public function deleteGuest(array $dataGuest, WebinarConvocation $webinarConvocation): void
    {
        if (empty($dataGuest['1'])) {
            return;
        }
        $guest = $webinarConvocation->getGuestByEmail($dataGuest['1']);
        if ($guest !== null) {
            $this->entityManager->remove($guest);
        }
    }
}
