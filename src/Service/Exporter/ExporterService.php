<?php

namespace App\Service\Exporter;

use App\Entity\ActivityLog;
use App\Entity\DeferredActionsTask;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Entity\WebinarConvocation;
use App\Exception\Exporter\ExporterException;
use App\Model\ActivityLog\ActivityLogModel;
use App\Model\ApiResponseTrait;
use App\Model\GeneratorTrait;
use App\Service\ActivityLogger;
use App\Service\Exporter\Pdf\PdfCertificateProviderParticipantSessionRoleExporter;
use App\Service\Exporter\Pdf\PdfEvaluationAnswersReportProviderSessionExporter;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetAnswersReportProviderSessionExporter;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetAttendanceReportProviderSessionExporter;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetEvaluationReportProviderSessionExporter;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetEvaluationsReportProviderSessionsExporter;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetRegisteredDataExporter;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetReplayStatisticsExporter;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetSessionDataExporter;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetSessionTPMDataExporter;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetSessionZohoDataExporter;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetStatisticsGuestsWebinarConvocationExporter;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetUserRGPDDatasExporter;
use Psr\Log\LoggerInterface;
use Redis;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;
use ZipArchive;

class ExporterService
{
    use GeneratorTrait;
    use ApiResponseTrait;
    public const PREFIX_REDIS = 'exporter_';

    public function __construct(
        private ExporterFactory $exporterFactory,
        private ActivityLogger $activityLogger,
        private Security $security,
        private Redis $redisClient,
        private TranslatorInterface $translator,
        private string $exportsSavefilesDirectory,
        private LoggerInterface $logger
    ) {
    }

    public function generateArchive(string $type, string $format, array $extraData = [], bool $grouping = false, bool $crypt = true): Response
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '3600');
        try {
            $exporter = $this->exporterFactory->getExporter($type, $format);

            //TODO Une future US viendra dans laquelle AbstractSpreadsheetEvaluationsReportProviderSessionsExporter permettra de passer le regroupement. Aujourd'hui grouping est passé
            $result = match ($type) {
                AbstractSpreadsheetAttendanceReportProviderSessionExporter::EXPORT_TYPE,
                AbstractSpreadsheetEvaluationsReportProviderSessionsExporter::EXPORT_TYPE => $this->generateSessionReportArchive(
                    $exporter, sessions: $extraData, grouping: $grouping, crypt: $crypt
                ),
                AbstractSpreadsheetStatisticsGuestsWebinarConvocationExporter::EXPORT_TYPE => $this->generateStatisticsGuestsArchive($exporter, webinarConvocation: $extraData['webinarConvocation']),
                AbstractSpreadsheetReplayStatisticsExporter::EXPORT_TYPE => $this->generateSessionReportArchive(
                    $exporter, sessions: $extraData, crypt: $crypt
                ),
                PdfEvaluationAnswersReportProviderSessionExporter::EXPORT_TYPE,
                AbstractSpreadsheetAnswersReportProviderSessionExporter::EXPORT_TYPE,
                AbstractSpreadsheetEvaluationReportProviderSessionExporter::EXPORT_TYPE => $this->generateSessionReportArchive(
                    $exporter, $extraData, $grouping
                ),
                AbstractSpreadsheetRegisteredDataExporter::EXPORT_TYPE,
                AbstractSpreadsheetSessionDataExporter::EXPORT_TYPE => $this->generateSessionReportArchive(
                   exporter: $exporter, sessions: $extraData, grouping: true
                ),
                AbstractSpreadsheetSessionZohoDataExporter::EXPORT_TYPE => $this->generateSessionZohoArchive(
                    exporter: $exporter, sessions: $extraData, grouping: true
                ),
                AbstractSpreadsheetSessionTPMDataExporter::EXPORT_TYPE => $this->generateSessionTPMReportArchive(
                    exporter: $exporter, sessions: $extraData, grouping: true
                ),
                PdfCertificateProviderParticipantSessionRoleExporter::EXPORT_TYPE => $this->generateParticipantSessionRoleReport($exporter, $extraData, $grouping),
                AbstractSpreadsheetUserRGPDDatasExporter::EXPORT_TYPE => $this->generateUserReport($exporter, $extraData),
                default => throw new ExporterException('Unable to find method to generate archive for this exporter type'),
            };

            return $result;
        } catch (ExporterException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($this->translator->trans('Unable to generate this export'), extraData: ['exception' => $exception->getMessage()]);
        }
    }

    /**
     * @throws ExporterException
     */
    public function validateDownloadingToken(string $token): string
    {
        $fullToken = ExporterService::PREFIX_REDIS.$token;

        if (!$this->redisClient->exists($fullToken)) {
            $exception = new ExporterException('Invalid token. It is not valid or has not been found');
            $this->logger->error($exception);
            throw $exception;
        }

        /** @var User $currentUser */
        $currentUser = $this->security->getUser();
        $data = preg_split('/\|/', $this->redisClient->get($fullToken));

        if (intval($data[0]) !== $currentUser->getId()) {
            $exception = new ExporterException('The current user do not match with the token');
            $this->logger->error($exception);
            throw $exception;
        }

        $this->redisClient->unlink($fullToken);

        return $data[1];
    }

    public function generateActivityLogExport(User $user, string $filename): void
    {
        $this->activityLogger->initActivityLogContext(
            $user->getClient(),
            $user,
            ActivityLog::ORIGIN_EXPORT
        );
        $infos = [
            'message' => ['key' => 'You have successfully exported the file : <a class="success">%filename%</a>', 'params' => ['%filename%' => $filename]],
            'links' => [
                ['type' => 'link', 'routeKey' => 'download_export_file', 'routeParam' => ['filename' => $filename], 'transKey' => 'Download File'],
            ],
        ];
        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_EXPORT_DATA, ActivityLog::SEVERITY_INFORMATION, $infos));
        $this->activityLogger->flushActivityLogs();
    }

    public function generateActivityLogDeferredActionsTaskExportEvaluations(User $user, string $status, DeferredActionsTask $deferredActionsTask, ?string $filename = null): void
    {
        $this->activityLogger->initActivityLogContext(
            $user->getClient(),
            $user,
            ActivityLog::ORIGIN_EXPORT
        );
        switch ($status) {
            case DeferredActionsTask::STATUS_READY:
                $infos = [
                    'message' => ['key' => 'Your request to export ratings has been taken into account and will be processed soon'],
                    'deferredActionsTaskId' => $deferredActionsTask->getId(),
                    'links' => [['type' => 'component', 'name' => 'ModalDeferredExportStatusInfo']],
                ];
                break;
            case DeferredActionsTask::STATUS_FAILED:
                $infos = [
                    'message' => ['key' => 'Your export of assessments has failed: %error%', 'params' => ['%error%' => $deferredActionsTask->getResult()['Error']['message']]],
                ];
                break;
            case DeferredActionsTask::STATUS_DONE:
                $infos = [
                    'message' => ['key' => 'Your export of assessments is available in the file: <a class="success">%filename%</a>', 'params' => ['%filename%' => $filename]],
                    'links' => [
                        ['type' => 'link', 'routeKey' => 'download_export_file', 'routeParam' => ['filename' => $filename], 'transKey' => 'Download File'],
                    ],
                ];
                break;
            case 'NO DATA':
                $infos = [
                    'message' => ['key' => 'Your request to export ratings has been processed but no data is available for the selected period'],
                ];
                break;
        }

        $this->activityLogger->addActivityLog(new ActivityLogModel(ActivityLog::ACTION_EXPORT_DATA, ActivityLog::SEVERITY_INFORMATION, $infos));
        $this->activityLogger->flushActivityLogs();
    }

    private function generateSessionReportArchive(ExporterInterface $exporter, array $sessions, bool $grouping = false, bool $crypt = true): JsonResponse
    {
        /* @var ProviderSessionExporterInterface $exporter */

        try {
            $zipData = $this->createAndOpenZip($exporter);
            $zip = $zipData['zip'];
            $filename = $zipData['fileName'];
            $asFileGenerated = false;

            if ($grouping) {
                $file = $exporter->createExportFileGrouping($sessions);
                $zip->addFile($file, basename($file));
                $asFileGenerated = true;
            } else {
                foreach ($sessions as $session) {
                    if (!($session instanceof ProviderSession)) {
                        continue;
                    }

                    $file = $exporter->createExportFile($session);
                    $zip->addFile($file, basename($file));
                    $asFileGenerated = true;
                }
            }

            $zip->close();

            if (!$asFileGenerated) {
                if (file_exists($filename)) {
                    unlink($filename);
                }

                throw new ExporterException('Nothing to export');
            }

            if ($crypt) {
                return $this->apiResponse($this->generateArchiveTokenDownloading($filename));
            } else {
                return $this->apiResponse($filename);
            }
        } catch (ExporterException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($this->translator->trans('Unable to generate this export'), extraData: ['exception' => $exception->getMessage()]);
        }
    }

    private function generateSessionZohoArchive(ExporterInterface $exporter, array $sessions, bool $grouping = false, bool $crypt = true): JsonResponse
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '3600');
        /* @var ProviderSessionExporterInterface $exporter */

        try {
            $asFileGenerated = false;

            if ($grouping) {
                $file = $exporter->createExportFileGrouping($sessions);
                $filename = basename($file);
                $asFileGenerated = true;
            } else {
                foreach ($sessions as $session) {
                    if (!($session instanceof ProviderSession)) {
                        continue;
                    }

                    $file = $exporter->createExportFile($session);
                    $filename = basename($file);
                    $asFileGenerated = true;
                }
            }

            if (!$asFileGenerated) {
                throw new ExporterException('Nothing to export');
            }

            if ($crypt) {
                return $this->apiResponse($this->generateArchiveTokenDownloading($filename));
            } else {
                return $this->apiResponse($filename);
            }
        } catch (ExporterException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($this->translator->trans('Unable to generate this export'), extraData: ['exception' => $exception->getMessage()]);
        }
    }

    private function generateSessionTPMReportArchive(ExporterInterface $exporter, array $sessions, bool $grouping = false): JsonResponse
    {
        /* @var ProviderSessionTPMExporterInterface $exporter */

        try {
            $zipData = $this->createAndOpenZip($exporter);
            $zip = $zipData['zip'];
            $filename = $zipData['fileName'];
            $asFileGenerated = false;

            if ($grouping) {
                $file = $exporter->createExportFileGrouping($sessions);
                $zip->addFile($file, basename($file));
                $asFileGenerated = true;
            } else {
                foreach ($sessions as $session) {
                    if (!($session instanceof ProviderSession)) {
                        continue;
                    }

                    $file = $exporter->createExportFile($session);
                    $zip->addFile($file, basename($file));
                    $asFileGenerated = true;
                }
            }

            $zip->close();

            if (!$asFileGenerated) {
                if (file_exists($filename)) {
                    unlink($filename);
                }

                throw new ExporterException('Nothing to export');
            }

            return $this->apiResponse($this->generateArchiveTokenDownloading($filename));
        } catch (ExporterException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($this->translator->trans('Unable to generate this export'), extraData: ['exception' => $exception->getMessage()]);
        }
    }

    private function generateParticipantSessionRoleReport(ExporterInterface $exporter, array $sessions, bool $grouping = false): JsonResponse
    {
        /* @var ProviderParticipantSessionRoleExporterInterface $exporter */

        try {
            $zipData = $this->createAndOpenZip($exporter);
            $zip = $zipData['zip'];
            $filename = $zipData['fileName'];
            $asFileGenerated = false;

            if ($grouping) {
                $file = $exporter->createExportFileGrouping($sessions);
                $zip->addFile($file, basename($file));
                $asFileGenerated = true;
            } else {
                foreach ($sessions as $session) {
                    if (!($session instanceof ProviderSession)) {
                        continue;
                    }

                    if ($session->isADraft() || !$session->isFinished()) {
                        continue;
                    }

                    /** @var ProviderParticipantSessionRole $participantRole */
                    foreach ($session->getParticipantRoles() as $participantRole) {
                        if ($participantRole->isATrainee() && $participantRole->isPresent()) {
                            $file = $exporter->createExportFile($participantRole);
                            $zip->addFile($file, basename($file));
                            $asFileGenerated = true;
                        }
                    }
                }
            }

            $zip->close();

            if (!$asFileGenerated) {
                if (file_exists($filename)) {
                    unlink($filename);
                }

                throw new ExporterException('Nothing to export');
            }

            return $this->apiResponse($this->generateArchiveTokenDownloading($filename));
        } catch (ExporterException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($this->translator->trans('Unable to generate this export'), extraData: ['exception' => $exception->getMessage()]);
        }
    }

    public function generateUserReport(ExporterInterface $exporter, array $datas): JsonResponse
    {
        /* @var UserExporterInterface $exporter */
        try {
            if (!empty($datas['user'])) {
                $file = $exporter->createExportFile($datas['user']);
            } else {
                throw new ExporterException('User is not available for export');
            }

            if (empty($file)) {
                throw new ExporterException('Nothing to export');
            }

            return $this->apiResponse($this->generateArchiveTokenDownloading($file, $datas['userOrigin']));
        } catch (ExporterException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse(
                $this->translator->trans('Unable to generate this export'),
                extraData: ['exception' => $exception->getMessage()]
            );
        }
    }

    public function generateTrainingCertificateParticipant(ExporterInterface $exporter, ProviderParticipantSessionRole $participantSessionRole): JsonResponse
    {
        /* @var ProviderParticipantSessionRoleExporterInterface $exporter */
        try {
            $file = $exporter->createExportFile($participantSessionRole);

            if (empty($file)) {
                throw new ExporterException('Nothing to export');
            }

            return $this->apiResponse($this->generateArchiveTokenDownloading($file, $participantSessionRole->getParticipant()->getUser()));
        } catch (ExporterException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse(
                $this->translator->trans('Unable to generate this export'),
                extraData: ['exception' => $exception->getMessage()]
            );
        }
    }

    private function generateArchiveTokenDownloading(string $filename, ?User $user = null): string
    {
        if (empty($user)) {
            /** @var User $currentUser */
            $user = $this->security->getUser();
        }
        $uuid = Uuid::v4();

        $this->redisClient->set(self::PREFIX_REDIS.$uuid, "{$user?->getId()}|$filename");

        return $uuid->__toString();
    }

    /**
     * @throws ExporterException
     */
    private function createAndOpenZip(ExporterInterface $exporter): array
    {
        $random = $this->randomString();
        $type = $exporter->getSupportType();
        $filename = "$this->exportsSavefilesDirectory/$type-$random.zip";
        $zip = new ZipArchive();

        if ($zip->open($filename, \ZipArchive::CREATE) !== true) {
            throw new ExporterException('Unable to create the archive');
        }

        return [
            'zip' => $zip,
            'fileName' => $filename,
        ];
    }

    private function generateStatisticsGuestsArchive(ExporterInterface $exporter, WebinarConvocation $webinarConvocation): JsonResponse
    {
        /* @var AbstractSpreadsheetStatisticsGuestsWebinarConvocationExporter $exporter */
        try {
            if ($webinarConvocation->getGuests()->count() === 0) {
                throw new ExporterException('Nothing to export');
            }
            $zipData = $this->createAndOpenZip($exporter);
            $zip = $zipData['zip'];
            $filename = $zipData['fileName'];
            $file = $exporter->createExportStatisticsGuests($webinarConvocation);
            $zip->addFile($file, basename($file));

            $zip->close();

            return $this->apiResponse($this->generateArchiveTokenDownloading($filename));
        } catch (ExporterException|\Exception $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($this->translator->trans('Unable to generate this export'), extraData: ['exception' => $exception->getMessage()]);
        }
    }
}
