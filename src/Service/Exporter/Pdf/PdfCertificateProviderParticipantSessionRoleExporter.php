<?php

namespace App\Service\Exporter\Pdf;

use App\Entity\ProviderParticipantSessionRole;
use App\Service\Exporter\ExporterInterface;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\ProviderParticipantSessionRoleExporterInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use Knp\Snappy\Pdf;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\WebpackEncoreBundle\Asset\EntrypointLookupCollectionInterface;
use Twig\Environment;

class PdfCertificateProviderParticipantSessionRoleExporter implements ProviderParticipantSessionRoleExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'certificate_report';
    public const EXPORT_FORMAT = ExportFormat::PDF;

    public function __construct(
        private Pdf $pdf,
        private Environment $twig,
        private SluggerInterface $slugger,
        private string $exportsSavefilesDirectory,
        private EntrypointLookupCollectionInterface $entrypointLookups
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    public function getSupportFormat(): string
    {
        return self::EXPORT_FORMAT;
    }

    public function createExportFile(ProviderParticipantSessionRole $participantSessionRole): string
    {
        $imageDataClientHeader = $imageDataClientFooter = $imageDataClientOrganizationStamp = null;
        $client = $participantSessionRole->getSession()->getClient();

        if ($client->getHeader()) {
            $imagePathClientHeader = realpath(__DIR__.'/../../../../public/uploads/client_header/'.$client->getHeader());
            $imageDataClientHeader = base64_encode(file_get_contents($imagePathClientHeader));
        }

        if ($client->getFooter()) {
            $imagePathClientFooter = realpath(__DIR__.'/../../../../public/uploads/client_footer/'.$client->getFooter());
            $imageDataClientFooter = base64_encode(file_get_contents($imagePathClientFooter));
        }

        if ($client->getOrganizationStamp()) {
            $imagePathClientOrganizationStamp = realpath(__DIR__.'/../../../../public/uploads/client_organizationStamp/'.$client->getOrganizationStamp());
            $imageDataClientOrganizationStamp = base64_encode(file_get_contents($imagePathClientOrganizationStamp));
        }

        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFilename($participantSessionRole);

        $options = new Options();
        $options->setIsRemoteEnabled(true);
        $options->setIsJavascriptEnabled(true);

        $html = $this->generateContent($participantSessionRole, $imageDataClientHeader, $imageDataClientFooter, $imageDataClientOrganizationStamp);

        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($html);
        $dompdf->render();
        file_put_contents($path, $dompdf->output());

        return $path;
    }

    public function createExportFileGrouping(array $participantSessionRoles): string
    {
        throw new \LogicException('Not yet implemented');
    }

    public function createExportResponse(ProviderParticipantSessionRole $participantSessionRole): Response
    {
        return new BinaryFileResponse($this->createExportFile($participantSessionRole));
    }

    private function getPDFOptions(ProviderParticipantSessionRole $participantSessionRole): array
    {
        $session = $participantSessionRole->getSession();
        $client = $session->getClient();

        return [
            'header-html' => $this->twig->render('exporter/client_header.html.twig', ['client' => $client]),
            'footer-html' => $this->twig->render('exporter/client_footer.html.twig', ['client' => $client, 'session' => $session]),
        ];
    }

    private function generateFilename(ProviderParticipantSessionRole $participantSessionRole): string
    {
        $session = $participantSessionRole->getSession();
        $participantFullName = $participantSessionRole->getParticipant()->getFullName();
        $sessionDateStart = ($session->getDateStart())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('Y-m-d_H\hi');
        $sessionRef = $session->getRef();
        $extension = strtolower($this->getSupportFormat());

        return $this->slugger->slug("$sessionDateStart-certificate-$sessionRef-$participantFullName").".$extension";
    }

    private function generateContent(ProviderParticipantSessionRole $participantSessionRole, ?string $imageDataClientHeader = null, ?string $imageDataClientFooter = null, ?string $imageDataClientOrganizationStamp = null): string
    {
        $session = $participantSessionRole->getSession();

        return $this->twig->render('exporter/certificate_report.html.twig', [
            'client' => $session->getClient(),
            'session' => $session,
            'dateStartSession' => ($session->getDateStart())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i'),
            'dateEndSession' => ($session->getDateEnd())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i'),
            'presenceDateStart' => ($participantSessionRole->getPresenceDateStart())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('H:i'),
            'presenceDateEnd' => ($participantSessionRole->getPresenceDateEnd())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('H:i'),
            'participantSessionRole' => $participantSessionRole,
            'imageDataClientHeader' => $imageDataClientHeader,
            'imageDataClientFooter' => $imageDataClientFooter,
            'imageDataClientOrganizationStamp' => $imageDataClientOrganizationStamp,
        ]);
    }
}
