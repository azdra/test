<?php

namespace App\Service\Exporter\Pdf;

use App\Entity\ProviderSession;
use App\Repository\EvaluationParticipantAnswerRepository;
use App\Service\Exporter\ExporterInterface;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\ProviderSessionExporterInterface;
use Knp\Snappy\Pdf;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\WebpackEncoreBundle\Asset\EntrypointLookupCollectionInterface;
use Twig\Environment;

class PdfEvaluationReportProviderSessionExporter implements ProviderSessionExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'evaluation_report';
    public const EXPORT_FORMAT = ExportFormat::PDF;

    public function __construct(
        private Pdf $pdf,
        private Environment $twig,
        private SluggerInterface $slugger,
        private string $exportsSavefilesDirectory,
        private EntrypointLookupCollectionInterface $entrypointLookups,
        private EvaluationParticipantAnswerRepository $evaluationParticipantAnswerRepository
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    public function getSupportFormat(): string
    {
        return self::EXPORT_FORMAT;
    }

    public function createExportFileGrouping(array $sessions): string
    {
        throw new \LogicException('Not yet implemented');
    }

    public function createExportFile(ProviderSession $session): string
    {
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFilename($session);

        $this->pdf->generateFromHtml($this->generateContent($session), $path, $this->getPDFOptions($session), true);
        $this->entrypointLookups->getEntrypointLookup()->reset();

        return $path;
    }

    public function createExportResponse(ProviderSession $session): Response
    {
        return new BinaryFileResponse($this->createExportFile($session));
    }

    private function getPDFOptions(ProviderSession $session): array
    {
        return [
            'header-html' => $this->twig->render('exporter/client_header.html.twig', ['client' => $session->getClient()]),
            'footer-html' => $this->twig->render('exporter/client_footer.html.twig', ['client' => $session->getClient(), 'session' => $session]),
        ];
    }

    private function generateFilename(ProviderSession $session): string
    {
        $sessionDateStart = $session->getDateStart()
            ->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))
            ->format('Y-m-d_H\hi');
        $sessionRef = $session->getRef();
        $clientName = $session->getClient()->getName();
        $extension = strtolower($this->getSupportFormat());

        return $this->slugger->slug("$sessionDateStart-evaluation-$clientName-$sessionRef").".$extension";
    }

    private function generateContent(ProviderSession $session): string
    {
        $evaluations = $session->getEvaluations();

        if (count($evaluations) === 0) {
            throw new \LogicException('There is no evaluation associated with the session');
        }
        $evaluation = $evaluations->first();
        $extractedData = $this->evaluationParticipantAnswerRepository->getSessionAnswerForEvaluation($evaluation, $session);

        return $this->twig->render('exporter/evaluation_result.html.twig', [
            'session' => $session,
            'extractedData' => $extractedData,
            'formulaireName' => $evaluation->getTitle(),
        ]);
    }
}
