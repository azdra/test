<?php

namespace App\Service\Exporter\Pdf;

use App\Entity\ProviderSession;
use App\Service\Exporter\ExporterInterface;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\ProviderSessionExporterInterface;
use App\Service\ProviderSessionService;
use Dompdf\Dompdf;
use Dompdf\Options;
use Knp\Snappy\Pdf;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\WebpackEncoreBundle\Asset\EntrypointLookupCollectionInterface;
use Twig\Environment;

class PdfAttendanceReportProviderSessionExporter implements ProviderSessionExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'attendance_report';
    public const EXPORT_FORMAT = ExportFormat::PDF;

    public function __construct(
        private Pdf $pdf,
        private Environment $twig,
        private SluggerInterface $slugger,
        private string $exportsSavefilesDirectory,
        private ProviderSessionService $providerSessionService,
        private EntrypointLookupCollectionInterface $entrypointLookups
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    public function getSupportFormat(): string
    {
        return self::EXPORT_FORMAT;
    }

    public function createExportFileGrouping(array $sessions): string
    {
        throw new \LogicException('Not yet implemented');
    }

    public function createExportFile(ProviderSession $session): string
    {
        $imageDataClientHeader = $imageDataClientFooter = $imageDataClientOrganizationStamp = null;
        $client = $session->getClient();

        if ($client->getHeader()) {
            $imagePathClientHeader = realpath(__DIR__.'/../../../../public/uploads/client_header/'.$client->getHeader());
            $imageDataClientHeader = base64_encode(file_get_contents($imagePathClientHeader));
        }

        if ($client->getFooter()) {
            $imagePathClientFooter = realpath(__DIR__.'/../../../../public/uploads/client_footer/'.$client->getFooter());
            $imageDataClientFooter = base64_encode(file_get_contents($imagePathClientFooter));
        }

        if ($client->getOrganizationStamp()) {
            $imagePathClientOrganizationStamp = realpath(__DIR__.'/../../../../public/uploads/client_organizationStamp/'.$client->getOrganizationStamp());
            $imageDataClientOrganizationStamp = base64_encode(file_get_contents($imagePathClientOrganizationStamp));
        }

        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFilename($session);

        $options = new Options();
        $options->setIsRemoteEnabled(true);
        $options->setIsJavascriptEnabled(true);

        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($this->generateContent($session, $imageDataClientHeader, $imageDataClientFooter, $imageDataClientOrganizationStamp));
        $dompdf->render();
        file_put_contents($path, $dompdf->output());

        return $path;
    }

    public function createExportResponse(ProviderSession $session): Response
    {
        return new BinaryFileResponse($this->createExportFile($session));
    }

    private function getPDFOptions(ProviderSession $session): array
    {
        return [
            'header-html' => $this->twig->render('exporter/client_header.html.twig', ['client' => $session->getClient()]),
            'footer-html' => $this->twig->render('exporter/client_footer.html.twig', ['client' => $session->getClient(), 'session' => $session]),
        ];
    }

    private function generateFilename(ProviderSession $session): string
    {
        $sessionDateStart = $session->getDateStart()
            ->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))
            ->format('Y-m-d_H\hi');
        $sessionRef = $session->getRef();
        $clientName = $session->getClient()->getName();
        $extension = strtolower($this->getSupportFormat());

        return $this->slugger->slug("$sessionDateStart-presence-$clientName-$sessionRef").".$extension";
    }

    private function generateContent(ProviderSession $session, ?string $imageDataClientHeader = null, ?string $imageDataClientFooter = null, ?string $imageDataClientOrganizationStamp = null): string
    {
        $totalPresence = 0;
        $totalConnexionDuration = 0;
        foreach ($session->getParticipantRoles() as $participantRole) {
            if ($participantRole->getPresenceStatus() && $participantRole->getSlotPresenceDuration() > $this->providerSessionService->getDurationMinimumForPresence($session)) {
                ++$totalPresence;
                $totalConnexionDuration += $participantRole->getSlotPresenceDuration();
            }
        }

        return $this->twig->render('exporter/attendance_report.html.twig', [
            'session' => $session,
            'traineesOnly' => $this->alphabeticalOrder($session->getTraineesOnly()),
            'animatorsOnly' => $this->alphabeticalOrder($session->getAnimatorsOnly()),
            'totalPresence' => $totalPresence,
            'imageDataClientHeader' => $imageDataClientHeader,
            'imageDataClientFooter' => $imageDataClientFooter,
            'imageDataClientOrganizationStamp' => $imageDataClientOrganizationStamp,
            'totalConnexionDuration' => $totalConnexionDuration,
        ]);
    }

    public function alphabeticalOrder(array $participants): array
    {
        // item type is PPSR entity
        usort($participants, function ($a, $b) {
            return strcmp($a->getParticipant()->getLastName(), $b->getParticipant()->getLastName());
        });

        return $participants;
    }
}
