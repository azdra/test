<?php

namespace App\Service\Exporter\Spreadsheet;

use App\Entity\EvaluationProviderSession;
use App\Entity\ProviderSession;
use App\Repository\EvaluationProviderSessionRepository;
use App\Service\Exporter\ExporterInterface;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\ProviderSessionExporterInterface;
use App\Service\Utils\SpreadsheetUtils;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSpreadsheetAnswersReportProviderSessionExporter implements ProviderSessionExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'evaluation_answers_report';

    public function __construct(
        private TranslatorInterface $translator,
        private SluggerInterface $slugger,
        private string $exportsSavefilesDirectory,
        private EvaluationProviderSessionRepository $evaluationProviderSessionRepository
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    abstract public function getSupportFormat(): string;

    public function createExportFile(ProviderSession $session): string
    {
        $writer = $this->getSpreadsheetWriter($session);
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFileName($session);

        $writer->save($path);

        return $path;
    }

    public function createExportFileGrouping(array $sessions): string
    {
        $writer = $this->getSpreadsheetWriterGrouping($sessions);
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFileName($sessions[0]);
        $writer->save($path);

        return $path;
    }

    public function createExportResponse(ProviderSession $session): Response
    {
        $writer = $this->getSpreadsheetWriter($session);
        $filename = $this->generateFileName($session);
        $writer->save('php://output');

        $response = new Response();
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Cache-Control', 'max-age=0');

        return $response;
    }

    private function generateEvaluationProvider(Worksheet $worksheet, &$row, $data): void
    {
        $type = $this->translator->trans(EvaluationProviderSession::reverseEvaluationType[$data['evaluationType']]);
        $worksheet->setCellValue(sprintf('A%s', $row), sprintf('%s : %s - %s : %s', $this->translator->trans('Type'), $type, $this->translator->trans('Title'), $data['evaluation']['title']))
            ->mergeCells(sprintf('A%s:B%s', $row, $row + 1));
        $worksheet->setCellValue(sprintf('C%s', $row), $this->translator->trans('Participant information'))
            ->mergeCells(sprintf('C%s:F%s', $row, $row + 1));
        $worksheet->setCellValue(sprintf('G%s', $row), $this->translator->trans('Questions'))
            ->mergeCells(sprintf('G%s:R%s', $row, $row + 1));
        $worksheet->getStyle(sprintf('A%s:R%s', $row, $row + 1))
            ->getAlignment()
            ->setVertical(Alignment::VERTICAL_CENTER)
            ->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $row += 2;
    }

    private function generateEvaluationHeader(Worksheet $worksheet, &$row, $evaluation): void
    {
        $headers = $this->getHeaders($evaluation['evaluationSessionAnswers']);
        for ($colNumber = 0; $colNumber < count($headers); ++$colNumber) {
            $currentColumn = SpreadsheetUtils::numberToLetter($colNumber + 1);
            $worksheet->setCellValue($currentColumn.$row, $headers[$colNumber]);
        }
        ++$row;
    }

    private function generateSpreadsheet(ProviderSession $session): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $data = $this->evaluationProviderSessionRepository->getEvaluationProviderSessionForSession($session);

        foreach ($data as $index => $evaluation) {
            $type = $this->translator->trans(EvaluationProviderSession::reverseEvaluationType[$evaluation['evaluationType']]);
            $worksheet = $index === 0 ? $spreadsheet->getActiveSheet() : $spreadsheet->createSheet();
            $worksheet->setTitle(mb_strimwidth($type.' - '.$evaluation['evaluation']['title'], 0, 31, '...'));

            $row = 1;
            $this->generateEvaluationProvider($worksheet, $row, $evaluation);
            $this->generateEvaluationHeader($worksheet, $row, $evaluation);

            foreach ($evaluation['evaluationSessionAnswers'] as $evaluationSessionAnswer) {
                $user = $evaluationSessionAnswer['providerParticipantSessionRole']['participant']['user'];
                $extractedData = [
                    'firstName' => $user['firstName'],
                    'lastName' => $user['lastName'],
                    'email' => $user['email'],
                    'replyDate' => $evaluationSessionAnswer['replyDate'],
                    'answer' => $evaluationSessionAnswer['answer'],
                ];

                $rows = $this->generateRows($session, $extractedData);

                for ($i = 1; $i < count($rows[0]); ++$i) {
                    $spreadsheet->getActiveSheet()
                        ->getColumnDimension(SpreadsheetUtils::numberToLetter($i))
                        ->setAutoSize(true)
                    ;
                }

                foreach ($rows as $rowData) {
                    $columnNumber = 1;
                    foreach ($rowData as $cellData) {
                        if (is_array($cellData)) {
                            $cellData = implode(', ', $cellData);
                        }
                        $worksheet->setCellValueByColumnAndRow($columnNumber, $row, $cellData);
                        ++$columnNumber;
                    }
                    ++$row;
                }
            }
        }

        return $spreadsheet;
    }

    private function getHeaders(array $answers): array
    {
        $header = [
            $this->translator->trans('Title of the session'),
            $this->translator->trans('Reference'),
            $this->translator->trans('First name'),
            $this->translator->trans('Last name'),
            $this->translator->trans('Email'),
            $this->translator->trans('Date of reply'),
        ];

        if (empty($answers[0]) || empty($answers[0]['answer']) || empty($answers[0]['answer']['plainData'])) {
            throw new \LogicException('An error in the evaluation data blocks the generation of the export');
        }

        $plainData = $this->getSortedPlainData($answers[0]['answer']['plainData']);

        $headerSurvey = [];
        foreach ($plainData as $data) {
            $headerSurvey[] = $data['title'];
        }

        $header = array_merge($header, $headerSurvey);

        return $header;
    }

    private function getSpreadsheetWriterGrouping(array $sessions): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function generateFileName(ProviderSession $session): string
    {
        $extension = strtolower($this->getSupportFormat());

        return $this->slugger->slug($session->getDateStart()->format('Y-m-d_H\hi').'-evaluation-live-session-'.$session->getRef()).".$extension";
    }

    private function generateXLSX(ProviderSession $session): Xlsx
    {
        return new Xlsx($this->generateSpreadsheet($session));
    }

//    private function generateCSV(ProviderSession $session): Csv
//    {
//        return new Csv($this->generateSpreadsheet($session));
//    }

    private function getSpreadsheetWriter(ProviderSession $session): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            ExportFormat::XLSX => $this->generateXLSX($session),
//            ExportFormat::CSV => $this->generateCSV($session),
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function generateRows(ProviderSession $providerSession, array $data): array
    {
        $rows = [];

        $userData = [
            $providerSession->getName(),
            $providerSession->getRef(),
            $data['firstName'],
            $data['lastName'],
            $data['email'],
            \DateTimeImmutable::createFromMutable($data['replyDate'])->setTimezone(new \DateTimeZone($providerSession->getClient()->getTimezone()))->format('Y-m-d H:i:s'),
        ];

        $sortedAnswer = $this->getSortedPlainData($data['answer']['plainData']);

        $answerData = [];
        foreach ($sortedAnswer as $answer) {
            $answerData[] = $answer['displayValue'];
        }

        $rows[] = array_merge($userData, $answerData);

        return $rows;
    }

    private function getSortedPlainData(array $plainData): array
    {
        usort($plainData, function ($a, $b) {
            $indexA = str_replace('question', '', $a['name']);
            $indexB = str_replace('question', '', $b['name']);

            if ($indexA == $indexB) {
                return 0;
            }

            return ($indexA < $indexB) ? -1 : 1;
        });

        return $plainData;
    }
}
