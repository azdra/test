<?php

namespace App\Service\Exporter\Spreadsheet;

use App\Entity\ActivityLog;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Repository\ActivityLogRepository;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionRepository;
use App\Service\Exporter\ExporterInterface;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\UserExporterInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSpreadsheetUserRGPDDatasExporter implements UserExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'user_rgpd_datas';
    public const EXPORT_FORMAT = ExportFormat::XLSX;

    public function __construct(
        private TranslatorInterface $translator,
        private SluggerInterface $slugger,
        private string $exportsSavefilesDirectory,
        private ProviderSessionRepository $providerSessionRepository,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        private ActivityLogRepository $activityLogRepository
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    abstract public function getSupportFormat(): string;

    public function createExportFile(User $user): string
    {
        $writer = $this->getSpreadsheetWriter($user);
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFileName($user);

        $writer->save($path);

        return $path;
    }

    public function createExportFileGrouping(array $users): string
    {
        $writer = $this->getSpreadsheetWriterGrouping($users);
        $path = $this->exportsSavefilesDirectory.'/grouped-extract-datas-gdpr'.strtolower($this->getSupportFormat());
        $writer->save($path);

        return $path;
    }

    public function createExportResponse(User $user): Response
    {
        $writer = $this->getSpreadsheetWriter($user);
        $filename = $this->generateFileName($user);
        $writer->save('php://output');

        $response = new BinaryFileResponse($filename);
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Cache-Control', 'max-age=0');

        return $response;
    }

    private function generateFileName(User $user): string
    {
        $extension = strtolower($this->getSupportFormat());

        $fullName = strtolower($user->getFirstName().'-'.$user->getLastName().'-'.$user->getId());

        return $this->slugger->slug(
            $fullName.'-extract-datas-gdpr-'.
            $user->getClient()->getName()
            ).".$extension";
    }

    private function generateSpreadsheet(User $user): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();

        $this->generateUserDetailsWorkSheet($spreadsheet, $user);

        $this->generateSessionsLinkedWorksheet($spreadsheet, $user);

        $this->generateSessionsResultsEvaluationWorksheet($spreadsheet, $user);

        $this->generateActivityLogWorksheet($spreadsheet, $user);

        foreach ($spreadsheet->getAllSheets() as $sheet) {
            foreach ($sheet->getColumnIterator() as $column) {
                $sheet->getColumnDimension($column->getColumnIndex())->setAutoSize(true);
            }
        }

        return $spreadsheet;
    }

    private function generateXLSX(User $user): Xlsx
    {
        return new Xlsx($this->generateSpreadsheet($user));
    }

    private function getSpreadsheetWriter(User $user): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            ExportFormat::XLSX => $this->generateXLSX($user),
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function getSpreadsheetWriterGrouping(array $users): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function generateExportForSessions(Worksheet $worksheet, int $i, array $sessions, string $title, User $user, string $exportType): void
    {
        $worksheet->setCellValue('A'.$i, $title);
        ++$i;

        $worksheet->setCellValue('A'.$i, $this->translator->trans('Session reference'));
        $worksheet->setCellValue('B'.$i, $this->translator->trans('Session Date'));
        $worksheet->setCellValue('C'.$i, $this->translator->trans('Session name'));
        $worksheet->setCellValue('D'.$i, $this->translator->trans('Session client'));
        $worksheet->setCellValue('E'.$i, $this->translator->trans('Session provider'));
        $worksheet->setCellValue('F'.$i, $this->translator->trans('Session provider ID'));

        if ($exportType === 'Participation') {
            $worksheet->setCellValue('G'.$i, $this->translator->trans('Status'));
            $worksheet->setCellValue('H'.$i, $this->translator->trans('Connection time'));
        }

        /** @var ProviderSession $session */
        foreach ($sessions as $session) {
            ++$i;
            $worksheet->setCellValue('A'.$i, $session->getRef());
            $worksheet->setCellValue('B'.$i, $session->getDateStart()->format('G'));
            $worksheet->setCellValue('C'.$i, $session->getName());
            $worksheet->setCellValue('D'.$i, $session->getClient()->getName());
            $worksheet->setCellValue('E'.$i, $session::getProvider());
            $worksheet->setCellValue('F'.$i, $session->getProviderDistantID());

            if ($exportType === 'Participation') {
                $participantUser = $this->providerParticipantSessionRoleRepository->findBySessionAndUser($session, $user);
                $worksheet->setCellValue('G'.$i, $participantUser->isPresent() ? $this->translator->trans('Present') : $this->translator->trans('Absent'));
                $worksheet->setCellValue('H'.$i, ($participantUser->getSlotPresenceDuration() ?: '0').' min');
            }
        }
    }

    private function generateUserDetails(Worksheet $worksheet, int $i, User $user): void
    {
        $worksheet->setCellValue('A'.$i, $this->translator->trans('Last name'));
        $worksheet->setCellValue('B'.$i, $this->translator->trans('First name'));
        $worksheet->setCellValue('C'.$i, $this->translator->trans('Email'));

        ++$i;
        $worksheet->setCellValue('A'.$i, $user->getLastName());
        $worksheet->setCellValue('B'.$i, $user->getFirstName());
        $worksheet->setCellValue('C'.$i, $user->getEmail());
    }

    private function generateUserRoleDetails(Worksheet $worksheet, int $i, User $user): void
    {
        if (!empty($user->getRoles())) {
            $worksheet->setCellValue('A'.$i, $this->translator->trans('Role name'));

            foreach ($user->getRoles() as $role) {
                ++$i;
                $worksheet->setCellValue('A'.$i, $role);
            }
        }
    }

    private function generateActivityLogWorksheet(Spreadsheet $spreadsheet, User $user): void
    {
        $worksheet = $spreadsheet->createSheet();
        $worksheet->setTitle($this->translator->trans('Activity log'));

        $i = 1;

        $activities = $this->activityLogRepository->findAllActivityLogForUser($user);

        if (count($activities) > 0) {
            $worksheet->setCellValue('A'.$i, $this->translator->trans('Created at'));
            $worksheet->setCellValue('B'.$i, $this->translator->trans('Created by'));
            $worksheet->setCellValue('C'.$i, $this->translator->trans('Origin'));
            $worksheet->setCellValue('D'.$i, $this->translator->trans('Action'));
            $worksheet->setCellValue('E'.$i, $this->translator->trans('Severity'));
            $worksheet->setCellValue('F'.$i, $this->translator->trans('Informations'));

            /** @var ActivityLog $activity */
            foreach ($activities as $activity) {
                ++$i;

                $worksheet->setCellValue('A'.$i, $activity->getCreatedAt()->format('c'));
                $worksheet->setCellValue('B'.$i, $activity->getUser()->getFullName());
                $worksheet->setCellValue('C'.$i, $activity->getOrigin());
                $worksheet->setCellValue('D'.$i, $activity->getAction());
                $worksheet->setCellValue('E'.$i, $activity->getSeverity());
                $worksheet->setCellValue('F'.$i, json_encode($activity->getInfos()));
            }
        }
    }

    private function generateUserDetailsWorkSheet(Spreadsheet $spreadsheet, User $user): void
    {
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setTitle($this->translator->trans('User details'));

        $i = 1;
        $this->generateUserDetails($worksheet, $i, $user);

        $i = 4;
        $this->generateUserRoleDetails($worksheet, $i, $user);
    }

    private function generateSessionsLinkedWorksheet(Spreadsheet $spreadsheet, User $user): void
    {
        $worksheet = $spreadsheet->createSheet();
        $worksheet->setTitle($this->translator->trans('Linked Sessions'));

        $i = 1;

        $sessions = $this->providerSessionRepository->findAllSessionByUser($user);

        if (count($sessions) > 0) {
            $this->generateExportForSessions(
                $worksheet,
                $i,
                $sessions,
                $this->translator->trans('Participant or animator of the sessions'),
                $user,
                'Participation'
            );
        }
        $i += 3 + count($sessions);

        // Fouth Block - Sessions created by the user
        $sessions = $this->providerSessionRepository->findBy([
            'createdBy' => $user,
        ]);

        if (count($sessions) > 0) {
            $this->generateExportForSessions(
                $worksheet,
                $i,
                $sessions,
                $this->translator->trans('Sessions created by').' '.$user->getFullName(),
                $user,
                'Creation'
            );
        }
    }

    private function generateSessionsResultsEvaluationWorksheet(Spreadsheet $spreadsheet, User $user): void
    {
        $worksheet = $spreadsheet->createSheet();
        $worksheet->setTitle($this->translator->trans('Results of evaluation'));

        $i = 0;
        $sessions = $this->providerSessionRepository->findAllSessionByUser($user);

        ++$i;
        $worksheet->setCellValue('A'.$i, $this->translator->trans('Session reference'));
        $worksheet->setCellValue('B'.$i, $this->translator->trans('Session client'));

        /** @var ProviderSession $session */
        foreach ($sessions as $session) {
            $participantUser = $this->providerParticipantSessionRoleRepository->findBySessionAndUser($session, $user);
            ++$i;
            $worksheet->setCellValue('A'.$i, $session->getRef());
            $worksheet->setCellValue('B'.$i, $session->getClient()->getName());
            if ($participantUser->getAnswers()->isEmpty()) {
                $worksheet->setCellValue('C'.$i, $this->translator->trans('No answer'));
                ++$i;
            } else {
                foreach ($participantUser->getAnswers() as $answer) {
                    $nextLigne = $i + 1;
                    $worksheet->setCellValue('C'.$i, $this->translator->trans('Evaluation title'));
                    $worksheet->setCellValue('D'.$i, $this->translator->trans('Date of reply'));
                    $worksheet->setCellValue('C'.$nextLigne, $answer->getEvaluation()->getTitle());
                    $worksheet->setCellValue('D'.$nextLigne, $answer->getReplyDate()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i'));
                    foreach ($answer->getAnswer()['plainData'] as $key => $response) {
                        $worksheet->setCellValue($this->getLetter(5 + $key).$i, $response['title']);
                        $worksheet->setCellValue($this->getLetter(5 + $key).$nextLigne, ' '.$response['displayValue']);
                    }
                    $i += 2;
                }
            }
        }
    }

    public function getLetter($number): string
    {
        $letter = '';
        while ($number > 0) {
            $remainder = ($number - 1) % 26;
            $letter = chr($remainder + 65).$letter;
            $number = floor(($number - 1) / 26);
        }

        return $letter;
    }
}
