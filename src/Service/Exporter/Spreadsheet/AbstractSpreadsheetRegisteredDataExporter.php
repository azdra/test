<?php

namespace App\Service\Exporter\Spreadsheet;

use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Service\Exporter\ExporterInterface;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\ProviderSessionExporterInterface;
use App\Service\Utils\SpreadsheetUtils;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSpreadsheetRegisteredDataExporter implements ProviderSessionExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'export_registered_session';
    public const EXPORT_FORMAT = ExportFormat::XLSX;

    public function __construct(
        private TranslatorInterface $translator,
        private SluggerInterface $slugger,
        private RouterInterface $router,
        private string $endPointDomain,
        private Security $security,
        private string $exportsSavefilesDirectory
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    abstract public function getSupportFormat(): string;

    public function createExportFile(ProviderSession $session): string
    {
        throw new \LogicException('Not yet implemented');
    }

    public function createExportFileGrouping(array $sessions): string
    {
        $writer = $this->getSpreadsheetWriterGrouping($sessions);
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFileName();
        $writer->save($path);

        return $path;
    }

    public function createExportResponse(ProviderSession $session): Response
    {
        throw new \LogicException('Not yet implemented');
    }

    private function generateFileName(): string
    {
        $extension = strtolower($this->getSupportFormat());

        return $this->slugger->slug(
            'export-registered-session-data'
            ).".$extension";
    }

    private function generateSpreadsheetGrouping(array $sessions): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setTitle($this->translator->trans('Registered data export'));

        $rowNumber = 1;
        foreach ($sessions as $indexSession => $session) {
            $allRegistered = $session->getParticipantRoles();
            foreach ($allRegistered as $indexRegistered => $registered) {
                if ($indexSession === 0 && $indexRegistered === 0) {
                    $rows = array_merge($this->getHeaders($session), $this->generateRows($session, $registered));
                } else {
                    $rows = $this->generateRows($session, $registered);
                }

                if (!empty($rows)) {
                    for ($i = 1; $i < count($rows[0]); ++$i) {
                        $spreadsheet->getActiveSheet()
                            ->getColumnDimension(SpreadsheetUtils::numberToLetter($i))
                            ->setAutoSize(true);
                    }
                }

                $rowNumber = $this->insertRowsDataIntoSpreadSheet($rows, $worksheet, $rowNumber);
            }
        }

        return $spreadsheet;
    }

    private function getSpreadsheetWriterGrouping(array $sessions): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            ExportFormat::XLSX => new Xlsx($this->generateSpreadsheetGrouping($sessions)),
            ExportFormat::CSV => new Csv($this->generateSpreadsheetGrouping($sessions)),
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function getHeaders(ProviderSession $session): array
    {
        $header = [
            $this->translator->trans('User reference'),
            $this->translator->trans('Session reference'),
            $this->translator->trans('Type'),
            $this->translator->trans('First name'),
            $this->translator->trans('Last name'),
            $this->translator->trans('Email'),
            $this->translator->trans('Company'),
            $this->translator->trans('Phone'),
            $this->translator->trans('Preferred language'),
            $this->translator->trans('Informations'),
            $this->translator->trans('Start date'),
            $this->translator->trans('Start time'),
            $this->translator->trans('End date'),
            $this->translator->trans('End time'),
            $this->translator->trans('Duration'),
            $this->translator->trans('Session name'),
        ];

        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            array_splice($header, 17, 0, $this->translator->trans('Account'));
        }
        if ($session->getClient()->isEnableAccessLinkOnExport()) {
            array_splice($header, 18, 0, $this->translator->trans('Participant URL'));
        }

        if ($session->getRegistrationPageTemplate() && $session->getRegistrationPageTemplate()->getRegistrationForm()) {
            foreach ($session->getRegistrationPageTemplate()->getRegistrationForm() as $field) {
                $header[] = $this->translator->trans($field['question']);
            }
        }

        if ($session->getSubject() && $session->getSubject()->getTheme()->getModule()->getRegistrationForm()) {
            $registrationFormResponses = array_filter($session->getSubject()->getTheme()->getModule()->getRegistrationForm(), function ($item) {
                return !in_array($item['type'], User::TYPES_TO_EXCLUDE);
            });
            foreach ($registrationFormResponses as $fieldModule) {
                $header[] = $this->translator->trans($fieldModule['question']);
            }
        }

        return [$header];
    }

    private function generateRows(ProviderSession $session, ProviderParticipantSessionRole $registered): array
    {
        $timeZone = new \DateTimeZone($session->getClient()->getTimezone());
        $rows = [
            $registered->getParticipant()->getUser()->getReference(),
            $session->getRef(),
            $this->translator->trans($registered->getRole()),
            $registered->getFirstName(),
            $registered->getLastName(),
            $registered->getEmail(),
            $registered->getParticipant()->getUser()->getCompany(),
            $registered->getParticipant()->getUser()->getPhone(),
            $registered->getParticipant()->getUser()->getPreferredLang(),
            $registered->getParticipant()->getUser()->getInformation(),
            $session->getDateStart()->setTimezone($timeZone)->format('Y-m-d'),
            $session->getDateStart()->setTimezone($timeZone)->format('H:i:s'),
            $session->getDateEnd()->setTimezone($timeZone)->format('Y-m-d'),
            $session->getDateEnd()->setTimezone($timeZone)->format('H:i:s'),
            $session->getDuration(),
            $session->getName(),
        ];

        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            array_splice($rows, 17, 0, $session->getClient()->getName());
        }
        if ($session->getClient()->isEnableAccessLinkOnExport()) {
            array_splice($rows, 18, 0, $this->endPointDomain.$this->router->generate('_session_access_plateforme', ['token' => $registered->getProviderSessionAccessToken()->getToken()]));
        }
        $customData = [];
        if ($session->getRegistrationPageTemplate() && $session->getRegistrationPageTemplate()->getRegistrationForm()) {
            $registrationForm = $session->getRegistrationPageTemplate()->getRegistrationForm();
            $registrationFormResponses = $registered->getRegistrationFormResponses();

            foreach ($registrationForm as $field) {
                $responseValue = null;
                foreach ($registrationFormResponses as $response) {
                    if ($response['question'] === $field['question']) {
                        $responseValue = $response['response'];
                        break;
                    }
                }
                $customData[] = $responseValue;
            }
        }

        if ($session->getSubject() && $session->getSubject()->getTheme()->getModule()->getRegistrationForm()) {
            $registrationForm = array_filter($session->getSubject()->getTheme()->getModule()->getRegistrationForm(), function ($item) {
                return !in_array($item['type'], User::TYPES_TO_EXCLUDE);
            });
            $registrationFormResponses = $registered->getParticipant()->getUser()->getRegistrationFormCatalogResponses();
            foreach ($registrationForm as $fieldModule) {
                $responseValue = null;
                foreach ($registrationFormResponses as $response) {
                    if ($response['question'] === $fieldModule['question']) {
                        $responseValue = $response['response'];
                        break;
                    }
                }
                $customData[] = $responseValue;
            }
        }

        $rows = array_merge($rows, $customData);

        return [$rows];
    }

    private function insertRowsDataIntoSpreadSheet(
        array $rows,
        Worksheet $worksheet,
        int $rowNumber): int
    {
        foreach ($rows as $rowData) {
            $columnNumber = 1;
            foreach ($rowData as $cellData) {
                $worksheet->setCellValueByColumnAndRow($columnNumber, $rowNumber, $cellData);
                ++$columnNumber;
            }
            ++$rowNumber;
        }

        return $rowNumber;
    }
}
