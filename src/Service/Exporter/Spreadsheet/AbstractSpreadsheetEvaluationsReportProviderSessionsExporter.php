<?php

namespace App\Service\Exporter\Spreadsheet;

use App\Entity\Evaluation;
use App\Entity\EvaluationProviderSession;
use App\Entity\ProviderSession;
use App\Repository\EvaluationParticipantAnswerRepository;
use App\Repository\EvaluationProviderSessionRepository;
use App\Service\Exporter\ExporterInterface;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\ProviderSessionExporterInterface;
use App\Service\Utils\SpreadsheetUtils;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSpreadsheetEvaluationsReportProviderSessionsExporter implements ProviderSessionExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'multi_evaluations_report';

    public function __construct(
        private TranslatorInterface $translator,
        private SluggerInterface $slugger,
        private string $exportsSavefilesDirectory,
        private EvaluationParticipantAnswerRepository $evaluationParticipantAnswerRepository,
        private EvaluationProviderSessionRepository $evaluationProviderSessionRepository
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    abstract public function getSupportFormat(): string;

    public function createExportFile(ProviderSession $session): string
    {
        $writer = $this->getSpreadsheetWriter($session);
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFileName($session);

        $writer->save($path);

        return $path;
    }

    public function createExportFileGrouping(array $sessions): string
    {
        $writer = $this->getSpreadsheetWriterGrouping($sessions);
        //dump(reset($sessions));
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFileName(reset($sessions));
        $writer->save($path);

        return $path;
    }

    public function createExportResponse(ProviderSession $session): Response
    {
        $writer = $this->getSpreadsheetWriter($session);
        $filename = $this->generateFileName($session);
        $writer->save('php://output');

        $response = new Response();
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Cache-Control', 'max-age=0');

        return $response;
    }

    private function generateEvaluationHeader(Worksheet $worksheet, &$row, $evaluation): void
    {
        $headers = $this->getHeaders($evaluation['evaluationSessionAnswers']);
        for ($colNumber = 0; $colNumber < count($headers); ++$colNumber) {
            $currentColumn = SpreadsheetUtils::numberToLetter($colNumber + 1);
            $worksheet->setCellValue($currentColumn.$row, $headers[$colNumber]);
        }
        ++$row;
    }

    private function generateEvaluationProvider(Worksheet $worksheet, &$row, $data): void
    {
        $type = $this->translator->trans(EvaluationProviderSession::reverseEvaluationType[$data['evaluationType']]);
        $worksheet->setCellValue(sprintf('A%s', $row), $this->translator->trans('Evaluation type').' '.$type)
                  ->mergeCells(sprintf('A%s:B%s', $row, $row + 1));
        $worksheet->setCellValue(sprintf('C%s', $row), $data['evaluation']['title'])
                  ->mergeCells(sprintf('C%s:D%s', $row, $row + 1));
        $worksheet->setCellValue(sprintf('E%s', $row), $this->translator->trans('Participant information'))
                  ->mergeCells(sprintf('E%s:H%s', $row, $row + 1));
        $worksheet->setCellValue(sprintf('I%s', $row), $this->translator->trans('Questions'))
                  ->mergeCells(sprintf('I%s:K%s', $row, $row + 1));
        $worksheet->getStyle(sprintf('A%s:K%s', $row, $row + 1))
                  ->getAlignment()
                  ->setVertical(Alignment::VERTICAL_CENTER)
                  ->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $row += 2;
    }

    private function generateSpreadsheet(ProviderSession $session): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $data = $this->evaluationProviderSessionRepository->getEvaluationProviderSessionForSession($session);

        foreach ($data as $index => $evaluation) {
            $type = $this->translator->trans(EvaluationProviderSession::reverseEvaluationType[$evaluation['evaluationType']]);
            $worksheet = $index === 0 ? $spreadsheet->getActiveSheet() : $spreadsheet->createSheet();
            $worksheet->setTitle(mb_strimwidth($type.' - '.$evaluation['evaluation']['title'], 0, 31, '...'));

            $row = 1;
            $this->generateEvaluationProvider($worksheet, $row, $evaluation);
            $this->generateEvaluationHeader($worksheet, $row, $evaluation);

            foreach ($evaluation['evaluationSessionAnswers'] as $evaluationSessionAnswer) {
                $user = $evaluationSessionAnswer['providerParticipantSessionRole']['participant']['user'];
                $extractedData = [
                    'firstName' => $user['firstName'],
                    'lastName' => $user['lastName'],
                    'email' => $user['email'],
                    'replyDate' => $evaluationSessionAnswer['replyDate'],
                    'answer' => $evaluationSessionAnswer['answer'],
                ];

                $rows = $this->generateRows($session, $extractedData);

                for ($i = 1; $i < count($rows[0]); ++$i) {
                    $spreadsheet->getActiveSheet()
                                ->getColumnDimension(SpreadsheetUtils::numberToLetter($i))
                                ->setAutoSize(true)
                    ;
                }

                foreach ($rows as $rowData) {
                    $columnNumber = 1;
                    foreach ($rowData as $cellData) {
                        if (is_array($cellData)) {
                            $cellData = implode(', ', $cellData);
                        }
                        $worksheet->setCellValueByColumnAndRow($columnNumber, $row, $cellData);
                        ++$columnNumber;
                    }
                    ++$row;
                }
            }
        }

        return $spreadsheet;
    }

    private function getSpreadsheetWriterGrouping(array $sessions): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            ExportFormat::XLSX => new Xlsx($this->generateSpreadsheetGrouping($sessions)),
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function generateSpreadsheetGrouping(array $sessions): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();

        $rowNumber = 1;
        $sheetIndex = [];
        foreach ($sessions as $index => $session) {
            $evaluations = $session->getEvaluations();
            //dump('Index: '.$index.' | session: '.$session->getId().' | nbrEvaluation: '.count($evaluations));
            if (count($evaluations) === 0) {
                throw new \LogicException('There is no evaluation associated with the session');
            }
            /** @var Evaluation $evaluation */
            $evaluation = $evaluations->first();
            $evaluationTitle = $this->getCleanTitleSheet("{$this->translator->trans('Form')} {$evaluation->getTitle()}");
            $sheetNew = $evaluationTitle;
            if (empty($sheetIndex)) {
                $sheetIndex[] = $sheetNew;
                $worksheet = $spreadsheet->getActiveSheet();
                $worksheet->setTitle($sheetNew);
                $worksheet->setCellValue('A1', "{$this->translator->trans('Form')} {$evaluation->getTitle()}")
                    ->mergeCells('A1:B2');
                $worksheet->setCellValue('C1', $this->translator->trans('Participant information'))
                    ->mergeCells('C1:F2');
                $worksheet->setCellValue('G1', $this->translator->trans('Questions'))
                    ->mergeCells('G1:L2');
                $worksheet->getStyle('A1:L2')
                    ->getAlignment()
                    ->setVertical(Alignment::VERTICAL_CENTER)
                    ->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $rowNumber = 3;
            } else {
                if (!in_array($sheetNew, $sheetIndex)) {
                    $sheetIndex[] = $sheetNew;
                    $worksheet = $spreadsheet->createSheet();
                    $worksheet->setTitle($sheetNew);
                    $worksheet->setCellValue('A1', "{$this->translator->trans('Form')} {$evaluation->getTitle()}")
                        ->mergeCells('A1:B2');
                    $worksheet->setCellValue('C1', $this->translator->trans('Participant information'))
                        ->mergeCells('C1:F2');
                    $worksheet->setCellValue('G1', $this->translator->trans('Questions'))
                        ->mergeCells('G1:L2');
                    $worksheet->getStyle('A1:L2')
                        ->getAlignment()
                        ->setVertical(Alignment::VERTICAL_CENTER)
                        ->setHorizontal(Alignment::HORIZONTAL_CENTER);
                    $rowNumber = 3;
                }
            }
            $extractedData = $this->evaluationParticipantAnswerRepository->getSessionAnswerForEvaluation($evaluation, $session);
            $headers = $this->getHeaders($extractedData);
            if ($index === 0 || $rowNumber == 3) {
                $rows = $this->generateRows($session, $extractedData);
                array_unshift($rows, $headers);
            } else {
                $rows = $this->generateRows($session, $extractedData);
            }
            $rowNumber = $this->insertRowsDataIntoSpreadSheet($rows, $worksheet, $rowNumber);
        }

        return $spreadsheet;
    }

    private function insertRowsDataIntoSpreadSheet(
        array $rows,
        Worksheet $worksheet,
        int $rowNumber): int
    {
        foreach ($rows as $rowData) {
            $columnNumber = 1;
            foreach ($rowData as $cellData) {
                $worksheet->setCellValueByColumnAndRow($columnNumber, $rowNumber, $cellData);
                ++$columnNumber;
            }
            ++$rowNumber;
        }

        return $rowNumber;
    }

    private function generateFileName(ProviderSession $session): string
    {
        $extension = strtolower($this->getSupportFormat());

        return $this->slugger->slug($session->getDateStart()->format('Y-m-d_H\hi').'-evaluation-live-session-'.$session->getRef()).".$extension";
    }

    private function generateXLSX(ProviderSession $session): Xlsx
    {
        return new Xlsx($this->generateSpreadsheet($session));
    }

    private function generateCSV(ProviderSession $session): Csv
    {
        return new Csv($this->generateSpreadsheet($session));
    }

    private function getSpreadsheetWriter(ProviderSession $session): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            ExportFormat::XLSX => $this->generateXLSX($session),
            ExportFormat::CSV => $this->generateCSV($session),
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function getHeaders(array $answers): array
    {
        $header = [
            $this->translator->trans('Title of the session'),
            $this->translator->trans('Reference'),
            $this->translator->trans('First name'),
            $this->translator->trans('Last name'),
            $this->translator->trans('Email'),
            $this->translator->trans('Date of reply'),
        ];

        if (empty($answers[0]) || empty($answers[0]['answer']) || empty($answers[0]['answer']['plainData'])) {
            throw new \LogicException('An error in the evaluation data blocks the generation of the export');
        }

        $plainData = $this->getSortedPlainData($answers[0]['answer']['plainData']);

        $headerSurvey = [];
        foreach ($plainData as $data) {
            $headerSurvey[] = $data['title'];
        }

        $header = array_merge($header, $headerSurvey);

        return $header;
    }

    private function generateRows(ProviderSession $providerSession, array $data): array
    {
        $rows = [];

        $userData = [
            $providerSession->getName(),
            $providerSession->getRef(),
            $data['firstName'],
            $data['lastName'],
            $data['email'],
            \DateTimeImmutable::createFromMutable($data['replyDate'])->setTimezone(new \DateTimeZone($providerSession->getClient()->getTimezone()))->format('Y-m-d H:i:s'),
        ];

        $sortedAnswer = $this->getSortedPlainData($data['answer']['plainData']);

        $answerData = [];
        foreach ($sortedAnswer as $answer) {
            $answerData[] = $answer['displayValue'];
        }

        $rows[] = array_merge($userData, $answerData);

        return $rows;
    }

    private function getSortedPlainData(array $plainData): array
    {
        usort($plainData, function ($a, $b) {
            $indexA = str_replace('question', '', $a['name']);
            $indexB = str_replace('question', '', $b['name']);

            if ($indexA == $indexB) {
                return 0;
            }

            return ($indexA < $indexB) ? -1 : 1;
        });

        return $plainData;
    }

    private function getCleanTitleSheet(string $title): string
    {
        $chaine = $title; //"Formulaire Evaluation de l'assistance";

        // Supprimer le mot "Formulaire"
        $chaine = str_replace('Formulaire', '', $chaine);

        // Supprimer tous les caractères spéciaux et les espaces
        $chaine = preg_replace('/[^a-zA-Z0-9]/', '', $chaine);

        // Convertir la chaîne en majuscules avec des majuscules après chaque espace
        $chaine = ucwords($chaine);

        // Limiter la chaîne à une longueur maximale de 30 caractères
        $chaine = substr($chaine, 0, 30);

        return $chaine;
        // Sortie : "FormulaireEvaluationDeLAssis" ou "EvaluationDeLAssistance"
    }
}
