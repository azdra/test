<?php

namespace App\Service\Exporter\Spreadsheet;

use App\Entity\Guest;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\WebinarConvocation;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Service\Exporter\ExporterInterface;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\ProviderSessionExporterInterface;
use App\Service\Utils\SpreadsheetUtils;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSpreadsheetStatisticsGuestsWebinarConvocationExporter implements ProviderSessionExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'statistics_guests_communication_report';
    public const EXPORT_FORMAT = ExportFormat::XLSX;
    public const SIGNATURE_TEMP_PATH = '/tmp_signatures/';

    public function __construct(
        private TranslatorInterface $translator,
        private SluggerInterface $slugger,
        private string $exportsSavefilesDirectory,
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    abstract public function getSupportFormat(): string;

    public function createExportFile(ProviderSession $session): string
    {
        throw new \LogicException('Not yet implemented');
    }

    /**
     * @throws \Exception
     */
    public function createExportStatisticsGuests(WebinarConvocation $webinarConvocation): string
    {
        $writer = $this->getSpreadsheetWriter($webinarConvocation);
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFileName($webinarConvocation);

        $writer->save($path);
        $this->removeSignatureTempFolder();

        return $path;
    }

    public function createExportFileGrouping(array $sessions): string
    {
        throw new \LogicException('Not yet implemented');
    }

    public function createExportResponse(ProviderSession $session): Response
    {
        throw new \LogicException('Not yet implemented');
    }

    /**
     * @throws \Exception
     */
    private function generateFileName(WebinarConvocation $webinarConvocation): string
    {
        $extension = strtolower($this->getSupportFormat());

        $sessionDateStart = $webinarConvocation->getConvocationDate()
            ->setTimezone(new \DateTimeZone($webinarConvocation->getSession()->getClient()->getTimezone()))
            ->format('Y-m-d_H\hi');

        return $this->slugger->slug(
                $sessionDateStart.'-statistics-guest-invitation-'.$webinarConvocation->getSession()->getRef()
            ).".$extension";
    }

    private function generateSpreadsheet(WebinarConvocation $webinarConvocation): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setTitle($this->translator->trans('Statistics guests'));

        $header = $this->getHeaders();
        $rows = array_merge($header, $this->generateRows($webinarConvocation));

        for ($i = 1; $i < count($rows[0]); ++$i) {
            $spreadsheet->getActiveSheet()
                ->getColumnDimension(SpreadsheetUtils::numberToLetter($i))
                ->setAutoSize(true);
        }

        $rowNumber = 1;
        $this->insertRowsDataIntoSpreadSheet($rows, $worksheet, $rowNumber);

        return $spreadsheet;
    }

    private function generateXLSX(WebinarConvocation $webinarConvocation): Xlsx
    {
        return new Xlsx($this->generateSpreadsheet($webinarConvocation));
    }

    private function generateCSV(WebinarConvocation $webinarConvocation): Csv
    {
        return new Csv($this->generateSpreadsheet($webinarConvocation));
    }

    private function getSpreadsheetWriter(WebinarConvocation $webinarConvocation): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            ExportFormat::XLSX => $this->generateXLSX($webinarConvocation),
            ExportFormat::CSV => $this->generateCSV($webinarConvocation),
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function getHeaders(): array
    {
        $header = [
            $this->translator->trans('ID'),
            $this->translator->trans('Last name'),
            $this->translator->trans('First name'),
            $this->translator->trans('Email'),
            $this->translator->trans('Status'),
            $this->translator->trans('Last send email'),
            $this->translator->trans(' '),
        ];

        return [$header];
    }

    /**
     * @throws \Exception
     */
    private function generateRows(WebinarConvocation $webinarConvocation): array
    {
        $rows = [];
        $dateLastSend = $webinarConvocation->isSent() ? $webinarConvocation->getConvocationDate()->setTimezone(new \DateTimeZone($webinarConvocation->getSession()->getClient()->getTimezone()))->format('d/m/Y H:i') : '';

        /* @var ProviderParticipantSessionRole $participantRole */
        foreach ($webinarConvocation->getGuests() as $guest) {
            $guestData = $this->generateGuestRow($guest, $dateLastSend);
            $rows[] = $guestData;
        }

        return $rows;
    }

    private function generateGuestRow(Guest $guest, string $dateLastSend): array
    {
        return [
            $guest->getId(),
            $guest->getLastName(),
            $guest->getFirstName(),
            $guest->getEmail(),
            $this->getStatusGuest($guest),
            $guest->isEmailSent() ? $dateLastSend : '',
        ];
    }

    private function removeSignatureTempFolder(): void
    {
        $files = glob($this->exportsSavefilesDirectory.self::SIGNATURE_TEMP_PATH.'*');

        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        if (is_dir($this->exportsSavefilesDirectory.self::SIGNATURE_TEMP_PATH)) {
            rmdir($this->exportsSavefilesDirectory.self::SIGNATURE_TEMP_PATH);
        }
    }

    private function insertRowsDataIntoSpreadSheet(
        array $rows,
        Worksheet $worksheet,
        int $rowNumber): void
    {
        foreach ($rows as $rowData) {
            $columnNumber = 1;
            foreach ($rowData as $cellData) {
                $worksheet->setCellValueByColumnAndRow($columnNumber, $rowNumber, $cellData);
                ++$columnNumber;
            }
            ++$rowNumber;
        }
    }

    private function getStatusGuest(Guest $guest): string
    {
        if ($guest->isUnsubscribe()) {
            return $this->translator->trans('Guest Unsubscribed');
        } elseif ($this->isInscritInSession($guest)) {
            return $this->translator->trans('Registered');
        } else {
            return $this->translator->trans('On hold');
        }
    }

    private function isInscritInSession(Guest $guest): bool
    {
        return (bool) $this->providerParticipantSessionRoleRepository->getParticipantWithSessionAndEmail(
            $guest->getWebinarConvocation()->getSession(),
            $guest->getEmail()
        );
    }
}
