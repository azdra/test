<?php

namespace App\Service\Exporter\Spreadsheet\Csv;

use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetRegisteredDataExporter;

class CsvSpreadsheetDataRegisteredSessionExporter extends AbstractSpreadsheetRegisteredDataExporter
{
    public const EXPORT_FORMAT = ExportFormat::CSV;

    public function getSupportFormat(): string
    {
        return self::EXPORT_FORMAT;
    }
}
