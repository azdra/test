<?php

namespace App\Service\Exporter\Spreadsheet\Csv;

use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetAttendanceReportProviderSessionExporter;

class CsvSpreadsheetAttendanceReportProviderSessionExporter extends AbstractSpreadsheetAttendanceReportProviderSessionExporter
{
    public const EXPORT_FORMAT = ExportFormat::CSV;

    public function getSupportFormat(): string
    {
        return self::EXPORT_FORMAT;
    }
}
