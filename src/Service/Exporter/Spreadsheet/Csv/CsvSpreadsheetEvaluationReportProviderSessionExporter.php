<?php

namespace App\Service\Exporter\Spreadsheet\Csv;

use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetEvaluationReportProviderSessionExporter;

class CsvSpreadsheetEvaluationReportProviderSessionExporter extends AbstractSpreadsheetEvaluationReportProviderSessionExporter
{
    public const EXPORT_FORMAT = ExportFormat::CSV;

    public function getSupportFormat(): string
    {
        return self::EXPORT_FORMAT;
    }
}
