<?php

namespace App\Service\Exporter\Spreadsheet;

use App\Entity\ProviderSession;
use App\Entity\ProviderSessionReplay;
use App\Service\Exporter\ExporterInterface;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\ProviderSessionExporterInterface;
use App\Service\Utils\SpreadsheetUtils;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSpreadsheetReplayStatisticsExporter implements ProviderSessionExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'replay_export_statistics';
    public const EXPORT_FORMAT = ExportFormat::XLSX;

    public function __construct(
        private TranslatorInterface $translator,
        private SluggerInterface $slugger,
        private string $exportsSavefilesDirectory
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    abstract public function getSupportFormat(): string;

    public function createExportFile(ProviderSession $session): string
    {
        $writer = $this->getSpreadsheetWriter($session);
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFileName($session->getRef());

        $writer->save($path);

        return $path;
    }

    public function createExportFileGrouping(array $sessions): string
    {
        throw new \LogicException('Not yet implemented');
    }

    public function createExportResponse(ProviderSession $session): Response
    {
        throw new \LogicException('Not yet implemented');
    }

    private function generateFileName(string $reference): string
    {
        $extension = strtolower($this->getSupportFormat());

        return $this->slugger->slug(
            'export-replay-statistics-session'.$reference
            ).".$extension";
    }

    private function getSpreadsheetWriter(ProviderSession $session): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            ExportFormat::XLSX => $this->generateXLSX($session),
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function getHeaders(ProviderSession $session): array
    {
        $header = [
            $this->translator->trans('Session name'),
            $this->translator->trans('Session reference'),
            $this->translator->trans('First name'),
            $this->translator->trans('Last name'),
            $this->translator->trans('Email'),
            $this->translator->trans('Number of views'),
            $this->translator->trans('Date of last viewing'),
        ];

        if ($session->getRegistrationPageTemplate() && $session->getRegistrationPageTemplate()->getRegistrationForm()) {
            foreach ($session->getRegistrationPageTemplate()->getRegistrationForm() as $question) {
                $header[] = $question['question'];
            }
        }

        return [$header];
    }

    /**
     * @throws \Exception
     */
    private function generateRows(ProviderSession $session): array
    {
        $commonHeader = [
            $session->getName(),
            $session->getRef(),
        ];

        $rows = [];
        /** @var ProviderSessionReplay $replay */
        foreach ($session->getReplays() as $replay) {
            $lastShowReplay = $replay->getLastShowReplay();
            $userData = array_merge(
                $commonHeader,
                [
                    $replay->getFirstname(),
                    $replay->getLastname(),
                    $replay->getEmail(),
                    $replay->getSeen(),
                    $lastShowReplay ? $lastShowReplay->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i') : $this->translator->trans("Haven't looked yet"),
                ],
            );
            $responses = [];
            foreach ($replay->getRegistrationFormResponses() as $responseShow) {
                if ($responseShow['type'] == 'checkbox') {
                    if (is_array($responseShow['response'])) {
                        $responses[] = implode(', ', $responseShow['response']);
                    } else {
                        $responses[] = $responseShow['response'];
                    }
                } else {
                    $responses[] = $responseShow['response'];
                }
            }
            $rows[] = array_merge($userData, $responses);
        }

        return $rows;
    }

    private function insertRowsDataIntoSpreadSheet(
        array $rows,
        Worksheet $worksheet,
        int $rowNumber): int
    {
        foreach ($rows as $rowData) {
            $columnNumber = 1;
            foreach ($rowData as $cellData) {
                $worksheet->setCellValueByColumnAndRow($columnNumber, $rowNumber, $cellData);
                ++$columnNumber;
            }
            ++$rowNumber;
        }

        return $rowNumber;
    }

    private function generateXLSX(ProviderSession $session): Xlsx
    {
        return new Xlsx($this->generateSpreadsheet($session));
    }

    private function generateSpreadsheet(ProviderSession $session): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setTitle($this->translator->trans('Replay statistics'));

        $header = $this->getHeaders($session);
        $rows = array_merge($header, $this->generateRows($session, $header));

        for ($i = 1; $i < count($rows[0]) + 1; ++$i) {
            $spreadsheet->getActiveSheet()
                        ->getColumnDimension(SpreadsheetUtils::numberToLetter($i))
                        ->setAutoSize(true);
        }
        $this->insertRowsDataIntoSpreadSheet($rows, $worksheet, 1, $spreadsheet);

        return $spreadsheet;
    }
}
