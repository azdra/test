<?php

namespace App\Service\Exporter\Spreadsheet;

use App\CustomerService\Entity\HelpNeed;
use App\CustomerService\Entity\HelpTask;
use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\CategorySessionType;
use App\Entity\ProviderSession;
use App\Service\Exporter\ExporterInterface;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\ProviderSessionTPMExporterInterface;
use App\Service\Utils\SpreadsheetUtils;
use Doctrine\Common\Collections\Collection;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSpreadsheetSessionTPMDataExporter implements ProviderSessionTPMExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'sessions_export_tpm';
    public const EXPORT_FORMAT = ExportFormat::XLSX;

    public function __construct(
        private TranslatorInterface $translator,
        private SluggerInterface $slugger,
        private string $exportsSavefilesDirectory
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    abstract public function getSupportFormat(): string;

    public function createExportFile(ProviderSession $session): string
    {
        throw new \LogicException('Not yet implemented');
    }

    public function createExportFileGrouping(array $sessions): string
    {
        $writer = $this->getSpreadsheetWriterGrouping($sessions);
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFileName();
        $writer->save($path);

        return $path;
    }

    public function createExportResponse(ProviderSession $session): Response
    {
        throw new \LogicException('Not yet implemented');
    }

    private function generateFileName(): string
    {
        $extension = strtolower($this->getSupportFormat());

        return $this->slugger->slug(
            'export-session-data-tpm'
            ).".$extension";
    }

    private function generateSpreadsheetGrouping(array $sessions): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();

        $rowNumber = 1;
        $sheetIndex = [];
        foreach ($sessions as $index => $session) {
            if (!$session->isSessionTest()) {
                $clientNew = $session->getClient()->getName();
                if (empty($sheetIndex)) {
                    $sheetIndex[] = $clientNew;
                    $worksheet = $spreadsheet->getActiveSheet();
                    $worksheet->setTitle($clientNew);
                    $rowNumber = 1;
                } else {
                    if (!in_array($clientNew, $sheetIndex)) {
                        $sheetIndex[] = $clientNew;
                        $worksheet = $spreadsheet->createSheet();
                        $worksheet->setTitle($clientNew);
                        $rowNumber = 1;
                    }
                }

                if ($index === 0 || $rowNumber == 1) {
                    $rows = array_merge($this->getHeaders(), $this->generateRows($session));
                } else {
                    $rows = $this->generateRows($session);
                }

                if (!empty($rows)) {
                    for ($i = 1; $i < count($rows[0]); ++$i) {
                        $spreadsheet->getActiveSheet()
                                    ->getColumnDimension(SpreadsheetUtils::numberToLetter($i))
                                    ->setAutoSize(true);
                    }
                }

                $rowNumber = $this->insertRowsDataIntoSpreadSheet($rows, $worksheet, $rowNumber);
            }
        }

        return $spreadsheet;
    }

    private function getSpreadsheetWriterGrouping(array $sessions): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            ExportFormat::XLSX => new Xlsx($this->generateSpreadsheetGrouping($sessions)),
            ExportFormat::CSV => new Csv($this->generateSpreadsheetGrouping($sessions)),
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function getHeaders(): array
    {
        $header = [
            $this->translator->trans('Billed'),
            $this->translator->trans('Account name'),
            $this->translator->trans('Name'),
            $this->translator->trans('Reference'),
            $this->translator->trans('Case number'),
            $this->translator->trans('Assistance start time'),
            $this->translator->trans('Starting date'),
            $this->translator->trans('End date'),
            $this->translator->trans('Category'),
            $this->translator->trans('Statute'),
            $this->translator->trans('Duration').' (mn) ',
            $this->translator->trans('Description').' 1 ',
            $this->translator->trans('Description').' 2 ',
            $this->translator->trans('Assistance'),
            $this->translator->trans('Assisted by'),
            $this->translator->trans('Duration of assistance'),
            //$this->translator->trans('Oncle'),
            $this->translator->trans('Support'),
            $this->translator->trans('Support provided by'),
            $this->translator->trans('Attendees count'),
            $this->translator->trans('Number of animators'),
            $this->translator->trans('Number of attendees'),
            $this->translator->trans('Number days'),
            $this->translator->trans('Lunch break'),
            $this->translator->trans('Self-registration'),
            //$this->translator->trans('Logistics to bill for cancelled sessions'),
            $this->translator->trans('Assistance cancelled within 24 hours'),
            //$this->translator->trans(''),
            $this->translator->trans('Assisted by'),
        ];

        return [$header];
    }

    private function generateRows(ProviderSession $session): array
    {
        if ($session->getCategory() == CategorySessionType::CATEGORY_SESSION_PRESENTIAL) {
            $nbrDays = $session->getNbrdays();
            $addDay = ($nbrDays > 1) ? $nbrDays - 1 : 0;
            if ($addDay == 0) {
                $dateEnd = $session->getDateEnd()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('Y-m-d H:i');
            } else {
                $dateEnd = $session->getDateEnd()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->modify('+'.$addDay.' days')->format('Y-m-d H:i');
            }
            $meetingDateEnd = $dateEnd;
        } else {
            $meetingDateEnd = $session->getDateEnd()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('Y-m-d H:i');
        }
        $meetingDateStart = $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('Y-m-d H:i');

        $services = $session->getClient()->getServices();
        $meetingDurationAssist = $services->getAssistanceDuration() / 60;
        $meetingDateStartAssist = $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->modify("- {$services->getAssistanceStart()} minutes")->format('Y-m-d H:i');

        $assistanceHelpNeeds = $session->getHelpNeedsByType(HelpNeed::TYPE_ASSISTANCE);
        $usersAssistance = '';
        /** @var HelpNeed $assistanceHelpNeed */
        foreach ($assistanceHelpNeeds as $assistanceHelpNeed) {
            /** @var HelpTask $assistanceHelpTask */
            foreach ($assistanceHelpNeed->getHelpTasks() as $assistanceHelpTask) {
                $usersAssistance .= $assistanceHelpTask->getUser()->getFullName().' | ';
            }
        }

        $supportHelpNeeds = $session->getHelpNeedsByType(HelpNeed::TYPE_SUPPORT);
        $usersSupport = '';
        /** @var HelpNeed $supportHelpNeed */
        foreach ($supportHelpNeeds as $supportHelpNeed) {
            /** @var HelpTask $supportHelpTask */
            foreach ($supportHelpNeed->getHelpTasks() as $supportHelpTask) {
                $usersSupport .= $supportHelpTask->getUser()->getFirstName().' '.$supportHelpTask->getUser()->getLastName().' | ';
            }
        }

        $rows[] = [
            '',
            $session->getClient()->getName(),
            $session->getName(),
            $session->getRef(),
            $session->getBusinessNumber(),
            $meetingDateStartAssist,
            $meetingDateStart,
            $meetingDateEnd,
            $session->getCategory()->label(),
            $this->translator->trans($session->getReadableState()),
            $session->getDuration(),
            $session->getDescription(),
            $session->getDescription2(),
            $this->translator->trans($session->getLegendAssistanceType()),
            $usersAssistance,
            $meetingDurationAssist,
            //'astreinteBy_firstname astreinteBy_name',
            $session->getSupportType(),
            $usersSupport,
            count($session->getTraineesOnly()),
            count($session->getAnimatorsOnly()),
            $this->getNombresOfPresence($session->getParticipantRoles()),
            $session->getNbrdays(),
            $session->getRestlunch(),
            $session->getTitleAttachementModuleThemeSubject(),
            //($session->isCancelled()) ? 1 : 0,
            //($usersAssistance != '') ? 'OUI' : 'NON',
            $session->isSessionCanceledLessOneDay() ? $this->translator->trans('YES') : $this->translator->trans('NO'),
            $usersAssistance,
        ];

        return $rows;
    }

    private function insertRowsDataIntoSpreadSheet(
        array $rows,
        Worksheet $worksheet,
        int $rowNumber): int
    {
        foreach ($rows as $rowData) {
            $columnNumber = 1;
            foreach ($rowData as $cellData) {
                $worksheet->setCellValueByColumnAndRow($columnNumber, $rowNumber, $cellData);
                ++$columnNumber;
            }
            ++$rowNumber;
        }

        return $rowNumber;
    }

    private function getTitleEvaluation(Collection $Evaluations): string
    {
        if ($Evaluations->getValues() != []) {
            return $Evaluations->getValues()[0]->getTitle();
        }

        return '';
    }

    private function getNombresOfPresence(Collection $participants): int
    {
        $total = 0;
        foreach ($participants->getValues() as $participant) {
            if ($participant->getPresenceStatus()) {
                ++$total;
            }
        }

        return $total;
    }

    private function getUrlPathSession(ProviderSession $session): ?string
    {
        if ($session instanceof AdobeConnectSCO) {
            return $session->getFullUrlpath();
        }

        return null;
    }

    private function getRoomModelSession(ProviderSession $session): ?int
    {
        if ($session instanceof AdobeConnectSCO) {
            return $session->getRoomModel();
        }

        return null;
    }
}
