<?php

namespace App\Service\Exporter\Spreadsheet;

use App\Entity\Evaluation;
use App\Entity\ProviderSession;
use App\Repository\EvaluationParticipantAnswerRepository;
use App\Service\Exporter\ExporterInterface;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\ProviderSessionExporterInterface;
use App\Service\Utils\SpreadsheetUtils;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSpreadsheetEvaluationReportProviderSessionExporter implements ProviderSessionExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'evaluation_report';

    public function __construct(
        private TranslatorInterface $translator,
        private SluggerInterface $slugger,
        private string $exportsSavefilesDirectory,
        private EvaluationParticipantAnswerRepository $evaluationParticipantAnswerRepository
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    abstract public function getSupportFormat(): string;

    public function createExportFile(ProviderSession $session): string
    {
        $writer = $this->getSpreadsheetWriter($session);
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFileName($session);

        $writer->save($path);

        return $path;
    }

    public function createExportFileGrouping(array $sessions): string
    {
        $writer = $this->getSpreadsheetWriterGrouping($sessions);
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFileName($sessions[0]);
        $writer->save($path);

        return $path;
    }

    public function createExportResponse(ProviderSession $session): Response
    {
        $writer = $this->getSpreadsheetWriter($session);
        $filename = $this->generateFileName($session);
        $writer->save('php://output');

        $response = new Response();
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Cache-Control', 'max-age=0');

        return $response;
    }

    private function generateSpreadsheet(ProviderSession $session): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();

        $evaluations = $session->getEvaluations();

        if (count($evaluations) === 0) {
            throw new \LogicException('There is no evaluation associated with the session');
        }

        /** @var Evaluation $evaluation */
        $evaluation = $evaluations->first();
        $evaluationTitle = "{$this->translator->trans('Form')} {$evaluation->getTitle()}";

        // 31 is the max length allowed on tab title
        $worksheet->setTitle(mb_strimwidth($evaluationTitle, 0, 31, '...'));
        $worksheet->setCellValue('A1', $evaluationTitle)
            ->mergeCells('A1:B2');
        $worksheet->setCellValue('C1', $this->translator->trans('Participant information'))
            ->mergeCells('C1:F2');
        $worksheet->setCellValue('G1', $this->translator->trans('Questions'))
            ->mergeCells('G1:I2');
        $worksheet->getStyle('A1:I2')
            ->getAlignment()
            ->setVertical(Alignment::VERTICAL_CENTER)
            ->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $extractedData = $this->evaluationParticipantAnswerRepository->getSessionAnswerForEvaluation($evaluation, $session);
        $headers = $this->getHeaders($extractedData);

        $rowNumber = 3;

        for ($colNumber = 0; $colNumber < count($headers); ++$colNumber) {
            $currentColumn = SpreadsheetUtils::numberToLetter($colNumber + 1);
            $worksheet->setCellValue($currentColumn.$rowNumber, $headers[$colNumber]);
        }

        ++$rowNumber;

        $rows = $this->generateRows($session, $extractedData);

        for ($i = 1; $i < count($rows[0]); ++$i) {
            $spreadsheet->getActiveSheet()
                ->getColumnDimension(SpreadsheetUtils::numberToLetter($i))
                ->setAutoSize(true)
            ;
        }

        foreach ($rows as $rowData) {
            $columnNumber = 1;
            foreach ($rowData as $cellData) {
                if (is_array($cellData)) {
                    $cellData = implode(', ', $cellData);
                }
                $worksheet->setCellValueByColumnAndRow($columnNumber, $rowNumber, $cellData);
                ++$columnNumber;
            }
            ++$rowNumber;
        }

        return $spreadsheet;
    }

    private function getSpreadsheetWriterGrouping(array $sessions): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function generateFileName(ProviderSession $session): string
    {
        $extension = strtolower($this->getSupportFormat());

        return $this->slugger->slug($session->getDateStart()->format('Y-m-d_H\hi').'-evaluation-live-session-'.$session->getRef()).".$extension";
    }

    private function generateXLSX(ProviderSession $session): Xlsx
    {
        return new Xlsx($this->generateSpreadsheet($session));
    }

    private function generateCSV(ProviderSession $session): Csv
    {
        return new Csv($this->generateSpreadsheet($session));
    }

    private function getSpreadsheetWriter(ProviderSession $session): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            ExportFormat::XLSX => $this->generateXLSX($session),
            ExportFormat::CSV => $this->generateCSV($session),
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function getHeaders(array $answers): array
    {
        $header = [
            $this->translator->trans('Title of the session'),
            $this->translator->trans('Reference'),
            $this->translator->trans('First name'),
            $this->translator->trans('Last name'),
            $this->translator->trans('Email'),
            $this->translator->trans('Date of reply'),
        ];

        if (empty($answers[0]) || empty($answers[0]['answer']) || empty($answers[0]['answer']['plainData'])) {
            throw new \LogicException('An error in the evaluation data blocks the generation of the export');
        }

        $plainData = $this->getSortedPlainData($answers[0]['answer']['plainData']);

        $headerSurvey = [];
        foreach ($plainData as $data) {
            $headerSurvey[] = $data['title'];
        }

        $header = array_merge($header, $headerSurvey);

        return $header;
    }

    private function generateRows(ProviderSession $providerSession, array $extractedData): array
    {
        $rows = [];

        foreach ($extractedData as $data) {
            $userData = [
                $providerSession->getName(),
                $providerSession->getRef(),
                $data['firstName'],
                $data['lastName'],
                $data['email'],
                \DateTimeImmutable::createFromMutable($data['replyDate'])->setTimezone(new \DateTimeZone($providerSession->getClient()->getTimezone()))->format('Y-m-d H:i:s'),
            ];

            $sortedAnswer = $this->getSortedPlainData($data['answer']['plainData']);

            $answerData = [];
            foreach ($sortedAnswer as $answer) {
                $answerData[] = $answer['displayValue'];
            }

            $rows[] = array_merge($userData, $answerData);
        }

        return $rows;
    }

    private function getSortedPlainData(array $plainData): array
    {
        usort($plainData, function ($a, $b) {
            $indexA = str_replace('question', '', $a['name']);
            $indexB = str_replace('question', '', $b['name']);

            if ($indexA == $indexB) {
                return 0;
            }

            return ($indexA < $indexB) ? -1 : 1;
        });

        return $plainData;
    }
}
