<?php

namespace App\Service\Exporter\Spreadsheet;

use App\Entity\CategorySessionType;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\User;
use App\Service\Exporter\ExporterInterface;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\ProviderSessionExporterInterface;
use App\Service\Utils\DateUtils;
use App\Service\Utils\SpreadsheetUtils;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSpreadsheetAttendanceReportProviderSessionExporter implements ProviderSessionExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'attendance_report';
    public const EXPORT_FORMAT = ExportFormat::XLSX;
    public const SIGNATURE_TEMP_PATH = '/tmp_signatures/';

    public function __construct(
        private TranslatorInterface $translator,
        private SluggerInterface $slugger,
        private Security $security,
        private string $exportsSavefilesDirectory
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    abstract public function getSupportFormat(): string;

    public function createExportFile(ProviderSession $session): string
    {
        $writer = $this->getSpreadsheetWriter($session);
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFileName($session);

        $writer->save($path);

        $this->removeSignatureTempFolder();

        return $path;
    }

    public function createExportFileGrouping(array $sessions): string
    {
        $writer = $this->getSpreadsheetWriterGrouping($sessions);
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFileName($sessions[0]);
        $writer->save($path);

        $this->removeSignatureTempFolder();

        return $path;
    }

    public function createExportResponse(ProviderSession $session): Response
    {
        $writer = $this->getSpreadsheetWriter($session);
        $filename = $this->generateFileName($session);
        $writer->save('php://output');

        $response = new Response();
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $filename
        );
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Cache-Control', 'max-age=0');

        return $response;
    }

    private function generateFileName(ProviderSession $session): string
    {
        $extension = strtolower($this->getSupportFormat());

        $sessionDateStart = $session->getDateStart()
            ->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))
            ->format('Y-m-d_H\hi');

        return $this->slugger->slug(
                $sessionDateStart.'-presence-live-session-'.$session->getRef()
            ).".$extension";
    }

    private function generateSpreadsheet(ProviderSession $session): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setTitle($this->translator->trans('Attendance report'));

        $header = $this->getHeaders($session);
        $rows = array_merge($header, $this->generateRows($session, $header));

        for ($i = 1; $i < count($rows[0]); ++$i) {
            $spreadsheet->getActiveSheet()
                ->getColumnDimension(SpreadsheetUtils::numberToLetter($i))
                ->setAutoSize(true);
        }

        $rowNumber = 1;
        $rowNumber = $this->insertRowsDataIntoSpreadSheet($rows, $worksheet, $rowNumber, $spreadsheet);

        return $spreadsheet;
    }

    private function generateSpreadsheetGrouping(array $sessions): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setTitle($this->translator->trans('Grouped attendance reports'));

        $rowNumber = 1;

        $header = $this->getHeaders($sessions[0]);
        foreach ($sessions as $session) {
            if ($session->getRegistrationPageTemplate() && $session->getRegistrationPageTemplate()->getRegistrationForm()) {
                foreach ($session->getRegistrationPageTemplate()->getRegistrationForm() as $field) {
                    if (!in_array($field['question'], $header[0])) {
                        $header[0][] = $field['question'];
                    }
                }
            }
        }

        foreach ($sessions as $index => $session) {
            if ($index === 0) {
                $rows = array_merge($header, $this->generateRows($session, $header));
            } else {
                $rows = $this->generateRows($session, $header);
            }

            if (!empty($rows)) {
                for ($i = 1; $i < count($rows[0]); ++$i) {
                    $spreadsheet->getActiveSheet()
                        ->getColumnDimension(SpreadsheetUtils::numberToLetter($i))
                        ->setAutoSize(true);
                }
            }

            $rowNumber = $this->insertRowsDataIntoSpreadSheet($rows, $worksheet, $rowNumber, $spreadsheet);
        }

        return $spreadsheet;
    }

    private function generateXLSX(ProviderSession $session): Xlsx
    {
        return new Xlsx($this->generateSpreadsheet($session));
    }

    private function generateCSV(ProviderSession $session): Csv
    {
        return new Csv($this->generateSpreadsheet($session));
    }

    private function getSpreadsheetWriter(ProviderSession $session): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            ExportFormat::XLSX => $this->generateXLSX($session),
            ExportFormat::CSV => $this->generateCSV($session),
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function getSpreadsheetWriterGrouping(array $sessions): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            ExportFormat::XLSX => new Xlsx($this->generateSpreadsheetGrouping($sessions)),
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function getHeaders(ProviderSession $session): array
    {
        $this->translator->setLocale($session->getLanguage());

        $header = [
            $this->translator->trans('Title of the session'),
            $this->translator->trans('Session reference'),
            $this->translator->trans('Date and time of session'),
            $this->translator->trans('User reference'),
            $this->translator->trans('Email'),
            $this->translator->trans('First name'),
            $this->translator->trans('Last name'),
            $this->translator->trans('Company'),
            $this->translator->trans('Role'),
            $this->translator->trans('Status'),
            $this->translator->trans('Start'),
            $this->translator->trans('End'),
            $this->translator->trans('Duration'),
            $this->translator->trans('Minimum duration'),
        ];

        if ($session->getCategory() === CategorySessionType::CATEGORY_SESSION_MIXTE) {
            array_splice($header, 9, 0, $this->translator->trans('Attendance'));
        }

        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            array_splice($header, 6, 0, $this->translator->trans('Account'));
        }

        if ($session->getClient()->isEnableRemoteSignature()) {
            array_push($header, $this->translator->trans('Signatures'));
        }

        if ($session->getRegistrationPageTemplate() && $session->getRegistrationPageTemplate()->getRegistrationForm()) {
            foreach ($session->getRegistrationPageTemplate()->getRegistrationForm() as $field) {
                $header[] = $this->translator->trans($field['question']);
            }
        }

        if ($session->getSubject() && $session->getSubject()->getTheme()->getModule()->getRegistrationForm()) {
            foreach ($session->getSubject()->getTheme()->getModule()->getRegistrationForm() as $fieldCatalog) {
                $header[] = $this->translator->trans($fieldCatalog['question']);
            }
        }

        return [$header];
    }

    private function generateRows(ProviderSession $session, array $header): array
    {
        $commonHeader = [
            $session->getName(),
            $session->getRef(),
            $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i'),
        ];

        $rows = [];
        /** @var ProviderParticipantSessionRole $participantRole */
        foreach ($session->getParticipantRoles() as $participantRole) {
            $user = $participantRole->getParticipant()->getUser();

            $userData = array_merge(
                $commonHeader,
                [
                    $user->getReference(),
                    $user->getEmail(),
                    $user->getFirstName(),
                    $user->getLastName(),
                    $user->getCompany(),
                    ucfirst($this->translator->trans($participantRole->getRole())),
                ],
                $this->getPresenceData($participantRole)
            );

            if ($session->getCategory() === CategorySessionType::CATEGORY_SESSION_MIXTE) {
                array_splice(
                    $userData, 9, 0,
                    $participantRole->getSituationMixte() === ProviderParticipantSessionRole::SITUATION_MIXTE_DISTANTIAL
                        ? $this->translator->trans('remotely') : $this->translator->trans('face-to-face')
                );
            }

            if ($this->security->isGranted(User::ROLE_ADMIN)) {
                array_splice($userData, 6, 0, $user->getClient()->getName());
            }

            $customHeader = array_slice($header[0], count($userData));
            if ($participantRole->getRegistrationFormResponses()) {
                $registrationFormResponses = $participantRole->getRegistrationFormResponses();
                foreach ($customHeader as $question) {
                    $index = array_search($question, array_column($registrationFormResponses, 'question'));
                    if ($index !== false) {
                        $response = $registrationFormResponses[$index]['response'];
                        switch ($registrationFormResponses[$index]['type']) {
                            case 'date':
                                $response = (new \DateTime($response))->format('d/m/Y');
                                break;
                            default:
                                break;
                        }
                        $userData[] = $response;
                    } else {
                        $userData[] = null;
                    }
                }
            }
            if ($participantRole->getParticipant()->getUser()->getRegistrationFormCatalogResponses()) {
                $registrationFormResponses = $participantRole->getParticipant()->getUser()->getRegistrationFormCatalogResponses();
                foreach ($customHeader as $questionCatalog) {
                    $index = array_search($questionCatalog, array_column($registrationFormResponses, 'question'));
                    if ($index !== false) {
                        $response = $registrationFormResponses[$index]['response'];
                        switch ($registrationFormResponses[$index]['type']) {
                            case 'date':
                                $response = (new \DateTime($response))->format('d/m/Y');
                                break;
                            default:
                                break;
                        }
                        $userData[] = $response;
                    } else {
                        $userData[] = null;
                    }
                }
            }
            $rows[] = $userData;
        }

        return $rows;
    }

    private function getPresenceData(ProviderParticipantSessionRole $participantSessionRole): array
    {
        $presenceData = [
            $this->translator->trans($participantSessionRole->isPresent() ? 'Present' : 'Absent'),
        ];

        $session = $participantSessionRole->getSession();
        $timezone = new \DateTimeZone($session->getClient()->getTimezone());

        // Ajout des dates de présence si elles ne sont pas null, sinon laissez les champs vides
        if ($participantSessionRole->getPresenceDateStart() && $participantSessionRole->getPresenceDateEnd()) {
            $presenceData[] = $participantSessionRole->getPresenceDateStart()->setTimezone($timezone)->format('d/m/Y H:i');
            $presenceData[] = $participantSessionRole->getPresenceDateEnd()->setTimezone($timezone)->format('d/m/Y H:i');
        } else {
            $presenceData[] = null;
            $presenceData[] = null;
        }

        // Ajout de la durée de présence humanisée et du pourcentage de présence
        $slotPresenceDuration = $participantSessionRole->getSlotPresenceDuration();
        $duration = $slotPresenceDuration !== null ?
            DateUtils::humanizeDuration($slotPresenceDuration, empty($this->security->getUser()) ? $session->getLanguage() : $this->security->getUser()->getPreferredLang()) :
            '';
        $presenceData[] = $duration;
        $presenceData[] = $session->getPercentageAttendance().'%';

        // Gestion des signatures pour les clients activant la signature à distance
        if ($session->getClient()->isEnableRemoteSignature()) {
            $signatures = $participantSessionRole->getOrderedSignatures();

            foreach ($signatures as $item) {
                $presenceData[] = $item->getSignature();
            }
        }

        return $presenceData;
    }

    private function addSignatureImage(Worksheet $sheet, string $cell, string $image): void
    {
        if (preg_match('/^data:image\/(\w+);base64,/', $image, $type)) {
            $image = substr($image, strpos($image, ',') + 1);
            $type = strtolower($type[1]);
            $image = base64_decode($image);
        }

        if (!is_dir($this->exportsSavefilesDirectory.self::SIGNATURE_TEMP_PATH)) {
            mkdir($this->exportsSavefilesDirectory.self::SIGNATURE_TEMP_PATH, 0775, true);
        }

        $fileName = 'signature_'.rand().'.'.$type;
        $filePath = $this->exportsSavefilesDirectory.self::SIGNATURE_TEMP_PATH.$fileName;
        file_put_contents($filePath, $image);

        $drawing = new Drawing();
        $drawing->setName($fileName);
        $drawing->setDescription($fileName);
        $drawing->setCoordinates($cell);
        $drawing->setPath($filePath);
        $drawing->setWidth(110);
        $drawing->setWorksheet($sheet);
    }

    private function removeSignatureTempFolder(): void
    {
        $files = glob($this->exportsSavefilesDirectory.self::SIGNATURE_TEMP_PATH.'*');

        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        if (is_dir($this->exportsSavefilesDirectory.self::SIGNATURE_TEMP_PATH)) {
            rmdir($this->exportsSavefilesDirectory.self::SIGNATURE_TEMP_PATH);
        }
    }

    private function insertRowsDataIntoSpreadSheet(
        array $rows,
        Worksheet $worksheet,
        int $rowNumber,
        Spreadsheet $spreadsheet): int
    {
        foreach ($rows as $rowData) {
            $columnNumber = 1;
            foreach ($rowData as $cellData) {
                if (preg_match('/^data:image\/(\w+);base64,/', $cellData)) {
                    $this->addSignatureImage($worksheet, SpreadsheetUtils::numberToLetter($columnNumber).$rowNumber, $cellData);
                    $worksheet->getRowDimension($rowNumber)->setRowHeight(38);
                    $spreadsheet->getActiveSheet()
                        ->getColumnDimension(SpreadsheetUtils::numberToLetter($columnNumber))
                        ->setWidth(15);
                } else {
                    $worksheet->setCellValueByColumnAndRow($columnNumber, $rowNumber, $cellData);
                }
                ++$columnNumber;
            }
            ++$rowNumber;
        }

        return $rowNumber;
    }
}
