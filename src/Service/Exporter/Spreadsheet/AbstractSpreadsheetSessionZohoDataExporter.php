<?php

namespace App\Service\Exporter\Spreadsheet;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Cisco\WebexMeeting;
use App\Entity\Cisco\WebexRestMeeting;
use App\Entity\Microsoft\MicrosoftTeamsSession;
use App\Entity\ProviderSession;
use App\Entity\White\WhiteSession;
use App\Service\Exporter\ExporterInterface;
use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\ProviderSessionExporterInterface;
use App\Service\Utils\SpreadsheetUtils;
use Doctrine\Common\Collections\Collection;
use League\Flysystem\Filesystem;
use League\Flysystem\PhpseclibV3\SftpAdapter;
use League\Flysystem\PhpseclibV3\SftpConnectionProvider;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSpreadsheetSessionZohoDataExporter implements ProviderSessionExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'sessions_export_zoho';
    public const EXPORT_FORMAT = ExportFormat::XLSX;

    public function __construct(
        private TranslatorInterface $translator,
        private SluggerInterface $slugger,
        private string $exportSaveFilesZohoDirectory
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    abstract public function getSupportFormat(): string;

    public function createExportFile(ProviderSession $session): string
    {
        throw new \LogicException('Not yet implemented');
    }

    public function createExportFileGrouping(array $sessions): string
    {
        $writer = $this->getSpreadsheetWriterGrouping($sessions);
        $path = $this->exportSaveFilesZohoDirectory.'/'.$this->generateFileName();
        $writer->save($path);

        $filesystem = new Filesystem(new SftpAdapter(
            new SftpConnectionProvider(
                '91.207.255.151',
                'sftpzoho',
                'ls1115!!',
                null,
                null,
                10122,
                false,
                10,
                4,
                null,
                null
            ),
            '/home/sftpzoho/'
        ));

        $remotePath = basename($path);
        $filesystem->write($remotePath, file_get_contents($path));
        unlink($path);

        return $path;
    }

    public function createExportResponse(ProviderSession $session): Response
    {
        throw new \LogicException('Not yet implemented');
    }

    private function generateFileName(): string
    {
        $extension = strtolower($this->getSupportFormat());

        return $this->slugger->slug(
            'export-session-zoho-data'
            ).".$extension";
    }

    private function generateSpreadsheetGrouping(array $sessions): Spreadsheet
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '3600');

        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setTitle($this->translator->trans('Session Data'));

        $rowNumber = 1;

        foreach ($sessions as $index => $session) {
            if ($index === 0) {
                $rows = array_merge($this->getHeaders(), $this->generateRows($session));
            } else {
                $rows = $this->generateRows($session);
            }

            if (!empty($rows)) {
                for ($i = 1; $i < count($rows[0]); ++$i) {
                    $spreadsheet->getActiveSheet()
                        ->getColumnDimension(SpreadsheetUtils::numberToLetter($i))
                        ->setAutoSize(true);
                }
            }

            $rowNumber = $this->insertRowsDataIntoSpreadSheet($rows, $worksheet, $rowNumber);
        }

        return $spreadsheet;
    }

    private function getSpreadsheetWriterGrouping(array $sessions): BaseWriter
    {
        $exporterClass = get_class($this);

        return match ($this->getSupportFormat()) {
            ExportFormat::XLSX => new Xlsx($this->generateSpreadsheetGrouping($sessions)),
            ExportFormat::CSV => new Csv($this->generateSpreadsheetGrouping($sessions)),
            default => throw new \LogicException("Given format is not supported by $exporterClass exporter")
        };
    }

    private function getHeaders(): array
    {
        $header = [
            $this->translator->trans('Title of the session'),
            $this->translator->trans('Session reference'),
            $this->translator->trans('Account name'),
            //$this->translator->trans('Connector name'),
            $this->translator->trans('Sending emails'),
            $this->translator->trans('Business Number'),
            $this->translator->trans('Start date'),
            $this->translator->trans('End date'),
            $this->translator->trans('Duration').' (mn) ',
            $this->translator->trans('Status'),
            $this->translator->trans('Support'),
            $this->translator->trans('Support provided by'),
            $this->translator->trans('Assistance'),
            $this->translator->trans('Assisted by'),
            $this->translator->trans('Duration of assistance'),
            $this->translator->trans('Session creation'),
            $this->translator->trans('Update'),
            $this->translator->trans('Created by'),
            $this->translator->trans('Updated by'),
            $this->translator->trans('Sending the report'),
            $this->translator->trans('informations'),
            $this->translator->trans('informations'),
            $this->translator->trans('Evaluation'),
            $this->translator->trans('Evaluation flush'),
            $this->translator->trans('Category'),
            $this->translator->trans('Origin'),
            $this->translator->trans('Notify animators'),
            $this->translator->trans('Audio'),
            $this->translator->trans('Supplier'),
            $this->translator->trans('Attendees count'),
            $this->translator->trans('Number of animators'),
            $this->translator->trans('Convocations'),
            $this->translator->trans('Number of attendees'),
            $this->translator->trans('Number days'),
            $this->translator->trans('Lunch break'),
            $this->translator->trans('Access link to the virtual room'),
            $this->translator->trans('Room template'),
            $this->translator->trans('Testing session'),
            $this->translator->trans('Tags'),
        ];

        return [$header];
    }

    private function generateRows(ProviderSession $session): array
    {
        $rows[] = [
            $session->getName(),
            $session->getRef(),
            $session->getClient()->getName(),
            //$session->getAbstractProviderConfigurationHolder()->getName(),
            $session->isSendInvitationEmails(),
            $session->getBusinessNumber(),
            $session->getDateStart()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('Y-m-d H:i:s'),
            $session->getDateEnd()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('Y-m-d H:i:s'),
            $session->getDuration(),
            $this->translator->trans($session->getReadableState()),
            $session->getSupportType(),
            '', // Support provided by
            $session->getLegendAssistanceType(), //$session->getAssistanceType(),
            '', // Assisted by
            '', // Duration of assistance
            $session->getCreatedAt()->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('Y-m-d H:i:s'),
            $session->getUpdatedAt()?->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('Y-m-d H:i:s'),
            $session->getCreatedBy(),
            $session->getUpdatedBy(),
            '', // Sending the report
            $session->getInformation(),
            '', // Informations2
            $this->getTitleEvaluation($session->getEvaluations()),
            '', // Evaluation flush
            $session->getCategory()->label(),
            $session->getOrigin(),
            $session->isEnableSendRemindersToAnimators(),
            $session->getProviderAudio()?->getName(),
            $session->getProviderAudio()?->getConnectionType(),
            count($session->getTraineesOnly()),
            count($session->getAnimatorsOnly()),
            $session->getConvocation()?->getName(),
            $this->getNombresOfPresence($session->getParticipantRoles()),
            $session->getNbrdays(),
            $session->getRestlunch(),
            $this->getUrlPathSession($session),
            $this->getRoomModelSession($session),
            $this->translator->trans($this->isTestingSession($session)),
            implode(' ; ', $session->getTags() ?? []),
        ];

        return $rows;
    }

    private function insertRowsDataIntoSpreadSheet(
        array $rows,
        Worksheet $worksheet,
        int $rowNumber): int
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '3600');

        foreach ($rows as $rowData) {
            $columnNumber = 1;
            foreach ($rowData as $cellData) {
                $worksheet->setCellValueByColumnAndRow($columnNumber, $rowNumber, $cellData);
                ++$columnNumber;
            }
            ++$rowNumber;
        }

        return $rowNumber;
    }

    private function getTitleEvaluation(Collection $Evaluations): string
    {
        if ($Evaluations->getValues() != []) {
            return $Evaluations->getValues()[0]->getTitle();
        }

        return '';
    }

    private function getNombresOfPresence(Collection $participants): int
    {
        $total = 0;
        foreach ($participants->getValues() as $participant) {
            if ($participant->getPresenceStatus()) {
                ++$total;
            }
        }

        return $total;
    }

    private function getUrlPathSession(ProviderSession $session): ?string
    {
        if ($session instanceof AdobeConnectSCO) {
            return $session->getFullUrlpath();
        } elseif ($session instanceof WebexMeeting) {
            return $session->getMeetingLink();
        } elseif ($session instanceof WebexRestMeeting) {
            return $session->getMeetingRestWebLink();
        } elseif ($session instanceof MicrosoftTeamsSession) {
            return $session->getJoinWebUrl();
        } elseif ($session instanceof WhiteSession) {
            return $session->getJoinSessionWebUrl();
        }

        return null;
    }

    private function getRoomModelSession(ProviderSession $session): ?int
    {
        if ($session instanceof AdobeConnectSCO) {
            return $session->getRoomModel();
        }

        return null;
    }

    private function isTestingSession(ProviderSession $session): string
    {
        if ($session->isSessionTest()) {
            return 'true';
        }

        return 'false';
    }
}
