<?php

namespace App\Service\Exporter\Spreadsheet\Xlsx;

use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetEvaluationsReportProviderSessionsExporter;

class XslxSpreadsheetEvaluationsReportProviderSessionsExporter extends AbstractSpreadsheetEvaluationsReportProviderSessionsExporter
{
    public const EXPORT_FORMAT = ExportFormat::XLSX;

    public function getSupportFormat(): string
    {
        return self::EXPORT_FORMAT;
    }
}
