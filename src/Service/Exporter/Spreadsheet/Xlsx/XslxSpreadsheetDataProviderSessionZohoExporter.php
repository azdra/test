<?php

namespace App\Service\Exporter\Spreadsheet\Xlsx;

use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetSessionZohoDataExporter;

class XslxSpreadsheetDataProviderSessionZohoExporter extends AbstractSpreadsheetSessionZohoDataExporter
{
    public const EXPORT_FORMAT = ExportFormat::XLSX;

    public function getSupportFormat(): string
    {
        return self::EXPORT_FORMAT;
    }
}
