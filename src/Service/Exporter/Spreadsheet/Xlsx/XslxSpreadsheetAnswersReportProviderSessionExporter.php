<?php

namespace App\Service\Exporter\Spreadsheet\Xlsx;

use App\Service\Exporter\ExportFormat;
use App\Service\Exporter\Spreadsheet\AbstractSpreadsheetAnswersReportProviderSessionExporter;

class XslxSpreadsheetAnswersReportProviderSessionExporter extends AbstractSpreadsheetAnswersReportProviderSessionExporter
{
    public const EXPORT_FORMAT = ExportFormat::XLSX;

    public function getSupportFormat(): string
    {
        return self::EXPORT_FORMAT;
    }
}
