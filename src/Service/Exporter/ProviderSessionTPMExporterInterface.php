<?php

namespace App\Service\Exporter;

use App\Entity\ProviderSession;
use Symfony\Component\HttpFoundation\Response;

interface ProviderSessionTPMExporterInterface
{
    public function createExportFile(ProviderSession $session): string;

    public function createExportFileGrouping(array $sessions): string;

    public function createExportResponse(ProviderSession $session): Response;
}
