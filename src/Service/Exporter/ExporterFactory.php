<?php

namespace App\Service\Exporter;

use App\Exception\Exporter\ExporterNotFoundException;

class ExporterFactory
{
    public function __construct(
        private iterable $exporters
    ) {
    }

    /**
     * @throws ExporterNotFoundException
     */
    public function getExporter(string $type, string $format): ExporterInterface
    {
        /** @var ExporterInterface $exporter */
        foreach ($this->exporters as $exporter) {
            if ($type === $exporter->getSupportType() && $format === $exporter->getSupportFormat()) {
                return $exporter;
            }
        }

        throw new ExporterNotFoundException('Exporter not found');
    }
}
