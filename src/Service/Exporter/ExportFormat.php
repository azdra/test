<?php

namespace App\Service\Exporter;

class ExportFormat
{
    public const PDF = 'PDF';
    public const XLSX = 'XLSX';
    public const CSV = 'CSV';
}
