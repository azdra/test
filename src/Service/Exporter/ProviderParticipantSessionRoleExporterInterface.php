<?php

namespace App\Service\Exporter;

use App\Entity\ProviderParticipantSessionRole;
use Symfony\Component\HttpFoundation\Response;

interface ProviderParticipantSessionRoleExporterInterface
{
    public function createExportFile(ProviderParticipantSessionRole $participantSessionRole): string;

    public function createExportFileGrouping(array $participantSessionRoles): string;

    public function createExportResponse(ProviderParticipantSessionRole $participantSessionRole): Response;
}
