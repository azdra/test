<?php

namespace App\Service\Exporter;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;

interface UserExporterInterface
{
    public function createExportFile(User $user): string;

    public function createExportFileGrouping(array $users): string;

    public function createExportResponse(User $user): Response;
}
