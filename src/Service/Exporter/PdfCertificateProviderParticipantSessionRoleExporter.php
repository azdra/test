<?php

namespace App\Service\Exporter;

use App\Entity\ProviderParticipantSessionRole;
use Knp\Snappy\Pdf;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\WebpackEncoreBundle\Asset\EntrypointLookupCollectionInterface;
use Twig\Environment;

class PdfCertificateProviderParticipantSessionRoleExporter implements ProviderParticipantSessionRoleExporterInterface, ExporterInterface
{
    public const EXPORT_TYPE = 'certificate_report';
    public const EXPORT_FORMAT = ExportFormat::PDF;

    public function __construct(
        private Pdf $pdf,
        private Environment $twig,
        private SluggerInterface $slugger,
        private string $exportsSavefilesDirectory,
        private EntrypointLookupCollectionInterface $entrypointLookups
    ) {
    }

    public function getSupportType(): string
    {
        return self::EXPORT_TYPE;
    }

    public function getSupportFormat(): string
    {
        return self::EXPORT_FORMAT;
    }

    public function createExportFile(ProviderParticipantSessionRole $participantSessionRole): string
    {
        $path = $this->exportsSavefilesDirectory.'/'.$this->generateFilename($participantSessionRole);
        $html = $this->generateContent($participantSessionRole);
        $pdfOptions = $this->getPDFOptions($participantSessionRole);

        $this->pdf->generateFromHtml($html, $path, $pdfOptions, true);
        $this->entrypointLookups->getEntrypointLookup()->reset();

        return $path;
    }

    public function createExportFileGrouping(array $participantSessionRoles): string
    {
        throw new \LogicException('Not yet implemented');
    }

    public function createExportResponse(ProviderParticipantSessionRole $participantSessionRole): Response
    {
        return new BinaryFileResponse($this->createExportFile($participantSessionRole));
    }

    private function getPDFOptions(ProviderParticipantSessionRole $participantSessionRole): array
    {
        $session = $participantSessionRole->getSession();
        $client = $session->getClient();

        return [
            'header-html' => $this->twig->render('exporter/client_header.html.twig', ['client' => $client]),
            'footer-html' => $this->twig->render('exporter/client_footer.html.twig', ['client' => $client, 'session' => $session]),
        ];
    }

    private function generateFilename(ProviderParticipantSessionRole $participantSessionRole): string
    {
        $session = $participantSessionRole->getSession();
        $participantFullName = $participantSessionRole->getParticipant()->getFullName();
        $sessionDateStart = ($session->getDateStart())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('Y-m-d_H\hi');
        $sessionRef = $session->getRef();
        $extension = strtolower($this->getSupportFormat());

        return $this->slugger->slug("$sessionDateStart-certificate-$sessionRef-$participantFullName").".$extension";
    }

    private function generateContent(ProviderParticipantSessionRole $participantSessionRole): string
    {
        $session = $participantSessionRole->getSession();

        return $this->twig->render('exporter/certificate_report.html.twig', [
            'client' => $session->getClient(),
            'session' => $session,
            'dateStartSession' => ($session->getDateStart())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i'),
            'dateEndSession' => ($session->getDateEnd())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('d/m/Y H:i'),
            'presenceDateStart' => ($participantSessionRole->getPresenceDateStart())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('H:i'),
            'presenceDateEnd' => ($participantSessionRole->getPresenceDateEnd())->setTimezone(new \DateTimeZone($session->getClient()->getTimezone()))->format('H:i'),
            'participantSessionRole' => $participantSessionRole,
        ]);
    }
}
