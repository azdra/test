<?php

namespace App\Service\Exporter;

interface ExporterInterface
{
    public function getSupportType(): string;

    public function getSupportFormat(): string;
}
