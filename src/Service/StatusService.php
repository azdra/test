<?php

namespace App\Service;

use App\Repository\ClientRepository;
use App\Repository\ProviderSessionRepository;
use Psr\Log\LoggerInterface;

class StatusService
{
    public function __construct(
        private \Redis $redisClient,
        private ProviderSessionRepository $sessionRepository,
        private ClientRepository $clientRepository,
        private LoggerInterface $logger
    ) {
    }

    public function redisStatus(): int
    {
        try {
            $this->redisClient->set('check-status', 1);
            $this->redisClient->del('check-status');

            return 1;
        } catch (\Throwable $t) {
            $this->logger->error($t);

            return 0;
        }
    }

    public function bddStatus(): int
    {
        try {
            $client = $this->clientRepository->findFirstForStatus();
            $session = $this->sessionRepository->findFirstForStatus();

            return 1;
        } catch (\Throwable $t) {
            $this->logger->error($t);

            return 0;
        }
    }
}
