<?php

namespace App\RegistrationPage\Serializer;

use App\RegistrationPage\Entity\RegistrationPageTemplate;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class RegistrationPageNormalizer implements NormalizerInterface
{
    public function __construct(protected ObjectNormalizer $normalizer, private string $registrationPageTemplateLogoUri)
    {
    }

    public function normalize($object, string $format = null, array $context = []): ?array
    {
        $data = $this->normalizer->normalize($object, $format, $context);
        $data = is_array($data) ? $data : $data->getArrayCopy();

        $blocks = [
            'blockGraphicalCharter' => ['banner', 'illustration', 'logo'],
            'blockLowerBanner' => ['privacyFile', 'legalNoticeFile'],
            'blockUpperBanner' => ['needHelpIcon'],
        ];

        foreach ($blocks as $blockKey => $blockValue) {
            foreach ($blockValue as $key => $value) {
                $data[$blockKey][sprintf('%sUri', $value)] = (!empty($data[$blockKey][$value])) ? $this->registrationPageTemplateLogoUri.$data[$blockKey][$value] : null;
            }
        }

        return $data;
    }

    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof RegistrationPageTemplate;
    }

    public function getSupportedTypes(?string $format = null): array
    {
        return [
            RegistrationPageTemplate::class => true,
        ];
    }
}
