<?php

namespace App\RegistrationPage\Controller;

use App\Entity\CategorySessionType;
use App\Entity\ProviderParticipantSessionRole;
use App\Entity\ProviderSession;
use App\Entity\ProviderSessionReplay;
use App\Entity\User;
use App\Event\TransactionalEmailRequestEventInterface;
use App\Event\TransactionalParticipantEmailRequestEvent;
use App\Exception\UploadFile\UploadFileException;
use App\Model\ApiResponseTrait;
use App\RegistrationPage\Entity\RegistrationPageTemplate;
use App\RegistrationPage\Service\RegistrationPageService;
use App\RegistrationPage\Service\RegistrationPageTemplateBlackAndWhiteListImporterService;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionReplayRepository;
use App\Repository\UserRepository;
use App\Service\ActivityLogger;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\ProviderSessionReplayService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/weblink/{client}/{session}')]
#[ParamConverter('session', converter: 'register_page_param_converter')]
class RegisterController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private ActivityLogger $activityLogger,
        private LoggerInterface $logger,
        private EntityManagerInterface $entityManager,
        private EventDispatcherInterface $eventDispatcher,
        private ParticipantSessionSubscriber $participantSessionSubscriber,
        private ProviderSessionReplayService $providerParticipantService,
        private SerializerInterface $serializer,
        private RegistrationPageService $registrationPageService,
        private RegistrationPageTemplateBlackAndWhiteListImporterService $blackAndWhiteListImporter,
        protected TranslatorInterface $translator,
    ) {
    }

    #[Route('/', name: '_registration_page_weblink_home', options: ['expose' => true], methods: ['GET'])]
    public function HomeWebLinkRegistrationPage(Request $request, ProviderSession $session, string $registrationPageTemplateLogoUri): Response
    {
        $request->setLocale($session->getLanguage());

        return $this->render('registration-page/page/base.html.twig', [
            'template' => $session->getRegistrationPageTemplate(),
            'session' => $session,
            'isPreview' => false,
            'registrationPageTemplateLogoUri' => $registrationPageTemplateLogoUri,
        ]);
    }

    #[Route('/new-subscription', name: '_subscription_new_participant', options: ['expose' => true], methods: ['POST'])]
    public function subscriptionParticipant(Request $request, ProviderSession $session, UserRepository $userRepository): Response
    {
        if ($session->getProviderSessionRegistrationPage()->getPlaces() !== null && $session->getRegistrationPagePlaceAvailable() <= 0) {
            return $this->apiErrorResponse('No more place available');
        }

        if ($session->isFinished()) {
            return $this->apiErrorResponse('Session is finished');
        }

        if (!$session->getRegistrationPageTemplate()->isEmailAllowed($request->query->get('email'))) {
            return $this->apiErrorResponse('Not authorized to registration');
        }

        try {
            $data = $request->query->all();
            $user = $userRepository->findOneBy(['email' => $request->query->get('email'), 'client' => $session->getClient()]);

            if ($user === null) {
                $user = new User();
                $user->setEmail($request->query->get('email'))
                    ->setLastName($request->query->get('lastname'))
                    ->setFirstName($request->query->get('firstname'))
                    ->setClearPassword(uniqid())
                    ->setClient($session->getClient())
                    ->setStatus(User::STATUS_ACTIVE)
                    ->setPreferredLang($session->getClient()->getDefaultLang());

                $this->entityManager->persist($user);
                $this->entityManager->flush();
            }

            $ProviderParticipantSessionRole = $this->participantSessionSubscriber->subscribe(
                $session,
                $user,
                ProviderParticipantSessionRole::ROLE_TRAINEE
            );

            if (array_key_exists('formAnswer', $data)) {
                $ProviderParticipantSessionRole->setRegistrationFormResponses($data['formAnswer']);
                $this->entityManager->persist($ProviderParticipantSessionRole);
            }
            $this->entityManager->flush();

            return $this->apiResponse(['message' => 'Successful registration',
                'currentUser' => [
                    'id' => $ProviderParticipantSessionRole->getId(),
                    'token' => $ProviderParticipantSessionRole->getProviderSessionAccessToken()->getToken(),
                ],
            ]);
        } catch (\Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/subscription-replay', name: '_subscription_replay', options: ['expose' => true], methods: ['POST'])]
    public function subscriptionReplay(Request $request, ProviderSession $session, ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository, ProviderSessionReplayRepository $providerSessionReplayRepository): Response
    {
        if (!$session->isFinished()) {
            return $this->apiErrorResponse('Session is not finished');
        }

        $userPPSR = $providerParticipantSessionRoleRepository->getParticipantWithSessionAndEmail($session, $request->query->get('email'));
        /** @var ProviderSessionReplay $providerSessionReplay */
        $providerSessionReplay = $providerSessionReplayRepository->findOneBy(['email' => $request->query->get('email'), 'session' => $session]);
        $params = [
            'session' => $session,
            'firstname' => $userPPSR?->getFirstName() ?? $request->query->get('firstname'),
            'lastname' => $userPPSR?->getLastName() ?? $request->query->get('lastname'),
            'email' => $userPPSR?->getEmail() ?? $request->query->get('email'),
            'registrationFormResponses' => $userPPSR?->getRegistrationFormResponses() ?? [],
        ];
        if (array_key_exists('formAnswer', $request->query->all())) {
            $params['registrationFormResponses'] = $userPPSR?->getRegistrationFormResponses() ?? $request->query->all()['formAnswer'];
        }
        $this->providerParticipantService->generateSessionReplay($providerSessionReplay, $params);

        return $this->apiResponse([
            'replayLink' => $this->generateUrl('_weblink_replay', [
                'client' => $session->getClient()->getSlug(),
                'session' => $session->getId(),
                'token' => $providerSessionReplay->getProviderReplayAccessToken()->getToken(),
            ], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);
    }

    #[Route('/check-subscription', name: '_check_participant_inscrit', options: ['expose' => true, 'stateless' => true], methods: ['GET'])]
    public function checkParticipantInscrit(Request $request, ProviderSession $session): Response
    {
        try {
            return $this->apiResponse($this->registrationPageService->checkParticipantRegistered($session, $request->query->get('email')));
        } catch (\Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/communication/resend', name: '_resend_communication', options: ['expose' => true], methods: ['POST'])]
    public function resendCommunication(Request $request, ProviderSession $session, ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository): Response
    {
        $participantSessionRole = $providerParticipantSessionRoleRepository->findOneBy(['id' => $request->query->get('currentUserId')]);
        try {
            if ($participantSessionRole->getSession()->getCategory() == CategorySessionType::CATEGORY_SESSION_WEBINAR) {
                $this->eventDispatcher->dispatch(
                    new TransactionalParticipantEmailRequestEvent($participantSessionRole, $participantSessionRole->getParticipant()->getUser(), true),
                    TransactionalEmailRequestEventInterface::COMMUNICATION_ONE_PARTICIPANT
                );
            }
            $this->activityLogger->flushActivityLogs();

            return $this->apiResponse(['message' => 'Successful registration']);
        } catch (\Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/replay/{token}', name: '_weblink_replay', options: ['expose' => true], methods: ['GET'])]
    public function replay(Request $request, ProviderSession $session, string $token, ProviderSessionReplayRepository $providerSessionReplayRepository, string $registrationPageTemplateLogoUri): Response
    {
        if (!$session->isFinished()) {
            throw $this->createNotFoundException('Session is not finished');
        }

        if (!$session->isPublishedReplay()) {
            throw $this->createNotFoundException('Session replay is not published');
        }

        /** @var ProviderSessionReplay $providerSessionReplay */
        $providerSessionReplay = $providerSessionReplayRepository->findOneBy(['providerReplayAccessToken.token' => $token, 'session' => $session]);

        if (!$providerSessionReplay) {
            throw $this->createAccessDeniedException();
        }

        if ($providerSessionReplay->getLastShowReplay() < new \DateTimeImmutable('-30 seconds')) {
            $providerSessionReplay->incrementSeen();
            $providerSessionReplay->setLastShowReplay(new \DateTimeImmutable());
            $this->entityManager->persist($providerSessionReplay);
            $this->entityManager->flush();
        }

        return $this->render('registration-page/page/base.html.twig', [
            'template' => $session->getRegistrationPageTemplate(),
            'session' => $session,
            'isPreview' => false,
            'isReplay' => true,
            'registrationPageTemplateLogoUri' => $registrationPageTemplateLogoUri,
        ]);
    }

    #[Route('/faqs', name: '_registration_page_weblink_faqs', options: ['expose' => true], methods: ['GET'])]
    public function webLinkRegistrationPageFaqs(ProviderSession $session, string $registrationPageTemplateLogoUri): Response
    {
        return $this->render('registration-page/page/faq.html.twig', [
            'session' => $session,
            'template' => $session->getRegistrationPageTemplate(),
            'registrationPageTemplateLogoUri' => $registrationPageTemplateLogoUri,
            'isFaqs' => true,
        ]);
    }

    #[Route('/access-replay/{token}', name: '_weblink_access_replay', options: ['expose' => true], methods: ['GET'])]
    public function accessReplay(Request $request, ProviderSession $session, string $token, ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository, RegistrationPageService $registrationPageService): RedirectResponse|JsonResponse
    {
        try {
            $ppsr = $providerParticipantSessionRoleRepository->findOneBy(['providerSessionAccessToken.token' => $token, 'session' => $session]);
            $data = $registrationPageService->checkParticipantRegistered($session, $ppsr->getEmail());

            return $this->redirectToRoute('_weblink_replay', [
                'client' => $session->getClient()->getSlug(),
                'session' => $session->getId(),
                'token' => $data['currentUser']['token'],
            ]);
        } catch (\Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/import-file-mail-template/{id}', name: '_template_white_black_list_import_file', options: ['expose' => true], methods: ['POST'])]
    public function importFileMailModule(Request $request, RegistrationPageTemplate $template): Response
    {
        try {
            $this->blackAndWhiteListImporter->importMailsForWhiteAndBackList($request, $template);
            $this->entityManager->flush();

            return new Response($this->serializer->serialize(
                $template,
                'json',
                ['groups' => 'ui:registration_page_template:read']
            ));
        } catch (UploadFileException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($this->translator->trans('No import file'));
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($this->translator->trans('The file can not be imported for an unknown error'));
        }
    }
}
