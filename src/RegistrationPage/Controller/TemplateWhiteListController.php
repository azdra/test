<?php

namespace App\RegistrationPage\Controller;

use App\Model\ApiResponseTrait;
use App\RegistrationPage\Entity\RegistrationPageTemplate;
use App\RegistrationPage\Entity\RegistrationPageTemplateWhiteListEntry;
use App\RegistrationPage\Security\RegistrationPageTemplateVoter;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

#[Route('/registration-page/template/{id}/whitelist')]
#[IsGranted(RegistrationPageTemplateVoter::EDIT, subject: 'registrationPageTemplate')]
class TemplateWhiteListController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger,
        private SerializerInterface $serializer,
        private TranslatorInterface $translator
    ) {
    }

    #[Route('/add', name: '_new_allowed_template_white_list', options: ['expose' => true], methods: ['POST'])]
    public function addNewAllowedInTemplateWhiteList(Request $request, RegistrationPageTemplate $registrationPageTemplate): Response
    {
        try {
            if (!$request->query->has('allowedMail')) {
                return $this->apiErrorResponse($this->translator->trans('The field of the email form cannot be empty'));
            }
            $registrationPageTemplate->addWhiteListEntry($request->query->get('allowedMail'));
            $this->entityManager->flush();

            return new Response($this->serializer->serialize(
                $registrationPageTemplate,
                'json',
                ['groups' => 'ui:registration_page_template:read']
            ));
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/{whitelistId}/remove', name: '_delete_allowed_template_white_list', options: ['expose' => true], methods: ['POST'])]
    #[ParamConverter('whiteList', options: ['mapping' => ['whitelistId' => 'id']])]
    public function deleteAllowedInWhiteList(RegistrationPageTemplate $registrationPageTemplate, RegistrationPageTemplateWhiteListEntry $whiteList): Response
    {
        try {
            if ($registrationPageTemplate !== $whiteList->getRegistrationPageTemplate()) {
                $this->logger->error(new \Exception('WhiteList does not belongs to given Registration Page Template, maybe someone is playing with url'));

                return $this->apiErrorResponse('An unknown error occurred during delete a allowed in white List');
            }
            $registrationPageTemplate->removeWhiteListEntry($whiteList);

            $this->entityManager->remove($whiteList);
            $this->entityManager->flush();

            return new Response($this->serializer->serialize(
                $registrationPageTemplate,
                'json',
                ['groups' => 'ui:registration_page_template:read']
            ));
        } catch (Throwable $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }
}
