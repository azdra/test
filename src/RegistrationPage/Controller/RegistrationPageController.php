<?php

namespace App\RegistrationPage\Controller;

use App\Entity\Adobe\AdobeConnectSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectConfigurationHolder;
use App\Entity\ProviderSession;
use App\Entity\ProviderSessionRegistrationPage;
use App\Entity\ProviderSessionSpeaker;
use App\Exception\UploadFile\UploadFileException;
use App\Model\ApiResponseTrait;
use App\RegistrationPage\Entity\RegistrationPageTemplate;
use App\RegistrationPage\Entity\RegistrationPageTemplateFAQ;
use App\RegistrationPage\Repository\RegistrationPageTemplateFAQRepository;
use App\RegistrationPage\Repository\RegistrationPageTemplateRepository;
use App\RegistrationPage\Security\RegistrationPageTemplateVoter;
use App\Repository\ClientRepository;
use App\Repository\ProviderSessionRepository;
use App\Service\ActivityLogger;
use App\Service\ImportSessionService;
use App\Service\Provider\Common\ParticipantSessionSubscriber;
use App\Service\RessourceUploader\RessourceUploader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/registration-page/template')]
class RegistrationPageController extends AbstractController
{
    use ApiResponseTrait;

    public function __construct(
        private ClientRepository $clientRepository,
        private ProviderSessionRepository $sessionRepository,
        private RegistrationPageTemplateRepository $registrationPageTemplateRepository,
        private TranslatorInterface $translator,
        private ActivityLogger $activityLogger,
        private LoggerInterface $logger,
        private NormalizerInterface $normalizer,
        private DenormalizerInterface $denormalizer,
        private RessourceUploader $ressourceUploader,
        private ValidatorInterface $validator,
        private EntityManagerInterface $entityManager,
        private EventDispatcherInterface $eventDispatcher,
        private ParticipantSessionSubscriber $participantSessionSubscriber,
        private $registrationPageTemplateLogoSavefileDirectory,
        private SerializerInterface $serializer,
    ) {
    }

    #[Route('/', name: '_registration_page_index', options: ['expose' => true], methods: ['GET'])]
    public function registrationPageIndex(): Response
    {
        $listRegistrationPageTemplates = $this->isGranted('ROLE_ADMIN') ?
            $this->registrationPageTemplateRepository->findAll() :
            $this->registrationPageTemplateRepository->findBy(['client' => $this->getUser()->getClient()]);

        return $this->render('registration-page/index.html.twig', [
            'title' => 'Registration pages',
            'client' => $this->getUser()->getClient(),
            'listRegistrationPageTemplates' => $listRegistrationPageTemplates,
            'clients' => $this->clientRepository->findAll(),
        ]);
    }

    #[Route('/edit/{id}', name: '_registration_page_template_edit', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(RegistrationPageTemplateVoter::EDIT, subject: 'registrationPageTemplate')]
    public function editTemplate(RegistrationPageTemplate $registrationPageTemplate): Response
    {
        $importRestrictions = [
            'max_file_size' => RessourceUploader::humanReadableFileSize(ImportSessionService::MAX_FILE_SIZE),
            'allowed_extension' => implode(', ', ImportSessionService::AUTHORIZED_EXTENSION),
            'allowed_max_row' => ImportSessionService::ALLOWED_MAX_ROW,
        ];

        return $this->render('registration-page/template/edit.html.twig', [
            'title' => $registrationPageTemplate->getName(),
            'registrationPageTemplate' => $registrationPageTemplate,
            'importRestrictions' => $importRestrictions,
        ]);
    }

    #[Route('/delete/{id}', name: '_template_delete', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(RegistrationPageTemplateVoter::EDIT, subject: 'registrationPageTemplate')]
    public function deleteTemplate(RegistrationPageTemplate $registrationPageTemplate): Response
    {
        try {
            $this->entityManager->remove($registrationPageTemplate);
            $this->entityManager->flush();
            $this->addFlash('success', 'The template page has been deleted.');
        } catch (\Throwable $throwable) {
            $this->logger->error($throwable->getMessage());
            $this->addFlash('danger', 'An error occurred while deleting the template page.');

            return $this->apiErrorResponse('An error occurred while deleting the template page.', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->apiResponse('The template has been deleted');
    }

    #[Route('/save', name: '_registration_page_template_save', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function saveTemplate(Request $request): Response
    {
        $request = json_decode($request->getContent(), true);

        $context = [
            AbstractNormalizer::GROUPS => 'ui:registration_page_template:update',
            AbstractNormalizer::DEFAULT_CONSTRUCTOR_ARGUMENTS => [
                RegistrationPageTemplate::class => ['client' => $this->getUser()->getClient()],
            ],
        ];
        if (array_key_exists('id', $request)) {
            /** @var RegistrationPageTemplate $template */
            $template = $this->registrationPageTemplateRepository->find($request['id']);
            /*if (empty($template) || !$this->isGranted(RegistrationPageTemplateVoter::EDIT, $template)) {
                throw new AccessDeniedException();
            }*/
            if (!empty($template->getBlockGraphicalCharter()->getLogo()) && empty($request['blockGraphicalCharter']['logo'])) {
                unlink($this->registrationPageTemplateLogoSavefileDirectory.'/'.$template->getBlockGraphicalCharter()->getLogo());
            }
            if (!empty($template->getBlockGraphicalCharter()->getBanner()) && empty($request['blockGraphicalCharter']['banner'])) {
                unlink($this->registrationPageTemplateLogoSavefileDirectory.'/'.$template->getBlockGraphicalCharter()->getBanner());
            }
            if (!empty($template->getBlockGraphicalCharter()->getIllustration()) && empty($request['blockGraphicalCharter']['illustration'])) {
                unlink($this->registrationPageTemplateLogoSavefileDirectory.'/'.$template->getBlockGraphicalCharter()->getIllustration());
            }
            if (!empty($template->getBlockLowerBanner()->getPrivacyFile()) && empty($request['blockLowerBanner']['privacyFile'])) {
                unlink($this->registrationPageTemplateLogoSavefileDirectory.'/'.$template->getBlockLowerBanner()->getPrivacyFile());
            }
            if (!empty($template->getBlockLowerBanner()->getLegalNoticeFile()) && empty($request['blockLowerBanner']['legalNoticeFile'])) {
                unlink($this->registrationPageTemplateLogoSavefileDirectory.'/'.$template->getBlockLowerBanner()->getLegalNoticeFile());
            }
            if (!empty($template->getBlockUpperBanner()->getNeedHelpIcon()) && empty($request['blockUpperBanner']['needHelpIcon'])) {
                unlink($this->registrationPageTemplateLogoSavefileDirectory.'/'.$template->getBlockUpperBanner()->getNeedHelpIcon());
            }

            $context[AbstractNormalizer::OBJECT_TO_POPULATE] = $template;
            $this->denormalizer->denormalize($request, RegistrationPageTemplate::class, context: $context);
        } else {
            /** @var RegistrationPageTemplate $template */
            $template = $this->denormalizer->denormalize($request, RegistrationPageTemplate::class, context: $context);

            if (
                $this->isGranted('ROLE_ADMIN')
                && array_key_exists('client', $request)
                && array_key_exists('id', $request['client'])
                && $this->getUser()->getClient()->getId() !== $request['client']['id']
            ) {
                $client = $this->clientRepository->find($request['client']['id']);
                if ($client !== null) {
                    $template->setClient($client);
                }
            }
        }
        $violations = $this->validator->validate($template);
        if ($violations->count() === 0) {
            $this->entityManager->persist($template);
            $this->entityManager->flush();

            return $this->apiResponse($this->normalizer->normalize([$template], context: ['groups' => ['ui:registration_page_template:read']]));
        } else {
            $extraDatas = [];
            foreach ($violations as $violation) {
                $extraDatas[$violation->getPropertyPath()] = $violation->getMessage();
            }

            return $this->apiErrorResponse($this->translator->trans('Check your data'), extraData: $extraDatas);
        }
    }

    #[Route('/save-form/{id}', name: '_registration_page_form_save', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted(RegistrationPageTemplateVoter::EDIT, subject: 'registrationPageTemplate')]
    public function saveForm(Request $request, RegistrationPageTemplate $registrationPageTemplate): Response
    {
        try {
            $dataForm = $request->request->all();
            $registrationPageTemplate->setRegistrationForm($dataForm);
            $this->entityManager->persist($registrationPageTemplate);
            $this->entityManager->flush();

            return $this->apiResponse('The form for Registration Page Template has been saved');
        } catch (\Throwable $throwable) {
            $this->logger->error($throwable);

            return $this->apiErrorResponse('A erreur has been occurred when saving the form for Registration Page Template => '.$throwable->getMessage());
        }
    }

    #[Route('/upload/files', name: '_registration_page_template_upload_files', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted('ROLE_MANAGER')]
    public function uploadFile(Request $request, string $registrationPageTemplateLogoUri): Response
    {
        try {
            /** @var UploadedFile $file */
            $file = $request->files->get('file');

            $formatParam = $request->request->get('format', implode(',', RegistrationPageTemplate::AUTHORIZED_EXTENSIONS));
            $this->ressourceUploader->isFileUploadedValid($file, explode(',', $formatParam), RegistrationPageTemplate::MAX_FILE_SIZE);

            $filename = \md5(\random_bytes(32)).'.'.$file->getClientOriginalExtension();

            (new Filesystem())->copy($file->getRealPath(), $this->registrationPageTemplateLogoSavefileDirectory.'/'.$filename, true);

            return $this->apiResponse([
                'filetarget' => $filename,
                'previewUri' => $registrationPageTemplateLogoUri.'/'.$filename,
            ]);
        } catch (UploadFileException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/preview/{id}', name: '_registration_page_template_preview', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(RegistrationPageTemplateVoter::EDIT, subject: 'template')]
    public function HomeWebLinkRegistrationPage(RegistrationPageTemplate $template, string $registrationPageTemplateLogoUri): Response
    {
        $speakers = [
            (new ProviderSessionSpeaker())
                ->setName('Nom de l\'intervenant')
                ->setTitle('Intitulé du poste')
                ->setInformation('Information complémentaire')
                ->setPhoto('https://fastly.picsum.photos/id/823/200/200.jpg?hmac=zD0Ti1kYqMOUsfNVS7xtDou-2ECcI0RXYs18C54EdYo'),
            (new ProviderSessionSpeaker())
                ->setName('Nom de l\'intervenant')
                ->setTitle('Intitulé du poste')
                ->setInformation('Information complémentaire')
                ->setPhoto('https://fastly.picsum.photos/id/823/200/200.jpg?hmac=zD0Ti1kYqMOUsfNVS7xtDou-2ECcI0RXYs18C54EdYo'),
            (new ProviderSessionSpeaker())
                ->setName('Nom de l\'intervenant')
                ->setTitle('Intitulé du poste')
                ->setInformation('Information complémentaire')
                ->setPhoto('https://fastly.picsum.photos/id/823/200/200.jpg?hmac=zD0Ti1kYqMOUsfNVS7xtDou-2ECcI0RXYs18C54EdYo'),
        ];

        $client = new Client();
        $client->setName("Nom d'un client");

        $holder = new AdobeConnectConfigurationHolder();
        $holder->setClient($client);

        $providerSessionRegistrationPage = new ProviderSessionRegistrationPage();
        $providerSessionRegistrationPage->setPlaces(12);
        $providerSessionRegistrationPage->setDescription("<div> <h1>La Masterclass ultime : Devenez un expert de votre domaine en un rien de temps !</h1> <p>Vous souhaitez améliorer vos compétences professionnelles et devenir un expert dans votre domaine ? Ne cherchez plus ! La Masterclass ultime est faite pour vous !</p> <p>Notre formation en ligne est animée par des professionnels expérimentés qui ont une connaissance approfondie de leur secteur d'activité. Ils partageront avec vous les meilleures pratiques et les techniques les plus efficaces pour réussir dans votre domaine.</p> <p>Grâce à cette Masterclass, vous pourrez :</p> <ul> <li>Acquérir de nouvelles compétences et renforcer vos connaissances</li> <li>Découvrir les secrets des meilleurs experts de votre secteur</li> <li>Améliorer votre productivité et votre efficacité</li> <li>Booster votre carrière et augmenter vos opportunités professionnelles</li> </ul> <p>Ne manquez pas cette opportunité unique de vous former avec les meilleurs experts de votre domaine ! Inscrivez-vous dès maintenant à la Masterclass ultime et transformez votre carrière !</p> </div>");
        $providerSessionRegistrationPage->setDescription2('Découvrez la Masterclass ultime et devenez un expert dans votre domaine en un rien de temps ! Apprenez les meilleures pratiques et techniques de la part de professionnels expérimentés et transformez votre carrière dès maintenant !');

        $providerSession = new AdobeConnectSCO($holder);
        $providerSession
            ->setName('La Masterclass ultime : Devenez un expert de votre domaine en un rien de temps !')
            ->setCreatedAt(new \DateTime())
            ->setDateStart(new \DateTime())
            ->setDuration(60)
            ->setProviderSessionRegistrationPage($providerSessionRegistrationPage)
            ->setRegistrationPageTemplate($template)
            ->setSpeakers(new ArrayCollection($speakers));

        return $this->render('registration-page/page/base.html.twig', [
            'template' => $template,
            'session' => $providerSession,
            'isPreview' => true,
            'registrationPageTemplateLogoUri' => $registrationPageTemplateLogoUri,
        ]);
    }

    #[Route('/preview/{id}/faqs', name: '_registration_page_template_faqs', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(RegistrationPageTemplateVoter::EDIT, subject: 'template')]
    public function registrationPageFaqs(RegistrationPageTemplate $template, string $registrationPageTemplateLogoUri): Response
    {
        $holder = new AdobeConnectConfigurationHolder();
        $holder->setClient($template->getClient());

        $providerSession = new AdobeConnectSCO($holder);
        $providerSession
            ->setName('La Masterclass ultime : Devenez un expert de votre domaine en un rien de temps !');

        return $this->render('registration-page/page/faq.html.twig', [
            'session' => $providerSession,
            'template' => $template,
            'registrationPageTemplateLogoUri' => $registrationPageTemplateLogoUri,
            'isFaqs' => true,
            'isPreview' => true,
        ]);
    }

    #[Route('/faq/{id}/delete/{faqId}', name: '_registration_page_template_faq_delete', options: ['expose' => true], methods: ['DELETE'])]
    #[IsGranted(RegistrationPageTemplateVoter::EDIT, subject: 'template')]
    public function deleteFaq(RegistrationPageTemplate $template, int $faqId, RegistrationPageTemplateFAQRepository $FAQRepository): Response
    {
        /** @var RegistrationPageTemplateFAQ $faq */
        $faq = $FAQRepository->find($faqId);
        if (empty($faq) || $faq->getTemplateId() !== $template->getId()) {
            throw new AccessDeniedException();
        }

        if (count($template->getActiveFaqs())) {
            $template->getBlockUpperBanner()->setConnexionHelpCheck(false);
        }

        $this->entityManager->remove($faq);
        $this->entityManager->flush();

        return $this->apiResponse($this->normalizer->normalize($template->getFaqs(), context: ['groups' => ['ui:registration_page_template:read']]));
    }

    #[Route('/faq/save/{id}', name: '_registration_page_template_faq', options: ['expose' => true], methods: ['POST'])]
    #[IsGranted(RegistrationPageTemplateVoter::EDIT, subject: 'template')]
    public function createOrUpdateFaq(RegistrationPageTemplate $template, Request $request, RegistrationPageTemplateFAQRepository $FAQRepository): Response
    {
        $request = json_decode($request->getContent(), true);
        $context = [
            AbstractNormalizer::GROUPS => 'ui:faq:update',
            AbstractNormalizer::DEFAULT_CONSTRUCTOR_ARGUMENTS => [
                RegistrationPageTemplateFAQ::class => ['template' => $template],
            ],
        ];

        if (array_key_exists('id', $request)) {
            /** @var RegistrationPageTemplateFAQ $faq */
            $faq = $FAQRepository->find($request['id']);
            if (empty($faq) || $faq->getTemplateId() != $template->getId()) {
                throw new AccessDeniedException();
            }

            $context[AbstractNormalizer::OBJECT_TO_POPULATE] = $faq;
            $this->denormalizer->denormalize($request, RegistrationPageTemplateFAQ::class, context: $context);
        } else {
            $faq = $this->denormalizer->denormalize($request, RegistrationPageTemplateFAQ::class, context: $context);
        }

        $violations = $this->validator->validate($faq);
        if ($violations->count() === 0) {
            $this->entityManager->persist($faq);
            $this->entityManager->flush();

            return $this->apiResponse($this->normalizer->normalize($faq, context: ['groups' => ['ui:registration_page_template:read']]));
        } else {
            $extraDatas = [];
            foreach ($violations as $violation) {
                $extraDatas[$violation->getPropertyPath()] = $violation->getMessage();
            }

            return $this->apiErrorResponse($this->translator->trans('Check your data'), extraData: $extraDatas);
        }
    }

    #[Route('/upload/replay-image/{id}', name: '_provider_session_replay_image_upload_files', options: ['expose' => true], methods: ['POST'])]
    public function uploadReplayImage(Request $request, ProviderSession $providerSession): Response
    {
        try {
            /** @var UploadedFile $file */
            $file = $request->files->get('file');

            $formatParam = $request->request->get('format', implode(',', ProviderSessionSpeaker::AUTHORIZED_EXTENSIONS));
            $this->ressourceUploader->isFileUploadedValid($file, explode(',', $formatParam), ProviderSessionSpeaker::MAX_FILE_SIZE);

            $filename = \md5(\random_bytes(32)).'.'.$file->getClientOriginalExtension();

            (new Filesystem())->copy($file->getRealPath(), $this->registrationPageTemplateLogoSavefileDirectory.'/'.$filename, true);
            $providerSession->setPhotoReplay($filename);
            $this->entityManager->flush();

            return $this->apiResponse([
                'filetarget' => $filename,
                'previewUri' => $this->registrationPageTemplateLogoSavefileDirectory.$filename,
            ]);
        } catch (UploadFileException $exception) {
            $this->logger->error($exception);

            return $this->apiErrorResponse($exception->getMessage());
        }
    }

    #[Route('/{id}/sessions', name: '_session_get_sessions_from_registration_page', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted(RegistrationPageTemplateVoter::EDIT, subject: 'pageTemplate')]
    public function getSessions(Request $request, RegistrationPageTemplate $pageTemplate, ProviderSessionRepository $providerSessionRepository): Response
    {
        $data['sessions'] = $providerSessionRepository->getSessionByRegistrationPageTemplate($pageTemplate, $request->query->getBoolean('allow-outdated', false), $request->query->get('q', null));

        return new Response(
            $this->serializer->serialize(
                $data,
                'json',
                ['groups' => 'read_session_list'],
            )
        );
    }
}
