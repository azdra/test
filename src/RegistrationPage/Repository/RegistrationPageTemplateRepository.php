<?php

namespace App\RegistrationPage\Repository;

use App\RegistrationPage\Entity\RegistrationPageTemplate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class RegistrationPageTemplateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegistrationPageTemplate::class);
    }
}
