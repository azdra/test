<?php

namespace App\RegistrationPage\Repository;

use App\RegistrationPage\Entity\RegistrationPageTemplateFAQ;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class RegistrationPageTemplateFAQRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RegistrationPageTemplateFAQ::class);
    }
}
