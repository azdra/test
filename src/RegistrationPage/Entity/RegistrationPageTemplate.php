<?php

namespace App\RegistrationPage\Entity;

use App\Entity\Client\Client;
use App\Entity\ProviderSession;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class RegistrationPageTemplate
{
    public const AUTHORIZED_EXTENSIONS = ['jpg', 'png', 'pdf', 'odt', 'ods', 'doc', 'docx', 'txt'];
    public const MAX_FILE_SIZE = 2000000;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['ui:registration_page_template:read', 'read_session', 'read_session_list', 'read_client', 'ui:registration_page_template:hub'])]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: Client::class, inversedBy: 'registrationPageTemplates')]
    #[Groups(['ui:registration_page_template:read'])]
    protected Client $client;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups(['ui:registration_page_template:read', 'read_session', 'read_session_list', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    protected string $name;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Assert\Length(max: 200, maxMessage: 'Description must not exceed 200 character')]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    protected ?string $description = null;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    protected bool $active = true;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    protected ?string $language = 'fr';

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['ui:registration_page_template:read', 'read_client'])]
    protected \DateTimeImmutable $createdAt;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'created_by', referencedColumnName: 'id')]
    #[Groups(['ui:registration_page_template:read'])]
    protected ?User $createdBy = null;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['ui:registration_page_template:read', 'read_client'])]
    protected \DateTimeImmutable $updatedAt;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'updated_by', referencedColumnName: 'id')]
    #[Groups(['ui:registration_page_template:read'])]
    protected ?User $updatedBy = null;

    #[ORM\Column(type: 'json')]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?array $registrationForm = [];

    #[Assert\Valid]
    #[ORM\Embedded(class: RegistrationPageTemplateGraphicalCharter::class)]
    #[Groups(['ui:registration_page_template:read', 'ui:registration_page_template:update'])]
    private RegistrationPageTemplateGraphicalCharter $blockGraphicalCharter;

    #[Assert\Valid]
    #[ORM\Embedded(class: RegistrationPageTemplateDescription::class)]
    #[Groups(['ui:registration_page_template:read', 'ui:registration_page_template:update'])]
    private RegistrationPageTemplateDescription $blockDescription;

    #[Assert\Valid]
    #[ORM\Embedded(class: RegistrationPageTemplateUpperBanner::class)]
    #[Groups(['ui:registration_page_template:read', 'ui:registration_page_template:update'])]
    private RegistrationPageTemplateUpperBanner $blockUpperBanner;

    #[Assert\Valid]
    #[ORM\Embedded(class: RegistrationPageTemplateLowerBanner::class)]
    #[Groups(['ui:registration_page_template:read', 'ui:registration_page_template:update'])]
    private RegistrationPageTemplateLowerBanner $blockLowerBanner;

    #[ORM\OneToMany(mappedBy: 'registrationPageTemplate', targetEntity: RegistrationPageTemplateWhiteListEntry::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups('ui:registration_page_template:read')]
    private Collection $registrationPageTemplateWhiteList;

    #[ORM\OneToMany(mappedBy: 'registrationPageTemplate', targetEntity: RegistrationPageTemplateBlackListEntry::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups('ui:registration_page_template:read')]
    private Collection $registrationPageTemplateBlackList;

    #[ORM\OneToMany(mappedBy: 'registrationPageTemplate', targetEntity: ProviderSession::class, cascade: ['all'])]
    private Collection $sessions;

    #[ORM\OneToMany(mappedBy: 'template', targetEntity: RegistrationPageTemplateFAQ::class, cascade: ['persist'], orphanRemoval: true)]
    #[Groups(['ui:registration_page_template:read'])]
    protected Collection $faqs;

    public function __construct(Client $client)
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->client = $client;
        $this->blockGraphicalCharter = new RegistrationPageTemplateGraphicalCharter();
        $this->blockUpperBanner = new RegistrationPageTemplateUpperBanner();
        $this->blockDescription = new RegistrationPageTemplateDescription();
        $this->blockLowerBanner = new RegistrationPageTemplateLowerBanner();
        $this->sessions = new ArrayCollection();
        $this->faqs = new ArrayCollection();
        $this->registrationPageTemplateWhiteList = new ArrayCollection();
        $this->addWhiteListentry('*');
        $this->registrationPageTemplateBlackList = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    #[Groups(['ui:registration_page_template:hub'])]
    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getBlockGraphicalCharter(): RegistrationPageTemplateGraphicalCharter
    {
        return $this->blockGraphicalCharter;
    }

    public function setBlockGraphicalCharter(RegistrationPageTemplateGraphicalCharter $blockGraphicalCharter): self
    {
        $this->blockGraphicalCharter = $blockGraphicalCharter;

        return $this;
    }

    public function getBlockUpperBanner(): RegistrationPageTemplateUpperBanner
    {
        return $this->blockUpperBanner;
    }

    public function setBlockUpperBanner(RegistrationPageTemplateUpperBanner $blockUpperBanner): self
    {
        $this->blockUpperBanner = $blockUpperBanner;

        return $this;
    }

    public function getBlockDescription(): RegistrationPageTemplateDescription
    {
        return $this->blockDescription;
    }

    public function setBlockDescription(RegistrationPageTemplateDescription $blockDescription): self
    {
        $this->blockDescription = $blockDescription;

        return $this;
    }

    public function getBlockLowerBanner(): RegistrationPageTemplateLowerBanner
    {
        return $this->blockLowerBanner;
    }

    public function setBlockLowerBanner(RegistrationPageTemplateLowerBanner $blockLowerBanner): self
    {
        $this->blockLowerBanner = $blockLowerBanner;

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?User $updatedBy): self
    {
        if ($this->createdBy === null) {
            $this->createdBy = $updatedBy;
        }

        $this->updatedBy = $updatedBy;

        return $this;
    }

    #[Groups(['ui:registration_page_template:hub'])]
    public function getPathLogoClient(): ?string
    {
        return $this->client->getLogo();
    }

    #[Groups(['ui:registration_page_template:hub'])]
    public function getCategoryAvailableClient(): ?array
    {
        return $this->client->getCategories();
    }

    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function setSessions(Collection $sessions): void
    {
        $this->sessions = $sessions;
    }

    public function getRegistrationForm(): ?array
    {
        return $this->registrationForm;
    }

    public function setRegistrationForm(?array $registrationForm): self
    {
        $this->registrationForm = $registrationForm;

        return $this;
    }

    public function getFaqs(): ArrayCollection|Collection
    {
        return $this->faqs;
    }

    public function setFaqs(ArrayCollection|Collection $faqs): self
    {
        $this->faqs = $faqs;

        return $this;
    }

    public function addFaq(RegistrationPageTemplateFAQ $faq): self
    {
        if (!$this->faqs->contains($faq)) {
            $this->faqs->add($faq);
        }

        return $this;
    }

    public function removeFaq(RegistrationPageTemplateFAQ $faq): self
    {
        if ($this->faqs->contains($faq)) {
            $this->faqs->removeElement($faq);
        }

        return $this;
    }

    #[Groups(['ui:registration_page_template:read'])]
    public function getActiveFaqs(): ArrayCollection|Collection
    {
        return $this->faqs->filter(fn (RegistrationPageTemplateFAQ $faq) => $faq->isActive());
    }

    public function getRegistrationPageTemplateWhiteList(): Collection
    {
        return $this->registrationPageTemplateWhiteList;
    }

    public function setRegistrationPageTemplateWhiteList(Collection $registrationPageTemplateWhiteList): self
    {
        $this->registrationPageTemplateWhiteList = $registrationPageTemplateWhiteList;

        return $this;
    }

    public function getRegistrationPageTemplateBlackList(): Collection
    {
        return $this->registrationPageTemplateBlackList;
    }

    public function setRegistrationPageTemplateBlackList(Collection $registrationPageTemplateBlackList): self
    {
        $this->registrationPageTemplateBlackList = $registrationPageTemplateBlackList;

        return $this;
    }

    public function addWhiteListEntry(string $mail): self
    {
        $registrationPageTemplateWhiteListEntry = new RegistrationPageTemplateWhiteListEntry($this, $mail);
        if ($this->registrationPageTemplateWhiteList->count() == 1 && $this->getRegistrationPageTemplateWhiteList()->first()->getAllowed() == '*') {
            $this->registrationPageTemplateWhiteList->removeElement($this->getRegistrationPageTemplateWhiteList()->first());
        }
        if (!$this->registrationPageTemplateWhiteList->contains($registrationPageTemplateWhiteListEntry)) {
            $this->registrationPageTemplateWhiteList->add($registrationPageTemplateWhiteListEntry);
        }

        return $this;
    }

    public function removeWhiteListEntry(RegistrationPageTemplateWhiteListEntry $registrationPageTemplateWhiteListEntry): self
    {
        if ($this->registrationPageTemplateWhiteList->contains($registrationPageTemplateWhiteListEntry)) {
            $this->registrationPageTemplateWhiteList->removeElement($registrationPageTemplateWhiteListEntry);
        }

        if ($this->registrationPageTemplateWhiteList->count() === 0) {
            $this->registrationPageTemplateWhiteList->add(new RegistrationPageTemplateWhiteListEntry($this, '*'));
        }

        return $this;
    }

    public function addBlackListEntry(string $mail): self
    {
        $registrationPageTemplateBlackListEntry = new RegistrationPageTemplateBlackListEntry($this, $mail);

        if (!$this->registrationPageTemplateBlackList->contains($registrationPageTemplateBlackListEntry)) {
            $this->registrationPageTemplateBlackList->add($registrationPageTemplateBlackListEntry);
        }

        return $this;
    }

    public function removeBlackListEntry(RegistrationPageTemplateBlackListEntry $registrationPageTemplateBlackListEntry): self
    {
        if ($this->registrationPageTemplateBlackList->contains($registrationPageTemplateBlackListEntry)) {
            $this->registrationPageTemplateBlackList->removeElement($registrationPageTemplateBlackListEntry);
        }

        return $this;
    }

    public function isEmailAllowed(string $email): bool
    {
        return !$this->isBlackListed($email) && $this->isWhiteListed($email);
    }

    private function isBlackListed(string $email): bool
    {
        $blackListEntries = $this->registrationPageTemplateBlackList
            ->map(function (RegistrationPageTemplateBlackListEntry $entry) {
                return trim($entry->getProhibited());
            })
            ->toArray();

        foreach ($blackListEntries as $entry) {
            if (str_starts_with($entry, '*')) {
                $entry = substr_replace($entry, '.*', 0, 1);
            }
            if (preg_match("/$entry/", $email)) {
                return true;
            }
        }

        return false;
    }

    private function isWhiteListed(string $email): bool
    {
        $entries = $this->registrationPageTemplateWhiteList
            ->map(function (RegistrationPageTemplateWhiteListEntry $entry) {
                return trim($entry->getAllowed());
            })
            ->toArray();

        foreach ($entries as $entry) {
            if (str_starts_with($entry, '*')) {
                $entry = substr_replace($entry, '.*', 0, 1);
            }
            if (preg_match("/$entry/", $email)) {
                return true;
            }
        }

        return false;
    }
}
