<?php

namespace App\RegistrationPage\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class RegistrationPageTemplateFAQ
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups(['ui:registration_page_template:read'])]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: RegistrationPageTemplate::class, inversedBy: 'faqs')]
    protected RegistrationPageTemplate $template;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups(['ui:registration_page_template:read', 'ui:faq:update'])]
    protected string $question;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank()]
    #[Groups(['ui:registration_page_template:read', 'ui:faq:update'])]
    protected string $answer;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['ui:registration_page_template:read', 'ui:faq:update'])]
    protected bool $active = true;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['ui:registration_page_template:read'])]
    protected \DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['ui:registration_page_template:read'])]
    protected \DateTimeImmutable $updatedAt;

    public function __construct(RegistrationPageTemplate $template)
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->template = $template;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getQuestion(): string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getAnswer(): string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTemplate(): RegistrationPageTemplate
    {
        return $this->template;
    }

    public function setTemplate(RegistrationPageTemplate $template): void
    {
        $this->template = $template;
    }

    #[Groups(['ui:registration_page_template:read'])]
    public function getTemplateId(): int
    {
        return $this->template->getId();
    }
}
