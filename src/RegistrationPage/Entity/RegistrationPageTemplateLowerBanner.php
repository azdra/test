<?php

namespace App\RegistrationPage\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Embeddable]
class RegistrationPageTemplateLowerBanner
{
    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private bool $privacyCheck = false;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => 'Confidentalité'])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $privacyText = 'Confidentalité';

    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private bool $privacyUrlCheck = true;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => null])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $privacyUrl = null;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private bool $privacyFileCheck = false;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => null])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $privacyFile = null;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private bool $legalNoticeCheck = false;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => 'Mentions légales'])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $legalNoticeText = 'Mentions légales';

    #[ORM\Column(type: 'boolean', options: ['default' => true])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private bool $legalNoticeUrlCheck = true;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => null])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $legalNoticeUrl = null;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private bool $legalNoticeFileCheck = false;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => null])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $legalNoticeFile = null;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    protected bool $socialNetworksCheck = false;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => null])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    protected ?string $facebookLink = null;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => null])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    protected ?string $linkedinLink = null;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => null])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    protected ?string $instagramLink = null;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => null])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $twitterLink = null;

    public function isPrivacyCheck(): bool
    {
        return $this->privacyCheck;
    }

    public function setPrivacyCheck(bool $privacyCheck): void
    {
        $this->privacyCheck = $privacyCheck;
    }

    public function getPrivacyText(): ?string
    {
        return $this->privacyText;
    }

    public function setPrivacyText(?string $privacyText): void
    {
        $this->privacyText = $privacyText;
    }

    public function isPrivacyUrlCheck(): bool
    {
        return $this->privacyUrlCheck;
    }

    public function setPrivacyUrlCheck(bool $privacyUrlCheck): void
    {
        $this->privacyUrlCheck = $privacyUrlCheck;
    }

    public function getPrivacyUrl(): ?string
    {
        return $this->privacyUrl;
    }

    public function setPrivacyUrl(?string $privacyUrl): void
    {
        $this->privacyUrl = $privacyUrl;
    }

    public function isPrivacyFileCheck(): bool
    {
        return $this->privacyFileCheck;
    }

    public function setPrivacyFileCheck(bool $privacyFileCheck): void
    {
        $this->privacyFileCheck = $privacyFileCheck;
    }

    public function getPrivacyFile(): ?string
    {
        return $this->privacyFile;
    }

    public function setPrivacyFile(?string $privacyFile): void
    {
        $this->privacyFile = $privacyFile;
    }

    public function isLegalNoticeCheck(): bool
    {
        return $this->legalNoticeCheck;
    }

    public function setLegalNoticeCheck(bool $legalNoticeCheck): void
    {
        $this->legalNoticeCheck = $legalNoticeCheck;
    }

    public function getLegalNoticeText(): ?string
    {
        return $this->legalNoticeText;
    }

    public function setLegalNoticeText(?string $legalNoticeText): void
    {
        $this->legalNoticeText = $legalNoticeText;
    }

    public function isLegalNoticeUrlCheck(): bool
    {
        return $this->legalNoticeUrlCheck;
    }

    public function setLegalNoticeUrlCheck(bool $legalNoticeUrlCheck): void
    {
        $this->legalNoticeUrlCheck = $legalNoticeUrlCheck;
    }

    public function getLegalNoticeUrl(): ?string
    {
        return $this->legalNoticeUrl;
    }

    public function setLegalNoticeUrl(?string $legalNoticeUrl): void
    {
        $this->legalNoticeUrl = $legalNoticeUrl;
    }

    public function isLegalNoticeFileCheck(): bool
    {
        return $this->legalNoticeFileCheck;
    }

    public function setLegalNoticeFileCheck(bool $legalNoticeFileCheck): void
    {
        $this->legalNoticeFileCheck = $legalNoticeFileCheck;
    }

    public function getLegalNoticeFile(): ?string
    {
        return $this->legalNoticeFile;
    }

    public function setLegalNoticeFile(?string $legalNoticeFile): void
    {
        $this->legalNoticeFile = $legalNoticeFile;
    }

    public function isSocialNetworksCheck(): bool
    {
        return $this->socialNetworksCheck;
    }

    public function setSocialNetworksCheck(bool $socialNetworksCheck): void
    {
        $this->socialNetworksCheck = $socialNetworksCheck;
    }

    public function getFacebookLink(): ?string
    {
        return $this->facebookLink;
    }

    public function setFacebookLink(?string $facebookLink): void
    {
        $this->facebookLink = $facebookLink;
    }

    public function getLinkedinLink(): ?string
    {
        return $this->linkedinLink;
    }

    public function setLinkedinLink(?string $linkedinLink): void
    {
        $this->linkedinLink = $linkedinLink;
    }

    public function getInstagramLink(): ?string
    {
        return $this->instagramLink;
    }

    public function setInstagramLink(?string $instagramLink): void
    {
        $this->instagramLink = $instagramLink;
    }

    public function getTwitterLink(): ?string
    {
        return $this->twitterLink;
    }

    public function setTwitterLink(?string $twitterLink): void
    {
        $this->twitterLink = $twitterLink;
    }
}
