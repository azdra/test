<?php

namespace App\RegistrationPage\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Embeddable]
class RegistrationPageTemplateUpperBanner
{
    public const AUTHORIZED_EXTENSIONS = ['jpg', 'png'];
    public const MAX_FILE_SIZE = 2000000;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $catchphrase = null;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private bool $needHelpCheck = false;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => 'Besoin D\'aide ?'])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $needHelp = 'Besoin D\'aide ?';

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => null])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $needHelpIcon = null;

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private bool $connexionHelpCheck = false;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => 'Foire aux questions'])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $connexionHelp = 'Foire aux questions';

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $connexionHelpLink = null;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => '+33(0) 970 440 168'])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $supportCoordinates = '+33(0) 970 440 168';

    public function getCatchphrase(): ?string
    {
        return $this->catchphrase;
    }

    public function setCatchphrase(?string $catchphrase): void
    {
        $this->catchphrase = $catchphrase;
    }

    public function isNeedHelpCheck(): bool
    {
        return $this->needHelpCheck;
    }

    public function setNeedHelpCheck(bool $needHelpCheck): void
    {
        $this->needHelpCheck = $needHelpCheck;
    }

    public function getNeedHelp(): ?string
    {
        return $this->needHelp;
    }

    public function setNeedHelp(?string $needHelp): void
    {
        $this->needHelp = $needHelp;
    }

    public function getNeedHelpIcon(): ?string
    {
        return $this->needHelpIcon;
    }

    public function setNeedHelpIcon(?string $needHelpIcon): void
    {
        $this->needHelpIcon = $needHelpIcon;
    }

    public function isConnexionHelpCheck(): bool
    {
        return $this->connexionHelpCheck;
    }

    public function setConnexionHelpCheck(bool $connexionHelpCheck): void
    {
        $this->connexionHelpCheck = $connexionHelpCheck;
    }

    public function getConnexionHelp(): ?string
    {
        return $this->connexionHelp;
    }

    public function setConnexionHelp(?string $connexionHelp): void
    {
        $this->connexionHelp = $connexionHelp;
    }

    public function getConnexionHelpLink(): ?string
    {
        return $this->connexionHelpLink;
    }

    public function setConnexionHelpLink(?string $connexionHelpLink): void
    {
        $this->connexionHelpLink = $connexionHelpLink;
    }

    public function getSupportCoordinates(): ?string
    {
        return $this->supportCoordinates;
    }

    public function setSupportCoordinates(?string $supportCoordinates): void
    {
        $this->supportCoordinates = $supportCoordinates;
    }
}
