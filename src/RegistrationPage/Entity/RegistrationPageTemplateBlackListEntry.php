<?php

namespace App\RegistrationPage\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity]
class RegistrationPageTemplateBlackListEntry
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    #[ORM\Column(type: 'integer')]
    #[Groups('ui:registration_page_template:read')]
    protected int $id;

    #[ORM\ManyToOne(targetEntity: RegistrationPageTemplate::class, inversedBy: 'registrationPageTemplateBlackList')]
    protected RegistrationPageTemplate $registrationPageTemplate;

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups('ui:registration_page_template:read')]
    protected string $prohibited;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups('ui:registration_page_template:read')]
    protected \DateTimeImmutable $createdAt;

    public function __construct(RegistrationPageTemplate $registrationPageTemplate, string $prohibited)
    {
        $this->registrationPageTemplate = $registrationPageTemplate;
        $this->prohibited = $prohibited;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getRegistrationPageTemplate(): RegistrationPageTemplate
    {
        return $this->registrationPageTemplate;
    }

    public function setRegistrationPageTemplate(RegistrationPageTemplate $registrationPageTemplate): self
    {
        $this->registrationPageTemplate = $registrationPageTemplate;

        return $this;
    }

    public function getProhibited(): string
    {
        return $this->prohibited;
    }

    public function setProhibited(string $prohibited): self
    {
        $this->prohibited = $prohibited;

        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
