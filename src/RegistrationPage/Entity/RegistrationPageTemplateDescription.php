<?php

namespace App\RegistrationPage\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Embeddable]
class RegistrationPageTemplateDescription
{
    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private bool $descriptionCheck = false;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => 'Description'])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $descriptionTitle = 'Description';

    #[ORM\Column(type: 'boolean', options: ['default' => false])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private bool $speakersCheck = false;

    #[ORM\Column(type: 'string', nullable: true, options: ['default' => 'Intervenants'])]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $speakerTitle = 'Intervenants';

    public function isDescriptionCheck(): bool
    {
        return $this->descriptionCheck;
    }

    public function setDescriptionCheck(bool $descriptionCheck): void
    {
        $this->descriptionCheck = $descriptionCheck;
    }

    public function getDescriptionTitle(): ?string
    {
        return $this->descriptionTitle;
    }

    public function setDescriptionTitle(?string $descriptionTitle): void
    {
        $this->descriptionTitle = $descriptionTitle;
    }

    public function isSpeakersCheck(): bool
    {
        return $this->speakersCheck;
    }

    public function setSpeakersCheck(bool $speakersCheck): void
    {
        $this->speakersCheck = $speakersCheck;
    }

    public function getSpeakerTitle(): ?string
    {
        return $this->speakerTitle;
    }

    public function setSpeakerTitle(?string $speakerTitle): void
    {
        $this->speakerTitle = $speakerTitle;
    }
}
