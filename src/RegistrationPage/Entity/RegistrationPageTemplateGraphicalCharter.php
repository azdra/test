<?php

namespace App\RegistrationPage\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Embeddable]
class RegistrationPageTemplateGraphicalCharter
{
    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $font = 'Lato';

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $backgroundColorBanner = '#F8F8F8';

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $colorButton = '#93c626';

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $logo = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $banner = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['ui:registration_page_template:read', 'read_client', 'read_session', 'ui:registration_page_template:update', 'ui:registration_page_template:hub'])]
    private ?string $illustration = null;

    public function getFont(): ?string
    {
        return $this->font;
    }

    public function setFont(?string $font): void
    {
        $this->font = $font;
    }

    public function getBackgroundColorBanner(): ?string
    {
        return $this->backgroundColorBanner;
    }

    public function setBackgroundColorBanner(?string $backgroundColorBanner): void
    {
        $this->backgroundColorBanner = $backgroundColorBanner;
    }

    public function getColorButton(): ?string
    {
        return $this->colorButton;
    }

    public function setColorButton(?string $colorButton): void
    {
        $this->colorButton = $colorButton;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): void
    {
        $this->logo = $logo;
    }

    public function getBanner(): ?string
    {
        return $this->banner;
    }

    public function setBanner(?string $banner): void
    {
        $this->banner = $banner;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(?string $illustration): void
    {
        $this->illustration = $illustration;
    }
}
