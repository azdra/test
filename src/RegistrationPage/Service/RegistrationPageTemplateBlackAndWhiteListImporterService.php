<?php

namespace App\RegistrationPage\Service;

use App\Exception\MissingClientParameter;
use App\Exception\UploadFile\UploadFileNoFoundInRequestException;
use App\RegistrationPage\Entity\RegistrationPageTemplate;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Ods;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationPageTemplateBlackAndWhiteListImporterService
{
    public const ACTION_ADD_NEW_MAIL = 1;
    public const ACTION_DELETE_MAIL = 2;

    public function __construct(
        private EntityManagerInterface $entityManager,
        private TranslatorInterface $translator
    ) {
    }

    /**
     * @throws MissingClientParameter
     * @throws UploadFileNoFoundInRequestException
     */
    public function importMailsForWhiteAndBackList(Request $request, RegistrationPageTemplate $registrationPageTemplate): void
    {
        $typeList = $request->query->get('typeList');
        $this->validateRequest($request);
        $file = $request->files->get('file');

        if ($typeList != '') {
            $spreadsheet = $this->getSpreadsheet($file);
            $sheet = $spreadsheet->getSheet(0);
            $rowsData = $this->readData($sheet);

            match ($typeList) {
                'WhiteListTab' => $this->importWhiteList($rowsData, $registrationPageTemplate),
                'BlackListTab' => $this->importBlackList($rowsData, $registrationPageTemplate)
            };
        }
    }

    /**
     * @throws MissingClientParameter
     * @throws UploadFileNoFoundInRequestException
     */
    private function validateRequest(Request $request): void
    {
        if (empty($request->files->get('file')) || empty($request->files->get('file')->getClientOriginalExtension())) {
            throw new UploadFileNoFoundInRequestException($this->translator->trans('Not file is present'));
        }
    }

    private function getSpreadsheet(UploadedFile $file): Spreadsheet
    {
        $reader = match ($file->getClientOriginalExtension()) {
            'xls' => new Xls(),
            'xlsx' => new Xlsx(),
            'csv' => new Csv(),
            'ods' => new Ods()
        };
        $reader->setReadDataOnly(true);
        $reader->setReadEmptyCells(false);

        return $reader->load($file->getRealPath());
    }

    public function readData(Worksheet $sheet): array
    {
        $rowsData = [];
        $rowNum = 0;

        foreach ($sheet->getRowIterator(4) as $row) {
            ++$rowNum;
            $rowsData[$rowNum]['mail'] = $sheet->getCell('A'.$row->getRowIndex())->getValue();
            $rowsData[$rowNum]['action'] = $sheet->getCell('B'.$row->getRowIndex())->getValue();
        }

        return $rowsData;
    }

    public function importWhiteList(array $rowsData, RegistrationPageTemplate $registrationPageTemplate): void
    {
        foreach ($rowsData as $row) {
            if ($row['action'] == self::ACTION_DELETE_MAIL) {
                foreach ($registrationPageTemplate->getRegistrationPageTemplateWhiteList() as $entry) {
                    if (trim($entry->getAllowed()) === trim($row['mail'])) {
                        $registrationPageTemplate->removeWhiteListEntry($entry);
                    }
                }
            } else {
                $registrationPageTemplate->addWhiteListEntry($row['mail']);
            }
        }
    }

    public function importBlackList(array $rowsData, RegistrationPageTemplate $registrationPageTemplate): void
    {
        foreach ($rowsData as $row) {
            if ($row['action'] === self::ACTION_DELETE_MAIL) {
                foreach ($registrationPageTemplate->getRegistrationPageTemplateBlackList() as $entry) {
                    if (trim($entry->getProhibited()) === trim($row['mail'])) {
                        $registrationPageTemplate->removeBlackListEntry($entry);
                    }
                }
            } else {
                $registrationPageTemplate->addBlackListEntry($row['mail']);
            }
        }
    }
}
