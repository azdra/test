<?php

namespace App\RegistrationPage\Service;

use App\Entity\ProviderSession;
use App\Repository\ProviderParticipantSessionRoleRepository;
use App\Repository\ProviderSessionReplayRepository;
use App\Service\ProviderSessionReplayService;

class RegistrationPageService
{
    public function __construct(
        private ProviderParticipantSessionRoleRepository $providerParticipantSessionRoleRepository,
        private ProviderSessionReplayService $providerParticipantService,
        private ProviderSessionReplayRepository $providerSessionReplayRepository
    ) {
    }

    public function checkParticipantRegistered(ProviderSession $session, string $email): array
    {
        $userPPSR = $this->providerParticipantSessionRoleRepository->getParticipantWithSessionAndEmail($session, $email);
        $providerSessionReplay = $this->providerSessionReplayRepository->findOneBy(['email' => $email, 'session' => $session]);

        if ($session->isFinished()) {
            if ($session->getAccessParticipantReplay() && !$userPPSR) {
                return [
                    'isNotHaveAccess' => true,
                    'isInscrit' => false,
                    'currentUser' => null,
                ];
            }
            if ($userPPSR && !$providerSessionReplay) {
                $params = [
                    'session' => $session,
                    'firstname' => $userPPSR->getFirstName(),
                    'lastname' => $userPPSR->getLastName(),
                    'email' => $userPPSR->getEmail(),
                    'registrationFormResponses' => $userPPSR->getRegistrationFormResponses(),
                ];
                $this->providerParticipantService->generateSessionReplay($providerSessionReplay, $params);
            }

            if ($providerSessionReplay) {
                return [
                    'isInscrit' => true,
                    'currentUser' => [
                        'id' => $providerSessionReplay->getId(),
                        'token' => $providerSessionReplay->getProviderReplayAccessToken()->getToken(),
                    ],
                ];
            }
        } else {
            if ($userPPSR) {
                return [
                    'isInscrit' => true,
                    'currentUser' => [
                        'id' => $userPPSR->getId(),
                    ],
                ];
            }
        }

        return [
            'isInscrit' => false,
            'currentUser' => null,
        ];
    }
}
