<?php

namespace App\Exception;

class MissingProviderConfigurationHandler extends AbstractProviderException
{
}
