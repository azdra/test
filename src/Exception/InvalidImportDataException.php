<?php

namespace App\Exception;

class InvalidImportDataException extends \Exception
{
    public function __construct(
        private string $key,
        private array $params,
        string $message
    ) {
        parent::__construct($message);
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): self
    {
        $this->params = $params;

        return $this;
    }
}
