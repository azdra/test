<?php

namespace App\Exception;

class ClientNoMoreCreditAvailableException extends AbstractProviderException
{
    public function __construct()
    {
        parent::__construct("We're sorry, but your session modification credit is currently depleted. Please reload your account so you can make changes to your session. Thank you for your understanding.");
    }
}
