<?php

namespace App\Exception\UploadFile;

class UploadFileEmptyException extends UploadFileException
{
}
