<?php

namespace App\Exception\UploadFile;

class UploadFileNoFoundInRequestException extends UploadFileException
{
}
