<?php

namespace App\Exception\UploadFile;

class UploadFileTooManyRowException extends UploadFileException
{
}
