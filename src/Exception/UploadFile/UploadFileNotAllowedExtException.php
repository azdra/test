<?php

namespace App\Exception\UploadFile;

class UploadFileNotAllowedExtException extends UploadFileException
{
}
