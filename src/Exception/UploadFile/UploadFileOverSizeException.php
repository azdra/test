<?php

namespace App\Exception\UploadFile;

class UploadFileOverSizeException extends UploadFileException
{
}
