<?php

namespace App\Exception;

class UnknownProviderConfigurationHolderException extends MiddlewareException
{
}
