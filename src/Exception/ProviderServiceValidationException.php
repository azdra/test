<?php

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ProviderServiceValidationException extends \Exception
{
    public const MESSAGE = 'One or more fields are not valid for the session';

    public ConstraintViolationListInterface $violationList;

    public function __construct(ConstraintViolationListInterface $violationList)
    {
        parent::__construct(self::MESSAGE);
        $this->violationList = $violationList;
    }
}
