<?php

namespace App\Exception;

class LicenceCheckException extends ProviderSessionRequestHandlerException
{
    public function __construct(string $key, array $params, string $message)
    {
        parent::__construct($key, $params, true, $message);
    }
}
