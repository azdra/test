<?php

namespace App\Exception;

class UnknownMethodException extends MiddlewareException
{
}
