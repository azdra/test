<?php

namespace App\Exception;

class UnableToFindHostGroupException extends AbstractProviderException
{
}
