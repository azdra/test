<?php

namespace App\Exception;

final class MissingEntityInProviderActionException extends AbstractProviderException
{
    public function __construct(string $entityClass, int $identifier, string $methodName)
    {
        parent::__construct(sprintf('Unable to find %s with external identifier %s during action %s', $entityClass, $identifier, $methodName));
    }
}
