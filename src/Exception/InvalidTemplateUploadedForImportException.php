<?php

namespace App\Exception;

class InvalidTemplateUploadedForImportException extends AbstractProviderException
{
}
