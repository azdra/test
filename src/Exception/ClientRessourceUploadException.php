<?php

namespace App\Exception;

class ClientRessourceUploadException extends \Exception
{
    private string $target;

    public function __construct(string $target, string $message)
    {
        $this->target = $target;
        parent::__construct($message);
    }

    public function getTarget(): string
    {
        return $this->target;
    }
}
