<?php

namespace App\Exception;

class ConfigurationHolderViolationsException extends \Exception
{
    public function __construct(public array $violations)
    {
        parent::__construct();
    }
}
