<?php

namespace App\Exception;

class MiddlewareFailureException extends \Exception
{
    public function __construct(
        string $message,
        private mixed $data = null
    ) {
        parent::__construct($message);
    }

    public function getData(): mixed
    {
        return $this->data;
    }
}
