<?php

namespace App\Exception;

class UnsupportedProviderConfigurationHolderException extends AbstractProviderException
{
}
