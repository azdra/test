<?php

namespace App\Exception;

class InvalidArgumentException extends AbstractProviderException
{
}
