<?php
/**
 * @author:    Emmanuel SMITH <emmanuel.smith@live-session.fr>
 * project:    mls_m3
 * file:    ConvocationSendingFailureException.php
 * Date:    20/09/2021
 * Time:    16:09
 */

namespace App\Exception;

use Exception;

class ConvocationSendingFailureException extends Exception
{
}
