<?php

namespace App\Exception\Connector;

use App\Exception\AbstractProviderException;

class WebexRestMeetingMissingLicenceException extends AbstractProviderException
{
}
