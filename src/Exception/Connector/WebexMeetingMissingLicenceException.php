<?php

namespace App\Exception\Connector;

use App\Exception\AbstractProviderException;

class WebexMeetingMissingLicenceException extends AbstractProviderException
{
}
