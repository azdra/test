<?php

namespace App\Exception;

final class ProviderLoginNotImplementedException extends AbstractProviderException
{
    public function __construct(string $connectorClass)
    {
        parent::__construct(sprintf('Connector %s does not implement login() method.', $connectorClass));
    }
}
