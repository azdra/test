<?php

namespace App\Exception;

class ProviderGenericException extends AbstractProviderException
{
    public function __construct(string $className, string $message, ?string $code = null)
    {
        parent::__construct(sprintf('Provider %s returned the following error: %s [code: %s]', $className, $message, $code));
    }
}
