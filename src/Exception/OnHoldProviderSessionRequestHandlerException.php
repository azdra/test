<?php

namespace App\Exception;

class OnHoldProviderSessionRequestHandlerException extends \Exception
{
    public function __construct(
        private string $key,
        private array $params,
        private bool $displayable,
        string $message
    ) {
        parent::__construct($message);
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): self
    {
        $this->params = $params;

        return $this;
    }

    public function isDisplayable(): bool
    {
        return $this->displayable;
    }

    public function setDisplayable(bool $displayable): self
    {
        $this->displayable = $displayable;

        return $this;
    }
}
