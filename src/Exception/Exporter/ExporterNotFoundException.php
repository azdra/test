<?php

namespace App\Exception\Exporter;

class ExporterNotFoundException extends ExporterException
{
}
