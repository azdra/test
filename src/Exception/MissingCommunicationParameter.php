<?php

namespace App\Exception;

class MissingCommunicationParameter extends AbstractProviderException
{
}
