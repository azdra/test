<?php

namespace App\Exception;

final class UnexpectedProviderActionErrorException extends AbstractProviderException
{
    public function __construct(string $action)
    {
        parent::__construct(sprintf('Connector returned an unexpected result during action %s', $action));
    }
}
