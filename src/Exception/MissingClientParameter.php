<?php

namespace App\Exception;

class MissingClientParameter extends AbstractProviderException
{
}
