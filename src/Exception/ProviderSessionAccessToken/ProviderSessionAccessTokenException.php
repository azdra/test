<?php

namespace App\Exception\ProviderSessionAccessToken;

use App\Entity\ProviderSessionAccessToken;

class ProviderSessionAccessTokenException extends \Exception
{
    private ProviderSessionAccessToken $providerSessionAccessToken;

    private ?array $decryptedData;

    public function __construct(ProviderSessionAccessToken $providerSessionAccessToken, ?array $decryptedData)
    {
        parent::__construct();
        $this->providerSessionAccessToken = $providerSessionAccessToken;
        $this->decryptedData = $decryptedData;
    }

    public function getProviderSessionAccessToken(): ProviderSessionAccessToken
    {
        return $this->providerSessionAccessToken;
    }

    public function getDecryptedData(): ?array
    {
        return $this->decryptedData;
    }
}
