<?php

namespace App\Exception\ProviderSessionAccessToken;

class DecryptProviderSessionAccessTokenException extends ProviderSessionAccessTokenException
{
}
