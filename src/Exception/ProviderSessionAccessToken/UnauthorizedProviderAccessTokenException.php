<?php

namespace App\Exception\ProviderSessionAccessToken;

class UnauthorizedProviderAccessTokenException extends ProviderSessionAccessTokenException
{
}
