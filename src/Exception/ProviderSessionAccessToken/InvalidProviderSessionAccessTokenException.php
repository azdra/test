<?php

namespace App\Exception\ProviderSessionAccessToken;

class InvalidProviderSessionAccessTokenException extends ProviderSessionAccessTokenException
{
}
