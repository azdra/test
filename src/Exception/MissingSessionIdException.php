<?php

namespace App\Exception;

class MissingSessionIdException extends MiddlewareException
{
}
