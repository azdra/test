<?php

namespace App\Form;

use App\Entity\CategorySessionType;
use App\Entity\ProviderSession;
use App\Form\Type\TagsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractSessionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Title of the session',
            ])
            ->add('ref', TextType::class, [
                'label' => 'Reference',
            ])
            ->add('dateStart', DateTimeType::class, [
                'label' => 'Date',
            ])
            ->add('businessNumber', TextType::class, [
                'label' => 'Business Number',
                'required' => false,
            ])
            ->add('category', EnumType::class, [
                'label' => 'Category',
                'class' => CategorySessionType::class,
                'choice_label' => fn (CategorySessionType $category): string => $category->label(),
            ])
            ->add('tags', TagsType::class, [
                'label' => 'Tags',
                'required' => false,
            ])
            ->add('duration', TextType::class, [
                'label' => 'Duration',
            ])
            ->add('information', TextType::class, [
                'label' => 'Informations',
                'required' => false,
            ])
            ->add('language', LanguageType::class,
                ['label' => 'Default language']
            )
            ->add('origin', TextType::class, [
                'label' => 'Origin',
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProviderSession::class,
        ]);
        $resolver->setRequired('provider');
    }
}
