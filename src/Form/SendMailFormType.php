<?php

namespace App\Form;

use App\Form\Type\TagsType;
use App\Model\SendMailFormModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class SendMailFormType extends AbstractType
{
    public function __construct(private TranslatorInterface $translator)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('recipients', TagsType::class, [
            'label' => 'To :',
            'required' => true,
            'attr' => [
                'placeholder' => $this->translator->trans('Add an email'),
            ],
        ])->add('cc', TagsType::class, [
            'label' => 'CC :',
            'required' => false,
            'attr' => [
                'placeholder' => $this->translator->trans('Add an email'),
            ],
        ])->add('bcc', TagsType::class, [
            'label' => 'BCC :',
            'required' => false,
            'attr' => [
                'placeholder' => $this->translator->trans('Add an email'),
            ],
        ])->add('save', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary float-end'],
            'label' => 'Send',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SendMailFormModel::class,
        ]);
    }
}
