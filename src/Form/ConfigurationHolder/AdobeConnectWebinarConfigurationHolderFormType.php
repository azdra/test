<?php

namespace App\Form\ConfigurationHolder;

use App\Entity\Adobe\AdobeConnectWebinarSCO;
use App\Entity\Client\Client;
use App\Entity\ConfigurationHolder\Adobe\AdobeConnectWebinarConfigurationHolder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdobeConnectWebinarConfigurationHolderFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class)
            ->add('subdomain', TextType::class)
            ->add('username', TextType::class)
            ->add('password', PasswordType::class)
            ->add('client', EntityType::class, ['class' => Client::class])
            ->add('defaultConnexionType', ChoiceType::class, [
                'choices' => [
                    AdobeConnectWebinarSCO::PERMISSION_VERB_DENIED,
                    AdobeConnectWebinarSCO::PERMISSION_VERB_REMOVE,
                    AdobeConnectWebinarSCO::PERMISSION_VERB_VIEW_HIDDEN,
                ],
            ])
            ->add('birthday', DateTimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('licenceType', ChoiceType::class, [
                'choices' => [
                    AdobeConnectWebinarConfigurationHolder::LICENCES_CLIENT_SIMULTANEOUS,
                    AdobeConnectWebinarConfigurationHolder::LICENCES_CLIENT_NAMED,
                ],
            ])
            ->add('defaultRoom', NumberType::class, ['required' => false])
            ->add('standardViewOnOpeningRoom', CheckboxType::class, ['required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AdobeConnectWebinarConfigurationHolder::class,
            'csrf_protection' => false,
        ]);
    }
}
