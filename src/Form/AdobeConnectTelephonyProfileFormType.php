<?php

namespace App\Form;

use App\Entity\Adobe\AdobeConnectTelephonyProfile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdobeConnectTelephonyProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name',
                'required' => false,
                'disabled' => true,
            ])
            ->add('connectionType', ChoiceType::class, [
                'label' => 'Connection Type',
                'choices' => AdobeConnectTelephonyProfile::getAvailableConnectionType(),
            ])
            ->add('phoneNumber', TextType::class, [
                'label' => 'Phone number',
                'required' => false,
                'disabled' => true,
            ])
            ->add('codeParticipant', TextType::class, [
                'label' => 'Participant code',
                'required' => false,
                'disabled' => true,
            ])
            ->add('codeAnimator', TextType::class, [
                'label' => 'Animator code',
                'required' => false,
                'disabled' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AdobeConnectTelephonyProfile::class,
        ]);
    }
}
