<?php

namespace App\Form\Api;

use App\Entity\Microsoft\MicrosoftTeamsEventSession;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MicrosoftTeamsEventApiType extends ProviderSessionApiType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder->add('licence');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => MicrosoftTeamsEventSession::class,
        ]);
    }
}
