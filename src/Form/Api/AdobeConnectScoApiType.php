<?php

namespace App\Form\Api;

use App\Entity\Adobe\AdobeConnectSCO;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdobeConnectScoApiType extends ProviderSessionApiType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => AdobeConnectSCO::class,
        ]);
    }
}
