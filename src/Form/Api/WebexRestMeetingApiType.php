<?php

namespace App\Form\Api;

use App\Entity\Cisco\WebexRestMeeting;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WebexRestMeetingApiType extends ProviderSessionApiType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder->add('licence')
        ->add('openTime');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => WebexRestMeeting::class,
        ]);
    }
}
