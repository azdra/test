<?php

namespace App\Form\Api;

use App\Controller\Api\OnHoldProviderSessionApiController;
use App\Entity\OnHoldProviderSession;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OnHoldProviderSessionApiType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['group'] === OnHoldProviderSessionApiController::GROUP_CREATE) {
            $builder
                ->add('ref');
        }

        $builder
            ->add('dateStart', DateTimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('dateEnd', DateTimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('duration')
            ->add('name');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => OnHoldProviderSession::class,
            'allow_extra_fields' => true,
            'csrf_protection' => false,
        ]);
        $resolver->setRequired('group');
    }
}
