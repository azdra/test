<?php

namespace App\Form\Api;

use App\Entity\Cisco\WebexMeeting;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WebexMeetingApiType extends ProviderSessionApiType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder->add('licence')
        ->add('openTime');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class' => WebexMeeting::class,
        ]);
    }
}
