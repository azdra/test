<?php

namespace App\Form\Api;

use App\Controller\Api\ProviderSessionApiController;
use App\Entity\ProviderSession;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProviderSessionApiType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['group'] === ProviderSessionApiController::GROUP_CREATE) {
            $builder
                ->add('ref');
        }

        $builder
            ->add('dateStart', DateTimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('dateEnd', DateTimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('duration')
            ->add('name')
            ->add('convocation');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProviderSession::class,
            'allow_extra_fields' => true,
            'csrf_protection' => false,
        ]);
        $resolver->setRequired('group');
    }
}
