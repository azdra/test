<?php

namespace App\Form\Client;

use App\Entity\Client\ClientEmailScheduling;
use App\Form\Client\Tab\ClientEmailTabFormType;
use App\Form\Type\ScheduleType;
use App\Form\Type\TagsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ClientEmailSchedulingFormType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('timeToSendConvocationBeforeSession', ScheduleType::class, [
                'label' => 'Sending the default convocation',
                'required' => false,
                'validation_groups' => [ClientEmailTabFormType::FORM_NAME],
            ])
            ->add('timesToSendReminderBeforeSession', CollectionType::class, [
                'label' => 'Sending the default reminders',
                'allow_add' => true,
                'allow_delete' => true,
                'required' => false,
                'entry_type' => ScheduleType::class,
                'entry_options' => [
                    'label' => false,
                    'validation_groups' => [ClientEmailTabFormType::FORM_NAME],
                ],
            ])
//            ->add('enableSendRemindersToAnimators', CheckboxType::class, [
//                    'label' => 'Sending reminders to animators',
//                    'required' => false,
//                    'attr' => ['class' => 'inline'],
//                    'validation_groups' => [ClientEmailTabFormType::FORM_NAME],
//                ]
//            )
            ->add('npaiAlertRecipients', TagsType::class, [
                'label' => 'Notice for invalid emails',
                'required' => false,
                'validation_groups' => [ClientEmailTabFormType::FORM_NAME],
                'attr' => [
                    'placeholder' => $this->translator->trans('Add an email'),
                ],
            ])
            ->add('timeToSendPresenceReportAfterSession', ScheduleType::class, [
                'label' => 'Sending the attendance report',
                'required' => false,
                'validation_groups' => [ClientEmailTabFormType::FORM_NAME],
                'displayMinutes' => false,
                'help' => '(after the end of the session)',
            ])
            ->add('presenceReportRecipients', TagsType::class, [
                'label' => 'Recipient',
                'required' => false,
                'validation_groups' => [ClientEmailTabFormType::FORM_NAME],
                'attr' => [
                    'placeholder' => $this->translator->trans('Add an email'),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ClientEmailScheduling::class,
            'validation_groups' => ClientEmailTabFormType::FORM_NAME,
        ]);
    }
}
