<?php

namespace App\Form\Client;

use App\Entity\Client\ClientLicensesAvailable;
use App\Form\Client\Tab\ClientOptionsTabFormType;
use App\Form\Type\TagsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ClientLicensesAvailableFormType extends AbstractType
{
    public function __construct(
        private readonly TranslatorInterface $translator
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('enabled', CheckboxType::class, [
                'label' => 'Enable Licenses available page',
                'required' => false,
            ])
            ->add('thresholdMin', NumberType::class, [
                'label' => 'Minimum threshold',
                'scale' => 0,
                'html5' => true,
                'help' => 'Red color',
                'required' => false,
            ])
            ->add('thresholdMiddle', NumberType::class, [
                'label' => 'Middle threshold',
                'html5' => true,
                'help' => 'Orange color',
                'required' => false,
            ])
            ->add('licensesReportRecipients', TagsType::class, [
                'label' => 'Email alert',
                'required' => false,
                'help' => 'Each night, an email alert will be sent with the list of red zone slots.',
                'attr' => [
                    'placeholder' => $this->translator->trans('Add an email'),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ClientLicensesAvailable::class,
            'validation_groups' => [ClientOptionsTabFormType::FORM_NAME],
        ]);
    }
}
