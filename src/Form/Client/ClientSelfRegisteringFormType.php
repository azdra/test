<?php

namespace App\Form\Client;

use App\Entity\Client\ClientSelfRegistering;
use App\Form\Client\Tab\ClientOptionsTabFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientSelfRegisteringFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('enabled', CheckboxType::class, [
                'label' => 'Activate the registration catalog',
                'required' => false,
            ])
            ->add('gdprInformation', TextareaType::class, [
                'label' => 'GDPR Informative Text',
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ClientSelfRegistering::class,
            'validation_groups' => [ClientOptionsTabFormType::FORM_NAME],
        ]);
    }
}
