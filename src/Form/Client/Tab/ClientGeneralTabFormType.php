<?php

namespace App\Form\Client\Tab;

use App\Entity\Client\Client;
use App\Form\Client\ClientContactFormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ClientGeneralTabFormType extends AbstractClientTabFormType
{
    public const FORM_NAME = 'clientGeneralForm';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Account name',
            ])
            ->add(
                'defaultLang',
                LanguageType::class,
                ['label' => 'Default language', 'choice_loader' => null, 'choices' => ['French' => 'fr', 'English' => 'en'], 'choice_translation_domain' => true]
            )
            ->add(
                'timezone',
                ChoiceType::class,
                [
                    'label' => 'Timezone',
                    'choices' => $this->getTimezoneChoices(),
                ]
            )
            ->add(
                'categories',
                ChoiceType::class,
                [
                    'label' => 'Category',
                    'choices' => [...Client::CATEGORY_ALL],
                    'choice_label' => function ($key) {
                        return $key;
                    },
                    'multiple' => true,
                    'expanded' => true,
                ]
            );

        if (!$options['minimal']) {
            $builder
                ->add('commercialContact', ClientContactFormType::class, [
                    'label' => 'Commercial',
                    'required' => false,
                ])
                ->add('customerContact', ClientContactFormType::class, [
                    'label' => 'Customer success',
                    'required' => false,
                ]);
        }
    }

    public static function getName(): string
    {
        return self::FORM_NAME;
    }

    private function getTimezoneChoices(): array
    {
        $timezones = \DateTimeZone::listIdentifiers();

        $choices = [];
        foreach ($timezones as $timezone) {
            $tz = new \DateTimeZone($timezone);
            $offset = $tz->getOffset(new \DateTime());
            $offsetHours = floor(abs($offset) / 3600);
            $offsetMinutes = abs($offset) % 3600 / 60;
            $offsetSign = ($offset < 0) ? '-' : '+';
            $offsetFormatted = sprintf('GMT %s%02d:%02d', $offsetSign, $offsetHours, $offsetMinutes);
            $choices[sprintf('%s (%s)', $timezone, $offsetFormatted)] = $timezone;
        }

        return $choices;
    }
}
