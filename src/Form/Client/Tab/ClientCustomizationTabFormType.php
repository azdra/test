<?php

namespace App\Form\Client\Tab;

use App\Entity\Client\Client;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ClientCustomizationTabFormType extends AbstractClientTabFormType
{
    public const FORM_NAME = 'clientCustomizationForm';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('logoFile', VichImageType::class, [
                'label' => 'Logo',
                'required' => false,
                'allow_delete' => true,
                'download_label' => fn (Client $client): string => $this->getOriginalFileName($client->getLogo()) ?? '',
                'constraints' => [
                    new Image([
                        'groups' => self::getName(),
                        'maxSize' => '2M',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                    ]),
                ],
                'help' => 'Maximum 2MB and image JPG/JPEG/PNG accepted',
            ])
            ->add('headerFile', VichImageType::class, [
                'label' => 'Header',
                'required' => false,
                'allow_delete' => true,
                'download_label' => fn (Client $client): string => $this->getOriginalFileName($client->getHeader()) ?? '',
                'constraints' => [
                    new Image([
                        'groups' => self::getName(),
                        'maxSize' => '2M',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Please upload an image with JPEG, JPG or PNG extension',
                    ]),
                ],
                'help' => 'Maximum 2MB and image JPG/JPEG/PNG accepted. Recommended size: 725x100 pixels',
            ])
            ->add('footerFile', VichImageType::class, [
                'label' => 'Footer',
                'required' => false,
                'allow_delete' => true,
                'download_label' => fn (Client $client): string => $this->getOriginalFileName($client->getFooter()) ?? '',
                'constraints' => [
                    new Image([
                        'groups' => self::getName(),
                        'maxSize' => '2M',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Please upload an image with JPEG, JPG or PNG extension',
                    ]),
                ],
                'help' => 'Maximum 2MB and image JPG/JPEG/PNG accepted. Recommended size: 725x100 pixels',
            ]);

        if (!$options['minimal']) {
            $builder
                    ->add('organizationStampFile', VichImageType::class, [
                        'label' => 'Organization stamp',
                        'required' => false,
                        'allow_delete' => true,
                        'download_label' => fn (Client $client): string => $this->getOriginalFileName($client->getOrganizationStamp()) ?? '',
                        'constraints' => [
                            new Image([
                                'groups' => self::getName(),
                                'maxSize' => '2M',
                                'mimeTypes' => [
                                    'image/jpeg',
                                    'image/png',
                                ],
                                'mimeTypesMessage' => 'Please upload an image with JPEG, JPG or PNG extension',
                            ]),
                        ],
                        'help' => 'Maximum 2MB and image JPG/JPEG/PNG accepted. Recommended size: 725x150 pixels',
                    ])
                    ->add('sessionImportModelFile', VichFileType::class, [
                        'label' => 'Session Import File Template',
                        'required' => false,
                        'allow_delete' => true,
                        'download_label' => fn (Client $client): string => $this->getOriginalFileName($client->getSessionImportModel()) ?? '',
                        'constraints' => [
                            // @See https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types for all MIME types
                            new File([
                                'groups' => self::getName(),
                                'maxSize' => '2M',
                                'mimeTypes' => [
                                    'application/vnd.ms-excel',
                                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                ],
                                'mimeTypesMessage' => 'Please upload a document with XLS or XLSX extension',
                            ]),
                        ],
                        'help' => 'Maximum 2MB and file Excel or OpenDocument accepted',
                    ])
                    ->add('traineesImportModelFile', VichFileType::class, [
                        'label' => 'Participant Import File Template',
                        'required' => false,
                        'allow_delete' => true,
                        'download_label' => fn (Client $client): string => $this->getOriginalFileName($client->getTraineesImportModel()) ?? '',
                        'constraints' => [
                            // @See https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types for all MIME types
                            new File([
                                'groups' => self::getName(),
                                'maxSize' => '2M',
                                'mimeTypes' => [
                                    'application/vnd.ms-excel',
                                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                ],
                                'mimeTypesMessage' => 'Please upload a document with XLS or XLSX extension',
                            ]),
                        ],
                        'help' => 'Maximum 2MB and file Excel or OpenDocument accepted',
                    ])
                    ->add('closedRoomMessage', TextType::class, [
                        'label' => 'Closed room message',
                        'required' => false,
                    ])
                    ->add('color', ColorType::class, [
                        'label' => 'Color',
                        'required' => false,
                    ]);
        }
    }

    private function getOriginalFileName(?string $currentFileName): ?string
    {
        return preg_replace('/(-[\w]+)\.png$/', '', $currentFileName ?? '').'.png';
    }

    public static function getName(): string
    {
        return self::FORM_NAME;
    }
}
