<?php

namespace App\Form\Client\Tab;

use App\Entity\Client\ClientServices;
use App\Form\Type\ServiceScheduleType;
use App\Validator\Constraint\ServiceSchedule;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientServicesTabFormType extends AbstractClientTabFormType
{
    public const FORM_NAME = 'clientServiceForm';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('assistanceType', ChoiceType::class, [
            'choices' => [
                'No assistance' => ClientServices::ENUM_NO,
                'Available on demand' => ClientServices::ENUM_ON_DEMAND,
                'Launch assistance for each session' => ClientServices::ENUM_FULL,
            ],
            'required' => false,
            'placeholder' => false,
            'label' => false,
            'expanded' => true,
            'attr' => [
                'readonly' => $options['readonly'] ?? false,
            ],
            'disabled' => $options['readonly'] ?? false,
        ])
        ->add('assistanceDuration', null, [
            'label' => 'Duration of assistance (min)',
        ])
        ->add('assistanceStart', null, [
            'label' => 'Start of the assistance (min)',
        ])
        ->add('commitment', null, [
            'label' => 'Contractual commitment',
            'help' => 'Number of parallel assistances that our consultants will provide',
        ])
        ->add('emailToNotifyForAssistance', null, [
            'label' => 'Notification email',
            'required' => false,
        ])
        ->add('supportType', ChoiceType::class, [
            'choices' => [
                'No support' => ClientServices::ENUM_NO,
                'Available on demand' => ClientServices::ENUM_ON_DEMAND,
                'Launch support for each session' => ClientServices::ENUM_FULL,
            ],
            'required' => false,
            'placeholder' => false,
            'label' => false,
            'expanded' => true,
        ])
        ->add('standardHours', ServiceScheduleType::class, [
            'label_active' => 'standard working hours',
            'constraints' => new ServiceSchedule(['groups' => [ClientServicesTabFormType::FORM_NAME]]),
        ])
        ->add('outsideStandardHours', ServiceScheduleType::class, [
            'label_active' => 'outside standard working hours',
            'constraints' => new ServiceSchedule(['groups' => [ClientServicesTabFormType::FORM_NAME]]),
        ])
        ->add('outsideSupportType', ChoiceType::class, [
            'choices' => [
                'The session launch' => ClientServices::ENUM_SUPPORT_FOR_START_SESSION,
                'The full session' => ClientServices::ENUM_SUPPORT_FOR_ALL_SESSION,
            ],
            'label' => false,
            'expanded' => false,
        ])
        ->add('timeBeforeSession')
        ->add('timeSupportDurationSession')
        ->add('emailToNotifyForSupport', null, [
            'label' => 'Notification email',
            'required' => false,
        ])
        ->add('dedicatedSupport', null, [
            'label' => 'Active support',
            'required' => false,
        ])
        ->add('mailSupport', null, [
            'label' => 'Email',
        ])
        ->add('phoneSupport', null, [
            'label' => 'Phone number',
        ])
        ->add('dedicatedUserGuide', null, [
            'label' => 'Customized user guide',
        ])
        ->add('personnalizedInfoMailTransactionnal', TextareaType::class, [
            'label' => 'GDPR Informative text',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ClientServices::class,
            'validation_groups' => $this::getName(),
            'minimal' => false,
        ]);
    }

    public static function getName(): string
    {
        return self::FORM_NAME;
    }
}
