<?php

namespace App\Form\Client\Tab;

use App\Form\Client\ClientLicensesAvailableFormType;
use App\Form\Client\ClientSelfRegisteringFormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class ClientOptionsTabFormType extends AbstractClientTabFormType
{
    public const FORM_NAME = 'clientOptionsForm';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            //            ->add('enableAssessmentAchievement', CheckboxType::class, [
            //                'label' => 'Enable learning assessment',
            //                'required' => false,
            //            ])
            //            ->add('enableSharedEmail', CheckboxType::class, [
            //                'label' => 'Enable shared emails',
            //                'required' => false,
            //            ])
            ->add('enableSendEmail', CheckboxType::class, [
                'label' => 'Enable send emails',
                'required' => false,
            ])
            ->add('enableRemoteSignature', CheckboxType::class, [
                'label' => 'Enable digital sign-in',
                'required' => false,
                //'help' => 'To be ticked if the client wants the participants to sign an attendance sheet via a link to be shared and/or a QR code to be scanned',
            ])
            ->add('fillEmail', ChoiceType::class, [
                'choices' => [
                    'Participants will be asked to enter their email address in order to sign in' => true,
                    'Participants will have to click on their name in the list of participants to be able to sign' => false,
                ],
                'label' => ' ',
                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'placeholder' => false,
            ])
            ->add('enableAccessLinkOnExport', CheckboxType::class, [
                'label' => 'Add the individual access link in the registrants export files',
                'required' => false,
                'help' => 'Check it if your client wants to recover the individual links of connection of the participants (ex.: integration to an LMS and/or not reception of the convocations)',
            ])
            ->add('enableSendToReferrer', CheckboxType::class, [
                'label' => 'Send an email in cc to the referent',
                'required' => false,
                'help' => "The referrer will receive a copy of the participant's invitation, modification or cancellation emails.",
            ])
           ->add('selfRegistering', ClientSelfRegisteringFormType::class, [
                'label' => 'Self-registration',
                'required' => false,
            ])
            ->add('enableRegistrationPage', CheckboxType::class, [
                'label' => 'Activate registering page',
                'required' => false,
            ])
            ->add('licensesAvailable', ClientLicensesAvailableFormType::class, [
                'label' => 'Licenses-available',
                'required' => false,
            ])
        ;
    }

    public static function getName(): string
    {
        return self::FORM_NAME;
    }
}
