<?php

namespace App\Form\Client\Tab;

use App\Entity\Client\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractClientTabFormType extends AbstractType
{
    abstract public static function getName(): string;

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
            'validation_groups' => $this::getName(),
            'minimal' => false,
            'disabled' => false,
        ]);
    }
}
