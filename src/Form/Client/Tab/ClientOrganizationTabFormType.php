<?php

namespace App\Form\Client\Tab;

use App\Entity\Client\TrainingActionNatureEnum;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ClientOrganizationTabFormType extends AbstractClientTabFormType
{
    public const FORM_NAME = 'clientOrganizationForm';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'organisationName',
                TextType::class,
                [
                    'label' => 'Name',
                    'required' => false,
                ]
            )
            ->add(
                'organisationLegalRepresentative',
                TextType::class,
                [
                    'label' => 'First and last name',
                    'required' => false,
                ]
            )
            ->add(
                'organisationFunctionLegalRepresentative',
                TextType::class,
                [
                    'label' => 'Function',
                    'required' => false,
                ]
            )
            ->add(
                'organisationCompetentCommercialCourt',
                TextType::class,
                [
                    'label' => 'Competent commercial court',
                    'required' => false,
                ]
            )
            ->add(
                'organisationDefaultTrainingActionNature',
                EnumType::class,
                [
                    'label' => 'Default nature of training actions',
                    'class' => TrainingActionNatureEnum::class,
                    'choice_label' => fn (TrainingActionNatureEnum $trainingActionNature): string => $trainingActionNature->label(),
                    'required' => false,
                ]
            )
            ->add(
                'organisationTrainingActivityRegistrationNumber',
                TextType::class,
                [
                    'label' => 'Training Activity Registration Number',
                    'required' => false,
                ]
            )
            ->add(
                'organisationCity',
                TextType::class,
                [
                    'label' => 'City',
                    'required' => false,
                ]
            )
            ->add(
                'organisationDistrict',
                TextType::class,
                [
                    'label' => 'District',
                    'required' => false,
                ]
            );
    }

    public static function getName(): string
    {
        return self::FORM_NAME;
    }
}
