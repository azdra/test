<?php

namespace App\Form\Client\Tab;

use App\Entity\Client\Client;
use App\Form\Client\ClientEmailOptionsType;
use App\Form\Client\ClientEmailSchedulingFormType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ClientEmailTabFormType extends AbstractClientTabFormType
{
    public const FORM_NAME = 'clientEmailsForm';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('senderTransporter', ChoiceType::class, [
                'choices' => [
                    'Mandrill' => Client::TRANSPORTER_EMAIL_MANDRILL,
                    'Brevo' => Client::TRANSPORTER_EMAIL_BREVO,
                ],
                'label' => ' ',
                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'placeholder' => false,
                'help' => 'Allows you to choose the carrier used to send transactional emails',
            ])
            ->add(
                'senderName',
                TextType::class,
                [
                    'label' => 'Name',
                    'required' => false,
                ]
            )
            ->add(
                'senderEmail',
                TextType::class,
                [
                    'label' => 'Email',
                    'required' => false,
                    'attr' => [
                        'class' => 'inline',
                    ],
                ]
            )
            ->add(
                'replayToEmail',
                TextType::class,
                [
                    'label' => 'Reply to',
                    'required' => false,
                ]
            )
            ->add('emailOptions', ClientEmailOptionsType::class, ['label' => false])
            ->add('emailScheduling', ClientEmailSchedulingFormType::class, ['label' => false]);

        $builder->addEventListener(
            FormEvents::SUBMIT,
            function (FormEvent $event) {
                if ($event->getData()->getSenderTransporter() == Client::TRANSPORTER_EMAIL_MANDRILL) {
                    $senderEmail = $event->getData()->getSenderEmail().Client::SENDER_EMAIL_SUFFIX_MANDRILL;
                    $event->getData()->setSenderEmail($senderEmail);
                } elseif ($event->getData()->getSenderTransporter() == Client::TRANSPORTER_EMAIL_BREVO) {
                    $senderEmail = $event->getData()->getSenderEmail().Client::SENDER_EMAIL_SUFFIX_BREVO;
                    $event->getData()->setSenderEmail($senderEmail);
                }
            }
        );

        /*$builder
            ->get('senderEmail')
            ->addModelTransformer(new CallbackTransformer(
                fn ($formToEntity) => str_replace(Client::SENDER_EMAIL_SUFFIX_MANDRILL, '', $formToEntity ?? ''),
                fn ($entityToForm) => ($entityToForm) ? $entityToForm.Client::SENDER_EMAIL_SUFFIX_MANDRILL : null
            ));*/
    }

    public static function getName(): string
    {
        return self::FORM_NAME;
    }
}
