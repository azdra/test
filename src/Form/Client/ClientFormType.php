<?php

namespace App\Form\Client;

use App\Form\Client\Tab\ClientCustomizationTabFormType;
use App\Form\Client\Tab\ClientGeneralTabFormType;
use App\Form\Client\Tab\ClientOrganizationTabFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('general', ClientGeneralTabFormType::class, [
                'label' => 'General',
                'data' => $options['client'],
                'minimal' => true,
            ])
            ->add('customization', ClientCustomizationTabFormType::class, [
                'label' => 'Customization',
                'data' => $options['client'],
                'minimal' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'label' => false,
            'validation_groups' => [
                'Default',
                ClientGeneralTabFormType::getName(),
                ClientOrganizationTabFormType::getName(),
                ClientCustomizationTabFormType::getName(),
            ],
        ]);

        $resolver->setRequired('client');
    }
}
