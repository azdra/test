<?php

namespace App\Form\Client;

use App\Entity\Client\ClientEmailOptions;
use App\Form\Client\Tab\ClientEmailTabFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientEmailOptionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('sendACancellationEmailToTrainees', CheckboxType::class, [
                'label' => 'Sending an email to participants when a session is cancelled',
                'required' => false,
                'attr' => [
                    'class' => 'inline',
                ],
            ])
            ->add('sendAnUnsubscribeEmailToParticipants', CheckboxType::class, [
                'label' => 'Sending an email when unsubscribing a participant or animator',
                'required' => false,
                'attr' => [
                    'class' => 'inline',
                ],
            ])
            ->add('sendAnAttestationAutomatically', CheckboxType::class, [
                'label' => 'Automatic sending of certificates after the session to participants present only',
                'required' => false,
                'attr' => [
                    'class' => 'inline',
                ],
                'help' => 'Every day at 11pm, for the past 24-hour sessions, individual training certificates will be automatically sent to each participant present.',
            ])
            ->add('sendPresenceReportAfterSession', CheckboxType::class, [
                'label' => 'Automatically send attendance reports',
                'required' => false,
                'attr' => ['class' => 'inline'],
                'help' => 'Attendance reports are available 24 hours after the session is completed.',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ClientEmailOptions::class,
            'validation_groups' => [ClientEmailTabFormType::getName()],
        ]);
    }
}
