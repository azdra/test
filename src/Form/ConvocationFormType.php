<?php

namespace App\Form;

use App\Entity\CategorySessionType;
use App\Entity\ConfigurationHolder\AbstractProviderAudio;
use App\Entity\Convocation;
use App\Form\Type\TinymceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConvocationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('status', CheckboxType::class, [
                'label' => 'Active',
                'required' => false,
            ])
            ->add('name', TextType::class, [
                'label' => 'Template name',
            ])
            ->add('subjectMail', TextType::class, [
                'label' => 'Subject of the e-mail',
            ])
            ->add('category', EnumType::class, [
                'label' => 'Category',
                'class' => CategorySessionType::class,
                'choice_label' => fn (CategorySessionType $category): string => $category->label(),
                'required' => false,
            ])
            ->add('languages', LanguageType::class, [
                'label' => '',
                'choice_loader' => null,
                'choices' => ['French' => 'fr', 'English' => 'en'],
                'choice_translation_domain' => true,
            ])
            ->add('content', TinymceType::class, [
                'label' => 'Message body',
                'required' => false,
            ])
            ->add('categoryTypeAudio', ChoiceType::class, [
                'label' => 'Type de connexion',
                'required' => false,
                'placeholder' => false,
                'attr' => [
                    'onChange' => 'reloadTypesAudio(this.value)',
                ],
                'choices' => [
                    'Telephone' => AbstractProviderAudio::AUDIO_CATEGORY_PHONE,
                    'Computer' => AbstractProviderAudio::AUDIO_CATEGORY_COMPUTER,
                    'Hybride' => AbstractProviderAudio::AUDIO_CATEGORY_HYBRIDE,
                    'Presential' => AbstractProviderAudio::AUDIO_CATEGORY_PRESENTIAL,
                ],
            ])
            ->add('typeAudio', ChoiceType::class, [
                'label' => 'Audio provider',
                'required' => false,
                'placeholder' => false,

                'choices' => [
                    'Intercall' => AbstractProviderAudio::AUDIO_INTERCALL,
                    'PGi NA' => AbstractProviderAudio::AUDIO_PGI,
                    'PGi EMEA' => AbstractProviderAudio::AUDIO_PGI_EMEA,
                    'CALLIN' => AbstractProviderAudio::AUDIO_CALLIN,
                    'AUDIO_VOIP' => AbstractProviderAudio::AUDIO_VOIP,
                    'MeetingOne EMEA' => AbstractProviderAudio::AUDIO_MEETINGONE_EMEA,
                    'MeetingOne NA' => AbstractProviderAudio::AUDIO_MEETINGONE_NA,
                ],
            ])
            ->add('connectorType', ChoiceType::class, [
                'label' => 'Connector Type',
                'required' => true,
                'choices' => [
                    'Adobe Connect' => Convocation::TYPE_ADOBE_CONNECT,
                    'Adobe Connect Webinar' => Convocation::TYPE_ADOBE_CONNECT_WEBINAR,
                    'Cisco Webex' => Convocation::TYPE_CISCO_WEBEX,
                    'Cisco Webex Rest' => Convocation::TYPE_CISCO_WEBEX_REST,
                    'Microsoft Teams' => Convocation::TYPE_MICROSOFT_TEAMS,
                    'Microsoft Teams Event' => Convocation::TYPE_MICROSOFT_TEAMS_EVENT,
                    'White Connector' => Convocation::TYPE_WHITE_CONNECTOR,
                ],
                'attr' => [
                    'onChange' => 'filterTypesAudio(this.value)',
                ],
            ])
            ->add('ics', CheckboxType::class, [
                'label' => 'ICS',
                'required' => false,
                'attr' => [
                    'class' => 'form-check-input',
                    'style' => 'height: 20px;',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Convocation::class,
            'label' => false,
        ]);
    }
}
