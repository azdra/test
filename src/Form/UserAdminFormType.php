<?php

namespace App\Form;

use App\Entity\Client\Client;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class UserAdminFormType extends AbstractType
{
    protected Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Email',
            ])
            ->add('firstName', TextType::class, [
                'label' => 'First name',
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Last name',
            ])
            ->add('phone', TextType::class, [
                'label' => 'Phone',
                'required' => false,
            ])
            ->add('preferredLang', ChoiceType::class, [
                'choices' => [
                    'French' => 'fr',
                    'English' => 'en',
                ],
                'label' => 'Preferred language',
                'expanded' => false,
            ])
            ->add('roles', ChoiceType::class, [
                'label' => 'Roles',
                'choices' => ['Super Admin' => User::ROLE_ADMIN],
                'data' => [User::ROLE_ADMIN],
                'multiple' => true,
                'expanded' => true,
                'disabled' => true,
            ]);
        if ($this->security->isGranted(User::ROLE_ADMIN)) {
            $builder->add('client', EntityType::class, [
                'class' => Client::class,
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
