<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TagsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('tags', TextType::class);

        $builder
            ->get('tags')
            ->addModelTransformer(new CallbackTransformer(
                fn ($tagsAsArray) => (!is_null($tagsAsArray)) ? implode(',', $tagsAsArray) : null,
                fn ($tagsAsString) => explode(',', $tagsAsString)
            ));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['compound' => true]);
    }

    public function getParent(): string
    {
        return TextType::class;
    }
}
