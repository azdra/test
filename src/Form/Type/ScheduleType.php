<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Range;

class ScheduleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('days', NumberType::class, [
                'scale' => 0,
                'html5' => true,
                'label' => false,
                'constraints' => [
                    new Range(min: 0, groups: $options['validation_groups']),
                ],
                'attr' => [
                    'class' => 'inline',
                ],
            ])
            ->add('hours', NumberType::class, [
                'label' => false,
                'scale' => 0,
                'html5' => true,
                'constraints' => [
                    new Range(min: 0, max: 23, groups: $options['validation_groups']),
                ],
                'attr' => [
                    'class' => 'inline',
                ],
            ]);

        if ($options['displayMinutes']) {
            $builder->add('minutes', NumberType::class, [
                'label' => false,
                'scale' => 0,
                'html5' => true,
                'constraints' => [
                    new Range(min: 0, max: 59, groups: $options['validation_groups']),
                ],
                'attr' => [
                    'class' => 'inline',
                ],
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'compound' => true,
            'displayMinutes' => true,
        ]);
    }

    public function getParent(): string
    {
        return TextType::class;
    }
}
