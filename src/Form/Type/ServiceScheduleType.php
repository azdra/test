<?php

namespace App\Form\Type;

use App\Entity\ServiceSchedule;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServiceScheduleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('active', CheckboxType::class, [
                'required' => false,
                'label' => $options['label_active'],
            ])
            ->add('startTimeAM', TimeType::class, [
                'label' => false,
                'required' => false,
                'widget' => 'single_text',
            ])
            ->add('endTimeAM', TimeType::class, [
                'label' => false,
                'required' => false,
                'widget' => 'single_text',
            ])
            ->add('startTimeLH', TimeType::class, [
                'label' => false,
                'required' => false,
                'widget' => 'single_text',
            ])
            ->add('endTimeLH', TimeType::class, [
                'label' => false,
                'required' => false,
                'widget' => 'single_text',
            ])
            ->add('startTimePM', TimeType::class, [
                'label' => false,
                'required' => false,
                'widget' => 'single_text',
            ])
            ->add('endTimePM', TimeType::class, [
                'label' => false,
                'required' => false,
                'widget' => 'single_text',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ServiceSchedule::class,
            'compound' => true,
            'label_active' => false,
        ]);
    }
}
