<?php

namespace App\Form;

use App\Entity\Client\Client;
use App\Entity\User;
use App\Repository\ClientRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserAccountManagerFormType extends AbstractType
{
    protected AuthorizationCheckerInterface $authorizationChecker;

    protected TokenStorageInterface $token;

    protected ClientRepository $clientRepository;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker, TokenStorageInterface $token, ClientRepository $clientRepository)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->token = $token;
        $this->clientRepository = $clientRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'First name',
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Last name',
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
            ])
            ->add('phone', TextType::class, [
                'label' => 'Phone',
                'required' => false,
            ])
            ->add('preferredLang', ChoiceType::class, [
                'choices' => [
                    'French' => 'fr',
                    'English' => 'en',
                ],
                'label' => 'Preferred language',
                'expanded' => false,
            ])
            ->add('roles', ChoiceType::class, [
                'label' => 'Roles',
                'choices' => [
                    'Super Admin' => User::ROLE_ADMIN,
                    'Account admin' => User::ROLE_ADMIN_MANAGER,
                    'Account manager' => User::ROLE_MANAGER,
                    'Account API user' => User::ROLE_API,
                ],
                'multiple' => true,
                'expanded' => true,
            ]);

        if ($this->authorizationChecker->isGranted(User::ROLE_ADMIN)) {
            $builder->add('client', EntityType::class, [
                'label' => 'Account',
                'class' => Client::class,
                'data' => $options['client'],
            ]);
        } else {
            $builder->add('client', HiddenType::class, [
                'label' => 'Account',
                'mapped' => false,
                'data' => $options['client'],
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'client' => new Client(),
        ]);
    }
}
