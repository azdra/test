<?php

namespace App\Form;

use App\Entity\Client\Client;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class UserParticipantFormType extends AbstractType
{
    protected Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Email',
            ])
            ->add('firstName', TextType::class, [
                'label' => 'First name',
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Last name',
            ])
            ->add('company', TextType::class, [
                'label' => 'Company',
                'required' => false,
            ])
            ->add('phone', TextType::class, [
                'label' => 'Cellphone',
                'required' => false,
            ])
            ->add('preferredLang', ChoiceType::class, [
                'choices' => [
                    'French' => 'fr',
                    'English' => 'en',
                ],
                'label' => 'Preferred language',
                'expanded' => false,
            ])
            ->add('emailReferrer', EmailType::class, [
                'label' => 'Email Referrer',
                'required' => false,
                'help' => "The referrer will receive a copy of the participant's invitation, modification or cancellation emails.",
            ])
            ->add('information', TextType::class, [
                'label' => 'Informations', 'required' => false,
            ]);

        if ($this->security->isGranted(User::ROLE_HOTLINE_CUSTOMER_SERVICE)) {
            $builder->add('client', EntityType::class, [
                'class' => Client::class,
            ]);
        }

        if ($options['update_role']) {
            $builder->add('roles', ChoiceType::class, [
                'label' => 'Roles',
                'choices' => user::ROLES,
                'multiple' => true,
                'expanded' => true,
                'disabled' => !$this->security->isGranted(User::ROLE_ADMIN),
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'update_role' => false,
        ]);
    }
}
