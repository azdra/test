<?php

namespace App\Form;

use App\Model\UpdateUserPasswordModel;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdateUserPasswordFormType extends ResetUserPasswordFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder->add('oldPassword', PasswordType::class, [
            'priority' => 10, // Display this field first, before typing new password
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UpdateUserPasswordModel::class,
        ]);
    }
}
