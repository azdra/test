<?php

namespace App\Form;

use App\Entity\Client\Client;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Image;

class UserProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var User $user */
        $user = $options['data'] ?? null;

        $builder
            ->add('profilePicture', FileType::class, [
                'required' => false,
                'mapped' => false,
                'label' => 'Profile picture',
                'constraints' => [
                    new Image([
                        'maxSize' => '2M',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                    ]),
                ],
            ])
            ->add('firstName', TextType::class, [
                'label' => 'First name',
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Last name',
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'constraints' => [
                    new Email(),
                ],
            ])
            ->add('phone', TelType::class, [
                'label' => 'Phone',
                'required' => false,
                'help' => 'Number in 10-digit or international format',
            ])
            ->add('preferredLang', ChoiceType::class, [
                'choices' => [
                    'French' => 'fr',
                    'English' => 'en',
                ],
                'label' => 'Preferred language',
                'help' => 'Language of the interface',
            ])
            ->add('preferredInitialRoute', ChoiceType::class, [
                'choices' => $options['preferredInitialRoute'],
                'label' => 'Home page',
            ])
            ->add('defaultClient', EntityType::class, [
                'label' => 'Default account',
                'class' => Client::class,
                'query_builder' => function (EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder(Client::ALIAS)
                        ->join(User::class, User::ALIAS, Join::WITH, User::ALIAS.'.client = '.Client::ALIAS)
                        ->where(User::ALIAS.'.status IN (:status)')
                        ->setParameter('status', [User::STATUS_ACTIVE, User::STATUS_INACTIVE])
                        ->andWhere(User::ALIAS.'.email = :email')
                        ->setParameter('email', $user->getEmail())
                        ->orderBy(Client::ALIAS.'.name');
                },
                'data' => $options['activeClient'],
                'mapped' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'label' => false,
            'allow_extra_fields' => true,
        ]);
        $resolver->setRequired([
            'activeClient',
            'preferredInitialRoute',
        ]);
    }
}
