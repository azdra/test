<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;

class LoginAuthenticatorFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', EmailType::class, [
                'attr' => ['placeholder' => 'Email'],
                'constraints' => [new Email()],
            ])
            ->add('password', PasswordType::class, [
                'attr' => ['placeholder' => 'Password'],
            ])
            ->add('_remember_me', CheckboxType::class, [
                'required' => false,
            ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}
