import axios from 'axios'
import Vue from 'vue'
import moment                 from 'moment'
import Vuex                   from 'vuex'
import { Datetime }           from 'vue-datetime'
import { Settings, DateTime } from 'luxon'
import VueClipboard           from 'vue-clipboard2'
import VueVirtualScroller from 'vue-virtual-scroller'
import VueSignaturePad from 'vue-signature-pad'
import * as bootstrap from 'bootstrap'

import 'vue-datetime/dist/vue-datetime.css'
import 'vue-virtual-scroller/dist/vue-virtual-scroller.css'
import { routing, translator } from '../utils/app.utils'

moment.locale(document.documentElement.lang)

export default () => {
  Vue.prototype.$translator = translator
  Vue.prototype.$axios = axios
  Vue.prototype.$moment = moment
  Vue.prototype.$routing = routing
  Vue.prototype.$dateTime = DateTime
  Vue.prototype.$bootstrap = bootstrap
  Vue.mixin({
    filters: {
      formatDate: (date, format) => {
        // see https://moment.github.io/luxon/#/formatting?id=presets for a list of preset formatting (DATETIME_MED)
        // or https://moment.github.io/luxon/#/formatting?id=table-of-tokens for defined yours (dd MMM y 'à' HH:mm)
        const presetFormat = {
          datetime_med: DateTime.DATETIME_MED,
          hours_minutes: DateTime.TIME_24_SIMPLE,
          date_simple: DateTime.DATE_SHORT
        }

        const datetime = DateTime.fromISO(date)

        if (format in presetFormat) {
          return datetime.toLocaleString(presetFormat[format])
        }
        return datetime.toFormat(format)
      }
    }
  })
  Vue.use(Datetime)
  Vue.use(Vuex)
  Vue.use(VueClipboard)
  Vue.use(VueVirtualScroller)
  Vue.use(VueSignaturePad)

  Vue.directive('click-outside', {
    bind: function (el, binding, vnode) {
      el.clickOutsideEvent = function (event) {
        if (!(el === event.target || el.contains(event.target))) {
          vnode.context[binding.expression](event)
        }
      }
      document.body.addEventListener('click', el.clickOutsideEvent)
    },
    unbind: function (el) {
      document.body.removeEventListener('click', el.clickOutsideEvent)
    }
  })
}

// Set to display dates for English language
/* global userPreferredTimezone,userPreferredLocale */
if (typeof userPreferredTimezone !== 'undefined') {
  Settings.defaultLocale = userPreferredLocale
  Settings.defaultZoneName = userPreferredTimezone
}
