import Vue from 'vue'
import Vuex from 'vuex'
import currentUser from './module/currentUser'
import formError from './module/formError'
import session from './module/session'
import sessionMembersSelector from './module/session-members-selector'
import sessionAdditionalsTimezonesSelector from './module/session-additionals-timezones-selector'
import membersGroupSelector from './module/group-member-selector'
import sessionWizard from './module/sessionWizard'
import sessionWizardCreation from './module/sessionWizardCreation'
import client from './module/clientServices'
import module from './module/module'
import process from './module/process'
import selfSubscription from './module/selfSubscription'
import configurationHolders from './module/configurationHolders'
import template from './module/template'
import openai from './module/openai'
import editSubject from './module/editSubject'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    formError,
    session,
    sessionWizard,
    sessionWizardCreation,
    currentUser,
    sessionMembersSelector,
    sessionAdditionalsTimezonesSelector,
    membersGroupSelector,
    client,
    module,
    process,
    selfSubscription,
    configurationHolders,
    template,
    openai,
    editSubject
  }
})
