import { PROVIDER_ADOBE_CONNECT } from '../../utils/adobe-connect-configuration-holder.utils'
import { PROVIDER_ADOBE_CONNECT_WEBINAR } from '../../utils/adobe-connect-webinar-configuration-holder.utils'
import { translator }             from '../../utils/app.utils'
import store                      from '../index'
import { PROVIDER_CISCO_WEBEX } from '../../utils/cisco-webex-configuration-holder.utils'
import { PROVIDER_CISCO_WEBEX_REST } from '../../utils/cisco-webex-rest-configuration-holder.utils'
import { PROVIDER_MICROSOFT_TEAMS } from '../../utils/microsoft-teams-configuration-holder.utils'
import { PROVIDER_MICROSOFT_TEAMS_EVENT } from '../../utils/microsoft-teams-event-configuration-holder.utils'

const state = () => ({
  errorsFields: {
    title: {
      notnull: {
        'is-valid': true,
        message: translator.trans('The title of the session cannot be empty')
      }
    },
    category: {
      notnull: {
        'is-valid': true,
        message: translator.trans('Required field')
      }
    },
    lang: {
      notnull: {
        'is-valid': true,
        message: translator.trans('Required field')
      }
    },
    audioCategoryType: {
      notnull: {
        'is-valid': true,
        message: translator.trans('Required field')
      }
    },
    modelConvocation: {
      notnull: {
        'is-valid': true,
        message: translator.trans('Required field')
      }
    },
    admittedSession: {
      notnull: {
        'is-valid': true,
        message: translator.trans('Required field')
      }
    },
    roomModel: {
      notnull: {
        'is-valid': true,
        message: translator.trans('Session room template cannot be blank')
      }
    },
    reference: {
      notnull: {
        'is-valid': true,
        message: translator.trans('Required field')
      }
    },
    licence: {
      notnull: {
        'is-valid': true,
        message: translator.trans('Required field')
      }
    }
  },
  validationDone: 0,
  formSuccess: false
})

const getters = {}

const actions = {
  validateFields ({ state }) {
    let formSuccess = true
    const session = store.getters['sessionWizard/sessionToSaved']
    const clientData = store.getters['sessionWizard/clientData']

    if (typeof (session.title) === 'undefined' || session.title == null || session.title === '') {
      state.errorsFields.title.notnull['is-valid'] = false
      formSuccess = false
    } else {
      state.errorsFields.title.notnull['is-valid'] = true
    }
    if (typeof (session.category) === 'undefined' || session.category === '') {
      state.errorsFields.category.notnull['is-valid'] = false
      formSuccess = false
    } else {
      state.errorsFields.category.notnull['is-valid'] = true
    }
    if (typeof (session.lang) === 'undefined' || session.lang == null || session.lang === '') {
      state.errorsFields.lang.notnull['is-valid'] = false
      formSuccess = false
    } else {
      state.errorsFields.lang.notnull['is-valid'] = true
    }
    if (typeof (session.audioCategoryType) === 'undefined' || session.audioCategoryType == null || session.audioCategoryType === '') {
      state.errorsFields.audioCategoryType.notnull['is-valid'] = false
      formSuccess = false
    } else {
      state.errorsFields.audioCategoryType.notnull['is-valid'] = true
    }
    if (typeof (session.convocation) === 'undefined' || session.convocation == null || session.convocation === '' || (clientData.convocationsListClient && session.convocation && clientData.convocationsListClient[session.convocation].audioCategoryType !== session.audioCategoryType)) {
      state.errorsFields.modelConvocation.notnull['is-valid'] = false
      formSuccess = false
    } else {
      state.errorsFields.modelConvocation.notnull['is-valid'] = true
    }
    if (typeof (session.reference) === 'undefined' || session.reference === null || session.reference === '') {
      state.errorsFields.reference.notnull['is-valid'] = false
      formSuccess = false
    } else {
      state.errorsFields.reference.notnull['is-valid'] = true
    }

    switch (clientData.provider.type) {
      case PROVIDER_ADOBE_CONNECT:
        if (session.accessType === undefined || session.accessType === null) {
          state.errorsFields.admittedSession.notnull['is-valid'] = false
          formSuccess = false
        } else {
          state.errorsFields.admittedSession.notnull['is-valid'] = true
        }
        break
      case PROVIDER_ADOBE_CONNECT_WEBINAR:
        if (session.accessType === undefined || session.accessType === null) {
          state.errorsFields.admittedSession.notnull['is-valid'] = false
          formSuccess = false
        } else {
          state.errorsFields.admittedSession.notnull['is-valid'] = true
        }
        break
      case PROVIDER_CISCO_WEBEX:
        if (!clientData.provider.automaticLicensing) {
          if (typeof (session.licence) === 'undefined' || session.licence === null || session.licence === '') {
            state.errorsFields.licence.notnull['is-valid'] = false
            formSuccess = false
          }
        }
        break
      case PROVIDER_CISCO_WEBEX_REST:
        if (!clientData.provider.automaticLicensing) {
          if (typeof (session.licence) === 'undefined' || session.licence === null || session.licence === '') {
            state.errorsFields.licence.notnull['is-valid'] = false
            formSuccess = false
          } else {
            state.errorsFields.licence.notnull['is-valid'] = true
          }
        }
        break
      case PROVIDER_MICROSOFT_TEAMS:
        if (!clientData.provider.automaticLicensing) {
          if (typeof (session.licence) === 'undefined' || session.licence === null || session.licence === '') {
            state.errorsFields.licence.notnull['is-valid'] = false
            formSuccess = false
          }
        }
        break
      case PROVIDER_MICROSOFT_TEAMS_EVENT:
        if (!clientData.provider.automaticLicensing) {
          if (typeof (session.licence) === 'undefined' || session.licence === null || session.licence === '') {
            state.errorsFields.licence.notnull['is-valid'] = false
            formSuccess = false
          }
        }
        break
    }

    state.formSuccess = formSuccess
  }
}

const mutations = {}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
