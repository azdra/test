const state = () => ({
  loading:            true,
  saving:             false,
  success:            null,
  error:              null,
  errorModal:         null,
  formErrors:         {}
})

const getters = {}

const actions = {}

const mutations = {
  loading (state, loading) {
    state.loading = loading
  },
  resetSaving (state) {
    state.saving = false
    state.success = null
    state.error = null
    state.errorModal = null
    state.formErrors = {}
  },
  initSaving (state)  {
    state.saving = true
    state.success = null
    state.error = null
    state.errorModal = null
    state.formErrors = {}
  },
  endSaving (state) {
    state.saving = false
    state.success = true
    state.error = null
    state.errorModal = null
    state.formErrors = {}
  },
  errorSaving (state, payload) {
    state.saving = false
    state.success = false
    for (const key in payload) {
      state[key] = payload[key]
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
