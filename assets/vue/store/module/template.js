import Vue from 'vue'

const state = () => ({
  view: 'List',
  templates: [],
  template: null,
  displayClient: false,
  importRestrictions: false,
  clients: {},
  faq: null,
  faqSettings: {
    searchText: '',
    searchStatus: true
  },
  contentTemplate: {
    openedBlocks: []
  }
})

const getters = {
  filteredSession (state, getters) {
    const search = new RegExp('.*' + state.contentSettings.search + '.*', 'g')
    return getters.sessions.filter(session => session.name.match(search) && (!state.contentSettings.onlyCurrentSession || session.running || session.scheduled))
  },
  filteredSessionIds (state, getters) {
    return getters.filteredSession.map(session => session.id)
  },
  activeFAQs (state) {
    const search = new RegExp('.*' + state.faqSettings.searchText + '.*', 'g')
    return state.template.faqs
      .filter(faq => {
        if (state.faqSettings.searchStatus && faq.active !== true) {
          return false
        }
        return faq.question.match(search) || faq.answer.match(search)
      })
  }
}

const actions = {}

const mutations = {
  emptyTemplates (state) {
    // empty templates
    state.templates.splice(0, state.templates.length)
  },
  newTemplate (state) {
    state.template = {
      name: '',
      description: '',
      language: 'fr',
      blockGraphicalCharter: {
        font: 'Lato',
        backgroundColorBanner: '#F8F8F8',
        colorButton: '#93c626',
        logo: null,
        banner: null,
        illustration: null
      },
      blockUpperBanner: {
        catchphrase: null,
        needHelpCheck: false,
        needHelp: 'Besoin d\'aide',
        needHelpIcon: null,
        connexionHelpCheck: false,
        connexionHelp: 'Foire aux questions',
        connexionHelpLink: null,
        supportCoordinates: '+33(0) 970 440 168'
      },
      blockDescription: {
        descriptionCheck: false,
        descriptionTitle: 'Description',
        speakersCheck: false,
        speakerTitle: 'Intervenants'
      },
      blockLowerBanner: {
        privacyCheck: false,
        privacyText: 'Informations légales',
        privacyUrlCheck: true,
        privacyUrl: null,
        privacyFileCheck: false,
        privacyFile: null,

        legalNoticeCheck: false,
        legalNoticeText: 'Mentions légales',
        legalNoticeUrlCheck: true,
        legalNoticeUrl: null,
        legalNoticeFileCheck: false,
        legalNoticeFile: null,

        socialNetworksCheck: false,
        facebookLink: null,
        linkedinLink: null,
        instagramLink: null,
        twitterLink: null
      },
      client: {
        id: state.clients[Object.keys(state.clients)[0]].id
      }
    }
  },
  updateTemplates (state, templates) {
    templates.forEach(template => {
      const templateIndex = state.templates.findIndex((search) => search.id === template.id)
      if (templateIndex === -1) {
        state.templates.push(template)
      } else {
        for (const key in module) {
          Vue.set(state.templates[templateIndex], key, template[key])
        }
      }
    })
  },
  removeTemplate (state, templateId) {
    state.templates.splice(state.templates.findIndex((template) => template.id === templateId), 1)
  },
  updateTemplate (state, template) {
    if (template) {
      state.template = template
    }
  },
  displayClient (state, displayClient) {
    state.displayClient = displayClient
  },
  importRestrictions (state, importRestrictions) {
    state.importRestrictions = importRestrictions
  },
  categoryAvailable (state, categoryAvailable) {
    state.categoryAvailable = categoryAvailable
  },
  contentTemplate (state, changes) {
    for (const key in changes) {
      Vue.set(state.contentTemplate, key, changes[key])
    }
  },
  openAll (state) {
    state.contentTemplate.openedBlocks = ['GraphicalCharter', 'Description', 'UpperBanner', 'LowerBanner']
  },
  closeAll (state) {
    state.contentTemplate.openedBlocks = []
  },
  toggleBLock (state, blockName) {
    if (state.contentTemplate.openedBlocks.indexOf(blockName) === -1) {
      state.contentTemplate.openedBlocks.push(blockName)
    } else {
      state.contentTemplate.openedBlocks.splice(state.contentTemplate.openedBlocks.indexOf(blockName), 1)
    }
  },
  clients (state, client) {
    state.clients = client
  },
  saveFAQ (state, faq) {
    const faqIndex = state.template.faqs.findIndex((search) => search.id === faq.id)
    if (faqIndex === -1) {
      state.template.faqs.push(faq)
    } else {
      for (const key in faq) {
        Vue.set(state.template.faqs[faqIndex], key, faq[key])
      }
    }
    state.faq = null
  },
  deleteFAQ (state, faqId) {
    state.template.faqs.splice(state.template.faqs.findIndex((faq) => faq.id === faqId), 1)
  },
  newFAQ (state) {
    state.faq = {
      name: '',
      templateId: state.template.id,
      content: '',
      active: true
    }
  },
  editFAQ (state, faq) {
    state.faq = faq
  },
  disabledConnexionHelp (state) {
    state.template.blockUpperBanner.connexionHelpCheck = false
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
