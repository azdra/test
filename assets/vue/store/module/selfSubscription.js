import Vue from 'vue'
import { isValidEmail } from '../../self-subscription-hub/login-register/rules-visualizer/email-checker.utils'
import { DateTime } from 'luxon'

const state = () => ({
  user: {},
  userForm: {},
  module: {},
  subjects: {},
  subscriptions: [],
  currentSubscriptionBySubject: {},
  previousSubscriptionBySubject: {},
  selectedSubscriptionBySubject: {},
  filter: {
    text: '',
    theme: '0',
    type: 'both',
    from: null,
    to: null,
    view: 'Search',
    state: 'both',
    sort: 'orderInModule'
  },
  sortOptions: [
    { value: 'orderInModule', text: 'Subjects position' },
    { value: 'date', text: 'Date' },
    { value: 'sessions', text: 'Number of session' },
    { value: 'name', text: 'Alphabetic' },
    { value: 'duration', text: 'Duration' }
  ],
  filterReplay: {
    text: '',
    category: []
  },
  sessionConvocation: null,
  sessionAddToCalendar: null,
  isEmbed: false,
  initialFilteredSubjects: false,
  onlyOneSubject: null
})

const getters = {
  userFormConstraints (state) {
    const constraints = []
    if (!('lastName' in state.userForm) || state.userForm.lastName === null || state.userForm.lastName === '') {
      constraints.lastName = [{ messageKey: 'Field can not be empty', valid: false }]
    }
    if (!('firstName' in state.userForm) || state.userForm.firstName === null || state.userForm.firstName === '') {
      constraints.firstName = [{ messageKey: 'Field can not be empty', valid: false }]
    }
    if (!('company' in state.userForm) || state.userForm.company === null || state.userForm.company === '') {
      constraints.company = [{ messageKey: 'Field can not be empty', valid: false }]
    }
    if (!('email' in state.userForm) || state.userForm.email === null || state.userForm.email === '') {
      constraints.email = [
        { messageKey: 'Field can not be empty', valid: false },
        { messageKey: 'Be in email@domain.com format', valid: false }
      ]
    } else if (!isValidEmail(state.userForm.email)) {
      constraints.email = [{ messageKey: 'Be in email@domain.com format', valid: false }]
    }
    return constraints
  },
  filteredSubjects (state) {
    const searchURL = new URL(window.location)

    const filtered = state.subjects.filter(subject => {
      if (state.filter.view === 'Previous') {
        return subject.id in state.previousSubscriptionBySubject
      } else if (state.filter.view === 'Current') {
        return subject.id in state.currentSubscriptionBySubject
      } else {
        [
          { key: 'text', default: '', type: 'string' },
          { key: 'theme', default: '0', type: 'number' },
          { key: 'type', default: 'both', type: 'string' },
          { key: 'state', default: 'both', type: 'string' },
          { key: 'from', default: '', type: 'string' },
          { key: 'to', default: '', type: 'string' },
          { key: 'sort', default: 'orderInModule', type: 'string' }
        ].forEach(({ key, default: defaultValue, type }) => {
          if (state.initialFilteredSubjects) {
            searchURL.searchParams.delete(key)
          }

          if (state.initialFilteredSubjects && state.filter[key]) {
            searchURL.searchParams.set(key, state.filter[key])
          }

          if (state.filter[key] === defaultValue && searchURL.searchParams.get(key) && state.initialFilteredSubjects) {
            searchURL.searchParams.delete(key)
          }

          if (state.filter[key] === defaultValue && !state.initialFilteredSubjects) {
            if (searchURL.searchParams.get(key)) {
              state.filter[key] = type === 'number' ? parseInt(searchURL.searchParams.get(key)) : searchURL.searchParams.get(key)
            }
          }
        })
        state.initialFilteredSubjects = true

        if (state.filter.text !== '' && subject.name.toLowerCase().indexOf(state.filter.text.toLowerCase()) === -1) {
          return false
        }

        if (
          (state.filter.type === 'face-to-face training' && subject.categorySession !== 4) ||
          (state.filter.type === 'Meeting' && subject.categorySession !== 2) ||
          (state.filter.type === 'Webinar' && subject.categorySession !== 7) ||
          (state.filter.type === 'Virtual classroom' && subject.categorySession !== 1) ||
          (state.filter.type === 'Blended training' && subject.categorySession !== 6)
        ) {
          return false
        }

        if (state.filter.theme !== '0' && state.filter.theme !== subject.theme.id) {
          return false
        }

        if (state.filter.from !== '' && state.filter.to !== '') {
          if (subject.sessions.length === 0) {
            return false
          }

          let hasSession = false
          const from = DateTime.fromISO(state.filter.from).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
          const to = DateTime.fromISO(state.filter.to).set({ hour: 23, minute: 59, second: 59, millisecond: 999 })

          for (const session of subject.sessions) {
            if (DateTime.fromISO(session.dateStart) <= to && DateTime.fromISO(session.dateEnd) >= from) {
              hasSession = true
              break
            }
          }
          return hasSession
        } else if (state.filter.from !== '') {
          if (subject.sessions.length === 0) {
            return false
          }

          let hasSession = false
          const from = DateTime.fromISO(state.filter.from).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
          for (const session of subject.sessions) {
            if (DateTime.fromISO(session.dateEnd) >= from) {
              hasSession = true
              break
            }
          }
          return hasSession
        } else if (state.filter.to !== '') {
          if (subject.sessions.length === 0) {
            return false
          }

          let hasSession = false
          const to = DateTime.fromISO(state.filter.to).set({ hour: 23, minute: 59, second: 59, millisecond: 999 })
          for (const session of subject.sessions) {
            if (DateTime.fromISO(session.dateStart) <= to) {
              hasSession = true
              break
            }
          }
          return hasSession
        }

        if (state.filter.state !== 'both') {
          return (state.filter.state === 'replay' && subject.videoReplay?.published) || (state.filter.state === 'live' && subject.sessions.some(session => DateTime.fromISO(session.dateEnd) > DateTime.now()))
        }

        return true
      }
    })

    window.history.pushState({}, '', searchURL)

    return filtered.sort((a, b) => {
      switch (state.filter.sort) {
        case 'date':
          const sessionAvailableA = a.sessions.filter(session => DateTime.fromISO(session.dateEnd) > DateTime.now())
          const sessionAvailableB = b.sessions.filter(session => DateTime.fromISO(session.dateEnd) > DateTime.now())
          if (sessionAvailableA.length === 0 && sessionAvailableB.length === 0) {
            return a.orderInModule - b.orderInModule
          }

          if (sessionAvailableA.length === 0) {
            return 1
          }

          if (sessionAvailableB.length === 0) {
            return -1
          }
          return DateTime.fromISO(sessionAvailableA[0].dateEnd) - DateTime.fromISO(sessionAvailableB[0].dateEnd) || a.orderInModule - b.orderInModule
        case 'sessions':
          const sessionsAvailableA = a.sessions.filter(session => DateTime.fromISO(session.dateEnd) > DateTime.now()).length
          const sessionsAvailableB = b.sessions.filter(session => DateTime.fromISO(session.dateEnd) > DateTime.now()).length
          return sessionsAvailableB - sessionsAvailableA || a.orderInModule - b.orderInModule
        case 'name':
          return a.name.localeCompare(b.name) || a.orderInModule - b.orderInModule
        case 'duration':
          return b.duration - a.duration || a.orderInModule - b.orderInModule
        default:
          return a.orderInModule - b.orderInModule
      }
    })
  }
}

const actions = {
  initHub ({ commit }, { module, subjects, subscriptions, user }) {
    commit('module', module)
    commit('subjects', subjects)
    commit('subscriptions', subscriptions)
    commit('user', user)
    for (const subscriptionIndex in subscriptions) {
      const subscription = subscriptions[subscriptionIndex]
      if (subscription.running || subscription.scheduled) {
        commit('addCurrentSubscription', subscriptionIndex)
        commit('selectSession', { subject: subscription.subject, session: subscription })
      } else {
        commit('addPreviousSubscription', subscriptionIndex)
      }
    }
    commit('process/loading', false, { root: true })
  },
  newSubscription ({ commit, state }, { subscription }) {
    commit('addSubscription', subscription)
    const subscriptionIndex = state.subscriptions.length - 1
    if (subscription.running || subscription.scheduled) {
      commit('addCurrentSubscription', subscriptionIndex)
    } else {
      commit('addPreviousSubscription', subscriptionIndex)
    }
  }
}

const mutations = {
  view (state, view) {
    state.filter.view = view

    const searchURL = new URL(window.location)
    searchURL.searchParams.set('page', view)
    window.history.pushState({}, '', searchURL)
  },
  module (state, module) {
    state.module = module
  },
  subjects (state, subjects) {
    state.subjects = subjects
  },
  subscriptions (state, subscriptions) {
    state.subscriptions = subscriptions
  },
  addSubscription (state, subscription) {
    state.subscriptions.push(subscription)
  },
  user (state, user) {
    state.user = user
    Vue.set(state.userForm, 'lastName', user.lastName)
    Vue.set(state.userForm, 'firstName', user.firstName)
    Vue.set(state.userForm, 'email', user.email)
    Vue.set(state.userForm, 'company', user.company)
  },
  userField (state, payload) {
    for (const index in payload) {
      state.user[index] = payload[index]
    }
  },
  userFormField (state, payload) {
    for (const index in payload) {
      state.userForm[index] = payload[index]
    }
  },
  unsubscribe (state, subject) {
    Vue.delete(state.currentSubscriptionBySubject, subject.id)
    Vue.delete(state.selectedSubscriptionBySubject, subject.id)
  },
  addCurrentSubscription (state, subscriptionIndex) {
    const subscription = state.subscriptions[subscriptionIndex]
    Vue.set(state.currentSubscriptionBySubject, subscription.subject.id, subscriptionIndex)
  },
  addPreviousSubscription (state, subscriptionIndex) {
    const subscription = state.subscriptions[subscriptionIndex]
    if (!(subscription.subject.id in state.previousSubscriptionBySubject)) {
      Vue.set(state.previousSubscriptionBySubject, subscription.subject.id, [])
    }
    state.previousSubscriptionBySubject[subscription.subject.id].push(subscriptionIndex)
  },
  filter (state, payload) {
    for (const index in payload) {
      state.filter[index] = payload[index]
    }
  },
  selectSession (state, payload) {
    Vue.set(state.selectedSubscriptionBySubject, payload.subject.id, payload.session)
  },
  sessionConvocation (state, sessionId) {
    state.sessionConvocation = sessionId
  },
  sessionAddToCalendar (state, session) {
    state.sessionAddToCalendar = session
  },
  embed (state, isEmbed) {
    state.isEmbed = isEmbed
  },
  onlyOneSubject (state, subject) {
    state.onlyOneSubject = subject
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
