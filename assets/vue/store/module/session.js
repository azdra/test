import Vue from 'vue'
import axios from 'axios'
import { routing } from '../../utils/app.utils'
import moment from 'moment'

export const CATEGORY_SESSION_FORMATION = 1
export const CATEGORY_SESSION_REUNION = 2
export const CATEGORY_SESSION_TEST = 3
export const CATEGORY_SESSION_PRESENTIAL = 4
export const CATEGORY_SESSION_SERVICES = 5
export const CATEGORY_SESSION_MIXTE = 6
export const CATEGORY_SESSION_WEBINAR = 7

export const CONNECTION_TYPE_PRESENTIAL = 'Presential'
export const CONNECTION_TYPE_HYBRID = 'Hybrid'
export const CONNECTION_TYPE_TELEPHONE = 'Telephone'
export const CONNECTION_TYPE_COMPUTER = 'Computer'

export const EVALUATION_KNOWLEDGE = 'knowledge'
export const EVALUATION_COLD = 'cold'
export const EVALUATION_HOT = 'hot'
export const EVALUATION_ACQUIRED = 'acquired'
export const EVALUATION_POSITIONING = 'positioning'
export const EVALUATION_OTHER = 'other'

export const WEBINAR_REPLAY_PLATFORM_VIMEO = '1'
export const WEBINAR_REPLAY_PLATFORM_AC = '2'
export const WEBINAR_REPLAY_PLATFORM_OTHER = '3'
export const WEBINAR_REPLAY_PLATFORM_YOUTUBE = '4'
export const WEBINAR_REPLAY_PLATFORM_DAILYMOTION = '5'

export const EMBED_VIMEO_URL = 'https://player.vimeo.com/video/'
export const EMBED_YOUTUBE_URL = 'https://www.youtube.com/embed/'
export const EMBED_DAILYMOTION_URL = 'https://www.dailymotion.com/embed/video/'

const state = () => ({
  natureValues: {},
  session: {
    name: null,
    category: null,
    dateStart: null,
    duration: null,
    ref: null,
    sessionTest: null,
    automaticSendingTrainingCertificates: true,
    language: null,
    businessNumber: null,
    tags: [],
    additionalTimezones: [],
    information: null,
    origin: null,
    admittedSession: null,
    roomModel: null,
    roomModelName: null,
    urlPath: null,
    licence: null,
    client: {
      convocations: null,
      evaluations: null,
      id: null,
      selfSubscriptionModules: null,
      enableRegistrationPage: null,
      registrationPageTemplates: []
    },
    providerAudio: {
      name: null,
      phoneNumber: null,
      profileIdentifier: null,
      connectionType: '',
      codeAnimator: null,
      codeParticipant: null,
      meetingOneConferenceId: null,
      providerAudioType: null,
      audioConferencing: null
    },
    convocation: {
      id: null
    },
    notificationDate: null,
    dateOfSendingReminders: [''],
    attachments: [],
    sendInvitationEmails: null,
    enableSendRemindersToAnimators: null,
    personalizedContent: null,
    abstractProviderConfigurationHolder: {
      automaticLicensing: false,
      client: {
        id: null,
        convocations: []
      }
    },
    animatorsOnly: [],
    traineesOnly: [],
    evaluations: [],
    participantRoles: [],
    createdBy: null,
    createdAt: null,
    updatedAt: null,
    running: null,
    nature: null,
    objectives: [],
    assistanceType: 0,
    supportType: 0,
    standardHoursActive: null,
    outsideStandardHoursActive: null,
    alertSupport: null,
    restlunch: 0,
    nbrdays: 1,
    location: null,
    subject: {
      id: null
    },
    theme: {
      id: null
    },
    module: {
      id: null
    },
    subscriptionMax: 0,
    alertLimit: 0,
    description: null,
    description2: null,
    replayVideoPlatform: null,
    replayUrlKey: null,
    sendEmailForEdit: true,
    registrationPageTemplate: null,
    registrationPageTemplateId: null,
    registrationPageTemplateBanner: null,
    registrationPageTemplateIllustration: null,
    providerSessionRegistrationPage: {
      places: null,
      description: null,
      description2: null
    },
    replayLayoutPlayButton: false,
    folderDefaultRoom: null
  },
  errors: {
    attachments: null
  },
  loaded: false,
  readOnly: false,
  evaluationsProviderSession: [],
  userActions: {}
})

const getters = {
  getSession: (state) => state.session,
  sessionIsLoaded: (state) => state.loaded,
  natureValues: (state) => state.natureValues,
  getSessionAnimators: (state) => state.session.participantRoles.filter((participantRole) => participantRole.role === 'animator'),
  getActiveTemplate: (state) => {
    return Object.values(state.session.client.registrationPageTemplates)
      .filter(template => template.active === true)
  },
  templateReadOnly: (state) => {
    return state.session.client.enableRegistrationPage !== true || state.session.category !== CATEGORY_SESSION_WEBINAR || state.session.client.registrationPageTemplates.length < 1
  },
  videoPlatform: () => [
    {
      label: 'Vimeo',
      value: WEBINAR_REPLAY_PLATFORM_VIMEO,
      description: 'Platform url or video code',
      url: EMBED_VIMEO_URL
    },
    {
      label: 'Youtube',
      value: WEBINAR_REPLAY_PLATFORM_YOUTUBE,
      description: 'Platform url or video code',
      url: EMBED_YOUTUBE_URL
    },
    {
      label: 'Dailymotion',
      value: WEBINAR_REPLAY_PLATFORM_DAILYMOTION,
      description: 'Platform url or video code',
      url: EMBED_DAILYMOTION_URL
    },
    {
      label: 'Other platforms',
      value: WEBINAR_REPLAY_PLATFORM_OTHER,
      description: 'URL of the other platform'
    }
  ],
  currentPlatform (state) {
    if (this.session.replayVideoPlatform === null) return null
    return this.videoPlatform.find(platform => platform.value === state.session.replayVideoPlatform)
  },
  readableDateStart: (state) => {
    return moment(state.session.dateStart).format('DD/MM/YYYY HH:mm')
  },
  readableDuration: state => {
    const clientLang = state.session.client.defaultLang
    const minutes = state.session.duration % 60
    const hours = Math.floor(state.session.duration / 60)
    return (hours < 10 ? '0' : '') +
        hours +
        (clientLang === 'fr' ? 'h' : ':') +
        (minutes < 10 ? '0' : '') +
        minutes
  }
}

const actions = {}

const mutations = {
  readOnly (state, readOnly) {
    state.readOnly = readOnly
  },
  userActions (state, userActions) {
    Vue.set(state, 'userActions', userActions)
  },
  setSession (state, session) {
    if (session.providerAudio === undefined || session.providerAudio === null) {
      session.providerAudio = {
        name: null,
        phoneNumber: null,
        profileIdentifier: null,
        connectionType: '',
        codeAnimator: null,
        codeParticipant: null,
        meetingOneConferenceId: null,
        providerAudioType: null,
        audioConferencing: null
      }
    }

    if (session.convocation === undefined || session.convocation === null) {
      session.convocation = {
        id: null
      }
    }

    if (session.abstractProviderConfigurationHolder === undefined || session.abstractProviderConfigurationHolder === null) {
      session.abstractProviderConfigurationHolder = {
        client: {
          id: null,
          convocations: []
        }
      }
    }

    state.session = session
    state.loaded = true

    state.session.registrationPageTemplateId = session.registrationPageTemplate ? session.registrationPageTemplate.id : null
  },
  setNatureValues (state, payload) {
    state.natureValues = payload
  },
  addAttachmentsSuccess (state, response) {
    state.session.attachments.push(response.data.message)
  },
  addAttachmentsCatch (state, errorMessage) {
    state.errors.attachments = errorMessage
  },
  removeAttachment (state, attachmentId) {
    const index = state.session.attachments.findIndex((attachment) => {
      return attachment.id === attachmentId
    })
    if (index < 0) {
      return
    }
    axios.get(routing.generate('_session_attachment_delete_session', {
      id: state.session.id,
      attachmentId: attachmentId
    }))
      .then(() => {
        state.session.attachments.splice(index, 1)
      })
  },
  updateOneAnimatorOnly (state, animatorOnly) {
    const index = state.session.animatorsOnly.findIndex((currentAnimator) => {
      return currentAnimator.id === animatorOnly.id
    })
    if (index < 0) {
      return
    }
    state.session.animatorsOnly[index] = animatorOnly
  },
  updateOneTraineeOnly (state, traineeOnly) {
    const index = state.session.traineesOnly.findIndex((currentTrainee) => {
      return currentTrainee.id === traineeOnly.id
    })
    if (index < 0) {
      return
    }
    state.session.traineesOnly[index] = traineeOnly
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
