import Vue from 'vue'

const state = () => ({
  configurationHolder: null,
  contentSettings: {
    openedConfigurationHolders: []
  }
})

const getters = {}

const actions = {}

const mutations = {
  contentSettings (state, changes) {
    for (const key in changes) {
      Vue.set(state.contentSettings, key, changes[key])
    }
  },
  openAll (state, configurationHolders) {
    configurationHolders.forEach(configurationHolder => state.contentSettings.openedConfigurationHolders.push(configurationHolder.id))
  },
  closeAll (state) {
    state.contentSettings.openedConfigurationHolders = []
  },
  openConfigurationHolder (state, configurationHolderId) {
    state.contentSettings.openedConfigurationHolders.push(configurationHolderId)
  },
  closeConfigurationHolder (state, configurationHolderId) {
    state.contentSettings.openedConfigurationHolders.splice(state.contentSettings.openedConfigurationHolders.indexOf(configurationHolderId), 1)
  },
  newConfigurationHolder (state, clientId) {
    state.configurationHolder = {
      name: '',
      clientId: clientId,
      type: 'adobe-connect'
    }
  },
  saveConfigurationHolder (state) {
  },
  updateConfigurationHolder (state, configurationHolder) {
    state.configurationHolder = configurationHolder
  },
  deleteConfigurationHolder (state, message, configurationHolder) {
  },
  editConfigurationHolder (state, configurationHolder) {
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
