const state = () => ({
  errors : {
    form: [],
    fields: {}
  }
})

const getters = {
  errors: state => state.errors
}

const actions = {

}

const mutations = {
  addFieldError (state, payload) {
    const errors = {}
    errors[payload.identifier]  = {}

    if (Object.hasOwnProperty.call(payload, 'name')) {
      if (payload.subFieldName) {
        const subFieldObject = {}
        subFieldObject[payload.subFieldName] = payload.errors
        errors[payload.identifier][payload.name] = subFieldObject
      } else {
        errors[payload.identifier][payload.name]  = payload.errors
      }
    } else {
      errors[payload.identifier]  = payload.errors
    }

    state.errors.fields = Object.assign({}, state.errors.fields, errors)
  },
  eraseError (state) {
    state.errors.fields = {}
  },
  addFormError (state, payload) {
    state.errors.form.push(payload)
  },
  removeSpecError (state, payload) {
    const errors = {}
    errors[payload.identifier]  = {}
    if (Object.hasOwnProperty.call(payload, 'name')) {
      errors[payload.identifier][payload.name] = []
    }
    state.errors.fields = Object.assign({}, state.errors.fields, errors)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
