import _            from 'lodash'
import Vue          from 'vue'
import axios        from 'axios'
import { routing } from '../../utils/app.utils'

export const CATEGORY_SESSION_PRESENTIAL = '4'
export const SITUATION_MIXTE_DISTANTIAL = '1'
export const SITUATION_MIXTE_PRESENTIAL = '2'
export const PRESENTIAL_AUDIO_CATEGORY_TYPE = 'Presential'

const state = () => ({
  clientId: 0,
  clients: {},
  restrictions: {},
  attachments: [],
  errors: {
    attachments: null
  },
  user: null,
  clientData: {
    convocationsListClient: null,
    provider: {
      type: null,
      allRoomModels: null,
      roomModels: null
    },
    evaluations: [],
    categories: [],
    connectors: [],
    organisationDefaultTrainingActionNature: null,
    services: [],
    billingMode: 0,
    remainingCredit: 0,
    totalCredit: 0,
    notificationCreditExhausted: 0,
    modules: [],
    selfRegisteringEnabled: null,
    timezone: null
  },
  sessionToSaved : {
    clientId: null,
    configurationHolder: null,
    title: null,
    reference: null,
    sessionTest: false,
    dateStart: new Date(Date.now() + 86400000).toISOString(),
    duration: '01h00',
    category: null,
    lang: null,
    businessNumber: null,
    tags: [],
    information: null,
    audioCategoryType: null,
    convocation: null,
    notificationDate: null,
    dateOfSendingReminders: [''],
    attachments: [],
    sendInvitationEmails: null,
    enableSendRemindersToAnimators: null,
    automaticSendingTrainingCertificates: true,
    personalizedContent: null,
    accessType: null,
    defaultModelId: null,
    url: null,
    licence: null,
    licenceEmailUserTeams: null,
    participants: [],
    groups: [],
    evaluations: [],
    nature: null,
    objectives: [],
    assistanceType: 0,
    supportType: 0,
    restlunch: 0,
    nbrdays: 1,
    location: null,
    subject: null,
    subscriptionMax: 0,
    alertLimit: 0,
    description: null,
    description2: null,
    replayVideoPlatform: null,
    replayUrlKey: null,
    publishedReplay: false,
    accessParticipantReplay: false,
    replayLayoutPlayButton: false,
    providerAudio: null,
    registrationPageTemplate: null,
    providerSessionRegistrationPage: {
      places: null,
      description: null,
      description2: null
    },
    additionalTimezones: [],
    folderDefaultRoom: null
  },
  stepWizard : {
    current: 'selectType',
    previous: null,
    bullet: {
      selectDate: false,
      createSession: false,
      selectParticipants: false,
      validSession: false
    },
    availableUserSearch: '',
    availableGroupSearch: '',
    userLoaded: false,
    groupLoaded: false,
    warnings: []
  },
  usersToAdd: [],
  groupsToAdd: [],
  users: {},
  natures: {},
  groups: []
})

const getters = {
  sessionToSaved: state => state.sessionToSaved,
  clientData: state => state.clientData,
  natures: state => state.natures,
  availableUsers: (state) => {
    return Object.values(state.users)
      .filter((user) => !_.find(state.sessionToSaved.participants, { user_id: user.id }))
      .sort((a, b) => a.searchableField >= b.searchableField)
  },
  availableGroups: (state) => {
    return Object.values(state.groups)
      .filter((group) => !_.find(state.sessionToSaved.groups, { group_id: group.id }))
      .sort((a, b) => a.name >= b.name)
  },
  filteredUser: (state, getters) => {
    const search = state.stepWizard.availableUserSearch.toLowerCase()
    return getters.availableUsers
      .filter((user) => user.searchableField.indexOf(search) !== -1)
  },
  filteredGroup: (state, getters) => {
    const search = state.stepWizard.availableGroupSearch.toLowerCase()
    return getters.availableGroups
      .filter((group) => group.name.toLowerCase().indexOf(search) !== -1)
  },
  allParticipants: (state) => {
    return state.sessionToSaved.participants
      .sort((a, b) => {
        return state.users[a.user_id].searchableField >= state.users[b.user_id].searchableField
      })
  },
  allGroups: (state) => {
    return state.sessionToSaved.groups
      .sort((a, b) => {
        return a.name >= b.name
      })
  },
  trainees: (state, getters) => {
    return getters.allParticipants.filter(participant => participant.role === 'trainee')
  },
  animators: (state, getters) => {
    return getters.allParticipants.filter(participant => participant.role === 'animator')
  },
  convocations: (state) => {
    return Object.values(state.clientData.convocationsListClient)
      .filter((convocation) => {
        return (convocation.audioCategoryType === state.sessionToSaved.audioCategoryType &&
        convocation.lang === state.sessionToSaved.lang && convocation.connectorType === state.clientData.provider.type) || (convocation.audioCategoryType === PRESENTIAL_AUDIO_CATEGORY_TYPE && state.sessionToSaved.category === CATEGORY_SESSION_PRESENTIAL)
      })
  },
  evaluationsForms: (state) => {
    return Object.values(state.clientData.evaluations)
  },
  activeModules: (state) => {
    return Object.values(state.clientData.modules)
      .filter(module => module.active === true)
  },
  activeTemplates: (state) => {
    return Object.values(state.clientData.registrationPages)
      .filter(template => template.active === true)
  },
  finalNumberSession: (state) => {
    const uniqueUserIds = new Set()

    // Add participant IDs
    state.sessionToSaved.participants.forEach(participant => {
      uniqueUserIds.add(participant.user_id)
    })

    // Add group user IDs
    state.sessionToSaved.groups.forEach(group => {
      group.idsUsers.forEach(userId => {
        uniqueUserIds.add(userId)
      })
    })

    // The total number of unique participants
    return uniqueUserIds.size
  }
}

const actions = {
  loadUsers: ({ commit, state }) => {
    return axios.get(routing.generate('_session_create_from_wizard_get_available_user', { id: state.sessionToSaved.clientId }))
      .then((response) => {
        if (response.data.code === 200) {
          commit('setAvailableUsers', response.data.message)
        }
      })
  },
  loadGroups: ({ commit, state }) => {
    return axios.get(routing.generate('_session_create_from_wizard_get_available_group', { id: state.sessionToSaved.clientId }))
      .then((response) => {
        if (response.data.code === 200) {
          commit('setAvailableGroups', response.data.message)
        }
      })
  },
  addSubscribeUsers: ({ commit, state }) => {
    while (state.usersToAdd.length > 0) {
      commit('addUserToSession', state.usersToAdd.pop())
    }
    while (state.groupsToAdd.length > 0) {
      commit('addGroupToSession', state.groupsToAdd.pop())
    }
  },
  registerLicenceWebex: ({ commit, state }) => {
    for (const userId in state.users) {
      const user = state.users[userId]
      if (user.email === state.sessionToSaved.licence) {
        commit('addUserToSessionAsAnimator', user.id)
        return
      }
    }
  },
  registerLicenceTeamsEvent: ({ commit, state }) => {
    for (const userId in state.users) {
      const user = state.users[userId]
      if (user.email === state.sessionToSaved.licenceEmailUserTeams) {
        commit('addUserToSessionAsAnimator', user.id)
        return
      }
    }
  },
  registerLicenceWebexRest: ({ commit, state }) => {
    for (const userId in state.users) {
      const user = state.users[userId]
      if (user.email === state.sessionToSaved.licence) {
        commit('addUserToSessionAsAnimator', user.id)
        return
      }
    }
  }
}

const mutations = {
  addAttachmentsSuccess (state, response) {
    state.attachments.push(response.data.message)
    state.sessionToSaved.attachments.push(response.data.message.id)
  },
  addAttachmentsCatch (state, errorMessage) {
    state.errors.attachments = errorMessage
  },
  removeAttachment (state, attachmentId) {
    const index = state.sessionToSaved.attachments.indexOf(attachmentId)
    if (index < 0) {
      return
    }
    axios.get(routing.generate('_session_attachment_delete_orphan', { id: attachmentId }))
      .then(() => {
        state.attachments.splice(index, 1)
        state.sessionToSaved.attachments.splice(index, 1)
      })
  },
  setAvailableUsers (state, users) {
    Vue.set(state, 'users', users)
    state.stepWizard.userLoaded = true
  },
  setAvailableGroups (state, groups) {
    Vue.set(state, 'groups', groups)
    state.stepWizard.groupLoaded = true
  },
  addUserToSession (state, userId) {
    const userParticipation = { user_id: userId, role: 'trainee', situationMixte: SITUATION_MIXTE_DISTANTIAL }
    if (state.sessionToSaved.category === CATEGORY_SESSION_PRESENTIAL) {
      userParticipation.situationMixte = SITUATION_MIXTE_PRESENTIAL
    }
    if (_.find(state.sessionToSaved.participants, userParticipation) === undefined) { state.sessionToSaved.participants.unshift(userParticipation) }
  },
  addGroupToSession (state, group) {
    const groupParticipation = { group_id: group.id, name: group.name, numberUsers: group.numberUsers, idsUsers: group.idsUsers, id: group.id }
    if (_.find(state.sessionToSaved.groups, groupParticipation) === undefined) { state.sessionToSaved.groups.unshift(groupParticipation) }
  },
  addUserToSessionAsAnimator (state, userId) {
    const userParticipation = { user_id: userId, role: 'animator', situationMixte: SITUATION_MIXTE_DISTANTIAL }
    if (state.sessionToSaved.category === CATEGORY_SESSION_PRESENTIAL) {
      userParticipation.situationMixte = SITUATION_MIXTE_PRESENTIAL
    }
    if (_.find(state.sessionToSaved.participants, userParticipation) === undefined) { state.sessionToSaved.participants.unshift({ user_id: userId, role: 'animator' }) }
  },
  removeUserFromSession (state, userId) {
    Vue.delete(state.sessionToSaved.participants, state.sessionToSaved.participants.findIndex(participant => participant.user_id === userId))
  },
  removeGroupFromSession (state, groupId) {
    Vue.delete(state.sessionToSaved.groups, state.sessionToSaved.groups.findIndex(group => group.group_id === groupId))
  },
  refreshClientData (state, clientData) {
    for (const key in clientData) {
      Vue.set(state.clientData, key, clientData[key])
    }
  },
  refreshSession (state, session) {
    for (const key in session) {
      Vue.set(state.sessionToSaved, key, session[key])
    }
  },
  updateProviderInClientData (state, provider) {
    state.clientData.provider = provider
  },
  initWizard (state, payload) {
    state.sessionToSaved.clientId = payload.clientId
    state.clientId = payload.clientId
    state.clients = payload.clients
    state.restrictions = payload.restrictions
    state.user = payload.user
    state.natures = payload.sessionNatureValues
  },
  resetWizard (state) {
    state.sessionToSaved = {
      clientId: state.clientId,
      configurationHolder: null,
      title: null,
      reference: null,
      sessionTest: false,
      dateStart: new Date(Date.now() + 86400000).toISOString(),
      duration: '01h00',
      category: null,
      lang: null,
      businessNumber: null,
      tags: [],
      information: null,
      audioCategoryType: null,
      convocation: null,
      notificationDate: null,
      dateOfSendingReminders: [''],
      attachment: null,
      sendInvitationEmails: null,
      enableSendRemindersToAnimators: null,
      personalizedContent: null,
      automaticSendingTrainingCertificates: true,
      accessType: null,
      defaultModelId: null,
      url: null,
      licence: null,
      participants: [],
      groups: [],
      evaluations: [],
      nature: state.clientData.organisationDefaultTrainingActionNature,
      objectives: [],
      assistanceType: 0,
      supportType: 0,
      restlunch: 0,
      nbrdays: 1,
      location: null,
      subject: null,
      subscriptionMax: 0,
      alertLimit: 0,
      description: null,
      description2: null,
      replayVideoPlatform: null,
      replayUrlKey: null,
      publishedReplay: false,
      accessParticipantReplay: false,
      replayLayoutPlayButton: false,
      folderDefaultRoom: null,
      additionalTimezones: []
    }

    state.stepWizard.current = 'selectDate'
    state.stepWizard.previous = null
    state.stepWizard.bullet = {
      selectDate: true,
      createSession: false,
      selectParticipants: false,
      validSession: false
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
