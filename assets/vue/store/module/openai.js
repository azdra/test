const state = () => ({
  openai: {
    billing: [],
    error: null,
    id: null,
    sessionId: null,
    totalAvailable: 0.0,
    totalGranted: 0.0,
    totalPaidAvailable: 0.0,
    totalUsed: 0.0,
    creditThreshold: 5.0
  }
})

const getters = {
  getOpenai: (state) => state.openai
}

const actions = {}

const mutations = {
  data (state, data) {
    state.openai = data
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
