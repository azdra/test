const state = () => ({
  currentUser : {
    aWebexMeetingLicence:false,
    anAdmin:false,
    AManager:false,
    email:'',
    firstName:'',
    fullName:'',
    id:0,
    lastName:'',
    preferredLang:'fr',
    roles: []
  }
})

const getters = {
  getCurrentUser: (state) => state.currentUser
}

const actions = {}

const mutations = {
  setCurrentUser (state, currentUser) {
    state.currentUser = currentUser
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
