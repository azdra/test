import Vue from 'vue'
import { translator } from '../../utils/app.utils'

export const REPLAY_PLATFORM_VIMEO = 'vimeo'
export const REPLAY_PLATFORM_OTHER = 'other'
export const REPLAY_PLATFORM_YOUTUBE = 'youtube'
export const REPLAY_PLATFORM_DAILYMOTION = 'dailymotion'

export const EMBED_VIMEO_URL = 'https://player.vimeo.com/video/'
export const EMBED_YOUTUBE_URL = 'https://www.youtube.com/embed/'
export const EMBED_DAILYMOTION_URL = 'https://www.dailymotion.com/embed/video/'

const state = () => ({
  view: 'List',
  modules: [],
  module: null,
  theme: null,
  subject: null,
  subscriptionMax: 0,
  alertLimit: 0,
  closeSubscriptions: false,
  closeSubscriptionsBeforeNbrDays: 0,
  templateDesign: 'square',
  linkToVideo: null,
  faq: null,
  video: null,
  categoryReplay: null,
  isUpdateVideo: null,
  faqSettings: {
    searchText: '',
    searchStatus: true
  },
  videoSettings: {
    searchText: '',
    searchStatus: true
  },
  contentSettings: {
    openedThemes: [],
    openedSubjects: [],
    search: '',
    includeDisable: false,
    onlyCurrentSession: true
  },
  displayClient: false,
  importRestrictions: false,
  categoryAvailable: [],
  statistics: [],
  clients: {}
})

const getters = {
  videoPlatforms: () => [
    {
      label: 'Vimeo',
      value: REPLAY_PLATFORM_VIMEO,
      description: 'Platform url or video code',
      url: EMBED_VIMEO_URL
    },
    {
      label: 'Youtube',
      value: REPLAY_PLATFORM_YOUTUBE,
      description: 'Platform url or video code',
      url: EMBED_YOUTUBE_URL
    },
    {
      label: 'Dailymotion',
      value: REPLAY_PLATFORM_DAILYMOTION,
      description: 'Platform url or video code',
      url: EMBED_DAILYMOTION_URL
    },
    {
      label: 'Other platforms',
      value: REPLAY_PLATFORM_OTHER,
      description: 'URL of the other platform'
    }
  ],
  filteredThemes (state, getters) {
    const search = new RegExp('.*' + state.contentSettings.search + '.*', 'g')
    return state.module.themes
      .filter(theme =>
        (theme.name.match(search) || theme.subjects.some(subject => getters.filteredSubjectIds.includes(subject.id))) &&
        (state.contentSettings.includeDisable || theme.active === true)
      )
  },
  activeFAQs (state) {
    const search = new RegExp('.*' + state.faqSettings.searchText + '.*', 'g')
    return state.module.faqs
      .filter(faq => {
        if (state.faqSettings.searchStatus && faq.active !== true) {
          return false
        }
        return faq.question.match(search) || faq.answer.match(search)
      })
  },
  activeVideo (state) {
    const search = new RegExp('.*' + state.videoSettings.searchText + '.*', 'g')
    return state.module.videosReplay
      .filter(video => {
        if (state.videoSettings.searchStatus && video.published !== true) {
          return false
        }
        if (video.subjectId) return false
        return video.title.match(search) || video.description.match(search)
      })
  },
  subjects (state) {
    const subjects = []
    state.module.themes.forEach(theme => subjects.push(...theme.subjects))
    return subjects
  },
  filteredSubjects (state, getters) {
    const search = new RegExp('.*' + state.contentSettings.search + '.*', 'g')
    return getters.subjects.filter(subject =>
      (subject.name.match(search) || subject.sessions.some(session => getters.filteredSessionIds.includes(session.id))) &&
      (state.contentSettings.includeDisable || subject.active === true)
    )
  },
  filteredSubjectIds (state, getters) {
    return getters.filteredSubjects.map(subject => subject.id)
  },
  sessions (state, getters) {
    const sessions = []
    getters.subjects.forEach(subject => sessions.push(...subject.sessions))
    return sessions
  },
  filteredSession (state, getters) {
    const search = new RegExp('.*' + state.contentSettings.search + '.*', 'g')
    return getters.sessions.filter(session => session.name.match(search) && (!state.contentSettings.onlyCurrentSession || session.running || session.scheduled))
  },
  filteredSessionIds (state, getters) {
    return getters.filteredSession.map(session => session.id)
  },
  getView (state) {
    return state.view
  }
}

const actions = {}

const mutations = {
  emptyModules (state) {
    // empty modules
    state.modules.splice(0, state.modules.length)
  },
  newModule (state) {
    state.module = {
      name: '',
      description: '',
      domain: '',
      mainColor: '#ffffff',
      secondaryColor: '#ffffff',
      logo: null,
      banner: null,
      companyLabel: 'Company',
      templateDesign: 'square',
      companyPlaceholder: 'Company',
      emailLabel: 'E-mail address',
      emailPlaceholder: 'E-mail address',
      subscriptionMax: 0,
      alertLimit: 0,
      public: false,
      closeSubscriptions: false,
      closeSubscriptionsBeforeNbrDays: 0,
      lang: state.clients[Object.keys(state.clients)[0]].defaultLang,
      client: {
        id: state.clients[Object.keys(state.clients)[0]].id
      },
      personalization: {
        enabledFaq: false,
        faqTitle: 'FAQ',
        faqDescription: null,
        // faqBanner: null,
        // faqBannerPosition: 'center',

        enabledTraining: false,
        trainingDescription: null,
        trainingTitle: 'Trainings',
        trainingBanner: null,
        trainingBannerPosition: 'center',

        enabledReplay: false,
        replayTitle: 'Replay',
        replayDescription: null,
        replayBanner: null,
        replayBannerPosition: 'center',

        enabledSessions: false,
        sessionsTitle: 'My sessions',
        sessionsDescription: null,
        sessionsBanner: null,
        sessionsBannerPosition: 'center'
      }
    }
  },
  updateModules (state, modules) {
    modules.forEach(module => {
      const moduleIndex = state.modules.findIndex((search) => search.id === module.id)
      if (moduleIndex === -1) {
        state.modules.push(module)
      } else {
        for (const key in module) {
          Vue.set(state.modules[moduleIndex], key, module[key])
        }
      }
    })
  },
  removeModule (state, moduleId) {
    state.modules.splice(state.modules.findIndex((module) => module.id === moduleId), 1)
  },
  updateModule (state, module) {
    if (module) {
      module.whiteList = Object.values(module.whiteList)
      module.blackList = Object.values(module.blackList)
      state.module = module
    }
  },
  newTheme (state) {
    state.theme = {
      name: '',
      moduleId: state.module.id,
      code: '',
      description: '',
      active: true
    }
  },
  saveTheme (state, theme) {
    const themeIndex = state.module.themes.findIndex((search) => search.id === theme.id)
    if (themeIndex === -1) {
      state.module.themes.push(theme)
    } else {
      for (const key in theme) {
        Vue.set(state.module.themes[themeIndex], key, theme[key])
      }
    }
    state.theme = null
  },
  updateTheme (state, theme) {
    state.theme = theme
  },
  deleteTheme (state, themeId) {
    state.module.themes.splice(state.module.themes.findIndex((theme) => theme.id === themeId), 1)
  },
  editTheme (state, theme) {
    state.theme = theme
  },
  newFAQ (state) {
    state.faq = {
      name: '',
      moduleId: state.module.id,
      content: '',
      active: true
    }
  },
  newVideo (state) {
    state.isUpdateVideo = false
    state.video = {
      description: '',
      title: translator.trans('Titre of the video'),
      platform: '',
      urlKey: '',
      photoReplay: '',
      moduleId: state.module.id,
      catalogsReplay : [],
      published: true
    }
  },
  updateCategoryReplayList (state, categoryReplay) {
    const newCategoryReplay = JSON.parse(categoryReplay.categoryReplay)
    state.module.catalogsReplay.push(newCategoryReplay)
  },
  updateCategoryReplay (state, categoryReplay) {
    const updatedCategory = JSON.parse(categoryReplay.categoryReplay)
    const indexToUpdate = state.module.catalogsReplay.findIndex(category => category.id === updatedCategory.id)
    if (indexToUpdate !== -1) {
      state.module.catalogsReplay.splice(indexToUpdate, 1, updatedCategory)
    }
  },
  updateCategoryToEdit (state, categoryToEdit) {
    state.categoryReplay = categoryToEdit
  },
  deleteCategory (state, category) {
    const indexToDelete = state.module.catalogsReplay.findIndex(item => item.id === category.category.id)
    if (indexToDelete !== -1) {
      state.module.catalogsReplay.splice(indexToDelete, 1)
    }
  },
  saveFAQ (state, faq) {
    const faqIndex = state.module.faqs.findIndex((search) => search.id === faq.id)
    if (faqIndex === -1) {
      state.module.faqs.push(faq)
    } else {
      for (const key in faq) {
        Vue.set(state.module.faqs[faqIndex], key, faq[key])
      }
    }
    state.faq = null
  },
  saveVideo (state, video) {
    const videoIndex = state.module.videosReplay.findIndex((search) => search.id === video.id)
    if (videoIndex === -1) {
      state.module.videosReplay.push(video)
    } else {
      for (const key in video) {
        Vue.set(state.module.videosReplay[videoIndex], key, video[key])
      }
    }
  },
  updateFAQ (state, faq) {
    state.faq = faq
  },
  updateVideo (state, video) {
    state.isUpdateVideo = true
    state.video = video
  },
  deleteFAQ (state, faqId) {
    state.module.faqs.splice(state.module.faqs.findIndex((faq) => faq.id === faqId), 1)
  },
  deleteVideo (state, videoId) {
    state.module.videosReplay.splice(state.module.videosReplay.findIndex((video) => video.id === videoId), 1)
  },
  editFAQ (state, faq) {
    state.faq = faq
  },
  updateLinkToVideo (state, linkToVideo) {
    state.linkToVideo = linkToVideo
  },
  updateVideoSelected (state, video) {
    state.video = video
  },
  faqSearch (state, search) {
    for (const key in search) {
      Vue.set(state.faqSettings, key, search[key])
    }
  },
  videoSearch (state, search) {
    for (const key in search) {
      Vue.set(state.videoSettings, key, search[key])
    }
  },
  newSubject (state, themeId) {
    state.subject = {
      name: '',
      code: '',
      description: '',
      categorySession: 0,
      duration: null,
      active: true,
      privateAccess: '',
      themeId: themeId,
      moduleId: state.module.id
    }
  },
  saveSubject (state, subject) {
    const themeIndex = state.module.themes.findIndex((search) => search.id === subject.themeId)
    if (themeIndex === -1) {
      return
    }
    const subjectIndex = state.module.themes[themeIndex].subjects.findIndex((search) => search.id === subject.id)
    if (subjectIndex === -1) {
      state.module.themes[themeIndex].subjects.push(subject)
    } else {
      for (const key in subject) {
        Vue.set(state.module.themes[themeIndex].subjects[subjectIndex], key, subject[key])
      }
    }
    state.subject = null
  },
  updateSubject (state, subject) {
    state.subject = subject
  },
  deleteSubject (state, subject) {
    const themeIndex = state.module.themes.findIndex((search) => search.id === subject.themeId)
    state.module.themes[themeIndex].subjects.splice(state.module.themes[themeIndex].subjects.findIndex((search) => search.id === subject.id), 1)
  },
  editSubject (state, subject) {
    state.subject = subject
  },
  updateView (state, view) {
    if (['List', 'Tile'].includes(view)) {
      state.view = view
    }
  },
  displayClient (state, displayClient) {
    state.displayClient = displayClient
  },
  importRestrictions (state, importRestrictions) {
    state.importRestrictions = importRestrictions
  },
  uploadStatistics (state, statistics) {
    state.statistics = statistics
  },
  categoryAvailable (state, categoryAvailable) {
    state.categoryAvailable = categoryAvailable
  },
  contentSettings (state, changes) {
    for (const key in changes) {
      Vue.set(state.contentSettings, key, changes[key])
    }
  },
  openAll (state) {
    state.contentSettings.openedThemes = state.module.themes.map(theme => theme.id)
    const subjects = []
    state.module.themes.forEach(theme => theme.subjects.forEach(subject => subjects.push(subject.id)))
    state.contentSettings.openedSubjects = subjects
  },
  closeAll (state) {
    state.contentSettings.openedThemes = []
    state.contentSettings.openedSubjects = []
  },
  openTheme (state, themeId) {
    state.contentSettings.openedThemes.push(themeId)
    state.contentSettings.openedSubjects.push(...state.module.themes[state.contentSettings.openedThemes.indexOf(themeId)].subjects.map(subject => subject.id))
  },
  closeTheme (state, themeId) {
    state.module.themes[state.contentSettings.openedThemes.indexOf(themeId)].subjects.forEach((subject) => {
      utilsCloseSubject(state, subject.id)
    })
    state.contentSettings.openedThemes.splice(state.contentSettings.openedThemes.indexOf(themeId), 1)
  },
  openSubject (state, subjectId) {
    state.contentSettings.openedSubjects.push(subjectId)
  },
  closeSubject (state, subjectId) {
    utilsCloseSubject(state, subjectId)
  },
  clients (state, client) {
    state.clients = client
  },
  changeView (state, view) {
    state.view = view
  }
}

const utilsCloseSubject = (state, subjectId) => {
  state.contentSettings.openedSubjects.splice(state.contentSettings.openedSubjects.indexOf(subjectId), 1)
}

export const utilsIsAvailableOptions = (element) => {
  return element.type === 'checkbox' || element.type === 'radio' || element.type === 'select'
}

export const utilHumanizeInputType = (type) => {
  switch (type) {
    case 'text':
      return 'One line text boxes'
    case 'email':
      return 'Mail-type text box'
    case 'number':
      return 'Number box'
    case 'date':
      return 'Date'
    case 'checkbox':
      return 'Check box'
    case 'radio':
      return 'Radio button'
    case 'select':
      return 'Dropdown list'
    case 'textarea':
      return 'Multi-line text box'
    case 'phone_system':
    case 'tel':
      return 'Phone number text box'
    case 'firstName':
      return 'One line text boxes'
    case 'lastName':
      return 'One line text boxes'
    case 'company':
      return 'One line text boxes'
    case 'email_system':
      return 'Mail-type text box'
    case 'phone':
      return 'Phone number text box'
    case 'password':
      return 'Password box'
  }
}
export const isDisabledObligationType = (type) => {
  const disabledTypes = new Set(['firstName', 'lastName', 'email_system', 'password'])
  return disabledTypes.has(type)
}

export const canUpdatePlaceholder = (type) => {
  const updatedTypes = new Set(['email_system', 'company'])
  return updatedTypes.has(type)
}

export const splitUrlKeyByPlatform  = (urlKey, platform) => {
  if (platform === REPLAY_PLATFORM_VIMEO) {
    return urlKey.split('/').pop()
  } else if (platform === REPLAY_PLATFORM_YOUTUBE) {
    const regex = /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:[^\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/
    const match = urlKey.match(regex)
    return match && match[1] ? match[1] : urlKey
  } else if (platform === REPLAY_PLATFORM_DAILYMOTION) {
    return urlKey.split('/').pop()
  }
  return urlKey
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
