import _                                 from 'lodash'
import { generateMemberOfSessionObject } from '../../utils/session-members-selector.utils'

const state = () => ({
  users:            [],
  membersOfSession: [],
  currentSelection: [],
  loaded:           false,
  query:            '',
  groups:            [],
  groupsOfSession: [],
  currentGroupSelection: [],
  queryGroup:            ''
})

const getters = {
  getUsers:            (state) => state.users,
  getGroups:            (state) => state.groups,
  getMembersOfSession: (state) => state.membersOfSession,
  getGroupsOfSession: (state) => state.groupsOfSession,
  isLoaded:            (state) => state.loaded,
  getFinalNumberSession: (state) => {
    const uniqueIds = new Set()
    // Add session member emails
    state.membersOfSession.forEach(member => {
      uniqueIds.add(member.user_id)
    })

    // Add group member emails
    state.groupsOfSession.forEach(group => {
      group.idsUsers.forEach(userId => {
        uniqueIds.add(userId)
      })
    })

    // The total number of unique participants
    return uniqueIds.size
  }
}

const actions = {
  passSelectionAsMemberOfSession: ({ commit, state }) => {
    while (state.currentSelection.length > 0) {
      const currentUser = state.currentSelection.pop()
      commit('addMemberToSession', currentUser)
    }
  },
  passSelectionAsGroupsOfSession: ({ commit, state }) => {
    while (state.currentGroupSelection.length > 0) {
      const currentGroup = state.currentGroupSelection.pop()
      commit('addGroupToSession', currentGroup)
    }
  }
}

const mutations = {
  setAvailableUsers (state, users) {
    state.users  = Object.values(users)
    state.loaded = true
  },
  setAvailableGroups (state, groups) {
    state.groups  = Object.values(groups)
    state.loaded = true
  },
  setMembersOfSession (state, users) {
    state.membersOfSession = Array.from(
      Object.values(users),
      (user) => generateMemberOfSessionObject(user)
    )
  },
  setGroupsOfSession (state, groups) {
    state.groupsOfSession = groups
  },
  setQuery (state, query) {
    state.query = query
  },
  setQueryGroup (state, query) {
    state.queryGroup = query
  },
  addMemberToSession (state, user) {
    const userParticipation = { ...user, user_id: user.id, role: 'trainee' }
    if (_.find(state.membersOfSession, userParticipation) === undefined) {
      state.membersOfSession.unshift(userParticipation)
    }
  },
  addGroupToSession (state, group) {
    const groupParticipation = { ...group, group_id: group.id }
    if (_.find(state.groupsOfSession, groupParticipation) === undefined) {
      state.groupsOfSession.unshift(groupParticipation)
    }
  },
  removeUserAsAMemberOfSession (state, userId) {
    state.membersOfSession = [...state.membersOfSession]
    _.remove(state.membersOfSession, { user_id: userId })
  },
  removeGroupAsAGroupOfSession (state, groupId) {
    state.groupsOfSession = [...state.groupsOfSession]
    _.remove(state.groupsOfSession, { group_id: groupId })
  },
  changeRoleOfMemberSession (state, { userId, role }) {
    const userIndex = _.findIndex(state.membersOfSession, { user_id: userId })
    if (userIndex !== -1) {
      state.membersOfSession[userIndex].role = role
    }
  },
  removeParticipantOfGroupSelected (state, groupId) {
    // Identifier les IDs des utilisateurs du groupe
    const groupUserIds = new Set()
    const group = state.groupsOfSession.find(g => g.group_id === groupId)
    if (group) {
      group.idsUsers.forEach(userId => groupUserIds.add(userId))
    }

    // Filtrer les membres de la session pour ne garder que ceux qui ne sont pas dans le groupe spécifié
    state.membersOfSession = state.membersOfSession.filter(member => !groupUserIds.has(member.user_id))
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
