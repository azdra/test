import _                                 from 'lodash'

const state = () => ({
  users:            [],
  memberOfGroup: [],
  currentSelection: [],
  loaded:           false,
  query:            ''
})

const getters = {
  getUsers:            (state) => state.users,
  memberOfGroup: (state) => state.memberOfGroup,
  isLoaded:            (state) => state.loaded
}

const actions = {
  passSelectionAsMemberOfGroup: ({ commit, state }) => {
    while (state.currentSelection.length > 0) {
      const currentMember = state.currentSelection.pop()
      commit('addMemberToGroup', currentMember)
    }
  }
}

const mutations = {
  setMemberOfGroup (state, members) {
    state.memberOfGroup = members
  },
  setUsers (state, users) {
    state.users = users
  },
  setQuery (state, query) {
    state.query = query
  },
  addMemberToGroup (state, member) {
    const memberAdditional = member
    if (_.find(state.memberOfGroup, memberAdditional) === undefined) {
      state.memberOfGroup.unshift(memberAdditional)
    }
  },
  removeMemberAsMemberOfGroup (state, member) {
    const indexToRemove = state.memberOfGroup.indexOf(member)
    if (indexToRemove !== -1) {
      state.memberOfGroup.splice(indexToRemove, 1)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
