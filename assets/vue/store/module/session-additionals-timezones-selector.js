import _                                 from 'lodash'
// import { generateTimezoneOfSessionObject } from '../../utils/session-additionals-timezones-selector.utils'

const state = () => ({
  // users:            [],
  timezonesOfSession: [],
  currentSelection: [],
  loaded:           false,
  query:            ''
})

const getters = {
  // getUsers:            (state) => state.users,
  getTimezonesOfSession: (state) => state.timezonesOfSession,
  isLoaded:            (state) => state.loaded
}

const actions = {
  passSelectionAsTimezoneOfSession: ({ commit, state }) => {
    while (state.currentSelection.length > 0) {
      const currentTimezone = state.currentSelection.pop()
      commit('addTimezoneToSession', currentTimezone)
    }
  }
}

const mutations = {
  setTimezonesOfSession (state, timezones) {
    state.timezonesOfSession = timezones
  },
  setQuery (state, query) {
    state.query = query
  },
  addTimezoneToSession (state, timezone) {
    const timezoneAdditional = timezone
    if (_.find(state.timezonesOfSession, timezoneAdditional) === undefined) {
      state.timezonesOfSession.unshift(timezoneAdditional)
    }
  },
  removeTimezoneAsATimezoneOfSession (state, timezone) {
    const indexToRemove = state.timezonesOfSession.indexOf(timezone)
    if (indexToRemove !== -1) {
      state.timezonesOfSession.splice(indexToRemove, 1)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
