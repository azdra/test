export const ID_CLIENT_IFCAM = 6

const state = () => ({
  client: {
    id: null,
    name: null,
    active: true,
    createdAt: null,
    updatedAt: null,
    billingMode: 0,
    allowedCredit: 0,
    currentSessionCount: 0,
    remainingCredit: 0,
    creditAllocations: [],
    color: '#ffffff',
    evaluations: [],
    preferences: {}
  },
  clientServices: {
    assistanceType: null,
    assistanceDuration: null,
    assistanceStart: null,
    commitment: null,
    emailToNotifyForAssistance: null,
    supportType: null,
    standardHours: null,
    outsideStandardHoursActive: null,
    outsideStandardHoursStartTimeAm: null,
    outsideStandardHoursStartTimePm: null,
    outsideStandardHoursStartTimeLh: null,
    outsideStandardHoursEndTimeLh: null,
    outsideStandardHoursEndTimeAm: null,
    outsideStandardHoursEndTimePm: null,
    outsideSupportType: null,
    timeBeforeSession: null,
    timeSupportDurationSession: null,
    emailToNotifyForSupport: null,
    dedicatedSupport: null,
    mailSupport: null,
    phoneSupport: null,
    dedicatedUserGuide: null,
    personnalizedInfoMailTransactionnal: null,
    alertSupport: null
  }
})

const getters = {
  getClientServices : (state) => state.clientServices
}

const actions = {}

const mutations = {
  setClientServices (state, clientServices) {
    state.clientServices = clientServices
  },
  setClient (state, client) {
    state.client = client
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
