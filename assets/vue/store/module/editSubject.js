import { translator } from '../../utils/app.utils'

const state = () => ({
  subject: {},
  module: {},
  video: null
})

const getters = {
  subject: state => state.subject,
  categoryAvailable: state => state.categoryAvailable,
  module: state => state.module,
  readableDuration: state => {
    const clientLang = state.module.client.defaultLang
    const minutes = state.subject.duration % 60
    const hours = Math.floor(state.subject.duration / 60)
    return (hours < 10 ? '0' : '') +
            hours +
            (clientLang === 'fr' ? 'h' : ':') +
            (minutes < 10 ? '0' : '') +
            minutes
  }
}
const actions = {}
const mutations = {
  subject (state, subject) {
    state.subject = subject
  },
  categoryAvailable (state, categoryAvailable) {
    state.categoryAvailable = categoryAvailable
  },
  module (state, module) {
    state.module = module
  },
  newVideo (state) {
    state.isUpdateVideo = false
    state.video = {
      description: '',
      title: translator.trans('Replay title'),
      platform: '',
      urlKey: '',
      photoReplay: '',
      moduleId: state.module.id,
      subjectId: state.subject.id,
      catalogsReplay : [],
      published: state.subject.futureSessions.length <= 0
    }
  },
  saveVideo (state, video) {
    state.subject.videoReplay = video
  },
  deleteVideo (state, videoId) {
    state.subject.videoReplay = null
  },
  updateVideo (state, video) {
    state.isUpdateVideo = true
    state.video = video
  },
  closeVideo (state) {
    state.video = null
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
