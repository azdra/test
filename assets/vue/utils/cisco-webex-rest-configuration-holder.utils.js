import axios       from 'axios'
import { routing } from './app.utils'

export const PROVIDER_CISCO_WEBEX_REST = 'cisco-webex-rest'
export const CISCO_WEBEX_REST_FORM_COMPONENT = 'CiscoWebexRestConfigurationHolderForm'

export const webexRestLicences = webexRestConfigurationHolderId => {
  if (webexRestConfigurationHolderId === null) { return new Promise(resolve => resolve([])) }

  const route = routing.generate('_webex_rest_licences', { id: webexRestConfigurationHolderId })
  return axios.get(route)
    .then(data => Object.values(data.data.message).sort())
}

export const setIsLicenceWebexRest = formData => {
  if (formData === null) { return new Promise(resolve => resolve([])) }

  return axios
    .post(routing.generate('_user_webex_rest_is_licence'), formData)
}

export const handleWebexRestDefaultDataForTheSessionCreationWizard = (clientData, session, data) => {
  if (data.configurationHolder.licences.length > 0 && !clientData.provider.automaticLicensing) {
    session.licence = data.configurationHolder.licences[0]
  }
  return session
}
