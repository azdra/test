import axios from 'axios'
import _           from 'lodash'
import { routing } from './app.utils'

export const PROVIDER_ADOBE_CONNECT_WEBINAR = 'adobe-connect-webinar'
export const ADOBE_CONNECT_WEBINAR_FORM_COMPONENT = 'AdobeConnectWebinarConfigurationHolderForm'

export const adobeConnectWebinarSharedMeetingTemplate = adobeConnectWebinarConfigurationHolderId => {
  if (adobeConnectWebinarConfigurationHolderId === null) { return new Promise(resolve => resolve([])) }

  return axios.get(routing.generate('_adobe_connect_shared_webinar_templates', { id: adobeConnectWebinarConfigurationHolderId }))
    .then(data => _.sortBy(data.data.message, ['name']))
}

export const adobeConnectWebinarLicences = adobeConnectWebinarConfigurationHolderId => {
  if (adobeConnectWebinarConfigurationHolderId === null) { return new Promise(resolve => resolve([])) }

  return axios.get(routing.generate('_adobe_connect_webinar_licences', { id: adobeConnectWebinarConfigurationHolderId }))
    .then(data => _.sortBy(data.data.message, ['name']))
}

export const adobeConnectWebinarTelephonyProfileList = adobeConnectWebinarConfigurationHolderId => {
  if (adobeConnectWebinarConfigurationHolderId === null) { return new Promise(resolve => resolve([])) }

  return axios.get(routing.generate('_adobe_connect_webinar_audios', { id: adobeConnectWebinarConfigurationHolderId }))
    .then(data => _.sortBy(data.data.message, ['name']))
}

export const adobeConnectWebinarTelephonyProfileUpdateNumber = (adobeConnectWebinarTelephonyProfileId, number) => {
  if (adobeConnectWebinarTelephonyProfileId === null || number === null) { return new Promise(resolve => resolve([])) }

  return axios.post(routing.generate('_adobe_connect_webinar_audios_update_number', { id: adobeConnectWebinarTelephonyProfileId, number: number }))
}

export const adobeConnectWebinarTelephonyProfileUpdateLanguage = (adobeConnectWebinarTelephonyProfileId, language) => {
  if (adobeConnectWebinarTelephonyProfileId === null || language === null) { return new Promise(resolve => resolve([])) }

  return axios.post(routing.generate('_adobe_connect_webinar_audios_update_language', { id: adobeConnectWebinarTelephonyProfileId, language: language }))
}

export const handleAdobeConnectWebinarDefaultDataForTheSessionCreationWizard = (clientData, session, data) => {
  session.accessType = data.configurationHolder.admittedOnRoom
  session.defaultModelId = data.configurationHolder.defaultRoomModel

  return session
}

export const setIsLicenceAdobe = formData => {
  if (formData === null) { return new Promise(resolve => resolve([])) }

  return axios
    .post(routing.generate('_user_adobe_is_licence'), formData)
}
