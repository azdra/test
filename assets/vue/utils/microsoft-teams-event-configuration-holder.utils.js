import axios       from 'axios'
import { routing } from './app.utils'

export const PROVIDER_MICROSOFT_TEAMS_EVENT = 'microsoft-teams-event'
export const MICROSOFT_TEAMS_EVENT_FORM_COMPONENT = 'MicrosoftTeamsEventConfigurationHolderForm'

export const microsoftTeamsEventLicences = microsoftTeamsEventConfigurationHolderId => {
  if (microsoftTeamsEventConfigurationHolderId === null) { return new Promise(resolve => resolve([])) }

  const route = routing.generate('_microsoft_teams_event_licences', { id: microsoftTeamsEventConfigurationHolderId })

  const objectLicences = axios.get(route).then(data => Object.entries(data.data.message).sort((a, b) => (a[0] > b[0]) ? 1 : (b[0] > a[0]) ? -1 : 0))

  return objectLicences
}

export const microsoftTeamsEventLicencesAttributed = microsoftTeamsEventConfigurationHolderId => {
  if (microsoftTeamsEventConfigurationHolderId === null) { return new Promise(resolve => resolve([])) }

  const route = routing.generate('_microsoft_teams_event_licences_attributed', { id: microsoftTeamsEventConfigurationHolderId })

  const objectLicencesAttributed = axios.get(route).then(data => Object.entries(data.data.message).sort((a, b) => (a[0] > b[0]) ? 1 : (b[0] > a[0]) ? -1 : 0))

  return objectLicencesAttributed
}

export const setIsLicenceMicrosoftTeamsEvent = formData => {
  if (formData === null) { return new Promise(resolve => resolve([])) }

  return axios
    .post(routing.generate('_user_microsoft_teams_event_is_licence'), formData)
}

export const handleMicrosoftTeamsEventDefaultDataForTheSessionCreationWizard = (clientData, session, data) => {
  if (data.configurationHolder.licences.length > 0 && !clientData.provider.automaticLicensing) {
    session.licence = data.configurationHolder.licences[0]
  }
  return session
}
