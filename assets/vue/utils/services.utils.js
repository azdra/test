export const ENUM_ASSISTANCE_NO = 0
export const ENUM_ASSISTANCE_ON_DEMAND = 1
export const ENUM_ASSISTANCE_FULL = 2
export const ENUM_ASSISTANCE_TOTAL = 3
export const ENUM_ASSISTANCE_PEDA = 4
export const ENUM_ASSISTANCE_COANIM = 5
export const ENUM_ASSISTANCE_TYPE = 1

export const ENUM_SUPPORT_NO = 0
export const ENUM_SUPPORT_ON_DEMAND = 1
export const ENUM_SUPPORT_LAUNCH = 3
export const ENUM_SUPPORT_TOTAL = 2

export const ENUM_SUPPORT_TYPE = 2

export const TYPE_DIRECT_NEED = 0
export const TYPE_ASSISTANCE = 1
export const TYPE_SUPPORT = 2

export const CLIQ_CHANNEL_ASSIST = 'https://cliq.zoho.com/company/654213497/channels/ext:livecsc'
export const CLIC_CHANNEL_HOTLINER = 'https://cliq.zoho.com/company/654213497/channels/ext:hotlineifcamdifcam'
