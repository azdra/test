// Adding search by full name in addition to email
export const getMemberMatchWithSearchQuery = (users, query) => {
  return users
    .filter((user) => {
      // Check if the email matches the query (case-insensitive)
      const emailMatch = user.email.toLowerCase().includes(query.toLowerCase())

      // Check if the full name matches the query (case-insensitive)
      const fullNameMatch = user.fullName.toLowerCase().includes(query.toLowerCase())

      // Return true if the query matches either the email or the full name
      return emailMatch || fullNameMatch
    })
}
