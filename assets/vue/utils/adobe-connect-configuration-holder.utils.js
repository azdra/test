import axios from 'axios'
import _           from 'lodash'
import { routing } from './app.utils'

export const PROVIDER_ADOBE_CONNECT = 'adobe-connect'
export const ADOBE_CONNECT_FORM_COMPONENT = 'AdobeConnectConfigurationHolderForm'

export const adobeConnectSharedMeetingTemplate = adobeConnectConfigurationHolderId => {
  if (adobeConnectConfigurationHolderId === null) { return new Promise(resolve => resolve([])) }

  return axios.get(routing.generate('_adobe_connect_shared_meeting_templates', { id: adobeConnectConfigurationHolderId, filter: 'meeting' }))
    .then(data => _.sortBy(data.data.message, ['name']))
}

export const adobeConnectLicences = adobeConnectConfigurationHolderId => {
  if (adobeConnectConfigurationHolderId === null) { return new Promise(resolve => resolve([])) }

  return axios.get(routing.generate('_adobe_connect_licences', { id: adobeConnectConfigurationHolderId }))
    .then(data => _.sortBy(data.data.message, ['name']))
}

export const adobeConnectTelephonyProfileList = adobeConnectConfigurationHolderId => {
  if (adobeConnectConfigurationHolderId === null) { return new Promise(resolve => resolve([])) }

  return axios.get(routing.generate('_adobe_connect_audios', { id: adobeConnectConfigurationHolderId }))
    .then(data => _.sortBy(data.data.message, ['name']))
}

export const adobeConnectTelephonyProfileUpdateNumber = (adobeConnectTelephonyProfileId, number) => {
  if (adobeConnectTelephonyProfileId === null || number === null) { return new Promise(resolve => resolve([])) }

  return axios.post(routing.generate('_adobe_connect_audios_update_number', { id: adobeConnectTelephonyProfileId, number: number }))
}

export const adobeConnectTelephonyProfileUpdateLanguage = (adobeConnectTelephonyProfileId, language) => {
  if (adobeConnectTelephonyProfileId === null || language === null) { return new Promise(resolve => resolve([])) }

  return axios.post(routing.generate('_adobe_connect_audios_update_language', { id: adobeConnectTelephonyProfileId, language: language }))
}

export const handleAdobeConnectDefaultDataForTheSessionCreationWizard = (clientData, session, data) => {
  session.accessType = data.configurationHolder.admittedOnRoom
  session.defaultModelId = data.configurationHolder.defaultRoomModel
  session.folderDefaultRoom = data.configurationHolder.folderRoomModel

  return session
}

export const setIsLicenceAdobe = formData => {
  if (formData === null) { return new Promise(resolve => resolve([])) }

  return axios
    .post(routing.generate('_user_adobe_is_licence'), formData)
}
