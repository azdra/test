import axios       from 'axios'
import { routing } from './app.utils'

export const PROVIDER_CISCO_WEBEX = 'cisco-webex'
export const CISCO_WEBEX_FORM_COMPONENT = 'CiscoWebexConfigurationHolderForm'

export const webexLicences = webexConfigurationHolderId => {
  if (webexConfigurationHolderId === null) { return new Promise(resolve => resolve([])) }

  const route = routing.generate('_webex_licences', { id: webexConfigurationHolderId })
  return axios.get(route)
    .then(data => Object.values(data.data.message).sort())
}

export const setIsLicenceWebex = formData => {
  if (formData === null) { return new Promise(resolve => resolve([])) }

  return axios
    .post(routing.generate('_user_webex_is_licence'), formData)
}

export const handleWebexDefaultDataForTheSessionCreationWizard = (clientData, session, data) => {
  if (data.configurationHolder.licences.length > 0 && !clientData.provider.automaticLicensing) {
    session.licence = data.configurationHolder.licences[0]
  }
  return session
}
