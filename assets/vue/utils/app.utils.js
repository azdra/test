import Translator from 'bazinga-translator'
import routes     from '../../../public/js/fos_js_routes.json'
import Routing    from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js'

Routing.setRoutingData(routes)
export const routing = Routing

global.Translator = Translator
require('../../../public/translations/translations/config')
require('../../../public/translations/translations/fr')
export const translator = Translator

export const randomHexString = () => Math.floor(Math.random() * 16777215).toString(16)
