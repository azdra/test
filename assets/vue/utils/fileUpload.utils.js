export const CONVOCATION_MAX_SIZE = 2 * 1024 * 1024 // 20MB

export const IMAGE_FORMATS = ['jpeg', 'png', 'jpg']
export const IMAGE_FORMATS_STRING = IMAGE_FORMATS.join(', ')
export const IMAGE_FORMATS_FORMATTED = IMAGE_FORMATS.map((format) => '.' + format).join(', ')

export const DOCUMENT_FORMATS = IMAGE_FORMATS.concat(['pdf', 'odt', 'ods', 'doc', 'docx', 'txt', 'ppt', 'pptx'])
export const DOCUMENT_FORMATS_STRING = DOCUMENT_FORMATS.join(', ')
export const DOCUMENT_FORMATS_FORMATTED = DOCUMENT_FORMATS.map((format) => '.' + format).join(', ')

export const COMPRESSION_FORMAT = ['zip', 'rar']
export const DATA_FORMATS = ['xls', 'xlsx', 'csv']

export const ALL_FORMATS = [...IMAGE_FORMATS, ...DOCUMENT_FORMATS, ...COMPRESSION_FORMAT, ...DATA_FORMATS]
export const ALL_FORMATS_STRING = ALL_FORMATS.join(', ')
export const ALL_FORMATS_FORMATTED = ALL_FORMATS.map((format) => '.' + format).join(', ')
