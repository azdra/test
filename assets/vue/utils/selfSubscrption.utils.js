
export const getDetailsFromSubject = (clientData, subjectId)  => {
  for (let iModule = 0; iModule < clientData.modules.length; iModule++) {
    for (let iTheme = 0; iTheme < clientData.modules[iModule].themes.length; iTheme++) {
      for (let iSubject = 0; iSubject < clientData.modules[iModule].themes[iTheme].subjects.length; iSubject++) {
        if (clientData.modules[iModule].themes[iTheme].subjects[iSubject].id === subjectId) {
          const details = {}
          details.moduleName = clientData.modules[iModule].name
          details.themeName = clientData.modules[iModule].themes[iTheme].name
          details.subjectName = clientData.modules[iModule].themes[iTheme].subjects[iSubject].name
          return details
        }
      }
    }
  }
  return ''
}
