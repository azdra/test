import axios       from 'axios'
import { routing } from './app.utils'

export const PROVIDER_MICROSOFT_TEAMS = 'microsoft-teams'
export const MICROSOFT_TEAMS_FORM_COMPONENT = 'MicrosoftTeamsConfigurationHolderForm'

export const microsoftTeamsLicences = microsoftTeamsConfigurationHolderId => {
  if (microsoftTeamsConfigurationHolderId === null) { return new Promise(resolve => resolve([])) }

  const route = routing.generate('_microsoft_teams_licences', { id: microsoftTeamsConfigurationHolderId })

  const objectLicences = axios.get(route).then(data => Object.entries(data.data.message).sort((a, b) => (a[0] > b[0]) ? 1 : (b[0] > a[0]) ? -1 : 0))

  return objectLicences
}

export const setIsLicenceMicrosoftTeams = formData => {
  if (formData === null) { return new Promise(resolve => resolve([])) }

  return axios
    .post(routing.generate('_user_microsoft_teams_is_licence'), formData)
}

export const handleMicrosoftTeamsDefaultDataForTheSessionCreationWizard = (clientData, session, data) => {
  if (data.configurationHolder.licences.length > 0 && !clientData.provider.automaticLicensing) {
    session.licence = data.configurationHolder.licences[0]
  }
  return session
}
