import axios                                                from 'axios'
import store                                                from '../store'
import {
  PROVIDER_ADOBE_CONNECT,
  ADOBE_CONNECT_FORM_COMPONENT,
  handleAdobeConnectDefaultDataForTheSessionCreationWizard
} from './adobe-connect-configuration-holder.utils'
import {
  PROVIDER_ADOBE_CONNECT_WEBINAR,
  ADOBE_CONNECT_WEBINAR_FORM_COMPONENT,
  handleAdobeConnectWebinarDefaultDataForTheSessionCreationWizard
} from './adobe-connect-webinar-configuration-holder.utils'
import { routing }                                          from './app.utils'
import {
  PROVIDER_CISCO_WEBEX,
  CISCO_WEBEX_FORM_COMPONENT,
  handleWebexDefaultDataForTheSessionCreationWizard
} from './cisco-webex-configuration-holder.utils'
import {
  PROVIDER_CISCO_WEBEX_REST,
  CISCO_WEBEX_REST_FORM_COMPONENT,
  handleWebexRestDefaultDataForTheSessionCreationWizard
} from './cisco-webex-rest-configuration-holder.utils'
import {
  MICROSOFT_TEAMS_FORM_COMPONENT,
  PROVIDER_MICROSOFT_TEAMS,
  handleMicrosoftTeamsDefaultDataForTheSessionCreationWizard
} from './microsoft-teams-configuration-holder.utils'
import {
  MICROSOFT_TEAMS_EVENT_FORM_COMPONENT,
  PROVIDER_MICROSOFT_TEAMS_EVENT,
  handleMicrosoftTeamsEventDefaultDataForTheSessionCreationWizard
} from './microsoft-teams-event-configuration-holder.utils'
import {
  WHITE_CONNECTOR_FORM_COMPONENT,
  PROVIDER_WHITE_CONNECTOR,
  handleWhiteConnectorDefaultDataForTheSessionCreationWizard
} from './white-configuration-holder.utils'

export const createAnAdobeConnectRequest = data => createAConfigurationHolderRequest('adobe_connect_configuration_holder', data)
export const createAnAdobeConnectWebinarRequest = data => createAConfigurationHolderRequest('adobe_connect_webinar_configuration_holder', data)

export const ENUM_NO = 0
export const ENUM_ON_DEMAND = 1
export const ENUM_FULL = 2
export const ENUM_TOTAL = 3
export const ENUM_PEDA = 4
export const ENUM_COANIM = 5

export const ENUM_CLIENT_SUPPORT_NO = 0
export const ENUM_CLIENT_SUPPORT_ON_DEMAND = 1
export const ENUM_CLIENT_SUPPORT_TOTAL = 2
export const ENUM_CLIENT_SUPPORT_LAUNCH = 3

export const ENUM_SUPPORT_NO = 0
export const ENUM_SUPPORT_ON_DEMAND = 1
export const ENUM_SUPPORT_LAUNCH = 2
export const ENUM_SUPPORT_TOTAL = 3

export const ENUM_SUPPORT_FOR_START_SESSION = 0
export const ENUM_SUPPORT_FOR_ALL_SESSION = 1

export const createAConfigurationHolderRequest = (formName, data) =>  {
  const request =  new URLSearchParams()
  for (const key in data) {
    if (key === 'id' || key === 'licences') { continue }

    let value =  data[key]

    if (value === null) { value = '' }

    request.append(`${formName}[${key}]`, value)
  }

  return request
}

export const sendConfigurationHolderRequest = (id, data) => {
  return axios.post(`/configuration-holder/${id}/update`, data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
}

export const providerToComponentName = provider =>  {
  switch (provider) {
    case PROVIDER_ADOBE_CONNECT:
      return ADOBE_CONNECT_FORM_COMPONENT
    case PROVIDER_ADOBE_CONNECT_WEBINAR:
      return ADOBE_CONNECT_WEBINAR_FORM_COMPONENT
    case PROVIDER_CISCO_WEBEX:
      return CISCO_WEBEX_FORM_COMPONENT
    case PROVIDER_CISCO_WEBEX_REST:
      return CISCO_WEBEX_REST_FORM_COMPONENT
    case PROVIDER_MICROSOFT_TEAMS:
      return MICROSOFT_TEAMS_FORM_COMPONENT
    case PROVIDER_MICROSOFT_TEAMS_EVENT:
      return MICROSOFT_TEAMS_EVENT_FORM_COMPONENT
    case PROVIDER_WHITE_CONNECTOR:
      return WHITE_CONNECTOR_FORM_COMPONENT
    default:
      throw new Error('Unknown provider ' + provider)
  }
}

export const providerToConfigurationHolderProps = provider => {
  switch (provider.provider) {
    case PROVIDER_ADOBE_CONNECT:
    case PROVIDER_ADOBE_CONNECT_WEBINAR:
    case PROVIDER_CISCO_WEBEX:
      return {
        connectorIn: provider,
        identifier:  provider.identifier
      }
    case PROVIDER_CISCO_WEBEX_REST:
      return {
        connectorIn: provider,
        identifier:  provider.identifier
      }
    case PROVIDER_MICROSOFT_TEAMS:
      return {
        connectorIn: provider,
        identifier:  provider.identifier
      }
    case PROVIDER_MICROSOFT_TEAMS_EVENT:
      return {
        connectorIn: provider,
        identifier:  provider.identifier
      }
    case PROVIDER_WHITE_CONNECTOR:
      return {
        connectorIn: provider,
        identifier: provider.identifier
      }
    default:
      return {}
  }
}

export const handleClientDefaultDataForTheSessionCreationWizard = clientId => {
  return axios.post(routing.generate('_session_choice_client_filter'), { clientId, dateStart: store.getters['sessionWizard/sessionToSaved'].dateStart, duration: store.getters['sessionWizard/sessionToSaved'].duration })
    .then(response => {
      let session = {}
      const clientData = {}

      const data = response.data

      clientData.provider = data.configurationHolder
      clientData.convocationsListClient = data.convocations
      clientData.evaluations = JSON.parse(data.client.evaluations)
      clientData.organisationDefaultTrainingActionNature = data.client.organisationDefaultTrainingActionNature
      clientData.services = data.client.services
      clientData.billingMode = data.client.billingMode
      clientData.remainingCredit = data.client.remainingCredit
      clientData.allowedCredit = data.client.allowedCredit
      clientData.categories = data.client.categories
      clientData.connectors = data.client.connectors
      clientData.isSendAnAttestationAutomatically = data.client.isSendAnAttestationAutomatically
      clientData.notificationCreditExhausted = data.client.notificationCreditExhausted
      clientData.modules = data.client.modules
      clientData.selfRegisteringEnabled = data.client.selfRegisteringEnabled
      clientData.enabledSalesForce = data.client.enabledSalesForce
      clientData.registrationPageEnabled = data.client.registrationPageEnabled
      clientData.registrationPages = data.client.registrationPages
      clientData.timezone = data.client.timezone

      session.configurationHolder = data.configurationHolder.id
      session.lang = data.client.defaultLang
      session.reference = data.reference
      // TODO Add default field on client
      // session.audioCategoryType = 'Hybrid'

      session.notificationDate = data.notificationDate
      session.dateOfSendingReminders = data.datesReminders
      session.nature = clientData.organisationDefaultTrainingActionNature
      session.standardHoursActive = clientData.services.standardHours.active
      session.outsideStandardHoursActive = clientData.services.outsideStandardHours.active

      switch (clientData.services.assistanceType) {
        case ENUM_NO:
          session.assistanceType = ENUM_NO
          break
        case ENUM_ON_DEMAND:
          session.assistanceType = ENUM_NO
          break
        case ENUM_FULL:
          session.assistanceType = ENUM_FULL
          break
        default:
          session.assistanceType = ENUM_NO
          break
      }

      switch (clientData.services.supportType) {
        case ENUM_CLIENT_SUPPORT_NO:
          session.supportType = ENUM_SUPPORT_NO
          break
        case ENUM_CLIENT_SUPPORT_ON_DEMAND:
          session.supportType = ENUM_SUPPORT_NO
          break
        case ENUM_CLIENT_SUPPORT_LAUNCH:
          session.supportType = ENUM_SUPPORT_LAUNCH
          break
        case ENUM_CLIENT_SUPPORT_TOTAL:
          if (clientData.services.outsideStandardHours.active && clientData.services.outsideSupportType === ENUM_SUPPORT_FOR_START_SESSION) {
            session.supportType = ENUM_SUPPORT_LAUNCH
          } else if (clientData.services.outsideStandardHours.active && clientData.services.outsideSupportType === ENUM_SUPPORT_FOR_ALL_SESSION) {
            session.supportType = ENUM_SUPPORT_TOTAL
          } else {
            if (clientData.services.standardHours.active) {
              session.supportType = ENUM_SUPPORT_TOTAL
            }
          }
          break
        default:
          session.supportType = ENUM_SUPPORT_NO
          break
      }

      switch (data.configurationHolder.type) {
        case PROVIDER_ADOBE_CONNECT:
          session = handleAdobeConnectDefaultDataForTheSessionCreationWizard(clientData, session, data)
          break
        case PROVIDER_ADOBE_CONNECT_WEBINAR:
          session = handleAdobeConnectWebinarDefaultDataForTheSessionCreationWizard(clientData, session, data)
          break
        case PROVIDER_CISCO_WEBEX:
          session = handleWebexDefaultDataForTheSessionCreationWizard(clientData, session, data)
          break
        case PROVIDER_CISCO_WEBEX_REST:
          session = handleWebexRestDefaultDataForTheSessionCreationWizard(clientData, session, data)
          break
        case PROVIDER_MICROSOFT_TEAMS:
          session = handleMicrosoftTeamsDefaultDataForTheSessionCreationWizard(clientData, session, data)
          break
        case PROVIDER_MICROSOFT_TEAMS_EVENT:
          session = handleMicrosoftTeamsEventDefaultDataForTheSessionCreationWizard(clientData, session, data)
          break
        case PROVIDER_WHITE_CONNECTOR:
          session = handleWhiteConnectorDefaultDataForTheSessionCreationWizard(clientData, session, data)
          break
      }

      store.commit('sessionWizard/refreshClientData', clientData)
      return store.commit('sessionWizard/refreshSession', session)
    })
}
