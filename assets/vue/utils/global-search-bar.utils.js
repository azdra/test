import { routing } from './app.utils'
import axios from 'axios'

export const launchTheGlobalSearch = (globalSearchValue, allowOutdated) => {
  return axios.get(routing.generate('_main_global_search', { q: globalSearchValue, 'allow-outdated': allowOutdated }))
}
