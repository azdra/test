import axios from 'axios'
import store from '../store'
import { routing } from './app.utils'

export const updateProviderConfigurationHolder = (clientId, providerConfigurationHolders)  => {
  const path = routing.generate('_client_update_configuration_holders', { id: clientId })
  return axios
    .post(path, providerConfigurationHolders)
    .catch(error => {
      if (!Object.prototype.hasOwnProperty.call(error.response.data, 'message')) {
        this.addFormError('An error occurred')
      } else {
        const fieldsErrors = error.response.data.message.data
        Object.keys(fieldsErrors).forEach(key => store.commit('formError/addFieldError', {
          identifier: key,
          errors:     fieldsErrors[key]
        }))
      }
    })
}
