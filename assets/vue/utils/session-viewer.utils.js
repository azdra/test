export const SEARCH_PARAM_VIEWER = 'viewer'

/**
 * @doc https://developer.mozilla.org/fr/docs/Web/API/URLSearchParams
 */
export const parseQueryParam = () => {
  return new URLSearchParams(window.location.search)
}

export const getSearchParam = (param) => {
  return parseQueryParam().get(param)
}

export const getViewer = () => {
  switch (getSearchParam(SEARCH_PARAM_VIEWER)) {
    case 'calendar': return 'SessionsCalendar'
    default: return 'SessionsList'
  }
}

export const filterSession = (sessions, connectorSelected) => {
  return sessions.filter((session) => session.abstractProviderConfigurationHolder.name === connectorSelected || connectorSelected === null)
}
