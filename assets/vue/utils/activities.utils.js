import axios        from 'axios'
import _            from 'lodash'
import { DateTime } from 'luxon'
import { routing }  from './app.utils'

export const mergeActivities = (initialData, data) => {
  return _.orderBy(_.concat(initialData, data), 'createdAt', 'desc')
}

export const retrieveLatestActivity = (activities) => {
  return _.last(activities)
}

export const retrieveDaysOfActivities = (activities) => {
  return _.orderBy(_.uniqBy(activities, 'dayOfCreation'), 'dayOfCreation', 'desc')
}

export const getNextActivities = (route, params) => {
  return axios.get(routing.generate(route, params))
    .then(response => response.data)
}

export const activityTypeToIcon = (type) => {
  switch (type) {
    case 'notification': return 'edit-filter'
    case 'information': return 'info'
    case 'alert': return 'alerte-info'
    case 'import_or_export': return 'add-filter'
    case 'error': return 'minus-filter'
    case 'delete': return 'minus-filter'
    default: return type
  }
}

export const activityTypeToLabel = (type) => {
  switch (type) {
    case 'import_or_export': return 'Importing / Exporting'
    default: return _.capitalize(type)
  }
}

export const filterActivities = (activity, filters, currentUser) => {
  const matchWithFilters = [true]

  Object.keys(filters)
    .forEach((filter) => {
      const filterValue = filters[filter]
      const valueIsEmpty = _.isEmpty(filterValue)
      const excludeFilter = ['from', 'to', 'message', 'severity']

      if (filter === 'myActivity') {
        if (filterValue) {
          if (currentUser !== null && activity.user !== null) {
            matchWithFilters.push(activity.user.email === currentUser.email)
          } else {
            matchWithFilters.push(false)
          }
        }
      }

      if (!valueIsEmpty) {
        if (excludeFilter.indexOf(filter) === -1) {
          matchWithFilters.push(activity[filter] === filterValue)
        }

        if (filter === 'severity') {
          matchWithFilters.push(
            filterValue.indexOf(activity.severity) !== -1
          )
        }

        if (filter === 'message') {
          const inMessage = activity.message.toLowerCase().indexOf(filterValue.toLowerCase()) > -1
          const inActivityUser = (activity.user !== null)
            ? activity.user.fullName.toLowerCase().indexOf(filterValue.toLowerCase()) > -1
            : false

          matchWithFilters.push(inMessage || inActivityUser)
        }

        if (filter === 'from') {
          const activityDate = DateTime.fromISO(activity.createdAt).toUTC()
          const filterFromDate = DateTime.fromISO(filterValue)
            .toUTC()
            .set({ hour: 0, minute: 0, second: 0 })
          matchWithFilters.push(activityDate >= filterFromDate)
        }

        if (filter === 'to') {
          const activityDate = DateTime.fromISO(activity.createdAt).toUTC()
          const filterFromDate = DateTime.fromISO(filterValue)
            .toUTC()
            .set({ hour: 23, minute: 59, second: 59 })
          matchWithFilters.push(activityDate <= filterFromDate)
        }
      }
    })

  return _.reduce(matchWithFilters, (all, current) => all && current)
}

export const availableTypes = [
  'notification',
  'information',
  'alert',
  'error'
]

export const availableOrigins = [
  'UserInterface',
  'Cron',
  'Import',
  'Export'
]

export const availableActions = [
  'ParticipantAdd',
  'SessionCancel',
  'SessionCreation',
  'SessionUncancel',
  'ImportSessions',
  'ImportUsers',
  'SessionUpdate',
  'UserUpdate',
  'SessionDelete',
  'UserDeleted'
]

export const activityOriginToLabel = (origin) => {
  switch (origin) {
    case 'UserInterface': return 'Interface'
    case 'Cron': return 'The system'
    case 'Import': return 'The import'
    case 'Export': return 'The export'
    default: return _.capitalize(origin)
  }
}
export const activityActionToLabel = (action) => {
  switch (action) {
    case 'ImportSessions': return 'Importing sessions'
    case 'ImportUsers': return 'Importing users'
    case 'SessionCreation': return 'Session creation'
    case 'SessionUpdate': return 'Session update'
    case 'SessionDelete': return 'Session deletion'
    case 'SessionCancel': return 'Session cancel'
    case 'SessionUncancel': return 'Session uncancel'
    case 'UserUpdate': return 'User update'
    case 'UserDeleted': return 'User deleted'
    case 'ParticipantAdd': return 'Participant add'
    default: return _.capitalize(action)
  }
}
