// import axios       from 'axios'
import _           from 'lodash'
import store       from '../store'
import moment from 'moment-timezone'
// import { routing } from './app.utils'

const momentTimezone = moment.default || moment

export const getTimezonesNotRegisteredOnSession = (additionalsTimezonesOfSession) => {
  return store.getters['sessionAdditionalsTimezonesSelector/getTimezones']
    .filter((timezone) => !_.find(additionalsTimezonesOfSession, { timezone: timezone.value }))
    .sort((a, b) => a.searchableField >= b.searchableField)
}

export const getTimezonesNotRegisteredOnSessionAndMatchWithSearchQuery = (additionalsTimezonesOfSession, query) => {
  return additionalsTimezonesOfSession
    .filter((timezone) => timezone.label.toLowerCase().includes(query.toLowerCase()))
}

export const adjustDateForTimezone = (dateString, timezone, language) => {
  const date = new Date(dateString)
  if (language === 'fr') {
    return momentTimezone.tz(date, timezone).format('HH:mm (DD/MM/YYYY)')
  } else {
    return momentTimezone.tz(date, timezone).format('HH:mm (MM/DD/YYYY)')
  }
}

export const calculateTimeDifference = (timezone) => {
  const timezoneOffsetMinutes = momentTimezone.tz(timezone).utcOffset()

  const hours = Math.floor(timezoneOffsetMinutes / 60)
  const minutes = timezoneOffsetMinutes % 60

  const sign = timezoneOffsetMinutes < 0 ? '-' : '+'

  return `GMT ${sign}${Math.abs(hours).toString().padStart(2, '0')}:${Math.abs(minutes).toString().padStart(2, '0')}`
}

export const generateTimezoneOfSessionObject = (timezone) => {
  return {
    timezone_value:   timezone.value,
    timezone_label:   timezone.label
  }
}
