import axios       from 'axios'
import _           from 'lodash'
import store       from '../store'
import { routing } from './app.utils'

export const getUsersNotRegisteredOnSession = (membersOfSession) => {
  return store.getters['sessionMembersSelector/getUsers']
    .filter((user) => !_.find(membersOfSession, { user_id: user.id }))
    .sort((a, b) => a.searchableField >= b.searchableField)
}

export const getUsersNotRegisteredOnSessionAndMatchWithSearchQuery = (membersOfSession, query) => {
  return getUsersNotRegisteredOnSession(membersOfSession)
    .filter((user) => user.searchableField.toLowerCase().includes(query.toLowerCase()))
}

export const retrieveUser = (userId) => {
  return _.find(store.getters['sessionMembersSelector/getUsers'], { id: userId })
}

export const findAllUserForAClient = (clientId) => {
  const route = routing.generate('_session_create_from_wizard_get_available_user', { id: clientId })
  return axios.get(route)
    .then((response) => {
      if (response.data.code === 200) {
        return response.data.message
      }
    })
}

export const generateMemberOfSessionObject = (user) => {
  return {
    user_id:   user.participant.userId,
    role:      user.role,
    lastName:  user.participant.lastName,
    firstName: user.participant.firstName,
    email:     user.email
  }
}
