import _           from 'lodash'
import store       from '../store'

export const getGroupsNotRegisteredOnSession = (groupsOfSession) => {
  return store.getters['sessionMembersSelector/getGroups']
    .filter((group) => !_.find(groupsOfSession, { group_id: group.id }))
    .sort((a, b) => a.name >= b.name)
}

export const getGroupsNotRegisteredOnSessionAndMatchWithSearchQuery = (groupsOfSession, query) => {
  return getGroupsNotRegisteredOnSession(groupsOfSession).filter((group) => group.name.toLowerCase().includes(query.toLowerCase()))
}

export const retrieveGroup = (groupId) => {
  return _.find(store.getters['sessionMembersSelector/getGroups'], { id: groupId })
}
