import axios from 'axios'
import { routing } from './app.utils'

export const addNewEvaluationsForm = (clientId, survey) => {
  return axios.post(routing.generate('_evaluation_save', { client: clientId, survey }))
}

export const deleteEvaluationsForm = (clientId, survey) => {
  return axios.delete(routing.generate('_evaluation_delete', { id: survey.id }))
}

export const sendLinkToEvaluationFormByEmailToSelectedParticipant = (id, participant) => {
  return axios.post(routing.generate('_send_email_evaluation', { id: id, participant }))
}

export const sendLinkToSignatureFormByEmailToSelectedParticipant = (id, participant) => {
  return axios.post(routing.generate('_send_email_signature', { id:id, participant }))
}

export const getSurveyInfoFromPublicAPI = (surveyId) => {
  return axios.get('https://api.surveyjs.io/public/Survey/getSurvey?surveyId=' + surveyId)
}
