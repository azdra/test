import { PROVIDER_MICROSOFT_TEAMS } from '../utils/microsoft-teams-configuration-holder.utils'
import { app } from '../utils/utils'

export const create = initialData  => {
  const inputObject = initialData ?? {}

  return Object.assign({
    id: null,
    client: null,
    domain: null,
    username: 'mlsm@live-session.fr',
    password: null,
    provider: PROVIDER_MICROSOFT_TEAMS,
    identifier: app.randomHexString(),
    organizerEmail: '',
    scope: '',
    grantType: '',
    organizerUniqueIdentifier: '',
    tokenEndPoint: '',
    licences: null,
    tenantId: null,
    clientId: null,
    clientSecret: null
  }, inputObject)
}
