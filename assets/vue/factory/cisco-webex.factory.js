import { PROVIDER_CISCO_WEBEX } from '../utils/cisco-webex-configuration-holder.utils'
import { app } from '../utils/utils'

export const create = initialData  => {
  const inputObject = initialData ?? {}

  return Object.assign({
    id: null,
    client: null,
    domain: null,
    username: 'mlsm@live-session.fr',
    password: null,
    provider: PROVIDER_CISCO_WEBEX,
    optionJoinTeleconfBeforeHost: false,
    optionOpenTime: 30,
    identifier: app.randomHexString(),
    licences: null
  }, inputObject)
}
