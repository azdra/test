import { PROVIDER_WHITE_CONNECTOR } from '../utils/white-configuration-holder.utils'
import { app } from '../utils/utils'

export const create = initialData  => {
  const inputObject = initialData ?? {}

  return Object.assign({
    id: null,
    client: null,
    domain: null,
    username: 'mlsm@live-session.fr',
    password: null,
    provider: PROVIDER_WHITE_CONNECTOR,
    identifier: app.randomHexString(),
    organizerEmail: '',
    scope: '',
    grantType: '',
    organizerUniqueIdentifier: '',
    tokenEndPoint: ''
  }, inputObject)
}
