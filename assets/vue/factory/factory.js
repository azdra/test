import { PROVIDER_ADOBE_CONNECT } from '../utils/adobe-connect-configuration-holder.utils'
import * as adobeConnectFactory   from './adobe-connect.factory'
import { PROVIDER_ADOBE_CONNECT_WEBINAR } from '../utils/adobe-connect-webinar-configuration-holder.utils'
import * as adobeConnectWebinarFactory   from './adobe-connect-webinar.factory'
import { PROVIDER_CISCO_WEBEX } from '../utils/cisco-webex-configuration-holder.utils'
import * as ciscoWebexFactory   from './cisco-webex.factory'
import { PROVIDER_CISCO_WEBEX_REST } from '../utils/cisco-webex-rest-configuration-holder.utils'
import * as ciscoWebexRestFactory   from './cisco-webex-rest.factory'
import { PROVIDER_MICROSOFT_TEAMS } from '../utils/microsoft-teams-configuration-holder.utils'
import * as microsoftTeamsFactory from './microsoft-teams.factory'
import { PROVIDER_MICROSOFT_TEAMS_EVENT } from '../utils/microsoft-teams-event-configuration-holder.utils'
import * as microsoftTeamsEventFactory from './microsoft-teams-event.factory'
import { PROVIDER_WHITE_CONNECTOR } from '../utils/white-configuration-holder.utils'
import * as whiteConnectorFactory from './white-connector.factory'

export const createProviderConfigurationHolder = (provider, configurationHolder = {}) => {
  switch (provider) {
    case PROVIDER_ADOBE_CONNECT:
      return adobeConnectFactory.create(configurationHolder)
    case PROVIDER_ADOBE_CONNECT_WEBINAR:
      return adobeConnectWebinarFactory.create(configurationHolder)
    case PROVIDER_CISCO_WEBEX:
      return ciscoWebexFactory.create(configurationHolder)
    case PROVIDER_CISCO_WEBEX_REST:
      return ciscoWebexRestFactory.create(configurationHolder)
    case PROVIDER_MICROSOFT_TEAMS:
      return microsoftTeamsFactory.create(configurationHolder)
    case PROVIDER_MICROSOFT_TEAMS_EVENT:
      return microsoftTeamsEventFactory.create(configurationHolder)
    case PROVIDER_WHITE_CONNECTOR:
      return whiteConnectorFactory.create(configurationHolder)

    default:
      throw new Error('Unknown provider ' + provider)
  }
}
