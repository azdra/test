import moment                       from 'moment'
import { PROVIDER_ADOBE_CONNECT }   from '../utils/adobe-connect-configuration-holder.utils'
import {  app } from '../utils/utils'

export const create = initialData  => {
  const inputObject = initialData ?? {}

  if (Object.hasOwnProperty.call(inputObject, 'baseUrl')) {
    inputObject.baseUrl = inputObject.baseUrl.replace('.adobeconnect.com', '')
  }

  if (Object.hasOwnProperty.call(inputObject, 'birthday')) {
    inputObject.birthday = moment(inputObject.birthday).format('YYYY-MM-DDTHH:mm')
  }

  return Object.assign({
    id: null,
    client: null,
    name: null,
    subdomain: null,
    username: 'mlsm@live-session.fr',
    password: null,
    defaultConnexionType: 'view-hidden',
    birthday: moment().format('YYYY-MM-DDTHH:mm'),
    licenceType: 1,
    defaultRoom: null,
    folderDefaultRoom: null,
    standardViewOnOpeningRoom: true,
    licences: null,
    provider: PROVIDER_ADOBE_CONNECT,
    identifier: app.randomHexString()
  }, inputObject)
}
