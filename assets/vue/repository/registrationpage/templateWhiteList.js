import axios       from 'axios'
import store       from '../../store'
import { routing, translator } from '../../utils/app.utils'

export const addNewWhiteListeEntry = (templateId, mailEntry) => {
  store.commit('process/initSaving', true)
  axios.post(routing.generate('_new_allowed_template_white_list', { id: templateId, allowedMail: mailEntry }))
    .then(response => {
      if (typeof response.data.registrationPageTemplateWhiteList === 'object' && !Array.isArray(response.data.registrationPageTemplateWhiteList)) {
        // Si registrationPageTemplateWhiteList est un objet, le convertir en tableau
        response.data.registrationPageTemplateWhiteList = Object.values(response.data.registrationPageTemplateWhiteList)
      }
      store.commit('process/endSaving', false)
      store.commit('template/updateTemplate',  response.data)
    })
    .catch(error => {
      const payload = { error: translator.trans('The email could not be saved for an unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}

export const deleteWhiteListeEntry = (moduleId, BlackListeEntryId) => {
  store.commit('process/initSaving', true)
  axios.post(routing.generate('_delete_allowed_template_white_list', { id: moduleId, whitelistId: BlackListeEntryId }))
    .then(response => {
      if (typeof response.data.registrationPageTemplateWhiteList === 'object' && !Array.isArray(response.data.registrationPageTemplateWhiteList)) {
        // Si registrationPageTemplateWhiteList est un objet, le convertir en tableau
        response.data.registrationPageTemplateWhiteList = Object.values(response.data.registrationPageTemplateWhiteList)
      }
      store.commit('process/endSaving', false)
      store.commit('template/updateTemplate',  response.data)
    })
    .catch(error => {
      const payload = { error: translator.trans('The email could not be deleted for an unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}
