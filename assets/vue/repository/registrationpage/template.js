import axios       from 'axios'
import store       from '../../store'
import { routing, translator } from '../../utils/app.utils'

export const getTemplates = (search = {}) => {
  store.commit('template/emptyTemplates')
  store.commit('process/loading', true)
  axios.get(routing.generate('_template_search', search))
    .then(response => {
      store.commit('template/updateTemplates', response.data.message)
      store.commit('process/loading', false)
    })
}

export const createTemplate = (template) => {
  store.commit('process/initSaving', true)
  return axios.post(routing.generate('_registration_page_template_save'), template)
    .then((response) => {
      store.commit('template/updateTemplates',  response.data.message)
      store.commit('template/updateTemplate', null)
      store.commit('process/endSaving')
      window.location.href = routing.generate('_registration_page_index')
    })
    .catch((error) => {
      const payload = {
        errorModal: translator.trans('The template could not be created for an unknown reason.'),
        formErrors: {}
      }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.errorModal = error.response.data.message.text
        if ('data' in error.response.data.message) {
          payload.formErrors = error.response.data.message.data
        }
      }
      store.commit('process/errorSaving', payload)
    })
}

export const deleteTemplate = (templateId) => {
  store.commit('process/initSaving', true)
  return axios.get(routing.generate('_template_delete', { id: templateId }))
    .then((response) => {
      store.commit('template/removeTemplate', templateId)
      store.commit('process/endSaving')
      window.location.href = routing.generate('_registration_page_index')
    }).catch((error) => {
      const payload = { error: translator.trans('Template could not be deleted for unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}

export const updateTemplate = (template) => {
  store.commit('process/initSaving', true)
  return axios.post(routing.generate('_registration_page_template_save'), template)
    .then((response) => {
      store.commit('template/updateTemplates', response.data.message)
      store.commit('process/endSaving')
    }).catch((error) => {
      const payload = {
        error: translator.trans('Template could not be updated for unknown reason.'),
        formErrors: {}
      }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
        if ('data' in error.response.data.message) {
          payload.formErrors = error.response.data.message.data
        }
      }
      store.commit('process/errorSaving', payload)
    })
}
