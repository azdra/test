import axios from 'axios'
import { routing } from '../utils/app.utils'

export const correctTitle = (title, lang) => {
  return new Promise((resolve, reject) => {
    return axios.post(routing.generate('_gptapi_correct'), { title, lang }).then((response) => {
      resolve(response.data.message)
    }).catch((error) => {
      reject(error)
    })
  })
}

export const shortenTitle = (title, lang) => {
  return new Promise((resolve, reject) => {
    return axios.post(routing.generate('_gptapi_shoten'), { title, lang }).then((response) => {
      resolve(response.data.message)
    }).catch((error) => {
      reject(error)
    })
  })
}

export const generateTitle = (data) => {
  return new Promise((resolve, reject) => {
    return axios.post(routing.generate('_gptapi_generate'), data).then((response) => {
      resolve(response.data.message)
    }).catch((error) => {
      reject(error)
    })
  })
}

export const generateSessionRegistrationProgram = (data, session) => {
  return new Promise((resolve, reject) => {
    return axios.post(routing.generate('_gptapi_generate_session_registration_program', { session: session.id }), data).then((response) => {
      resolve(response.data.message)
    }).catch((error) => {
      reject(error)
    })
  })
}

export const generateCatalogSubjectDescription = (data, subject) => {
  return new Promise((resolve, reject) => {
    return axios.post(routing.generate('_gptapi_generate_catalog_subject_description', { subject: subject.id }), data).then((response) => {
      resolve(response.data.message)
    }).catch((error) => {
      reject(error)
    })
  })
}

export const generateCatalogSubjectProgram = (data, subject) => {
  return new Promise((resolve, reject) => {
    return axios.post(routing.generate('_gptapi_generate_catalog_subject_program', { subject: subject.id }), data).then((response) => {
      resolve(response.data.message)
    }).catch((error) => {
      reject(error)
    })
  })
}
