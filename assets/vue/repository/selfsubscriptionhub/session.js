import axios from 'axios'
import store from '../../store'
import { routing, translator } from '../../utils/app.utils'

export const subscribeUser = (subject, session) => {
  store.commit('process/initSaving')
  return axios.get(routing.generate('_self_subscription_hub_subscription', { clientName: store.state.selfSubscription.module.slugClient, domain: store.state.selfSubscription.module.domain, id: session.id }))
    .then((response) => {
      store.dispatch('selfSubscription/newSubscription', { subscription: response.data.message })
      store.commit('process/endSaving')
    }).catch((error) => {
      const payload = { error: translator.trans('An unknown error occurred while subscribing you to the session.', {}, 'self-subscription') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}

export const unsubscribeUser = (subject) => {
  store.commit('process/initSaving')
  return axios.get(routing.generate('_self_subscription_hub_unsubscribe', { clientName: store.state.selfSubscription.module.slugClient, domain: store.state.selfSubscription.module.domain, subjectId: subject.id }))
    .then((response) => {
      store.commit('selfSubscription/unsubscribe', subject)
      store.commit('process/endSaving')
    }).catch((error) => {
      const payload = { error: translator.trans('An unknown error occurred while subscribing you to the session.', {}, 'self-subscription') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}
