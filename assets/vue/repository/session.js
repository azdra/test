import axios from 'axios'
import { routing } from '../utils/app.utils'

export const saveSession = (session, sendEmail) => {
  const sessionToSaved = {
    id: session.id,
    clientId: session.client.id,
    configurationHolder: session.abstractProviderConfigurationHolder.id,
    title: session.name,
    reference: session.ref,
    dateStart: session.dateStart,
    duration: session.datesDiffString,
    category: session.category,
    lang: session.language,
    businessNumber: session.businessNumber,
    tags: session.tags,
    animatorsOnly: session.animatorsOnly,
    traineesOnly: session.traineesOnly,
    information: session.information,
    convocation: session.convocation.id,
    notificationDate: session.notificationDate,
    dateOfSendingReminders: session.dateOfSendingReminders,
    personalizedContent: session.personalizedContent,
    automaticSendingTrainingCertificates: session.automaticSendingTrainingCertificates,
    webinarCommunication: session.webinarConvocation,
    accessType: session.admittedSession,
    sessionTest: session.sessionTest,
    defaultModelId: session.roomModel,
    url: session.urlPath,
    licence: session.licence,
    evaluations: Array.from(session.evaluations, evaluation => evaluation.id),
    attachments: Array.from(session.attachments, attachment => attachment.id),
    nature: session.nature,
    objectives: session.objectives,
    assistanceType: session.assistanceType,
    supportType: session.supportType,
    standardHoursActive: session.standardHoursActive,
    outsideStandardHoursActive: session.outsideStandardHoursActive,
    alertSupport: session.alertSupport,
    restlunch: session.restlunch,
    nbrdays: session.nbrdays,
    location: session.location,
    subject: (session.subject ? session.subject.id : null),
    subscriptionMax: session.subscriptionMax,
    alertLimit: session.alertLimit,
    replayVideoPlatform: session.replayVideoPlatform,
    replayUrlKey: session.replayUrlKey,
    publishedReplay: session.publishedReplay,
    accessParticipantReplay: session.accessParticipantReplay,
    sendEmailForEdit: sendEmail,
    registrationPageTemplate: session.registrationPageTemplateId,
    providerSessionRegistrationPage: session.providerSessionRegistrationPage,
    replayLayoutPlayButton: session.replayLayoutPlayButton,
    registrationPageTemplateBanner: session.registrationPageTemplateBanner,
    registrationPageTemplateIllustration: session.registrationPageTemplateIllustration,
    additionalTimezones: session.additionalTimezones,
    evaluationsProviderSession: session.evaluationsProviderSession,
    disableSubjectReplayOnSave: session.disableSubjectReplayOnSave ?? false
  }

  if (session.abstractProviderConfigurationHolder.type === 'white_connector') {
    sessionToSaved.joinSessionWebUrl = session.joinSessionWebUrl
    sessionToSaved.connectionInformation = session.connectionInformation
  }

  return axios
    .post(routing.generate('_session_save'), sessionToSaved)
}
