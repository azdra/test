import axios from 'axios'
import store from '../../store'
import { routing, translator } from '../../utils/app.utils'

export const saveVideo = (video) => {
  return new Promise((resolve, reject) => {
    store.commit('process/initSaving', true)
    return axios.post(routing.generate('_video_save', { id: video.moduleId }), video)
      .then((response) => {
        store.commit('process/endSaving')
        resolve(response.data.message)
      })
      .catch((error) => {
        const payload = {
          errorModal: translator.trans('Video could not be saved for unknown reason.'),
          formErrors: {}
        }
        if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
          payload.errorModal = error.response.data.message.text
          if ('data' in error.response.data.message) {
            payload.formErrors = error.response.data.message.data
          }
        }
        store.commit('process/errorSaving', payload)
        reject(payload)
      })
  })
}

export const deleteVideo = (video) => {
  return new Promise((resolve, reject) => {
    store.commit('process/initSaving', true)
    return axios.get(routing.generate('_video_delete', { id: video.moduleId, videoId: video.id }))
      .then((response) => {
        store.commit('process/endSaving')
        resolve(video.id)
      })
      .catch((error) => {
        const payload = { error: translator.trans('Video could not be deleted for unknown reason.') }
        if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
          payload.error = error.response.data.message.text
        }
        store.commit('process/errorSaving', payload)
        reject(payload)
      })
  })
}

export const newViewerReplay = (video) => {
  if (video) {
    return axios.post(routing.generate('_new_viewer_replay', { id: video.id }))
  }
}
