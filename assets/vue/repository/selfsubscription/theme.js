import axios       from 'axios'
import store       from '../../store'
import { routing, translator } from '../../utils/app.utils'

export const saveTheme = (theme) => {
  store.commit('process/initSaving', true)
  return axios.post(routing.generate('_theme_save', { id: theme.moduleId }), theme)
    .then((response) => {
      store.commit('module/saveTheme', response.data.message)
      store.commit('process/endSaving')
    })
    .catch((error) => {
      const payload = {
        errorModal: translator.trans('Theme could not be saved for unknown reason.'),
        formErrors: {}
      }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.errorModal = error.response.data.message.text
        if ('data' in error.response.data.message) {
          payload.formErrors = error.response.data.message.data
        }
      }
      store.commit('process/errorSaving', payload)
    })
}

export const deleteTheme = (theme) => {
  store.commit('process/initSaving', true)
  return axios.get(routing.generate('_theme_delete', { id: theme.moduleId, themeId: theme.id }))
    .then((response) => {
      store.commit('module/deleteTheme', theme.id)
      store.commit('process/endSaving')
    })
    .catch((error) => {
      const payload = { error: translator.trans('Theme could not be deleted for unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}
