import axios       from 'axios'
import store       from '../../store'
import { routing, translator } from '../../utils/app.utils'

export const saveTemplateFaq = (faq) => {
  store.commit('process/initSaving', true)
  return axios.post(routing.generate('_registration_page_template_faq', { id: faq.templateId }), faq)
    .then((response) => {
      store.commit('template/saveFAQ', response.data.message)
      store.commit('process/endSaving')
    })
    .catch((error) => {
      const payload = {
        errorModal: translator.trans('Theme could not be saved for unknown reason.'),
        formErrors: {}
      }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.errorModal = error.response.data.message.text
        if ('data' in error.response.data.message) {
          payload.formErrors = error.response.data.message.data
        }
      }
      store.commit('process/errorSaving', payload)
    })
}

export const deleteTemplateFaq = (faq) => {
  store.commit('process/initSaving', true)
  return axios.delete(routing.generate('_registration_page_template_faq_delete', { id: faq.templateId, faqId: faq.id }))
    .then((response) => {
      store.commit('template/deleteFAQ', faq.id)
      store.commit('process/endSaving')
      if (store.getters['template/activeFAQs'].length === 0) store.commit('template/disabledConnexionHelp')
    })
    .catch((error) => {
      const payload = { error: translator.trans('Theme could not be deleted for unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}

export const saveFAQ = (faq) => {
  store.commit('process/initSaving', true)
  return axios.post(routing.generate('_faq_save', { id: faq.moduleId }), faq)
    .then((response) => {
      store.commit('module/saveFAQ', response.data.message)
      store.commit('process/endSaving')
    })
    .catch((error) => {
      const payload = {
        errorModal: translator.trans('Theme could not be saved for unknown reason.'),
        formErrors: {}
      }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.errorModal = error.response.data.message.text
        if ('data' in error.response.data.message) {
          payload.formErrors = error.response.data.message.data
        }
      }
      store.commit('process/errorSaving', payload)
    })
}

export const deleteFAQ = (faq) => {
  store.commit('process/initSaving', true)
  return axios.get(routing.generate('_faq_delete', { id: faq.moduleId, faqId: faq.id }))
    .then((response) => {
      store.commit('module/deleteFAQ', faq.id)
      store.commit('process/endSaving')
    })
    .catch((error) => {
      const payload = { error: translator.trans('Theme could not be deleted for unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}
