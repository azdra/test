import axios       from 'axios'
import store       from '../../store'
import { routing, translator } from '../../utils/app.utils'

export const addNewWhiteListeEntry = (moduleId, mailEntry) => {
  store.commit('process/initSaving', true)
  axios.post(routing.generate('_new_allowed_white_list', { id: moduleId, allowedMail: mailEntry }))
    .then(response => {
      store.commit('process/endSaving', false)
      store.commit('module/updateModule',  response.data)
    })
    .catch(error => {
      const payload = { error: translator.trans('The email could not be saved for an unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}

export const deleteWhiteListeEntry = (moduleId, BlackListeEntryId) => {
  store.commit('process/initSaving', true)
  axios.post(routing.generate('_delete_allowed_white_list', { id: moduleId, whitelistId: BlackListeEntryId }))
    .then(response => {
      store.commit('process/endSaving', false)
      store.commit('module/updateModule',  response.data)
    })
    .catch(error => {
      const payload = { error: translator.trans('The email could not be deleted for an unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}
