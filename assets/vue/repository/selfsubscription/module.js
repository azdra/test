import axios       from 'axios'
import store       from '../../store'
import { routing, translator } from '../../utils/app.utils'

export const getModules = (search = {}) => {
  store.commit('module/emptyModules')
  store.commit('process/loading', true)
  axios.get(routing.generate('_module_search', search))
    .then(response => {
      store.commit('module/updateModules', response.data.message)
      store.commit('process/loading', false)
    })
}

export const createModule = (module) => {
  store.commit('process/initSaving', true)
  return axios.post(routing.generate('_module_save'), module)
    .then((response) => {
      store.commit('module/updateModules',  response.data.message)
      store.commit('module/updateModule', null)
      store.commit('process/endSaving')
    })
    .catch((error) => {
      const payload = {
        errorModal: translator.trans('The module could not be created for an unknown reason.'),
        formErrors: {}
      }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.errorModal = error.response.data.message.text
        if ('data' in error.response.data.message) {
          payload.formErrors = error.response.data.message.data
        }
      }
      store.commit('process/errorSaving', payload)
    })
}

export const deleteModule = (moduleId) => {
  store.commit('process/initSaving', true)
  return axios.get(routing.generate('_module_delete', { id: moduleId }))
    .then((response) => {
      store.commit('module/removeModule', moduleId)
      store.commit('process/endSaving')
    }).catch((error) => {
      const payload = { error: translator.trans('Module could not be deleted for unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}

export const updateModule = (module) => {
  store.commit('process/initSaving', true)
  return axios.post(routing.generate('_module_save'), module)
    .then((response) => {
      store.commit('module/updateModules', response.data.message)
      store.commit('process/endSaving')
    }).catch((error) => {
      const payload = {
        error: translator.trans('Module could not be updated for unknown reason.'),
        formErrors: {}
      }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
        if ('data' in error.response.data.message) {
          payload.formErrors = error.response.data.message.data
        }
      }
      store.commit('process/errorSaving', payload)
    })
}
