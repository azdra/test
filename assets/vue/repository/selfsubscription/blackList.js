import axios       from 'axios'
import store       from '../../store'
import { routing, translator } from '../../utils/app.utils'

export const addNewBlackListeEntry = (moduleId, mailEntry) => {
  store.commit('process/initSaving', true)
  axios.post(routing.generate('_new_prohibited_black_list', { id: moduleId, prohibitedMail: mailEntry }))
    .then(response => {
      store.commit('process/endSaving', false)
      store.commit('module/updateModule',  response.data)
    })
    .catch(error => {
      const payload = { error: translator.trans('The email could not be saved for an unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}

export const deleteBlackListeEntry = (moduleId, BlackListeEntryId) => {
  store.commit('process/initSaving', true)
  axios.post(routing.generate('_delete_prohibited_black_list', { id: moduleId, blacklistId: BlackListeEntryId }))
    .then(response => {
      store.commit('process/endSaving', false)
      store.commit('module/updateModule',  response.data)
    })
    .catch(error => {
      const payload = { error: translator.trans('The email could not be deleted for an unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}
