import axios       from 'axios'
import store       from '../../store'
import { routing, translator } from '../../utils/app.utils'

export const saveSubject = (subject) => {
  return new Promise((resolve, reject) => {
    store.commit('process/initSaving', true)
    return axios.post(routing.generate('_subject_save', { id: subject.moduleId, themeId: subject.themeId }), subject)
      .then((response) => {
        // store.commit('module/saveSubject', response.data.message)
        store.commit('process/endSaving')
        resolve(response.data.message)
      })
      .catch((error) => {
        const payload = {
          errorModal: translator.trans('Subject could not be saved for unknown reason.'),
          formErrors: {}
        }
        if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
          payload.errorModal = error.response.data.message.text
          if ('data' in error.response.data.message) {
            payload.formErrors = error.response.data.message.data
          }
        }
        reject(payload)
        store.commit('process/errorSaving', payload)
      })
  })
}

export const deleteSubject = (subject) => {
  store.commit('process/initSaving', true)
  return axios.get(routing.generate('_subject_delete', { id: subject.moduleId, themeId: subject.themeId, subjectId: subject.id }))
    .then((response) => {
      store.commit('module/deleteSubject', subject)
      store.commit('process/endSaving')
    })
    .catch((error) => {
      const payload = { error: translator.trans('Subject could not be deleted for unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}
