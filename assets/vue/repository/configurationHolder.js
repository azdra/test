import axios       from 'axios'
import store       from '../store'
import { routing, translator } from '../utils/app.utils'

export const saveConfigurationHolder = (configurationHolder) => {
  store.commit('process/initSaving', true)
  return axios.post(routing.generate('_configuration_holder_save', { id: configurationHolder.clientId }), configurationHolder)
    .then((response) => {
      store.commit('process/endSaving')
      window.location.href = routing.generate('_client_edit', { id: configurationHolder.clientId, 'tab-name': 'clientConnectorTab' })
    })
    .catch((error) => {
      const payload = {
        errorModal: translator.trans('The connector could not be saved for an unknown reason.'),
        formErrors: {}
      }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.errorModal = error.response.data.message.text
        if ('data' in error.response.data.message) {
          payload.formErrors = error.response.data.message.data
        }
      }
      store.commit('process/errorSaving', payload)
    })
}

export const deleteConfigurationHolder = (configurationHolder) => {
  store.commit('process/initSaving', true)
  return axios.get(routing.generate('_configuration_holder_delete', { id: configurationHolder.id }))
    .then((response) => {
      store.commit('process/endSaving')
      window.location.href = routing.generate('_client_edit', { id: configurationHolder.client, 'tab-name': 'clientConnectorTab' })
    })
    .catch((error) => {
      const payload = { error: translator.trans('The connector could not be deleted for an unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}

export const archiveConfigurationHolder = (configurationHolder) => {
  store.commit('process/initSaving', true)
  return axios.get(routing.generate('_configuration_holder_archive', { id: configurationHolder.id }))
    .then((response) => {
      store.commit('process/endSaving')
      window.location.href = routing.generate('_client_edit', { id: configurationHolder.client, 'tab-name': 'clientConnectorTab' })
    })
    .catch((error) => {
      const payload = { error: translator.trans('The connector could not be archived for an unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}

export const unarchivedConfigurationHolder = (configurationHolder) => {
  store.commit('process/initSaving', true)
  return axios.get(routing.generate('_configuration_holder_unarchived', { id: configurationHolder.id }))
    .then((response) => {
      store.commit('process/endSaving')
      window.location.href = routing.generate('_client_edit', { id: configurationHolder.client, 'tab-name': 'clientConnectorTab' })
    })
    .catch((error) => {
      const payload = { error: translator.trans('The connector could not be unarchived for an unknown reason.') }
      if ('response' in error && 'data' in error.response && typeof error.response.data === 'object' && 'message' in error.response.data && 'text' in error.response.data.message) {
        payload.error = error.response.data.message.text
      }
      store.commit('process/errorSaving', payload)
    })
}
