const REGEX = {
  VALID_EMAIL: /^[\w-.]+@([\w-]+\.)+[\w-]{2,12}$/g
}

export const isValidEmail = (input) => input.match(REGEX.VALID_EMAIL)
