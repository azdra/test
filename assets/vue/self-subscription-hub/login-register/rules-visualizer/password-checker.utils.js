const LENGTH = {
  MIN: 8
}

const REGEX = {
  CONTAIN_LOWER_LETTER: /[a-z]/,
  CONTAIN_UPPER_LETTER: /[A-Z]/,
  CONTAIN_NUMBER: /\d/,
  CONTAIN_SPECIAL_CHARACTERS: /[\\/!@#$%^&*(),.?:{}|<>]/
}

export const lengthIsMoreMinimal = (input) => input.length >= LENGTH.MIN
export const containLowerLetter = (input) => input.match(REGEX.CONTAIN_LOWER_LETTER)
export const containUpperLetter = (input) => input.match(REGEX.CONTAIN_UPPER_LETTER)
export const containNumber = (input) => input.match(REGEX.CONTAIN_NUMBER)
export const containSpecialCharacters = (input) => input.match(REGEX.CONTAIN_SPECIAL_CHARACTERS)

export const matchWithAllRequirements = (input) => {
  return containLowerLetter(input) &&
    containUpperLetter(input) &&
    containNumber(input) &&
    containSpecialCharacters(input) &&
    lengthIsMoreMinimal(input)
}
