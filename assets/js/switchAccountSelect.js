import Vue         from 'vue'
import SwitchAccountSelect   from '../vue/components/security/SwitchAccountSelect'
import AppVueMixin from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#switch-account-select',
  name: 'SwitchAccountSelectApp',
  components: {
    SwitchAccountSelect
  }
})
