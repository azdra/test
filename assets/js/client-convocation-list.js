import Vue                   from 'vue'
import ClientConvocationList from '../vue/components/clients/ClientConvocationList'
import AppVueMixin           from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#client-convocation-list',
  name: 'ClientConvocationListApp',
  components: {
    ClientConvocationList
  }
})
