import Vue            from 'vue'
import PresenceValuesForm from '../vue/components/clients/PresenceValuesForm'
import AppVueMixin    from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#presence-values-form',
  name: 'PresenceValuesFormApp',
  components: {
    PresenceValuesForm
  }
})
