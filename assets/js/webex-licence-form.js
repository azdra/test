import Vue          from 'vue'
import WebexLicenceUserForm from '../vue/components/users/WebexLicenceUserForm'
import AppVueMixin  from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#webex-licence-form',
  name: 'WebexLicenceFormApp',
  components: {
    WebexLicenceUserForm
  }
})
