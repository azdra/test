const vichWidgets = document.querySelectorAll('.vich-widget')
vichWidgets.forEach(function (vichWidget) {
  const deleteButton = vichWidget.querySelector('.btn-delete')
  if (deleteButton) {
    deleteButton.addEventListener('click', function () {
      vichWidget.querySelector('input').checked = true
      vichWidget.querySelector('.vich-widget-preview').classList.add('d-none')
      vichWidget.querySelector('.vich-widget-input').classList.remove('d-none')
    })
  }
})
