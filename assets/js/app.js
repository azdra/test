// Manipulation ( ouverture et fermeture ) de la user-nav
const userNav = document.querySelector('#user-nav')
const userNavToggler = document.querySelectorAll('.user-nav-toggler')
const userNavToggle = () => userNav.classList.toggle('user-nav--is-open')
userNavToggler.forEach(function (i) {
  i.addEventListener('click', userNavToggle)
})

// Affichage du dropdown en cliquant sur l'icone cloche des notifications
const bellDropdownDisplayer = elt => {
  bellNotificationsDropdown.classList.add('show')
}
const bellNotifications = document.getElementById('bell-notifications')
const bellNotificationsDropdown = document.getElementById('bell-notifications-dropdown')

if (bellNotifications) {
  bellNotifications.addEventListener('click', function () { bellDropdownDisplayer(this) })
}

if (bellNotificationsDropdown !== null) {
  // Masque la dropdown si on clique en dehors de cette fenêtre
  document.addEventListener('click', (e) => {
    let targetElement = e.target
    do {
      if (targetElement === bellNotifications) {
        return
      }

      targetElement = targetElement.parentNode
    } while (targetElement)

    bellNotificationsDropdown.classList.remove('show')
  })
}

// Prevent Bootstrap dialog from blocking focusin
document.addEventListener('focusin', (e) => {
  if (e.target.closest('.tox-tinymce-aux, .moxman-window, .tam-assetmanager-root') !== null) {
    e.stopImmediatePropagation()
  }
})
