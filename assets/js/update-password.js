import Vue                from 'vue'
import UpdatePasswordForm from '../vue/components/security/UpdatePasswordForm'
import AppVueMixin        from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#update-password-form',
  name: 'UpdatePasswordFormApp',
  components: {
    UpdatePasswordForm
  }
})
