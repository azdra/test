import Vue         from 'vue'
import TagsInput   from '../vue/components/ui/TagsInput'
import AppVueMixin from '../vue/mixin/app.mixin'

AppVueMixin()

const vues = document.querySelectorAll('.tags-input')
Array.prototype.forEach.call(vues, (el) =>
  new Vue({
    el: el,
    name: 'TagsInputApp',
    components: {
      TagsInput
    }
  })
)
