import Vue          from 'vue'
import MicrosoftTeamsLicenceUserForm from '../vue/components/users/MicrosoftTeamsLicenceUserForm'
import AppVueMixin  from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#microsoft-teams-licence-form',
  name: 'MicrosoftTeamsLicenceFormApp',
  components: {
    MicrosoftTeamsLicenceUserForm
  }
})
