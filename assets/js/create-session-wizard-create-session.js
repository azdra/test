import Vue                                     from 'vue'
import CreateSessionWizardCreateSession        from '../vue/components/sessions/wizard/step-create-session/CreateSessionWizardCreateSession'
import CreateSessionWizardCreateSessionGeneral from '../vue/components/sessions/wizard/step-create-session/CreateSessionWizardCreateSessionGeneral'
import AppVueMixin                             from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#create-session-wizard-create-session',
  name: 'CreateSessionWizardCreateSessionApp',
  components: {
    CreateSessionWizardCreateSession,
    CreateSessionWizardCreateSessionGeneral
  }
})
