import Vue         from 'vue'
import ClientsList from '../vue/components/clients/ClientsList'
import AppVueMixin from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#clients-list',
  name: 'ClientsListApp',
  components: {
    ClientsList
  }
})
