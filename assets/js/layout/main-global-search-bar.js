import Vue from 'vue'
import MainGlobalSearchBar from '../../vue/components/layout/MainGlobalSearchBar'
import AppVueMixin from '../../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#main-global-searchbar',
  name: 'MainGlobalSearchBarApp',
  components: {
    MainGlobalSearchBar
  }
})
