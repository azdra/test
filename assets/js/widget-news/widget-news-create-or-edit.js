import Vue         from 'vue'
import AppVueMixin from '../../vue/mixin/app.mixin'
import WidgetNewsCreateOrEdit from '../../vue/components/widgetNews/WidgetNewsCreateOrEdit.vue'

AppVueMixin()

new Vue({
  el: '#widget-news-create-or-edit',
  name: 'WidgetNewsCreateOrEditApp',
  components: {
    WidgetNewsCreateOrEdit
  }
})
