import Vue         from 'vue'
import AppVueMixin from '../../vue/mixin/app.mixin'
import WidgetNewsList from '../../vue/components/widgetNews/WidgetNewsList.vue'

AppVueMixin()

new Vue({
  el: '#widget-news-list',
  name: 'WidgetNewsListApp',
  components: {
    WidgetNewsList
  }
})
