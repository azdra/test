import Translator from 'bazinga-translator'
require('../../public/translations/translations/config')
require('../../public/translations/translations/fr')

const tinymce = require('./tinymce.min')

const message = Translator.trans('A modification is in progress. Are you sure you want to continue?')
const clientForms = {
  '#nav-general': false,
  '#nav-organization': false,
  '#nav-services': false,
  '#nav-users': false,
  '#nav-customization': false,
  '#nav-options': false
}

function addEventListenerOnForm (form) {
  const fields = document.querySelectorAll(`${form} input, ${form} select, ${form} textarea`)

  for (let i = 0; i < fields.length; i++) {
    const field = fields[i]

    field.addEventListener('change', () => {
      clientForms[form] = true
    })
  }
}

for (const form in clientForms) {
  addEventListenerOnForm(form)
}

const tabs = document.querySelectorAll('#nav-tab-user > a[data-toggle="tab"]')

for (let i = 0; i < tabs.length; i++) {
  const tab = tabs[i]

  tab.addEventListener('show.bs.tab', function (event) {
    if (clientForms[event.relatedTarget.getAttribute('href')]) {
      const response = confirm(message)

      if (!response) { event.preventDefault() }
    }
  })
}

/** Client Form - Onglet Service */

tinymce.init({
  selector:'textarea#client_services_tab_form_personnalizedInfoMailTransactionnal',
  width: '100%',
  height: '355px',
  menubar: 'file edit view insert format table help',
  plugins: [
    'print preview paste importcss searchreplace autolink autosave',
    'save directionality code visualblocks visualchars fullscreen image',
    'link media template codesample table charmap hr pagebreak nonbreaking',
    'anchor toc insertdatetime advlist lists wordcount imagetools textpattern',
    'noneditable help charmap quickbars emoticons'
  ],
  toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
  toolbar_sticky: true,
  toolbar_mode: 'sliding',
  language: 'fr_FR'
})
