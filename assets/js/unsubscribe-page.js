import Vue         from 'vue'
import UnsubscribePage from '../vue/components/users/UnsubscribePage.vue'
import AppVueMixin from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#unsubscribe-page',
  name: 'UnsubscribePage',
  components: {
    UnsubscribePage
  }
})
