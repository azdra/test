import Vue             from 'vue'
import EvaluationSessionLobby from '../vue/components/evaluationsSession/EvaluationSessionLobby.vue'
import AppVueMixin     from '../vue/mixin/app.mixin'
import store        from '../vue/store'
import vueResponsive from 'vue-responsive'

AppVueMixin()
Vue.directive('responsive', vueResponsive)

new Vue({
  el: '#evaluation-session-lobby',
  name: 'EvaluationSessionLobby',
  store,
  components: {
    EvaluationSessionLobby
  }
})
