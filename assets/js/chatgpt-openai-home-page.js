import Vue           from 'vue'
import AppVueMixin  from '../vue/mixin/app.mixin'
import store        from '../vue/store'
import ChatgptOpenAIHomePage from '../vue/components/chatgpt-openai/ChatgptOpenAIHomePage.vue'
import ChatgptOpenAIHomePageHeader from '../vue/components/chatgpt-openai/ChatgptOpenAIHomePageHeader.vue'

AppVueMixin()

new Vue({
  el: '#chatgpt-openai-home-page',
  name: 'ChatgptOpenAIHomePage',
  store,
  components: {
    ChatgptOpenAIHomePage
  }
})

new Vue({
  el: '#chatgpt-openai-home-page-header',
  name: 'ChatgptOpenAIHomePageHeader',
  store,
  components: {
    ChatgptOpenAIHomePageHeader
  }
})
