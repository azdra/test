import Vue                   from 'vue'
import ClientEvaluationMailList from '../vue/components/clients/ClientEvaluationMailList'
import AppVueMixin           from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#client-evaluation-mail-list',
  name: 'ClientEvaluationMailListApp',
  components: {
    ClientEvaluationMailList
  }
})
