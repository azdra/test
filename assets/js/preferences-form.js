import Vue            from 'vue'
import PreferencesForm from '../vue/components/clients/PreferencesForm'
import AppVueMixin    from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#preferences-form',
  components: {
    PreferencesForm
  }
})
