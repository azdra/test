import Vue               from 'vue'
import PasswordResetForm from '../vue/components/security/PasswordResetForm'
import AppVueMixin       from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#password-reset-form',
  name: 'PasswordResetFormApp',
  components: {
    PasswordResetForm
  }
})
