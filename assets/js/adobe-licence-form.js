import Vue          from 'vue'
import AdobeLicenceUserForm from '../vue/components/users/AdobeLicenceUserForm'
import AppVueMixin  from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#adobe-licence-form',
  name: 'AdobeLicenceUserForm',
  components: {
    AdobeLicenceUserForm
  }
})
