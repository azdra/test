import Vue               from 'vue'
import ParticipantForm from '../vue/components/evaluations/participant-form/ParticipantForm'
import AppVueMixin       from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#participant-form',
  name: 'ParticipantFormApp',
  components: {
    ParticipantForm
  }
})
