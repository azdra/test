import Vue                  from 'vue'
import GroupParticipantEdit from '../vue/components/users/GroupParticipantEdit.vue'
import AppVueMixin          from '../vue/mixin/app.mixin'
import store        from '../vue/store'

AppVueMixin()

new Vue({
  el: '#group-participant-edit',
  name: 'GroupParticipantEdit',
  store,
  components: {
    GroupParticipantEdit
  }
})
