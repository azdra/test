import Vue           from 'vue'
import AppVueMixin  from '../../vue/mixin/app.mixin'
import store        from '../../vue/store'
import SessionEvaluationSynthesis from '../../vue/components/sessions/SessionEvaluationSynthesis.vue'

AppVueMixin()

new Vue({
  el: '#session-evaluation-synthesis',
  name: 'SessionEvaluationSynthesis',
  store,
  components: {
    SessionEvaluationSynthesis
  }
})
