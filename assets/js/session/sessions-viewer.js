import Vue            from 'vue'
import SessionsViewer from '../../vue/components/sessions/SessionsViewer'
import AppVueMixin    from '../../vue/mixin/app.mixin'
import store          from '../../vue/store'

AppVueMixin()

new Vue({
  el: '#sessions-viewer',
  name: 'SessionsViewerApp',
  store,
  components: {
    SessionsViewer
  }
})
