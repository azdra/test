import Vue           from 'vue'
import SessionTabs   from '../../vue/components/sessions/view-tab/SessionTabs'
import SessionSubmit from '../../vue/components/sessions/SessionSubmit'
import SessionExportModal from '../../vue/components/sessions/view-tab/SessionExportModal'
import AppVueMixin  from '../../vue/mixin/app.mixin'
import store        from '../../vue/store'

AppVueMixin()

new Vue({
  el: '#sessions-tabs',
  name: 'SessionsTabsApp',
  store,
  components: {
    SessionTabs
  }
})

new Vue({
  el: '#session-submit',
  name: 'SessionSubmitApp',
  store,
  components : {
    SessionSubmit
  }
})

new Vue({
  el: '#session-export-modal',
  name: 'SessionExportModal',
  store,
  components : {
    SessionExportModal
  }
})
