import Vue                                 from 'vue'
import SessionAccessRequestPasswordTooltip from '../../vue/components/sessions/SessionAccessRequestPasswordTooltip'
import AppVueMixin                         from '../../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#session-access-request-password-tooltip',
  name: 'SessionAccessRequestPasswordTooltipApp',
  components: {
    SessionAccessRequestPasswordTooltip
  }
})
