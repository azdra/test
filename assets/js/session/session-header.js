import Vue           from 'vue'
import SessionHeader from '../../vue/components/sessions/view-tab/SessionHeader'
import AppVueMixin  from '../../vue/mixin/app.mixin'
import store        from '../../vue/store'

AppVueMixin()

new Vue({
  el: '#session-header',
  name: 'SessionHeaderApp',
  store,
  components: {
    SessionHeader
  }
})
