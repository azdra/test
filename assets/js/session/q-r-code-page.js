import Vue            from 'vue'
import QRCodePage from '../../vue/components/sessions/view-tab/QRCodePage.vue'
import AppVueMixin    from '../../vue/mixin/app.mixin'
import store          from '../../vue/store'
import vueResponsive from 'vue-responsive'

AppVueMixin()
Vue.directive('responsive', vueResponsive)

new Vue({
  el: '#q-r-code-page',
  name: 'QRCodePage',
  store,
  components: {
    QRCodePage
  }
})
