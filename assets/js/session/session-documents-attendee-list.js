import Vue            from 'vue'
import SessionsDocumentsAttendeeList from '../../vue/components/sessions/view-tab/SessionsDocumentsAttendeeList'
import AppVueMixin    from '../../vue/mixin/app.mixin'
import store          from '../../vue/store'
import vueResponsive from 'vue-responsive'

AppVueMixin()
Vue.directive('responsive', vueResponsive)

new Vue({
  el: '#session-documents-attendee-list',
  name: 'SessionsDocumentsAttendeeListApp',
  store,
  components: {
    SessionsDocumentsAttendeeList
  },
  props: {

  }
})
