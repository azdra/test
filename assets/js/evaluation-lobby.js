import Vue                 from 'vue'
import EvaluationLobby from '../vue/components/evaluations/EvaluationLobby'
import AppVueMixin         from '../vue/mixin/app.mixin'
import store        from '../vue/store'
import vueResponsive from 'vue-responsive'

AppVueMixin()
Vue.directive('responsive', vueResponsive)

new Vue({
  el: '#evaluation-lobby',
  name: 'EvaluationLobby',
  store,
  components: {
    EvaluationLobby
  }
})
