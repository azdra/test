import Vue                from 'vue'
import CollectionType     from '../vue/components/ui/CollectionType'
import CollectionTypeItem from '../vue/components/ui/CollectionTypeItem'
import AppVueMixin        from '../vue/mixin/app.mixin'

AppVueMixin()

const vues = document.querySelectorAll('.collection-type')
Array.prototype.forEach.call(vues, (el) =>
  new Vue({
    el: el,
    name: 'CollectionTypeApp',
    components: {
      CollectionType,
      CollectionTypeItem
    }
  })
)
