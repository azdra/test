import Vue         from 'vue'
import BouncesList   from '../vue/components/npai/BouncesList'
import AppVueMixin from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#bounces-list',
  name: 'BouncesListApp',
  components: {
    BouncesList
  }
})
