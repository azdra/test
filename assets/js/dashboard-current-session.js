import Vue                   from 'vue'
import CurrentSession from '../vue/components/dashboard/CurrentSession'
import store from '../vue/store'

new Vue({
  el: '#current-session',
  name: 'CurrentSessionApp',
  store,
  components: {
    CurrentSession
  }
})
