import Vue            from 'vue'
import EvaluationForm from '../vue/components/evaluations/EvaluationForm'
import AppVueMixin    from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#evaluations-form',
  name: 'EvaluationsFormApp',
  components: {
    EvaluationForm
  }
})
