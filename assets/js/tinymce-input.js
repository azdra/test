import Vue from 'vue'
import AppVueMixin  from './../vue/mixin/app.mixin'
import TinymceInput from '../vue/components/ui/TinymceInput'

AppVueMixin()

const vues = document.querySelectorAll('.tinymce-input')
Array.prototype.forEach.call(vues, (el) =>
  new Vue({
    el: el,
    name: 'TinyMceInputApp',
    components: {
      TinymceInput
    }
  })
)
