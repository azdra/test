import Vue          from 'vue'
import MicrosoftTeamsEventLicenceUserForm from '../vue/components/users/MicrosoftTeamsEventLicenceUserForm'
import AppVueMixin  from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#microsoft-teams-event-licence-form',
  name: 'MicrosoftTeamsEventLicenceFormApp',
  components: {
    MicrosoftTeamsEventLicenceUserForm
  }
})
