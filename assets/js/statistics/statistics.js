import Vue         from 'vue'
import Statistics  from '../../vue/components/statistics/Statistics'
import AppVueMixin from '../../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#statistics',
  name: 'StatisticsApp',
  components: {
    Statistics
  }
})
