import Vue from 'vue'
import StatisticsEvaluations  from '../../vue/components/statistics/evaluations/StatisticsEvaluations'
import AppVueMixin from '../../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#statistics-evaluations',
  name: 'StatisticsEvaluationsApp',
  components: {
    StatisticsEvaluations
  }
})
