import Vue               from 'vue'
import ParticipantEvaluationSession from '../vue/components/evaluationsSession/participant-form/ParticipantEvaluationSessionForm.vue'
import AppVueMixin       from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#participant-form-evaluation-session',
  name: 'ParticipantEvaluationSessionApp',
  components: {
    ParticipantEvaluationSession
  }
})
