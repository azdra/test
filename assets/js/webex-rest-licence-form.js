import Vue          from 'vue'
import WebexRestLicenceUserForm from '../vue/components/users/WebexRestLicenceUserForm'
import AppVueMixin  from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#webex-rest-licence-form',
  name: 'WebexRestLicenceFormApp',
  components: {
    WebexRestLicenceUserForm
  }
})
