import Vue            from 'vue'
import PreferencesProfilForm from '../vue/components/users/PreferencesProfilForm'
import AppVueMixin    from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#preferences-profil-form',
  components: {
    PreferencesProfilForm
  }
})
