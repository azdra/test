import Vue                   from 'vue'
import LiveSessionUserAvatar from '../vue/components/users/LiveSessionUserAvatar'
import AppVueMixin           from '../vue/mixin/app.mixin'

AppVueMixin()

const vues = document.querySelectorAll('.user-avatar')
Array.prototype.forEach.call(vues, (el) =>
  new Vue({
    el: el,
    name: 'UserAvatarApp',
    components: {
      LiveSessionUserAvatar
    }
  })
)
