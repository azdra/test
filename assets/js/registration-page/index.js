import Vue                   from 'vue'
import AppVueMixin           from '../../vue/mixin/app.mixin'
import RegistrationPageIndex from '../../vue/components/registration-page/RegistrationPageIndex.vue'
import store from '../../vue/store'

AppVueMixin()
new Vue({
  el: '#registration-page-index',
  name: 'CustomerServiceDashboardApp',
  store,
  components: {
    RegistrationPageIndex
  }
})
