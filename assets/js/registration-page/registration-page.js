import Vue from 'vue'
import AppVueMixin from '../../vue/mixin/app.mixin'
import store from '../../vue/store'
import RegistrationPage from '../../vue/components/registration-page/weblink/RegistrationPage.vue'

AppVueMixin()
new Vue({
  el: '#registration-page',
  name: 'RegistrationPageApp',
  store,
  components: {
    RegistrationPage
  }
})
