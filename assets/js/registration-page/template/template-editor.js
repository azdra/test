import Vue            from 'vue'
import TemplateEditor from '../../../vue/components/registration-page/template/Editor/TemplateEditor'
import AppVueMixin    from '../../../vue/mixin/app.mixin'
import store          from '../../../vue/store'

AppVueMixin()

new Vue({
  el: '#template-editor',
  name: 'TemplateEditorApp',
  store,
  components: {
    TemplateEditor
  }
})
