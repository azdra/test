import Vue               from 'vue'
import ClientTailored from '../../vue/components/clients/ClientTailored'
import AppVueMixin       from '../../vue/mixin/app.mixin'
import store          from '../../vue/store'

AppVueMixin()

new Vue({
  el: '#client-tailored',
  name: 'ClientTailored',
  store,
  components: {
    ClientTailored
  }
})
