import Vue               from 'vue'
import ClientBilling from '../../vue/components/clients/ClientBilling'
import AppVueMixin       from '../../vue/mixin/app.mixin'
import store          from '../../vue/store'

AppVueMixin()

new Vue({
  el: '#client-billing',
  name: 'ClientBilling',
  store,
  components: {
    ClientBilling
  }
})
