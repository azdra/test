import Vue                 from 'vue'
import CreateSessionWizard from '../vue/components/sessions/wizard/CreateSessionWizard'
import AppVueMixin         from '../vue/mixin/app.mixin'
import store        from '../vue/store'

AppVueMixin()

new Vue({
  el: '#create-session-wizard',
  name: 'CreateSessionWizardApp',
  store,
  components: {
    CreateSessionWizard
  }
})
