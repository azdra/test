import Vue          from 'vue'
import WhiteLicenceUserForm from '../vue/components/users/WhiteLicenceUserForm'
import AppVueMixin  from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#white-licence-form',
  name: 'WhiteLicenceFormApp',
  components: {
    WhiteLicenceUserForm
  }
})
