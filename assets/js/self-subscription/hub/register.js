import Vue from 'vue'
import AppVueMixin from '../../../vue/mixin/app.mixin'
import store from '../../../vue/store'
import Register from '../../../vue/self-subscription-hub/login-register/Register.vue'

AppVueMixin()
new Vue({
  el: '#self-subscription-register',
  name: 'SelfSubscriptionRegisterApp',
  store,
  components: {
    Register
  }
})
