import Vue from 'vue'
import AppVueMixin from '../../../vue/mixin/app.mixin'
import store from '../../../vue/store'
import ResetPassword from '../../../vue/self-subscription-hub/login-register/ResetPassword.vue'

AppVueMixin()
new Vue({
  el: '#self-subscription-reset-password',
  name: 'SelfSubscriptionResetPasswordApp',
  store,
  components: {
    ResetPassword
  }
})
