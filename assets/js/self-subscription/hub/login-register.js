import Vue              from 'vue'
import AppVueMixin      from '../../../vue/mixin/app.mixin'
import LoginAndRegister from '../../../vue/self-subscription-hub/login-register/LoginAndRegister'
import store            from '../../../vue/store'

AppVueMixin()

new Vue({
  el: '#ss-login-register',
  name: 'LoginAndRegisterApp',
  store,
  components: {
    LoginAndRegister
  }
})
