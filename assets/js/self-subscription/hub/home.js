import Vue               from 'vue'
import AppVueMixin       from '../../../vue/mixin/app.mixin'
import ModuleUserHub from '../../../vue/self-subscription-hub/ModuleUserHub'
import store             from '../../../vue/store'

import 'vue-slick-carousel/dist/vue-slick-carousel.css'
import 'vue-slick-carousel/dist/vue-slick-carousel-theme.css'

AppVueMixin()

new Vue({
  el: '#module-user-hub',
  name: 'ModuleUserHubApp',
  store,
  components: {
    ModuleUserHub
  }
})
