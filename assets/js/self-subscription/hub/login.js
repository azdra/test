import Vue from 'vue'
import AppVueMixin from '../../../vue/mixin/app.mixin'
import store from '../../../vue/store'
import Login from '../../../vue/self-subscription-hub/login-register/Login.vue'

AppVueMixin()
new Vue({
  el: '#self-subscription-login',
  name: 'SelfSubscriptionLoginApp',
  store,
  components: {
    Login
  }
})
