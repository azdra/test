import Vue            from 'vue'
import ModuleViewer from '../../../vue/components/self-subscription/module/Viewer/ModuleViewer'
import AppVueMixin    from '../../../vue/mixin/app.mixin'
import store          from '../../../vue/store'

AppVueMixin()

new Vue({
  el: '#module-viewer',
  name: 'ModuleViewerApp',
  store,
  components: {
    ModuleViewer
  }
})
