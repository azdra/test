import Vue            from 'vue'
import ModuleEditor from '../../../vue/components/self-subscription/module/Editor/ModuleEditor'
import AppVueMixin    from '../../../vue/mixin/app.mixin'
import store          from '../../../vue/store'

AppVueMixin()

new Vue({
  el: '#module-editor',
  name: 'ModuleEditorApp',
  store,
  components: {
    ModuleEditor
  }
})
