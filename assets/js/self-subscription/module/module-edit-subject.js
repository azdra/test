import Vue            from 'vue'
import AppVueMixin    from '../../../vue/mixin/app.mixin'
import store          from '../../../vue/store'
import ModuleEditSubject from '../../../vue/components/self-subscription/module/EditSubject/ModuleEditSubject.vue'

AppVueMixin()

new Vue({
  el: '#module-edit-subject',
  name: 'ModuleEditSubjectApp',
  store,
  components: {
    ModuleEditSubject
  }
})
