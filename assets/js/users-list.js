import Vue         from 'vue'
import UsersList   from '../vue/components/users/UsersList'
import AppVueMixin from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#users-list',
  name: 'UsersListApp',
  components: {
    UsersList
  }
})
