import Vue                   from 'vue'
import UsersParticipantsList from '../vue/components/users/UsersParticipantsList'
import AppVueMixin           from '../vue/../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#users-participants-list',
  name: 'UsersParticipantsListApp',
  components: {
    UsersParticipantsList
  }
})
