import Vue               from 'vue'
import ClientManagerList from '../vue/components/clients/ClientManagerList'
import AppVueMixin       from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#client-manager-list',
  name: 'ClientManagerListApp',
  components: {
    ClientManagerList
  }
})
