import Vue         from 'vue'
import Activities  from '../vue/components/activites/Activities'
import AppVueMixin from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#activities',
  name: 'ActivitiesApp',
  components: {
    Activities
  }
})
