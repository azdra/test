import Vue                   from 'vue'
import WidgetNews from '../vue/components/dashboard/WidgetNews'
import store from '../vue/store'

new Vue({
  el: '#widget-news',
  name: 'WidgetNews',
  store,
  components: {
    WidgetNews
  }
})
