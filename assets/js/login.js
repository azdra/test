import Vue         from 'vue'
import LoginForm   from '../vue/components/security/LoginForm'
import AppVueMixin from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#login-form',
  name: 'LoginFormApp',
  components: {
    LoginForm
  }
})
