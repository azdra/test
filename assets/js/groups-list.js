import Vue                   from 'vue'
import GroupsList from '../vue/components/users/GroupsList.vue'
import AppVueMixin           from '../vue/../vue/mixin/app.mixin'
import store from '../vue/store'

AppVueMixin()

new Vue({
  el: '#groups-list',
  name: 'GroupsListApp',
  components: {
    GroupsList,
    store
  }
})
