import Vue            from 'vue'
import LicensesAvailable  from '../vue/components/licenses/LicensesAvailable'
import AppVueMixin from '../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#licenses-available',
  name: 'LicensesAvailable',
  components: {
    LicensesAvailable
  }
})
