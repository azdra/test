import Vue            from 'vue'
import EvaluationPerformanceAnimator from '../../vue/components/customer-service/EvaluationPerformanceAnimator'
import AppVueMixin    from '../../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#evaluation-performance-animator',
  name: 'EvaluationPerformanceAnimator',
  components: {
    EvaluationPerformanceAnimator
  }
})
