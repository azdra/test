import Vue            from 'vue'
import BillingClaimantList from '../../vue/components/customer-service/Dashboard/Billing/BillingClaimantList'
import AppVueMixin    from '../../vue/mixin/app.mixin'
import store          from '../../vue/store'

AppVueMixin()

new Vue({
  el: '#billing-claimant-list',
  name: 'BillingClaimantListApp',
  store,
  components: {
    BillingClaimantList
  }
})
