import Vue                   from 'vue'
import UsersHelperList from '../../vue/components/customer-service/UsersHelperList'
import AppVueMixin           from '../../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#users-helper-list',
  name: 'UsersHelperListApp',
  components: {
    UsersHelperList
  }
})
