import Vue                   from 'vue'
import PersonalCalendarSelect from '../../vue/components/customer-service/PersonalCalendarSelect'
import AppVueMixin           from '../../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#personal-calendar-select',
  name: 'PersonalCalendarSelectApp',
  components: { PersonalCalendarSelect }
})
