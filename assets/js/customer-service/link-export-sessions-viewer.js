import Vue            from 'vue'
import LinkExportSessionsViewer from '../../vue/components/customer-service/LinkExportSessionsViewer'
import AppVueMixin    from '../../vue/mixin/app.mixin'
import store          from '../../vue/store'

AppVueMixin()

new Vue({
  el: '#link-export-sessions-viewer',
  name: 'LinkExportSessionsViewerApp',
  store,
  components: {
    LinkExportSessionsViewer
  }
})
