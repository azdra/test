import Vue from 'vue'
import HelpTaskDetails from '../../vue/components/customer-service/HelpTaskDetails'
import AppVueMixin from '../../vue/mixin/app.mixin'
import store from '../../vue/store'

AppVueMixin()

new Vue({
  el: '#help-task-details',
  name: 'HelpTaskDetailsApp',
  store,
  components: {
    HelpTaskDetails
  }
})
