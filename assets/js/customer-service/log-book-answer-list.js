import Vue            from 'vue'
import LogBookAnswerList from '../../vue/components/customer-service/LogBookAnswerList'
import AppVueMixin    from '../../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#log-book-answer-list',
  name: 'LogBookAnswerList',
  components: {
    LogBookAnswerList
  }
})
