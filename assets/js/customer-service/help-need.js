import Vue from 'vue'
import HelpNeed from '../../vue/components/customer-service/HelpNeed'
import AppVueMixin from '../../vue/mixin/app.mixin'
import store from '../../vue/store'

AppVueMixin()

new Vue({
  el: '#help-need',
  name: 'HelpNeedApp',
  store,
  components: {
    HelpNeed
  }
})
