import Vue                   from 'vue'
import PersonalCalendarHelpTask from '../../vue/components/customer-service/PersonalCalendarHelpTask'
import AppVueMixin           from '../../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#personal-calendar-help-task',
  name: 'PersonalCalendarHelpTaskApp',
  components: { PersonalCalendarHelpTask }
})
