import Vue            from 'vue'
import EvaluationPerformanceAnimatorAnswerList from '../../vue/components/customer-service/EvaluationPerformanceAnimatorAnswerList'
import AppVueMixin    from '../../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#evaluation-performance-animator-answer-list',
  name: 'EvaluationPerformanceAnimatorAnswerList',
  components: {
    EvaluationPerformanceAnimatorAnswerList
  }
})
