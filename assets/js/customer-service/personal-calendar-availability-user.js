import Vue                   from 'vue'
import PersonalCalendarAvailabilityUser from '../../vue/components/customer-service/PersonalCalendarAvailabilityUser'
import AppVueMixin           from '../../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#personal-calendar-availability-user',
  name: 'PersonalCalendarAvailabilityUserApp',
  components: { PersonalCalendarAvailabilityUser }
})
