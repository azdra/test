import Vue                   from 'vue'
import CustomerServiceDashboard from '../../vue/components/customer-service/Dashboard/Dashboard.vue'
import AppVueMixin           from '../../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#customer-service-dashboard',
  name: 'CustomerServiceDashboardApp',
  components: {
    CustomerServiceDashboard
  }
})
