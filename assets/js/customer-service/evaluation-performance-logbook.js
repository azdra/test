import Vue            from 'vue'
import EvaluationPerformanceLogbook from '../../vue/components/customer-service/EvaluationPerformanceLogbook'
import AppVueMixin    from '../../vue/mixin/app.mixin'

AppVueMixin()

new Vue({
  el: '#evaluation-performance-logbook',
  name: 'EvaluationPerformanceLogbook',
  components: {
    EvaluationPerformanceLogbook
  }
})
