import Vue            from 'vue'
import ClientTabServices from '../vue/components/clients/ClientTabServices'
import AppVueMixin    from '../vue/mixin/app.mixin'
import store        from '../vue/store'
AppVueMixin()

new Vue({
  el: '#client-tab-services',
  name: 'ClientTabsServicesApp',
  store,
  components: {
    ClientTabServices
  }
})
