import Vue                     from 'vue'
import ConfigurationHolderForm from '../vue/components/configuration-holder/ConfigurationHolderForm'
import AppVueMixin             from '../vue/mixin/app.mixin'
import store                   from '../vue/store'

AppVueMixin()

new Vue({
  el: '#configuration-holder-form',
  name: 'ConfigurationHolderFormApp',
  store,
  components: {
    ConfigurationHolderForm
  }
})
