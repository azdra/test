import Vue           from 'vue'
import OnHoldProviderSessionViewer   from '../../vue/components/onHoldProviderSession/OnHoldProviderSessionViewer'
import AppVueMixin  from '../../vue/mixin/app.mixin'
import store        from '../../vue/store'

AppVueMixin()

new Vue({
  el: '#on-hold-provider-session-viewer',
  name: 'OnHoldProviderSessionViewer',
  store,
  components: {
    OnHoldProviderSessionViewer
  }
})
