image: atlassian/default-image:2

options:
  docker: true

definitions:
  services:
    docker:
      memory: 2048
    db:
      image: mysql:8
      memory: 896
      variables:
        MYSQL_DATABASE: mlsmdb
        MYSQL_ROOT_PASSWORD: root
    redis:
      image: redis:6
      memory: 128

pipelines:
  pull-requests:
    '**':
      - step:
          name: 'Install'
          image:
            name: plzg7n43.gra7.container-registry.ovh.net/live-session/mlsm3-packager:ci
            username: $OVH_REGISTRY_USERNAME
            password: $OVH_REGISTRY_PASSWORD
          script:
            - composer install --ignore-platform-reqs
            - ./bin/console.php bazinga:js-translation:dump --format=js --merge-domains public/translations
            - ./bin/console.php fos:js-routing:dump --format=json --target=public/js/fos_js_routes.json
          artifacts:
            - vendor/**
            - public/translations/**
            - public/js/**
      - step:
          name: 'Yarn install & build'
          size: 2x
          image:
            name: plzg7n43.gra7.container-registry.ovh.net/live-session/mlsm3-packager:ci
            username: $OVH_REGISTRY_USERNAME
            password: $OVH_REGISTRY_PASSWORD
          script:
            - yarn install
            - yarn run font:build
            - yarn build
          artifacts:
            - node_modules/**
            - public/build/**
      - parallel:
          - step:
              name: 'Unit tests'
              image:
                name: plzg7n43.gra7.container-registry.ovh.net/live-session/mlsm3-packager:ci
                username: $OVH_REGISTRY_USERNAME
                password: $OVH_REGISTRY_PASSWORD
              script:
                - export APP_ENV="test"
                - ./vendor/bin/phpunit --testsuite=unit
          - step:
              name: 'Path functional tests'
              image:
                name: plzg7n43.gra7.container-registry.ovh.net/live-session/mlsm3-packager:ci
                username: $OVH_REGISTRY_USERNAME
                password: $OVH_REGISTRY_PASSWORD
              services:
                - db
                - redis
              script:
                - export DATABASE_NAME="mlsmdb"
                - export DATABASE_HOST="127.0.0.1"
                - export DATABASE_PORT="3306"
                - export DATABASE_USER="root"
                - export DATABASE_DRIVER="pdo_mysql"
                - export DATABASE_PASSWORD="root"
                - export APP_ENV="test"
                - export REDIS_URL=redis://127.0.0.1
                - export LOCK_DSN=redis://127.0.0.1
                - export SYMFONY_DECRYPTION_SECRET=$CI_SF_DECRYPTION_SECRET
                - ./bin/console.php doctrine:database:drop --if-exists --force
                - ./bin/console.php doctrine:database:create
                - ./bin/console.php doctrine:migrations:migrate latest --allow-no-migration --no-interaction
                - ./bin/console.php hautelook:fixtures:load --no-interaction --append
                - ./vendor/bin/phpunit --testsuite=path-functional
          - step:
              name: 'Functional tests'
              image:
                name: plzg7n43.gra7.container-registry.ovh.net/live-session/mlsm3-packager:ci
                username: $OVH_REGISTRY_USERNAME
                password: $OVH_REGISTRY_PASSWORD
              services:
                - db
                - redis
              script:
                - export DATABASE_NAME="mlsmdb"
                - export DATABASE_HOST="127.0.0.1"
                - export DATABASE_PORT="3306"
                - export DATABASE_USER="root"
                - export DATABASE_DRIVER="pdo_mysql"
                - export DATABASE_PASSWORD="root"
                - export APP_ENV="behat"
                - export REDIS_URL=redis://127.0.0.1
                - export LOCK_DSN=redis://127.0.0.1
                - ./bin/console.php doctrine:database:drop --if-exists --force
                - ./bin/console.php doctrine:database:create
                - ./bin/console.php doctrine:migrations:migrate latest --allow-no-migration --no-interaction
                - ./bin/console.php hautelook:fixtures:load --no-interaction --append
                - ./vendor/bin/behat
          - step:
              name: 'Lint YAML'
              image:
                name: plzg7n43.gra7.container-registry.ovh.net/live-session/mlsm3-packager:ci
                username: $OVH_REGISTRY_USERNAME
                password: $OVH_REGISTRY_PASSWORD
              script:
                - ./bin/console.php lint:yaml config/ --parse-tags
          - step:
              name: 'Static analysis'
              image:
                name: plzg7n43.gra7.container-registry.ovh.net/live-session/mlsm3-packager:ci
                username: $OVH_REGISTRY_USERNAME
                password: $OVH_REGISTRY_PASSWORD
              script:
                - ./vendor/bin/psalm
          - step:
              name: 'Code style checker'
              image:
                name: plzg7n43.gra7.container-registry.ovh.net/live-session/mlsm3-packager:ci
                username: $OVH_REGISTRY_USERNAME
                password: $OVH_REGISTRY_PASSWORD
              script:
                - php-cs-fixer fix --dry-run --diff --config .php-cs-fixer.dist.php
          - step:
              name: 'ES Lint'
              image:
                name: plzg7n43.gra7.container-registry.ovh.net/live-session/mlsm3-packager:ci
                username: $OVH_REGISTRY_USERNAME
                password: $OVH_REGISTRY_PASSWORD
              script:
                - yarn run eslint 'assets/vue/**/*' 'assets/js/**/*' --ignore-pattern 'assets/js/**/*.min.js'

  branches:
    release-candidate/*:
      - step:
          name: 'Install dependencies'
          image:
            name: plzg7n43.gra7.container-registry.ovh.net/live-session/mlsm3-packager:ci
            username: $OVH_REGISTRY_USERNAME
            password: $OVH_REGISTRY_PASSWORD
          script:
            - export APP_ENV="staging"
            - export APP_DEBUG="0"
            - export DATABASE_PASSWORD="none"
            - composer install --no-dev --no-scripts --optimize-autoloader
            - ./bin/console.php bazinga:js-translation:dump --format=js --merge-domains public/translations --env=staging
            - ./bin/console.php fos:js-routing:dump --format=json --target=public/js/fos_js_routes.json --env=staging
            - ./bin/console.php assets:install --env=staging
          artifacts:
            - vendor/**
            - public/translations/**
            - public/js/**
            - public/bundles/**
      - step:
          name: 'Yarn install & build'
          size: 2x
          image:
            name: plzg7n43.gra7.container-registry.ovh.net/live-session/mlsm3-packager:ci
            username: $OVH_REGISTRY_USERNAME
            password: $OVH_REGISTRY_PASSWORD
          script:
            - yarn install
            - yarn run font:build
            - yarn build
          artifacts:
            - public/build/**
      - step:
          deployment: staging
          name: 'Deploying to staging'
          script:
            - echo $OVH_REGISTRY_PASSWORD | docker login --username $OVH_REGISTRY_USERNAME --password-stdin $OVH_REGISTRY_URL
            # DOCKER_TAG is generated from branch name (release-candidate/tag) and build date
            - APP_VERSION=$(echo $(git rev-parse --abbrev-ref HEAD) | cut -d/ -f 2)
            - DOCKER_TAG=staging-$APP_VERSION-$(date +%s)
            - docker build --target php_staging --build-arg APP_VERSION=$APP_VERSION -t $OVH_REGISTRY_PHP_IMAGE_NAME:$DOCKER_TAG -f docker/php/Dockerfile .
            - docker build --target web_staging -t $OVH_REGISTRY_WEB_IMAGE_NAME:$DOCKER_TAG -f docker/web/Dockerfile .
            - docker push $OVH_REGISTRY_PHP_IMAGE_NAME:$DOCKER_TAG
            - docker push $OVH_REGISTRY_WEB_IMAGE_NAME:$DOCKER_TAG

  tags:
    v*:
      - step:
          name: 'Install dependencies'
          image:
            name: plzg7n43.gra7.container-registry.ovh.net/live-session/mlsm3-packager:ci
            username: $OVH_REGISTRY_USERNAME
            password: $OVH_REGISTRY_PASSWORD
          script:
            - export APP_ENV="prod"
            - export APP_DEBUG="0"
            - export DATABASE_PASSWORD="none"
            - composer install --no-dev --no-scripts --optimize-autoloader
            - ./bin/console.php bazinga:js-translation:dump --format=js --merge-domains public/translations --env=prod
            - ./bin/console.php fos:js-routing:dump --format=json --target=public/js/fos_js_routes.json --env=prod
            - ./bin/console.php assets:install --env=prod
          artifacts:
            - vendor/**
            - public/translations/**
            - public/js/**
            - public/bundles/**
      - step:
          name: 'Yarn install & build'
          size: 2x
          image:
            name: plzg7n43.gra7.container-registry.ovh.net/live-session/mlsm3-packager:ci
            username: $OVH_REGISTRY_USERNAME
            password: $OVH_REGISTRY_PASSWORD
          script:
            - yarn install
            - yarn run font:build
            - yarn build
          artifacts:
            - public/build/**
      - step:
          deployment: production
          name: 'Deploying to production'
          script:
            - echo $OVH_REGISTRY_PASSWORD | docker login --username $OVH_REGISTRY_USERNAME --password-stdin $OVH_REGISTRY_URL
            # DOCKER_TAG is latest git tag and build date
            - APP_VERSION=$(git describe --abbrev=0 --tags)
            - DOCKER_TAG=prod-$APP_VERSION-$(date +%s)
            - docker build --target php_prod --build-arg APP_VERSION=$APP_VERSION -t $OVH_REGISTRY_PHP_IMAGE_NAME:$DOCKER_TAG -f docker/php/Dockerfile .
            - docker build --target web_prod -t $OVH_REGISTRY_WEB_IMAGE_NAME:$DOCKER_TAG -f docker/web/Dockerfile .
            - docker push $OVH_REGISTRY_PHP_IMAGE_NAME:$DOCKER_TAG
            - docker push $OVH_REGISTRY_WEB_IMAGE_NAME:$DOCKER_TAG
