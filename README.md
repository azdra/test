# My Live Session Manager V3

## Développement

### Stack technique

 - [Symfony 5](https://symfony.com/)
 - [PHP 8](https://www.php.net/releases/8.0/en.php)
 - [Docker et Docker Compose](https://www.docker.com/)
 - Tests fonctionnels avec [Behat](https://docs.behat.org/en/latest/)
 - Tests unitaires avec [PHPUnit](https://phpunit.de/)
 - MySQL 8
 - [Vue.js](https://vuejs.org/)
 - [Redis](https://redis.io/)
 - Tout ce qui est précisé dans [ce document](doc/Useful%20links.md)
### Prérequis

Afin de développer ce projet, il est nécessaire d'installer Docker et Docker Compose. Vous pouvez vous référer aux documentations d'installation de ceux-ci :

 - [Installation de Docker](https://docs.docker.com/engine/install/debian/) (vous pouvez changer la distribution cible dans le menu de gauche si vous utilisez quelque chose d'autre que Debian)
 - [Installation de Docker Compose](https://docs.docker.com/compose/install/)

### Démarrage des conteneurs sur votre poste local

```bash
# Create containers
make build
# Install dependencies
make install
# Create database and populate with fixtures
make db && make fixtures
# Launch containers
make start
```

Le fichier `Makefile` contient les commandes suivantes :

 - `install`, installation des dépendances
 - `build`, création des conteneurs Docker
 - `push`, envoyé les images directement sur la registry (attention, selon le tag utilisé, cela peut déclencher un déploiement !)
 - `start`, lancement des conteneurs
 - `stop`, arrêt des conteneurs
 - `restart`, redémarrage des conteneurs
 - `fix-php-cs`, corriger les erreurs de style des fichiers PHP
 - `lint-yaml`, vérifie que le YAML du projet est bien formatté
 - `db`, supprime (si besoin) et créer la base de données
 - `fixtures`, charger les fixtures en base de données
 - `clean`, nettoie les dépendances
 - `unit-tests`, lance les tests unitaires
 - `ci`, lance l'ensemble des vérifications effectuées lors de la CI. Si cette commande s'exécute sans erreur, la CI ne devrait pas en renvoyer non plus
 - `yarn-install`, installe les dépendances front
 - `translations`, génère les traductions front

Ces commandes sont des helpers, vous pouvez bien évidemment lancer les commandes du `Makefile` sans l'outil `make`.

Par exemple, si vous avez besoin de lancer une commande `composer`, vous pouvez utiliser `bin/composer require ...`. Il en va de même pour la console Symfony et Yarn, avec `bin/console` et `bin/yarn` par exemple. Ainsi, voici quelques exemples de commandes classiques utilisant cette syntaxe :

```bash
bin/console c:c
bin/yarn add lodash
bin/composer require symfony/symfony
bin/phpunit
```

Cette façon de faire permet de s'assurer que toutes les commandes seront exécutées dans les conteneurs, et non pas en dehors.

### Webpack

Lors de vos développements front-end, pensez à bien lancer Webpack et la compilation des assets à la volée pour que vos modifications soient prises en compte :

```bash
bin/yarn watch
```

En ce qui concerne les traductions, nous utilisons [BazingaJsTranslationBundle](https://github.com/willdurand/BazingaJsTranslationBundle). Lorsqu'une traduction est ajoutée, il est nécessaire de lancer :

```bash
make translations
# ou
bin/console bazinga:js-translation:dump --format=js --merge-domains public/translations
```

Pour que cela soit pris en compte, il sera peut-être nécessaire de relancer la compilation des assets à la volée, comme expliqué plus haut.

## Déploiement, staging et production

### Registry

Toutes les images Docker sont stockées sur le serveur 2033. Ce dernier exécute la registry de Live Session, disponible à l'adresse [hub-docker.mylivesession.com](hub-docker.mylivesession.com).

Exécuter une registry Docker revient à lancer le conteneur officiel `registry:2` sur le serveur. Au besoin, la commande pour lancer la registry est :

```bash
docker run -d -p 443:5000 --restart=always --name registry -v /mnt/registry:/var/lib/registry registry:2
```

Le binding de volume nous indique que, en dehors du conteneur, toutes les données sont stockées dans `/mnt/registry` du serveur 2033.

Pour des raisons de sécurité, la connexion à la registry est sécurisée. Les certificats se trouvent dans le dossier `/home/v2033/certs`. Ceux-ci ont été générés à l'aide de [Certbot](https://certbot.eff.org/about/), un outil permettant de générer gratuitement des certificats TLS valides et officiels.

Les identifiants d'accès à la registry sont transmis pour un autre médium de communications que cette documentation.

### Docker de staging et de production

Les conteneurs de staging et de production sont déployés sur le serveur dédié. Chaque instance de l'application est constituée de :

 - Un conteneur PHP (`php_prod|php_staging`)
 - Un conteneur de la base de données MySQL (`db_prod|db_staging`)
 - Un conteneur du serveur web Nginx, permettant de requêter le back-end et PHP (`web_prod|web_staging`)

Les conteneurs peuvent êtres lancés grâce aux scripts bash `run-mlsm3-staging-containers` et `run-mlsm3-staging-containers` disponible de le `home` du serveur dédié.

**Cela n'est utile que si vous venez d'installer un nouveau serveur. La mise à jour des conteneurs se fait par la suite automatiquement grâce à Watchtower (voir ci-dessous). Vous ne devriez jamais à avoir à lancer ces scripts.**

```bash
# STAGING
docker network create -d bridge mlsm_network_staging
docker run -d --restart=always --name php_staging -e "APP_ENV=prod" -e "APP_DEBUG=1" -v ~/mlsm3/prod.decrypt.private.php:/srv/config/secrets/prod/prod.decrypt.private.php -v "/home/debian/mlsm3-uploads":"/srv/public/uploads" --network="mlsm_network_staging" hub-docker.mylivesession.com/v3/php:staging
docker run -d --restart=always --name web_staging -p 8080:80 -v "/home/debian/mlsm3-uploads":"/srv/public/uploads" --network="mlsm_network_staging" hub-docker.mylivesession.com/v3/web:staging
docker run -d --restart=always --name db_staging -e "MYSQL_ROOT_PASSWORD=XXXXX" -e "MYSQL_DATABASE=mlsmdb" -v "/home/debian/mlsm3-db-data/staging":"/var/lib/mysql" --network="mlsm_network_staging" hub-docker.mylivesession.com/v3/db:staging

# PRODUCTION
docker network create -d bridge mlsm_network_prod
docker run -d --restart=always --name php_prod -e "APP_ENV=prod" -e "APP_DEBUG=0" -v ~/mlsm3/prod.decrypt.private.php:/srv/config/secrets/prod/prod.decrypt.private.php -v "/home/debian/mlsm3-uploads":"/srv/public/uploads" --network="mlsm_network_prod" hub-docker.mylivesession.com/v3/php:latest
docker run -d --restart=always --name web_prod -p 8079:80 -v "/home/debian/mlsm3-uploads":"/srv/public/uploads" --network="mlsm_network_prod" hub-docker.mylivesession.com/v3/web:latest
docker run -d --restart=always --name db_prod -e "MYSQL_ROOT_PASSWORD=XXXXX" -e "MYSQL_DATABASE=mlsmdb" -v "/home/debian/mlsm3-db-data/prod":"/var/lib/mysql" --network="mlsm_network_prod" hub-docker.mylivesession.com/v3/db:latest
```

Les données des bases de données sont stockées sur le serveur de production, aux chemins `/var/lib/mysql-data-mlsm3-prod` et `mysql-data-mlsm3-staging`.

### Watchtower

[Watchtower](https://containrrr.dev/watchtower/) est utilisé sur le serveur dédié. Cela permet la mise à jour automatique des conteneurs de staging et de production lorsqu'une nouvelle version de l'image est poussée sur la registry. Si Watchtower n'est pas en cours d'exécution (vérifiez l'existence du conteneur avec `docker ps`), la commande pour le lancer est :

```bash
docker run -d \
    --name watchtower \
    -v "$(pwd)"/.docker/config.json:/config.json \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -e "WATCHTOWER_LABEL_ENABLE=true" \
    -e "WATCHTOWER_SCHEDULE=* * * * *" \
    containrrr/watchtower \
    php_prod web_prod db_prod php_staging web_staging db_staging
```

**4** choses sont à noter :

 - La configuration de l'authentification à la registry privée de Live Session est stockée dans le fichier `~/.docker/config.json`. Nous passons cette configuration à Watchtower avec le volume suivant : `-v "$(pwd)"/.docker/config.json:/config.json`
 - Watchtower ne mettra à jour que les images Docker possédant le label `com.centurylinklabs.watchtower.enable="true"` (que vous pouvez voir dans les Dockerfile du projet). Cela s'effectue grâce à la variable d'environnement `WATCHTOWER_LABEL_ENABLE`
 - Watchtower effectue la vérification des nouvelles images toutes les minutes. La variable `WATCHTOWER_SCHEDULE` prend une valeur au format _cron_.
 - La dernière ligne indique les conteneurs à surveiller. Cela agit comme une double sécurité, conjointement avec `WATCHTOWER_LABEL_ENABLE`, pour être sûr de ne pas mettre à jour les autres conteneurs présents sur le serveur.

### Redis

Un serveur Redis est installé, pour les besoins de performance des appels d'API. Si le conteneur ne tourne plus, vous pouvez le relancer avec la commande suivante :

```bash
docker run --name redis_staging --network="mlsm_staging_network" -d redis
docker run --name redis_prod --network="mlsm_network_prod" -d redis
```

### Lancement d'un ou plusieurs workers pour les tâches asynchrones

```bash
docker run -d --restart=always --name php_staging_worker -e "APP_ENV=XXX" -v ~/mlsm3/prod.decrypt.private.php:/srv/config/secrets/prod/prod.decrypt.private.php --network="XXX" hub-docker.mylivesession.com/v3/php:staging php bin/console.php async:worker:watch
```